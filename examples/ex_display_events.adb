
with Ada.Unchecked_Deallocation;
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;

package body Ex_Display_Events is

    procedure Ada_Main is

        MAX_EVENTS : constant := 23;

        type A_String is access all String;
        type String_Array is array(Integer range <>) of A_String;
        events : String_Array(1..MAX_EVENTS);

        procedure Free is new Ada.Unchecked_Deallocation( String, A_String );

        ----------------------------------------------------------------------------

        procedure Add_Event( str : String ) is
        begin
            if events(events'Last) /= null then
                Free( events(events'Last) );
            end if;
            for i in reverse events'First+1..events'Last loop
                events(i) := events(i-1);
            end loop;
            events(events'First) := new String'(str);
        end Add_Event;

        ----------------------------------------------------------------------------

        display : A_Allegro_Display;
        queue   : A_Allegro_Event_Queue;
        event   : aliased Allegro_Event;
        font    : A_Allegro_Font;
        color,
        black,
        red,
        blue    : Allegro_Color;
        x, y    : Float;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon" );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        font := Al_Create_Builtin_Font;
        if font = null then
            Abort_Example( "Error creating builtin found" );
        end if;

        black := Al_Map_RGB_f( 0.0, 0.0, 0.0 );
        red := Al_Map_RGB_f( 1.0, 0.0, 0.0 );
        blue := Al_Map_RGB_f( 0.0, 0.0, 1.0 );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        loop
            if Al_Is_Event_Queue_Empty( queue ) then
                x := 8.0;
                y := 28.0;
                Al_Clear_To_Color( Al_Map_RGB( 16#ff#, 16#ff#, 16#c0# ) );

                Al_Draw_Text( font, blue, 8.0, 8.0, 0, "Display events (newest on top)" );

                color := red;
                for i in events'Range loop
                    if events(i) /= null then
                        Al_Draw_Text( font, color, x, y, 0, events(i).all );
                        color := black;
                        y := y + 20.0;
                    end if;
                end loop;
                Al_Flip_Display;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY =>
                    Add_Event( "ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY" );

                when ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY =>
                    Add_Event( "ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY" );

                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exit;
                    end if;

                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                    Add_Event( "ALLEGRO_EVENT_DISPLAY_RESIZE " &
                                "x =" & event.display.x'Img &
                                ", y =" & event.display.y'Img &
                                ", width =" & event.display.width'Img &
                                ", height =" & event.display.height'Img );
                    Al_Acknowledge_Resize( event.display.source );

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    Add_Event( "ALLEGRO_EVENT_DISPLAY_CLOSE" );

                when ALLEGRO_EVENT_DISPLAY_LOST =>
                    Add_Event( "ALLEGRO_EVENT_DISPLAY_LOST" );

                when ALLEGRO_EVENT_DISPLAY_FOUND =>
                    Add_Event( "ALLEGRO_EVENT_DISPLAY_FOUND" );

                when ALLEGRO_EVENT_DISPLAY_SWITCH_OUT =>
                    Add_Event( "ALLEGRO_EVENT_DISPLAY_SWITCH_OUT" );

                when ALLEGRO_EVENT_DISPLAY_SWITCH_IN =>
                    Add_Event( "ALLEGRO_EVENT_DISPLAY_SWITCH_IN" );

                when others =>
                    null;

            end case;
        end loop;

        Al_Destroy_Event_Queue( queue );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Display_Events;
