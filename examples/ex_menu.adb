
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Unchecked_Conversion;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Interfaces;                        use Interfaces;
with Common;                            use Common;
with System;                            use System;
with System.Address_To_Access_Conversions;

package body Ex_Menu is

    package Display_Conversions is new System.Address_To_Access_Conversions( Allegro_Display );

    -- The following is a list of menu item ids. They can be any non-zero, positive
    -- integer. A menu item must have an id in order for it to generate an event.
    -- Also, each menu item's id should be unique to get well defined results.
    FILE_ID             : constant := 1;
    FILE_OPEN_ID        : constant := 2;
    FILE_RESIZE_ID      : constant := 3;
    FILE_FULLSCREEN_ID  : constant := 4;
    FILE_CLOSE_ID       : constant := 5;
    FILE_EXIT_ID        : constant := 6;
    DYNAMIC_ID          : constant := 7;
    DYNAMIC_CHECKBOX_ID : constant := 8;
    DYNAMIC_DISABLED_ID : constant := 9;
    DYNAMIC_DELETE_ID   : constant := 10;
    DYNAMIC_CREATE_ID   : constant := 11;
    HELP_ABOUT_ID       : constant := 12;

    -- This is one way to define a menu. The entire system, nested menus and all,
    -- can be defined by this single array.
    main_menu_info : aliased constant Allegro_Menu_Info_Array := (
        Allegro_Start_Of_Menu( "&File", FILE_ID ),
            Allegro_Menu_Item( "&Open", FILE_OPEN_ID ),
            ALLEGRO_MENU_SEPARATOR,
            Allegro_Menu_Item( "E&xit", FILE_EXIT_ID ),
            ALLEGRO_END_OF_MENU,

        Allegro_Start_Of_Menu( "&Dynamic Options", DYNAMIC_ID ),
            Allegro_Menu_Item( "&Checkbox", DYNAMIC_CHECKBOX_ID, ALLEGRO_MENU_ITEM_CHECKED ),
            Allegro_Menu_Item( "&Disabled", DYNAMIC_DISABLED_ID, ALLEGRO_MENU_ITEM_DISABLED ),
            Allegro_Menu_Item( "DELETE ME!", DYNAMIC_DELETE_ID ),
            Allegro_Menu_Item( "Click Me", DYNAMIC_CREATE_ID ),
            ALLEGRO_END_OF_MENU,

        Allegro_Start_Of_Menu( "&Help", 0 ),
            Allegro_Menu_Item( "&About", HELP_ABOUT_ID ),
            ALLEGRO_END_OF_MENU,

        ALLEGRO_END_OF_MENU
    );

    -- This is the menu on the secondary windows.
    child_menu_info : aliased constant Allegro_Menu_Info_Array := (
        Allegro_Start_Of_Menu( "&File", 0 ),
            Allegro_Menu_Item( "&Close", FILE_CLOSE_ID ),
            ALLEGRO_END_OF_MENU,
        ALLEGRO_END_OF_MENU
    );

    ----------------------------------------------------------------------------

    procedure Ada_Main is

        function To_U16( a : Address ) return Unsigned_16 is
            type Uint_Addr is mod 2 ** Standard'Address_Size;
            function From_Addr is new Ada.Unchecked_Conversion( Address, Uint_Addr );
        begin
            return Unsigned_16(From_Addr( a ));
        end To_U16;

        initial_width       : constant Integer := 320;
        initial_height      : constant Integer := 200;
        dcount              : Integer := 0;
        display             : A_Allegro_Display;
        menu                : A_Allegro_Menu;
        queue               : A_Allegro_Event_Queue;
        timer               : A_Allegro_Timer;
        redraw              : Boolean := True;
        menu_visible        : Boolean := True;
        pmenu               : A_Allegro_Menu;
        bg                  : A_Allegro_Bitmap;
        event               : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Native_Dialog_Addon then
            Abort_Example( "Could not init the native dialog addon." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;

        queue := Al_Create_Event_Queue;
        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );

        display := Al_Create_Display( initial_width, initial_height );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;
        Al_Set_Window_Title( display, "ex_menu - Main Window" );

        menu := Al_Build_Menu( main_menu_info );
        if menu = null then
            Abort_Example( "Error creating menu" );
        end if;

        -- Add an icon to the Help/About item. Note that Allegro assumes ownership
        -- of the bitmap.
        Al_Set_Menu_Item_Icon( menu, HELP_ABOUT_ID, Al_Load_Bitmap( "data/icon.tga" ) );

        if not Al_Set_Display_Menu( display, menu ) then
            -- Since the menu could not be attached to the window, then treat it as
            -- a popup menu instead.
            pmenu := Al_Clone_Menu_For_Popup( menu );
            Al_Destroy_Menu( menu );
            menu := pmenu;
        else
            -- Create a simple popup menu used when right clicking.
            pmenu := Al_Create_Popup_Menu;
            if pmenu /= null then
                Al_Append_Menu_Item( pmenu, "&Open", FILE_OPEN_ID, 0, null, null );
                Al_Append_Menu_Item( pmenu, "&Resize", FILE_RESIZE_ID, 0, null, null );
                Al_Append_Menu_Item( pmenu, "&Fullscreen window", FILE_FULLSCREEN_ID, 0, null, null );
                Al_Append_Menu_Item( pmenu, "E&xit", FILE_EXIT_ID, 0, null, null );
            end if;
        end if;

        timer := Al_Create_Timer( 1.0 / 60.0 );

        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Default_Menu_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        bg := Al_Load_Bitmap( "data/mysha.pcx" );

        Al_Start_Timer( timer );

        loop
            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                redraw := False;
                if bg /= null then
                    declare
                        t  : constant Float := Float(Long_Float(Al_Get_Timer_Count( timer )) * 0.1);
                        sw : constant Float := Float(Al_Get_Bitmap_Width( bg ));
                        sh : constant Float := Float(Al_Get_Bitmap_Height( bg ));
                        dw : Float := Float(Al_Get_Display_Width( display ));
                        dh : Float := Float(Al_Get_Display_Height( display ));
                        cx : constant Float := dw / 2.0;
                        cy : constant Float := dh / 2.0;
                    begin
                        dw := dw * (1.2 + 0.2 * Cos( t ));
                        dh := dh * (1.2 + 0.2 * Cos( 1.1 * t ));
                        Al_Draw_Scaled_Bitmap( bg, 0.0, 0.0, sw, sh,
                                               cx - dw / 2.0, cy - dh / 2.0, dw, dh, 0 );
                    end;
                end if;
                Al_Flip_Display;
            end if;

            Al_Wait_For_Event( queue, event );

            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                if event.display.source = display then
                    -- Closing the primary display
                    exit;
                else
                    -- Closing a secondary display
                    -- v5.2.0 BUG: If Al_Set_Display_Menu is called here, then
                    -- creating a child window, closing it, and creating another
                    -- one will crash in Al_Create_Display. Commenting this out
                    -- avoids the crash.
                    Al_Set_Display_Menu( event.display.source, null );
                    Al_Destroy_Display( event.display.source );
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MENU_CLICK then
                -- data1: id
                -- data2: display (could be null)
                -- data3: menu    (could be null)
                --
                if event.user.data2 = Display_Conversions.To_Address( Display_Conversions.Object_Pointer(display) ) then
                    -- The main window.
                    if To_U16( event.user.data1 ) = FILE_OPEN_ID then
                        declare
                            -- v5.2.0 BUG: This will crash when creating another child display
                            -- after closing the most recently created display. See the call
                            -- to Al_Set_Display_Menu above.
                            d    : constant A_Allegro_Display := Al_Create_Display( 320, 240 );
                            menu : A_Allegro_Menu;
                        begin
                            if d /= null then
                                menu := Al_Build_Menu( child_menu_info );
                                Al_Set_Display_Menu( d, menu );
                                Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
                                Al_Flip_Display;
                                Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( d ) );
                                Al_Set_Target_Backbuffer( display );
                                Al_Set_Window_Title( d, "ex_menu - Child Window" );
                            end if;
                        end;
                    elsif To_U16( event.user.data1 ) = DYNAMIC_CHECKBOX_ID then
                        Al_Set_Menu_Item_Flags( menu, DYNAMIC_DISABLED_ID, Al_Get_Menu_Item_Flags( menu, DYNAMIC_DISABLED_ID ) xor ALLEGRO_MENU_ITEM_DISABLED );
                        Al_Set_Menu_Item_Caption( menu, DYNAMIC_DISABLED_ID,
                                                  (if (Al_Get_Menu_Item_Flags( menu, DYNAMIC_DISABLED_ID ) and ALLEGRO_MENU_ITEM_DISABLED) /= 0 then "&Disabled" else "&Enabled") );
                    elsif To_U16( event.user.data1 ) = DYNAMIC_DELETE_ID then
                        Al_Remove_Menu_Item( menu, DYNAMIC_DELETE_ID );
                    elsif To_U16( event.user.data1 ) = DYNAMIC_CREATE_ID then
                        if dcount < 5 then
                            dcount := dcount + 1;
                            if dcount = 1 then
                                -- append a separator
                                Al_Append_Menu_Item( Al_Find_Menu( menu, DYNAMIC_ID ), "", 0, 0, null, null );
                            end if;

                            Al_Append_Menu_Item( Al_Find_Menu( menu, DYNAMIC_ID ), "New #" & dcount'Img, 0, 0, null, null );

                            if dcount = 5 then
                                -- disable the option
                                Al_Set_Menu_Item_Flags( menu, DYNAMIC_CREATE_ID, ALLEGRO_MENU_ITEM_DISABLED );
                            end if;
                       end if;
                    elsif To_U16( event.user.data1 ) = HELP_ABOUT_ID then
                        Al_Show_Native_Message_Box( display, "About", "ex_menu",
                                                    "This is a sample program that shows how to use menus",
                                                    "OK", 0 );
                    elsif To_U16( event.user.data1 ) = FILE_EXIT_ID then
                        exit;
                    elsif To_U16( event.user.data1 ) = FILE_RESIZE_ID then
                        declare
                            w : constant Integer := Integer'Min( Al_Get_Display_Width( display ) * 2, 960 );
                            h : constant Integer := Integer'Min( Al_Get_Display_Height( display ) * 2, 600 );
                        begin
                            Al_Resize_Display( display, w, h );
                        end;
                    elsif To_U16( event.user.data1 ) = FILE_FULLSCREEN_ID then
                        Al_Set_Display_Flag( display, ALLEGRO_FULLSCREEN_WINDOW, (Al_Get_Display_Flags( display ) and ALLEGRO_FULLSCREEN_WINDOW) = 0 );
                    end if;
                else
                    -- The child window
                    if To_U16( event.user.data1 ) = FILE_CLOSE_ID then
                        declare
                            d : A_Allegro_Display := A_Allegro_Display(Display_Conversions.To_Pointer( event.user.data2 ));
                        begin
                            if d /= null then
                                -- v5.2.0 BUG: If Al_Set_Display_Menu is called here, then
                                -- creating a child window, closing it, and creating another
                                -- one will crash in Al_Create_Display. Commenting this out
                                -- avoids the crash.
                                Al_Set_Display_Menu( d, null );
                                Al_Destroy_Display( d );
                            end if;
                        end;
                    end if;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_UP then
                -- Popup a context menu on a right click.
                if event.mouse.display = display and then event.mouse.button = 2 then
                    if pmenu /= null then
                        if not Al_Popup_Menu( pmenu, display ) then
                            Log_Print( "Couldn't popup menu!" );
                        end if;
                    end if;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                -- Toggle the menu if the spacebar is pressed
                if event.keyboard.display = display then
                    if event.keyboard.unichar = Character'Pos( ' ' ) then
                        if menu_visible then
                            Al_Remove_Display_Menu( display );
                        else
                            Al_Set_Display_Menu( display, menu );
                        end if;
                        menu_visible := not menu_visible;
                    end if;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_RESIZE then
                Al_Acknowledge_Resize( display );
                redraw := True;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := True;
            end if;
        end loop;

        -- You must remove the menu before destroying the display to free resources
        Al_Set_Display_Menu( display, null );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Menu;

