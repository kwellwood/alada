
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
--    Example program for the Allegro library.
--
--    This program blitting to/from sub-bitmaps.
--
--
package body Ex_Subbitmap is

    procedure Ada_Main is

        procedure Swap_Greater( f1, f2 : in out Float ) is
            tmp : Float;
        begin
            if f1 > f2 then
                tmp := f1;
                f1 := f2;
                f2 := tmp;
            end if;
        end Swap_Greater;

        ------------------------------------------------------------------------

        type Mode_Type is (PLAIN_BLIT, SCALED_BLIT);

        SRC_WIDTH  : constant := 640;
        SRC_HEIGHT : constant := 480;
        SRC_X      : constant := 160;
        SRC_Y      : constant := 120;
        DST_WIDTH  : constant := 640;
        DST_HEIGHT : constant := 480;

        src_display,
        dst_display : A_Allegro_Display;
        queue       : A_Allegro_Event_Queue;
        src_bmp     : A_Allegro_Bitmap;

        src_x1 : Integer := SRC_X;
        src_y1 : Integer := SRC_Y;
        src_x2 : Integer := SRC_X + 319;
        src_y2 : Integer := SRC_Y + 199;
        dst_x1 : Integer := 0;
        dst_y1 : Integer := 0;
        dst_x2 : Integer := DST_WIDTH - 1;
        dst_y2 : Integer := DST_HEIGHT - 1;

        mode       : Mode_Type := PLAIN_BLIT;
        draw_flags : Unsigned_32 := 0;

        type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

        src_subbmp     : Bitmap_Array(0..1);
        dst_subbmp     : Bitmap_Array(0..1);
        event          : aliased Allegro_Event;
        mouse_down,
        recreate_subbitmaps,
        redraw,
        use_memory     : Boolean;
        image_filename : Unbounded_String;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon" );
        end if;
        Init_Platform_Specific;

        Open_Log;

        Al_Set_New_Display_Flags( ALLEGRO_GENERATE_EXPOSE_EVENTS );
        src_display := Al_Create_Display( SRC_WIDTH, SRC_HEIGHT );
        if src_display = null then
            Abort_Example( "Error creating display" );
        end if;
        Al_Set_Window_Title( src_display, "Source" );

        dst_display := Al_Create_Display( DST_WIDTH, DST_HEIGHT );
        if dst_display = null then
            Abort_Example( "Error creating display" );
        end if;
        Al_Set_Window_Title( dst_display, "Destination" );

        if Argument_Count = 0 then
            image_filename := To_Unbounded_String( "data/mysha.pcx" );
        elsif Argument_Count = 1 then
            image_filename := To_Unbounded_String( Argument( 1 ) );
        else
            Abort_Example( "Unknown Arguments" );
        end if;

        src_bmp := Al_Load_Bitmap( To_String( image_filename ) );
        if src_bmp = null then
            Abort_Example( "Could not load image file" );
        end if;

        src_x2 := src_x1 + Al_Get_Bitmap_Width( src_bmp );
        src_y2 := src_y1 + Al_Get_Bitmap_Height( src_bmp );

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( src_display ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( dst_display ) );

        mouse_down := False;
        recreate_subbitmaps := True;
        redraw := True;
        use_memory := False;

        Log_Print( "Highlight sub-bitmap regions with left mouse button." );
        Log_Print( "Press 'm' to toggle memory bitmaps." );
        Log_Print( "Press '1' to perform plain blits." );
        Log_Print( "Press 's' to perform scaled blits." );
        Log_Print( "Press 'h' to flip horizontally." );
        Log_Print( "Press 'v' to flip vertically." );

        loop
            if recreate_subbitmaps then
                declare
                    l, r, t, b, sw, sh : Integer;
                begin
                    Al_Destroy_Bitmap( src_subbmp(0) );
                    Al_Destroy_Bitmap( dst_subbmp(0) );
                    Al_Destroy_Bitmap( src_subbmp(1) );
                    Al_Destroy_Bitmap( dst_subbmp(1) );

                    l := Integer'Min( src_x1, src_x2 );
                    r := Integer'Max( src_x1, src_x2 );
                    t := Integer'Min( src_y1, src_y2 );
                    b := Integer'Max( src_y1, src_y2 );

                    l := l - SRC_X;
                    t := t - SRC_Y;
                    r := r - SRC_X;
                    b := b - SRC_Y;

                    src_subbmp(0) := Al_Create_Sub_Bitmap( src_bmp, l, t, r - l + 1, b - t + 1 );
                    sw := Al_Get_Bitmap_Width( src_subbmp(0) );
                    sh := Al_Get_Bitmap_Height( src_subbmp(0) );
                    src_subbmp(1) := Al_Create_Sub_Bitmap( src_subbmp(0), 2, 2, sw - 4, sh - 4 );

                    l := Integer'Min( dst_x1, dst_x2 );
                    r := Integer'Max( dst_x1, dst_x2 );
                    t := Integer'Min( dst_y1, dst_y2 );
                    b := Integer'Max( dst_y1, dst_y2 );

                    Al_Set_Target_Backbuffer( dst_display );
                    dst_subbmp(0) := Al_Create_Sub_Bitmap( Al_Get_Backbuffer( dst_display ), l, t, r - l + 1, b - t + 1 );
                    dst_subbmp(1) := Al_Create_Sub_Bitmap( dst_subbmp(0), 2, 2, r - l - 3, b - t - 3 );

                    recreate_subbitmaps := False;
                end;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Al_Set_Target_Backbuffer( dst_display );
                Al_Clear_To_Color( al_map_rgb( 0, 0, 0 ) );

                Al_Set_Target_Bitmap( dst_subbmp(1) );
                case mode is
                    when PLAIN_BLIT =>
                        Al_Draw_Bitmap( src_subbmp(1), 0.0, 0.0, draw_flags );
                    when SCALED_BLIT =>
                        Al_Draw_Scaled_Bitmap( src_subbmp(1),
                                               0.0, 0.0,
                                               Float(Al_Get_Bitmap_Width( src_subbmp(1) )),
                                               Float(Al_Get_Bitmap_Height( src_subbmp(1) )),
                                               0.0, 0.0,
                                               Float(Al_Get_Bitmap_Width( dst_subbmp(1) )),
                                               Float(Al_Get_Bitmap_Height( dst_subbmp(1) )),
                                               draw_flags );
                end case;

                 -- pixel center is at 0.5/0.5
                 declare
                     x  : Float := Float(dst_x1) + 0.5;
                     y  : Float := Float(dst_y1) + 0.5;
                     x2 : Float := Float(dst_x2) + 0.5;
                     y2 : Float := Float(dst_y2) + 0.5;
                 begin
                     Swap_Greater( x, x2 );
                     Swap_Greater( y, y2 );
                     Al_Set_Target_Backbuffer( dst_display );
                     Al_Draw_Rectangle( x, y, x2, y2, Al_Map_RGB( 0, 255, 255 ), 0.0 );
                     Al_Draw_Rectangle( x + 2.0, y + 2.0, x2 - 2.0, y2 - 2.0,
                                        Al_Map_RGB( 255, 255, 0 ), 0.0 );
                     Al_Flip_Display;
                 end;

                -- pixel center is at 0.5/0.5
                declare
                    x  : Float := Float(src_x1) + 0.5;
                    y  : Float := Float(src_y1) + 0.5;
                    x2 : Float := Float(src_x2) + 0.5;
                    y2 : Float := Float(src_y2) + 0.5;
                begin
                    SWAP_GREATER( x, x2 );
                    SWAP_GREATER( y, y2 );
                    Al_Set_Target_Backbuffer( src_display );
                    Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
                    Al_Draw_Bitmap( src_bmp, Float(SRC_X), Float(SRC_Y), 0 );
                    Al_Draw_Rectangle( x, y, x2, y2, Al_Map_RGB( 0, 255, 255 ), 0.0 );
                    Al_Draw_Rectangle( x + 2.0, y + 2.0, x2 - 2.0, y2 - 2.0,
                                       Al_Map_RGB( 255, 255, 0 ), 0.0 );
                    Al_Flip_Display;
                end;

                redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            end if;
            if event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                elsif event.keyboard.unichar = Character'Pos( '1' ) then
                    mode := PLAIN_BLIT;
                    redraw := True;
                elsif event.keyboard.unichar = Character'Pos( 's' ) then
                    mode := SCALED_BLIT;
                    redraw := True;
                elsif event.keyboard.unichar = Character'Pos( 'h' ) then
                    draw_flags := draw_flags xor ALLEGRO_FLIP_HORIZONTAL;
                    redraw := True;
                elsif event.keyboard.unichar = Character'Pos( 'v' ) then
                    draw_flags := draw_flags xor ALLEGRO_FLIP_VERTICAL;
                    redraw := True;
                elsif event.keyboard.unichar = Character'Pos( 'm' ) then
                    declare
                        temp : A_Allegro_Bitmap := src_bmp;
                    begin
                        use_memory := not use_memory;
                        if use_memory then
                            Log_Print( "Using a memory bitmap." );
                            Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
                        else
                            Log_Print( "Using a video bitmap." );
                            Al_Set_New_Bitmap_Flags( ALLEGRO_VIDEO_BITMAP );
                        end if;
                        src_bmp := Al_Clone_Bitmap( temp );
                        Al_Destroy_Bitmap( temp );
                        redraw := True;
                        recreate_subbitmaps := True;
                    end;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN and then event.mouse.button = 1 then
                if event.mouse.display = src_display then
                    src_x1 := event.mouse.x;
                    src_x2 := src_x1;
                    src_y1 := event.mouse.y;
                    src_y2 := src_y1;
                elsif event.mouse.display = dst_display then
                    dst_x1 := event.mouse.x;
                    dst_x2 := dst_x1;
                    dst_y1 := event.mouse.y;
                    dst_y2 := dst_y1;
                end if;
                mouse_down := True;
                redraw := True;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_AXES then
                if mouse_down then
                    if event.mouse.display = src_display then
                        src_x2 := event.mouse.x;
                        src_y2 := event.mouse.y;
                    elsif event.mouse.display = dst_display then
                        dst_x2 := event.mouse.x;
                        dst_y2 := event.mouse.y;
                    end if;
                    redraw := True;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_UP and then event.mouse.button = 1 then
                mouse_down := False;
                recreate_subbitmaps := True;
                redraw := True;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_EXPOSE then
                redraw := True;
            end if;
        end loop;

        Al_Destroy_Event_Queue( queue );

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Subbitmap;
