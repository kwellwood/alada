
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Configuration;             use Allegro.Configuration;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

--
--    Example program for the Allegro library, by Todd Cope.
--
--    Stream seeking.
--
package body Ex_Stream_Seek is

    procedure Ada_Main is

        type Boolean_Array is array (Unsigned_32 range <>) of Boolean;

        display         : A_Allegro_Display;
        timer           : A_Allegro_Timer;
        queue           : A_Allegro_Event_Queue;
        basic_font      : A_Allegro_Font := null;
        music_stream    : A_Allegro_Audio_Stream := null;
        stream_filename : Unbounded_String := To_Unbounded_String( "data/welcome.wav" );

        slider_pos      : Float := 0.0;
        loop_start,
        loop_end        : double;
        mouse_button    : Boolean_Array(0..15) := (others => False);

        exiting         : Boolean := False;

        ------------------------------------------------------------------------

        procedure Initialize is
        begin
            if not Al_Initialize then
                Abort_Example( "Could not init Allegro." );
            end if;

            Open_Log;

            if not Al_Init_Primitives_Addon then
                Abort_Example( "Could not init primitives addon!" );
            end if;
            if not Al_Init_Image_Addon then
                Abort_Example( "Could not init image I/O addon!" );
            end if;
            if not Al_Init_Font_Addon then
                Abort_Example( "Could not init Allegro font addon." );
            end if;
            if not Al_Install_Keyboard then
                Abort_Example( "Could not init keyboard!" );
            end if;
            if not Al_Install_Mouse then
                Abort_Example( "Could not init mouse!" );
            end if;

            if not Al_Init_Acodec_Addon then
                Abort_Example( "Could not init audio codec addon!" );
            end if;

            if not Al_Install_Audio then
                Abort_Example( "Could not init sound!" );
            end if;
            if not Al_Reserve_Samples( 16 ) then
                Abort_Example( "Could not set up voice and mixer." );
            end if;

            Init_Platform_Specific;

            display := Al_Create_Display( 640, 228 );
            if display = null then
                Abort_Example( "Could not create display!" );
            end if;

            basic_font := Al_Load_Font( "data/font.tga", 0, 0 );
            if basic_font = null then
                Abort_Example( "Could not load font!" );
            end if;
            timer := Al_Create_Timer( 1.000 / 30.0 );
            if timer = null then
                Abort_Example( "Could not init timer!" );
            end if;
            queue := Al_Create_Event_Queue;
            if queue = null then
                Abort_Example( "Could not create event queue!" );
            end if;
            Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
            Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
            Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
            Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        end Initialize;

        ------------------------------------------------------------------------

        procedure Logic is
            w   : constant Float := Float(Al_Get_Display_Width(display) - 20);
            pos : constant Float := Float(Al_Get_Audio_Stream_Position_Secs( music_stream ));
            len : constant Float := Float(Al_Get_Audio_Stream_Length_Secs( music_stream ));
        begin
            -- calculate the position of the slider
            slider_pos := w * (pos / len);
        end Logic;

        ------------------------------------------------------------------------

        procedure Print_Time( x, y : Float; t : double ) is
            hours,
            minutes : Integer;
            t2      : double := t;
        begin
            hours := Integer(t2) / 3600;
            t2 := t2 - double(hours * 3600);
            minutes := Integer(t2) / 60;
            t2 := t2 - double(minutes * 60);
            Al_Draw_Text( basic_font, Al_Map_RGB (255, 255, 255 ), x, y, 0, hours'Img & ":" & minutes'Img & ":" & t2'Img );
        end Print_Time;

        ------------------------------------------------------------------------

        procedure Render is
            pos            : constant double := Al_Get_Audio_Stream_Position_Secs( music_stream );
            length         : constant double := Al_Get_Audio_Stream_Length_Secs( music_stream );
            w              : constant Float := Float(Al_Get_Display_Width( display ) - 20);
            loop_start_pos : constant Float := w * Float(loop_start / length);
            loop_end_pos   : constant Float := w * Float(loop_end / length);
            c              : constant Allegro_Color := Al_Map_RGB( 255, 255, 255 );
        begin
            Al_Clear_To_Color( Al_Map_RGB( 64, 64, 128 ) );

            -- render "music player"
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Text( basic_font, c, 0.0, 0.0, 0, "Playing " & To_String( stream_filename ) );
            Print_Time( 8.0, 24.0, pos );
            Al_Draw_Text( basic_font, c, 100.0, 24.0, 0, "/" );
            Print_Time( 110.0, 24.0, length );
            Al_Draw_Filled_Rectangle( 10.0, 48.0 + 7.0, 10.0 + w, 48.0 + 9.0, Al_Map_RGB(0, 0, 0 ) );
            Al_Draw_Line( 10.0 + loop_start_pos, 46.0, 10.0 + loop_start_pos, 66.0, Al_Map_RGB( 0, 168, 128 ), 0.0 );
            Al_Draw_Line( 10.0 + loop_end_pos, 46.0, 10.0 + loop_end_pos, 66.0, Al_Map_RGB( 255, 0, 0 ), 0.0 );
            Al_Draw_Filled_Rectangle( 10.0 + slider_pos - 2.0, 48.0, 10.0 + slider_pos + 2.0, 64.0, Al_Map_RGB( 224, 224, 224 ) );

            -- show help
            Al_Draw_Text( basic_font, c, 0.0, 96.0, 0, "Drag the slider to seek." );
            Al_Draw_Text( basic_font, c, 0.0, 120.0, 0, "Middle-click to set loop start." );
            Al_Draw_Text( basic_font, c, 0.0, 144.0, 0, "Right-click to set loop end." );
            Al_Draw_Text( basic_font, c, 0.0, 168.0, 0, "Left/right arrows to seek." );
            Al_Draw_Text( basic_font, c, 0.0, 192.0, 0, "Space to pause." );

            Al_Flip_Display;
        end Render;

        ------------------------------------------------------------------------

        procedure MyExit is
        begin
            if music_stream /= null and then Al_Get_Audio_Stream_Playing( music_stream ) then
                Al_Drain_Audio_Stream( music_stream );
            end if;
            Al_Destroy_Audio_Stream( music_stream );
        end MyExit;

        ------------------------------------------------------------------------

        procedure Maybe_Fiddle_Sliders( mx, my : Integer ) is
            w        : constant double := double(Al_Get_Display_Width( display ) - 20);
            seek_pos : double;
        begin
            if not (mx >= 10 and then mx < 10 + Integer(w) and then my >= 48 and then my < 64) then
                return;
            end if;

            seek_pos := Al_Get_Audio_Stream_Length_Secs( music_stream ) * (double(mx - 10) / w);
            if mouse_button(1) then
                if not Al_Seek_Audio_Stream_Secs( music_stream, seek_pos ) then
                    null;
                end if;
            elsif mouse_button(2) then
                loop_end := seek_pos;
                if Al_Set_Audio_Stream_Loop_Secs( music_stream, loop_start, loop_end ) then
                    null;
                end if;
            elsif mouse_button(3) then
                loop_start := seek_pos;
                if Al_Set_Audio_Stream_Loop_Secs( music_stream, loop_start, loop_end ) then
                    null;
                end if;
            end if;
        end Maybe_Fiddle_Sliders;

        ------------------------------------------------------------------------

        procedure Event_Handler( event : access Allegro_Event ) is
            pos     : double;
            playing : Boolean;
        begin
            case event.any.typ is
                -- Was the X button on the window pressed?
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    exiting := True;

                -- Was a key pressed?
                when ALLEGRO_EVENT_KEY_CHAR =>
                    if event.keyboard.keycode = ALLEGRO_KEY_LEFT then
                        pos := Al_Get_Audio_Stream_Position_Secs( music_stream );
                        pos := double'Max( pos - 5.0, 0.0 );
                        if not Al_Seek_Audio_Stream_Secs( music_stream, pos ) then
                            null;
                        end if;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_RIGHT then
                        pos := Al_Get_Audio_Stream_Position_Secs( music_stream );
                        pos := pos + 5.0;
                        if not Al_Seek_Audio_Stream_Secs( music_stream, pos ) then
                            Log_Print( "seek error!" );
                        end if;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        playing := not Al_Get_Audio_Stream_Playing( music_stream );
                        if not Al_Set_Audio_Stream_Playing( music_stream, playing ) then
                            -- failed to change playing state
                            if playing then
                                Log_Print( "failed to play!" );
                            else
                                Log_Print( "failed to pause!" );
                            end if;
                            playing := not playing;
                        end if;
                        if Al_Get_Audio_Stream_Playing( music_stream ) /= playing then
                            Log_Print( "playing state out of sync" );
                        end if;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exiting := True;
                    end if;

                when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                    mouse_button(event.mouse.button) := True;
                    Maybe_Fiddle_Sliders( event.mouse.x, event.mouse.y );

                when ALLEGRO_EVENT_MOUSE_AXES =>
                    Maybe_Fiddle_Sliders( event.mouse.x, event.mouse.y );

                when ALLEGRO_EVENT_MOUSE_BUTTON_UP =>
                    mouse_button(event.mouse.button) := False;

                when ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY =>
                    mouse_button := (others => False);

                    -- Is it time for the next timer tick?
                when ALLEGRO_EVENT_TIMER =>
                    Logic;
                    Render;

                when ALLEGRO_EVENT_AUDIO_STREAM_FINISHED =>
                    Log_Print( "Stream finished." );

                when others =>
                    null;

            end case;
        end Event_Handler;

        ------------------------------------------------------------------------

        config       : A_Allegro_Config;
        event        : aliased Allegro_Event;
        buffer_count : size_t := 0;
        samples      : unsigned := 0;
        playmode     : ALLEGRO_PLAYMODE := ALLEGRO_PLAYMODE_LOOP;
    begin
        Initialize;

        if Argument_Count > 0 then
            stream_filename := To_Unbounded_String( Argument( 1 ) );
        end if;

        config := Al_Load_Config_File( "ex_stream_seek.cfg" );
        if config /= null then
            declare
                s : constant String := Al_Get_Config_Value( config, "", "buffer_count");
            begin
                if s'Length > 0 then
                    buffer_count := size_t'Value( s );
                end if;
            end;
            declare
                s : constant String := Al_Get_Config_Value( config, "", "samples");
            begin
                if s'Length > 0 then
                    samples := unsigned'Value( s );
                end if;
            end;
            declare
                s : constant String := Al_Get_Config_Value( config, "", "playmode" );
            begin
                if s = "loop" then
                    playmode := ALLEGRO_PLAYMODE_LOOP;
                elsif s = "once" then
                    playmode := ALLEGRO_PLAYMODE_ONCE;
                end if;
            end;
            Al_Destroy_Config( config );
        end if;
        if buffer_count = 0 then
            buffer_count := 4;
        end if;
        if samples = 0 then
            samples := 1024;
        end if;

        music_stream := Al_Load_Audio_Stream( To_String( stream_filename ), buffer_count, samples );
        if music_stream = null then
            Abort_Example( "Stream error!" );
        end if;
        Al_Register_Event_Source( queue, Al_Get_Audio_Stream_Event_Source( music_stream ) );

        loop_start := 0.0;
        loop_end := Al_Get_Audio_Stream_Length_Secs( music_stream );
        if not Al_Set_Audio_Stream_Loop_Secs( music_stream, loop_start, loop_end ) then
            null;
        end if;

        if not Al_Set_Audio_Stream_Playmode( music_stream, playmode ) then
            null;
        end if;
        if not Al_Attach_Audio_Stream_To_Mixer( music_stream, Al_Get_Default_Mixer ) then
            null;
        end if;
        Al_Start_Timer( timer );

        while not exiting loop
            Al_Wait_For_Event( queue, event );
            Event_Handler( event'Access );
        end loop;

        MyExit;
        Al_Destroy_Display( display );
        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Stream_Seek;
