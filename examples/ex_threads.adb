
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.State;                     use Allegro.State;
with Allegro.System;                    use Allegro.System;
with Allegro.Threads;                   use Allegro.Threads;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

--
--    Example program for the Allegro library, by Peter Wang.
--
--    In this example, each thread handles its own window and event queue.
--
package body Ex_Threads is

    procedure Ada_Main is

        MAX_THREADS     : constant := 100;
        MAX_BACKGROUNDS : constant := 10;
        MAX_SQUARES     : constant := 25;

        gen : Generator;

        type Background_Type is
            record
                rmax,
                gmax,
                bmax : double;
            end record;

        type Square_Type is
            record
                cx, cy : Float;
                dx, dy : Float;
                size,
                dsize  : Float;
                rot,
                drot   : Float;
                life,
                dlife  : Float;
            end record;

        type Square_Array is array (Integer range <>) of Square_Type;

        type Thread_Array is array (Integer range <>) of A_Allegro_Thread;

        thread : Thread_Array(0..MAX_THREADS-1);

        type Background_Array is array (Integer range <>) of Background_Type;

        background : Background_Array(0..9) := Background_Array'((1.0, 0.5, 0.5),
                                                                 (0.5, 1.0, 0.5),
                                                                 (0.5, 0.5, 1.0),
                                                                 (1.0, 1.0, 0.5),
                                                                 (0.5, 1.0, 1.0),
                                                                 (1.0, 0.7, 0.5),
                                                                 (0.5, 1.0, 0.7),
                                                                 (0.7, 0.5, 1.0),
                                                                 (1.0, 0.7, 0.5),
                                                                 (0.5, 0.7, 1.0));

        ------------------------------------------------------------------------

        function Rand01 return Float is
        begin
            return Float(Unsigned_32'(Random( gen )) mod 10000) / 10000.0;
        end Rand01;

        ------------------------------------------------------------------------

        function Rand11 return Float is
        begin
            return Float(-10000 + Unsigned_32'(Random( gen )) mod 20000) / 20000.0;
        end Rand11;

        ------------------------------------------------------------------------

        procedure Gen_Square( sq : in out Square_Type; w, h : Integer ) is
        begin
            sq.cx := Float(Unsigned_32'(Random( gen )) mod Unsigned_32(w));
            sq.cy := Float(Unsigned_32'(Random( gen )) mod Unsigned_32(h));
            sq.dx := 3.0 * Rand11;
            sq.dy := 3.0 * Rand11;
            sq.size := 10.0 + Float(Unsigned_32'(Random( gen )) mod Unsigned_32(10));
            sq.dsize := Rand11;
            sq.rot := ALLEGRO_PI * Rand01;
            sq.drot := Rand11 / 3.0;
            sq.life := 0.0;
            sq.dlife := (ALLEGRO_PI / 100.0) + (ALLEGRO_PI / 30.0) * Rand01;
        end Gen_Square;

        ------------------------------------------------------------------------

        procedure Animate_Square( sq : in out Square_Type ) is
            bmp : A_Allegro_Bitmap;
        begin
            sq.cx := sq.cx + sq.dx;
            sq.cy := sq.cy + sq.dy;
            sq.size := sq.size + sq.dsize;
            sq.rot := sq.rot + sq.drot;
            sq.life := sq.life + sq.dlife;

            if sq.size < 1.0 or else sq.life > ALLEGRO_PI then
                bmp := Al_Get_Target_Bitmap;
                Gen_Square( sq, Al_Get_Bitmap_Width( bmp ), Al_Get_Bitmap_Height( bmp ) );
            end if;
        end Animate_Square;

        ------------------------------------------------------------------------

        procedure Draw_Square( sq : Square_Type ) is
            trans : aliased Allegro_Transform;
            alpha : Float;
            size  : Float;
            tint  : Allegro_Color;
        begin
            Al_Build_Transform( trans, sq.cx, sq.cy, 1.0, 1.0, sq.rot );
            Al_Use_Transform( trans );

            alpha := sin( sq.life );
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_ONE );
            tint := Al_Map_RGBA_f( 0.5, 0.3, 0.0, alpha );

            size := sq.size;
            Al_Draw_Filled_Rounded_Rectangle( -size, -size, size, size, 3.0, 3.0, tint );

            size := size * 1.1;
            Al_Draw_Rounded_Rectangle( -size, -size, size, size, 3.0, 3.0, tint, 2.0 );
        end Draw_Square;

        ------------------------------------------------------------------------

        function thread_func( thr : A_Allegro_Thread; arg : Address ) return Address;
        pragma Convention( C, thread_func );

        function thread_func( thr : A_Allegro_Thread; arg : Address ) return Address is
            INITIAL_WIDTH  : constant := 300;
            INITIAL_HEIGHT : constant := 300;
            background     : Background_Type;
                for background'Address use arg;
            display        : A_Allegro_Display;
            queue          : A_Allegro_Event_Queue;
            timer          : A_Allegro_Timer;
            event          : Allegro_Event;
            state          : Allegro_State;
            squares        : Square_Array(0..MAX_SQUARES-1);
            theta          : Float := 0.0;
            r              : Float;
            c              : Allegro_Color;
            redraw         : Boolean := True;

            pragma Unreferenced( thr );
        begin
            Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );

            display := Al_Create_Display( INITIAL_WIDTH, INITIAL_HEIGHT );
            if display = null then
                return Null_Address;
            end if;
            queue := Al_Create_Event_Queue;
            if queue = null then
                Al_Destroy_Display( display );
                return Null_Address;
            end if;
            timer := Al_Create_Timer( 0.05 );    -- 20 fps
            if timer = null then
                Al_Destroy_Event_Queue( queue );
                Al_Destroy_Display( display );
                return Null_Address;
            end if;

            Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
            Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
            Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

            for i in squares'Range loop
                Gen_Square( squares(i), INITIAL_WIDTH, INITIAL_HEIGHT );
            end loop;

            Al_Start_Timer( timer );

            loop
                if Al_Is_Event_Queue_Empty( queue ) and then redraw then
                    r := 0.7 + 0.3 * (sin( theta ) + 1.0) / 2.0;
                    c := Al_Map_RGB_f( Float(background.rmax) * r,
                                       Float(background.gmax) * r,
                                       Float(background.bmax) * r );
                    Al_Clear_To_Color( c );

                    Al_Store_State( state, ALLEGRO_STATE_BLENDER or ALLEGRO_STATE_TRANSFORM );
                    for i in squares'Range loop
                        Draw_Square( squares(i) );
                    end loop;
                    Al_Restore_State( state );

                    Al_Flip_Display;
                    redraw := False;
                end if;

                Al_Wait_For_Event( queue, event );
                if event.any.typ = ALLEGRO_EVENT_TIMER then
                    for i in squares'Range loop
                        Animate_Square( squares(i) );
                    end loop;
                   theta := theta + 0.1;
                   redraw := True;
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                    exit;
                elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
                      event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
                then
                    exit;
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_RESIZE then
                    Al_Acknowledge_Resize( event.display.source );
                end if;
            end loop;

            Al_Destroy_Timer( timer );
            Al_Destroy_Event_Queue( queue );
            Al_Destroy_Display( display );

            return Null_Address;
        end thread_func;

        ------------------------------------------------------------------------

        num_threads : Integer;
        addr        : Address;
    begin
        Reset( gen );

        if Argument_Count = 1 then
            num_threads := Integer'Min( Integer'Value( Argument( 1 ) ), MAX_THREADS );
        else
            num_threads := 3;
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse ." );
        end if;

        for i in 0..num_threads - 1 loop
            thread(i) := Al_Create_Thread( thread_func'Unrestricted_Access,
                                           background(i mod MAX_BACKGROUNDS)'Address );
        end loop;
        for i in 0..num_threads - 1 loop
            if thread(i) /= null then
                Al_Start_Thread( thread(i) );
            else
                Abort_Example( "Error creating thread" & i'Img );
            end if;
        end loop;
        for i in 0..num_threads - 1 loop
            Al_Join_Thread( thread(i), addr );
            Al_Destroy_Thread( thread(i) );
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Threads;
