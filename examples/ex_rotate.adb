
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
--    Example program for the Allegro library, by Peter Wang.
--
package body Ex_Rotate is

    procedure Ada_Main is
        display_w    : constant := 640;
        display_h    : constant := 480;

        dpy          : A_Allegro_Display;
        buf,
        bmp,
        mem_bmp,
        src_bmp      : A_Allegro_Bitmap;
        queue        : A_Allegro_Event_Queue;
        event        : aliased Allegro_Event;
        theta        : Float := 0.0;
        k            : Float := 1.0;
        mode         : Integer := 0;
        wide_mode,
        mem_src_mode,
        trans_mode   : Boolean := False;
        flags        : Unsigned_32 := 0;
        clip_mode    : Boolean := False;
        trans        : Allegro_Color;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        Init_Platform_Specific;

        Open_Log;
        Log_Print("Press 'w' to toggle wide mode.");
        Log_Print("Press 's' to toggle memory source bitmap.");
        Log_Print("Press space to toggle drawing to backbuffer or off-screen bitmap.");
        Log_Print("Press 't' to toggle translucency.");
        Log_Print("Press 'h' to toggle horizontal flipping.");
        Log_Print("Press 'v' to toggle vertical flipping.");
        Log_Print("Press 'c' to toggle clipping.");
        Log_Print("");

        dpy := Al_Create_Display( display_w, display_h );
        if dpy = null then
            Abort_Example( "Unable to set any graphic mode" );
        end if;

        buf := Al_Create_Bitmap( display_w, display_h );
        if buf = null then
            Abort_Example( "Unable to create buffer" );
        end if;

        bmp := Al_Load_Bitmap( "data/mysha.pcx" );
        if bmp = null then
            Abort_Example( "Unable to load image" );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        mem_bmp := Al_Load_Bitmap( "data/mysha.pcx" );
        if mem_bmp = null then
            Abort_Example( "Unable to load image" );
        end if;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, al_get_keyboard_event_source );

        loop
            if Al_Get_Next_Event( queue, event'Access ) then
                if event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exit;
                    elsif event.keyboard.unichar = Character'Pos( ' ' ) then
                        if mode = 0 then
                            mode := 1;
                        else
                            mode := 0;
                        end if;
                        if mode = 0 then
                            Log_Print( "Drawing to off-screen buffer" );
                        else
                            Log_Print( "Drawing to display backbuffer" );
                        end if;
                    elsif event.keyboard.unichar = Character'Pos( 'w' ) then
                        wide_mode := not wide_mode;
                    elsif event.keyboard.unichar = Character'Pos( 's' ) then
                        mem_src_mode := not mem_src_mode;
                        if mem_src_mode then
                            Log_Print( "Source is memory bitmap" );
                        else
                            Log_Print( "Source is display bitmap" );
                        end if;
                    elsif event.keyboard.unichar = Character'Pos( 't' ) then
                        trans_mode := not trans_mode;
                    elsif event.keyboard.unichar = Character'Pos( 'h' ) then
                        flags := flags xor ALLEGRO_FLIP_HORIZONTAL;
                    elsif event.keyboard.unichar = Character'Pos( 'v' ) then
                        flags := flags xor ALLEGRO_FLIP_VERTICAL;
                    elsif event.keyboard.unichar = Character'Pos( 'c' ) then
                        clip_mode := not clip_mode;
                    end if;
                end if;
            end if;

            --
            -- mode 0 = draw scaled to off-screen buffer before
            --          blitting to display backbuffer
            -- mode 1 = draw scaled to display backbuffer
            --

            if mode = 0 then
                Al_Set_Target_Bitmap( buf );
            else
                Al_Set_Target_Backbuffer( dpy );
            end if;

            if mem_src_mode then
                src_bmp := mem_bmp;
            else
                src_bmp := bmp;
            end if;
            if wide_mode then
                k := 2.0;
            else
                k := 1.0;
            end if;

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            trans := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
            if mode = 0 then
                Al_Clear_To_Color( Al_Map_RGBA_f( 1.0, 0.0, 0.0, 1.0 ) );
            else
                Al_Clear_To_Color( Al_Map_RGBA_f( 0.0, 0.0, 1.0, 1.0 ) );
            end if;

            if trans_mode then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
                trans := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 0.5 );
            end if;

            if clip_mode then
                Al_Set_Clipping_Rectangle( 50, 50, display_w - 100, display_h - 100 );
            else
                Al_Set_Clipping_Rectangle( 0, 0, display_w, display_h );
            end if;

            Al_Draw_Tinted_Scaled_Rotated_Bitmap( src_bmp, trans,
                                                  50.0, 50.0,
                                                  Float(display_w / 2),
                                                  Float(display_h / 2),
                                                  k, k, theta, flags );

            if mode = 0 then
                Al_Set_Target_Backbuffer( dpy );
                Al_Set_Clipping_Rectangle( 0, 0, display_w, display_h );
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                Al_Draw_Bitmap( buf, 0.0, 0.0, 0 );
            end if;

            Al_Flip_Display;
            Al_Rest( 0.01 );
            theta := theta - 0.01;
        end loop;

        Al_Destroy_Bitmap( bmp );
        Al_Destroy_Bitmap( mem_bmp );
        Al_Destroy_Bitmap( buf );

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Rotate;
