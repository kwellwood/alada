
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Fonts.TTF;                 use Allegro.Fonts.TTF;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Projection is

    -- How far has the text been scrolled.
    scroll_y : Float;

    -- Total length of the scrolling text in pixels.
    text_length : Float;

    -- The Alex logo.
    logo : A_Allegro_Bitmap;

    -- The star particle.
    particle : A_Allegro_Bitmap;

    -- The font we use for everything.
    font : A_Allegro_Font;

    ----------------------------------------------------------------------------

    -- Local pseudo-random number generator.
    procedure Rnd( seed : in out Unsigned_32; result : out Integer ) is
    begin
        seed := (seed + 1) * 1103515245 + 12345;
        result := Integer(Shift_Right( seed, 16 ) and 16#0000_ffff#);
    end Rnd;

    ----------------------------------------------------------------------------

    -- Print fading text.
    function Print( font : A_Allegro_Font; x, y, r, g, b, fade : Float; text : String ) return Float is
        c : Float := 1.0 + (y - fade) / 360.0 / 2.0;
    begin
        if c > 1.0 then
            c := 1.0;
        elsif c < 0.0 then
            c := 0.0;
        end if;
        Al_Draw_Text( font, Al_Map_RGBA_f( c * r, c * g, c * b, c ), x, y, ALLEGRO_ALIGN_CENTER, text );
        return y + Float(Al_Get_Font_Line_Height( font ));
    end Print;

    ----------------------------------------------------------------------------

    -- Set up a perspective transform. We make the screen span
    -- 180 vertical units with square pixel aspect and 90� vertical
    -- FoV.
    --
    procedure Setup_3d_Projection( projection : in out Allegro_Transform ) is
        display : constant A_Allegro_Display := Al_Get_Current_Display;
        dw      : constant Float := Float(Al_Get_Display_Width( display ));
        dh      : constant Float := Float(Al_Get_Display_Height( display ));
    begin
        Al_Perspective_Transform( projection, -180.0 * dw / dh, -180.0, 180.0, 180.0 * dw / dh, 180.0, 3000.0 );
        Al_Use_Projection_Transform( projection );
    end Setup_3d_Projection;

    ----------------------------------------------------------------------------

    -- 3D transformations make it very easy to draw a starfield.
    procedure Draw_Stars is
        projection : Allegro_Transform;
        x, y, z    : Integer;
        seed       : Unsigned_32 := 0;
    begin
        Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );

        for i in 0..99 loop
            Rnd( seed, x );
            Rnd( seed, y );
            Rnd( seed, z );

            Al_Identity_Transform( projection );
            Al_Translate_Transform_3d( projection, 0.0, 0.0,
                                       Float(-2000 + (Integer(scroll_y) * 1000 / Integer(text_length) + z) mod 2000 - 180) );
            Setup_3d_Projection( projection );
            Al_Draw_Bitmap( particle, Float(x mod 4000 - 2000), Float(y mod 2000 - 1000), 0 );
        end loop;
    end Draw_Stars;

    ----------------------------------------------------------------------------

    -- The main part of this example.
    procedure Draw_Scrolling_Text is
        projection : Allegro_Transform;
        bw         : constant Float := Float(Al_Get_Bitmap_Width( logo ));
        bh         : constant Float := Float(Al_Get_Bitmap_Height( logo ));
        x, y, c    : Float;
        pragma Warnings( Off, y );
    begin
        Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );

        Al_Identity_Transform( projection );

        -- First, we scroll the text in in the y direction (inside the x/z
        -- plane) and move it away from the camera (in the z direction).
        -- We move it as far as half the display height to get a vertical
        -- FOV of 90 degrees.
        --
        Al_Translate_Transform_3d( projection, 0.0, -scroll_y, -180.0);

        -- Then we tilt it backwards 30 degrees.
        Al_Rotate_Transform_3d( projection, 1.0, 0.0, 0.0, 30.0 * ALLEGRO_PI / 180.0 );

        -- And finally move it down so the 0 position ends up
        -- at the bottom of the screen.
        --
        Al_Translate_Transform_3d( projection, 0.0, 180.0, 0.0 );

        Setup_3d_Projection( projection );

        x := 0.0;
        y := 0.0;
        c := 1.0 + (y - scroll_y) / 360.0 / 2.0;
        if c < 0.0 then
            c := 0.0;
        end if;
        Al_Draw_Tinted_Bitmap( logo, Al_Map_RGBA_f( c, c, c, c ), x - bw / 2.0, y, 0 );
        y := y + bh;

        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "Allegro 5" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "It is a period of game programming." );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "Game coders have won their first" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "victory against the evil" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "General Protection Fault." );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "During the battle, hackers managed" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "to steal the secret source to the" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "General's ultimate weapon," );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "the ACCESS VIOLATION, a kernel" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "exception with enough power to" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "destroy an entire program." );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "Pursued by sinister bugs, the" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "Allegro developers race home" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "aboard their library to save" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "all game programmers and restore" );
        y := Print( font, x, y, 1.0, 0.9, 0.3, scroll_y, "freedom to the open source world." );
    end Draw_Scrolling_Text;

    ----------------------------------------------------------------------------

    procedure Draw_Intro_Text is
        projection : Allegro_Transform;
        fade       : Float;
        fh         : constant Float := Float(Al_Get_Font_Line_Height( font ));
        ignore     : Float;
        pragma Warnings( Off, ignore );
    begin
        if scroll_y < 50.0 then
            fade := (50.0 - scroll_y) * 12.0;
        else
            fade := (scroll_y - 50.0) * 4.0;
        end if;
   
        Al_Identity_Transform( projection );
        Al_Translate_Transform_3d( projection, 0.0, -scroll_y / 3.0, -181.0 );
        Setup_3d_Projection( projection );

        ignore := Print( font, 0.0, 0.0, 0.0, 0.9, 1.0, fade, "A long time ago, in a galaxy" );
        ignore := Print( font, 0.0, 0.0 + fh, 0.0, 0.9, 1.0, fade, "not too far away..." );
    end Draw_Intro_Text;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
        queue   : A_Allegro_Event_Queue;
        redraw  : Boolean := False;
        event   : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Init_TTF_Addon then
            Abort_Example( "Could not init TTF addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );
        display := Al_Create_Display( 640, 360 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR or ALLEGRO_MIPMAP );

        font := Al_Load_Font( "data/DejaVuSans.ttf", 40, 0 );
        if font = null then
            Abort_Example( "Failed to load data/DejaVuSans.ttf" );
        end if;
        logo := Al_Load_Bitmap( "data/alexlogo.png" );
        if logo = null then
            Abort_Example( "Failed to load data/alexlogo.ttf" );
        end if;
        particle := Al_Load_Bitmap( "data/haiku/air_effect.png" );
        if particle = null then
            Abort_Example( "Failed to load data/haiku/air_effect.ttf" );
        end if;

        Al_Convert_Mask_To_Alpha( logo, Al_Map_RGB( 255, 0, 255 ) );

        text_length := Float(Al_Get_Bitmap_Height( logo ) + 19 * Al_Get_Font_Line_Height( font ));

        timer := Al_Create_Timer( 1.0 / 60.0 );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );
        loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_RESIZE then
                Al_Acknowledge_Resize( display );
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                scroll_y := scroll_y + 1.0;
                if scroll_y > text_length * 2.0 then
                    scroll_y := scroll_y - text_length * 2.0;
                end if;
                redraw := True;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Al_Clear_To_Color( Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 ) );

                Draw_Stars;

                Draw_Scrolling_Text;

                Draw_Intro_Text;

                Al_Flip_Display;
                redraw := False;
            end if;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Projection;
