
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Joystick;                  use Allegro.Joystick;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;

package body Ex_Joystick_Events is

    MAX_AXES    : constant := 3;
    MAX_STICKS  : constant := 16;
    MAX_BUTTONS : constant := 32;

    -- globals
    event_queue : A_ALLEGRO_EVENT_QUEUE;
    font        : A_ALLEGRO_FONT;
    black       : Allegro_Color;
    grey        : Allegro_Color;
    white       : Allegro_Color;

    type Int_Array is array (Integer range <>) of Integer;
    type Float_2D is array (Integer range <>, Integer range <>) of Float;
    type Bool_Array is array (Integer range <>) of Boolean;

    num_sticks   : Integer := 0;
    num_buttons  : Integer := 0;
    num_axes     : Int_Array(0..MAX_STICKS-1) := (others => 0);
    joys         : Float_2D(0..MAX_STICKS-1, 0..MAX_AXES-1) := (others => (others => 0.0));
    joys_buttons : Bool_Array(0..MAX_BUTTONS-1) := (others => False);

    ----------------------------------------------------------------------------

    procedure Setup_Joystick_Values( joy : A_Allegro_Joystick ) is
        jst : Allegro_Joystick_State;
    begin
        if joy = null then
            num_sticks := 0;
            num_buttons := 0;
            return;
        end if;

        Al_Get_Joystick_State( joy, jst );

        num_sticks := Al_Get_Joystick_Num_Sticks( joy );
        if num_sticks > MAX_STICKS then
            num_sticks := MAX_STICKS;
        end if;
        for i in 0..num_sticks-1 loop
            num_axes(i) := Al_Get_Joystick_Num_Axes( joy, i );
            for j in 0..num_axes(i)-1 loop
                joys(i, j) := jst.stick(i).axis(j);
            end loop;
        end loop;

        num_buttons := Al_Get_Joystick_Num_Buttons( joy );
        if num_buttons > MAX_BUTTONS then
            num_buttons := MAX_BUTTONS;
        end if;

        for i in 0..num_buttons-1 loop
            joys_buttons(i) := (jst.button(i) >= 16384);
        end loop;
    end Setup_Joystick_Values;

    ----------------------------------------------------------------------------

    procedure Draw_Joystick_Axes( joy : A_Allegro_Joystick; cx, cy : Float; stick : Integer ) is
        size  : constant := 30.0;
        csize : constant := 5.0;
        osize : constant := size + csize;
        zx    : constant Float := cx + osize + csize * 2.0;
        x     : constant Float := cx + joys(stick, 0) * size;
        y     : constant Float := cy + joys(stick, 1) * size;
        z     : constant Float := cy + joys(stick, 2) * size;
    begin
        Al_Draw_Filled_Rectangle( cx - osize, cy - osize, cx + osize, cy + osize, grey );
        Al_Draw_Rectangle( cx - osize + 0.5, cy - osize + 0.5, cx + osize - 0.5, cy + osize - 0.5, black, 0.0 );
        Al_Draw_Filled_Rectangle( x - 5.0, y - 5.0, x + 5.0, y + 5.0, black );

        if num_axes(stick) >= 3 then
            Al_Draw_Filled_Rectangle( zx - csize, cy - osize, zx + csize, cy + osize, grey );
            Al_Draw_Rectangle( zx - csize + 0.5, cy - osize + 0.5, zx + csize - 0.5, cy + osize - 0.5, black, 0.0 );
            Al_Draw_Filled_Rectangle( zx - 5.0, z - 5.0, zx + 5.0, z + 5.0, black );
        end if;

        if joy /= null then
            Al_Draw_Text( font, black, cx, cy + osize + 1.0, ALLEGRO_ALIGN_CENTRE,
                Al_Get_Joystick_Stick_Name( joy, stick ) );
            for i in 0..num_axes(stick)-1 loop
                Al_Draw_Text( font, black, cx, cy + osize + Float(1 + i) * 10.0,
                    ALLEGRO_ALIGN_CENTRE,
                    Al_Get_Joystick_Axis_Name( joy, stick, i ) );
            end loop;
        end if;
    end Draw_Joystick_Axes;

    ----------------------------------------------------------------------------

    procedure Draw_Joystick_Button( joy : A_Allegro_Joystick; button : Integer; down : Boolean ) is
        bmp : constant A_Allegro_Bitmap := Al_Get_Target_Bitmap;
        x   : constant Float := Float(Al_Get_Bitmap_Width( bmp ) / 2 - 120 + (button mod 8) * 30);
        y   : constant Float := Float(Al_Get_Bitmap_Height( bmp ) - 120 + (button / 8) * 30);
        fg  : Allegro_Color;
    begin
        Al_Draw_Filled_Rectangle( x, y, x + 25.0, y + 25.0, grey );
        Al_Draw_Rectangle( x + 0.5, y + 0.5, x + 24.5, y + 24.5, black, 0.0 );
        if down then
            Al_Draw_Filled_Rectangle( x + 2.0, y + 2.0, x + 23.0, y + 23.0, black );
            fg := white;
        else
            fg := black;
        end if;

        if joy /= null then
            declare
                name : constant String := Al_Get_Joystick_Button_Name( joy, button );
            begin
                if name'Length < 4 then
                    Al_Draw_Text( font, fg, x + 13.0, y + 8.0, ALLEGRO_ALIGN_CENTRE, name );
                end if;
            end;
        end if;
    end Draw_Joystick_Button;

    ----------------------------------------------------------------------------

    procedure Draw_All( joy : A_Allegro_Joystick ) is
        bmp    : constant A_Allegro_Bitmap := Al_Get_Target_Bitmap;
        width  : constant Float := Float(Al_Get_Bitmap_Width( bmp ));
        height : constant Float := Float(Al_Get_Bitmap_Height( bmp ));
    begin
        Al_Clear_To_Color( Al_Map_RGB( 16#ff#, 16#ff#, 16#c0# ) );

        if joy /= null then
            Al_Draw_Text( font, black, width / 2.0, 10.0, ALLEGRO_ALIGN_CENTRE,
                           "Joystick: " & Al_Get_Joystick_Name( joy ) );
        end if;

        for i in 0..num_sticks-1 loop
            declare
                u  : constant Integer := i mod 4;
                v  : constant Integer := i / 4;
                cx : constant Float := Float'Floor( (Float(u) + 0.5) * width / 4.0 );
                cy : constant Float := Float'Floor( (Float(v) + 0.5) * height / 6.0 );
            begin
                Draw_Joystick_Axes( joy, cx, cy, i );
            end;
        end loop;

        for i in 0..num_buttons-1 loop
            Draw_Joystick_Button( joy, i, joys_buttons(i) );
        end loop;

        Al_Flip_Display;
    end Draw_All;

    ----------------------------------------------------------------------------

    procedure Main_Loop is
        event : aliased Allegro_Event;
    begin
        loop
            if Al_Is_Event_Queue_Empty( event_queue ) then
                Draw_All( Al_Get_Joystick( 0 ) );
            end if;

            Al_Wait_For_Event( event_queue, event );

            case event.any.typ is
                -- ALLEGRO_EVENT_JOYSTICK_AXIS - a joystick axis value changed.
                --
                when ALLEGRO_EVENT_JOYSTICK_AXIS =>
                    if event.joystick.stick < MAX_STICKS and event.joystick.axis < MAX_AXES then
                       joys(event.joystick.stick, event.joystick.axis) := event.joystick.pos;
                    end if;

                -- ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN - a joystick button was pressed.
                --
                when ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN =>
                    joys_buttons(event.joystick.button) := True;

                -- ALLEGRO_EVENT_JOYSTICK_BUTTON_UP - a joystick button was released.
                --
                when ALLEGRO_EVENT_JOYSTICK_BUTTON_UP =>
                    joys_buttons(event.joystick.button) := False;

                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        return;
                    end if;

                -- ALLEGRO_EVENT_DISPLAY_CLOSE - the window close button was pressed.
                --
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    return;

                when ALLEGRO_EVENT_JOYSTICK_CONFIGURATION =>
                    if not Al_Reconfigure_Joysticks then
                        null;
                    end if;
                    Setup_Joystick_Values( Al_Get_Joystick( 0 ) );

                -- We received an event of some type we don't know about.
                -- Just ignore it.
                --
                when others => null;

            end case;
        end loop;
    end Main_Loop;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display : A_Allegro_Display;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init font addon." );
        end if;

        display := Al_Create_Display( 1024, 768 );
        if display = null then
            Abort_Example( "al_create_display failed" );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "al_install_keyboard failed" );
        end if;

        black := Al_Map_RGB( 0, 0, 0 );
        grey := Al_Map_RGB( 16#e0#, 16#e0#, 16#e0# );
        white := Al_Map_RGB( 255, 255, 255 );
        font := Al_Create_Builtin_Font;

        if not Al_Install_Joystick then
            Abort_Example( "al_install_joystick failed" );
        end if;

        event_queue := Al_Create_Event_Queue;
        if event_queue = null then
            Abort_Example( "al_create_event_queue failed" );
        end if;

        if Al_Get_Keyboard_Event_Source /= null then
            Al_Register_Event_Source( event_queue, Al_Get_Keyboard_Event_Source );
        end if;
        Al_Register_Event_Source(event_queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source(event_queue, Al_Get_Joystick_Event_Source );

        Setup_Joystick_Values( Al_Get_Joystick( 0 ) );

        Main_Loop;

        Al_Destroy_Font( font );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Joystick_Events;
