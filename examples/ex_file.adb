
with Allegro;                           use Allegro;
with Allegro.File_IO;                   use Allegro.File_IO;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with GNAT.OS_Lib;                       use GNAT.OS_Lib;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

package body Ex_File is

    error  : Integer := 0;

    type U8_Array is array (Integer range <>) of Unsigned_8;
    pragma Convention( C, U8_Array );

    procedure Sep( line : Integer ) is
    begin
        Log_Print( "" );
        Log_Print( "# line" & line'Img );
    end Sep;

    ----------------------------------------------------------------------------

    generic
        type Check_Type is (<>);
    procedure Check( val, expected : Check_Type; expr : String );

    procedure Check( val, expected : Check_Type; expr : String ) is
    begin
        if val /= expected then
            Log_Print( "FAIL " & expr );
            error := error + 1;
        else
            Log_Print( "OK" );
        end if;
    end Check;

    procedure Check_Int   is new Check( Integer );
    procedure Check_Sizet is new Check( size_t );
    procedure Check_Int64 is new Check( Integer_64 );
    procedure Check_U8    is new Check( Unsigned_8 );
    procedure Check_Bool  is new Check( Boolean );

    ----------------------------------------------------------------------------

    procedure Read_Test is
        f  : A_Allegro_File;
        bs : U8_Array(0..7);
        sz : Integer_64;
    begin
        f := Al_Fopen( "data/allegro.pcx", "rb" );
        if f = null then
            error := error + 1;
            return;
        end if;

        -- Test: reading bytes advances position.
        Sep( 1 );
        Check_Int64( Al_Ftell( f ),     0, "Al_Ftell( f )" );
        Check_Int  ( Al_Fgetc( f ), 16#A#, "Al_Fgetc( f )" );
        Check_Int  ( Al_Fgetc( f ), 16#5#, "Al_Fgetc( f )" );
        Check_Int64( Al_Ftell( f ),     2, "Al_Ftell( f )" );

        -- Test: ungetc moves position back.
        Sep( 2 );
        Check_Int  ( Al_Fungetc( f, 16#55#), 16#55#, "Al_Fungetc( f, 16#55#)" );
        Check_Int64( Al_Ftell( f ),               1, "Al_Ftell( f )" );

        -- Test: read buffer.
        Sep( 3 );
        Check_Sizet( Al_Fread( f, bs(bs'First)'Address, bs'Length ), bs'Length, "Al_Fread( f, bs(bs'First)'Address, bs'Length )" );
        Check_U8   ( bs(0), 16#55#, "bs(0)" );    -- from pushback
        Check_U8   ( bs(1), 16#01#, "bs(1)" );
        Check_U8   ( bs(2), 16#08#, "bs(2)" );
        Check_U8   ( bs(3), 16#00#, "bs(3)" );
        Check_U8   ( bs(4), 16#00#, "bs(4)" );
        Check_U8   ( bs(5), 16#00#, "bs(5)" );
        Check_U8   ( bs(6), 16#00#, "bs(6)" );
        Check_U8   ( bs(7), 16#3f#, "bs(7)" );
        Check_Int64( Al_Ftell( f ), 9, "Al_Ftell( f )" );

        -- Test: seek absolute.
        Sep( 4 );
        Check_Bool ( Al_Fseek( f, 13, ALLEGRO_SEEK_SET ), True, "Al_Fseek( f, 13, ALLEGRO_SEEK_SET )" );
        Check_Int64( Al_Ftell( f ),                         13, "Al_Ftell( f )" );
        Check_Int  ( Al_Fgetc( f ),                     16#02#, "Al_Fgetc( f )" );

        -- Test: seek nowhere.
        Sep( 5 );
        Check_Bool ( Al_Fseek( f, 0, ALLEGRO_SEEK_CUR ), True, "Al_Fseek( f, 0, ALLEGRO_SEEK_CUR )" );
        Check_Int64( Al_Ftell( f ),                        14, "Al_Ftell( f )" );
        Check_Int  ( Al_Fgetc( f ),                    16#e0#, "Al_Fgetc( f )" );

        -- Test: seek nowhere with pushback.
        Sep( 6 );
        Check_Int  ( Al_Fungetc( f, 16#55#),           16#55#, "Al_Fungetc( f, 16#55#)" );
        Check_Bool ( Al_Fseek( f, 0, ALLEGRO_SEEK_CUR ), True, "Al_Fseek( f, 0, ALLEGRO_SEEK_CUR )" );
        Check_Int64( Al_Ftell( f ),                        14, "Al_Ftell( f )" );
        Check_Int  ( Al_Fgetc( f ),                    16#e0#, "Al_Fgetc( f )" );

        -- Test: seek relative backwards.
        Sep( 7 );
        Check_Bool ( Al_Fseek( f, -3, ALLEGRO_SEEK_CUR ), True, "Al_Fseek( f, -3, ALLEGRO_SEEK_CUR )" );
        Check_Int64( Al_Ftell( f ),                         12, "Al_Ftell( f )" );
        Check_Int  ( Al_Fgetc( f ),                     16#80#, "Al_Fgetc( f )" );

        -- Test: seek backwards with pushback.
        Sep( 8 );
        Check_Int64( Al_Ftell( f ),                         13, "Al_Ftell( f )" );
        Check_Int  ( Al_Fungetc( f, 16#66#),            16#66#, "Al_Fungetc( f, 16#66#)" );
        Check_Int64( Al_Ftell( f ),                         12, "Al_Ftell( f )" );
        Check_Bool ( Al_Fseek( f, -2, ALLEGRO_SEEK_CUR ), True, "Al_Fseek( f, -2, ALLEGRO_SEEK_CUR )" );
        Check_Int64( Al_Ftell( f ),                         10, "Al_Ftell( f )" );
        Check_Int  ( Al_Fgetc( f ),                     16#c7#, "Al_Fgetc( f )" );

        -- Test: seek relative to end.
        Sep( 9 );
        Check_Bool ( Al_Fseek( f, 0, ALLEGRO_SEEK_END ),  True, "Al_Fseek( f, 0, ALLEGRO_SEEK_END )" );
        Check_Bool ( Al_Feof( f ),                       False, "Al_Feof( f )" );
        Check_Int64( Al_Ftell( f ),                   16#ab06#, "Al_Ftell( f )" );

        -- Test: read past EOF.
        Sep( 10 );
        Check_Int ( Al_Fgetc( f ),    -1, "Al_Fgetc( f )" );
        Check_Bool( Al_Feof( f ),   True, "Al_Feof( f )" );
        Check_Int ( Al_Ferror( f ),    0, "Al_Ferror( f )" );

        -- Test: seek clears EOF indicator.
        Sep( 11 );
        Check_Bool ( Al_Fseek( f, 0, ALLEGRO_SEEK_END ),  True, "Al_Fseek( f, 0, ALLEGRO_SEEK_END )" );
        Check_Bool ( Al_Feof( f ),                       False, "Al_Feof( f )" );
        Check_Int64( Al_Ftell( f ),                   16#ab06#, "Al_Ftell( f )" );

        -- Test: seek backwards from end.
        Sep( 12 );
        Check_Bool ( Al_Fseek( f, -20, ALLEGRO_SEEK_END ),  True, "Al_Fseek( f, -20, ALLEGRO_SEEK_END )" );
        Check_Int64( Al_Ftell( f ),                     16#aaf2#, "Al_Ftell( f )" );

        -- Test: seek forwards from end.
        Sep( 13 );
        Check_Bool ( Al_Fseek( f, 20, ALLEGRO_SEEK_END ),   True, "Al_Fseek( f, 20, ALLEGRO_SEEK_END )" );
        Check_Int64( Al_Ftell( f ),                     16#ab1a#, "Al_Ftell( f )" );
        Check_Int  ( Al_Fgetc( f ),                           -1, "Al_Fgetc( f )" );
        Check_Bool ( Al_Feof( f ),                          True, "Al_Feof( f )" );

        -- Test: get file size if possible.
        Sep( 14 );
        sz := Al_Fsize( f );
        if sz /= -1 then
            Check_Int64( sz, 16#ab06#, "sz = Al_Fsize( f )" );
        end if;

        -- Test: close.
        Sep( 15 );
        Check_Bool( Al_Fclose( f ), True, "Al_Fclose( f )" );
    end Read_Test;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
    begin
        if not Al_Initialize then
            OS_Exit( 1 );
        end if;
        Init_Platform_Specific;
        Open_Log_Monospace;

        Read_Test;

        Close_Log( True );

        if error /= 0 then
            OS_Exit( 1 );
        end if;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_File;

