
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;

--
--    Example program for the Allegro library.
--
--    This program tests if the ALLEGRO_MOUSE_STATE `display' field
--    is set correctly.
--
package body Ex_Mouse_Focus is

    procedure Ada_Main is

        display1, display2 : A_Allegro_Display;

        ------------------------------------------------------------------------

        procedure Redraw( color1, color2 : Allegro_Color ) is
        begin
            Al_Set_Target_Backbuffer( display1 );
            Al_Clear_To_Color( color1 );
            Al_Flip_Display;

            Al_Set_Target_Backbuffer( display2 );
            Al_Clear_To_Color( color2 );
            Al_Flip_Display;
        end Redraw;

        ------------------------------------------------------------------------

        black, red : Allegro_Color;
        mst0, mst  : Allegro_Mouse_State;
        kst        : Allegro_Keyboard_State;
    begin
        if not Al_Initialize then
            Abort_Example( "Couldn't initialise Allegro." );
        end if;
        if not Al_Install_Mouse then
          Abort_Example( "Couldn't install mouse." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Couldn't install keyboard." );
        end if;

       display1 := Al_Create_Display( 300, 300 );
       display2 := Al_Create_Display( 300, 300 );
        if display1 = null or else display2 = null then
            Al_Destroy_Display( display1 );
            Al_Destroy_Display( display2 );
            Abort_Example( "Couldn't open displays." );
        end if;

        Open_Log;
        Log_Print( "Move the mouse cursor over the displays" );

        black := Al_Map_RGB( 0, 0, 0 );
        red := Al_Map_RGB( 255, 0, 0 );

        loop
            Al_Get_Mouse_State( mst );
            if mst.display /= mst0.display or else mst.x /= mst0.x or else mst.y /= mst0.y then
                if mst.display = null then
                    Log_Print( "Outside either display" );
                elsif mst.display = display1 then
                    Log_Print( "In display 1, x =" & mst.x'Img & ", y =" & mst.y'Img );
                elsif mst.display = display2 then
                    Log_Print( "In display 2, x =" & mst.x'Img & ", y =" & mst.y'Img );
                else
                    Log_Print( "Unknown display = ?, x =" & mst.x'Img & ", y =" & mst.y'Img );
                end if;
                mst0 := mst;
            end if;

            if mst.display = display1 then
                Redraw( red, black );
            elsif mst.display = display2 then
                Redraw( black, red );
            else
                Redraw( black, black );
            end if;

            Al_Rest( 0.1 );

            Al_Get_Keyboard_State( kst );
            if Al_Key_Down( kst, ALLEGRO_KEY_ESCAPE ) then
                exit;
            end if;
        end loop;

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Mouse_Focus;
