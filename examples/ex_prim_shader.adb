
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Shaders;                   use Allegro.Shaders;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;

package body Ex_Prim_Shader is

    type Custom_Vertex is
        record
            x, y       : Float;
            nx, ny, nz : Float;
        end record;

    type Custom_Vertex_Array is array (Integer range <>) of Custom_Vertex;

    RING_SIZE          : constant := 25.0;
    SPHERE_RADIUS      : constant := 150.1;
    SCREEN_WIDTH       : constant := 640;
    SCREEN_HEIGHT      : constant := 480;
    NUM_RINGS          : constant := (SCREEN_WIDTH / Integer(RING_SIZE) + 1);
    NUM_SEGMENTS       : constant := 64;
    NUM_VERTICES       : constant := (NUM_RINGS * NUM_SEGMENTS * 6);
    FIRST_OUTSIDE_RING : constant := Integer(SPHERE_RADIUS / RING_SIZE);

    ----------------------------------------------------------------------------

    procedure Setup_Vertex( vtx : in out Custom_Vertex; ring, segment : Integer; inside : Boolean ) is
        x   : constant Float := Float(ring) * RING_SIZE * Cos( 2.0 * ALLEGRO_PI * Float(segment) / Float(NUM_SEGMENTS) );
        y   : constant Float := Float(ring) * RING_SIZE * Sin( 2.0 * ALLEGRO_PI * Float(segment) / Float(NUM_SEGMENTS) );
        z   : Float;
        len : Float;
    begin
        vtx.x := x + Float(SCREEN_WIDTH / 2);
        vtx.y := y + Float(SCREEN_HEIGHT / 2);
        if inside then
            -- This comes from the definition of the normal vector as the
            -- gradient of the 3D surface.
            z := Sqrt( SPHERE_RADIUS * SPHERE_RADIUS - x * x - y * y );
            vtx.nx := x / z;
            vtx.ny := y / z;
        else
            vtx.nx := 0.0;
            vtx.ny := 0.0;
        end if;
        vtx.nz := 1.0;

        len := Sqrt( vtx.nx * vtx.nx + vtx.ny * vtx.ny + vtx.nz * vtx.nz );
        vtx.nx := vtx.nx / len;
        vtx.ny := vtx.ny / len;
        vtx.nz := vtx.nz / len;
    end Setup_Vertex;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display            : A_Allegro_Display;
        timer              : A_Allegro_Timer;
        queue              : A_Allegro_Event_Queue;
        redraw             : Boolean := True;
        shader             : A_Allegro_Shader;
        vertex_decl        : A_Allegro_Vertex_Decl;
        temp               : Custom_Vertex;
        pragma Warnings( Off, temp );
        vertex_elems       : constant Allegro_Vertex_Element_Array := (
            (ALLEGRO_PRIM_POSITION,  ALLEGRO_PRIM_FLOAT_2, Integer(temp.x'Position)),
            (ALLEGRO_PRIM_USER_ATTR, ALLEGRO_PRIM_FLOAT_3, Integer(temp.nx'Position)),
            LAST_ALLEGRO_VERTEX_ELEMENT
        );
        vertices           : Custom_Vertex_Array(0..NUM_VERTICES-1);
        quit               : Boolean := False;
        vertex_shader_file : Unbounded_String;
        pixel_shader_file  : Unbounded_String;
        vertex_idx         : Integer := 0;
        diffuse_color      : constant Float_Array(0..3) := (0.1, 0.1, 0.7, 1.0);
        light_position     : Float_Array(0..2) := (0.0, 0.0, 100.0);
        event              : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Mouse then
            Abort_Example( "Error installing mouse." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_PROGRAMMABLE_PIPELINE );
        display := Al_Create_Display( SCREEN_WIDTH, SCREEN_HEIGHT );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        vertex_decl := Al_Create_Vertex_Decl( vertex_elems, Custom_Vertex'Size / 8 );
        if vertex_decl = null then
            Abort_Example( "Error creating vertex declaration." );
        end if;

        -- Computes a "spherical" bump ring. The z coordinate is not actually set
        -- appropriately as this is a 2D example, but the normal vectors are computed
        -- correctly for the light shading effect.
        for ring in 0..NUM_RINGS-1 loop
            for segment in 0..NUM_SEGMENTS-1 loop
                Setup_Vertex( vertices(vertex_idx + 0), ring + 0, segment + 0, ring < FIRST_OUTSIDE_RING );
                Setup_Vertex( vertices(vertex_idx + 1), ring + 0, segment + 1, ring < FIRST_OUTSIDE_RING );
                Setup_Vertex( vertices(vertex_idx + 2), ring + 1, segment + 0, ring < FIRST_OUTSIDE_RING );
                Setup_Vertex( vertices(vertex_idx + 3), ring + 1, segment + 0, ring < FIRST_OUTSIDE_RING );
                Setup_Vertex( vertices(vertex_idx + 4), ring + 0, segment + 1, ring < FIRST_OUTSIDE_RING );
                Setup_Vertex( vertices(vertex_idx + 5), ring + 1, segment + 1, ring < FIRST_OUTSIDE_RING );
                vertex_idx := vertex_idx + 6;
            end loop;
        end loop;

        shader := Al_Create_Shader( ALLEGRO_SHADER_AUTO );
        if shader = null then
            Abort_Example( "Failed to create shader." );
        end if;

        if Al_Get_Shader_Platform(shader) = ALLEGRO_SHADER_GLSL then
            vertex_shader_file := To_Unbounded_String( "data/ex_prim_shader_vertex.glsl" );
            pixel_shader_file := To_Unbounded_String( "data/ex_prim_shader_pixel.glsl" );
        else
            vertex_shader_file := To_Unbounded_String( "data/ex_prim_shader_vertex.hlsl" );
            pixel_shader_file := To_Unbounded_String( "data/ex_prim_shader_pixel.hlsl" );
        end if;

        if not Al_Attach_Shader_Source_File( shader, ALLEGRO_VERTEX_SHADER, To_String( vertex_shader_file ) ) then
            Abort_Example( "al_attach_shader_source_file for vertex shader failed: " & Al_Get_Shader_Log( shader ) );
        end if;
        if not Al_Attach_Shader_Source_File( shader, ALLEGRO_PIXEL_SHADER, To_String( pixel_shader_file ) ) then
            Abort_Example( "al_attach_shader_source_file for pixel shader failed: " & Al_Get_Shader_Log( shader ) );
        end if;

        if not Al_Build_Shader( shader ) then
            Abort_Example( "al_build_shader for link failed: " & Al_Get_Shader_Log( shader ) );
        end if;

        Al_Use_Shader( shader );
        Al_Set_Shader_Float_Vector( "diffuse_color", 4, diffuse_color );
        -- alpha controls shininess, and 25 is very shiny
        Al_Set_Shader_Float( "alpha", 25.0 );

        timer := Al_Create_Timer( 1.0 / 60.0 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer );

        while not quit loop
            Al_Wait_For_Event( queue, event );

            case event.any.typ is
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    quit := True;
                when ALLEGRO_EVENT_MOUSE_AXES =>
                    light_position(0) := Float(event.mouse.x);
                    light_position(1) := Float(event.mouse.y);
                when ALLEGRO_EVENT_KEY_CHAR =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        quit := True;
                    end if;
                when ALLEGRO_EVENT_TIMER =>
                    redraw := True;
                when others => null;
            end case;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );

                Al_Set_Shader_Float_Vector( "light_position", 3, light_position );
                Al_Draw_Prim( vertices(vertices'First)'Address, vertex_decl, null, 0, NUM_VERTICES, ALLEGRO_PRIM_TRIANGLE_LIST );

                Al_Flip_Display;
                redraw := False;
            end if;
        end loop;

        Al_Use_Shader( null );
        Al_Destroy_Shader( shader );
        Al_Destroy_Vertex_Decl( vertex_decl );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Prim_Shader;
