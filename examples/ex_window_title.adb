
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;

-- An example showing how to set the title of a window, by Beoran.
package body Ex_Window_Title is

    INTERVAL : constant := 1.0;

    NEW_WINDOW_TITLE : constant String := "A Custom Window Title. Press space to start changing it.";

    procedure Ada_Main is
        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
        queue   : A_Allegro_Event_Queue;
        font    : A_Allegro_Font;
        step    : Integer := 0;
        done    : Boolean := False;
        text    : Unbounded_String;
        redraw  : Boolean := True;
        event   : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Failed to init IIO addon" );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        text := To_Unbounded_String( NEW_WINDOW_TITLE );

        Al_Set_New_Window_Title( NEW_WINDOW_TITLE );

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        if font = null then
            Abort_Example( "Error loading data/fixed_font.tga" );
        end if;

        text := To_Unbounded_String( Al_Get_New_Window_Title );

        timer := Al_Create_Timer( INTERVAL );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        while not done loop
            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
                Al_Draw_Text( font, Al_Map_RGBA_f( 1.0, 1.0, 1.0, 0.5 ), 0.0, 0.0, 0, To_String( text ) );
                Al_Flip_Display;
                redraw := False;
            end if;

            al_wait_for_event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        Al_Start_Timer( timer );
                    end if;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    done := True;

                when ALLEGRO_EVENT_TIMER =>
                    redraw := True;
                    step := step + 1;
                    text := To_Unbounded_String( "Title:" & step'Img );
                    Al_Set_Window_Title( display, To_String( text ) );

                when others => null;
            end case;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Window_Title;
