
with Ada.Unchecked_Deallocation;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Displays.Monitors;         use Allegro.Displays.Monitors;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Interfaces;                        use Interfaces;

package body Ex_Windows is

    procedure Ada_Main is

        type Allegro_Display_Array is array (Integer range <>) of A_Allegro_Display;

        type Allegro_Monitor_Info_Array is array (Integer range <>) of Allegro_Monitor_Info;
        type A_Allegro_Monitor_Info_Array is access all Allegro_Monitor_Info_Array;

        procedure Free is new Ada.Unchecked_Deallocation( Allegro_Monitor_Info_Array,
                                                          A_Allegro_Monitor_Info_Array );

        W             : constant := 100;
        H             : constant := 100;
        displays      : Allegro_Display_Array(0..1);
        info          : A_Allegro_Monitor_Info_Array;
        adapter_count : Integer;
        x, y          : Integer;
        myfont        : A_Allegro_Font;
        events        : A_Allegro_Event_Queue;
        event         : aliased Allegro_Event;
        gen           : Generator;
    begin
        Reset( gen );

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image IO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        adapter_count := Al_Get_Num_Video_Adapters;
        if adapter_count = 0 then
            Abort_Example( "No adapters found!" );
        end if;

        info := new Allegro_Monitor_Info_Array(0..adapter_count-1);

        for i in info'Range loop
            Al_Get_Monitor_Info( i, info(i) );
        end loop;

        x := ((info(0).x2 - info(0).x1) / 3) - (W / 2);
        y := ((info(0).y2 - info(0).y1) / 2) - (H / 2);

        Al_Set_New_Window_Position( x, y );

        displays(0) := Al_Create_Display( W, H );

        x := x * 2;
        Al_Set_New_Window_Position( x, y );

        displays(1) := Al_Create_Display( W, H );

        if displays(0) = null or else displays(1) = null then
            Abort_Example( "Could not create displays." );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        myfont := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        if myfont = null then
            Abort_Example( "Could not load font." );
        end if;

        events := Al_Create_Event_Queue;
        Al_Register_Event_Source( events, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( events, Al_Get_Display_Event_Source( displays(0) ) );
        Al_Register_Event_Source( events, Al_Get_Display_Event_Source( displays(1) ) );

        loop
            for i in displays'Range loop
                Al_Set_Target_Backbuffer( displays(i) );
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                if i = displays'First then
                    Al_Clear_To_Color( Al_Map_RGB( 255, 0, 255 ) );
                else
                    Al_Clear_To_Color( Al_Map_RGB( 155, 255, 0 ) );
                end if;
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                Al_Draw_Text( myfont, Al_Map_RGB( 0, 0, 0 ), Float(W / 2), Float(H / 2), ALLEGRO_ALIGN_CENTRE, "Click me..." );
                Al_Flip_Display;
            end loop;

            if Al_Wait_For_Event_Timed( events, event'Access, 1.0 ) then
                if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                    exit;
                elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                    declare
                        a      : constant Integer := Integer(Unsigned_32'(Random( gen )) mod Unsigned_32(adapter_count));
                        wLocal : constant Integer := info(a).x2 - info(a).x1;
                        hLocal : constant Integer := info(a).y2 - info(a).y1;
                        margin : constant Integer := 20;
                    begin
                        x := margin + info(a).x1 + Integer(Unsigned_32'(Random( gen )) mod Unsigned_32(wLocal - W - margin));
                        y := margin + info(a).y1 + Integer(Unsigned_32'(Random( gen )) mod Unsigned_32(hLocal - H - margin));
                        Al_Set_Window_Position( event.mouse.display, x, y );
                    end;
                end if;
            end if;
        end loop;

        Al_Destroy_Event_Queue( events );

        Al_Destroy_Display( displays(0) );
        Al_Destroy_Display( displays(1) );

        Free( info );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Windows;
