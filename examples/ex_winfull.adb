
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Displays.Monitors;         use Allegro.Displays.Monitors;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Interfaces;                        use Interfaces;

package body Ex_Winfull is

    procedure Ada_Main is
        win, full : A_Allegro_Display;
        events    : A_Allegro_Event_Queue;
        event     : aliased Allegro_Event;
        done      : Boolean := False;
        gen       : Generator;
    begin
        Reset( gen );
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        if Al_Get_Num_Video_Adapters < 2 then
            Abort_Example( "This example requires multiple video adapters." );
        end if;

        Al_Set_New_Display_Adapter( 1 );
        Al_Set_New_Display_Flags( ALLEGRO_WINDOWED );
        win := Al_Create_Display( 640, 480 );
        if win = null then
            Abort_Example( "Error creating windowed display on adapter 1 " &
    	               "(do you have multiple adapters?)" );
        end if;

        Al_Set_New_Display_Adapter( 0 );
        Al_Set_New_Display_Flags( ALLEGRO_FULLSCREEN );
        full := Al_Create_Display( 640, 480 );
        if full = null then
            Abort_Example( "Error creating fullscreen display on adapter 0" );
        end if;

        events := Al_Create_Event_Queue;
        Al_Register_Event_Source( events, Al_Get_Keyboard_Event_Source );

        while not done loop
            while not Al_Is_Event_Queue_Empty( events ) loop
                if Al_Get_Next_Event( events, event'Access ) then
                    if event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
                       event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
                    then
                        done := True;
                        exit;
                    end if;
                end if;
            end loop;

            Al_Set_Target_Backbuffer( full );
            Al_Clear_To_Color( Al_Map_RGB( Unsigned_8(Unsigned_32'(Random( gen )) mod 256),
                                           Unsigned_8(Unsigned_32'(Random( gen )) mod 256),
                                           Unsigned_8(Unsigned_32'(Random( gen )) mod 256) ) );
            Al_Flip_Display;

            Al_Set_Target_Backbuffer( win );
            Al_Clear_To_Color( Al_Map_RGB( Unsigned_8(Unsigned_32'(Random( gen )) mod 256),
                                           Unsigned_8(Unsigned_32'(Random( gen )) mod 256),
                                           Unsigned_8(Unsigned_32'(Random( gen )) mod 256) ) );
            Al_Flip_Display;

            Al_Rest( 0.5 );
        end loop;

        Al_Destroy_Event_Queue( events );

        Al_Destroy_Display( win );
        Al_Destroy_Display( full );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Winfull;
