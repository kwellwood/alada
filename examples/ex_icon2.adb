--
--      Example program for the Allegro library.
--
--      Set multiple window icons, a big one and a small one.
--      The small would would be used for the task bar,
--      and the big one for the alt-tab popup, for example.
--

with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Icon2 is

    procedure Ada_Main is
        NUM_ICONS : constant := 2;

        display : A_Allegro_Display;
        icons   : Allegro_Bitmap_Array(0..NUM_ICONS-1);
        queue   : A_Allegro_Event_Queue;
        event   : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if Al_Install_Keyboard then
            null;
        end if;
        if Al_Init_Image_Addon then
            null;
        end if;
        Init_Platform_Specific;

        -- First icon 16x16: Read from file.
        icons(0) := Al_Load_Bitmap( "data/cursor.tga" );
        if icons(0) = null then
            Abort_Example( "icons.tga not found" );
        end if;

        display := Al_Create_Display( 320, 200 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
        Al_Flip_Display;

        -- Second icon 32x32: Create it.
        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        icons(1) := Al_Create_Bitmap( 32, 32 );
        Al_Set_Target_Bitmap( icons(1) );
        for v in 0..31 loop
            for u in 0..31 loop
                Al_Put_Pixel( u, v, Al_Map_RGB_f( Float(u) / 31.0, Float(v) / 31.0, 1.0 ) );
            end loop;
        end loop;
        Al_Set_Target_Backbuffer( display );

        Al_Set_Display_Icons( display, icons );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        loop
            Al_Wait_For_Event( queue, event );

            if event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
               event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
            then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            end if;
        end loop;

        Al_Uninstall_System;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Icon2;
