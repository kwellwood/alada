
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.State;                     use Allegro.State;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;

-- Test performance of al_draw_bitmap_region, al_create_sub_bitmap and
-- al_set_clipping_rectangle when clipping a bitmap.
--
package body Ex_Clip is

    procedure Ada_Main is

        type Long_Float_Array is array (Integer range <>) of Long_Float;

        type Example is
            record
                pattern    : A_Allegro_Bitmap;
                font       : A_Allegro_Font;
                queue      : A_Allegro_Event_Queue;
                background,
                text,
                white      : Allegro_Color;
                timer      : Long_Float_Array(0..3) := (others => 0.0);
                counter    : Long_Float_Array(0..3) := (others => 0.0);
                targetFPS  : Integer := 0;
                text_x,
                text_y     : Float := 0.0;
            end record;

        ex : Example;

        ------------------------------------------------------------------------

        function Example_Bitmap( w, h : Integer ) return A_Allegro_Bitmap is
            mx      : constant Float := Float(w) * 0.5;
            my      : constant Float := Float(h) * 0.5;
            state   : aliased Allegro_State;
            lock    : A_Allegro_Locked_Region;
            pragma Warnings( Off, lock );
            pattern : A_Allegro_Bitmap;
            a, d    : Float;
            hue,
            sat,
            L       : Float;
        begin
            pattern := Al_Create_Bitmap( w, h );
            Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP );
            Al_Set_Target_Bitmap( pattern );
            lock := Al_Lock_Bitmap( pattern, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY );

            for i in 0..w-1 loop
                for j in 0..h-1 loop
                    if Float(i) - mx /= 0.0 or else Float(j) - my /= 0.0 then
                        a := Arctan( Y => Float(i) - mx, X => Float(j) - my );
                    end if;
                    d := Sqrt( ((Float(i) - mx) ** 2) + ((Float(j) - my) ** 2) );
                    L := 1.0 - ((1.0 - (1.0 / (1.0 + d * 0.1))) ** 5);
                    hue := a * 180.0 / ALLEGRO_PI;
                    sat := 1.0;
                    if i = 0 or else j = 0 or else i = w - 1 or else j = h - 1 then
                        hue := hue + 180.0;
                    elsif i = 1 or else j = 1 or else i = w - 2 or else j = h - 2 then
                        hue := hue + 180.0;
                        sat := 0.5;
                    end if;
                    Al_Put_Pixel( i, j, Al_Color_HSL( hue, sat, L ) );
                end loop;
            end loop;

            Al_Unlock_Bitmap( pattern );
            Al_Restore_State( state );
            return pattern;
        end Example_Bitmap;

        ------------------------------------------------------------------------

        procedure Set_XY( x, y : Float ) is
        begin
            ex.text_x := x;
            ex.text_y := y;
        end Set_XY;

        ------------------------------------------------------------------------

        procedure Get_XY( x, y : out Float ) is
        begin
            x := ex.text_x;
            y := ex.text_y;
        end Get_XY;

        ------------------------------------------------------------------------

        procedure Print( message : String ) is
        begin
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Text( ex.font, ex.text, ex.text_x, ex.text_y, 0, message );
            ex.text_y := ex.text_y + Float(Al_Get_Font_Line_Height( ex.font ));
        end Print;

        ------------------------------------------------------------------------

        procedure Start_Timer( i : Integer ) is
        begin
            ex.timer(i) := ex.timer(i) - Al_Get_Time;
            ex.counter(i) := ex.counter(i) + 1.0;
        end Start_Timer;

        ------------------------------------------------------------------------

        procedure Stop_Timer( i : Integer ) is
        begin
            ex.timer(i) := ex.timer(i) + Al_Get_Time;
        end Stop_Timer;

        ------------------------------------------------------------------------

        function Get_FPS( i : Integer ) return Integer is
        begin
            if ex.timer(i) = 0.0 then
                return 0;
            end if;
            return Integer(Long_Float'Min( Long_Float(Integer'Last), ex.counter(i) / ex.timer(i) ));
        end Get_FPS;

        ------------------------------------------------------------------------

        procedure Draw is
            iw     : constant Integer := Al_Get_Bitmap_Width( ex.pattern );
            ih     : constant Integer := Al_Get_Bitmap_Height( ex.pattern );
            gap    : constant Integer := 8;
            x, y   : Float;
            temp   : A_Allegro_Bitmap;
            cx, cy,
            cw, ch : Integer;
        begin
            Al_Get_Clipping_Rectangle( cx, cy, cw, ch );

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );

            Al_Clear_To_Color( ex.background );

            -- Test 1.
            Set_XY( 8.0, 8.0 );
            Print( "Al_Draw_Bitmap_Region (" & Integer'Image( Get_FPS( 0 ) ) & " fps )" );
            Get_XY( x, y );
            Al_Draw_Bitmap( ex.pattern, x, y, 0 );

            Start_Timer( 0 );
            Al_Draw_Bitmap_Region( ex.pattern, 1.0, 1.0, Float(iw - 2), Float(ih - 2), x + Float(8 + iw + 1), y + 1.0, 0 );
            Stop_Timer( 0 );
            Set_XY( x, y + Float(ih + gap) );

            -- Test 2.
            Print( "Al_Create_Sub_Bitmap (" & Integer'Image( Get_FPS( 1 ) ) & " fps )" );
            Get_XY( x, y );
            Al_Draw_Bitmap( ex.pattern, x, y, 0 );

            Start_Timer( 1 );
            temp := Al_Create_Sub_Bitmap( ex.pattern, 1, 1, iw - 2, ih - 2 );
            Al_Draw_Bitmap( temp, x + Float(8 + iw + 1), y + 1.0, 0 );
            Al_Destroy_Bitmap( temp );
            Stop_Timer( 1 );
            Set_XY( x, y + Float(ih + gap) );

            -- Test 3.
            Print( "Al_Set_Clipping_Rectangle (" & Integer'Image( Get_FPS( 2 ) ) & " fps )" );
            Get_XY( x, y );
            Al_Draw_Bitmap( ex.pattern, x, y, 0 );

            Start_Timer( 2 );
            Al_Set_Clipping_Rectangle( Integer(x) + 8 + iw + 1, Integer(y) + 1, iw - 2, ih - 2 );
            Al_Draw_Bitmap( ex.pattern, x + Float(8 + iw), y, 0 );
            Al_Set_Clipping_Rectangle( cx, cy, cw, ch );
            Stop_Timer( 2 );
            Set_XY( x, y + Float(ih + gap) );
        end Draw;

        ------------------------------------------------------------------------

        procedure Tick is
        begin
            Draw;
            Al_Flip_Display;
        end Tick;

        ------------------------------------------------------------------------

        procedure Run is
            event     : aliased Allegro_Event;
            need_draw : Boolean := True;
        begin
            loop
                if need_draw and then Al_Is_Event_Queue_Empty( ex.queue ) then
                    Tick;
                    need_draw := False;
                end if;

                Al_Wait_For_Event( ex.queue, event );

                case event.any.typ is
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        return;

                    when ALLEGRO_EVENT_KEY_DOWN =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            return;
                        end if;

                    when ALLEGRO_EVENT_TIMER =>
                        need_draw := True;

                    when others =>
                        null;
                end case;
            end loop;
        end Run;

        ------------------------------------------------------------------------

        procedure Init is
        begin
            ex.targetFPS := 60;
            ex.font := Al_Create_Builtin_Font;
            if ex.font = null then
                Abort_Example( "Error creating builtin font." );
            end if;
            ex.background := Al_Color_Name( "beige" );
            ex.text := Al_Color_Name( "blue" );
            ex.white := Al_Color_Name( "white" );
            ex.pattern := Example_Bitmap( 100, 100 );
        end Init;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / Long_Float(ex.targetFPS) );

        ex.queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( ex.queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( ex.queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );
        Run;

        Al_Destroy_Event_Queue( ex.queue );
        Al_Destroy_Timer( timer );
        Al_Destroy_Bitmap( ex.pattern );
        Al_Destroy_Font( ex.font );
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Clip;
