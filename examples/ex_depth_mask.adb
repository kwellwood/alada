
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Displays.Monitors;         use Allegro.Displays.Monitors;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Fonts.TTF;                 use Allegro.Fonts.TTF;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

package body Ex_Depth_Mask is

    FPS   : constant := 60.0;
    COUNT : constant := 80;

    type Sprite_Type is
        record
            x, y, angle : Float;
        end record;

    type Sprite_Type_Array is array (Integer range <>) of Sprite_Type;

    type Example_Type is
        record
            display              : A_Allegro_Display;
            mysha, obp           : A_Allegro_Bitmap;
            font, font2          : A_Allegro_Font;
            direct_speed_measure : double := 1.0;
            sprites              : Sprite_Type_Array(0..COUNT-1);
        end record;

    example : Example_Type;

    ----------------------------------------------------------------------------

    procedure Redraw is
        t : Allegro_Transform;
    begin
        -- first draw the Obp background and clear the depth buffer to 1.

        Al_Set_Render_State( ALLEGRO_ALPHA_TEST, 1 );
        Al_Set_Render_State( ALLEGRO_ALPHA_FUNCTION, ALLEGRO_RENDER_GREATER );
        Al_Set_Render_State( ALLEGRO_ALPHA_TEST_VALUE, 0 );

        Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 0 );

        Al_Set_Render_State( ALLEGRO_WRITE_MASK, ALLEGRO_MASK_DEPTH or ALLEGRO_MASK_RGBA );

        Al_Clear_Depth_Buffer( 1.0 );
        Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );

        Al_Draw_Scaled_Bitmap( example.obp, 0.0, 0.0, 532.0, 416.0, 0.0, 0.0, 640.0, 416.0 * 640.0 / 532.0, 0 );

        -- Next we draw all sprites but only to the depth buffer (with a depth value
        -- of 0).

        Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 1 );
        Al_Set_Render_State( ALLEGRO_DEPTH_FUNCTION, ALLEGRO_RENDER_ALWAYS );
        Al_Set_Render_State( ALLEGRO_WRITE_MASK, ALLEGRO_MASK_DEPTH );

        for i in example.sprites'Range loop
            Al_Hold_Bitmap_Drawing( True );
            for y in -1..0 loop
                for x in -1..0 loop
                    Al_Identity_Transform( t );
                    Al_Rotate_Transform( t, example.sprites(i).angle );
                    Al_Translate_Transform( t, example.sprites(i).x + (Float(x) * 640.0), example.sprites(i).y + (Float(y) * 480.0) );
                    Al_Use_Transform( t );
                    Al_Draw_Text( example.font, Al_Map_RGB( 0, 0, 0 ), 0.0, 0.0,
                                  ALLEGRO_ALIGN_CENTER, "Allegro 5" );
                end loop;
            end loop;
            Al_Hold_Bitmap_Drawing( False );
        end loop;
        Al_Identity_Transform( t );
        Al_Use_Transform( t );

        -- Finally we draw Mysha, with depth testing so she only appears where
        -- sprites have been drawn before.

        Al_Set_Render_State( ALLEGRO_DEPTH_FUNCTION, ALLEGRO_RENDER_EQUAL );
        Al_Set_Render_State( ALLEGRO_WRITE_MASK, ALLEGRO_MASK_RGBA );
        Al_Draw_Scaled_Bitmap( example.mysha, 0.0, 0.0, 320.0, 200.0, 0.0, 0.0, 320.0 * 480.0 / 200.0, 480.0, 0 );

        -- Finally we draw an FPS counter.
        Al_Set_Render_State(ALLEGRO_DEPTH_TEST, 0 );

        Al_Draw_Text( example.font2, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 640.0, 0.0,
                      ALLEGRO_ALIGN_RIGHT, Integer'Image( Integer(1.0 / example.direct_speed_measure) ) & " FPS" );
    end Redraw;

    ----------------------------------------------------------------------------

    procedure Update is
    begin
        for i in example.sprites'Range loop
            example.sprites(i).x := example.sprites(i).x - 4.0;
            if example.sprites(i).x < 80.0 then
                example.sprites(i).x := example.sprites(i).x + 640.0;
            end if;
            example.sprites(i).angle := example.sprites(i).angle + Float(i) * ALLEGRO_PI / 180.0 / Float(example.sprites'Length);
        end loop;
    end Update;

    ----------------------------------------------------------------------------

    procedure Init is
    begin
        for i in example.sprites'Range loop
            example.sprites(i).x := Float(i mod 4) * 160.0;
            example.sprites(i).y := Float(i / 4) * 24.0;
        end loop;
    end Init;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        info        : aliased Allegro_Monitor_Info;
        w           : Integer := 640;
        h           : Integer := 480;
        done        : Boolean := False;
        need_redraw : Boolean := True;
        background  : Boolean := False;
        ok          : Boolean;
        event       : aliased Allegro_Event;
        t           : double := 0.0;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Failed to init Allegro IIO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        if not Al_Init_TTF_Addon then
            Abort_Example( "Could not init TTF addon." );
        end if;

        Init_Platform_Specific;

        if Al_Get_Num_Video_Adapters > 0 then
            Al_Get_Monitor_Info( 0, info, ok );
        end if;

        Al_Set_New_Display_Option( ALLEGRO_SUPPORTED_ORIENTATIONS,
                                   ALLEGRO_DISPLAY_ORIENTATION_ALL, ALLEGRO_SUGGEST );

        Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, 8, ALLEGRO_SUGGEST );

        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR );

        example.display := Al_Create_Display( w, h );
        if example.display = null then
            Abort_Example("Error creating display.");
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        example.font := Al_Load_Font( "data/DejaVuSans.ttf", 40, 0 );
        if example.font = null then
            Abort_Example( "Error loading data/DejaVuSans.ttf" );
        end if;

        example.font2 := Al_Load_Font( "data/DejaVuSans.ttf", 12, 0 );
        if example.font2 = null then
            Abort_Example( "Error loading data/DejaVuSans.ttf" );
        end if;

        example.mysha := Al_Load_Bitmap( "data/mysha.pcx" );
        if example.mysha = null then
            Abort_Example( "Error loading data/mysha.pcx" );
        end if;

        example.obp := Al_Load_Bitmap( "data/obp.jpg" );
        if example.obp = null then
            Abort_Example( "Error loading data/obp.jpg" );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / FPS );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( example.display ) );
        Al_Start_Timer( timer );

        while not done loop
            w := Al_Get_Display_Width( example.display );
            h := Al_Get_Display_Height( example.display );

            if (not background and need_redraw) and then Al_Is_Event_Queue_Empty( queue ) then
                t := double(-Al_Get_Time);

                Redraw;

                t := t + double(Al_Get_Time);
                example.direct_speed_measure := t;
                Al_Flip_Display;
                need_redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_CHAR =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    end if;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    done := True;

                when ALLEGRO_EVENT_DISPLAY_HALT_DRAWING =>
                    background := True;
                    Al_Acknowledge_Drawing_Halt( event.display.source );

                when ALLEGRO_EVENT_DISPLAY_RESUME_DRAWING =>
                    background := False;

                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                    Al_Acknowledge_Resize( event.display.source );

                when ALLEGRO_EVENT_TIMER =>
                    Update;
                    need_redraw := True;

                when others => null;

            end case;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Depth_Mask;

