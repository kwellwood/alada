
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Projection2 is

    procedure Draw_Pyramid( texture : A_Allegro_Bitmap; x, y, z, theta : Float ) is
        c   : constant Allegro_Color := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
        t   : Allegro_Transform;
        vtx : constant Allegro_Vertex_Array(0..4) := (
        --   x     y     z     u     v    c  --
           ( 0.0,  1.0,  0.0,  0.0, 64.0, c),
           (-1.0, -1.0, -1.0,  0.0,  0.0, c),
           ( 1.0, -1.0, -1.0, 64.0, 64.0, c),
           ( 1.0, -1.0,  1.0, 64.0,  0.0, c),
           (-1.0, -1.0,  1.0, 64.0, 64.0, c)
        );
        indices : constant Vertex_Index_Array(0..11) := (
            0, 1, 2,
            0, 2, 3,
            0, 3, 4,
            0, 4, 1
        );
    begin
        Al_Identity_Transform( t );
        Al_Rotate_Transform_3d( t, 0.0, 1.0, 0.0, theta );
        Al_Translate_Transform_3d( t, x, y, z );
        Al_Use_Transform( t );
        Al_Draw_Indexed_Prim( vtx, texture, indices, ALLEGRO_PRIM_TRIANGLE_LIST );
    end Draw_Pyramid;

    ----------------------------------------------------------------------------

    procedure Set_Perspective_Transform( bmp : A_Allegro_Bitmap ) is
        p            : Allegro_Transform;
        aspect_ratio : constant Float := Float(Al_Get_Bitmap_Height( bmp )) / Float(Al_Get_Bitmap_Width( bmp ));
    begin
        Al_Set_Target_Bitmap( bmp );
        Al_Identity_Transform( p );
        Al_Perspective_Transform( p, -1.0, aspect_ratio, 1.0, 1.0, -aspect_ratio, 1000.0 );
        Al_Use_Projection_Transform( p );
    end Set_Perspective_Transform;

    ----------------------------------------------------------------------------

    procedure Ada_Main is

        function Fmod( x, y : Float ) return Float is
            sign : constant Float := (if x / y < 0.0 then -1.0 else 1.0);
        begin
            return ((x / y) - (sign * Float'Floor( abs ( x / y ) ) ) ) * y;
        end Fmod;

        display           : A_Allegro_Display;
        timer             : A_Allegro_Timer;
        queue             : A_Allegro_Event_Queue;
        texture           : A_Allegro_Bitmap;
        display_sub_persp : A_Allegro_Bitmap;
        display_sub_ortho : A_Allegro_Bitmap;
        buffer            : A_Allegro_Bitmap;
        font              : A_Allegro_Font;
        redraw            : Boolean := False;
        quit              : Boolean := False;
        fullscreen        : Boolean := False;
        background        : Boolean := False;
        display_flags     : Unsigned_32 := ALLEGRO_RESIZABLE;
        theta             : Float := 0.0;
        event             : Allegro_Event;
    begin
        if Argument_Count = 1 then
            if Argument( 1 ) = "--use-shaders" then
                display_flags := display_flags or ALLEGRO_PROGRAMMABLE_PIPELINE;
            else
                Abort_Example( "Unknown command line argument: " & Argument( 1 ) );
            end if;
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( display_flags );
        Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, 16, ALLEGRO_SUGGEST );
        -- Load everything as a POT bitmap to make sure the projection stuff works
        -- with mismatched backing texture and bitmap sizes.
        Al_Set_New_Display_Option( ALLEGRO_SUPPORT_NPOT_BITMAP, 0, ALLEGRO_REQUIRE );
        display := Al_Create_Display( 800, 600 );
        if display = null then
            Abort_Example("Error creating display\n");
        end if;
        Al_Set_Window_Constraints( display, 256, 512, 0, 0 );
        Al_Apply_Window_Constraints( display, True );
        Set_Perspective_Transform( Al_Get_Backbuffer( display ) );

        -- This bitmap is a sub-bitmap of the display, and has a perspective transformation.
        display_sub_persp := Al_Create_Sub_Bitmap( Al_Get_Backbuffer( display ), 0, 0, 256, 256 );
        Set_Perspective_Transform( display_sub_persp );

        -- This bitmap is a sub-bitmap of the display, and has a orthographic transformation.
        display_sub_ortho := Al_Create_Sub_Bitmap( Al_Get_Backbuffer( display ), 0, 0, 256, 512 );

        -- This bitmap has a perspective transformation, purposefully non-POT
        buffer := Al_Create_Bitmap( 200, 200 );
        Set_Perspective_Transform( buffer );

        timer := Al_Create_Timer( 1.0 / 60.0 );
        font := Al_Create_Builtin_Font;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR or ALLEGRO_MIPMAP );

        texture := Al_Load_Bitmap( "data/bkg.png" );
        if texture = null then
            Abort_Example( "Could not load data/bkg.png" );
        end if;

        Al_Start_Timer( timer );
        while not quit loop
            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    quit := True;
                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                    Al_Acknowledge_Resize( display );
                    Set_Perspective_Transform( Al_Get_Backbuffer( display ) );
                when ALLEGRO_EVENT_KEY_DOWN =>
                    case event.keyboard.keycode is
                        when ALLEGRO_KEY_ESCAPE =>
                            quit := True;
                        when ALLEGRO_KEY_SPACE =>
                            fullscreen := not fullscreen;
                            Al_Set_Display_Flag( display, ALLEGRO_FULLSCREEN_WINDOW, fullscreen );
                            Set_Perspective_Transform( Al_Get_Backbuffer( display ) );
                        when others => null;
                    end case;
                when ALLEGRO_EVENT_TIMER =>
                    redraw := True;
                    theta := Fmod( theta + 0.05, 2.0 * ALLEGRO_PI );
                when ALLEGRO_EVENT_DISPLAY_HALT_DRAWING =>
                    background := True;
                    Al_Acknowledge_Drawing_Halt( display );
                    Al_Stop_Timer( timer );
                when ALLEGRO_EVENT_DISPLAY_RESUME_DRAWING =>
                    background := False;
                    Al_Acknowledge_Drawing_Resume( display );
                    Al_Start_Timer( timer );
                when others => null;
            end case;

            if not background and then redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Al_Set_Target_Backbuffer( display );
                Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 1 );
                Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
                Al_Clear_Depth_Buffer( 1000.0 );
                Draw_Pyramid( texture, 0.0, 0.0, -4.0, theta );

                Al_Set_Target_Bitmap( buffer );
                Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 1 );
                Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.1, 0.1 ) );
                Al_Clear_Depth_Buffer( 1000.0 );
                Draw_Pyramid( texture, 0.0, 0.0, -4.0, theta );

                Al_Set_Target_Bitmap( display_sub_persp );
                Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 1 );
                Al_Clear_To_Color( al_map_rgb_f( 0.0, 0.0, 0.25 ) );
                Al_Clear_Depth_Buffer( 1000.0 );
                Draw_Pyramid( texture, 0.0, 0.0, -4.0, theta );

                Al_Set_Target_Bitmap( display_sub_ortho );
                Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 0 );
                Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 128.0, 16.0, ALLEGRO_ALIGN_CENTER,
                              "Press Space to toggle fullscreen" );
                Al_Draw_Bitmap( buffer, 0.0, 256.0, 0 );

                Al_Flip_Display;
                redraw := False;
            end if;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Projection2;
