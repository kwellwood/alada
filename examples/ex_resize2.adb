
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
--    Test program for Allegro.
--
--    Resizing the window currently shows broken behaviour.
--
package body Ex_Resize2 is

    procedure Ada_Main is
        display : A_Allegro_Display;
        bmp     : A_Allegro_Bitmap;
        font    : A_Allegro_Font;
        queue   : A_Allegro_Event_Queue;
        event   : aliased Allegro_Event;
        redraw  : Boolean;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE or ALLEGRO_GENERATE_EXPOSE_EVENTS );
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Unable to set any graphic mode" );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        bmp := Al_Load_Bitmap( "data/mysha.pcx" );
        if bmp = null then
            Abort_Example( "Unable to load image" );
        end if;

        font := Al_Create_Builtin_Font;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        redraw := True;
        loop
            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Al_Clear_To_Color( Al_Map_RGB( 255, 0, 0 ) );
                Al_Draw_Scaled_Bitmap( bmp, 0.0, 0.0, Float(Al_Get_Bitmap_Width( bmp )),
                                       Float(Al_Get_Bitmap_Height( bmp )), 0.0, 0.0,
                                       Float(Al_Get_Display_Width( display )),
                                       Float(Al_Get_Display_Height( display )), 0 );
                Al_Draw_Multiline_Text( font, Al_Map_RGB( 255, 255, 0 ), 0.0, 0.0, 640.0,
                                        Float(Al_Get_Font_Line_Height( font )), 0,
                                        "size:" & Al_Get_Display_Width( display )'Img & " x" & Al_Get_Display_Height( display )'Img & ASCII.LF &
                                        "maximized: " & (if (Al_Get_Display_Flags( display ) and ALLEGRO_MAXIMIZED) /= 0 then "yes" else "no") & ASCII.LF &
                                        "+ key to maximize" & ASCII.LF &
                                        "- key to un-maximize" );
                Al_Flip_Display;
                redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_RESIZE then
                Al_Acknowledge_Resize( event.display.source );
                redraw := True;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_EXPOSE then
                redraw := True;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR and then event.keyboard.unichar = Character'Pos( '+' ) then
                Al_Set_Display_Flag( display, ALLEGRO_MAXIMIZED, True );
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR and then event.keyboard.unichar = Character'Pos( '-' ) then
                Al_Set_Display_Flag( display, ALLEGRO_MAXIMIZED, False );
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            end if;
        end loop;

        Al_Destroy_Bitmap( bmp );
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Resize2;
