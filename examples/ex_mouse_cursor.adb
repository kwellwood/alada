
with Ada.Unchecked_Conversion;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Mouse.Cursors;             use Allegro.Mouse.Cursors;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
--    Example program for the Allegro library, by Peter Wang.
--
package body Ex_Mouse_Cursor is

    procedure Ada_Main is

        type CursorListRec is
            record
                system_cursor : Allegro_System_Mouse_Cursor;
                label         : access String;
            end record;

        type CursorList is array (Natural range <>) of CursorListRec;

        MARGIN_LEFT : constant := 20;
        MARGIN_TOP  : constant := 20;
        NUM_CURSORS : constant := 20;

        cursor_list : constant CursorList(0..NUM_CURSORS-1) := (
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT,     new String'("DEFAULT")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_ARROW,       new String'("ARROW")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_BUSY,        new String'("BUSY")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_QUESTION,    new String'("QUESTION")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_EDIT,        new String'("EDIT")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_MOVE,        new String'("MOVE")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_N,    new String'("RESIZE_N")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_W,    new String'("RESIZE_W")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_S,    new String'("RESIZE_S")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_E,    new String'("RESIZE_E")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NW,   new String'("RESIZE_NW")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SW,   new String'("RESIZE_SW")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SE,   new String'("RESIZE_SE")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NE,   new String'("RESIZE_NE")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_PROGRESS,    new String'("PROGRESS")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_PRECISION,   new String'("PRECISION")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_LINK,        new String'("LINK")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_ALT_SELECT,  new String'("ALT_SELECT")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_UNAVAILABLE, new String'("UNAVAILABLE")),
            (ALLEGRO_SYSTEM_MOUSE_CURSOR_NONE,        new String'("CUSTOM")));

        type Cursor_Array is array (Natural range <>) of Allegro_System_Mouse_Cursor;
        current_cursor : Cursor_Array := (ALLEGRO_SYSTEM_MOUSE_CURSOR_NONE,
                                          ALLEGRO_SYSTEM_MOUSE_CURSOR_NONE);

        ----------------------------------------------------------------------------

        procedure Draw_Display( font : A_Allegro_Font ) is
            th, i : Integer := 0;
        begin
            Al_Clear_To_Color( Al_Map_RGB( 128, 128, 128 ) );

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            th := Al_Get_Font_Line_Height( font );
            for i in 0..NUM_CURSORS - 1 loop
                Al_Draw_Text( font, Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 ),
                              Float(MARGIN_LEFT), Float(MARGIN_TOP + i * th), 0,
                              cursor_list(i).label.all );
            end loop;

            i := NUM_CURSORS;
            Al_Draw_Text( font, Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 ),
                          Float(MARGIN_LEFT), Float(MARGIN_TOP + i * th), 0,
                          "Press S/H to show/hide cursor" );

            Al_Flip_Display;
        end Draw_Display;

        ----------------------------------------------------------------------------

        function Hover( font : A_Allegro_Font; y : Integer ) return Integer is
            th, i : Integer;
        begin
            if y < MARGIN_TOP then
                return -1;
            end if;

            th := Al_Get_Font_Line_Height( font );
            i := (y - MARGIN_TOP) / th;
            if i < NUM_CURSORS then
                return i;
            end if;

            return -1;
        end Hover;

        ----------------------------------------------------------------------------

        display1,
        display2      : A_Allegro_Display;
        bmp,
        shrunk_bmp    : A_Allegro_Bitmap;
        custom_cursor : A_Allegro_Mouse_Cursor;
        queue         : A_Allegro_Event_Queue;
        font          : A_Allegro_Font;
        event         : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon" );
        end if;

        Init_Platform_Specific;

        if not Al_Install_Mouse then
            Abort_Example( "Error installing mouse" );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard" );
        end if;

        Al_Set_New_Display_Flags( ALLEGRO_GENERATE_EXPOSE_EVENTS );
        display1 := al_create_display( 400, 400 );
        if display1 = null then
            Abort_Example( "Error creating display1" );
        end if;

        display2 := Al_Create_Display( 400, 400 );
        if display2 = null then
            Abort_Example( "Error creating display2" );
        end if;

        bmp := Al_Load_Bitmap( "data/allegro.pcx" );
        if bmp = null then
            Abort_Example("Error loading data/allegro.pcx\n");
        end if;

        font := Al_Load_Bitmap_Font( "data/fixed_font.tga" );
        if font = null then
            Abort_Example( "Error loading data/fixed_font.tga" );
        end if;

        shrunk_bmp := Al_Create_Bitmap( 32, 32 );
        if shrunk_bmp = null then
            Abort_Example( "Error creating shrunk_bmp" );
        end if;

        Al_Set_Target_Bitmap( shrunk_bmp );
        Al_Draw_Scaled_Bitmap( bmp,
                               0.0, 0.0,
                               Float(Al_Get_Bitmap_Width( bmp )),
                               Float(Al_Get_Bitmap_Height( bmp )),
                               0.0, 0.0, 32.0, 32.0,
                               0 );

        custom_cursor := Al_Create_Mouse_Cursor( shrunk_bmp, 0, 0 );
        if custom_cursor = null then
            Abort_Example( "Error creating mouse cursor" );
        end if;

        Al_Set_Target_Bitmap( null );
        Al_Destroy_Bitmap( shrunk_bmp );
        Al_Destroy_Bitmap( bmp );
        shrunk_bmp := null;
        bmp := null;

        queue := Al_Create_Event_Queue;
        if queue = null then
            Abort_Example( "Error creating event queue" );
        end if;

        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display1 ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display2 ) );

        Al_Set_Target_Backbuffer( display1 );
        Draw_Display( font );

        Al_Set_Target_Backbuffer( display2 );
        Draw_Display( font );

        loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_EXPOSE then
                Al_Set_Target_Backbuffer( event.display.source );
                Draw_Display( font );
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                case event.keyboard.unichar is
                    when 27 => -- escape
                        goto Quit;
                    when Character'Pos('h') =>
                        Al_Hide_Mouse_Cursor( event.keyboard.display );
                    when Character'Pos('s') =>
                        Al_Show_Mouse_Cursor( event.keyboard.display );
                    when others =>
                        null;
                end case;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_AXES then
                declare
                    function To_Cursor is new Ada.Unchecked_Conversion( Integer, Allegro_System_Mouse_Cursor );
                    dpy : Integer;
                    i   : Integer;
                begin
                    if event.mouse.display = display1 then
                        dpy := 0;
                    else
                        dpy := 1;
                    end if;
                    i := Hover( font, event.mouse.y );

                    if i >= 0 and then current_cursor(dpy) /= To_Cursor( i ) then
                        if cursor_list(i).system_cursor /= ALLEGRO_SYSTEM_MOUSE_CURSOR_NONE then
                            Al_Set_System_Mouse_Cursor( event.mouse.display,
                                                        cursor_list(i).system_cursor );
                        else
                            Al_Set_Mouse_Cursor( event.mouse.display, custom_cursor );
                        end if;
                        current_cursor(dpy) := To_Cursor( i );
                    end if;
                end;
            end if;
        end loop;

    <<Quit>>

        Al_Destroy_Mouse_Cursor( custom_cursor );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Mouse_Cursor;
