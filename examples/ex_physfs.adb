
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Allegro.File_System;               use Allegro.File_System;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Paths;                     use Allegro.Paths;
with Allegro.PhysicsFS;                 use Allegro.PhysicsFS;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System;                            use System;

--
--    Example program for Allegro library.
--
--    Demonstrate PhysicsFS addon.
--
package body Ex_PhysFS is

    procedure Ada_Main is

        procedure Show_Image( bmp : A_Allegro_Bitmap ) is
            queue : A_Allegro_Event_Queue;
            event : aliased Allegro_Event;
        begin
            queue := Al_Create_Event_Queue;
            Al_Register_Event_Source( queue, al_get_keyboard_event_source );

            loop
                Al_Draw_Bitmap( bmp, 0.0, 0.0, 0 );
                Al_Flip_Display;
                Al_Wait_For_Event( queue, event );
                if event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
                   event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
                then
                    exit;
                end if;
            end loop;

            Al_Destroy_Event_Queue( queue );
        end Show_Image;

        ------------------------------------------------------------------------

        procedure Print_File( e : A_Allegro_Fs_Entry ) is

            function c_time( timer : Address ) return time_t;
            pragma Import( C, c_time, "time" );

            function IsAttr( mode : Allegro_File_Mode;
                             flag : Allegro_File_Mode;
                             str  : String ) return String is
            begin
                if (mode and flag) > 0 then
                    return str;
                end if;
                return ".";
            end IsAttr;

            mode  : constant Allegro_File_Mode := Al_Get_Fs_Entry_Mode( e );
            now   : constant time_t := c_time( Null_Address );
            atime : constant String := Trim( time_t'Image( now - Al_Get_Fs_Entry_Atime( e ) ), Left );
            ctime : constant String := Trim( time_t'Image( now - Al_Get_Fs_Entry_Ctime( e ) ), Left );
            mtime : constant String := Trim( time_t'Image( now - Al_Get_Fs_Entry_Mtime( e ) ), Left );
            name  : constant String := Al_Get_Fs_Entry_Name( e );
            size  : constant String := Trim( off_t'Image( Al_Get_Fs_Entry_Size( e ) ), Left );
        begin
            Log_Print(name & ((36 - name'Length) * ' ') & " " &
                      IsAttr( mode, ALLEGRO_FILEMODE_READ, "r" ) &
                      IsAttr( mode, ALLEGRO_FILEMODE_WRITE, "w" ) &
                      IsAttr( mode, ALLEGRO_FILEMODE_EXECUTE, "x" ) &
                      IsAttr( mode, ALLEGRO_FILEMODE_HIDDEN, "h" ) &
                      IsAttr( mode, ALLEGRO_FILEMODE_ISFILE, "f" ) &
                      IsAttr( mode, ALLEGRO_FILEMODE_ISDIR, "d" ) & " " &
                      ctime & ((9 - ctime'Length) * ' ') & " " &
                      mtime & ((9 - mtime'Length) * ' ') & " " &
                      atime & ((9 - atime'Length) * ' ') & " " &
                      size & ((9 - size'Length) * ' '));
        end Print_File;

        ------------------------------------------------------------------------

        procedure Listdir( e : A_Allegro_Fs_Entry ) is
            next : A_Allegro_Fs_Entry;
        begin
            if not Al_Open_Directory( e ) then
                return;
            end if;
            loop
                next := Al_Read_Directory( e );
                if next = null then
                    exit;
                end if;

                Print_File( next );
                if (Al_Get_Fs_Entry_Mode( next ) and ALLEGRO_FILEMODE_ISDIR) > 0 then
                    Listdir( next );
                end if;
                Al_Destroy_Fs_Entry( next );
            end loop;
            if Al_Close_Directory( e ) then
                null;
            end if;
        end Listdir;

        ------------------------------------------------------------------------

        function Add_Main_Zipfile return Boolean is
            exe     : A_Allegro_Path := Al_Get_Standard_Path( ALLEGRO_EXENAME_PATH );
            ext     : constant String := Al_Get_Path_Extension( exe );
            ret     : Boolean := False;
            zipfile : Unbounded_String;
        begin
            -- On Android we treat the APK itself as the zip file.
            if ext = ".apk" then
                zipfile := To_Unbounded_String( Al_Path_CStr( exe, '/' ) );
            else
                zipfile := To_Unbounded_String( "data/ex_physfs.zip" );
            end if;

            if PHYSFS_Mount( To_String( zipfile ), "", True ) then
                ret := True;
            else
                Log_Print( "Could not load the zip file: " & To_String( zipfile ) );
                ret := False;
            end if;

            Al_Destroy_Path( exe );
            return ret;
        end Add_Main_Zipfile;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        bmp     : A_Allegro_Bitmap;
        entrie  : A_Allegro_Fs_Entry;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Error initializing image addon" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard" );
        end if;
        Open_Log_Monospace;

        -- Set up PhysicsFS.
        if not PHYSFS_Init then
            Abort_Example( "Could not init PhysFS" );
        end if;
        if not Add_Main_Zipfile then
            Abort_Example( "Could not add zip file" );
        end if;

        for i in 1..Argument_Count loop
            if not PHYSFS_Mount( Argument( i ), "", True ) then
                Abort_Example( "Couldn't add " & Argument( i ) );
            end if;
        end loop;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        -- Make future calls to Al_Fopen() on this thread go to the PhysicsFS
        -- backend.
        --
        Al_Set_Physfs_File_Interface;

        -- List the contents of our example zip recursively.

        Log_Print( "name" & (32 * ' ') & " " &
                   "flags" & (1 * ' ') & " " &
                   "ctime" & (4 * ' ') & " " &
                   "mtime" & (4 * ' ') & " " &
                   "atime" & (4 * ' ') & " " &
                   "size"  & (5 * ' ') );
        Log_Print( "------------------------------------ " &
                   "------ " &
                   "--------- " &
                   "--------- " &
                   "--------- " &
                   "---------");
        entrie := Al_Create_Fs_Entry( "" );
        Listdir( entrie );
        Al_Destroy_Fs_Entry( entrie );

        bmp := Al_Load_Bitmap( "02.bmp" );
        if bmp = null then
            -- Fallback for Android. (assets/data/alexlogo.bmp)
            bmp := Al_Load_Bitmap( "data/alexlogo.bmp" );
        end if;
        if bmp /= null then
            Show_Image( bmp );
            Al_Destroy_Bitmap( bmp );
        end if;

        if not PHYSFS_Deinit then
            null;
        end if;

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_PhysFS;
