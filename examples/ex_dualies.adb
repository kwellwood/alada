
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Displays.Monitors;         use Allegro.Displays.Monitors;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Dualies is

    procedure Ada_Main is

        ------------------------------------------------------------------------

        procedure Go is
            d1, d2 : A_Allegro_Display;
            b1, b2 : A_Allegro_Bitmap;
            queue  : A_Allegro_Event_Queue;
            event  : aliased Allegro_Event;
        begin
            Al_Set_New_Display_Flags( ALLEGRO_FULLSCREEN );

            Al_Set_New_Display_Adapter( 0 );
            d1 := Al_Create_Display( 640, 480 );
            if d1 = null then
                Abort_Example( "Error creating first display" );
            end if;
            b1 := Al_Load_Bitmap( "data/mysha.pcx" );
            if b1 = null then
                Abort_Example( "Error loading mysha.pcx" );
            end if;

            Al_Set_New_Display_Adapter( 1 );
            d2 := Al_Create_Display( 640, 480 );
            if d2 = null then
                Abort_Example( "Error creating second display" );
            end if;
            b2 := Al_Load_Bitmap( "data/allegro.pcx" );
            if b2 = null then
                Abort_Example( "Error loading allegro.pcx" );
            end if;

            queue := Al_Create_Event_Queue;
            Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

            loop
                if not Al_Is_Event_Queue_Empty( queue ) then
                    if Al_Get_Next_Event( queue, event'Access ) then
                        if event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                            if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                                exit;
                            end if;
                        end if;
                    end if;
                end if;

                Al_Set_Target_Backbuffer( d1 );
                Al_Draw_Scaled_Bitmap( b1, 0.0, 0.0, 320.0, 200.0, 0.0, 0.0, 640.0, 480.0, 0 );
                Al_Flip_Display;

                Al_Set_Target_Backbuffer( d2 );
                Al_Draw_Scaled_Bitmap( b2, 0.0, 0.0, 320.0, 200.0, 0.0, 0.0, 640.0, 480.0, 0 );
                Al_Flip_Display;

                Al_Rest( 0.1 );
            end loop;

            Al_Destroy_Bitmap( b1 );
            Al_Destroy_Bitmap( b2 );
            Al_Destroy_Display( d1 );
            Al_Destroy_Display( d2 );
        end Go;

        ------------------------------------------------------------------------

    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init Allegro IIO addon." );
        end if;

        if Al_Get_Num_Video_Adapters < 2 then
            Abort_Example( "You need 2 or more adapters/monitors for this example." );
        end if;

        Go;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Dualies;
