
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Paths;                     use Allegro.Paths;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;

package body Ex_Path is

    procedure Ada_Main is
        exe,
        dyn,
        tostring,
        cloned   : A_Allegro_Path;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        Open_Log;

        if Argument_Count /= 1 then
            exe := Al_Create_Path( Command_Name );
            if exe /= null then
                Log_Print( "This example needs to be run from the command line." );
                Log_Print("usage1: " & Al_Get_Path_Filename( exe ) & " <path>" );
                Al_Destroy_Path( exe );
            else
                Log_Print( "usage2: " & Command_Name & " <path>" );
            end if;
            Close_Log( True );
            return;
        end if;

        dyn := Al_Create_Path( Argument( 1 ) );
        if dyn = null then
            Log_Print( "Failed to create path structure for '" & Argument( 1 ) & "'." );
        else
            Log_Print( "dyn: drive=""" & Al_Get_Path_Drive( dyn ) & """, file=""" &
                       Al_Get_Path_Filename( dyn ) & """" );
            Al_Destroy_Path( dyn );
        end if;

        tostring := Al_Create_Path( Argument( 1 ) );
        if tostring = null then
            Log_Print( "Failed to create path structure for tostring test" );
        else
            Log_Print("tostring: '" & Al_Path_Cstr( tostring, '/' ) & "'" );
            Log_Put("tostring: drive:'" & Al_Get_Path_Drive( tostring ) & "'" );
            Log_Put(" dirs:");
            for i in 0..Al_Get_Path_Num_Components( tostring ) - 1 loop
                if i > 0 then
                    Log_Put( "," );
                end if;
                Log_Put(" '" & Al_Get_Path_Component( tostring, i ) & "'" );
            end loop;
            Log_Print(" filename:'" & Al_Get_Path_Filename( tostring ) & "'" );
            Al_Destroy_Path( tostring );
        end if;

        -- FIXME: test out more of the al_path_ functions, ie: insert, remove,
        -- concat, relative
        --

        dyn := Al_Create_Path( Argument( 1 ) );
        if dyn /= null then
            cloned := Al_Clone_Path( dyn );
            if cloned /= null then
                Log_Print( "dyn: '" & Al_Path_Cstr( dyn, '/' ) & "'" );

                if Al_Make_Path_Canonical( cloned ) then
                    Log_Print( "can: '" & Al_Path_Cstr( cloned, '/' ) & "'" );
                else
                    Log_Print( "failed to make canonical path" );
                end if;

                Al_Destroy_Path( dyn );
                Al_Destroy_Path( cloned );
            else
                Log_Print( "failed to clone ALLEGRO_PATH :(" );
                Al_Destroy_Path( dyn );
            end if;
        else
            Log_Print( "failed to create new ALLEGRO_PATH for cloning..." );
        end if;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Path;
