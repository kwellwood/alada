
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;

package Common is

    procedure Init_Platform_Specific;

    procedure Abort_Example( str : String );

    procedure Open_Log;

    procedure Open_Log_Monospace;

    -- Doesn't print a new-line character.
    procedure Log_Put( str : String );

    -- Prints a whole line.
    procedure Log_Print( str : String );

    procedure Close_Log( wait_for_user : Boolean );

    textlog : A_Allegro_Textlog;

end Common;
