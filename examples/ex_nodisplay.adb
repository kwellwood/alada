
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.System;                    use Allegro.System;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

-- Test that bitmap manipulation without a display works.
package body Ex_Nodisplay is

    procedure Ada_Main is
        bmp, sprite : A_Allegro_Bitmap;
        c1, c2, c3  : Allegro_Color;
    begin
        if not Al_Initialize then
            Abort_Example( "Error initializing Allegro" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Error initializing image IO addon" );
        end if;
        Init_Platform_Specific;

        sprite := Al_Load_Bitmap( "data/cursor.tga" );
        if sprite = null then
            Abort_Example( "Error loading data/cursor.tga" );
        end if;

        bmp := Al_Create_Bitmap( 256, 256 );
        if bmp = null then
            Abort_Example( "Error creating bitmap" );
        end if;
        Al_Set_Target_Bitmap( bmp );

        c1 := Al_Map_RGB( 255, 0, 0 );
        c2 := Al_Map_RGB( 0, 255, 0 );
        c3 := Al_Map_RGB( 0, 0, 255 );

        Al_Clear_To_Color( Al_Map_RGBA( 255, 0, 0, 128 ) );
        Al_Draw_Bitmap( sprite, 0.0, 0.0, 0 );
        Al_Draw_Tinted_Bitmap( sprite, c1, 64.0, 0.0, ALLEGRO_FLIP_HORIZONTAL );
        Al_Draw_Tinted_Bitmap( sprite, c2, 0.0, 64.0, ALLEGRO_FLIP_VERTICAL );
        Al_Draw_Tinted_Bitmap( sprite, c3, 64.0, 64.0, ALLEGRO_FLIP_HORIZONTAL or ALLEGRO_FLIP_VERTICAL );

        Al_Set_Target_Bitmap( null );

        if Al_Save_Bitmap( "ex_nodisplay_out.tga", bmp ) then
            if Al_Show_Native_Message_Box( null, "ex_nodisplay_out", "",
                                          "Saved ex_nodisplay_out.tga", "", 0 ) = 0
            then
                null;
            end if;
        else
            Abort_Example( "Error saving ex_nodisplay_out.tga" );
        end if;

        Al_Destroy_Bitmap( sprite );
        Al_Destroy_Bitmap( bmp );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Nodisplay;
