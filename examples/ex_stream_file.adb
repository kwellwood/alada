
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.Events;                    use Allegro.Events;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
-- An example program that plays a file from the disk using Allegro5
-- streaming API. The file is being read in small chunks and played on the
-- sound device instead of being loaded at once.
--
-- usage: ./ex_stream_file file.[wav,ogg...] ...
--
-- by Milan Mimica
--
package body Ex_Stream_File is

    procedure Ada_Main is
        -- Attaches the stream directly to a voice. Streamed file's and voice's
        -- sample rate, channels and depth must match.
        BYPASS_MIXER : constant Boolean := False;

        voice     : A_Allegro_Voice;
        mixer     : A_Allegro_Mixer;
        doLoop    : Boolean := False;
        arg_start : Integer := 1;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if Argument_Count < 1 then
            Log_Print( "This example needs to be run from the command line." );
            Log_Print( "Usage: ex_stream_file [--loop] {audio_files}" );
            Close_Log( True );
            return;
        end if;

        if Argument( 1 ) = "--loop" then
            doLoop := True;
            arg_start := 2;
        end if;

        if not Al_Init_ACodec_Addon then
            Abort_Example( "Could not init audio codecs." );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound!" );
        end if;

        voice := Al_Create_Voice( 44100,
                                  ALLEGRO_AUDIO_DEPTH_INT16,
                                  ALLEGRO_CHANNEL_CONF_2 );
        if voice = null then
            Abort_Example( "Could not create ALLEGRO_VOICE." );
        end if;
        Log_Print( "Voice created." );

        if not BYPASS_MIXER then
            mixer := Al_Create_Mixer( 44100,
                                      ALLEGRO_AUDIO_DEPTH_FLOAT32,
                                      ALLEGRO_CHANNEL_CONF_2 );
            if mixer = null then
                Abort_Example( "Could not create ALLEGRO_MIXER." );
            end if;
            Log_Print( "Mixer created." );

            if not Al_Attach_Mixer_To_Voice( mixer, voice ) then
                Abort_Example( "al_attach_mixer_to_voice failed." );
            end if;
        end if;

        for i in arg_start..Argument_Count loop
            declare
                filename : constant String := Argument( i );
                stream   : A_Allegro_Audio_Stream;
                playing  : Boolean := True;
                event    : aliased Allegro_Event;
                queue    : A_Allegro_Event_Queue := Al_Create_Event_Queue;
                skip     : Boolean := False;
            begin
                stream := Al_Load_Audio_Stream( filename, 4, 2048 );
                if stream = null then
                    Log_Print( "Could not create an ALLEGRO_AUDIO_STREAM from '" & filename & "'!" );
                else
                    Log_Print( "Stream created from '" & filename & "'." );
                    Al_Set_Audio_Stream_Playmode( stream, (if doLoop then ALLEGRO_PLAYMODE_LOOP else ALLEGRO_PLAYMODE_ONCE) );

                    Al_Register_Event_Source( queue, Al_Get_Audio_Stream_Event_Source( stream ) );

                    if not BYPASS_MIXER then
                        if not Al_Attach_Audio_Stream_To_Mixer( stream, mixer ) then
                            Log_Print( "al_attach_audio_stream_to_mixer failed." );
                            skip := True;
                        end if;
                    else
                        if not Al_Attach_Audio_Stream_To_Voice( stream, voice ) then
                            Abort_Example( "al_attach_audio_stream_to_voice failed." );
                        end if;
                    end if;

                    if not skip then
                        Log_Put( "Playing " & filename & " ... Waiting for stream to finish " );
                        loop
                            Al_Wait_For_Event( queue, event );
                            if event.any.typ = ALLEGRO_EVENT_AUDIO_STREAM_FINISHED then
                                playing := False;
                            end if;
                            exit when not playing;
                        end loop;
                        Log_Print( "" );

                        Al_Destroy_Event_Queue( queue );
                        Al_Destroy_Audio_Stream( stream );
                    end if;
                end if;
            end;
        end loop;

        if not BYPASS_MIXER then
            Al_Destroy_Mixer( mixer );
        end if;
        Al_Destroy_Voice( voice );

        Al_Uninstall_Audio;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Stream_File;
