
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;

package body Ex_Blend_Test is

    procedure Ada_Main is
        test_only_index : Integer := 0;
        test_index      : Integer := 0;
        test_display    : Boolean := False;
        display         : A_Allegro_Display;

        ------------------------------------------------------------------------

        procedure Print_Color( c : Allegro_Color ) is
            r, g, b, a : Float;
        begin
            Al_Unmap_RGBA_f( c, r, g, b, a );
            Log_Put( r'Img & "," & g'Img & "," & b'Img & "," & a'Img );
        end Print_Color;

        ------------------------------------------------------------------------

        function Test( src_col,    dst_col    : Allegro_Color;
                       src_format, dst_format : Allegro_Pixel_Format;
                       src,        dst        : Allegro_Blend_Mode;
                       src_a,      dst_a      : Allegro_Blend_Mode;
                       operation              : Integer;
                       verbose                : Boolean ) return Allegro_Color is

            function Image( format : Allegro_Pixel_Format ) return String is
            begin
                if format = ALLEGRO_PIXEL_FORMAT_ABGR_8888 then
                    return "ABGR_8888";
                elsif format = ALLEGRO_PIXEL_FORMAT_BGR_888 then
                    return "BGR_888";
                else
                    return "?";
                end if;
            end Image;

            function Image( mode : Allegro_Blend_Mode ) return String is
            begin
                if mode = ALLEGRO_ALPHA then
                    return "ALPHA";
                elsif mode = ALLEGRO_ZERO then
                    return "ZERO";
                elsif mode = ALLEGRO_ONE then
                    return "ONE";
                elsif mode = ALLEGRO_INVERSE_ALPHA then
                    return "INVERSE_ALPHA";
                else
                    return "?";
                end if;
            end Image;

            result  : Allegro_Color;
            dst_bmp : A_Allegro_Bitmap;
            src_bmp : A_Allegro_Bitmap;
        begin
            Al_Set_New_Bitmap_Format( dst_format );
            Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            dst_bmp := Al_Create_Bitmap( 1, 1 );
            Al_Set_Target_Bitmap( dst_bmp );
            Al_Clear_To_Color( dst_col );
            if operation = 0 then
                Al_Set_New_Bitmap_Format( src_format );
                src_bmp := Al_Create_Bitmap( 1, 1 );
                Al_Set_Target_Bitmap( src_bmp );
                Al_Clear_To_Color( src_col );
                Al_Set_Target_Bitmap( dst_bmp );
                Al_Set_Separate_Blender( ALLEGRO_ADD, src, dst, ALLEGRO_ADD, src_a, dst_a );
                Al_Draw_Bitmap( src_bmp, 0.0, 0.0, 0 );
                Al_Destroy_Bitmap( src_bmp );
            elsif operation = 1 then
                Al_Set_Separate_Blender( ALLEGRO_ADD, src, dst, ALLEGRO_ADD, src_a, dst_a );
                Al_Draw_Pixel( 0.0, 0.0, src_col );
            elsif operation = 2 then
                Al_Set_Separate_Blender( ALLEGRO_ADD, src, dst, ALLEGRO_ADD, src_a, dst_a );
                Al_Draw_Line( 0.0, 0.0, 1.0, 1.0, src_col, 0.0 );
            end if;

            result := Al_Get_Pixel( dst_bmp, 0, 0 );

            Al_Set_Target_Backbuffer( display );

            if test_display then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                Al_Draw_Bitmap( dst_bmp, 0.0, 0.0, 0 );
            end if;

            Al_Destroy_Bitmap( dst_bmp );

            if verbose then
                Log_Print( "---" );
                Log_Print( "test id:" & test_index'Img );

                Log_Put( "source     : " );
                Print_Color( src_col );
                if operation = 0 then
                    Log_Put( " bitmap" );
                elsif operation = 1 then
                    Log_Put( " pixel" );
                else
                    Log_Put( " prim" );
                end if;
                Log_Print( " format=" & Image( src_format ) & " mode=" & Image( src ) & " alpha=" & Image( src_a ) );

                Log_Put( "destination: " );
                Print_Color( dst_col );
                Log_Print( " format=" & Image( src_format ) & " mode=" & Image( dst ) & " alpha=" & Image( dst_a ) );

                Log_Put( "result     : " );
                Print_Color( result );
                Log_Print( "" );
            end if;

            return result;
        end Test;

        ------------------------------------------------------------------------

        function Same_Color( c1, c2 : Allegro_Color ) return Boolean is
            r1, g1, b1, a1 : Float;
            r2, g2, b2, a2 : Float;
            dr, dg, db, da : Float;
            d              : Float;
        begin
            Al_Unmap_RGBA_f( c1, r1, g1, b1, a1 );
            Al_Unmap_RGBA_f( c2, r2, g2, b2, a2 );
            dr := r1 - r2;
            dg := g1 - g2;
            db := b1 - b2;
            da := a1 - a2;
            d := Sqrt( dr * dr + dg * dg + db * db + da * da );
            return d < 0.01;
        end Same_Color;

        ------------------------------------------------------------------------

        function Get_Factor( operation : Allegro_Blend_Mode; alpha : Float ) return Float is
        begin
            if operation = ALLEGRO_ZERO then
                return 0.0;
            elsif operation = ALLEGRO_ONE then
                return 1.0;
            elsif operation = ALLEGRO_ALPHA then
                return alpha;
            elsif operation = ALLEGRO_INVERSE_ALPHA then
                return 1.0 - alpha;
            else
                return 0.0;
            end if;
        end Get_Factor;

        ------------------------------------------------------------------------

        function Has_Alpha( format : Allegro_Pixel_Format ) return Boolean is
        begin
            if format = ALLEGRO_PIXEL_FORMAT_RGB_888 then
                return False;
            elsif format = ALLEGRO_PIXEL_FORMAT_BGR_888 then
                return False;
            else
                return True;
            end if;
        end Has_Alpha;

        ------------------------------------------------------------------------

        function Reference_Implementation( src_col,    dst_col    : Allegro_Color;
                                           src_format, dst_format : Allegro_Pixel_Format;
                                           src_mode,   dst_mode   : Allegro_Blend_Mode;
                                           src_alpha,  dst_alpha  : Allegro_Blend_Mode;
                                           operation              : Integer ) return Allegro_Color is
            sr, sg, sb, sa       : Float;
            dr, dg, db, da       : Float;
            r, g, b, a           : Float;
            src, dst, asrc, adst : Float;
        begin
            Al_Unmap_RGBA_f( src_col, sr, sg, sb, sa );
            Al_Unmap_RGBA_f( dst_col, dr, dg, db, da );

            -- Do we even have source alpha?
            if operation = 0 then
                if not Has_Alpha( src_format ) then
                    sa := 1.0;
                end if;
            end if;

            r := sr;
            g := sg;
            b := sb;
            a := sa;

            src := Get_Factor( src_mode, a );
            dst := Get_Factor( dst_mode, a );
            asrc := Get_Factor( src_alpha, a );
            adst := Get_Factor( dst_alpha, a );

            r := r * src + dr * dst;
            g := g * src + dg * dst;
            b := b * src + db * dst;
            a := a * asrc + da * adst;

            r := Float'Min( r, 1.0 );
            g := Float'Min( g, 1.0 );
            b := Float'Min( b, 1.0 );
            a := Float'Min( a, 1.0 );

            -- Do we even have destination alpha?
            if not Has_Alpha( dst_format ) then
                a := 1.0;
            end if;

            return Al_Map_RGBA_f( r, g, b, a );
        end Reference_Implementation;

        ------------------------------------------------------------------------

        procedure Do_Test2( src_col,    dst_col    : Allegro_Color;
                            src_format, dst_format : Allegro_Pixel_Format;
                            src_mode,   dst_mode   : Allegro_Blend_Mode;
                            src_alpha,  dst_alpha  : Allegro_Blend_Mode;
                            operation              : Integer ) is
            reference,
            result,
            from_display : Allegro_Color;
            dsp_format   : Allegro_Pixel_Format;
        begin
            test_index := test_index + 1;

            if test_only_index > 0 and then test_index /= test_only_index then
                return;
            end if;

            reference := Reference_Implementation( src_col, dst_col,
                                                   src_format, dst_format,
                                                   src_mode, dst_mode,
                                                   src_alpha, dst_alpha,
                                                   operation );

            result := Test( src_col, dst_col,
                            src_format, dst_format,
                            src_mode, dst_mode,
                            src_alpha, dst_alpha,
                            operation,
                            verbose => False );

            if not Same_Color( reference, result ) then
                result := Test( src_col, dst_col,
                                src_format, dst_format,
                                src_mode, dst_mode,
                                src_alpha, dst_alpha,
                                operation,
                                verbose => True );
                Log_Put( "expected   : " );
                Print_Color( reference );
                Log_Print( "" );
                Log_Print( "FAILED" );
            else
                Log_Put( " OK" );
            end if;

            if test_display then
                dsp_format := Al_Get_Display_Format( display );
                from_display := Al_Get_Pixel( Al_Get_Backbuffer( display ), 0, 0 );
                reference := Reference_Implementation( src_col, dst_col,
                                                       src_format, dsp_format,
                                                       src_mode, dst_mode,
                                                       src_alpha, dst_alpha,
                                                       operation );

                if not Same_Color( reference, from_display ) then
                    result := Test( src_col, dst_col,
                                    src_format, dsp_format,
                                    src_mode, dst_mode,
                                    src_alpha, dst_alpha,
                                    operation,
                                    verbose => True );
                    Log_Put( "displayed  : " );
                    Print_Color( from_display );
                    Log_Print( "" );
                    Log_Put( "expected   : " );
                    Print_Color( reference );
                    Log_Print( "" );
                    Log_Print( "(FAILED on display)" );
                end if;
            end if;
        end Do_Test2;

        ------------------------------------------------------------------------

        procedure Do_Test1( src_col,    dst_col    : Allegro_Color;
                            src_format, dst_format : Allegro_Pixel_Format ) is

            type Blend_Modes_Array is array(Integer range <>) of Allegro_Blend_Mode;

            smodes : constant Blend_Modes_Array := (ALLEGRO_ALPHA,
                                                    ALLEGRO_ZERO,
                                                    ALLEGRO_ONE,
                                                    ALLEGRO_INVERSE_ALPHA);
            dmodes : constant Blend_Modes_Array := (ALLEGRO_INVERSE_ALPHA,
                                                    ALLEGRO_ZERO,
                                                    ALLEGRO_ONE,
                                                    ALLEGRO_ALPHA);
        begin
            for i in smodes'Range loop
                for j in dmodes'Range loop
                    for k in smodes'Range loop
                        for l in dmodes'Range loop
                            for m in 0..2 loop
                                Do_Test2( src_col, dst_col,
                                          src_format, dst_format,
                                          smodes(i), dmodes(j),
                                          smodes(k), dmodes(l),
                                          m );
                            end loop;
                        end loop;
                    end loop;
                end loop;
            end loop;
        end Do_Test1;

        ------------------------------------------------------------------------

        type Color_Array is array(Integer range <>) of Allegro_Color;

        src_colors : Color_Array(0..1);
        dst_colors : Color_Array(0..1);

        type Format_Array is array(Integer range <>) of Allegro_Pixel_Format;

        src_formats : constant Format_Array := (ALLEGRO_PIXEL_FORMAT_ABGR_8888,
                                                ALLEGRO_PIXEL_FORMAT_BGR_888);
        dst_formats : constant Format_Array := (ALLEGRO_PIXEL_FORMAT_ABGR_8888,
                                                ALLEGRO_PIXEL_FORMAT_BGR_888);
    begin
        src_colors(0) := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 );
        src_colors(1) := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
        dst_colors(0) := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
        dst_colors(1) := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.0 );

        for i in 1..Argument_Count loop
            if Argument( i ) = "-d" then
                test_display := True;
            else
                test_only_index := Integer'Value( Argument( i ) );
            end if;
        end loop;

        if not Al_Initialize then
            Abort_Example( "Could not initialize Allegro" );
        end if;

        Open_Log;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not initialize primitives" );
        end if;
        if test_display then
            display := Al_Create_Display( 100, 100 );
            if display = null then
    	        Abort_Example( "Unable to create display" );
            end if;
        end if;

        for i in src_colors'Range loop
            for j in dst_colors'Range loop
                for l in src_formats'Range loop
                    for m in dst_formats'Range loop
                        Do_Test1( src_colors(i), dst_colors(j), src_formats(l), dst_formats(m) );
                    end loop;
                end loop;
            end loop;
        end loop;
        Log_Print( "Done" );
        Log_Print( "" );

        if test_only_index > 0 and then test_display then
            declare
                queue : A_Allegro_Event_Queue;
                event : aliased Allegro_Event;
            begin
                Al_Flip_Display;
                if not Al_Install_Keyboard then
                    Abort_Example( "Could not init keyboard" );
                end if;
                queue := Al_Create_Event_Queue;
                Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
                Al_Wait_For_Event( queue, event );
            end;
        end if;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Blend_Test;
