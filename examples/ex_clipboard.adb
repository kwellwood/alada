
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Clipboard;                 use Allegro.Clipboard;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;

-- An example showing bitmap flipping flags, by Steven Wallace.
package body Ex_Clipboard is

    INTERVAL : constant := 0.1;

    procedure Ada_Main is
        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
        queue   : A_Allegro_Event_Queue;
        font    : A_Allegro_Font;
        text    : Unbounded_String;
        done    : Boolean := False;
        redraw  : Boolean := True;
        event   : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not install Allegro IIO addon." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        if font = null then
            Abort_Example( "Error loading data/fixed_font.tga" );
        end if;

        timer := Al_Create_Timer( INTERVAL );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        Al_Start_Timer( timer );

        Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );

        while not done loop
            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                if (Al_Clipboard_Has_Text( display ) ) then
                    text := Al_Get_Clipboard_Text( display );
                else
                    text := To_Unbounded_String( "" );
                end if;

                Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );

                if Length( text ) > 0 then
                    Al_Draw_Text( font, Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 ), 0.0, 0.0, 0, To_String( text ) );
                else
                    Al_Draw_Text( font, Al_Map_RGBA_f( 1.0, 0.0, 0.0, 1.0 ), 0.0, 0.0, 0, "No clipboard text available." );
                end if;
                Al_Flip_Display;
                redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        Al_Set_Clipboard_Text( display, "Copied from Allegro!" );
                    end if;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    done := True;

                when ALLEGRO_EVENT_TIMER =>
                    redraw := True;

                when others => null;

            end case;
        end loop;

        Al_Destroy_Font( font );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Clipboard;
