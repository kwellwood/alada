
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces.C;                      use Interfaces.C;

--
-- Ryan Dickie
-- Audio example that loads a series of files and puts them through the mixer.
-- Originlly derived from the audio example on the wiki.
--
package body Ex_Acodec is

    procedure Ada_Main is
        voice          : A_Allegro_Voice;
        mixer          : A_Allegro_Mixer;
        sample         : A_Allegro_Sample_Instance;
        sample_data    : A_Allegro_Sample;
        sample_time    : Float;
        -- A matrix that puts everything in the left channel.
        mono_to_stereo : constant Float_Array := (1.0, 0.0);

        type A_String is access all String;
        type String_Array is array (Integer range <>) of A_String;
        type A_String_Array is access all String_Array;
        filenames : A_String_Array;
    begin
        if Argument_Count < 1 then
            filenames := new String_Array(1..1);
            filenames(1) := new String'("data/welcome.wav");
        else
            filenames := new String_Array(1..Argument_Count);
            for i in 1..Argument_Count loop
                filenames(i) := new String'(Argument(i));
            end loop;
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if not Al_Init_Acodec_Addon then
            Abort_Example( "Could not init audio codec addon." );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound!" );
        end if;

        voice := Al_Create_Voice( 44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2 );
        if voice = null then
            Abort_Example( "Could not create ALLEGRO_VOICE." );
        end if;

        mixer := Al_Create_Mixer( 44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2 );
        if mixer = null then
            Abort_Example( "al_create_mixer failed." );
        end if;

        if not Al_Attach_Mixer_To_Voice( mixer, voice ) then
            Abort_Example( "al_attach_mixer_to_voice failed." );
        end if;

        sample := Al_Create_Sample_Instance( null );
        if sample = null then
            Abort_Example( "al_create_sample failed." );
        end if;

        for i in filenames'Range loop
            sample_time := 0.0;

            -- Load the entire sound file from disk. */
            sample_data := Al_Load_Sample( filenames(i).all );
            if sample_data = null then
                Abort_Example( "Could not load sample from '" & filenames(i).all & "'!" );
            end if;

            if sample_data /= null and then not Al_Set_Sample( sample, sample_data ) then
                Abort_Example( "al_set_sample_instance_ptr failed." );
            end if;

            if sample /= null and then not Al_Attach_Sample_Instance_To_Mixer( sample, mixer ) then
                Abort_Example( "al_attach_sample_instance_to_mixer failed." );
            end if;

            -- Play sample in looping mode.
            if sample /= null then
               if not Al_Set_Sample_Instance_Playmode( sample, ALLEGRO_PLAYMODE_LOOP ) then
                    Abort_Example( "Al_Set_Sample_Instance_Playmode failed" );
                end if;
                if not Al_Play_Sample_Instance( sample ) then
                    Abort_Example( "Al_Play_Sample_Instance failed" );
                end if;

                sample_time := Al_Get_Sample_Instance_Time( sample );
                Log_Print( "Playing '" & filenames(i).all & "' (" & sample_time'Img & " seconds) normally" );

                Al_Rest( Long_Float(sample_time) );

                if Al_Get_Channel_Count( Al_Get_Sample_Instance_Channels( sample ) ) = 1 then
                    if not Al_Set_Sample_Instance_Channel_Matrix( sample, mono_to_stereo ) then
                        Abort_Example( "Failed to set channel matrix." );
                    end if;
                    Log_Print( "Playing left channel only." );
                    Al_Rest( Long_Float(sample_time) );
                end if;

                if not Al_Set_Sample_Instance_Gain( sample, 0.5 ) then
                    Abort_Example( "Failed to set gain." );
                end if;
                Log_Print( "Playing with gain 0.5." );
                Al_Rest( Long_Float(sample_time) );

                if not Al_Set_Sample_Instance_Gain( sample, 0.25 ) then
                    Abort_Example( "Failed to set gain." );
                end if;
                Log_Print( "Playing with gain 0.25." );
                Al_Rest( Long_Float(sample_time) );

                if not Al_Stop_Sample_Instance( sample ) then
                    Abort_Example( "Al_Stop_Sample_Instance failed" );
                end if;
                Log_Print( "Done playing " & filenames(i).all );

                -- Free the memory allocated.
                if not Al_Set_Sample( sample, null ) then
                    Abort_Example( "Al_Set_Sample failed" );
                end if;
            end if;
            if sample_data /= null then
                Al_Destroy_Sample( sample_data );
            end if;
        end loop;

        Al_Destroy_Sample_Instance( sample );
        Al_Destroy_Mixer( mixer );
        Al_Destroy_Voice( voice );

        Al_Uninstall_Audio;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Acodec;
