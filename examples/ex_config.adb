
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Configuration;             use Allegro.Configuration;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with GNAT.OS_Lib;

--
--    Example program for the Allegro library.
--
--    Test config file reading and writing.
--
package body Ex_Config is

    procedure Ada_Main is

        passed : Boolean := True;

        ------------------------------------------------------------------------

        procedure Test( name : String; pass : Boolean ) is
        begin
            if pass then
                Log_Print( " PASS - " & name );
            else
                Log_Print( "!FAIL - " & name );
                passed := False;
            end if;
        end Test;

        ------------------------------------------------------------------------

        cfg       : A_Allegro_Config;
        value     : Unbounded_String;
        iterator  : aliased A_Allegro_Config_Section;
        iterator2 : aliased A_Allegro_Config_Entry;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        cfg := Al_Load_Config_File( "data/sample.cfg" );
        if cfg = null then
            Abort_Example( "Couldn't load data/sample.cfg" );
        end if;

        value := To_Unbounded_String( Al_Get_Config_Value( cfg, "", "old_var" ) );
        Test( "global var", value = "old global value" );

        value := To_Unbounded_String( Al_Get_Config_Value(cfg, "section", "old_var" ) );
        Test( "section var", value = "old section value" );

        value := To_Unbounded_String( Al_Get_Config_Value( cfg, "", "mysha.xpm" ) );
        Test( "long value", Length( value ) = 1394 );

        -- Test removing.
        Al_Set_Config_Value( cfg, "empty", "key_remove", "to be removed" );
        Al_Remove_Config_Key( cfg, "empty", "key_remove" );

        Al_Set_Config_Value( cfg, "schrödinger", "box", "cat" );
        Al_Remove_Config_Section( cfg, "schrödinger" );

        -- Test whether iterating through our whole sample.cfg returns all
        -- sections and entries, in order.
        --

        value := To_Unbounded_String( Al_Get_First_Config_Section( cfg, iterator'Access ) );
        Test( "section1", value = "" );  -- global section

        value := To_Unbounded_String( Al_Get_First_Config_Entry( cfg, To_String( value ), iterator2'Access ) );
        Test( "entry1", value = "old_var" );

        value := To_Unbounded_String( Al_Get_Next_Config_Entry( iterator2'Access ) );
        Test( "entry2", value = "mysha.xpm" );

        value := To_Unbounded_String( Al_Get_Next_Config_Entry( iterator2'Access ) );
        Test( "entry3", value = "" );

        value := To_Unbounded_String( Al_Get_Next_Config_Section( iterator'Access ) );
        Test( "section2", value = "section" );

        value := To_Unbounded_String( Al_Get_First_Config_Entry( cfg, To_String( value ), iterator2'Access ) );
        Test( "entry4", value = "old_var" );

        value := To_Unbounded_String( Al_Get_Next_Config_Entry( iterator2'Access ) );
        Test( "entry5", value = "" );

        value := To_Unbounded_String( Al_Get_Next_Config_Section( iterator'Access ) );
        Test( "section3", Length( value ) > 0 );

        value := To_Unbounded_String( Al_Get_First_Config_Entry( cfg, To_String( value ), iterator2'Access ) );
        Test( "entry6", Length( value ) > 0 );

        value := To_Unbounded_String( Al_Get_Next_Config_Entry( iterator2'Access ) );
        Test( "entry7", value = "" );

        value := To_Unbounded_String( Al_Get_Next_Config_Section( iterator'Access ) );
        Test( "empty", value = "empty" );

        value := To_Unbounded_String( Al_Get_First_Config_Entry( cfg, To_String( value ), iterator2'Access ) );
        Test( "empty entry", value = "" );

        value := To_Unbounded_String( Al_Get_Next_Config_Section( iterator'Access ) );
        Test( "section4",  value = "" );

        Al_Set_Config_Value( cfg, "", "new_var", "new value" );
        Al_Set_Config_Value( cfg, "section", "old_var", "new value" );

        Test( "save_config", Al_Save_Config_File( "test.cfg", cfg ) );

        Log_Print( "Done" );

        Al_Destroy_Config( cfg );

        Close_Log( True );

        if not passed then
            GNAT.OS_Lib.OS_Exit( 1 );
        end if;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Config;
