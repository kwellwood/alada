
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with GNAT.OS_Lib;
with Interfaces;                        use Interfaces;

--
--    Example program for the Allegro library.
--
--    Demonstrate 'simple' audio interface.
--
package body Ex_Audio_Simple is

    procedure Ada_Main is
        RESERVED_SAMPLES : constant := 16;
        MAX_SAMPLE_DATA  : constant := 10;

        type Allegro_Sample_Array is array (Integer range <>) of A_Allegro_Sample;

        sample_data : Allegro_Sample_Array(0..MAX_SAMPLE_DATA-1);
        display     : A_Allegro_Display;
        event_queue : A_Allegro_Event_Queue;
        event       : aliased Allegro_Event;
        i           : Integer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if Argument_Count < 1 then
            Log_Print( "This example needs to be run from the command line." );
            Log_Print( "Usage: ex_audio_simple {audio_files}" );
            Close_Log( True );
            GNAT.OS_Lib.OS_Exit( 1 );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        event_queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( event_queue, Al_Get_Keyboard_Event_Source );

        if not Al_Init_Acodec_Addon then
            Abort_Example( "Could not init audio codecs." );
        end if;

<<Restart>>

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound." );
        end if;

        if not Al_Reserve_Samples( RESERVED_SAMPLES ) then
            Abort_Example( "Could not set up voice and mixer." );
        end if;

        for a in 1..Integer'Min( Argument_Count, MAX_SAMPLE_DATA ) loop
            -- Load the entire sound file from disk.
            sample_data(a - 1) := Al_Load_Sample( Argument(a) );
            if sample_data(a - 1) = null then
                Log_Print( "Could not load sample from '" & Argument(a) & "'!" );
            end if;
        end loop;

        Log_Print( "Press digits to play sounds, space to stop sounds, Escape to quit." );

        loop
            Al_Wait_For_Event( event_queue, event );
            if event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                elsif event.keyboard.unichar = Character'Pos( ' ' ) then
                    Al_Stop_Samples;
                end if;

               if event.keyboard.unichar >= Character'Pos( '0' ) and then
                  event.keyboard.unichar <= Character'Pos( '9' )
               then
                   i := (event.keyboard.unichar - Character'Pos( '0' ) + 9) mod 10;
                   if sample_data(i) /= null then
                       Log_Print( "Playing" & i'Img );
                       if not Al_Play_Sample( sample_data(sample_data'First + i), 1.0, 0.5, 1.0, ALLEGRO_PLAYMODE_LOOP, NULL ) then
                           Log_Print( "al_play_sample_data failed, perhaps too many sounds" );
                       end if;
                   end if;
               end if;

                -- Hidden feature: restart audio subsystem.
                -- For debugging race conditions on shutting down the audio.
                --
                if event.keyboard.unichar = Character'Pos( 'r' ) then
                    Al_Uninstall_Audio;
                    goto Restart;
                end if;
            end if;
        end loop;

        Al_Destroy_Display( display );
        Close_Log( True );

        -- Sample data and other objects will be automatically freed.
        Al_Uninstall_Audio;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Audio_Simple;
