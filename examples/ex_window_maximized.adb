
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
-- ex_window_maximized.adb - create the maximized, resizable window.
-- Press SPACE to change constraints.
--
package body Ex_Window_Maximized is

    DISPLAY_W : constant := 640;
    DISPLAY_H : constant := 480;

    use_constraints : Boolean := True;

    ----------------------------------------------------------------------------

    procedure Draw_Information( display : A_Allegro_Display;
                                font    : A_Allegro_Font;
                                color   : Allegro_Color ) is
        min_w, min_h, max_w, max_h : Integer;
    begin
        Al_Draw_Text( font, color, 20.0, 20.0, 0, "Hotkeys:");
        Al_Draw_Text( font, color, 30.0, 30.0, 0, "enabled/disable constraints: ENTER");
        Al_Draw_Text( font, color, 30.0, 40.0, 0, "change constraints: SPACE");

        Al_Draw_Text( font, color, 20.0, 50.0, 0, "Resolution: " &
                      Integer'Image( Al_Get_Display_Width( display ) ) & " x" &
                      Integer'Image( Al_Get_Display_Height( display ) ) );

        Al_Get_Window_Constraints( display, min_w, min_h, max_w, max_h );
        Al_Draw_Text( font, color, 20.0, 60.0, 0, "Constraints: " & (if use_constraints then "Enabled" else "Disabled") );
        Al_Draw_Text( font, color, 20.0, 70.0, 0, "min_w =" & Integer'Image( min_w ) & " min_h =" & Integer'Image( min_h ) );
        Al_Draw_Text( font, color, 20.0, 80.0, 0, "max_w =" & Integer'Image( max_w ) & " max_h =" & Integer'Image( max_h ) );
    end Draw_Information;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display    : A_Allegro_Display;
        queue      : A_Allegro_Event_Queue;
        done       : Boolean := False;
        redraw     : Boolean := True;
        font       : A_Allegro_Font;
        color_text : Allegro_Color;
        color_1    : Allegro_Color;
        color_2    : Allegro_Color;
        color      : Allegro_Color;
        event      : Allegro_Event;
        x2, y2     : Integer;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Error initializing primitives addon." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Failed to init IIO addon." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Error initializing font addon." );
        end if;

        if not Al_Init_Native_Dialog_Addon then
            Abort_Example( "Failed to init native dialog addon." );
        end if;

       Al_Set_New_Display_Flags( ALLEGRO_WINDOWED
                                  or ALLEGRO_RESIZABLE or ALLEGRO_MAXIMIZED
                                  or ALLEGRO_GENERATE_EXPOSE_EVENTS );

        -- creating really small display
        display := Al_Create_Display( DISPLAY_W / 3, DISPLAY_H / 3 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        -- set lower limits for constraints only
        if not Al_Set_Window_Constraints( display, DISPLAY_W / 2, DISPLAY_H / 2, 0, 0 ) then
            Abort_Example( "Unable to set window constraints." );
        end if;
        Al_Apply_Window_Constraints( display, use_constraints );

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        color_text := Al_Map_RGB( 0, 0, 0 );
        color_1    := Al_Map_RGB( 255, 127, 0 );
        color_2    := Al_Map_RGB( 0, 255, 0 );
        color      := color_1;

        font := Al_Create_Builtin_Font;

        while not done loop

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                redraw := False;
                x2 := Al_Get_Display_Width( display ) - 10;
                y2 := Al_Get_Display_Height( display ) - 10;

                Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
                Al_Draw_Filled_Rectangle( 10.0, 10.0, Float(x2), Float(y2), color );
                Draw_Information( display, font, color_text );
                Al_Flip_Display;
            end if;

            Al_Wait_For_Event( queue, event );

            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    end if;

                when ALLEGRO_EVENT_KEY_UP =>
                    if event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        redraw := True;
                        if color = color_1 then
                            if not Al_Set_Window_Constraints( display,
                                                              0, 0,
                                                              DISPLAY_W, DISPLAY_H )
                            then
                                Abort_Example( "Unable to set window constraints." );
                            end if;

                            color := color_2;
                        else
                           if not Al_Set_Window_Constraints( display,
                                                             DISPLAY_W / 2, DISPLAY_H / 2,
                                                             0, 0 )
                           then
                               Abort_Example( "Unable to set window constraints." );
                           end if;

                           color := color_1;
                        end if;
                        Al_Apply_Window_Constraints( display, use_constraints );

                    elsif event.keyboard.keycode = ALLEGRO_KEY_ENTER then
                       redraw := True;
                       use_constraints := not use_constraints;
                       Al_Apply_Window_Constraints( display, use_constraints );

                    end if;

                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                    Al_Acknowledge_Resize( event.display.source );
                    redraw := True;

                when ALLEGRO_EVENT_DISPLAY_EXPOSE =>
                    redraw := True;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    done := True;

                when others =>
                    null;
            end case;
        end loop;

        Al_Destroy_Font( font );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Window_Maximized;

