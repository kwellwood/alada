
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Allegro;                           use Allegro;
with Allegro.File_System;               use Allegro.File_System;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System;                            use System;

package body Ex_Dir is

    procedure Ada_Main is

        ------------------------------------------------------------------------

        function Pad( str : String; width : Positive ) return String is
        begin
            if str'Length < width then
                return str & ((width - str'Length) * " ");
            end if;
            return str;
        end Pad;

        ------------------------------------------------------------------------

        function Time( timer : access time_t ) return time_t;
        pragma Import( C, Time, "time" );

        ------------------------------------------------------------------------

        procedure Print_File( e : A_Allegro_Fs_Entry ) is

            --------------------------------------------------------------------

            function Attrib( value : Boolean; on, off : Character ) return Character is
            begin
                if value then
                    return on;
                end if;
                return off;
            end Attrib;

            --------------------------------------------------------------------

            mode  : constant Allegro_File_Mode := Al_Get_Fs_Entry_Mode( e );
            now   : constant time_t := Time( null );
            atime : constant time_t := Al_Get_Fs_Entry_Atime( e );
            ctime : constant time_t := Al_Get_Fs_Entry_Ctime( e );
            mtime : constant time_t := Al_Get_Fs_Entry_Mtime( e );
            size  : constant off_t := Al_Get_Fs_Entry_Size( e );
            name  : constant String := Al_Get_Fs_Entry_Name( e );
        begin
            Log_Print( Pad( name, 36 ) & ' ' &
                       Attrib( (mode and ALLEGRO_FILEMODE_READ)    > 0, 'r', '.' ) &
                       Attrib( (mode and ALLEGRO_FILEMODE_WRITE)   > 0, 'w', '.' ) &
                       Attrib( (mode and ALLEGRO_FILEMODE_EXECUTE) > 0, 'x', '.' ) &
                       Attrib( (mode and ALLEGRO_FILEMODE_HIDDEN)  > 0, 'h', '.' ) &
                       Attrib( (mode and ALLEGRO_FILEMODE_ISFILE)  > 0, 'f', '.' ) &
                       Attrib( (mode and ALLEGRO_FILEMODE_ISDIR)   > 0, 'd', '.' ) & ' ' &
                       Pad( Unsigned_32'Image( Unsigned_32(now - ctime) ), 10 ) & ' ' &
                       Pad( Unsigned_32'Image( Unsigned_32(now - atime) ), 10 ) & ' ' &
                       Pad( Unsigned_32'Image( Unsigned_32(now - mtime) ), 10 ) & ' ' &
                       Pad( Unsigned_32'Image( Unsigned_32(size) ), 8 ) );
        end Print_File;

        ------------------------------------------------------------------------

        procedure Print_Entry( e : A_Allegro_Fs_Entry ) is
            next : A_Allegro_Fs_Entry;
        begin
            Print_File( e );

            if (Al_Get_Fs_Entry_Mode( e ) and ALLEGRO_FILEMODE_ISDIR) = 0 then
                return;
            end if;

            if not Al_Open_Directory( e ) then
                Log_Print( "Error opening directory: " & Al_Get_Fs_Entry_Name( e ) );
                return;
            end if;

            loop
                next := Al_Read_Directory( e );
                exit when next = null;

                Print_Entry( next );
                Al_Destroy_Fs_Entry( next );
            end loop;

            if Al_Close_Directory( e ) then
                null;
            end if;
        end Print_Entry;

        ------------------------------------------------------------------------

        function Print_Fs_Entry_Cb( entrie : A_Allegro_Fs_Entry; extra : Address ) return Allegro_For_Each_Fs_Entry_Result is
            pragma Unreferenced( extra );
        begin
            Print_File( entrie );
            return ALLEGRO_FOR_EACH_FS_ENTRY_OK;
        end Print_Fs_Entry_Cb;

        ------------------------------------------------------------------------

        function Print_Fs_Entry_Cb_Norecurse( entrie : A_Allegro_Fs_Entry; extra : Address ) return Allegro_For_Each_Fs_Entry_Result is
            pragma Unreferenced( extra );
        begin
            Print_File( entrie );
            return ALLEGRO_FOR_EACH_FS_ENTRY_SKIP;
        end Print_Fs_Entry_Cb_Norecurse;

        ------------------------------------------------------------------------

        procedure Print_Fs_Entry( dir : A_Allegro_Fs_Entry )  is
            str    : constant String := Al_Get_Fs_Entry_Name( dir );
            result : Allegro_For_Each_Fs_Entry_Result;
            pragma Warnings( Off, result );
        begin
            Log_Print( "------------------------------------" );
            Log_Print( "Example of al_for_each_fs_entry with recursion:" );
            result := Al_For_Each_Fs_Entry( dir, Print_Fs_Entry_Cb'Access, str(str'First)'Address );
        end Print_Fs_Entry;

        ------------------------------------------------------------------------

        procedure Print_Fs_Entry_Norecurse( dir : A_Allegro_Fs_Entry ) is
            str    : constant String := Al_Get_Fs_Entry_Name( dir );
            result : Allegro_For_Each_Fs_Entry_Result;
            pragma Warnings( Off, result );
        begin
            Log_Print( "------------------------------------" );
            Log_Print( "Example of al_for_each_fs_entry without recursion:" );
            result := Al_For_Each_Fs_Entry( dir, Print_Fs_Entry_Cb_Norecurse'Access, str(str'First)'Address );
        end Print_Fs_Entry_Norecurse;

        ------------------------------------------------------------------------

        e : A_Allegro_Fs_Entry;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        Open_Log_Monospace;

        Log_Print( "Example of filesystem entry functions:" );
        Log_Print( Pad( "name", 36 ) & ' ' &
                   Pad( "flags", 6 ) & ' ' &
                   Pad( "ctime", 10 ) & ' ' &
                   Pad( "mtime", 10 ) & ' ' &
                   Pad( "atime", 10 ) & ' ' &
                   Pad( "size", 8 )  & ' ' );

        Log_Print( "------------------------------------ " &
                   "------ " &
                   "---------- " &
                   "---------- " &
                   "---------- " &
                   "-------- " );

        if Argument_Count = 0 then
            e := Al_Create_Fs_Entry( "data" );
            Print_Entry( e );
            Print_Fs_Entry( e );
            Print_Fs_Entry_Norecurse( e );
            Al_Destroy_Fs_Entry( e );
        else
            Print_Fs_Entry( e );

            for i in 1..Argument_Count loop
                e := Al_Create_Fs_Entry( Argument(i) );
                Print_Entry( e );
                Print_Fs_Entry( e );
                Print_Fs_Entry_Norecurse( e );
                Al_Destroy_Fs_Entry( e );
            end loop;
        end if;

        Close_log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Dir;
