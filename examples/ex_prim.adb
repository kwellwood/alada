
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

--         ______   ___    ___
--        /\  _  \ /\_ \  /\_ \
--        \ \ \L\ \\//\ \ \//\ \      __     __   _ __   ___
--         \ \  __ \ \ \ \  \ \ \   /'__`\ /'_ `\/\`'__\/ __`\
--          \ \ \/\ \ \_\ \_ \_\ \_/\  __//\ \L\ \ \ \//\ \L\ \
--           \ \_\ \_\/\____\/\____\ \____\ \____ \ \_\\ \____/
--            \/_/\/_/\/____/\/____/\/____/\/___L\ \/_/ \/___/
--                                           /\____/
--                                           \_/__/
--
--      A sampler of the primitive addon.
--      All primitives are rendered using the additive blender, so overdraw will
--      manifest itself as overly bright pixels.
--
--
--      By Pavel Sountsov.
--
--      See readme.txt for copyright information.
--
package body Ex_Prim is

    procedure Ada_Main is

        screenW : constant Integer := 800;
        screenH : constant Integer := 600;

        ROTATE_SPEED : constant := 0.0001;

        font         : A_Allegro_Font;
        identity     : Allegro_Transform;
        buffer,
        texture      : A_Allegro_Bitmap;
        solid_white  : Allegro_Color;

        soft         : Boolean := False;
        blend        : Boolean := True;
        speed        : Float := ROTATE_SPEED;
        theta        : Float;
        background   : Boolean := True;
        thickness    : Float := 0.0;
        MainTrans    : Allegro_Transform;

        ------------------------------------------------------------------------

        package Screens is

            type Int_Array is array (Integer range <>) of Integer;

            type Custom_Vertex is
                record
                    color : Allegro_Color;        -- 0..15
                    u, v  : Interfaces.C.short;   -- 16..17, 18..19
                    x, y  : Interfaces.C.short;   -- 20..21, 22..23
                    junk  : Int_Array(0..5);      -- 24..47
                end record;
            pragma Convention( C, Custom_Vertex );

            type Custom_Vertex_Array is array (Integer range <>) of Custom_Vertex;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            type Screen is interface;
            type A_Screen is access all Screen'Class;

            procedure Init( this : access Screen ) is abstract;
            procedure Logic( this : access Screen ) is abstract;
            procedure Draw( this : access Screen ) is abstract;
            procedure Deinit( this : access Screen ) is null;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            type CustomVertexFormatPrimitives is new Screen with
                record
                    vtx  : Custom_Vertex_Array(0..3);
                    decl : A_Allegro_Vertex_Decl;
                end record;

            procedure Init( this : access CustomVertexFormatPrimitives );
            procedure Logic( this : access CustomVertexFormatPrimitives );
            procedure Draw( this : access CustomVertexFormatPrimitives );

            type TexturePrimitives is new Screen with
                record
                    vtx  : Allegro_Vertex_Array(0..12);
                    vtx2 : Allegro_Vertex_Array(0..12);
                end record;

            procedure Init( this : access TexturePrimitives );
            procedure Logic( this : access TexturePrimitives );
            procedure Draw( this : access TexturePrimitives );

            type FilledTexturePrimitives is new Screen with
                record
                    vtx : Allegro_Vertex_Array(0..20);
                end record;

            procedure Init( this : access FilledTexturePrimitives );
            procedure Logic( this : access FilledTexturePrimitives );
            procedure Draw( this : access FilledTexturePrimitives );

            type FilledPrimitives is new Screen with
                record
                    vtx : Allegro_Vertex_Array(0..20);
                end record;

            procedure Init( this : access FilledPrimitives );
            procedure Logic( this : access FilledPrimitives );
            procedure Draw( this : access FilledPrimitives );

            type IndexedFilledPrimitives is new Screen with
                record
                    vtx      : Allegro_Vertex_Array(0..20);
                    indices1 : Vertex_Index_Array(0..5) := (12, 13, 14, 16, 17, 18);
                    indices2 : Vertex_Index_Array(0..5) := (6, 7, 8, 9, 10, 11);
                    indices3 : Vertex_Index_Array(0..5) := (0, 1, 2, 3, 4, 5);
                end record;

            procedure Init( this : access IndexedFilledPrimitives );
            procedure Logic( this : access IndexedFilledPrimitives );
            procedure Draw( this : access IndexedFilledPrimitives );

            type HighPrimitives is new Screen with null record;

            procedure Init( this : access HighPrimitives );
            procedure Logic( this : access HighPrimitives );
            procedure Draw( this : access HighPrimitives );

            type HighFilledPrimitives is new Screen with null record;

            procedure Init( this : access HighFilledPrimitives );
            procedure Logic( this : access HighFilledPrimitives );
            procedure Draw( this : access HighFilledPrimitives );

            type TransformationsPrimitives is new Screen with null record;

            procedure Init( this : access TransformationsPrimitives );
            procedure Logic( this : access TransformationsPrimitives );
            procedure Draw( this : access TransformationsPrimitives );

            type LowPrimitives is new Screen with
                record
                    vtx  : Allegro_Vertex_Array(0..12);
                    vtx2 : Allegro_Vertex_Array(0..12);
                end record;

            procedure Init( this : access LowPrimitives );
            procedure Logic( this : access LowPrimitives );
            procedure Draw( this : access LowPrimitives );

            type IndexedPrimitives is new Screen with
                record
                    indices1 : Vertex_Index_Array(0..3) := (0, 1, 3, 4);
                    indices2 : Vertex_Index_Array(0..3) := (5, 6, 7, 8);
                    indices3 : Vertex_Index_Array(0..3) := (9, 10, 11, 12);
                    vtx      : Allegro_Vertex_Array(0..12);
                    vtx2     : Allegro_Vertex_Array(0..12);
                end record;

            procedure Init( this : access IndexedPrimitives );
            procedure Logic( this : access IndexedPrimitives );
            procedure Draw( this : access IndexedPrimitives );

            type VertexBuffers is new Screen with
                record
                    vtx      : Allegro_Vertex_Array(0..12);
                    vtx2     : Allegro_Vertex_Array(0..12);
                    vbuff    : A_Allegro_Vertex_Buffer;
                    vbuff2   : A_Allegro_Vertex_Buffer;
                    no_soft  : Boolean := False;
                    no_soft2 : Boolean := False;
                end record;

            procedure Init( this : access VertexBuffers );
            procedure Logic( this : access VertexBuffers );
            procedure Draw( this : access VertexBuffers );
            procedure Deinit( this : access VertexBuffers );

            type IndexedBuffers is new Screen with
                record
                    vbuff : A_Allegro_Vertex_Buffer;
                    ibuff : A_Allegro_Index_Buffer;
                    soft  : Boolean := True;
                end record;

            procedure Init( this : access IndexedBuffers );
            procedure Logic( this : access IndexedBuffers );
            procedure Draw( this : access IndexedBuffers );
            procedure Deinit( this : access IndexedBuffers );

        end Screens;

        ------------------------------------------------------------------------

        package body Screens is

            procedure Init( this : access CustomVertexFormatPrimitives ) is
                elems : constant Allegro_Vertex_Element_Array :=
                    (
                        (ALLEGRO_PRIM_POSITION,        ALLEGRO_PRIM_SHORT_2,       20),
                        (ALLEGRO_PRIM_TEX_COORD_PIXEL, ALLEGRO_PRIM_SHORT_2,       16),
                        (ALLEGRO_PRIM_COLOR_ATTR,      ALLEGRO_PRIM_UNUSED,   0)
                    );
                x, y  : Float;
            begin
                this.decl := Al_Create_Vertex_Decl( elems, Custom_Vertex'Size / 8 );
                for ii in this.vtx'Range loop
                    x := 200.0 * cos( Float(ii) / 4.0 * 2.0 * ALLEGRO_PI );
                    y := 200.0 * sin( Float(ii) / 4.0 * 2.0 * ALLEGRO_PI );

                    this.vtx(ii).x := short(x);
                    this.vtx(ii).y := short(y);
                    this.vtx(ii).u := short(64.0 * x / 100.0);
                    this.vtx(ii).v := short(64.0 * y / 100.0);
                    this.vtx(ii).color := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
                end loop;
            end Init;

            procedure Logic( this : access CustomVertexFormatPrimitives ) is
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access CustomVertexFormatPrimitives ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;
                Al_Use_Transform( MainTrans );
                Al_Draw_Prim( this.vtx'Address, this.decl, Texture, 0, 4, ALLEGRO_PRIM_TRIANGLE_FAN );
                Al_Use_Transform( identity );
            end Draw;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            procedure Init( this : access TexturePrimitives ) is
                color : Allegro_Color;
                x, y  : Float;
            begin
                for ii in this.vtx'Range loop
                    x := 200.0 * cos( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );
                    y := 200.0 * sin( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );

                    color := Al_Map_RGB( Unsigned_8((ii + 1) mod 3 * 64),
                                         Unsigned_8((ii + 2) mod 3 * 64),
                                         Unsigned_8((ii) mod 3 * 64) );

                    this.vtx(ii).x := x;
                    this.vtx(ii).y := y;
                    this.vtx(ii).z := 0.0;
                    this.vtx2(ii).x := 0.1 * x;
                    this.vtx2(ii).y := 0.1 * y;
                    this.vtx(ii).u := 64.0 * x / 100.0;
                    this.vtx(ii).v := 64.0 * y / 100.0;
                    this.vtx2(ii).u := 64.0 * x / 100.0;
                    this.vtx2(ii).v := 64.0 * y / 100.0;
                    if ii < 10 then
                        this.vtx(ii).color := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
                    else
                        this.vtx(ii).color := color;
                    end if;
                    this.vtx2(ii).color := this.vtx(ii).color;
                end loop;
            end Init;

            procedure Logic( this : access TexturePrimitives ) is
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access TexturePrimitives ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Prim( this.vtx, Texture, 0, 4, ALLEGRO_PRIM_LINE_LIST );
                Al_Draw_Prim( this.vtx, Texture, 4, 9, ALLEGRO_PRIM_LINE_STRIP );
                Al_Draw_Prim( this.vtx, Texture, 9, 13, ALLEGRO_PRIM_LINE_LOOP );
                Al_Draw_Prim( this.vtx2, Texture, 0, 13, ALLEGRO_PRIM_POINT_LIST );

                Al_Use_Transform( identity );
            end Draw;

            -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

            procedure Init( this : access FilledTexturePrimitives ) is
                x, y  : Float;
                color : Allegro_Color;
            begin
                for ii in this.vtx'Range loop
                    if ii mod 2 = 0 then
                        x := 150.0 * cos( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                        y := 150.0 * sin( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                    else
                        x := 200.0 * cos( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                        y := 200.0 * sin( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                    end if;

                    if ii = 0 then
                        x := 0.0;
                        y := 0.0;
                    end if;

                    color := Al_Map_RGB( Unsigned_8((7 * ii + 1) mod 3 * 64),
                                         Unsigned_8((2 * ii + 2) mod 3 * 64),
                                         Unsigned_8((ii) mod 3 * 64) );

                    this.vtx(ii).x := x;
                    this.vtx(ii).y := y;
                    this.vtx(ii).z := 0.0;
                    this.vtx(ii).u := 64.0 * x / 100.0;
                    this.vtx(ii).v := 64.0 * y / 100.0;
                    if ii < 10 then
                        this.vtx(ii).color := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
                    else
                        this.vtx(ii).color := color;
                    end if;
                end loop;
            end Init;

            procedure Logic( this : access FilledTexturePrimitives ) is
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access FilledTexturePrimitives ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Prim( this.vtx, Texture, 0, 6, ALLEGRO_PRIM_TRIANGLE_FAN );
                Al_Draw_Prim( this.vtx, Texture, 7, 13, ALLEGRO_PRIM_TRIANGLE_LIST );
                Al_Draw_Prim( this.vtx, Texture, 14, 20, ALLEGRO_PRIM_TRIANGLE_STRIP );

                Al_Use_Transform( identity );
            end Draw;

            --------------------------------------------------------------------

            procedure Init( this : access FilledPrimitives ) is
                color : Allegro_Color;
                x, y  : Float;
            begin
                for ii in this.vtx'Range loop
                    if ii mod 2 = 0 then
                        x := 150.0 * cos( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                        y := 150.0 * sin( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                    else
                        x := 200.0 * cos( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                        y := 200.0 * sin( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                    end if;

                    if ii = 0 then
                        x := 0.0;
                        y := 0.0;
                    end if;

                    color := Al_Map_RGB( Unsigned_8((7 * ii + 1) mod 3 * 64),
                                         Unsigned_8((2 * ii + 2) mod 3 * 64),
                                         Unsigned_8((ii) mod 3 * 64) );

                    this.vtx(ii).x := x;
                    this.vtx(ii).y := y;
                    this.vtx(ii).z := 0.0;
                    this.vtx(ii).color := color;
                 end loop;
            end Init;

            procedure Logic( this : access FilledPrimitives ) is
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access FilledPrimitives ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Prim( this.vtx, null, 0, 6, ALLEGRO_PRIM_TRIANGLE_FAN );
                Al_Draw_Prim( this.vtx, null, 7, 13, ALLEGRO_PRIM_TRIANGLE_LIST );
                Al_Draw_Prim( this.vtx, null, 14, 20, ALLEGRO_PRIM_TRIANGLE_STRIP );

                Al_Use_Transform( identity );
            end Draw;

            --------------------------------------------------------------------

            procedure Init( this : access IndexedFilledPrimitives ) is
                x, y  : Float;
                color : Allegro_Color;
            begin
                for ii in this.vtx'Range loop
                    if ii mod 2 = 0 then
                        x := 150.0 * cos( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                        y := 150.0 * sin( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                    else
                        x := 200.0 * cos( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                        y := 200.0 * sin( Float(ii) / 20.0 * 2.0 * ALLEGRO_PI );
                    end if;

                    if ii = 0 then
                        x := 0.0;
                        y := 0.0;
                    end if;

                    color := Al_Map_RGB( Unsigned_8((7 * ii + 1) mod 3 * 64),
                                         Unsigned_8((2 * ii + 2) mod 3 * 64),
                                         Unsigned_8((ii) mod 3 * 64) );

                    this.vtx(ii).x := x;
                    this.vtx(ii).y := y;
                    this.vtx(ii).z := 0.0;
                    this.vtx(ii).color := color;
                end loop;
            end Init;

            procedure Logic( this : access IndexedFilledPrimitives ) is
            begin
                theta := theta + speed;
                for ii in 0..5 loop
                    this.indices1(ii) := (Integer(Al_Get_Time) + ii) mod 20 + 1;
                    this.indices2(ii) := (Integer(Al_Get_Time) + ii + 6) mod 20 + 1;
                    if ii > 0 then
                        this.indices3(ii) := (Integer(Al_Get_Time) + ii + 12) mod 20 + 1;
                    end if;
                end loop;

                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access IndexedFilledPrimitives ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Indexed_Prim( this.vtx, null, this.indices1, ALLEGRO_PRIM_TRIANGLE_LIST );
                Al_Draw_Indexed_Prim( this.vtx, null, this.indices2, ALLEGRO_PRIM_TRIANGLE_STRIP );
                Al_Draw_Indexed_Prim( this.vtx, null, this.indices3, ALLEGRO_PRIM_TRIANGLE_FAN );

                Al_Use_Transform( identity );
            end Draw;

            --------------------------------------------------------------------

            procedure Init( this : access HighPrimitives ) is
                pragma Unreferenced( this );
            begin
                null;
            end Init;

            procedure Logic( this : access HighPrimitives ) is
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access HighPrimitives ) is
                points : constant Point_2D_Array(0..3) := ((-300.0, -200.0),
                                                           ( 700.0,  200.0),
                                                           (-700.0,  200.0),
                                                           ( 300.0, -200.0));
                pragma Unreferenced( this );
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Line( -300.0, -200.0, 300.0, 200.0, Al_Map_RGBA_f( 0.0, 0.5, 0.5, 1.0 ), thickness );
                Al_Draw_Triangle( -150.0, -250.0, 0.0, 250.0, 150.0, -250.0, Al_Map_RGBA_f( 0.5, 0.0, 0.5, 1.0 ), thickness );
                Al_Draw_Rectangle( -300.0, -200.0, 300.0, 200.0, Al_Map_RGBA_f( 0.5, 0.0, 0.0, 1.0 ), thickness );
                Al_Draw_Rounded_Rectangle( -200.0, -125.0, 200.0, 125.0, 50.0, 100.0, Al_Map_RGBA_f( 0.2, 0.2, 0.0, 1.0 ), thickness );

                Al_Draw_Ellipse( 0.0, 0.0, 300.0, 150.0, Al_Map_RGBA_f( 0.0, 0.5, 0.5, 1.0 ), thickness );
                Al_Draw_Elliptical_Arc( -20.0, 0.0, 300.0, 200.0, -ALLEGRO_PI / 2.0, -ALLEGRO_PI, Al_Map_RGBA_f( 0.25, 0.25, 0.5, 1.0 ), thickness );
                Al_Draw_Arc( 0.0, 0.0, 200.0, -ALLEGRO_PI / 2.0, ALLEGRO_PI, Al_Map_RGBA_f( 0.5, 0.25, 0.0, 1.0 ), thickness );
                Al_Draw_Spline( points, Al_Map_RGBA_f( 0.1, 0.2, 0.5, 1.0 ), thickness );
                Al_Draw_Pieslice( 0.0, 25.0, 150.0, ALLEGRO_PI * 3.0 / 4.0, -ALLEGRO_PI / 2.0, Al_Map_RGBA_f( 0.4, 0.3, 0.1, 1.0 ), thickness );

                Al_Use_Transform( identity );
            end Draw;

            --------------------------------------------------------------------

            procedure Init( this : access HighFilledPrimitives ) is
                pragma Unreferenced( this );
            begin
                null;
            end Init;

            procedure Logic( this : access HighFilledPrimitives ) is
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access HighFilledPrimitives ) is
                pragma Unreferenced( this );
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Filled_Triangle( -100.0, -100.0, -150.0, 200.0, 100.0, 200.0, Al_Map_RGB_f( 0.5, 0.7, 0.3 ) );
                Al_Draw_Filled_Rectangle( 20.0, -50.0, 200.0, 50.0, Al_Map_RGB_f( 0.3, 0.2, 0.6 ) );
                Al_Draw_Filled_Rounded_Rectangle( 50.0, -250.0, 350.0, -75.0, 50.0, 70.0, Al_Map_RGB_f( 0.4, 0.2, 0.0 ) );
                Al_Draw_Filled_Pieslice( 200.0, 125.0, 50.0, ALLEGRO_PI / 4.0, 3.0 * ALLEGRO_PI / 2.0, Al_Map_RGB_f( 0.3, 0.3, 0.1 ) );

                Al_Use_Transform( identity );
            end Draw;

            --------------------------------------------------------------------

            procedure Init( this : access TransformationsPrimitives ) is
                pragma Unreferenced( this );

            begin
               null;
            end Init;

            procedure Logic( this : access TransformationsPrimitives ) is
                t : constant Long_Float := Al_Get_Time;
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans,
                                    Float(screenW / 2), Float(screenH / 2),
                                    sin( Float(t) / 5.0 ), cos( Float(t) / 5.0 ),
                                    theta );
            end Logic;

            procedure Draw( this : access TransformationsPrimitives ) is
                points : constant Point_2D_Array(0..3) := ((-300.0, -200.0),
                                                           ( 700.0,  200.0),
                                                           (-700.0,  200.0),
                                                           ( 300.0, -200.0));
                pragma Unreferenced( this );
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Line( -300.0, -200.0, 300.0, 200.0, Al_Map_RGBA_f( 0.0, 0.5, 0.5, 1.0 ), thickness );
                Al_Draw_Triangle( -150.0, -250.0, 0.0, 250.0, 150.0, -250.0, Al_Map_RGBA_f( 0.5, 0.0, 0.5, 1.0 ), thickness );
                Al_Draw_Rectangle( -300.0, -200.0, 300.0, 200.0, Al_Map_RGBA_f( 0.5, 0.0, 0.0, 1.0 ), thickness );
                Al_Draw_Rounded_Rectangle( -200.0, -125.0, 200.0, 125.0, 50.0, 100.0, Al_Map_RGBA_f( 0.2, 0.2, 0.0, 1.0 ), thickness );

                Al_Draw_Ellipse( 0.0, 0.0, 300.0, 150.0, Al_Map_RGBA_f( 0.0, 0.5, 0.5, 1.0 ), thickness );
                Al_Draw_Elliptical_Arc( -20.0, 0.0, 300.0, 200.0, -ALLEGRO_PI / 2.0, -ALLEGRO_PI, Al_Map_RGBA_f( 0.25, 0.25, 0.5, 1.0 ), thickness );
                Al_Draw_Arc( 0.0, 0.0, 200.0, -ALLEGRO_PI / 2.0, ALLEGRO_PI, Al_Map_RGBA_f( 0.5, 0.25, 0.0, 1.0 ), thickness );
                Al_Draw_Spline( points, Al_Map_RGBA_f( 0.1, 0.2, 0.5, 1.0 ), thickness );
                Al_Draw_Pieslice( 0.0, 25.0, 150.0, ALLEGRO_PI * 3.0 / 4.0, -ALLEGRO_PI / 2.0, Al_Map_RGBA_f( 0.4, 0.3, 0.1, 1.0 ), thickness );

                Al_Use_Transform( identity );
            end Draw;

            --------------------------------------------------------------------

            procedure Init( this : access LowPrimitives ) is
                x, y  : Float;
                color : Allegro_Color;
            begin
                for ii in this.vtx'Range loop
                    x := 200.0 * cos( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );
                    y := 200.0 * sin( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );

                    color := Al_Map_RGB( Unsigned_8((ii + 1) mod 3 * 64),
                                         Unsigned_8((ii + 2) mod 3 * 64),
                                         Unsigned_8((ii) mod 3 * 64) );

                    this.vtx(ii).x := x;
                    this.vtx(ii).y := y;
                    this.vtx(ii).z := 0.0;
                    this.vtx2(ii).x := 0.1 * x;
                    this.vtx2(ii).y := 0.1 * y;
                    this.vtx(ii).color := color;
                    this.vtx2(ii).color := color;
                end loop;
            end Init;

            procedure Logic( this : access LowPrimitives ) is
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access LowPrimitives ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Prim( this.vtx, null, 0, 4, ALLEGRO_PRIM_LINE_LIST );
                Al_Draw_Prim( this.vtx, null, 4, 9, ALLEGRO_PRIM_LINE_STRIP );
                Al_Draw_Prim( this.vtx, null, 9, 13, ALLEGRO_PRIM_LINE_LOOP );
                Al_Draw_Prim( this.vtx2, null, 0, 13, ALLEGRO_PRIM_POINT_LIST );

                Al_Use_Transform( identity );
            end Draw;

            --------------------------------------------------------------------

            procedure Init( this : access IndexedPrimitives ) is
                x, y  : Float;
                color : Allegro_Color;
            begin
                for ii in this.vtx'Range loop
                    x := 200.0 * cos( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );
                    y := 200.0 * sin( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );

                    color := Al_Map_RGB( Unsigned_8((ii + 1) mod 3 * 64),
                                         Unsigned_8((ii + 2) mod 3 * 64),
                                         Unsigned_8((ii) mod 3 * 64) );

                    this.vtx(ii).x := x;
                    this.vtx(ii).y := y;
                    this.vtx(ii).z := 0.0;
                    this.vtx2(ii).x := 0.1 * x;
                    this.vtx2(ii).y := 0.1 * y;
                    this.vtx(ii).color := color;
                    this.vtx2(ii).color := color;
                end loop;
            end Init;

            procedure Logic( this : access IndexedPrimitives ) is
            begin
                theta := theta + speed;
                for ii in 0..3 loop
                    this.indices1(ii) := (Integer(Al_Get_Time) + ii) mod 13;
                    this.indices2(ii) := (Integer(Al_Get_Time) + ii + 4) mod 13;
                    this.indices3(ii) := (Integer(Al_Get_Time) + ii + 8) mod 13;
                end loop;

                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access IndexedPrimitives ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                Al_Draw_Indexed_Prim( this.vtx, null, this.indices1, ALLEGRO_PRIM_LINE_LIST );
                Al_Draw_Indexed_Prim( this.vtx, null, this.indices2, ALLEGRO_PRIM_LINE_STRIP );
                Al_Draw_Indexed_Prim( this.vtx, null, this.indices3, ALLEGRO_PRIM_LINE_LOOP );
                Al_Draw_Indexed_Prim( this.vtx2, null, this.indices3, ALLEGRO_PRIM_POINT_LIST );

                Al_Use_Transform( identity );
            end Draw;

            --------------------------------------------------------------------

            procedure Init( this : access VertexBuffers ) is
                color : Allegro_Color;
                x, y  : Float;
            begin
                for ii in this.vtx'Range loop
                    x := 200.0 * cos( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );
                    y := 200.0 * sin( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );

                    color := Al_Map_RGB( Unsigned_8(((ii + 1) mod 3) * 64),
                                         Unsigned_8(((ii + 2) mod 3) * 64),
                                         Unsigned_8((ii mod 3) * 64) );

                    this.vtx(ii).x := x; this.vtx(ii).y := y; this.vtx(ii).z := 0.0;
                    this.vtx2(ii).x := 0.1 * x; this.vtx2(ii).y := 0.1 * y;
                    this.vtx(ii).color := color;
                    this.vtx2(ii).color := color;
                end loop;

                this.vbuff := Al_Create_Vertex_Buffer( this.vtx, ALLEGRO_PRIM_BUFFER_READWRITE );
                if this.vbuff = null then
                    this.vbuff := Al_Create_Vertex_Buffer( this.vtx, 0 );
                    this.no_soft := True;
                else
                    this.no_soft := False;
                end if;

                this.vbuff2 := Al_Create_Vertex_Buffer( this.vtx2, ALLEGRO_PRIM_BUFFER_READWRITE );
                if this.vbuff2 = null then
                    this.vbuff2 := Al_Create_Vertex_Buffer( this.vtx2, 0 );
                    this.no_soft2 := True;
                else
                    this.no_soft2 := False;
                end if;
            end Init;

            procedure Logic( this : access VertexBuffers ) is
                pragma Unreferenced( this );
            begin
                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access VertexBuffers ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                if this.vbuff /= null and then not (soft and then this.no_soft) then
                    Al_Draw_Vertex_Buffer( this.vbuff, 0, 4, ALLEGRO_PRIM_LINE_LIST );
                    Al_Draw_Vertex_Buffer( this.vbuff, 4, 9, ALLEGRO_PRIM_LINE_STRIP );
                    Al_Draw_Vertex_Buffer( this.vbuff, 9, 13, ALLEGRO_PRIM_LINE_LOOP );
                else
                    Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 0.0, -40.0, 0, "Vertex buffers not supported" );
                end if;

                if this.vbuff2 /= null and then not (soft and then this.no_soft2) then
                    Al_Draw_Vertex_Buffer( this.vbuff2, 0, 13, ALLEGRO_PRIM_POINT_LIST );
                else
                    Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 0.0, -40.0, 0, "Vertex buffers not supported" );
                end if;

                Al_Use_Transform( identity );
            end Draw;

            procedure Deinit( this : access VertexBuffers ) is
            begin
                Al_Destroy_Vertex_Buffer( this.vbuff );
                Al_Destroy_Vertex_Buffer( this.vbuff2 );
            end Deinit;

            --------------------------------------------------------------------

            procedure Init( this : access IndexedBuffers ) is
                use Locked_Vertex_Arrays;
                flags : Allegro_Prim_Buffer_Flags := ALLEGRO_PRIM_BUFFER_READWRITE;
                x, y  : Float;
                color : Allegro_Color;
                vtx   : Locked_Vertex_Ptr;
            begin
                this.vbuff := Al_Create_Vertex_Buffer( 13, ALLEGRO_PRIM_BUFFER_READWRITE );
                if this.vbuff = null then
                    this.vbuff := Al_Create_Vertex_Buffer( 13, 0 );
                    this.soft := False;
                    flags := 0;
                end if;

                this.ibuff := Al_Create_Index_Buffer( 2, 8, flags );

                if this.vbuff /= null then
                    vtx := Al_Lock_Vertex_Buffer( this.vbuff, 0, 13, ALLEGRO_LOCK_WRITEONLY );
                    for ii in 0..12 loop
                        x := 200.0 * cos( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );
                        y := 200.0 * sin( Float(ii) / 13.0 * 2.0 * ALLEGRO_PI );

                        color := Al_Map_RGB( Unsigned_8(((ii + 1) mod 3) * 64),
                                             Unsigned_8(((ii + 2) mod 3) * 64),
                                             Unsigned_8((ii mod 3) * 64) );

                        vtx.x := x;
                        vtx.y := y;
                        vtx.z := 0.0;
                        vtx.color := color;
                        Increment( vtx );
                    end loop;

                    Al_Unlock_Vertex_Buffer( this.vbuff );
                end if;
            end Init;

            procedure Logic( this : access IndexedBuffers ) is
                use Locked_Short_Arrays;
                t     : constant Integer := Integer(Al_Get_Time);
                index : Locked_Short_Ptr;
            begin
                if this.ibuff /= null then
                    index := Al_Lock_Index_Buffer( this.ibuff, 0, 8, ALLEGRO_LOCK_WRITEONLY );
                    if index /= null then
                        for ii in 0..7 loop
                            index.all := Integer_16((t + ii) mod 13);
                            Increment( index );
                        end loop;
                    end if;
                end if;

                theta := theta + speed;
                Al_Build_Transform( MainTrans, Float(screenW / 2), Float(screenH / 2), 1.0, 1.0, theta );
            end Logic;

            procedure Draw( this : access IndexedBuffers ) is
            begin
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                else
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                end if;

                Al_Use_Transform( MainTrans );

                if not (soft and not this.soft) and then this.vbuff /= null and then this.ibuff /= null then
                    Al_Draw_Indexed_Buffer( this.vbuff, NULL, this.ibuff, 0, 4, ALLEGRO_PRIM_LINE_LIST );
                    Al_Draw_Indexed_Buffer( this.vbuff, NULL, this.ibuff, 4, 8, ALLEGRO_PRIM_LINE_STRIP );
                else
                    Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 0.0, 0.0, 0, "Indexed buffers not supported" );
                end if;

                Al_Use_Transform( identity );
            end Draw;

            procedure Deinit( this : access IndexedBuffers ) is
            begin
                Al_Destroy_Vertex_Buffer( this.vbuff );
                Al_Destroy_Index_Buffer( this.ibuff );
            end Deinit;

        end Screens;

        ------------------------------------------------------------------------

        type Screen_Array is array (Natural range <>) of Screens.A_Screen;

        type String_Array is array (Natural range <>) of access String;

        Screen     : Screen_Array(0..11) := (others => null);
        ScreenName : constant String_Array(0..11) := String_Array'(new String'("Low Level Primitives"),
                                                                   new String'("Indexed Primitives"),
                                                                   new String'("High Level Primitives"),
                                                                   new String'("Transformations"),
                                                                   new String'("Low Level Filled Primitives"),
                                                                   new String'("Indexed Filled Primitives"),
                                                                   new String'("High Level Filled Primitives"),
                                                                   new String'("Textured Primitives"),
                                                                   new String'("Filled Textured Primitives"),
                                                                   new String'("Custom Vertex Format"),
                                                                   new String'("Vertex Buffers"),
                                                                   new String'("Indexed Buffers"));

        ------------------------------------------------------------------------

        refresh_rate   : constant Integer := 60;
        fixed_timestep : constant Long_Float := 1.0 / Long_Float(refresh_rate);

        display        : A_Allegro_Display;
        bkg            : A_Allegro_Bitmap;
        black          : Allegro_Color;
        queue          : A_Allegro_Event_Queue;
        use_shader     : Boolean := False;

        frames_done    : Integer := 0;
        time_diff      : Long_Float;
        real_time      : Long_Float;
        game_time      : Long_Float;
        start_time     : Long_Float;
        cur_screen     : Integer := 0;
        done           : Boolean := False;
        clip           : Boolean := False;
        timer          : A_Allegro_Timer;
        timer_queue    : A_Allegro_Event_Queue;
        key_event      : aliased Allegro_Event;
        old            : Allegro_Bitmap_Flags := 0;
    begin
        if Argument_Count > 0 then
            if Argument( 1 ) = "--shader" then
                use_shader := True;
            else
                Abort_Example( "Invalid command line option: " & Argument( 1 ) );
            end if;
        end if;

        -- Initialize Allegro 5 and addons
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Image_addon then
            Abort_Example( "Could not init image IO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        Init_Platform_Specific;

        if use_shader then
            Al_Set_New_Display_Flags( ALLEGRO_PROGRAMMABLE_PIPELINE );
        end if;

        -- Create a window to display things on: 640x480 pixels
        display := Al_Create_Display( screenW, screenH );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        -- Install the keyboard handler
        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        if not Al_Install_Mouse then
            Abort_Example( "Error installing mouse." );
        end if;

        -- Load a font
        font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        if font = null then
            Abort_Example( "Error loading ""data/fixed_font.tga""." );
        end if;

        solid_white := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );

        bkg := Al_Load_Bitmap( "data/bkg.png" );

        texture := Al_Load_Bitmap( "data/texture.tga" );

        -- Make and set some color to draw with
        black := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 );

        -- Start the event queue to handle keyboard input and our timer
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );

        Al_Set_Window_Title( display, "Primitives Example" );

        time_diff := Al_Get_Time;
        real_time := Al_Get_Time;
        game_time := Al_Get_Time;

        timer := Al_Create_Timer( ALLEGRO_BPS_TO_SECS( Long_Float(refresh_rate) ) );
        Al_Start_Timer( timer );
        timer_queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( timer_queue, Al_Get_Timer_Event_Source( timer ) );

        old := Al_Get_New_Bitmap_Flags;
        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        buffer := Al_Create_Bitmap( screenW, screenH );
        Al_Set_New_Bitmap_Flags( old );

        Al_Identity_Transform( identity );

        screen(0) := new Screens.LowPrimitives;
        screen(1) := new Screens.IndexedPrimitives;
        screen(2) := new Screens.HighPrimitives;
        screen(3) := new Screens.TransformationsPrimitives;
        screen(4) := new Screens.FilledPrimitives;
        screen(5) := new Screens.IndexedFilledPrimitives;
        screen(6) := new Screens.HighFilledPrimitives;
        screen(7) := new Screens.TexturePrimitives;
        screen(8) := new Screens.FilledTexturePrimitives;
        screen(9) := new Screens.CustomVertexFormatPrimitives;
        screen(10) := new Screens.VertexBuffers;
        screen(11) := new Screens.IndexedBuffers;

        for ii in screen'Range loop
            screen(ii).Init;
            screen(ii).Logic;
        end loop;

        while not done loop
            declare
                frame_duration : Long_Float;
            begin
                frame_duration := Al_Get_Time - real_time;
                Al_Rest( fixed_timestep - frame_duration );  -- rest at least fixed_dt
                frame_duration := Al_Get_Time - real_time;
                real_time := Al_Get_Time;

                if real_time - game_time > frame_duration then  -- eliminate excess overflow
                    game_time := game_time + fixed_timestep *
                                 Long_Float'Floor( (real_time - game_time) / fixed_timestep );
                end if;
            end;

            while real_time - game_time >= 0.0 loop
                start_time := Al_Get_Time;
                game_time := game_time + fixed_timestep;

                screen(cur_screen).Logic;

                while Al_Get_Next_Event( queue, key_event'Access ) loop
                    case key_event.any.typ is
                        when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                            cur_screen := cur_screen + 1;
                            if cur_screen > screen'Last then
                                cur_screen := screen'First;
                            end if;

                        when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                            done := True;

                        when ALLEGRO_EVENT_KEY_CHAR =>
                            case key_event.keyboard.keycode is
                                when ALLEGRO_KEY_ESCAPE =>
                                    done := True;
                                when ALLEGRO_KEY_S =>
                                    soft := not soft;
                                    time_diff := Al_Get_Time;
                                    frames_done := 0;
                                when ALLEGRO_KEY_C =>
                                    clip := not clip;
                                    time_diff := Al_Get_Time;
                                    frames_done := 0;
                                when ALLEGRO_KEY_L =>
                                    blend := not blend;
                                    time_diff := Al_Get_Time;
                                    frames_done := 0;
                                when ALLEGRO_KEY_B =>
                                    background := not background;
                                    time_diff := Al_Get_Time;
                                    frames_done := 0;
                                when ALLEGRO_KEY_LEFT =>
                                    speed := speed - ROTATE_SPEED;
                                when ALLEGRO_KEY_RIGHT =>
                                    speed := speed + ROTATE_SPEED;
                                when ALLEGRO_KEY_PGUP =>
                                    thickness := thickness + 0.5;
                                    if thickness > 1.0 then
                                        thickness := 1.0;
                                    end if;
                                when ALLEGRO_KEY_PGDN =>
                                    thickness := thickness - 0.5;
                                    if thickness < 1.0 then
                                        thickness := 0.0;
                                    end if;
                                when ALLEGRO_KEY_UP =>
                                    cur_screen := cur_screen + 1;
                                    if cur_screen > screen'Last then
                                        cur_screen := screen'First;
                                    end if;
                                when ALLEGRO_KEY_SPACE =>
                                    speed := 0.0;
                                when ALLEGRO_KEY_DOWN =>
                                    cur_screen := cur_screen - 1;
                                    if cur_screen < screen'First then
                                        cur_screen := screen'Last;
                                    end if;
                                when others =>
                                    null;
                            end case;

                        when others =>
                            null;
                    end case;
                end loop;

                if Al_Get_Time - start_time >= fixed_timestep then
                    -- break if we start taking too long
                    exit;
                end if;
            end loop;

            Al_Clear_To_Color( black );

            if soft then
                Al_Set_Target_Bitmap( buffer );
                Al_Clear_To_Color( black );
            end if;

            if background and then bkg /= null then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                Al_Draw_Scaled_Bitmap( bkg, 0.0, 0.0,
                                       Float(Al_Get_Bitmap_Width( bkg )),
                                       Float(Al_Get_Bitmap_Height( bkg )),
                                       0.0, 0.0,
                                       Float(screenW), Float(screenH), 0 );
            end if;

            if clip then
                Al_Set_Clipping_Rectangle( screenW / 2, screenH / 2,
                                           screenW / 2, screenH / 2 );
            end if;

            screen(cur_screen).Draw;

            Al_Set_Clipping_Rectangle( 0, 0, screenW, screenH );

            if soft then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                Al_Set_Target_Backbuffer( display );
                Al_Draw_Bitmap( buffer, 0.0, 0.0, 0 );
            end if;

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Text( font, solid_white, Float(screenW / 2), Float(screenH - 20), ALLEGRO_ALIGN_CENTRE, ScreenName(cur_screen).all );
            Al_Draw_Text( font, solid_white, 0.0, 0.0, 0, "FPS:" & Long_Float'Image( Long_Float(frames_done) / (Al_Get_Time - time_diff) ) );
            Al_Draw_Text( font, solid_white, 0.0, 20.0, 0, "Change Screen (Up/Down). Esc to Quit." );
            Al_Draw_Text( font, solid_white, 0.0, 40.0, 0, "Rotation (Left/Right/Space):" & speed'Img );
            Al_Draw_Text( font, solid_white, 0.0, 60.0, 0, "Thickness (PgUp/PgDown):" & thickness'Img );
            Al_Draw_Text( font, solid_white, 0.0, 80.0, 0, "Software (S):" & soft'Img );
            Al_Draw_Text( font, solid_white, 0.0, 100.0, 0, "Blending (L):" & blend'Img );
            Al_Draw_Text( font, solid_white, 0.0, 120.0, 0, "Background (B):" & background'Img );
            Al_Draw_Text( font, solid_white, 0.0, 140.0, 0, "Clip (C):" & clip'Img );

            Al_Flip_Display;
            frames_done := frames_done + 1;
        end loop;

        for ii in screen'Range loop
            screen(ii).Deinit;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Prim;
