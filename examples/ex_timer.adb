
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

-- A test of timer events. Since both al_get_time() as well as the timer
-- events may be the source of inaccuracy, it doesn't tell a lot.
--
package body Ex_Timer is

    procedure Ada_Main is

        type Int_Array is array (Integer range <>) of Integer;

        -- A structure holding all variables of our example program.
        type Example is
            record
                myfont        : A_Allegro_Font; -- Our font.
                queue         : A_Allegro_Event_Queue; -- Our events queue.

                fps           : Long_Float := 0.0; -- How often to update per second.

                x             : Int_Array(0..3) := (others => 0);

                first_tick    : Boolean := False;
                this_time,
                prev_time,
                accum_time    : Long_Float := 0.0;
                min_diff,
                max_diff,
                second_spread : Long_Float := 0.0;
                second        : Long_Float := 0.0;
                timer_events  : Long_Float := 0.0;
                timer_error   : Long_Float := 0.0;
                timestamp     : Long_Float := 0.0;
            end record;

        ex : Example;

        ------------------------------------------------------------------------

        -- Initialize the example.
        procedure Init is
        begin
            ex.fps := 50.0;
            ex.first_tick := True;

            ex.myfont := Al_Create_Builtin_Font;
            if ex.myfont = null then
                Abort_Example( "Error creating builtin font" );
            end if;
        end Init;

        ------------------------------------------------------------------------

        -- Cleanup. Always a good idea.
        procedure Cleanup is
        begin
            Al_Destroy_Font( ex.myfont );
            ex.myfont := null;
        end Cleanup;

        ------------------------------------------------------------------------

        -- Print some text.
        procedure Print( x, y : Float; message : String ) is
        begin
            Al_Draw_Text( ex.myfont, Al_Map_RGB_f( 0.0, 0.0, 0.0 ), x, y, 0, message );
        end Print;

        ------------------------------------------------------------------------

        -- Draw our example scene.
        procedure Draw is
            h, y           : Float;
            cur_time,
            event_overhead,
            total_error    : Long_Float;
        begin
            cur_time := Al_Get_Time;
            event_overhead := cur_time - ex.timestamp;
            total_error := event_overhead + ex.timer_error;

            h := Float(Al_Get_Font_Line_Height( ex.myfont ));
            Al_Clear_To_Color( Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );

            Print( 0.0, 0.0, Long_Float'Image( 1.0 / ex.FPS ) & " target for " & Integer'Image( Integer(ex.fps) ) & " Hz Timer" );
            Print( 0.0, h, Long_Float'Image( ex.this_time - ex.prev_time ) & " now" );
            Print( 0.0, 2.0 * h, Long_Float'Image( ex.accum_time / ex.timer_events ) & " accum over one second" );
            Print( 0.0, 3.0 * h, Long_Float'Image( ex.min_diff ) & " min" );
            Print( 0.0, 4.0 * h, Long_Float'Image( ex.max_diff ) & " max" );
            Print( 300.0, 3.5 * h, Long_Float'Image( ex.second_spread ) & " (max - min)" );
            Print( 300.0, 4.5 * h, Long_Float'Image( ex.timer_error ) & " (timer error)" );
            Print( 300.0, 5.5 * h, Long_Float'Image( event_overhead ) & " (event overhead)" );
            Print( 300.0, 6.5 * h, Long_Float'Image( total_error ) & " (total error)" );

            y := 240.0;
            for i in ex.x'Range loop
                Al_Draw_Filled_Rectangle( Float(ex.x(i)), y + Float(i) * 60.0,
                                          Float(ex.x(i)) + Float(Unsigned_32'(Shift_Left( 1, i ))),
                                          y + Float(i) * 60.0 + 60.0,
                                          Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
            end loop;
        end Draw;

        ------------------------------------------------------------------------

        -- Called a fixed amount of times per second.
        procedure Tick( timer_event : Allegro_Timer_Event ) is
            duration : Long_Float;
        begin
            ex.this_time := Al_Get_Time;

            if ex.first_tick then
                ex.first_tick := False;
            else
                if ex.this_time - ex.second >= 1.0 then
                    ex.second := ex.this_time;
                    ex.accum_time := 0.0;
                    ex.timer_events := 0.0;
                    ex.second_spread := ex.max_diff - ex.min_diff;
                    ex.max_diff := 0.0;
                    ex.min_diff := 1.0;
                end if;
                duration := ex.this_time - ex.prev_time;
                if duration < ex.min_diff then
                    ex.min_diff := duration;
                end if;
                if duration > ex.max_diff then
                    ex.max_diff := duration;
                end if;
                ex.accum_time := ex.accum_time + duration;
                ex.timer_events := ex.timer_events + 1.0;
                ex.timer_error := timer_event.error;
                ex.timestamp := timer_event.timestamp;
            end if;

            Draw;
            Al_Flip_Display;

            for i in ex.x'Range loop
                ex.x(i) := ex.x(i) + Integer(Unsigned_32'(Shift_Left( 1, i )));
                ex.x(i) := ex.x(i) mod 640;
            end loop;

            ex.prev_time := ex.this_time;
        end Tick;

        ------------------------------------------------------------------------

        -- Run our test.
        procedure Run is
            event : Allegro_Event;
        begin
            loop
                Al_Wait_For_Event( ex.queue, event );
                case event.any.typ is
                    -- Was the X button on the window pressed?
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        return;

                    -- Was a key pressed?
                    when ALLEGRO_EVENT_KEY_DOWN =>
                       if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                           return;
                       end if;

                    -- Is it time for the next timer tick?
                    when ALLEGRO_EVENT_TIMER =>
                        tick( event.timer );

                    when others =>
                        null;
                end case;
            end loop;
        end Run;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        Init;

        timer := Al_Create_Timer( 1.000 / ex.FPS );

        ex.queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( ex.queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( ex.queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );
        Run;

        Al_Destroy_Event_Queue( ex.queue );

        Cleanup;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Timer;
