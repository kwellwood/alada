
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Membmp is

    procedure Ada_Main is

        procedure Print( myfont : A_Allegro_Font; message : String; x, y : Float ) is
        begin
            Al_Draw_Text( myfont, Al_Map_RGB( 0, 0, 0 ), x + 2.0, y + 2.0, 0, message );
            Al_Draw_Text( myfont, Al_Map_RGB( 255, 255, 255 ), x, y, 0, message );
        end Print;

        ------------------------------------------------------------------------

        function Test( bitmap  : A_Allegro_Bitmap;
                       font    : A_Allegro_Font;
                       message : String ) return Boolean is
            queue       : A_Allegro_Event_Queue;
            event       : aliased Allegro_Event;
            start_time  : Long_Float;
            frames      : Integer := 0;
            fps         : Long_Float := 0.0;
            quit        : Boolean := False;
        begin
            queue := Al_Create_Event_Queue;
            Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

            start_time := Al_Get_Time;

            loop
                if Al_Get_Next_Event( queue, event'Access ) then
                    if event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                        if event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                            exit;
                        elsif event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            quit := True;
                            exit;
                        end if;
                    end if;
                end if;

                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );

                -- Clear the backbuffer with red so we can tell if the bitmap does not
                -- cover the entire backbuffer.
                --
                Al_Clear_To_Color( Al_Map_RGB( 255, 0, 0 ) );

                Al_Draw_Scaled_Bitmap( bitmap, 0.0, 0.0,
                                       Float(Al_Get_Bitmap_Width( bitmap )),
                                       Float(Al_Get_Bitmap_Height( bitmap )),
                                       0.0, 0.0,
                                       Float(Al_Get_Bitmap_Width( al_get_target_bitmap )),
                                       Float(Al_Get_Bitmap_Height( al_get_target_bitmap )),
                                       0 );

                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );

                -- Note this makes the memory buffer case much slower due to repeated
                -- locking of the backbuffer.  Officially you can't use al_lock_bitmap
                -- to solve the problem either.
                --
                Print( font, message, 0.0, 0.0 );
                print( font, Integer( fps )'Img & " fps", 0.0, Float(Al_Get_Font_Line_Height( font ) + 5) );

                Al_Flip_Display;

                frames := frames + 1;
                fps := Long_Float(frames) / (Al_Get_Time - start_time);
            end loop;

            Al_Destroy_Event_Queue( queue );

            return quit;
        end Test;

        ------------------------------------------------------------------------

       display   : A_Allegro_Display;
       accelfont,
       memfont   : A_Allegro_Font;
       accelbmp,
       membmp    : A_Allegro_Bitmap;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init Allegro IIO." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 400 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        accelfont := Al_Load_Font( "data/font.tga", 0, 0 );
        if accelfont = null then
            Abort_Example( "font.tga not found" );
        end if;
        accelbmp := Al_Load_Bitmap( "data/mysha.pcx" );
        if accelbmp = null then
            Abort_Example( "mysha.pcx not found" );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );

        memfont := Al_Load_Font( "data/font.tga", 0, 0 );
        membmp := Al_Load_Bitmap( "data/mysha.pcx" );

        loop
            if Test( membmp, memfont, "Memory bitmap (press SPACE key)" ) then
                exit;
            end if;
            if Test( accelbmp, accelfont, "Accelerated bitmap (press SPACE key)" ) then
                exit;
            end if;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Membmp;
