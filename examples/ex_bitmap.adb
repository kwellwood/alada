
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Bitmap is

    procedure Ada_Main is
        filename  : Unbounded_String;
        display   : A_Allegro_Display;
        bitmap    : A_Allegro_Bitmap;
        timer     : A_Allegro_Timer;
        queue     : A_Allegro_Event_Queue;
        redraw    : Boolean := True;
        zoom      : Float := 1.0;
        t0, t1    : Long_Float;
        event     : aliased Allegro_Event;
    begin
        -- The first commandline argument can optionally specify an
        -- image to display instead of the default. Allegro's image
        -- addon suports BMP, DDS, PCX, TGA and can be compiled with
        -- PNG and JPG support on all platforms. Additional formats
        -- are supported by platform specific libraries and support for
        -- image formats can also be added at runtime.
        if Argument_Count > 0 then
            filename := To_Unbounded_String( Argument(1) );
        else
            filename := To_Unbounded_String( "data/mysha.pcx" );
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        -- Initializes and displays a log window for debugging purposes.
        Open_Log;

        -- The second parameter to the process can optionally specify what
        -- adapter to use.
        if Argument_Count > 1 then
            Al_Set_New_Display_Adapter( Integer'Value( Argument(2) ) );
        end if;

        -- Allegro requires installing drivers for all input devices before
        -- they can be used.
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        -- Initialize the image addon. Requires the allegro_image addon
        -- library.
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;

        -- Helper functions from common.c.
        Init_Platform_Specific;

        -- Create a new display that we can render the image to.
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Al_Set_Window_Title( display, To_String( filename ) );

        -- Load the image and time how long it took for the log.
        t0 := Al_Get_Time;
        bitmap := Al_Load_Bitmap( To_String( filename ) );
        t1 := Al_Get_Time;
        if bitmap = null then
            Abort_Example( To_String( filename ) & " not found or failed to load" );
        end if;

        Log_Print( "Loading took" & Integer'Image( Integer((t1 - t0) * 1000.0) ) & " milliseconds" );

        -- Create a timer that fires 30 times a second.
        timer := Al_Create_Timer( 1.0 / 30.0 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer ); -- Start the timer

        -- Primary 'game' loop.
        loop
            Al_Wait_For_Event( queue, event ); -- Wait for and get an event.

            if event.any.typ = ALLEGRO_EVENT_DISPLAY_ORIENTATION then
                if event.display.orientation = ALLEGRO_DISPLAY_ORIENTATION_0_DEGREES then
                    Log_Print( "0 degrees" );
                elsif event.display.orientation = ALLEGRO_DISPLAY_ORIENTATION_90_DEGREES then
                    Log_Print( "90 degrees" );
                elsif event.display.orientation = ALLEGRO_DISPLAY_ORIENTATION_180_DEGREES then
                    Log_Print( "180 degrees" );
                elsif event.display.orientation = ALLEGRO_DISPLAY_ORIENTATION_270_DEGREES then
                    Log_Print( "270 degrees" );
                elsif event.display.orientation = ALLEGRO_DISPLAY_ORIENTATION_FACE_UP then
                    Log_Print( "Face up" );
                elsif event.display.orientation = ALLEGRO_DISPLAY_ORIENTATION_FACE_DOWN then
                    Log_Print( "Face down" );
                end if;

            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;

            -- Use keyboard to zoom image in and out.
            -- 1: Reset zoom.
            -- +: Zoom in 10%
            -- -: Zoom out 10%
            -- f: Zoom to width of window
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit; -- Break the loop and quit on escape key.
                elsif event.keyboard.unichar = Character'Pos( '1' ) then
                    zoom := 1.0;
                elsif event.keyboard.unichar = Character'Pos( '=' ) then
                    zoom := zoom * 1.1;
                elsif event.keyboard.unichar = Character'Pos( '-' ) then
                    zoom := zoom / 1.1;
                elsif event.keyboard.unichar = Character'Pos( 'f' ) then
                    zoom := Float(Al_Get_Display_Width( display )) /
                            Float(Al_Get_Bitmap_Width( bitmap ));
                end if;

            -- Trigger a redraw on the timer event
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := True;
            end if;

            -- Redraw, but only if the event queue is empty
            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                redraw := False;
                -- Clear so we don't get trippy artifacts left after zoom.
                Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
                if zoom = 1.0 then
                    Al_Draw_Bitmap( bitmap, 0.0, 0.0, 0 );
                else
                    Al_Draw_Scaled_Rotated_Bitmap( bitmap, 0.0, 0.0, 0.0, 0.0, zoom, zoom, 0.0, 0 );
                end if;
                Al_Flip_Display;
            end if;
        end loop;

        Al_Stop_Timer( timer );

        Al_Unregister_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Unregister_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Unregister_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Destroy_Event_Queue( queue );

        Al_Destroy_Timer( timer );

        Al_Destroy_Bitmap( bitmap );

        Al_Destroy_Display( display );

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Bitmap;
