
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Bitmap_Flip is

    procedure Ada_Main is
        bmp_x    : Float := 200.0;
        bmp_y    : Float := 200.0;
        bmp_dx   : Float := 96.0;
        bmp_dy   : Float := 96.0;
        bmp_flag : Draw_Flags := 0;

        -- Fire the update every 10 milliseconds.
        INTERVAL : constant := 0.01;

        ------------------------------------------------------------------------

        -- Updates the bitmap velocity, orientation and position.
        procedure Update( bmp : A_Allegro_Bitmap ) is
            target    : constant A_Allegro_Bitmap := Al_Get_Target_Bitmap;
            display_w : constant Integer := Al_Get_Bitmap_Width( target );
            display_h : constant Integer := Al_Get_Bitmap_Height( target );
            bitmap_w  : constant Integer := Al_Get_Bitmap_Width( bmp );
            bitmap_h  : constant Integer := Al_Get_Bitmap_Height( bmp );
        begin
            bmp_x := bmp_x + bmp_dx * INTERVAL;
            bmp_y := bmp_y + bmp_dy * INTERVAL;

            -- Make sure bitmap is still on the screen.
            if bmp_y < 0.0 then
                bmp_y := 0.0;
                bmp_dy := -bmp_dy;
                bmp_flag := bmp_flag and (not ALLEGRO_FLIP_VERTICAL);
            end if;

            if bmp_x < 0.0 then
                bmp_x := 0.0;
                bmp_dx := -bmp_dx;
                bmp_flag := bmp_flag and (not ALLEGRO_FLIP_HORIZONTAL);
            end if;

            if bmp_y > Float(display_h - bitmap_h) then
                bmp_y := Float(display_h - bitmap_h);
                bmp_dy := -bmp_dy;
                bmp_flag := bmp_flag or ALLEGRO_FLIP_VERTICAL;
            end if;

            if bmp_x > Float(display_w - bitmap_w) then
                bmp_x := Float(display_w - bitmap_w);
                bmp_dx := -bmp_dx;
                bmp_flag := bmp_flag or ALLEGRO_FLIP_HORIZONTAL;
            end if;
        end Update;

        ------------------------------------------------------------------------

        display   : A_Allegro_Display;
        timer     : A_Allegro_Timer;
        queue     : A_Allegro_Event_Queue;
        bmp       : A_Allegro_Bitmap;
        mem_bmp   : A_Allegro_Bitmap;
        disp_bmp  : A_Allegro_Bitmap;
        font      : A_Allegro_Font;
        event     : aliased Allegro_Event;
        text      : Unbounded_String;
        done      : Boolean := False;
        redraw    : Boolean := True;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        -- Initialize the image addon. Requires the allegro_image addon
        -- library.
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon." );
        end if;

        -- Initialize the image font. Requires the allegro_font addon
        -- library.
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init font addon." );
        end if;
        Init_Platform_Specific;    -- Helper functions from common.ads

        -- Create a new display that we can render the image to.
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        -- Allegro requires installing drivers for all input devices before
        -- they can be used.
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        -- Loads a font from disk. This will use al_load_bitmap_font if you
        -- pass the name of a known bitmap format, or else al_load_ttf_font.
        font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        if font = null then
            Abort_Example( "Error loading data/fixed_font.tga" );
        end if;

        disp_bmp := Al_Load_Bitmap( "data/mysha.pcx" );
        bmp := disp_bmp;
        if disp_bmp = null then
            Abort_Example( "Error loading data/mysha.pcx" );
        end if;
        text := To_Unbounded_String( "Display bitmap (space to change)" );

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        mem_bmp := Al_Load_Bitmap( "data/mysha.pcx" );
        if mem_bmp = null then
            Abort_Example( "Error loading data/mysha.pcx" );
        end if;

        timer := Al_Create_Timer( INTERVAL );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        Al_Start_Timer( timer );

        -- Default premultiplied aplha blending.
        Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );

        -- Primary 'game' loop.
        while not done loop
            -- If the timer has since been fired and the queue is empty, draw.
            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Update( bmp );
                -- Clear so we don't get trippy artifacts left after zoom.
                Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
                Al_Draw_Tinted_Bitmap( bmp, Al_Map_RGBA_f( 1.0, 1.0, 1.0, 0.5 ), bmp_x, bmp_y, bmp_flag );
                Al_Draw_Text( font, Al_Map_RGBA_f( 1.0, 1.0, 1.0, 0.5 ), 0.0, 0.0, 0, To_String( text ) );
                Al_Flip_Display;
                redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        -- Spacebar toggles whether render from a memory bitmap
                        -- or display bitamp.
                        if bmp = mem_bmp then
                            bmp := disp_bmp;
                            text := To_Unbounded_String( "Display bitmap (space to change)" );
                        else
                            bmp := mem_bmp;
                            text := To_Unbounded_String( "Memory bitmap (space to change)" );
                        end if;
                    end if;
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    done := True;
                when ALLEGRO_EVENT_TIMER =>
                    redraw := True;
                when others => null;
            end case;
        end loop;

        Al_Stop_Timer( timer );

        Al_Unregister_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Unregister_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Unregister_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Destroy_Event_Queue( queue );

        Al_Destroy_Timer( timer );

        Al_Destroy_Bitmap( mem_bmp );
        Al_Destroy_Bitmap( disp_bmp );

        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Bitmap_Flip;
