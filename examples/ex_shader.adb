
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Shaders;                   use Allegro.Shaders;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Shader is

    display_flags : Unsigned_32 := 0;

    procedure Parse_Args is
    begin
        for i in 1..Argument_Count loop
            if Argument( i ) = "--opengl" then
                display_flags := ALLEGRO_OPENGL;
            elsif Argument( i ) = "--d3d" then
                display_flags := ALLEGRO_DIRECT3D;
            else
                Abort_Example( "Unrecognised argument: " & Argument( i ) );
            end if;
        end loop;
    end Parse_Args;

    ----------------------------------------------------------------------------

    procedure Choose_Shader_Source( shader  : A_Allegro_Shader;
                                    vsource : in out Unbounded_String;
                                    psource : in out Unbounded_String ) is
        platform : constant Allegro_Shader_Platform := Al_Get_Shader_Platform( shader );
    begin
        if platform = ALLEGRO_SHADER_HLSL then
            vsource := To_Unbounded_String( "data/ex_shader_vertex.hlsl" );
            psource := To_Unbounded_String( "data/ex_shader_pixel.hlsl" );
        elsif platform = ALLEGRO_SHADER_GLSL then
            vsource := To_Unbounded_String( "data/ex_shader_vertex.glsl" );
            psource := To_Unbounded_String( "data/ex_shader_pixel.glsl" );
        end if;
    end Choose_Shader_Source;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display : A_Allegro_Display;
        bmp     : A_Allegro_Bitmap;
        shader  : A_Allegro_Shader;
        vsource : Unbounded_String;
        psource : Unbounded_String;

        tints   : constant Float_Array :=
        (
            4.0, 0.0, 1.0,
            0.0, 4.0, 1.0,
            1.0, 0.0, 4.0,
            4.0, 4.0, 1.0
        );

        s : Allegro_Keyboard_State;
    begin
        Parse_Args;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_PROGRAMMABLE_PIPELINE or display_flags );

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        bmp := Al_Load_Bitmap( "data/mysha.pcx" );
        if bmp = null then
            Abort_Example( "Could not load bitmap." );
        end if;

        shader := Al_Create_Shader( ALLEGRO_SHADER_AUTO );
        if shader = null then
            Abort_Example( "Could not create shader." );
        end if;

        Choose_Shader_Source( shader, vsource, psource );
        if Length( vsource ) = 0 or else Length( psource ) = 0 then
            Abort_Example( "Could not load source files." );
        end if;

        if not Al_Attach_Shader_Source_File( shader, ALLEGRO_VERTEX_SHADER, To_String( vsource ) ) then
            Abort_Example( "al_attach_shader_source_file failed: " & Al_Get_Shader_Log( shader ) );
        elsif not Al_Attach_Shader_Source_File( shader, ALLEGRO_PIXEL_SHADER, To_String( psource ) ) then
            Abort_Example( "al_attach_shader_source_file failed: " & Al_Get_Shader_Log( shader ) );
        end if;

        if not Al_Build_Shader( shader ) then
            Abort_Example( "al_build_shader failed: " & Al_Get_Shader_Log( shader ) );
        end if;

        Al_Use_Shader( shader );

        loop
            Al_Get_Keyboard_State( s );
            if Al_Key_Down( s, ALLEGRO_KEY_ESCAPE ) then
                exit;
            end if;

            al_clear_to_color(al_map_rgb(140, 40, 40));

            Al_Set_Shader_Float_Vector( "tint", 3, tints(0..2) );
            Al_Draw_Bitmap( bmp, 0.0, 0.0, 0 );

            Al_Set_Shader_Float_Vector( "tint", 3, tints(3..5) );
            Al_Draw_Bitmap( bmp, 320.0, 0.0, 0 );

            Al_Set_Shader_Float_Vector( "tint", 3, tints(6..8) );
            Al_Draw_Bitmap( bmp, 0.0, 240.0, 0 );

            -- Draw the last one transformed
            declare
                trans, backup : Allegro_Transform;
            begin
                Al_Copy_Transform( backup, Al_Get_Current_Transform.all );
                Al_Identity_Transform( trans );
                Al_Translate_Transform( trans, 320.0, 240.0 );
                Al_Set_Shader_Float_Vector( "tint", 3, tints(9..11) );
                Al_Use_Transform( trans );
                Al_Draw_Bitmap( bmp, 0.0, 0.0, 0 );
                Al_Use_Transform( backup );

                Al_Flip_Display;

                Al_Rest( 0.01 );
            end;
        end loop;

        Al_Use_Shader( null );
        Al_Destroy_Shader( shader );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Shader;
