
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
-- This example exercises line drawing, and single buffer mode.
--
package body Ex_Lines is

    procedure Ada_Main is

        -- XXX the software line drawer currently doesn't perform clipping properly

        W : constant := 640;
        H : constant := 480;

        display    : A_Allegro_Display;
        queue      : A_Allegro_Event_Queue;
        black,
        white,
        background : Allegro_Color;
        dbuf       : A_Allegro_Bitmap;

        last_x     : Float := -1.0;
        last_y     : Float := -1.0;

        ------------------------------------------------------------------------

        procedure Fade is
        begin
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Filled_Rectangle( 0.0, 0.0, Float(W), Float(H),
                                      Al_Map_RGBA_f( 0.5, 0.5, 0.6, 0.2 ) );
        end Fade;

        ------------------------------------------------------------------------

        procedure Red_Dot( x, y : Float ) is
        begin
            Al_Draw_Filled_Rectangle( x - 2.0, y - 2.0, x + 2.0, y + 2.0,
                                      Al_Map_RGB_f( 1.0, 0.0, 0.0 ) );
        end Red_Dot;

        ------------------------------------------------------------------------

        procedure Draw_Clip_Rect is
        begin
            Al_Draw_Rectangle( 100.5, 100.5, Float(W) - 100.5, Float(H) - 100.5, black, 0.0 );
        end Draw_Clip_Rect;

        ------------------------------------------------------------------------

        procedure My_Set_Clip_Rect is
        begin
            Al_Set_Clipping_Rectangle( 100, 100, W - 200, H - 200 );
        end My_Set_Clip_Rect;

        ------------------------------------------------------------------------

        procedure Reset_Clip_Rect is
        begin
            Al_Set_Clipping_Rectangle( 0, 0, W, H );
        end Reset_Clip_Rect;

        ------------------------------------------------------------------------

        procedure Flip is
        begin
            Al_Set_Target_Backbuffer( display );
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            Al_Draw_Bitmap( dbuf, 0.0, 0.0, 0 );
            Al_Flip_Display ;
        end Flip;

        ------------------------------------------------------------------------

        procedure Plonk( x, y : Float; blend : Boolean ) is
        begin
            Al_Set_Target_Bitmap( dbuf );

            Fade;
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_ZERO );
            Draw_Clip_Rect;
            Red_Dot( x, y );

            if last_x = -1.0 and then last_y = -1.0 then
                last_x := x;
                last_y := y;
            else
                My_Set_Clip_Rect;
                if blend then
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
                end if;
                Al_Draw_Line( last_x, last_y, x, y, white, 0.0 );
                last_x := -1.0;
                last_y := -1.0;
                Reset_Clip_Rect;
            end if;

            Flip;
        end Plonk;

        ------------------------------------------------------------------------

        procedure Splat( x, y : Float; blend : Boolean ) is
            theta : Float;
        begin
            Al_Set_Target_Bitmap( dbuf );

            Fade;
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_ZERO );
            Draw_Clip_Rect;
            Red_Dot( x, y );

            my_set_clip_rect;
            if blend then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
            end if;
            theta := 0.0;
            loop
                exit when theta < 2.0 * ALLEGRO_PI;
                Al_Draw_Line( x, y, x + 40.0 * Cos( theta ), y + 40.0 * Sin( theta ), white, 0.0 );
                theta := theta + ALLEGRO_PI / 16.0;
            end loop;
            Reset_Clip_Rect;

            Flip;
        end Splat;

        ------------------------------------------------------------------------

         event : aliased Allegro_Event;
         kst   : aliased Allegro_Keyboard_State;
         blend : Boolean := False;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;

        display := Al_Create_Display( W, H );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        black := Al_Map_RGB_f( 0.0, 0.0, 0.0 );
        white := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
        background := Al_Map_RGB_f( 0.5, 0.5, 0.6 );

        if Argument_Count = 1 and then Argument( 1 ) = "--memory-bitmap" then
            Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        end if;
        dbuf := Al_Create_Bitmap( W, H );
        if dbuf = null then
            Abort_Example( "Error creating double buffer" );
        end if;

        Al_Set_Target_Bitmap( dbuf );
        Al_Clear_To_Color( background );
        Draw_Clip_Rect;
        Flip;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, al_get_keyboard_event_source );
        Al_Register_Event_Source( queue, al_get_mouse_event_source );

        loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                Al_Get_Keyboard_State( kst );
                blend := Al_Key_Down( kst, ALLEGRO_KEY_LSHIFT ) or
                         Al_Key_Down( kst, ALLEGRO_KEY_RSHIFT );
                if event.mouse.button = 1 then
                    Plonk( Float(event.mouse.x), Float(event.mouse.y), blend );
                else
                    Splat( Float(event.mouse.x), Float(event.mouse.y), blend );
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_SWITCH_OUT then
               last_x := -1.0;
               last_y := -1.0;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
                  event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
            then
                exit;
            end if;
        end loop;

        Al_Destroy_Event_Queue( queue );
        Al_Destroy_Bitmap( dbuf );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Lines;
