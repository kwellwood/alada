
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System;                            use System;
with System.Storage_Elements;           use System.Storage_Elements;

--
--       Example program for the Allegro library.
--
--       From left to right you should see Red, Green, Blue gradients.
--
package body Ex_Lockbitmap is

    type Mode is (MODE_VIDEO, MODE_MEMORY, MODE_BACKBUFFER);

    ----------------------------------------------------------------------------

    procedure Fill( bitmap : A_Allegro_Bitmap; lock_flags : Allegro_Lock_Mode ) is
        ptr    : Address;
        locked : A_Allegro_Locked_Region;
        red,
        green,
        blue   : Unsigned_8;
    begin
        -- Locking the bitmap means, we work directly with pixel data.  We can
        -- choose the format we want to work with, which may imply conversions, or
        -- use the bitmap's actual format so we can work directly with the bitmap's
        -- pixel data.
        -- We use a 16-bit format and odd positions and sizes to increase the
        -- chances of uncovering bugs.
        --
        locked := Al_Lock_Bitmap_Region( bitmap, 193, 65, 3 * 127, 127,
                                         ALLEGRO_PIXEL_FORMAT_RGB_565,
                                         lock_flags );
        if locked = null then
            return;
        end if;

        for j in 0..126 loop
            ptr := locked.data + Storage_Offset(j * locked.pitch);

            for i in 0..3 * 127 - 1 loop
                declare
                    col   : Unsigned_16;
                    for col'Address use ptr;
                begin
                    if j = 0 or else j = 126 or else i = 0 or else i = 3 * 127 - 1 then
                        red   := 0;
                        green := 0;
                        blue  := 0;
                    elsif i < 127 then
                        red   := 255;
                        green := Unsigned_8(j * 2);
                        blue  := Unsigned_8(j * 2);
                    elsif i < 2 * 127 then
                        green := 255;
                        red   := Unsigned_8(j * 2);
                        blue  := Unsigned_8(j * 2);
                    else
                        blue  := 255;
                        red   := Unsigned_8(j * 2);
                        green := Unsigned_8(j * 2);
                    end if;

                    -- The RGB_555 format means, the 16 bits per pixel are laid out like
                    -- this, least significant bit right: RRRRR GGGGGG BBBBB
                    -- Because the byte order can vary per platform (big endian or
                    -- little endian) we encode an integer and store that
                    -- directly rather than storing each component separately.
                    --
                    -- In READWRITE mode the light blue background should show through
                    -- the stipple pattern.
                    if lock_flags = ALLEGRO_LOCK_WRITEONLY or else (j + i) mod 2 = 1 then
                        col := Shift_Left( (Unsigned_16(red) / 8), 11 ) or
                               Shift_Left( (Unsigned_16(green) / 4), 5 ) or
                               (Unsigned_16(blue) / 8);
                    end if;
                end;
                ptr := ptr + Storage_Offset(2);
            end loop;

        end loop;
        Al_Unlock_Bitmap( bitmap );
    end Fill;

    ----------------------------------------------------------------------------

    procedure Draw( display : A_Allegro_Display; md : Mode; lock_flags : Allegro_Lock_Mode ) is
        bitmap : A_Allegro_Bitmap;
    begin
        -- Create the bitmap to lock, or use the display backbuffer.
        if md = MODE_VIDEO then
            Log_Put( "Locking video bitmap" );
            Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
            Al_Set_New_Bitmap_Flags( ALLEGRO_VIDEO_BITMAP );
            bitmap := Al_Create_Bitmap( 3 * 256, 256 );
        elsif md = MODE_MEMORY then
            Log_Put( "Locking memory bitmap" );
            Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
            Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
            bitmap := Al_Create_Bitmap( 3 * 256, 256 );
        else
            Log_Put( "Locking display backbuffer" );
            bitmap := Al_Get_Backbuffer( display );
        end if;
        if bitmap = null then
            Abort_Example( "Error creating bitmap" );
        end if;

        if lock_flags = ALLEGRO_LOCK_WRITEONLY then
            Log_Print( " in write-only mode" );
        else
            Log_Print( " in read/write mode" );
        end if;

        Al_Set_Target_Bitmap( bitmap );
        Al_Clear_To_Color( Al_Map_RGB_f( 0.8, 0.8, 0.9 ) );
        Al_Set_Target_Backbuffer( display );

        Fill( bitmap, lock_flags );

        if md /= MODE_BACKBUFFER then
            Al_Draw_Bitmap( bitmap, 0.0, 0.0, 0 );
            Al_Destroy_Bitmap( bitmap );
            bitmap := null;
        end if;

        Al_Flip_Display;
    end Draw;

    ----------------------------------------------------------------------------

    function Cycle_Mode( md : Mode ) return Mode is
    begin
        return (if md = MODE_VIDEO then MODE_MEMORY else (if md = MODE_MEMORY then MODE_BACKBUFFER else MODE_VIDEO));
    end Cycle_Mode;

    ----------------------------------------------------------------------------

    function Toggle_Writeonly( lock_flags : Allegro_Lock_Mode ) return Allegro_Lock_Mode
    is ((if lock_flags = ALLEGRO_LOCK_WRITEONLY then ALLEGRO_LOCK_READWRITE else ALLEGRO_LOCK_WRITEONLY));

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display    : A_Allegro_Display;
        events     : A_Allegro_Event_Queue;
        event      : aliased Allegro_Event;
        md         : Mode := MODE_VIDEO;
        lock_flags : Allegro_Lock_Mode := ALLEGRO_LOCK_WRITEONLY;
        redraw     : Boolean := True;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;

        Open_Log;

        -- Create a window.
        display := Al_Create_Display( 3 * 256, 256 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        events := Al_Create_Event_Queue;
        Al_Register_Event_Source( events, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( events, Al_Get_Mouse_Event_Source );

        Log_Print( "Press space to change bitmap type" );
        Log_Print( "Press w to toggle WRITEONLY mode" );

        loop
            if redraw then
                Draw( display, md, lock_flags );
                redraw := False;
            end if;

            Al_Wait_For_Event( events, event );
            if event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                elsif event.keyboard.unichar = Character'Pos(' ') then
                    md := Cycle_Mode( md );
                    redraw := True;
                elsif event.keyboard.unichar = Character'Pos('w') or else
                      event.keyboard.unichar = Character'Pos('W')
                then
                    lock_flags := Toggle_Writeonly( lock_flags );
                    redraw := True;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                if event.mouse.button = 1 then
                    if event.mouse.x < Al_Get_Display_Width( display ) / 2 then
                        md := Cycle_Mode( md );
                    else
                        lock_flags := Toggle_Writeonly( lock_flags) ;
                    end if;
                    redraw := True;
                end if;
            end if;
        end loop;

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Lockbitmap;
