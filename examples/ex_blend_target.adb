
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;

package body Ex_Blend_Target is

    FPS : constant := 60.0;

    function Load_Bitmap( filename : String) return A_Allegro_Bitmap is
        bitmap : constant A_Allegro_Bitmap := Al_Load_Bitmap( filename );
    begin
        if bitmap = null then
            Abort_Example( filename & " not found or failed to load" );
        end if;
        return bitmap;
    end Load_Bitmap;

    procedure Ada_Main is
        type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        display     : A_Allegro_Display;
        mysha       : A_Allegro_Bitmap;
        parrot      : A_Allegro_Bitmap;
        targets     : Bitmap_Array(0..3);
        backbuffer  : A_Allegro_Bitmap;
        w           : Integer;
        h           : Integer;
        done        : Boolean := False;
        need_redraw : Boolean := True;
        event       : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Failed to init image addon." );
        end if;

        Init_Platform_Specific;

        mysha := Load_Bitmap( "data/mysha.pcx" );
        parrot := Load_Bitmap( "data/obp.jpg" );

        w := Al_Get_Bitmap_Width( mysha );
        h := Al_Get_Bitmap_Height( mysha );

        display := Al_Create_Display( 2 * w, 2 * h );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        -- Set up blend modes once
        backbuffer := Al_Get_Backbuffer( display );
        targets(0) := Al_Create_Sub_Bitmap( backbuffer, 0, 0, w, h );
        Al_Set_Target_Bitmap( targets(0) );
        Al_Set_Bitmap_Blender( ALLEGRO_DEST_MINUS_SRC, ALLEGRO_SRC_COLOR, ALLEGRO_DST_COLOR );
        targets(1) := Al_Create_Sub_Bitmap( backbuffer, w, 0, w, h );
        Al_Set_Target_Bitmap( targets(1) );
        Al_Set_Bitmap_Blender( ALLEGRO_ADD, ALLEGRO_SRC_COLOR, ALLEGRO_DST_COLOR );
        Al_Set_Bitmap_Blend_Color( Al_Map_RGB_f( 0.5, 0.5, 1.0 ) );
        targets(2) := Al_Create_Sub_Bitmap( backbuffer, 0, h, w, h );
        Al_Set_Target_Bitmap( targets(2) );
        Al_Set_Bitmap_Blender( ALLEGRO_SRC_MINUS_DEST, ALLEGRO_SRC_COLOR, ALLEGRO_DST_COLOR );
        targets(3) := Al_Create_Sub_Bitmap( backbuffer, w, h, w, h );
        Al_Set_Target_Bitmap( targets(3) );
        Al_Set_Bitmap_Blender( ALLEGRO_DEST_MINUS_SRC, ALLEGRO_INVERSE_SRC_COLOR, ALLEGRO_DST_COLOR );

        timer := Al_Create_Timer(1.0 / FPS );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        Al_Start_Timer( timer );

        while not done loop

            if need_redraw then
                Al_Set_Target_Bitmap( backbuffer );
                Al_Draw_Bitmap( parrot, 0.0, 0.0, 0 );

                for i in targets'Range loop
                    -- Simply setting the target also sets the blend mode.
                    Al_Set_Target_Bitmap( targets(i) );
                    Al_Draw_Bitmap( mysha, 0.0, 0.0, 0 );
                end loop;

                Al_Flip_Display;
                need_redraw := False;
            end if;

            loop
                Al_Wait_For_Event( queue, event );
                case event.any.typ is
                    when ALLEGRO_EVENT_KEY_CHAR =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            done := True;
                        end if;

                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                       done := True;

                    when ALLEGRO_EVENT_TIMER =>
                       need_redraw := True;

                    when others => null;
                end case;

                exit when Al_Is_Event_Queue_Empty( queue );
            end loop;

        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Blend_Target;
