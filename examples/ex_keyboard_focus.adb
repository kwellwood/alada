
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;

--
--    Example program for the Allegro library.
--
--    This program tests if the ALLEGRO_KEYBOARD_STATE `display' field
--    is set correctly to the focused display.
--
package body Ex_Keyboard_Focus is

    procedure Ada_Main is

        display1 : A_Allegro_Display;
        display2 : A_Allegro_Display;

        ----------------------------------------------------------------------------

        procedure Redraw( color1, color2 : Allegro_Color ) is
        begin
            Al_Set_Target_Backbuffer( display1 );
            Al_Clear_To_Color( color1 );
            Al_Flip_Display;

            Al_Set_Target_Backbuffer( display2 );
            Al_Clear_To_Color( color2 );
            Al_Flip_Display;
        end Redraw;

        ----------------------------------------------------------------------------

        black    : Allegro_Color;
        red      : Allegro_Color;
        kbdstate : Allegro_Keyboard_State;
    begin
        if not Al_Initialize then
            Abort_Example( "Error initialising Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        display1 := Al_Create_Display( 300, 300 );
        display2 := Al_Create_Display( 300, 300 );
        if display1 = null or else display2 = null then
            Abort_Example("Error creating displays." );
        end if;

        black := Al_Map_RGB( 0, 0, 0 );
        red := Al_Map_RGB( 255, 0, 0 );

        loop
            Al_Get_Keyboard_State( kbdstate );
            if Al_Key_Down( kbdstate, ALLEGRO_KEY_ESCAPE ) then
                exit;
            end if;

            if kbdstate.display = display1 then
                Redraw( red, black );
            elsif kbdstate.display = display2 then
                Redraw( black, red );
            else
                Redraw( black, black );
            end if;

            Al_Rest( 0.1 );
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Keyboard_Focus;
