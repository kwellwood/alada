
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Displays.Fullscreen;       use Allegro.Displays.Fullscreen;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

-- Test retrieving and settings possible modes.
package body Ex_Display_Options is

    procedure Ada_Main is
        font              : A_Allegro_Font;
        font_h            : Integer;
        modes_count       : Integer;
        options_count     : Integer;
        status            : Unbounded_String;
        flags,
        old_flags         : Unsigned_32 := 0;

        visible_rows      : Integer := 0;
        first_visible_row : Integer := 0;
        selected_column   : Integer := 0;
        selected_mode     : Integer := 0;
        selected_option   : Integer := 0;

        type A_String is access all String;

        Type String_Array is array (Integer range <>) of A_String;

        type Option_Rec is
            record
                name      : A_String;
                option    : Display_Option;
                value     : Unsigned_32;
                max_value : Unsigned_32;
                required  : Option_Importance;
            end record;

        type Option_Array is array (Integer range <>) of Option_Rec;

        options : Option_Array(0..23) := Option_Array'(
            (new String'("COLOR_SIZE"), ALLEGRO_COLOR_SIZE, 0, 32, 0),
            (new String'("RED_SIZE"), ALLEGRO_RED_SIZE, 0, 8, 0),
            (new String'("GREEN_SIZE"), ALLEGRO_GREEN_SIZE, 0, 8, 0),
            (new String'("BLUE_SIZE"), ALLEGRO_BLUE_SIZE, 0, 8, 0),
            (new String'("ALPHA_SIZE"), ALLEGRO_ALPHA_SIZE, 0, 8, 0),
            (new String'("RED_SHIFT"), ALLEGRO_RED_SHIFT, 0, 32, 0),
            (new String'("GREEN_SHIFT"), ALLEGRO_GREEN_SHIFT, 0, 32, 0),
            (new String'("BLUE_SHIFT"), ALLEGRO_BLUE_SHIFT, 0, 32, 0),
            (new String'("ALPHA_SHIFT"), ALLEGRO_ALPHA_SHIFT, 0, 32, 0),
            (new String'("DEPTH_SIZE"), ALLEGRO_DEPTH_SIZE, 0, 32, 0),
            (new String'("FLOAT_COLOR"), ALLEGRO_FLOAT_COLOR, 0, 1, 0),
            (new String'("FLOAT_DEPTH"), ALLEGRO_FLOAT_DEPTH, 0, 1, 0),
            (new String'("STENCIL_SIZE"), ALLEGRO_STENCIL_SIZE, 0, 32, 0),
            (new String'("SAMPLE_BUFFERS"), ALLEGRO_SAMPLE_BUFFERS, 0, 1, 0),
            (new String'("SAMPLES"), ALLEGRO_SAMPLES, 0, 8, 0),
            (new String'("RENDER_METHOD"), ALLEGRO_RENDER_METHOD, 0, 2, 0),
            (new String'("SINGLE_BUFFER"), ALLEGRO_SINGLE_BUFFER, 0, 1, 0),
            (new String'("SWAP_METHOD"), ALLEGRO_SWAP_METHOD, 0, 1, 0),
            (new String'("VSYNC"), ALLEGRO_VSYNC, 0, 2, 0),
            (new String'("COMPATIBLE_DISPLAY"), ALLEGRO_COMPATIBLE_DISPLAY, 0, 1, 0),
            (new String'("MAX_BITMAP_SIZE"), ALLEGRO_MAX_BITMAP_SIZE, 0, 65536, 0),
            (new String'("SUPPORT_NPOT_BITMAP"), ALLEGRO_SUPPORT_NPOT_BITMAP, 0, 1, 0),
            (new String'("CAN_DRAW_INTO_BITMAP"), ALLEGRO_CAN_DRAW_INTO_BITMAP, 0, 1, 0),
            (new String'("SUPPORT_SEPARATE_ALPHA"), ALLEGRO_SUPPORT_SEPARATE_ALPHA, 0, 1, 0)
        );

        flag_names : String_Array(0..31);

        ------------------------------------------------------------------------

        procedure Init_Flags is
        begin
            for i in flag_names'Range loop
                if Shift_Left( 1, i ) = ALLEGRO_WINDOWED then
                    flag_names(i) := new String'("WINDOWED");
                elsif Shift_Left( 1, i ) = ALLEGRO_FULLSCREEN then
                    flag_names(i) := new String'("FULLSCREEN");
                elsif Shift_Left( 1, i ) = ALLEGRO_OPENGL then
                    flag_names(i) := new String'("OPENGL");
                elsif Shift_Left( 1, i ) = ALLEGRO_RESIZABLE then
                    flag_names(i) := new String'("RESIZABLE");
                elsif Shift_Left( 1, i ) = ALLEGRO_FRAMELESS then
                    flag_names(i) := new String'("FRAMELESS");
                elsif Shift_Left( 1, i ) = ALLEGRO_GENERATE_EXPOSE_EVENTS then
                    flag_names(i) := new String'("GENERATE_EXPOSE_EVENTS");
                elsif Shift_Left( 1, i ) = ALLEGRO_FULLSCREEN_WINDOW then
                    flag_names(i) := new String'("FULLSCREEN_WINDOW");
                elsif Shift_Left( 1, i ) = ALLEGRO_MINIMIZED then
                    flag_names(i) := new String'("MINIMIZED");
                end if;
            end loop;
        end Init_Flags;

        ------------------------------------------------------------------------

        procedure Load_Font is
        begin
            font := Al_Create_Builtin_Font;
            if font = null then
                Abort_Example( "Error creating builtin font" );
            end if;
            font_h := Al_Get_Font_Line_Height( font );
        end Load_Font;

        ------------------------------------------------------------------------

        procedure Display_Options( display : A_Allegro_Display ) is
            n           : constant Integer := options_count;
            dw          : constant Integer := Al_Get_Display_Width( display );
            dh          : constant Integer := Al_Get_Display_Height( display );
            modes_count : constant Integer := Al_Get_Num_Display_Modes;
            y           : Float := 10.0;
            x           : Float := 10.0;
            c           : Allegro_Color;
            i           : Integer;
            mode        : aliased Allegro_Display_Mode;
            ok          : Boolean;
            pragma Warnings( Off, ok );
        begin
            c := Al_Map_RGB_f( 0.8, 0.8, 1.0 );
            Al_Draw_Text( font, c, x, y, 0, "Create new display" );
            y := y + Float(font_h);

            i := first_visible_row;
            while i < modes_count + 2 and then i < first_visible_row + visible_rows loop
                if i > 1 then
                    Al_Get_Display_Mode( i - 2, mode, ok );
                elsif i = 1 then
                    mode.width := 800;
                    mode.height := 600;
                    mode.format := ALLEGRO_PIXEL_FORMAT_ANY;
                    mode.refresh_rate := 0;
                else
                    mode.width := 800;
                    mode.height := 600;
                    mode.format := ALLEGRO_PIXEL_FORMAT_ANY;
                    mode.refresh_rate := 0;
                end if;

                if selected_column = 0 and then selected_mode = i then
                    c := Al_Map_RGB_f( 1.0, 1.0, 0.0 );
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                    Al_Draw_Filled_Rectangle( x, y, x + 300.0, y + Float(font_h), c );
                end if;

                c := Al_Map_RGB_f( 0.0, 0.0, 0.0 );
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                if (i = first_visible_row and then i > 0) or else
                   (i = first_visible_row + visible_rows - 1 and then i < modes_count + 1)
                then
                    Al_Draw_Text( font, c, x, y, 0, "..." );
                else
                    if i > 1 then
                        Al_Draw_Text( font, c, x, y, 0, "Fullscreen" & mode.width'Img & " x" & mode.height'Img & " (fmt: " & Image( mode.format ) & "," & mode.refresh_rate'Img & " Hz)" );
                    elsif i = 0 then
                        Al_Draw_Text( font, c, x, y, 0, "Windowed" & mode.width'Img & " x" & mode.height'Img & " (fmt: " & Image( mode.format ) & "," & mode.refresh_rate'Img & " Hz)" );
                    else
                        Al_Draw_Text( font, c, x, y, 0, "FS Window" & mode.width'Img & " x" & mode.height'Img & " (fmt: " & Image( mode.format ) & "," & mode.refresh_rate'Img & " Hz)" );
                    end if;
                end if;

                y := y + Float(font_h);
                i := i + 1;
            end loop;

            x := Float(dw / 2 + 10);
            y := 10.0;
            c := Al_Map_RGB_f( 0.8, 0.8, 1.0 );
            Al_Draw_Text( font, c, x, y, 0, "Options for new display" );
            Al_Draw_Text( font, c, Float(dw - 10), y, ALLEGRO_ALIGN_RIGHT, "(current display)" );
            y := y + Float(font_h);

            for i in 0..n-1 loop
                if selected_column = 1 and then selected_option = i then
                    c := Al_Map_RGB_f( 1.0, 1.0, 0.0 );
                    Al_Draw_Filled_Rectangle( x, y, x + 300.0, y + Float(font_h), c );
                end if;

                if options(i).required = ALLEGRO_REQUIRE then
                    c := Al_Map_RGB_f( 0.5, 0.0, 0.0 );
                    Al_Draw_Text( font, c, x, y, 0, options(i).name.all & ":" & options(i).value'Img & " (required)" );
                elsif options(i).required = ALLEGRO_SUGGEST then
                    c := Al_Map_RGB_f( 0.0, 0.0, 0.0 );
                    Al_Draw_Text( font, c, x, y, 0, options(i).name.all & ":" & options(i).value'Img & " (suggested)" );
                elsif options(i).required = ALLEGRO_DONTCARE then
                    c := Al_Map_RGB_f( 0.5, 0.5, 0.5 );
                    Al_Draw_Text( font, c, x, y, 0, options(i).name.all & ":" & options(i).value'Img & " (ignored)" );
                end if;

                c := Al_Map_RGB_f( 0.9, 0.5, 0.3 );
                Al_Draw_Text( font, c, Float(dw - 10), y, ALLEGRO_ALIGN_RIGHT,
                              Al_Get_Display_Option( display, options(i).option )'Img );
                y := y + Float(font_h);
            end loop;

            c := Al_Map_RGB_f( 0.0, 0.0, 0.8 );
            x := 10.0;
            y := Float(dh - font_h - 10);
            y := y - Float(font_h);
            Al_Draw_Text( font, c, x, y, 0, "PageUp/Down: modify values" );
            y := y - Float(font_h);
            Al_Draw_Text( font, c, x, y, 0, "Return: set mode or require option" );
            y := y - Float(font_h);
            Al_Draw_Text( font, c, x, y, 0, "Cursor keys: change selection" );

            y := y - Float(font_h * 2);
            for i in 0..31 loop
                if flag_names(i) /= null then
                    if (flags and Shift_Left( 1, i )) > 0 then
                        c := Al_Map_RGB_f( 0.5, 0.0, 0.0 );
                        Al_Draw_Text( font, c, x, y, 0, flag_names(i).all );
                        x := x + Float(Al_Get_Text_Width( font, flag_names(i).all ) + 10);
                    elsif (old_flags and Shift_Left( 1, i )) > 0 then
                        c := Al_Map_RGB_f( 0.5, 0.4, 0.4 );
                        Al_Draw_Text( font, c, x, y, 0, flag_names(i).all );
                        x := x + Float(Al_Get_Text_Width( font, flag_names(i).all ) + 10);
                    end if;
                end if;
            end loop;

            c := Al_Map_RGB_f( 1.0, 0.0, 0.0 );
            Al_Draw_Text( font, c, Float(dw) / 2.0, Float(dh - font_h), ALLEGRO_ALIGN_CENTRE, To_String( status ) );
        end Display_Options;

        ------------------------------------------------------------------------

        procedure Update_UI is
            h : constant Integer := Al_Get_Display_Height( Al_Get_Current_Display );
        begin
            visible_rows := h / font_h - 10;
        end Update_UI;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        queue   : A_Allegro_Event_Queue;
        timer   : A_Allegro_Timer;
        redraw  : Boolean := False;
        event   : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        Init_Flags;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init Allegro primitives addon." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        display := Al_Create_Display( 800, 600 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        Load_Font;

        timer := Al_Create_Timer( 1.0 / 60.0 );

        modes_count := Al_Get_Num_Display_Modes;
        options_count := options'Length;

        Update_UI;

        Al_Clear_To_Color( Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );
        Display_Options( display );
        Al_Flip_Display;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    exit;

                when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                    if event.mouse.button = 1 then
                        declare
                            dw     : constant Integer := Al_Get_Display_Width( display );
                            y      : constant Integer := 10;
                            row    : constant Integer := (event.mouse.y - y) / font_h - 1;
                            column : constant Integer := event.mouse.x / (dw / 2);
                        begin
                            if column = 0 then
                                if row >= 0 and then row <= modes_count then
                                    selected_column := column;
                                    selected_mode := row;
                                    redraw := True;
                                end if;
                            end if;
                            if column = 1 then
                                if row >= 0 and then row < options_count then
                                    selected_column := column;
                                    selected_option := row;
                                    redraw := True;
                                end if;
                            end if;
                        end;
                    end if;

                when ALLEGRO_EVENT_TIMER =>
                    declare
                        f : constant Display_Flags := Al_Get_Display_Flags( display );
                    begin
                        if f /= flags then
                            redraw := True;
                            flags := f;
                            old_flags := old_flags or f;
                        end if;
                    end;

                when ALLEGRO_EVENT_KEY_CHAR =>
                    declare
                        change : Integer := 0;
                    begin
                        case event.keyboard.keycode is
                            when ALLEGRO_KEY_ESCAPE =>
                                exit;

                            when ALLEGRO_KEY_LEFT =>
                                selected_column := 0;
                                redraw := True;

                            when ALLEGRO_KEY_RIGHT =>
                                selected_column := 1;
                                redraw := True;

                            when ALLEGRO_KEY_UP =>
                                if selected_column = 0 then
                                    selected_mode := selected_mode - 1;
                                elsif selected_column = 1 then
                                    selected_option := selected_option - 1;
                                end if;
                                redraw := True;

                            when ALLEGRO_KEY_DOWN =>
                                if selected_column = 0 then
                                    selected_mode := selected_mode + 1;
                                elsif selected_column = 1 then
                                    selected_option := selected_option + 1;
                                end if;
                                redraw := True;

                            when ALLEGRO_KEY_ENTER =>
                                if selected_column = 0 then
                                    declare
                                        mode        : aliased Allegro_Display_Mode;
                                        new_display : A_Allegro_Display;
                                        ok          : Boolean;
                                    begin
                                        if selected_mode > 1 then
                                            Al_Get_Display_Mode( selected_mode - 2, mode, ok );
                                            Al_Set_New_Display_Flags( ALLEGRO_FULLSCREEN );
                                        elsif selected_mode = 1 then
                                            mode.width := 800;
                                            mode.height := 600;
                                            Al_Set_New_Display_Flags( ALLEGRO_FULLSCREEN_WINDOW );
                                        else
                                            mode.width := 800;
                                            mode.height := 600;
                                            Al_Set_New_Display_Flags( ALLEGRO_WINDOWED );
                                        end if;

                                        Al_Destroy_Font( font );

                                        new_display := Al_Create_Display( mode.width, mode.height );
                                        if new_display /= null then
                                            Al_Destroy_Display( display );
                                            display := new_display;
                                            Al_Set_Target_Backbuffer( display );
                                            Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
                                            Update_UI;
                                            status := To_Unbounded_String( "Display creation succeeded." );
                                        else
                                            status := To_Unbounded_String( "Display creation failed." );
                                        end if;

                                        Load_Font;
                                    end;
                                elsif selected_column = 1 then
                                    options(selected_option).required := options(selected_option).required + 1;
                                    options(selected_option).required := options(selected_option).required mod 3;
                                    Al_Set_New_Display_Option( options(selected_option).option,
                                                               options(selected_option).value,
                                                               options(selected_option).required );
                                end if;
                                redraw := True;

                            when ALLEGRO_KEY_PGUP =>
                                change := 1;

                            when ALLEGRO_KEY_PGDN =>
                                change := -1;

                            when others =>
                                null;
                        end case;

                        if change /= 0 and then selected_column = 1 then
                            if change >= 0 or else Unsigned_32(-change) <= options(selected_option).value then
                                options(selected_option).value := Unsigned_32(Integer(options(selected_option).value) + change);
                            else
                                options(selected_option).value := 0;
                            end if;
                            if options(selected_option).value > options(selected_option).max_value then
                                options(selected_option).value := options(selected_option).max_value;
                            end if;
                            Al_Set_New_Display_Option( options(selected_option).option,
                                                       options(selected_option).value,
                                                       options(selected_option).required );
                            redraw := True;
                        end if;
                    end;

                when others =>
                    null;
            end case;

            if selected_mode < 0 then
                selected_mode := 0;
            elsif selected_mode > modes_count + 1 then
                selected_mode := modes_count + 1;
            end if;
            if selected_option < 0 then
                selected_option := 0;
            elsif selected_option >= options_count then
                selected_option := options_count - 1;
            end if;
            if selected_mode < first_visible_row then
                first_visible_row := selected_mode;
            elsif selected_mode > first_visible_row + visible_rows - 1 then
                first_visible_row := selected_mode - visible_rows + 1;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                redraw := False;
                Al_Clear_To_Color( Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );
                Display_Options( display );
                Al_Flip_Display;
            end if;
        end loop;

        Al_Destroy_Event_Queue( queue );
        Al_Destroy_Timer( timer );
        Al_Destroy_Font( font );
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Display_Options;
