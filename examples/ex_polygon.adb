
with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
-- Example program for the Allegro library, by Peter Wang.
--
-- This program tests the polygon routines in the primitives addon.
--
package body Ex_Polygon is

    MAX_VERTICES : constant := 64;
    MAX_POLYGONS : constant := 9;
    RADIUS       : constant := 5.0;

    type Mode_Type is (
        MODE_POLYLINE,
        MODE_POLYGON,
        MODE_FILLED_POLYGON,
        MODE_FILLED_HOLES
    );

    Type AddHole_Type is (
        NOT_ADDING_HOLE,
        NEW_HOLE,
        GROW_HOLE
    );

    type Example_Type is
        record
            display        : A_Allegro_Display;
            font           : A_Allegro_Font;
            fontbmp        : A_Allegro_Font;
            queue          : A_Allegro_Event_Queue;
            dbuf           : A_Allegro_Bitmap;
            bg, fg         : Allegro_Color;
            vertices       : Point_2D_Array(0..MAX_VERTICES-1);
            vertex_polygon : Integer_Array(0..MAX_VERTICES-1);
            vertex_count   : Integer := 0;
            cur_vertex     : Integer := -1;     -- -1 = none
            cur_polygon    : Integer := 0;
            mode           : Mode_Type;
            cap_style      : Allegro_Line_Cap := ALLEGRO_LINE_CAP_NONE;
            join_style     : Allegro_Line_Join := ALLEGRO_LINE_JOIN_NONE;
            thickness      : Float := 1.0;
            miter_limit    : Float := 1.0;
            software       : Boolean := False;
            zoom           : Float := 1.0;
            scroll_x,
            scroll_y       : Float := 0.0;
            add_hole       : AddHole_Type := NOT_ADDING_HOLE;
        end record;

    ex : Example_Type;

    ----------------------------------------------------------------------------

    procedure Reset is
    begin
        ex.vertex_count := 0;
        ex.cur_vertex := -1;
        ex.cur_polygon := 0;
        ex.cap_style := ALLEGRO_LINE_CAP_NONE;
        ex.join_style := ALLEGRO_LINE_JOIN_NONE;
        ex.thickness := 1.0;
        ex.miter_limit := 1.0;
        ex.software := false;
        ex.zoom := 1.0;
        ex.scroll_x := 0.0;
        ex.scroll_y := 0.0;
        ex.add_hole := NOT_ADDING_HOLE;
    end Reset;

    ----------------------------------------------------------------------------

    procedure Transform( x, y : in out Float ) is
    begin
        x := x / ex.zoom;
        y := y / ex.zoom;
        x := x - ex.scroll_x;
        y := y - ex.scroll_y;
    end Transform;

    ----------------------------------------------------------------------------

    function Hit_Vertex( mx, my : Integer ) return Integer is
        dx, dy : Float;
    begin
        for i in 0..ex.vertex_count-1 loop
            dx := ex.vertices(i).x - Float(mx);
            dy := ex.vertices(i).y - Float(my);
            if dx * dx + dy * dy <= RADIUS * RADIUS then
                return i;
            end if;
        end loop;
        return -1;
    end Hit_Vertex;

    ----------------------------------------------------------------------------

    procedure Lclick( mx, my : Integer ) is
        i : Integer;
    begin
        ex.cur_vertex := Hit_Vertex( mx, my );

        if ex.cur_vertex < 0 and then ex.vertex_count < MAX_VERTICES then
            i := ex.vertex_count;
            ex.vertex_count := ex.vertex_count + 1;
            ex.vertices(i).x := Float(mx);
            ex.vertices(i).y := Float(my);
            ex.cur_vertex := i;

            if ex.add_hole = NEW_HOLE and then ex.cur_polygon < MAX_POLYGONS then
                ex.cur_polygon := ex.cur_polygon + 1;
                ex.add_hole := GROW_HOLE;
            end if;

            ex.vertex_polygon(i) := ex.cur_polygon;
        end if;
    end Lclick;

    ----------------------------------------------------------------------------

    procedure Rclick( mx, my : Integer ) is
        i : constant Integer := Hit_Vertex( mx, my );
    begin
        if i >= 0 and then ex.add_hole = NOT_ADDING_HOLE then
            ex.vertex_count := ex.vertex_count - 1;
            for j in i+1..ex.vertex_count loop
                ex.vertices(i-1) := ex.vertices(i);
            end loop;
            for j in i+1..ex.vertex_count loop
                ex.vertex_polygon(i-1) := ex.vertex_polygon(i);
            end loop;
        end if;
        ex.cur_vertex := -1;
    end Rclick;

    ----------------------------------------------------------------------------

    procedure Drag( mx, my : Integer ) is
    begin
        if ex.cur_vertex >= 0 then
            ex.vertices(ex.cur_vertex).x := Float(mx);
            ex.vertices(ex.cur_vertex).y := Float(my);
        end if;
    end Drag;

    ----------------------------------------------------------------------------

    procedure Scroll( mx, my : Integer ) is
    begin
        ex.scroll_x := ex.scroll_x + Float(mx);
        ex.scroll_y := ex.scroll_y + Float(my);
    end Scroll;

    ----------------------------------------------------------------------------

    function Join_Style_To_String( x : Allegro_Line_Join ) return String is
    ((case x is when ALLEGRO_LINE_JOIN_NONE  => "NONE",
                when ALLEGRO_LINE_JOIN_BEVEL => "BEVEL",
                when ALLEGRO_LINE_JOIN_ROUND => "ROUND",
                when ALLEGRO_LINE_JOIN_MITER => "MITER",
                when others => "Unknown"
    ));

    ----------------------------------------------------------------------------

    function Cap_Style_To_String( x : Allegro_Line_Cap ) return String is
    ((case x is when ALLEGRO_LINE_CAP_NONE     => "NONE",
                when ALLEGRO_LINE_CAP_SQUARE   => "SQUARE",
                when ALLEGRO_LINE_CAP_ROUND    => "ROUND",
                when ALLEGRO_LINE_CAP_TRIANGLE => "TRIANGLE",
                when ALLEGRO_LINE_CAP_CLOSED   => "CLOSED",
                when others => "Unknown"
    ));

    ----------------------------------------------------------------------------

    function Choose_Font return A_Allegro_Font is ((if ex.software then ex.fontbmp else ex.font));

    ----------------------------------------------------------------------------

    procedure Draw_Vertices is
        f     : constant A_Allegro_Font := Choose_Font;
        vertc : constant Allegro_Color := Al_Map_RGBA_f( 0.7, 0.0, 0.0, 0.7 );
        textc : constant Allegro_Color := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.7 );
    begin
        for i in 0..ex.vertex_count-1 loop
            Al_Draw_Filled_Circle( ex.vertices(i).x, ex.vertices(i).y, RADIUS, vertc );
            Al_Draw_Text( f, textc, ex.vertices(i).x + RADIUS, ex.vertices(i).y + RADIUS, 0, i'Img );
        end loop;
    end Draw_Vertices;

    ----------------------------------------------------------------------------

    procedure Compute_Polygon_Vertex_Counts( polygon_vertex_count : in out Integer_Array ) is
    begin
        polygon_vertex_count := (others => 0);
        for i in 0..ex.vertex_count-1 loop
            polygon_vertex_count(ex.vertex_polygon(i)) := polygon_vertex_count(ex.vertex_polygon(i)) + 1;
        end loop;
    end Compute_Polygon_Vertex_Counts;

    ----------------------------------------------------------------------------

    procedure Draw_All is
        f     : constant A_Allegro_Font := Choose_Font;
        textc : constant Allegro_Color := Al_Map_RGB( 0, 0, 0 );
        texth : constant Float := Float(Al_Get_Font_Line_Height( f )) * 1.5;
        textx : constant Float := 5.0;
        texty : Float := 5.0;
        t     : Allegro_Transform;
        holec : Allegro_Color;
    begin
        Al_Clear_To_Color( ex.bg );

        Al_Identity_Transform( t );
        Al_Translate_Transform( t, ex.scroll_x, ex.scroll_y );
        Al_Scale_Transform( t, ex.zoom, ex.zoom );
        Al_Use_Transform( t );

        case ex.mode is
            when MODE_POLYLINE =>
                if ex.vertex_count >= 2 then
                    Al_Draw_Polyline( ex.vertices(ex.vertices'First..ex.vertices'First+ex.vertex_count-1),
                                      ex.join_style, ex.cap_style, ex.fg, ex.thickness, ex.miter_limit );
                end if;
            when MODE_FILLED_POLYGON =>
                if ex.vertex_count >= 2 then
                    Al_Draw_Filled_Polygon( ex.vertices(ex.vertices'First..ex.vertices'First+ex.vertex_count-1), ex.fg );
                end if;
            when MODE_POLYGON =>
                if ex.vertex_count >= 2 then
                    Al_Draw_Polygon( ex.vertices(ex.vertices'First..ex.vertices'First+ex.vertex_count-1),
                                     ex.join_style, ex.cap_style, ex.fg, ex.thickness, ex.miter_limit );
                end if;
            when MODE_FILLED_HOLES =>
                if ex.vertex_count >= 2 then
                    declare
                        polygon_vertex_count : Integer_Array(0..MAX_POLYGONS);
                    begin
                        Compute_Polygon_Vertex_Counts( polygon_vertex_count );
                        Al_Draw_Filled_Polygon_With_Holes( ex.vertices(ex.vertices'First..ex.vertices'First+ex.vertex_count-1), polygon_vertex_count, ex.fg );
                    end;
                end if;
        end case;

        Draw_Vertices;

        Al_Identity_Transform( t );
        Al_Use_Transform( t );

        case ex.mode is
            when MODE_POLYLINE =>
                Al_Draw_Text( f, textc, textx, texty, 0, "al_draw_polyline (SPACE)" );
                texty := texty + texth;
            when MODE_FILLED_POLYGON =>
                Al_Draw_Text( f, textc, textx, texty, 0, "al_draw_filled_polygon (SPACE)" );
                texty := texty + texth;
            when MODE_POLYGON =>
                Al_Draw_Text( f, textc, textx, texty, 0, "al_draw_polygon (SPACE)" );
                texty := texty + texth;
            when MODE_FILLED_HOLES =>
                Al_Draw_Text( f, textc, textx, texty, 0, "al_draw_filled_polygon_with_holes (SPACE)" );
                texty := texty + texth;
        end case;

        Al_Draw_Text( f, textc, textx, texty, 0, "Line join style: " & Join_Style_To_String( ex.join_style ) & " (J)" );
        texty := texty + texth;

        Al_Draw_Text( f, textc, textx, texty, 0, "Line cap style: " & Cap_Style_To_String( ex.cap_style ) & " (C)" );
        texty := texty + texth;

        Al_Draw_Text( f, textc, textx, texty, 0, "Line thickness:  " & ex.thickness'Img & " (+/-)" );
        texty := texty + texth;

        Al_Draw_Text( f, textc, textx, texty, 0, "Miter limit:     " & ex.miter_limit'Img & " ([/])" );
        texty := texty + texth;

        Al_Draw_Text( f, textc, textx, texty, 0, "Zoom:            " & ex.zoom'Img & " (wheel)" );
        texty := texty + texth;

        Al_Draw_Text( f, textc, textx, texty, 0, (if ex.software then "Software rendering" else "Hardware rendering") & " (S)" );
        texty := texty + texth;

        Al_Draw_Text( f, textc, textx, texty, 0, "Reset (R)" );
        texty := texty + texth;

        if ex.add_hole = NOT_ADDING_HOLE then
            holec := textc;
        elsif ex.add_hole = GROW_HOLE then
            holec := Al_Map_RGB( 200, 0, 0 );
        else
            holec := Al_Map_RGB( 0, 200, 0 );
        end if;
        Al_Draw_Text( f, holec, textx, texty, 0, "Add Hole (" & ex.cur_polygon'Img & ") (H)" );
    end Draw_All;

    ----------------------------------------------------------------------------

    -- Print vertices in a format for the test suite.
    procedure Print_Vertices is
    begin
        for i in 0..ex.vertex_count-1 loop
            Log_Print( "v" & i'Img & " = " & ex.vertices(i).x'Img & ", " & ex.vertices(i).y'Img );
        end loop;
        Log_Print( "" );
    end Print_Vertices;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        event : Allegro_Event;
        mdown : Boolean := False;
        x, y  : Float;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        ex.display := Al_Create_Display( 800, 600 );
        if ex.display = null then
            Abort_Example( "Error creating display" );
        end if;

        ex.font := Al_Create_Builtin_Font;
        if ex.font = null then
            Abort_Example( "Error creating builtin font" );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        ex.dbuf := Al_Create_Bitmap( 800, 600 );
        ex.fontbmp := Al_Create_Builtin_Font;
        if ex.fontbmp = null then
            Abort_Example( "Error creating builtin font" );
        end if;

        ex.queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( ex.queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Display_Event_Source( ex.display ) );

        ex.bg := Al_Map_RGBA_f( 1.0, 1.0, 0.9, 1.0 );
        ex.fg := Al_Map_RGBA_f( 0.0, 0.5, 1.0, 1.0 );

        Reset;

        loop
            if Al_Is_Event_Queue_Empty( ex.queue ) then
                if ex.software then
                    Al_Set_Target_Bitmap( ex.dbuf );
                    Draw_All;
                    Al_Set_Target_Backbuffer( ex.display );
                    Al_Draw_Bitmap( ex.dbuf, 0.0, 0.0, 0 );
                else
                    Al_Set_Target_Backbuffer( ex.display );
                    Draw_All;
                end if;
                Al_Flip_Display;
            end if;

            Al_Wait_For_Event( ex.queue, event );
            exit when event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE;
            if event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                exit when event.keyboard.keycode = ALLEGRO_KEY_ESCAPE;
                case To_Upper( Character'Val( event.keyboard.unichar ) ) is
                    when ' ' =>
                        if ex.mode /= Mode_Type'Last then
                            ex.mode := Mode_Type'Succ( ex.mode );
                        else
                            ex.mode := Mode_Type'First;
                        end if;
                    when 'J' =>
                        ex.join_style := ex.join_style + 1;
                        if ex.join_style > ALLEGRO_LINE_JOIN_MITER then
                            ex.join_style := ALLEGRO_LINE_JOIN_NONE;
                        end if;
                    when 'C' =>
                        ex.cap_style := ex.cap_style + 1;
                        if ex.cap_style > ALLEGRO_LINE_CAP_CLOSED then
                            ex.cap_style := ALLEGRO_LINE_CAP_NONE;
                        end if;
                    when '+' =>
                        ex.thickness := ex.thickness + 0.25;
                    when '-' =>
                        ex.thickness := ex.thickness - 0.25;
                        if ex.thickness <= 0.0 then
                            ex.thickness := 0.0;
                        end if;
                    when '[' =>
                        ex.miter_limit := ex.miter_limit - 0.1;
                        if ex.miter_limit < 0.0 then
                            ex.miter_limit := 0.0;
                        end if;
                    when ']' =>
                        ex.miter_limit := ex.miter_limit + 0.1;
                        if ex.miter_limit >= 10.0 then
                            ex.miter_limit := 10.0;
                        end if;
                    when 'S' =>
                        ex.software := not ex.software;
                    when 'R' =>
                        Reset;
                    when 'P' =>
                        Print_Vertices;
                    when 'H' =>
                        ex.add_hole := NEW_HOLE;
                    when others => null;
                end case;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                x := Float(event.mouse.x);
                y := Float(event.mouse.y);
                Transform( x, y );
                if event.mouse.button = 1 then
                    Lclick( Integer(x), Integer(y) );
                end if;
                if event.mouse.button = 2 then
                    Rclick( Integer(x), Integer(y) );
                end if;
                if event.mouse.button = 3 then
                    mdown := True;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_UP then
                ex.cur_vertex := -1;
                if event.mouse.button = 3 then
                    mdown := False;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_AXES then
                x := Float(event.mouse.x);
                y := Float(event.mouse.y);
                Transform( x, y );

                if mdown then
                    Scroll( event.mouse.dx, event.mouse.dy );
                else
                    Drag( Integer(x), Integer(y) );
                end if;

                ex.zoom := ex.zoom * (0.9 ** Float(event.mouse.dz));
            end if;
        end loop;

        Al_Destroy_Display( ex.display );
        Close_Log( true );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Polygon;

