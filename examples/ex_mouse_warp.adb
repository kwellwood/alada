
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Mouse_Warp is

    procedure Ada_Main is
        width             : constant Float := 640.0;
        height            : constant Float := 480.0;

        font              : A_Allegro_Font;
        display           : A_Allegro_Display;
        event_queue       : A_Allegro_Event_Queue;
        event             : aliased Allegro_Event;
        right_button_down : Boolean := False;
        redraw            : Boolean := False;
        fake_x, fake_y    : Float := 0.0;
        white             : Allegro_Color;
    begin
        event.mouse.x := 0;    -- avoids a "referenced before initialized" warning
        event.mouse.y := 0;    --

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        Al_Set_New_Display_Flags( ALLEGRO_WINDOWED );
        display := Al_Create_Display( Integer(width), Integer(height) );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        event_queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( event_queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( event_queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( event_queue, Al_Get_Keyboard_Event_Source );

        font := Al_Create_Builtin_Font;
        white := Al_Map_RGB_f( 1.0, 1.0, 1.0 );

        loop
            if redraw and then Al_Is_Event_Queue_Empty( event_queue ) then
                Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );

                if right_button_down then
                    Al_Draw_Line( width / 2.0, height / 2.0, fake_x, fake_y,
                                  Al_Map_RGB_f( 1.0, 0.0, 0.0 ), 1.0 );
                    Al_Draw_Line( fake_x - 5.0, fake_y, fake_x + 5.0, fake_y,
                                  Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 2.0 );
                    Al_Draw_Line( fake_x, fake_y - 5.0, fake_x, fake_y + 5.0,
                                  Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 2.0 );
                end if;

                Al_Draw_Text( font, white, 0.0, 0.0, 0,
                              "x:" & event.mouse.x'Img &
                              " y:" & event.mouse.y'Img &
                              " dx:" & event.mouse.dx'Img &
                              "dy:" & event.mouse.dy'Img );
                Al_Draw_Text( font, white, width / 2.0,
                              height / 2.0 - Float(Al_Get_Font_Line_Height( font )),
                              ALLEGRO_ALIGN_CENTRE,
                              "Left-Click to warp pointer to the middle once." );
                Al_Draw_Text( font, white, width / 2.0, height / 2.0,
                              ALLEGRO_ALIGN_CENTRE,
                              "Hold right mouse button to constantly move pointer to the middle." );
                Al_Flip_Display;
                redraw := False;
            end if;

            Al_Wait_For_Event( event_queue, event );

            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_WARPED then
                Log_Print( "Warp" );    -- it warped
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_AXES then
                if right_button_down then
                    if Al_Set_Mouse_XY( display, Integer(width) / 2, Integer(height) / 2 ) then
                        null;
                    end if;
                    fake_x := fake_x + Float(event.mouse.dx);
                    fake_y := fake_y + Float(event.mouse.dy);
                end if;
                redraw := True;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                if event.mouse.button = 1 then
                    if Al_Set_Mouse_XY( display, Integer(width) / 2, Integer(height) / 2 ) then
                        null;
                    end if;
                end if;
                if event.mouse.button = 2 then
                    right_button_down := True;
                    fake_x := width / 2.0;
                    fake_y := height / 2.0;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_UP then
                if event.mouse.button = 2 then
                    right_button_down := False;
                end if;
            end if;
        end loop;

        Al_Destroy_Event_Queue( event_queue );
        Al_Destroy_Display( display );
        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Mouse_Warp;
