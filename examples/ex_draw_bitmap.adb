
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Displays.Monitors;         use Allegro.Displays.Monitors;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Touch_Input;               use Allegro.Touch_Input;
with Common;                            use Common;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;

package body Ex_Draw_Bitmap is

    procedure Ada_Main is

        FPS         : constant := 60;
        MAX_SPRITES : constant := 1024;

        type Sprite is
            record
                x,   y : Float;
                dx, dy : Float;
            end record;

        type A_String is access all String;
        type String_Array is array (Integer range <>) of A_String;

        text : constant String_Array := String_Array'(
            0 => new String'("H - toggle held drawing"),
            1 => new String'("Space - toggle use of textures"),
            2 => new String'("B - toggle alpha blending"),
            3 => new String'("Left/Right - change bitmap size"),
            4 => new String'("Up/Down - change bitmap count"),
            5 => new String'("F1 - toggle help text")
        );

        target_info : constant String_Array := String_Array'(
            0 => new String'("textures"),
            1 => new String'("memory buffers")
        );

        blend_info : constant String_Array := String_Array'(
            0 => new String'("alpha"),
            1 => new String'("additive"),
            2 => new String'("tinted"),
            3 => new String'("solid")
        );

        type Sprite_Array is array (Integer range <>) of Sprite;

        type Long_Float_Array is array (Integer range <>) of Long_Float;

        type Example_Rec is
            record
                sprites              : Sprite_Array(0..MAX_SPRITES-1);
                use_memory_bitmaps   : Boolean := False;
                blending             : Integer := 0;
                display              : A_Allegro_Display;
                mysha                : A_Allegro_Bitmap;
                bitmap               : A_Allegro_Bitmap;

                hold_bitmap_drawing  : Boolean := False;
                bitmap_size          : Integer := 0;
                sprite_count         : Integer := 0;
                show_help            : Boolean := True;
                font                 : A_Allegro_Font;

                t                    : Integer := 0;

                white,
                half_white,
                dark,
                red                  : Allegro_Color;

                direct_speed_measure : Long_Float := 0.0;

                ftpos                : Integer := 0;
                frame_times          : Long_Float_Array(0..FPS-1) := (others => 0.0);
            end record;

        example : Example_Rec;

        ------------------------------------------------------------------------

        procedure Add_Time is
        begin
            example.frame_times(example.ftpos) := Al_Get_Time;
            example.ftpos := example.ftpos + 1;
            if example.ftpos >= FPS then
                example.ftpos := 0;
            end if;
        end Add_Time;

        ------------------------------------------------------------------------

        procedure Get_fps( average, minmax : out Integer ) is
            prev   : Integer := FPS - 1;
            min_dt : Long_Float := 1.0;
            max_dt : Long_Float := 1.0 / 1_000_000.0;
            av     : Long_Float := 0.0;
            d      : Long_Float;
            dt     : Long_Float;
        begin
            for i in 0..FPS-1 loop
                if i /= example.ftpos then
                    dt := example.frame_times(i) - example.frame_times(prev);
                    if dt < min_dt then
                        min_dt := dt;
                        if min_dt < 0.000_001 then
                            min_dt := 0.000_001;
                        end if;
                    elsif dt > max_dt then
                        max_dt := dt;
                    end if;
                    av := av + dt;
                end if;
                prev := i;
            end loop;

            av := av / Long_Float(FPS - 1);
            if av < 0.000_001 then
                -- prevent overflow on startup
                average := 1_000_000;
            else
                average := Integer(Long_Float'Ceiling( 1.0 / av ));
            end if;
            d := 1.0 / min_dt - 1.0 / max_dt;
            minmax := Integer(Long_Float'Floor( d / 2.0 ));
        end Get_fps;

        ------------------------------------------------------------------------

        procedure Add_Sprite is
            w : constant Integer := Al_Get_Display_Width( example.display );
            h : constant Integer := Al_Get_Display_Height( example.display );
            g : GNAT.Random_Numbers.Generator;
            a : Float;
            s : Integer;
        begin
            if example.sprite_count < MAX_SPRITES then
                s := example.sprite_count;
                example.sprite_count := example.sprite_count + 1;

                Reset( g );
                a := Float(Integer'(Random( g )) mod 360);
                example.sprites(s).x := Float(Integer'(Random( g )) mod (w - example.bitmap_size));
                example.sprites(s).y := Float(Integer'(Random( g )) mod (h - example.bitmap_size));
                example.sprites(s).dx := Cos( a ) * Float(FPS) * 2.0;
                example.sprites(s).dy := Sin( a ) * Float(FPS) * 2.0;
            end if;
        end Add_Sprite;

        ------------------------------------------------------------------------

        procedure Add_Sprites( n : Integer ) is
        begin
            for i in 1..n loop
                Add_Sprite;
            end loop;
        end Add_Sprites;

        ------------------------------------------------------------------------

        procedure Remove_Sprites( n : Integer ) is
        begin
            example.sprite_count := Integer'Max( example.sprite_count - n, 0 );
        end Remove_Sprites;

        ------------------------------------------------------------------------

        procedure Change_Size( size : Integer ) is
            bw, bh : Integer;
        begin
            if size < 0 then
                return;
            end if;

            if example.bitmap /= null then
                Al_Destroy_Bitmap( example.bitmap );
            end if;
            if example.use_memory_bitmaps then
                Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
            else
                Al_Set_New_Bitmap_Flags( ALLEGRO_VIDEO_BITMAP );
            end if;

            example.bitmap := Al_Create_Bitmap( size, size );
            example.bitmap_size := size;
            Al_Set_Target_Bitmap( example.bitmap );
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            Al_Clear_To_Color( Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.0 ) );
            bw := Al_Get_Bitmap_Width( example.mysha );
            bh := Al_Get_Bitmap_Height( example.mysha );
            Al_Draw_Scaled_Bitmap( example.mysha, 0.0, 0.0, Float(bw), Float(bh), 0.0, 0.0, Float(size), Float(size), 0 );
            Al_Set_Target_Backbuffer( example.display );
        end Change_Size;

        ------------------------------------------------------------------------

        procedure Sprite_Update( s : in out Sprite ) is
            w : constant Integer := Al_Get_Display_Width( example.display );
            h : constant Integer := Al_Get_Display_Height( example.display );
        begin
            s.x := s.x + s.dx / Float(FPS);
            s.y := s.y + s.dy / Float(FPS);

            if s.x < 0.0 then
                s.x := -s.x;
                s.dx := -s.dx;
            elsif Integer(s.x) + example.bitmap_size > w then
                s.x := -s.x + 2.0 * Float(w - example.bitmap_size);
                s.dx := -s.dx;
            end if;
            if s.y < 0.0 then
                s.y := -s.y;
                s.dy := -s.dy;
            elsif Integer(s.y) + example.bitmap_size > h then
                s.y := -s.y + 2.0 * Float(h - example.bitmap_size);
                s.dy := -s.dy;
            end if;

            if example.bitmap_size > w then
                s.x := Float(w / 2 - example.bitmap_size / 2);
            end if;
            if example.bitmap_size > h then
                s.y := Float(h / 2 - example.bitmap_size / 2);
            end if;
        end Sprite_Update;

        ------------------------------------------------------------------------

        procedure Update is
        begin
            for i in 0..example.sprite_count-1 loop
                Sprite_Update( example.sprites(i) );
            end loop;

            example.t := example.t + 1;
            if example.t = 60 then
                example.t := 0;
            end if;
        end Update;

        ------------------------------------------------------------------------

        procedure Redraw is
            w      : constant Integer := Al_Get_Display_Width( example.display );
            h      : constant Integer := Al_Get_Display_Height( example.display );
            fh     : constant Integer := Al_Get_Font_Line_Height( example.font );
            dh     : Integer := Integer(Float(fh) * 3.5);
            f1, f2 : Integer;
            tint   : Allegro_Color := example.white;
        begin
            if example.blending = 0 then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                tint := example.half_white;
            elsif example.blending = 1 then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                tint := example.dark;
            elsif example.blending = 2 then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                tint := example.red;
            elsif example.blending = 3 then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            end if;

            if example.hold_bitmap_drawing then
                Al_Hold_Bitmap_Drawing( True );
            end if;
            for i in 0..example.sprite_count-1 loop
                Al_Draw_Tinted_Bitmap( example.bitmap, tint, example.sprites(i).x, example.sprites(i).y, 0 );
            end loop;
            if example.hold_bitmap_drawing then
                Al_Hold_Bitmap_Drawing( False );
            end if;

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            if example.show_help then
                for i in reverse 0..5 loop
                    Al_Draw_Text( example.font, example.white, 0.0, Float(h - dh), 0, text(i).all );
                    dh := dh + fh * 6;
                end loop;
            end if;

            Al_Draw_Text( example.font, example.white, 0.0, 0.0, 0, "count:" & Integer'Image( example.sprite_count ) );
            Al_Draw_Text( example.font, example.white, 0.0, Float(fh), 0, "size:" & Integer'Image( example.bitmap_size ) );
            Al_Draw_Text( example.font, example.white, 0.0, Float(fh * 2), 0, target_info(Boolean'Pos( example.use_memory_bitmaps )).all );
            Al_Draw_Text( example.font, example.white, 0.0, Float(fh * 3), 0, blend_info(example.blending).all );

            Get_fps( f1, f2 );
            Al_Draw_Text( example.font, example.white, Float(w), 0.0, ALLEGRO_ALIGN_RIGHT, "FPS:" & Integer'Image( f1 ) & " +- " & Integer'Image( f2 ) );
            if example.direct_speed_measure < 0.000_001 then
                -- prevent overflow on startup
                Al_Draw_Text( example.font, example.white, Float(w), Float(fh), ALLEGRO_ALIGN_RIGHT, " --- / sec" );
            else
                Al_Draw_Text( example.font, example.white, Float(w), Float(fh), ALLEGRO_ALIGN_RIGHT, Integer'Image( Integer(1.0 / example.direct_speed_measure) ) & " / sec" );
            end if;
        end Redraw;

        ------------------------------------------------------------------------

        x, y : Float := 0.0;
        w    : Float := 640.0;
        h    : Float := 480.0;

        procedure Click is
            fh     : Float;
            button : Integer;
            s      : Integer;
        begin
            fh := Float(Al_Get_Font_Line_Height( example.font ));

            if x < fh * 12.0 and then y >= h - fh * 30.0 then
                button := Integer((y - (h - fh * 30.0)) / (fh * 6.0));
                if button = 0 then
                    example.use_memory_bitmaps := not example.use_memory_bitmaps;
                    change_size( example.bitmap_size );
                end if;
                if button = 1 then
                    example.blending := example.blending + 1;
                    if example.blending = 4 then
                        example.blending := 0;
                    end if;
                end if;
                if button = 3 then
                    if x < fh * 6.0 then
                        remove_sprites( example.sprite_count / 2 );
                    else
                        add_sprites( example.sprite_count );
                    end if;
                end if;
                if button = 2 then
                    s := example.bitmap_size * 2;
                    if x < fh * 6.0 then
                        s := example.bitmap_size / 2;
                    end if;
                    change_size( s );
                end if;
                if button = 4 then
                    example.show_help := not example.show_help;
                end if;
            end if;
        end Click;

        ------------------------------------------------------------------------

        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        info        : aliased Allegro_Monitor_Info;
        event       : aliased Allegro_Event;
        done        : Boolean := False;
        need_redraw : Boolean := True;
        background  : Boolean := False;
        adapters    : Integer := 0;
            pragma Warnings( Off, adapters );

        ------------------------------------------------------------------------

    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init Allegro IIO addon." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        adapters := Al_Get_Num_Video_Adapters;

        Al_Get_Monitor_Info( 0, info );

        Al_Set_New_Display_Option( ALLEGRO_SUPPORTED_ORIENTATIONS,
                                   ALLEGRO_DISPLAY_ORIENTATION_ALL, ALLEGRO_SUGGEST );
        example.display := Al_Create_Display( Integer(w), Integer(h) );
        if example.display = null then
            Abort_Example( "Error creating display." );
        end if;

        w := Float(Al_Get_Display_Width( example.display ));
        h := Float(Al_Get_Display_Height( example.display ));

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;

       if not Al_Install_Touch_Input then
            null;
        end if;

        example.font := Al_Create_Builtin_Font;
        if example.font = null then
            Abort_Example( "Error creating builtin font." );
        end if;

        example.mysha := Al_Load_Bitmap( "data/mysha256x256.png" );
        if example.mysha = null then
            Abort_Example( "Error loading data/mysha256x256.png" );
        end if;

        example.white := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
        example.half_white := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 0.5 );
        example.dark := Al_Map_RGB( 15, 15, 15 );
        example.red := Al_Map_RGB_f( 1.0, 0.2, 0.1 );
        Change_Size( 256 );
        Add_Sprite;
        Add_Sprite;

        timer := Al_Create_Timer( 1.0 / Long_Float(FPS) );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        if Al_Install_Touch_Input then
            Al_Register_Event_Source( queue, Al_Get_Touch_Input_Event_Source );
        end if;
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( example.display ) );

        Al_Start_Timer( timer );

        while not done loop
            w := Float(Al_Get_Display_Width( example.display ));
            h := Float(Al_Get_Display_Height( example.display ));

            if not background and then need_redraw and then Al_Is_Event_Queue_Empty( queue ) then
                declare
                    t : Long_Float := -Al_Get_Time;
                begin
                    Add_Time;
                    Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0) );
                    Redraw;
                    t := t + Al_Get_Time;
                    example.direct_speed_measure := t;
                    Al_Flip_Display;
                    need_redraw := False;
                end;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_CHAR => -- includes repeats
                    case event.keyboard.keycode is
                        when ALLEGRO_KEY_ESCAPE =>
                            done := True;
                        when ALLEGRO_KEY_UP =>
                            Add_Sprites( 1 );
                        when ALLEGRO_KEY_DOWN =>
                            Remove_Sprites( 1 );
                        when ALLEGRO_KEY_LEFT =>
                            Change_Size( example.bitmap_size - 1 );
                        when ALLEGRO_KEY_RIGHT =>
                            Change_Size( example.bitmap_size + 1 );
                        when ALLEGRO_KEY_F1 =>
                            example.show_help := not example.show_help;
                        when ALLEGRO_KEY_SPACE =>
                            example.use_memory_bitmaps := not example.use_memory_bitmaps;
                            Change_Size( example.bitmap_size );
                        when ALLEGRO_KEY_B =>
                            example.blending := (example.blending + 1) mod 4;
                        when ALLEGRO_KEY_H =>
                            example.hold_bitmap_drawing := not example.hold_bitmap_drawing;
                        when others =>
                            null;
                    end case;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    done := True;

                when ALLEGRO_EVENT_DISPLAY_HALT_DRAWING =>
                    background := True;
                    Al_Acknowledge_Drawing_Halt( event.display.source );

                when ALLEGRO_EVENT_DISPLAY_RESUME_DRAWING =>
                    background := False;
                    Al_Acknowledge_Drawing_Resume( event.display.source );

                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                    Al_Acknowledge_Resize( event.display.source );

                when ALLEGRO_EVENT_TIMER =>
                    Update;
                    need_redraw := True;

                when ALLEGRO_EVENT_TOUCH_BEGIN =>
                    x := event.touch.x;
                    y := event.touch.y;
                    Click;

                when ALLEGRO_EVENT_MOUSE_BUTTON_UP =>
                    x := Float(event.mouse.x);
                    y := Float(event.mouse.y);
                    Click;

                when others =>
                    null;

            end case;
        end loop;

        Al_Destroy_Event_Queue( queue );
        Al_Destroy_Timer( timer );
        Al_Destroy_Bitmap( example.mysha );
        Al_Destroy_Font( example.font );
        AL_Destroy_Display( example.display );
        Al_Destroy_Bitmap( example.bitmap );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Draw_Bitmap;
