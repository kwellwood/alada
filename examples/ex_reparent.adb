
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Reparent is

    FPS : constant := 60.0;

    type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

    type Example_Type is
        record
            display : A_Allegro_Display;
            glyphs  : Bitmap_Array(0..1);
            sub     : A_Allegro_Bitmap;
            i, p    : Integer := 0;
        end record;

    example : Example_Type;

    ----------------------------------------------------------------------------

    -- Draw the parent bitmaps.
    procedure Draw_Glyphs is
        x, y : Float := 0.0;
    begin
        Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
        Al_Draw_Bitmap( example.glyphs(0), 0.0, 0.0, 0 );
        Al_Draw_Bitmap( example.glyphs(1), 0.0, 32.0 * 6.0, 0 );
        if example.p = 1 then
            y := 32.0 * 6.0;
        end if;

        x := x + Float(Al_Get_Bitmap_X( example.sub ));
        y := y + Float(Al_Get_Bitmap_Y( example.sub ));
        Al_Draw_Filled_Rectangle( x, y,
                                  x + Float(Al_Get_Bitmap_Width( example.sub )),
                                  y + Float(Al_Get_Bitmap_Height( example.sub )),
                                  Al_Map_RGBA_f( 0.5, 0.0, 0.0, 0.5 ) );
    end Draw_Glyphs;

    ----------------------------------------------------------------------------

    procedure Redraw is
    begin
        Draw_Glyphs;

        -- Here we draw example.sub, which is never re-created, just
        -- re-parented.
        --
        for i in 0..7 loop
            Al_Draw_Scaled_Rotated_Bitmap( example.sub, 0.0, 0.0,
                                           Float(i) * 100.0, 32.0 * 6.0 + 33.0 * 5.0 + 4.0,
                                           2.0, 2.0, ALLEGRO_PI / 4.0 * Float(i) / 8.0, 0 );
        end loop;
    end Redraw;

    ----------------------------------------------------------------------------

    procedure Update is
        i : Integer;
    begin
        -- Re-parent out sub bitmap, jumping from glyph to glyph.
        example.i := example.i + 1;
        i := (example.i / 4) mod (16 * 6 + 20 * 5);
        if i < 16 * 6 then
            example.p := 0;
            Al_Reparent_Bitmap( example.sub, example.glyphs(0),
                                (i mod 16) * 32, (i / 16) * 32, 32, 32 );
        else
            example.p := 1;
            i := i - 16 * 6;
            Al_Reparent_Bitmap( example.sub, example.glyphs(1),
                                (i mod 20) * 37, (i / 20) * 33, 37, 33 );
        end if;
    end Update;

    ----------------------------------------------------------------------------

    procedure Init is
    begin
        -- We create out sub bitmap once then use it throughout the
        -- program, re-parenting as needed.
        --
        example.sub := Al_Create_Sub_Bitmap( example.glyphs(0), 0, 0, 32, 32 );
    end Init;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        w           : Integer := 800;
        h           : Integer := 600;
        done        : Boolean := False;
        need_redraw : Boolean := True;
        event       : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR );
        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );
        example.display := Al_Create_Display( w, h );
        if example.display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
           Abort_Example( "Error installing keyboard." );
        end if;

        example.glyphs(0) := Al_Load_Bitmap( "data/font.tga" );
        if example.glyphs(0) = null then
            Abort_Example( "Error loading data/font.tga" );
        end if;

        example.glyphs(1) := Al_Load_Bitmap( "data/bmpfont.tga" );
        if example.glyphs(1) = null then
            Abort_Example( "Error loading data/bmpfont.tga" );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / FPS );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( example.display ) );

        Al_Start_Timer( timer );

        while not done loop
            w := Al_Get_Display_Width( example.display );
            h := Al_Get_Display_Height( example.display );

            if need_redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Redraw;
                Al_Flip_Display;
                need_redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_CHAR =>
                   if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                       done := True;
                   end if;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                   done := True;

                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                   Al_Acknowledge_Resize( event.display.source );

                when ALLEGRO_EVENT_TIMER =>
                   Update;
                   need_redraw := True;

                when others => null;
            end case;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Reparent;
