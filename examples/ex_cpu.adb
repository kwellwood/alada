
with Allegro;                           use Allegro;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.CPUs;                      use Allegro.CPUs;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;

-- An example showing the use of al_get_cpu_count() and al_get_ram_size().
package body Ex_Cpu is

    INTERVAL : constant := 0.1;

    procedure Ada_Main is
        display    : A_Allegro_Display;
        timer      : A_Allegro_Timer;
        queue      : A_Allegro_Event_Queue;
        font       : A_Allegro_Font;
        done       : Boolean := False;
        redraw     : Boolean := True;
        event      : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        font := Al_Create_Builtin_Font;

        timer := Al_Create_Timer( INTERVAL );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        Al_Start_Timer( timer );

        Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );

        while not done loop
            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Al_Clear_To_Color( Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 ) );
                Al_Draw_Text( font, Al_Map_RGBA_f( 1.0, 1.0, 0.0, 1.0 ), 16.0, 16.0, 0,
                              "Amount of CPU cores detected:" & Al_Get_CPU_Count'Img & "." );
                Al_Draw_Text( font, Al_Map_RGBA_f( 0.0, 1.0, 1.0, 1.0 ), 16.0, 32.0, 0,
                              "Size of random access memory:" & Al_Get_Ram_Size'Img & " MiB." );
                Al_Flip_Display;
                redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    end if;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    done := True;

                when ALLEGRO_EVENT_TIMER =>
                    redraw := True;

                when others => null;
            end case;
        end loop;

        Al_Destroy_Font( font );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Cpu;

