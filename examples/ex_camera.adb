
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;

-- An example demonstrating how to use ALLEGRO_TRANSFORM to represent a 3D
-- camera.
package body Ex_Camera is

    pi : constant := ALLEGRO_PI;

    type Boolean_Array is array (Integer range <>) of Boolean;

    type Vector is
        record
            x, y, z : Float := 0.0;
        end record;

    type Camera_Type is
        record
            position : Vector;
            xaxis    : Vector;    -- This represent the direction looking to the right.
            yaxis    : Vector;    -- This is the up direction.
            zaxis    : Vector;    -- This is the direction towards the viewer ('backwards').
            vertical_field_of_View : Float;   -- In radians.
        end record;

    type String_Array is array (Integer range <>) of Unbounded_String;

    type Example is
        record
            camera : Camera_Type;

            -- controls sensitivity
            mouse_look_speed : Float;
            movement_speed   : Float;

            -- keyboard and mouse state
            button   : Boolean_Array(0..9) := (others => False);
            key      : Boolean_Array(0..ALLEGRO_KEY_MAX-1) := (others => False);
            keystate : Boolean_Array(0..ALLEGRO_KEY_MAX-1) := (others => False);
            mouse_dx,
            mouse_dy : Integer;

            -- control scheme selection
            controls : Integer;
            controls_names : String_Array(0..3);

            -- the vertex data
            n : Integer;
            v : A_Allegro_Vertex_Array := new Allegro_Vertex_Array(0..0);

            -- used to draw some info text
            font : A_Allegro_Font;

            -- if not NULL the skybox picture to use
            skybox : A_Allegro_Bitmap;
         end record;

    ex : Example;

    ----------------------------------------------------------------------------

    -- Calculate the dot product between two vectors. This corresponds to the
    -- angle between them times their lengths.
    --
    function Vector_Dot_Product( a, b : Vector ) return Float is
    (a.x * b.x + a.y * b.y + a.z * b.z);

    ----------------------------------------------------------------------------

    -- Calculate the cross product of two vectors. This produces a normal to the
    -- plane containing the operands.
    --
    function Vector_Cross_Product( a, b : Vector ) return Vector is
    ((a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x));

    ----------------------------------------------------------------------------

    -- Return a vector multiplied by a scalar.
    function Vector_Mul( a : Vector; s : Float ) return Vector is
    ((a.x * s, a.y * s, a.z * s));

    ----------------------------------------------------------------------------

    -- Return the vector norm (length).
    function Vector_Norm( a : Vector ) return Float is
    (Sqrt( Vector_Dot_Product( a, a ) ));

    ----------------------------------------------------------------------------

    -- Return a normalized version of the given vector.
    function Vector_Normalize( a : Vector ) return Vector is
        s : constant Float := Vector_Norm( a );
    begin
        if s = 0.0 then
            return a;
        end if;
        return Vector_Mul( a, 1.0 / s );
    end Vector_Normalize;

    ----------------------------------------------------------------------------

    -- In-place add another vector to a vector.
    procedure Vector_Iadd( a : in out Vector; b : Vector ) is
    begin
        a.x := a.x + b.x;
        a.y := a.y + b.y;
        a.z := a.z + b.z;
    end Vector_Iadd;

    ----------------------------------------------------------------------------

    -- Rotate the camera around the given axis.
    procedure Camera_Rotate_Around_Axis( c : in out Camera_Type; axis : Vector; radians : Float ) is
        t : Allegro_Transform;
    begin
        Al_Identity_Transform( t );
        Al_Rotate_Transform_3d( t, axis.x, axis.y, axis.z, radians );
        Al_Transform_Coordinates_3d( t, c.yaxis.x, c.yaxis.y, c.yaxis.z );
        Al_Transform_Coordinates_3d( t, c.zaxis.x, c.zaxis.y, c.zaxis.z );

        -- Make sure the axes remain orthogonal to each other.
        c.zaxis := Vector_Normalize( c.zaxis );
        c.xaxis := Vector_Cross_Product( c.yaxis, c.zaxis );
        c.xaxis := Vector_Normalize( c.xaxis );
        c.yaxis := Vector_Cross_Product( c.zaxis, c.xaxis );
    end Camera_Rotate_Around_Axis;

    ----------------------------------------------------------------------------

    -- Move the camera along its x axis and z axis (which corresponds to
    -- right and backwards directions).
    procedure Camera_Move_Along_Direction( camera : in out Camera_Type; right, forward : Float ) is
    begin
        Vector_Iadd( camera.position, Vector_Mul( camera.xaxis, right ) );
        Vector_Iadd( camera.position, Vector_Mul( camera.zaxis, -forward ) );
    end Camera_Move_Along_Direction;

    ----------------------------------------------------------------------------

    -- Get a vector with y = 0 looking in the opposite direction as the camera z
    -- axis. If looking straight up or down returns a 0 vector instead.
    function Get_Ground_Forward_Vector( camera : Camera_Type ) return Vector is
        move : Vector := Vector_Mul( camera.zaxis, 1.0 );
    begin
        move.y := 0.0;
        return Vector_Normalize( move );
    end Get_Ground_Forward_Vector;

    ----------------------------------------------------------------------------

    -- Get a vector with y = 0 looking in the same direction as the camera x axis.
    -- If looking straight up or down returns a 0 vector instead.
    function Get_Ground_Right_Vector( camera : Camera_Type ) return Vector is
        move : Vector := Vector_Mul( camera.xaxis, 1.0 );
    begin
        move.y := 0.0;
        return Vector_Normalize( move );
    end Get_Ground_Right_Vector;

    ----------------------------------------------------------------------------

    -- Like camera_move_along_direction but moves the camera along the ground plane
    -- only.
    procedure Camera_Move_Along_Ground( camera : in out Camera_Type; right, forward : Float ) is
        f : constant Vector := Get_Ground_Forward_Vector( camera );
        r : constant Vector := Get_Ground_Right_Vector( camera );
    begin
        camera.position.x := camera.position.x + f.x * forward + r.x * right;
        camera.position.z := camera.position.z + f.z * forward + r.z * right;
    end Camera_Move_Along_Ground;

    ----------------------------------------------------------------------------

    -- Calculate the pitch of the camera. This is the angle between the z axis
    -- vector and our direction vector on the y = 0 plane.
    function Get_Pitch( camera : Camera_Type ) return Float is
        f : constant Vector := Get_Ground_Forward_Vector( camera );
    begin
        return Arcsin( Vector_Dot_Product( f, camera.yaxis ) );
    end Get_Pitch;

    ----------------------------------------------------------------------------

    -- Calculate the yaw of the camera. This is basically the compass direction.
    function Get_Yaw( camera : Camera_Type ) return Float is
    (Arctan( camera.zaxis.x, camera.zaxis.z ));

    ----------------------------------------------------------------------------

    -- Calculate the roll of the camera. This is the angle between the x axis
    -- vector and its project on the y = 0 plane.
    function Get_Roll( camera : Camera_Type ) return Float is
        r : constant Vector := Get_Ground_Right_Vector( camera );
    begin
        return Arcsin( Vector_Dot_Product( r, camera.yaxis ) );
    end Get_Roll;

    ----------------------------------------------------------------------------

    -- Set up a perspective transform. We make the screen span
    -- 2 vertical units (-1 to +1) with square pixel aspect and the camera's
    -- vertical field of view. Clip distance is always set to 1.
    procedure Setup_3d_Projection is
        projection : Allegro_Transform;
        display    : constant A_Allegro_Display := Al_Get_Current_Display;
        dw         : constant Float := Float(Al_Get_Display_Width( display ));
        dh         : constant Float := Float(Al_Get_Display_Height( display ));
        f          : Float;
    begin
        Al_Identity_Transform( projection );
        Al_Translate_Transform_3d( projection, 0.0, 0.0, -1.0 );
        f := Tan( ex.camera.vertical_field_of_view / 2.0 );
        Al_Perspective_Transform( projection, -1.0 * dw / dh * f, f, 1.0,
                                  f * dw / dh, -f, 1000.0 );
        Al_Use_Projection_Transform( projection );
    end Setup_3d_Projection;

    ----------------------------------------------------------------------------

    -- Adds a new vertex to our scene.
    procedure Add_Vertex( x, y, z, u, v : Float; color : Allegro_Color ) is
        i  : Integer;
        v2 : A_Allegro_Vertex_Array;
    begin
        i := ex.n;
        ex.n := ex.n + 1;
        if i > ex.v'Last then
            v2 := new Allegro_Vertex_Array(0..i);
            v2(0..ex.v'Last) := ex.v(0..ex.v'Last);
            Delete( ex.v );
            ex.v := v2;
        end if;

        ex.v(i).x := x;
        ex.v(i).y := y;
        ex.v(i).z := z;
        ex.v(i).u := u;
        ex.v(i).v := v;
        ex.v(i).color := color;
    end Add_Vertex;

    ----------------------------------------------------------------------------

    -- Adds two triangles (6 vertices) to the scene.
    procedure Add_Quad( x, y, z, u, v      : Float;
                        ux, uy, uz, uu, uv : Float;
                        vx, vy, vz, vu, vv : Float;
                        c1, c2             : Allegro_Color ) is
    begin
        Add_Vertex( x, y, z, u, v, c1 );
        Add_Vertex( x + ux, y + uy, z + uz, u + uu, v + uv, c1 );
        Add_Vertex( x + vx, y + vy, z + vz, u + vu, v + vv, c2 );
        Add_Vertex( x + vx, y + vy, z + vz, u + vu, v + vv, c2 );
        Add_Vertex( x + ux, y + uy, z + uz, u + uu, v + uv, c1 );
        Add_Vertex( x + ux + vx, y + uy + vy, z + uz + vz, u + uu + vu, v + uv + vv, c2 );
    end Add_Quad;

    ----------------------------------------------------------------------------

    -- Create a checkerboard made from colored quads.
    procedure Add_Checkerboard is
        c1 : constant Allegro_Color := Al_Color_Name( "yellow" );
        c2 : constant Allegro_Color := Al_Color_Name( "green" );
        c  : Allegro_Color;
        px, py, pz : Float;
    begin
        for y in 0..19 loop
            for x in 0..19 loop
                px := Float(x) - 20.0 / 2.0;
                py := 0.2;
                pz := Float(y) - 20.0 / 2.0;
                c := c1;
                if (x + y) mod 2 = 1 then
                    c := c2;
                    py := py - 0.1;
                end if;
                Add_Quad( px, py, pz, 0.0, 0.0,
                          1.0, 0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 1.0, 0.0, 0.0,
                          c, c );
            end loop;
        end loop;
    end Add_Checkerboard;

    ----------------------------------------------------------------------------

    -- Create a skybox. This is simply 5 quads with a fixed distance to the
    -- camera.
    procedure Add_Skybox is
        c1 : Allegro_Color := Al_Color_Name( "black" );
        c2 : Allegro_Color := Al_Color_Name( "blue" );
        c3 : constant Allegro_Color := Al_Color_Name( "white" );
        p  : constant Vector := ex.camera.position;
        a  : Float := 0.0;
        b  : Float := 0.0;
    begin
        if ex.skybox /= null then
            a := Float(Al_Get_Bitmap_Width( ex.skybox )) / 4.0;
            b := Float(Al_Get_Bitmap_Height( ex.skybox )) / 3.0;
            c1 := c3;
            c2 := c3;
        end if;

        -- Back skybox wall.
        Add_Quad( p.x - 50.0, p.y, p.z - 50.0, a * 4.0, b * 2.0,
                  100.0, 0.0, 0.0, -a, 0.0,
                  0.0, 100.0, 0.0, 0.0, -b,
                  c1, c2 );

        -- Front skybox wall.
        Add_Quad( p.x - 50.0, p.y, p.z + 50.0, a, b * 2.0,
                  100.0, 0.0, 0.0, a, 0.0,
                  0.0, 100.0, 0.0, 0.0, -b,
                  c1, c2 );

        -- Left skybox wall.
        Add_Quad( p.x - 50.0, p.y, p.z - 50.0, 0.0, b * 2.0,
                  0.0, 0.0, 100.0, a, 0.0,
                  0.0, 100.0, 0.0, 0.0, -b,
                  c1, c2 );

        -- Right skybox wall.
        Add_Quad( p.x + 50.0, p.y, p.z - 50.0, a * 3.0, b * 2.0,
                  0.0, 0.0, 100.0, -a, 0.0,
                  0.0, 100.0, 0.0, 0.0, -b,
                  c1, c2 );

        -- Top of skybox.
        Add_Vertex( p.x - 50.0, p.y + 50.0, p.z - 50.0, a,       0.0,     c2 );
        Add_Vertex( p.x + 50.0, p.y + 50.0, p.z - 50.0, a * 2.0, 0.0,     c2 );
        Add_Vertex( p.x,        p.y + 50.0, p.z,        a * 1.5, b * 0.5, c3 );

        Add_Vertex( p.x + 50.0, p.y + 50.0, p.z - 50.0, a * 2.0, 0.0,     c2 );
        Add_Vertex( p.x + 50.0, p.y + 50.0, p.z + 50.0, a * 2.0, b,       c2 );
        Add_Vertex( p.x,        p.y + 50.0, p.z,        a * 1.5, b * 0.5, c3 );

        Add_Vertex( p.x + 50.0, p.y + 50.0, p.z + 50.0, a * 2.0, b,       c2 );
        Add_Vertex( p.x - 50.0, p.y + 50.0, p.z + 50.0, a,       b,       c2 );
        Add_Vertex( p.x,        p.y + 50.0, p.z,        a * 1.5, b * 0.5, c3 );

        Add_Vertex( p.x - 50.0, p.y + 50.0, p.z + 50.0, a,       b,       c2 );
        Add_Vertex( p.x - 50.0, p.y + 50.0, p.z - 50.0, a,       0.0,     c2 );
        Add_Vertex( p.x,        p.y + 50.0, p.z,        a * 1.5, b * 0.5, c3 );
    end Add_Skybox;

    ----------------------------------------------------------------------------

    procedure Draw_Scene is
        -- We save Allegro's projection so we can restore it for drawing text.
        projection : Allegro_Transform;
        t          : Allegro_Transform;
        back       : constant Allegro_Color := Al_Color_Name( "black" );
        front      : constant Allegro_Color := Al_Color_Name( "white" );
        th         : Float;
        pitch,
        yaw,
        roll       : Float;
    begin
        Al_Copy_Transform( projection, Al_Get_Current_Projection_Transform.all );

        Setup_3d_Projection;
        Al_Clear_To_Color( back );

        -- We use a depth buffer.
        Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 1 );
        Al_Clear_Depth_Buffer( 1.0 );

        -- Recreate the entire scene geometry - this is only a very small example
        -- so this is fine.
        ex.n := 0;
        Add_Checkerboard;
        Add_Skybox;

        -- Construct a transform corresponding to our camera. This is an inverse
        -- translation by the camera position, followed by an inverse rotation
        -- from the camera orientation.
        Al_Build_Camera_Transform( t,
                                   ex.camera.position.x, ex.camera.position.y, ex.camera.position.z,
                                   ex.camera.position.x - ex.camera.zaxis.x,
                                   ex.camera.position.y - ex.camera.zaxis.y,
                                   ex.camera.position.z - ex.camera.zaxis.z,
                                   ex.camera.yaxis.x, ex.camera.yaxis.y, ex.camera.yaxis.z );
        Al_Use_Transform( t );
        Al_Draw_Prim( ex.v.all, ex.skybox, 0, ex.n, ALLEGRO_PRIM_TRIANGLE_LIST );

        -- Restore projection.
        Al_Identity_Transform( t );
        Al_Use_Transform( t );
        Al_Use_Projection_Transform( projection );
        Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 0 );

        -- Draw some text.
        th := Float(Al_Get_Font_Line_Height( ex.font ));
        Al_Draw_Text( ex.font, front, 0.0, 0.0, 0,
                      "look: " & ex.camera.zaxis.x'Img & "/" & ex.camera.zaxis.y'Img & "/" & ex.camera.zaxis.z'Img &
                      " (change with left mouse button and drag)" );
        pitch := Get_Pitch( ex.camera ) * 180.0 / pi;
        yaw := Get_Yaw( ex.camera ) * 180.0 / pi;
        roll := Get_Roll( ex.camera ) * 180.0 / pi;
        Al_Draw_Text( ex.font, front, 0.0, th * 1.0, 0, "pitch: " & pitch'Img & " yaw: " & yaw'Img & " roll: " & roll'Img );
        Al_Draw_Text( ex.font, front, 0.0, th * 2.0, 0, "vertical field of view:" & Integer'Image( Integer(ex.camera.vertical_field_of_view * 180.0 / pi) ) & " (change with Z/X)" );
        Al_Draw_Text( ex.font, front, 0.0, th * 3.0, 0, "move with WASD or cursor" );
        Al_Draw_Text( ex.font, front, 0.0, th * 4.0, 0, "control style: " & To_String( ex.controls_names(ex.controls) ) & " (space to change)" );
    end Draw_Scene;

    ----------------------------------------------------------------------------

    procedure Setup_Scene is
    begin
        ex.camera.xaxis.x := 1.0;
        ex.camera.yaxis.y := 1.0;
        ex.camera.zaxis.z := 1.0;
        ex.camera.position.y := 2.0;
        ex.camera.vertical_field_of_view := 60.0 * pi / 180.0;

        ex.mouse_look_speed := 0.03;
        ex.movement_speed := 0.05;

        ex.controls_names(0) := To_Unbounded_String( "FPS" );
        ex.controls_names(1) := To_Unbounded_String( "airplane" );
        ex.controls_names(2) := To_Unbounded_String( "spaceship" );

        ex.font := Al_Create_Builtin_Font;
    end Setup_Scene;

    ----------------------------------------------------------------------------

    procedure Handle_Input is
        x, y : Float := 0.0;
        xy   : Float := 0.0;
        m    : Float;
    begin
        if ex.key(ALLEGRO_KEY_A) or ex.key(ALLEGRO_KEY_LEFT) then x := -1.0; end if;
        if ex.key(ALLEGRO_KEY_S) or ex.key(ALLEGRO_KEY_DOWN) then y := -1.0; end if;
        if ex.key(ALLEGRO_KEY_D) or ex.key(ALLEGRO_KEY_RIGHT) then x := 1.0; end if;
        if ex.key(ALLEGRO_KEY_W) or ex.key(ALLEGRO_KEY_UP) then y := 1.0; end if;

        -- Change field of view with Z/X.
        if ex.key(ALLEGRO_KEY_Z) then
            m := 20.0 * pi / 180.0;
            ex.camera.vertical_field_of_view := ex.camera.vertical_field_of_view - 0.01;
            if ex.camera.vertical_field_of_view < m then
                ex.camera.vertical_field_of_view := m;
            end if;
        end if;
        if ex.key(ALLEGRO_KEY_X) then
            m := 120.0 * pi / 180.0;
            ex.camera.vertical_field_of_view := ex.camera.vertical_field_of_view + 0.01;
            if ex.camera.vertical_field_of_view > m then
                ex.camera.vertical_field_of_view := m;
            end if;
        end if;

        -- In FPS style, always move the camera to height 2.
        if ex.controls = 0 then
            if ex.camera.position.y > 2.0 then
                ex.camera.position.y := ex.camera.position.y - 0.1;
            elsif ex.camera.position.y < 2.0 then
                ex.camera.position.y := 2.0;
            end if;
        end if;

        -- Set the roll (leaning) angle to 0 if not in airplane style.
        if ex.controls = 0 or ex.controls = 2 then
            Camera_Rotate_Around_Axis( ex.camera, ex.camera.zaxis, Get_Roll( ex.camera ) / 60.0 );
        end if;

        -- Move the camera, either freely or along the ground.
        xy := Sqrt( x * x + y * y );
        if xy > 0.0 then
            x := x / xy;
            y := y / xy;
            if ex.controls = 0 then
                Camera_Move_Along_Ground( ex.camera, ex.movement_speed * x, ex.movement_speed * y );
            elsif ex.controls = 1 or ex.controls = 2 then
                Camera_Move_Along_Direction( ex.camera, ex.movement_speed * x, ex.movement_speed * y );
            end if;
        end if;

        -- Rotate the camera, either freely or around world up only.
        if ex.button(1) then
            if ex.controls = 0 or ex.controls = 2 then
                Camera_Rotate_Around_Axis( ex.camera, ex.camera.xaxis, -ex.mouse_look_speed * Float(ex.mouse_dy) );
                Camera_Rotate_Around_Axis( ex.camera, Vector'(0.0, 1.0, 0.0), -ex.mouse_look_speed * Float(ex.mouse_dx) );
            elsif ex.controls = 1 then
                Camera_Rotate_Around_Axis( ex.camera, ex.camera.xaxis, -ex.mouse_look_speed * Float(ex.mouse_dy) );
                Camera_Rotate_Around_Axis( ex.camera, ex.camera.zaxis, -ex.mouse_look_speed * Float(ex.mouse_dx) );
            end if;
        end if;
    end Handle_Input;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
        queue   : A_Allegro_Event_Queue;
        redraw  : Boolean := True;
        event   : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        Init_Platform_Specific;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;

        Al_Set_New_Display_Option( ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST );
        Al_Set_New_Display_Option( ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST );
        Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, 16, ALLEGRO_SUGGEST );
        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );
        display := Al_Create_Display( 640, 360 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        if Argument_Count > 0 then
            if Al_Init_Image_Addon then
                ex.skybox := Al_Load_Bitmap( Argument( 1 ) );
            end if;
            if ex.skybox /= null then
                Ada.Text_IO.Put_Line( "Loaded skybox " & Argument( 1 ) & ":" &
                                      Integer'Image( Al_Get_Bitmap_Width( ex.skybox ) ) & " x" &
                                      Integer'Image( Al_Get_Bitmap_Height( ex.skybox ) ) );
            else
                Ada.Text_IO.Put_Line( "Failed loading skybox " & Argument( 1 ) );
            end if;
        end if;

        timer := Al_Create_Timer( 1.0 / 60.0 );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Setup_Scene;

        Al_Start_Timer( timer );
        loop
            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    exit;
                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                    Al_Acknowledge_Resize( display );
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exit;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        ex.controls := (ex.controls + 1) mod 3;
                    end if;
                    ex.key(event.keyboard.keycode) := True;
                    ex.keystate(event.keyboard.keycode) := True;
                when ALLEGRO_EVENT_KEY_UP =>
                    -- In case a key gets pressed and immediately released, we will still
                    -- have set ex.key so it is not lost.
                    ex.keystate(event.keyboard.keycode) := False;
                when ALLEGRO_EVENT_TIMER =>
                    Handle_Input;
                    redraw := True;

                    -- Reset keyboard state for keys not held down anymore.
                    for i in 0..ALLEGRO_KEY_MAX-1 loop
                        if not ex.keystate(i) then
                            ex.key(i) := False;
                        end if;
                    end loop;

                    ex.mouse_dx := 0;
                    ex.mouse_dy := 0;
                when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                    ex.button(Integer(event.mouse.button)) := True;
                when ALLEGRO_EVENT_MOUSE_BUTTON_UP =>
                    ex.button(Integer(event.mouse.button)) := False;
                when ALLEGRO_EVENT_MOUSE_AXES =>
                    ex.mouse_dx := ex.mouse_dx + event.mouse.dx;
                    ex.mouse_dy := ex.mouse_dy + event.mouse.dy;
                when others => null;
            end case;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Draw_Scene;

                Al_Flip_Display;
                redraw := False;
            end if;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Camera;

