
with Ada.Text_IO;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Allegro.System;                    use Allegro.System;
with GNAT.OS_Lib;

package body Common is

    procedure Init_Platform_Specific is
    begin
        null;
    end Init_Platform_Specific;

    ----------------------------------------------------------------------------

    procedure Abort_Example( str : String ) is
        display : A_Allegro_Display;
    begin
        if Al_Init_Native_Dialog_Addon then
            if Al_Is_System_Installed then
                display := Al_Get_Current_Display;
            end if;
            if Al_Show_Native_Message_Box( display, "Error", "Cannot run example", str, "", 0 ) > 0 then
                null;
            end if;
        else
            Ada.Text_IO.Put_Line( str );
        end if;
        GNAT.OS_Lib.OS_Exit( 1 );
    end Abort_Example;

    ----------------------------------------------------------------------------

    procedure Open_Log is
    begin
        if Al_Init_Native_Dialog_Addon then
            textlog := Al_Open_Native_Text_Log( "Log", 0 );
        end if;
    end Open_Log;

    ----------------------------------------------------------------------------

    procedure Open_Log_Monospace is
    begin
        if Al_Init_Native_Dialog_Addon then
            textlog := Al_Open_Native_Text_Log( "Log", ALLEGRO_TEXTLOG_MONOSPACE );
        end if;
    end Open_Log_Monospace;

    ----------------------------------------------------------------------------

    procedure Log_Put( str : String ) is
    begin
        if textlog /= null then
            Al_Append_Native_Text_Log( textlog, str );
        end if;
    end Log_Put;

    ----------------------------------------------------------------------------

    procedure Log_Print( str : String ) is
    begin
        if textlog /= null then
#if WINDOWS'Defined then
            Al_Append_Native_Text_Log( textlog, str & ASCII.CR & ASCII.LF );
#else
            Al_Append_Native_Text_Log( textlog, str & ASCII.LF );
#end if;
        end if;
    end Log_Print;

    ----------------------------------------------------------------------------

    procedure Close_Log( wait_for_user : Boolean ) is
        queue : A_Allegro_Event_Queue;
        event : aliased Allegro_Event;
    begin
        if textlog /= null and then wait_for_user then
            queue := Al_Create_Event_Queue;
            Al_Register_Event_Source( queue, Al_Get_Native_Text_Log_Event_Source( textlog ) );
            Al_Wait_For_Event( queue, event );
            Al_Destroy_Event_Queue( queue );
        end if;

        Al_Close_Native_Text_Log( textlog );
        textlog := null;
    end Close_Log;

end Common;
