
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Mouse;                     use Allegro.Mouse; 
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Shaders;                   use Allegro.Shaders;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Palette is

    type Unsigned_32_Array is array (Integer range <>) of Unsigned_32;

    pal_hex : constant Unsigned_32_Array(0..255) :=
    (
        16#FF00FF#, 16#000100#, 16#060000#, 16#040006#, 16#000200#,
        16#000306#, 16#010400#, 16#030602#, 16#02090C#, 16#070A06#,
        16#020C14#, 16#030F1A#, 16#0F0E03#, 16#0D0F0C#, 16#071221#,
        16#0D1308#, 16#0D1214#, 16#121411#, 16#12170E#, 16#151707#,
        16#0A182B#, 16#171816#, 16#131B0C#, 16#1A191C#, 16#171D08#,
        16#081D35#, 16#1A200E#, 16#1D1F1C#, 16#1D2013#, 16#0E2139#,
        16#06233F#, 16#17230E#, 16#1C270E#, 16#21260F#, 16#0D2845#,
        16#0A294C#, 16#1F2A12#, 16#252724#, 16#232B19#, 16#222D15#,
        16#0C2F51#, 16#0D2F57#, 16#263012#, 16#2B2D2B#, 16#233314#,
        16#273617#, 16#0D3764#, 16#17355E#, 16#2C3618#, 16#2E3623#,
        16#333432#, 16#2C3A15#, 16#093D70#, 16#333B17#, 16#163C6A#,
        16#2F3D18#, 16#323D24#, 16#383A38#, 16#30401B#, 16#2F431C#,
        16#1E4170#, 16#12447D#, 16#154478#, 16#3F403E#, 16#34471A#,
        16#3D482C#, 16#134B8B#, 16#3A4D20#, 16#184D86#, 16#474846#,
        16#3A511D#, 16#13549A#, 16#3D5420#, 16#195595#, 16#0F57A3#,
        16#4E504D#, 16#415925#, 16#435B27#, 16#485837#, 16#125DA9#,
        16#485E24#, 16#175FB2#, 16#235DA3#, 16#555754#, 16#0565BD#,
        16#1C61B5#, 16#2163B7#, 16#2164B1#, 16#49662A#, 16#1268C1#,
        16#2365B9#, 16#1769C3#, 16#5E605D#, 16#196BBE#, 16#55673D#,
        16#1B6BC5#, 16#2968BC#, 16#246BB8#, 16#526D2A#, 16#0E73CC#,
        16#0E74C6#, 16#246FC9#, 16#2470C4#, 16#56712E#, 16#666865#,
        16#007DCE#, 16#537530#, 16#2A72CC#, 16#55762B#, 16#1B77D0#,
        16#1F77D8#, 16#1E79CC#, 16#2E74CF#, 16#58782D#, 16#2E75CA#,
        16#59792E#, 16#2279D3#, 16#5A7A2F#, 16#3276D2#, 16#6D6F6C#,
        16#1081D3#, 16#137FDF#, 16#237DC9#, 16#5B7C30#, 16#637848#,
        16#2A7DD7#, 16#5E7F33#, 16#2C7DDE#, 16#2A80CD#, 16#1D82E2#,
        16#1A85D1#, 16#2B80D5#, 16#747673#, 16#2D82CF#, 16#2F84D1#,
        16#3381E3#, 16#2289D5#, 16#3285D2#, 16#2986EE#, 16#2189ED#,
        16#4782C5#, 16#3884DF#, 16#4083D2#, 16#3487D4#, 16#278BD7#,
        16#298ADD#, 16#67883B#, 16#7B7D7A#, 16#2A8CD9#, 16#6C8653#,
        16#3289E2#, 16#3889D7#, 16#2C8DDA#, 16#2E8FDB#, 16#3D8CDA#,
        16#2F90DC#, 16#338EE8#, 16#3191DD#, 16#3E8EDE#, 16#3392DE#,
        16#838582#, 16#709145#, 16#3593E0#, 16#4191D9#, 16#3794E1#,
        16#698AB1#, 16#4590E5#, 16#3B93E6#, 16#789158#, 16#4594DC#,
        16#3C97E4#, 16#4896DE#, 16#4397EA#, 16#3D9AE1#, 16#8B8E8B#,
        16#409CE3#, 16#4B99E1#, 16#439CEA#, 16#539AD6#, 16#5898E2#,
        16#439EE5#, 16#4E9BE4#, 16#439FEC#, 16#809C5F#, 16#7C9E57#,
        16#45A0E7#, 16#509FE1#, 16#47A1E8#, 16#599EDB#, 16#48A2E9#,
        16#80A153#, 16#4AA4EB#, 16#959794#, 16#5CA1DE#, 16#51A3EF#,
        16#59A3E3#, 16#4DA6ED#, 16#4FA7EF#, 16#51A8F0#, 16#87A763#,
        16#5AA8EA#, 16#53AAF2#, 16#9C9E9B#, 16#49AFF5#, 16#56ACF5#,
        16#55AFF0#, 16#8CAD67#, 16#64ACE8#, 16#60ADF0#, 16#59AFF7#,
        16#6EACE2#, 16#79A9E1#, 16#63AFF2#, 16#59B2F3#, 16#90B162#,
        16#A6A8A5#, 16#60B5F4#, 16#94B56D#, 16#99BC72#, 16#AEB0AD#,
        16#74BBF2#, 16#8DB8ED#, 16#94B7E3#, 16#8ABEEA#, 16#A0C379#,
        16#82C0F2#, 16#B6B8B5#, 16#A3C77C#, 16#A5C97E#, 16#A9CA79#,
        16#8FC7F3#, 16#BEC0BD#, 16#A1C6E9#, 16#97C9F0#, 16#ADD07E#,
        16#C8CAC7#, 16#ACD1F0#, 16#B6CFF0#, 16#B9D5ED#, 16#D1D3D0#,
        16#BEDAF4#, 16#D9DBD8#, 16#C7E2FB#, 16#CDE3F6#, 16#E1E3E0#,
        16#E4E9EC#, 16#DBEBF9#, 16#EAECE9#, 16#E7EFF8#, 16#F1F3F0#,
        16#ECF4FD#, 16#F2F7FA#, 16#F6F8F5#, 16#F7FCFF#, 16#FAFCF8#,
        16#FDFFFC#
    );

    type Sprite_Type is
        record
            x, y, angle, t : Float;
            flags          : Unsigned_32;
            i, j           : Integer;
            pal            : Float_Array(0..255);
        end record;

    type Sprite_Array is array (Integer range <>) of Sprite_Type;

    ----------------------------------------------------------------------------

    procedure Interpolate_Palette( pal        : in out Float_Array;
                                   pal1, pal2 : Float_Array;
                                   t          : Float );
    pragma Precondition( pal'Length = 768 );
    pragma Precondition( pal1'Length = pal'Length );
    pragma Precondition( pal2'Length = pal'Length );

    procedure Interpolate_Palette( pal        : in out Float_Array;
                                   pal1, pal2 : Float_Array;
                                   t          : Float ) is
    begin
        for i in 0..255 loop
            pal(pal'First + i * 3 + 0) := pal1(pal1'First + i * 3 + 0) * (1.0 - t) + pal2(pal2'First + i * 3 + 0) * t;
            pal(pal'First + i * 3 + 1) := pal1(pal1'First + i * 3 + 1) * (1.0 - t) + pal2(pal2'First + i * 3 + 1) * t;
            pal(pal'First + i * 3 + 2) := pal1(pal1'First + i * 3 + 2) * (1.0 - t) + pal2(pal2'First + i * 3 + 2) * t;
        end loop;
    end Interpolate_Palette;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display    : A_Allegro_Display;
        bitmap,
        background : A_Allegro_Bitmap;
        timer      : A_Allegro_Timer;
        queue      : A_Allegro_Event_Queue;
        redraw     : Boolean := True;
        shader     : A_Allegro_Shader;
        pal        : Float_Array(0..3*256-1);
        pals       : Float_Array(0..7*3*256-1);
        t          : Float := 0.0;
        sprite     : Sprite_Array(0..7);
        event      : Allegro_Event;
        dir        : Integer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Mouse then
            Abort_Example( "Error installing mouse" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Error initializing image addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_PROGRAMMABLE_PIPELINE or ALLEGRO_OPENGL );
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Al_Set_New_Bitmap_Format( ALLEGRO_PIXEL_FORMAT_SINGLE_CHANNEL_8 );
        bitmap := Al_Load_Bitmap_Flags( "data/alexlogo.bmp", ALLEGRO_KEEP_INDEX );
        if bitmap = null then
            Abort_Example( "alexlogo not found or failed to load" );
        end if;

        -- Create 8 sprites.
        for i in sprite'Range loop
            sprite(i).angle := ALLEGRO_PI * 2.0 * Float(i) / 8.0;
            sprite(i).x := 320.0 + Sin( sprite(i).angle ) * Float(64 + i * 16);
            sprite(i).y := 240.0 - Cos( sprite(i).angle ) * Float(64 + i * 16);
            sprite(i).flags := (if i mod 2 > 0 then ALLEGRO_FLIP_HORIZONTAL else 0);
            sprite(i).t := Float(i) / 8.0;
            sprite(i).i := i mod 6;
            sprite(i).j := (sprite(i).i + 1) mod 6;
        end loop;

        background := Al_Load_Bitmap( "data/bkg.png" );
        -- Continue even if fail to load.

        -- Create 7 palettes with changed hue.
        for j in 0..6 loop
            for i in 0..255 loop
                declare
                    r, g, b, h, s, l : Float;
                begin
                    r := Float(Shift_Right( pal_hex(i), 16 )) / 255.0;
                    g := Float(Shift_Right( pal_hex(i), 8 ) and 16#FF#) / 255.0;
                    b := Float(pal_hex(i) and 16#FF#) / 255.0;
     
                    Al_Color_RGB_To_HSL( r, g, b, h, s, l );
                    if j = 6 then
                        if l < 0.3 or l > 0.7 then
                            h := 0.0;
                            s := 1.0;
                            l := 0.5;
                        end if;
                    else
                        h := h + Float(j) * 60.0;
                        if l < 0.3 or l > 0.7 then
                            if j mod 2 = 1 then
                                l := 1.0 - l;
                            end if;
                        end if;
                    end if;
                    Al_Color_HSL_To_RGB( h, s, l, r, g, b );
     
                    pals(j*3*256 + i * 3 + 0) := r;
                    pals(j*3*256 + i * 3 + 1) := g;
                    pals(j*3*256 + i * 3 + 2) := b;
                end;
            end loop;
        end loop;

        shader := Al_Create_Shader( ALLEGRO_SHADER_GLSL );

        Al_Attach_Shader_Source( shader, ALLEGRO_VERTEX_SHADER,
                                 "attribute vec4 al_pos;" & ASCII.LF &
                                 "attribute vec4 al_color;" & ASCII.LF &
                                 "attribute vec2 al_texcoord;" & ASCII.LF &
                                 "uniform mat4 al_projview_matrix;" & ASCII.LF &
                                 "varying vec4 varying_color;" & ASCII.LF &
                                 "varying vec2 varying_texcoord;" & ASCII.LF &
                                 "void main()" & ASCII.LF &
                                 "{" & ASCII.LF &
                                 "  varying_color = al_color;" & ASCII.LF &
                                 "  varying_texcoord = al_texcoord;" & ASCII.LF &
                                 "  gl_Position = al_projview_matrix * al_pos;" & ASCII.LF &
                                 "}" & ASCII.LF
                               );
        Al_Attach_Shader_Source( shader, ALLEGRO_PIXEL_SHADER,
                                 "uniform sampler2D al_tex;" & ASCII.LF &
                                 "uniform vec3 pal[256];" & ASCII.LF &
                                 "varying vec4 varying_color;" & ASCII.LF &
                                 "varying vec2 varying_texcoord;" & ASCII.LF &
                                 "void main()" & ASCII.LF &
                                 "{" & ASCII.LF &
                                  "  vec4 c = texture2D(al_tex, varying_texcoord);" & ASCII.LF &
                                  "  int index = int(c.r * 255.0);" & ASCII.LF &
                                  "  if (index != 0) {;" & ASCII.LF &
                                  "    gl_FragColor = vec4(pal[index], 1);" & ASCII.LF &
                                  "  }" & ASCII.LF &
                                  "  else {;" & ASCII.LF &
                                  "    gl_FragColor = vec4(0, 0, 0, 0);" & ASCII.LF &
                                  "  };" & ASCII.LF &
                                  "}" & ASCII.LF
                                );

        Al_Build_Shader( shader );
        Al_Use_Shader( shader );

        timer := Al_Create_Timer( 1.0 / 60.0 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                exit when event.keyboard.keycode = ALLEGRO_KEY_ESCAPE;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := True;
                t := t + 1.0;
                for i in sprite'Range loop
                    dir := (if sprite(i).flags /= 0 then 1 else -1);
                    sprite(i).x := sprite(i).x + Cos( sprite(i).angle ) * 2.0 * Float(dir);
                    sprite(i).y := sprite(i).y + Sin( sprite(i).angle ) * 2.0 * Float(dir);
                    sprite(i).angle := sprite(i).angle + ALLEGRO_PI / 180.0 * Float(dir);
                end loop;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                declare
                    pos : Float := Float(Integer(t) mod 60) / 60.0;
                    p1  : constant Integer := Integer(t / 60.0) mod 3;
                    p2  : constant Integer := (p1 + 1) mod 3;
                begin
                    redraw := False;
                    Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );

                    Interpolate_Palette( pal, pals(p1*2..p1*2+3*256-1), pals(p2*2..p2*2+3*256-1), pos );

                    Al_Set_Shader_Float_Vector( "pal", 3, pal );
                    if background /= null then
                        Al_Draw_Bitmap( background, 0.0, 0.0, 0 );
                    end if;

                    for i in reverse sprite'Range loop
                        pos := (1.0 + Sin( (t / 60.0 + sprite(i).t) * 2.0 * ALLEGRO_PI )) / 2.0;
                        Interpolate_Palette( pal, pals(sprite(i).i..sprite(i).i+3*256-1), pals(sprite(i).j..sprite(i).j+3*256-1), pos );
                        Al_Set_Shader_Float_Vector( "pal", 3, pal );
                        Al_Draw_Rotated_Bitmap( bitmap, 64.0, 64.0, sprite(i).x, sprite(i).y, sprite(i).angle, sprite(i).flags );
                    end loop;

                    Al_Set_Shader_Float_Vector( "pal", 3,
                                                pals((if Integer(t) mod 20 > 15 then 6 else 0)..(if Integer(t) mod 20 > 15 then 6 else 0)+3*256-1) );

                    Al_Draw_Scaled_Rotated_Bitmap( bitmap, 0.0, 0.0,   0.0,   0.0,  0.5,  0.5, 0.0, 0 );
                    Al_Draw_Scaled_Rotated_Bitmap( bitmap, 0.0, 0.0, 640.0,   0.0, -0.5,  0.5, 0.0, 0 );
                    Al_Draw_Scaled_Rotated_Bitmap( bitmap, 0.0, 0.0,   0.0, 480.0,  0.5, -0.5, 0.0, 0 );
                    Al_Draw_Scaled_Rotated_Bitmap( bitmap, 0.0, 0.0, 640.0, 480.0, -0.5, -0.5, 0.0, 0 );

                    Al_Flip_Display;
                end;
            end if;
        end loop;

        Al_Use_Shader( null );

        Al_Destroy_Bitmap( bitmap );
        Al_Destroy_Shader( shader );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Palette;

