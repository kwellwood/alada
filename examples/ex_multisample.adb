
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

-- This example demonstrates the effect of multi-sampling on primitives and
-- bitmaps.
--
--
-- In the window without multi-sampling, the edge of the colored lines will not
-- be anti-aliased - each pixel is either filled completely with the primitive
-- color or not at all.
--
-- The same is true for bitmaps. They will always be drawn at the nearest
-- full-pixel position. However this is only true for the bitmap outline -
-- when texture filtering is enabled the texture itself will honor the sub-pixel
-- position.
--
-- Therefore in the case with no multi-sampling but texture-filtering the
-- outer black edge will stutter in one-pixel steps while the inside edge will
-- be filtered and appear smooth.
--
--
-- In the multi-sampled version the colored lines will be anti-aliased.
--
-- Same with the bitmaps. This means the bitmap outlines will always move in
-- smooth sub-pixel steps. However if texture filtering is turned off the
-- texels still are rounded to the next integer position.
--
-- Therefore in the case where multi-sampling is enabled but texture-filtering
-- is not the outer bitmap edges will move smoothly but the inner will not.
--
package body Ex_Multisample is

    procedure Ada_Main is

        font, font_ms      : A_Allegro_Font;
        bitmap_normal,
        bitmap_filter      : A_Allegro_Bitmap;
        bitmap_normal_ms,
        bitmap_filter_ms   : A_Allegro_Bitmap;

        type Float_Array is array (Integer range <>) of Float;
        bitmap_x, bitmap_y : Float_Array(0..7);
        bitmap_t           : Float := 0.0;

        ------------------------------------------------------------------------

        function Create_Bitmap return A_Allegro_Bitmap is
            checkers_size : constant := 8;
            bitmap_Size   : constant := 24;
            bitmap        : A_Allegro_Bitmap;
            locked        : A_Allegro_Locked_Region;
        begin
            bitmap := Al_Create_Bitmap( bitmap_size, bitmap_size );
            locked := Al_Lock_Bitmap( bitmap,
                                      ALLEGRO_PIXEL_FORMAT_ABGR_8888,
                                      ALLEGRO_LOCK_READWRITE );
            declare
                type Byte_Array is array (Integer range <>) of Unsigned_8;
                p    : constant Integer := locked.pitch;
                c    : Unsigned_8;
                data : Byte_Array(0..bitmap_size * p - 1);
                for data'Address use locked.data;
            begin
                for y in 0..bitmap_size - 1 loop
                    for x in 0..bitmap_size - 1 loop
                        c := Unsigned_8(((x / checkers_size + y / checkers_size) mod 2) * 255);
                        data(y * p + x * 4 + 0) := 0;
                        data(y * p + x * 4 + 1) := 0;
                        data(y * p + x * 4 + 2) := 0;
                        data(y * p + x * 4 + 3) := c;
                    end loop;
                end loop;
            end;
            Al_Unlock_Bitmap( bitmap );
            return bitmap;
        end Create_Bitmap;

        ------------------------------------------------------------------------

        procedure Bitmap_Move is
            a, s : Float;
        begin
            bitmap_t := bitmap_t + 1.0;
            for i in 0..7 loop
                a := 2.0 * ALLEGRO_PI * Float(i) / 16.0;
                s := Sin( (bitmap_t + Float(i) * 40.0) / 180.0 * ALLEGRO_PI ) * 90.0;
                bitmap_x(i) := 100.0 + s * Cos( a );
                bitmap_y(i) := 100.0 + s * Sin( a );
            end loop;
        end Bitmap_Move;

        ------------------------------------------------------------------------

        procedure Draw( bitmap : A_Allegro_Bitmap; f : A_Allegro_Font; y : Integer; text : String ) is
            a, s : Float;
            c    : Allegro_Color;
        begin
            Al_Draw_Text( f, Al_Map_RGB( 0, 0, 0 ), 0.0, Float(y),
                          ALLEGRO_ALIGN_LEFT, text );

            for i in 0..15 loop
                a := 2.0 * ALLEGRO_PI * Float(i) / 16.0;
                c := Al_Color_HSV( Float(i) * 360.0 / 16.0, 1.0, 1.0 );
                Al_Draw_Line( 150.0 + Cos( a ) * 10.0, Float(y) + 100.0 + Sin( a ) * 10.0,
                              150.0 + Cos( a ) * 90.0, Float(y) + 100.0 + Sin( a ) * 90.0,
                              c, 3.0 );
            end loop;

            for i in 0..7 loop
                a := 2.0 * ALLEGRO_PI * Float(i) / 16.0;
                s := Float(Al_Get_Bitmap_Width( bitmap ));
                Al_Draw_Rotated_Bitmap( bitmap, s / 2.0, s / 2.0,
                                        50.0 + bitmap_x(i), Float(y) + bitmap_y(i),
                                        a, 0 );
            end loop;
        end Draw;

        ------------------------------------------------------------------------

        display,
        ms_display : A_Allegro_Display;
        queue      : A_Allegro_Event_Queue;
        timer      : A_Allegro_Timer;
        memory     : A_Allegro_Bitmap;
        quit       : Boolean := False;
        redraw     : Boolean := True;
        wx, wy     : Integer := 0;
        event      : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Couldn't initialize Allegro." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        memory := Create_Bitmap;

        -- Create the normal display.
        Al_Set_New_Display_Option( ALLEGRO_SAMPLE_BUFFERS, 0, ALLEGRO_REQUIRE );
        Al_Set_New_Display_Option( ALLEGRO_SAMPLES, 0, ALLEGRO_SUGGEST );
        display := Al_Create_Display( 300, 450 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;
        Al_Set_Window_Title( display, "Normal" );

        -- Create bitmaps for the normal display.
        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR );
        bitmap_filter := Al_Clone_Bitmap( memory );
        Al_Set_New_Bitmap_Flags( 0 );
        bitmap_normal := Al_Clone_Bitmap( memory );

        font := Al_Create_Builtin_Font;

        Al_Get_Window_Position( display, wx, wy );
        if wx < 160 then
            wx := 160;
        end if;

        -- Create the multi-sampling display.
        Al_Set_New_Display_Option( ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_REQUIRE );
        Al_Set_New_Display_Option( ALLEGRO_SAMPLES, 4, ALLEGRO_SUGGEST );
        ms_display := Al_Create_Display( 300, 450 );
        if ms_display = null then
            Abort_Example( "Multisampling not available." );
        end if;
        Al_Set_Window_Title( ms_display, "Multisampling (" & Al_Get_Display_Option( ms_display, ALLEGRO_SAMPLES )'Img & " )" );

        -- Create bitmaps for the multi-sampling display.
        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR );
        bitmap_filter_ms := Al_Clone_Bitmap( memory );
        Al_Set_New_Bitmap_Flags( 0 );
        bitmap_normal_ms := Al_Clone_Bitmap( memory );

        font_ms := Al_Create_Builtin_Font;

        -- Move the windows next to each other, because some window manager
        -- would put them on top of each other otherwise.
        Al_Set_Window_Position( display, wx - 160, wy );
        Al_Set_Window_Position( ms_display, wx + 160, wy );

        timer := Al_Create_Timer( 1.0 / 30.0 );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( ms_display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );
        while not quit loop

            -- Check for ESC key or close button event and quit in either case.
            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    quit := True;

                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        quit := True;
                    end if;

                when ALLEGRO_EVENT_TIMER =>
                    Bitmap_Move;
                    redraw := True;

                when others =>
                    null;
            end case;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                -- Draw the multi-sampled version into the first window
                Al_Set_Target_Backbuffer( ms_display );

                Al_Clear_To_Color( Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );

                Draw( bitmap_filter_ms, font_ms, 0, "filtered, multi-sample" );
                Draw( bitmap_normal_ms, font_ms, 250, "no filter, multi-sample" );

                Al_Flip_Display;

                -- Draw the normal version into the second window.
                Al_Set_Target_Backbuffer( display );

                Al_Clear_To_Color( Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );

                Draw( bitmap_filter, font, 0, "filtered" );
                Draw( bitmap_normal, font, 250, "no filter" );

                Al_Flip_Display;

                redraw := False;
            end if;

        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Multisample;
