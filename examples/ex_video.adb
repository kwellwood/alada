
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Video;                     use Allegro.Video;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

package body Ex_Video is

    screen   : A_Allegro_Display;
    font     : A_Allegro_Font;
    filename : Unbounded_String;
    zoom     : Float := 0.0;

    ----------------------------------------------------------------------------

    -- Videos often do not use square pixels - these return the scaled dimensions
    -- of the video frame.
    procedure Video_Display( video : A_Allegro_Video ) is
        scaled_w : constant Float := Al_Get_Video_Scaled_Width( video );
        scaled_h : constant Float := Al_Get_Video_Scaled_Height( video );

        -- Get the currently visible frame of the video, based on clock time.
        frame : constant A_Allegro_Bitmap := Al_Get_Video_Frame( video );
        w, h,
        x, y  : Integer;
        tc    : constant Allegro_Color := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.5 );
        bc    : constant Allegro_Color := Al_Map_RGBA_f( 0.5, 0.5, 0.5, 0.5 );
        p     : Long_Float;
    begin
        if frame = null then
            return;
        end if;

        if zoom = 0.0 then
            -- Always make the video fit into the window.
            h := Al_Get_Display_Height( screen );
            w := Integer(Float(h) * scaled_w / scaled_h);
            if w > Al_Get_Display_Width( screen ) then
                w := Al_Get_Display_Width( screen );
                h := Integer(Float(w) * scaled_h / scaled_w);
            end if;
        else
            w := Integer(scaled_w);
            h := Integer(scaled_h);
        end if;
        x := (Al_Get_Display_Width( screen ) - w) / 2;
        y := (Al_Get_Display_Height( screen ) - h) / 2;

        -- Display the frame.
        Al_Draw_Scaled_Bitmap( frame, 0.0, 0.0,
                               Float(Al_Get_Bitmap_Width( frame )),
                               Float(Al_Get_Bitmap_Height( frame )),
                               Float(x), Float(y),
                               Float(w), Float(h),
                               0 );

        -- Show some video information.
        Al_Draw_Filled_Rounded_Rectangle( 4.0, 4.0,
                                          Float(Al_Get_Display_Width( screen )) - 4.0,
                                          4.0 + 14.0 * 4.0, 8.0, 8.0, bc );
        p := Al_Get_Video_Position( video, ALLEGRO_VIDEO_POSITION_ACTUAL );
        Al_Draw_Text( font, tc, 8.0, 8.0, 0, To_String( filename ) );
        Al_Draw_Text( font, tc, 8.0, 8.0 + 13.0, 0,
                      Integer'Image( Integer(p / 60.0) ) &
                      ":" & Integer'Image( Integer(p) mod 60 ) &
                      " (V: " & Float'Image( Float(Al_Get_Video_Position( video, ALLEGRO_VIDEO_POSITION_VIDEO_DECODE ) - p) ) &
                      " A: " & Float'Image( Float(Al_Get_Video_Position( video, ALLEGRO_VIDEO_POSITION_AUDIO_DECODE ) - p) ) &
                      ")" );
        Al_Draw_Text( font, tc, 8.0, 8.0 + 13.0 * 2.0, 0,
                      "video rate " & Al_Get_Video_Fps( video )'Img &
                      " (" & Al_Get_Bitmap_Width( frame )'Img &
                      "x" & Al_Get_Bitmap_Height( frame )'Img &
                      ", aspect " & Float'Image( scaled_w / scaled_h ) &
                      ") audio rate " & Al_Get_Video_Audio_Rate( video )'Img );
        Al_Draw_Text( font, tc, 8.0, 8.0 + 13.0 * 3.0, 0,
                      "playing: " & Al_Is_Video_Playing( video )'Img );

        Al_Flip_Display;
        Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
    end Video_Display;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        queue            : A_Allegro_Event_Queue;
        event            : Allegro_Event;
        timer            : A_Allegro_Timer;
        video            : A_Allegro_Video;
        fullscreen       : Boolean := False;
        redraw           : Boolean := True;
        use_frame_events : Boolean := False;
        filename_arg_idx : Integer := 1;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if Argument_Count = 0 then
            Log_Print( "This example needs to be run from the command line." );
            Log_Print( "Usage: ex_video [--use-frame-events] <file>" );
            Al_Destroy_Display( screen );
            Close_Log( True );
            return;
        end if;

        -- If use_frame_events is false, we use a fixed FPS timer. If the video is
        -- displayed in a game this probably makes most sense. In a
        -- dedicated video player you probably want to listen to
        -- ALLEGRO_EVENT_VIDEO_FRAME_SHOW events and only redraw whenever one
        -- arrives - to reduce possible jitter and save CPU.
        if Argument_Count = 2 and then Argument( 1 ) = "--use-frame-events" then
            use_frame_events := True;
            filename_arg_idx := filename_arg_idx + 1;
        end if;

        if not Al_Init_Video_Addon then
            Abort_Example( "Could not initialize the video addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not install audio." );
        end if;
        if not Al_Reserve_Samples( 1 ) then
            Abort_Example( "Could not reserve 1 sample." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;

        timer := Al_Create_Timer( 1.0 / 60.0 );

        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );
        Al_Set_New_Display_Option( ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST );
        screen := Al_Create_Display( 640, 480 );
        if screen = null then
             Abort_Example( "Could not set video mode - exiting" );
        end if;

        font := Al_Create_Builtin_Font;
        if font = null then
            Abort_Example( "No font." );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR );

        filename := To_Unbounded_String( Argument( filename_arg_idx ) );
        video := Al_Open_Video( To_String( filename ) );
        if video = null then
            Abort_Example( "Cannot read " & To_String( filename ) );
        end if;
        Log_Print( "video FPS: " & Al_Get_Video_Fps( video )'Img );
        Log_Print( "video audio rate: " & Al_Get_Video_Audio_Rate( video )'Img );
        Log_Print( "keys:" );
        Log_Print( "Space: Play/Pause" );
        Log_Print( "cursor right/left: seek 10 seconds" );
        Log_Print( "cursor up/down: seek one minute" );
        Log_Print( "F: toggle fullscreen" );
        Log_Print( "1: disable scaling" );
        Log_Print( "S: scale to window" );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Video_Event_Source( video ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( screen ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        Al_Start_Video( video, Al_Get_Default_Mixer );
        Al_Start_Timer( timer );
        loop

            if redraw and then Al_Event_Queue_Is_Empty( queue ) then
                Video_Display( video );
                redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    case event.keyboard.keycode is
                        when ALLEGRO_KEY_SPACE =>
                            Al_Set_Video_Playing( video, not Al_Is_Video_Playing( video ) );
                        when ALLEGRO_KEY_ESCAPE =>
                            Al_Close_Video( video );
                            exit;
                        when ALLEGRO_KEY_LEFT =>
                            Al_Seek_Video( video, Al_Get_Video_Position( video, ALLEGRO_VIDEO_POSITION_ACTUAL ) - 10.0 );
                        when ALLEGRO_KEY_RIGHT =>
                            Al_Seek_Video( video, Al_Get_Video_Position( video, ALLEGRO_VIDEO_POSITION_ACTUAL ) + 10.0 );
                        when ALLEGRO_KEY_UP =>
                            Al_Seek_Video( video, Al_Get_Video_Position( video, ALLEGRO_VIDEO_POSITION_ACTUAL ) + 60.0 );
                        when ALLEGRO_KEY_DOWN =>
                            Al_Seek_Video( video, Al_Get_Video_Position( video, ALLEGRO_VIDEO_POSITION_ACTUAL ) - 60.0 );
                        when ALLEGRO_KEY_F =>
                            fullscreen := not fullscreen;
                            Al_Set_Display_Flag( screen, ALLEGRO_FULLSCREEN_WINDOW, fullscreen );
                        when ALLEGRO_KEY_1 =>
                            zoom := 1.0;
                        when ALLEGRO_KEY_S =>
                            zoom := 0.0;
                        when others => null;
                    end case;

                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                    al_acknowledge_resize(screen);
                    al_clear_to_color(al_map_rgb(0, 0, 0));

                when ALLEGRO_EVENT_TIMER =>
                    --
                    -- display_time += 1.0 / 60;
                    -- if (display_time >= video_time) {
                    --     video_time = display_time + video_refresh_timer(is);
                    -- }

                    if not use_frame_events then
                        redraw := True;
                    end if;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    Al_Close_Video( video );
                    exit;

                when ALLEGRO_EVENT_VIDEO_FRAME_SHOW =>
                    if use_frame_events then
                        redraw := True;
                    end if;

                when ALLEGRO_EVENT_VIDEO_FINISHED =>
                    Log_Print( "video finished" );

                when others => null;
            end case;
        end loop;

        Al_Destroy_Display( screen );
        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Video;
