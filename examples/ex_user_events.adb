
with Ada.Unchecked_Conversion;
with Ada.Unchecked_Deallocation;
with Allegro_Ids;                       use Allegro_Ids;
with Allegro;                           use Allegro;
with Allegro.Events;                    use Allegro.Events;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System;                            use System;
with System.Address_Image;

--
--    Example program for the Allegro library.
--
package body Ex_User_Events is

    procedure Ada_Main is

        MY_SIMPLE_EVENT_TYPE  : constant AL_ID := To_AL_ID( 'm', 's', 'e', 't' );
        MY_COMPLEX_EVENT_TYPE : constant AL_ID := To_AL_ID( 'm', 'c', 'e', 't' );

        -- Just some fantasy event, supposedly used in an RPG - it's just to show that
        -- in practice, the 4 user fields we have now never will be enough.
        type My_Event is
            record
                id                  : Integer;
                typ                 : Integer; -- For example "attack" or "buy".
                x, y, z             : Integer; -- Position in the game world the event takes place.
                server_time         : Integer; -- Game time in ticks the event takes place.
                source_unit_id      : Integer; -- E.g. attacker or seller.
                destination_unit_id : Integer; -- E.g. defender of buyer.
                item_id             : Integer; -- E.g. weapon used or item sold.
                amount              : Integer; -- Gold the item is sold for.
            end record;
        type A_My_Event is access all My_Event;
        pragma No_Strict_Aliasing( A_My_Event );

        function To_My_Event is new Ada.Unchecked_Conversion( Address, A_My_Event );

        ----------------------------------------------------------------------------

        function New_Event( id : Integer ) return A_My_Event is
            event : constant A_My_Event := new My_Event;
        begin
            event.id := id;
            return event;
        end New_Event;

        ----------------------------------------------------------------------------

        procedure My_Event_Dtor( event : A_Allegro_User_Event );
        pragma Convention( C, My_Event_Dtor );

        procedure My_Event_Dtor( event : A_Allegro_User_Event ) is

            procedure Free is new Ada.Unchecked_Deallocation( My_Event, A_My_Event );

            my_event : A_My_Event;
        begin
            Log_Print( "My_Event_Dtor: " & System.Address_Image( event.data1 ) );
            my_event := To_My_Event( event.data1 );
            Free( my_event );
        end My_Event_Dtor;

        ----------------------------------------------------------------------------

        timer      : A_Allegro_Timer;
        user_src   : constant A_Allegro_Event_Source := new Allegro_Event_Source;
        queue      : A_Allegro_Event_Queue;
        user_event,
        event      : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        timer := Al_Create_Timer( 0.5 );
        if timer = null then
            Abort_Example( "Could not install timer." );
        end if;

        Open_Log;

        Al_Init_User_Event_Source( user_src );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, user_src );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( queue, event );

            if event.any.typ = ALLEGRO_EVENT_TIMER then
                declare
                    n   : constant Unsigned_64 := event.timer.count;
                    -- this is leaked because we don't have a destructor
                    tmp : constant access Integer := new Integer'(Integer(n));
                begin
                    Log_Print( "Got timer event" & n'Img );

                    user_event.user.typ := To_Unsigned_32( MY_SIMPLE_EVENT_TYPE );
                    user_event.user.data1 := tmp.all'Address;
                    Al_Emit_User_Event( user_src, user_event, null );

                    user_event.user.typ := To_Unsigned_32( MY_COMPLEX_EVENT_TYPE );
                    user_event.user.data1 := New_Event( Integer(n) ).all'Address;
                    Al_Emit_User_Event( user_src, user_event, My_Event_Dtor'Unrestricted_Access );
                end;

            elsif event.any.typ = To_Unsigned_32( MY_SIMPLE_EVENT_TYPE ) then
                declare
                    n : Integer;
                    for n'Address use event.user.data1;
                begin
                    pragma Assert(event.user.source = user_src);

                    Al_Unref_User_Event( event.user'Unrestricted_Access );

                    Log_Print( "Got simple user event" & n'Img );
                    if n = 5 then
                        exit;
                    end if;
                end;

            elsif event.any.typ = To_Unsigned_32( MY_COMPLEX_EVENT_TYPE ) then
                declare
                    my_event : A_My_Event;
                begin
                    my_event := To_My_Event( event.user.data1 );
                    pragma Assert( event.user.source = user_src );

                    Log_Print( "Got complex user event" & my_event.id'Img );
                    Al_Unref_User_Event( event.user'Unrestricted_Access );
                end;

            end if;
        end loop;

        Al_Destroy_User_Event_Source( user_src );
        Al_Destroy_Event_Queue( queue );
        Al_Destroy_Timer( timer );

        Log_Print( "Done." );
        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_User_Events;
