
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Disable_Screensaver is

    procedure Ada_Main is
        function Active_Text( active : Boolean ) return String is
        begin
            if active then
                return "Normal";
            end if;
            return "Inhibited";
        end Active_Text;

        display    : A_Allegro_Display;
        font       : A_Allegro_Font;
        events     : A_Allegro_Event_Queue;
        event      : aliased Allegro_Event;
        done       : Boolean := False;
        active     : Boolean := True;
        fullscreen : Boolean := False;
    begin
        if Argument_Count = 1 then
            if Argument(1) = "-fullscreen" then
                fullscreen := True;
            end if;
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        if fullscreen then
            Al_Set_New_Display_Flags( ALLEGRO_GENERATE_EXPOSE_EVENTS or
                                      ALLEGRO_FULLSCREEN );
        else
            Al_Set_New_Display_Flags( ALLEGRO_GENERATE_EXPOSE_EVENTS );
        end if;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        font := Al_Create_Builtin_Font;
        if font = null then
            Abort_Example( "Error creating builtin font" );
        end if;

        events := Al_Create_Event_Queue;
        Al_Register_Event_Source( events, Al_Get_Keyboard_Event_Source );
        -- For expose events
        Al_Register_Event_Source( events, Al_Get_Display_Event_Source( display ) );

        while not done loop
            Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
            Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 0.0, 0.0, 0,
                          "Screen saver: " & Active_Text( active ) );
            Al_Flip_Display;
            Al_Wait_For_Event( events, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        if Al_Inhibit_Screensaver( active ) then
                            active := not active;
                        end if;
                    end if;
                when others =>
                    null;
            end case;
        end loop;

        Al_Destroy_Event_Queue( events );
        Al_Destroy_Font( font );
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Disable_Screensaver;
