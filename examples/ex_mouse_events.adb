
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Mouse.Cursors;             use Allegro.Mouse.Cursors;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Mouse_Events is

    procedure Ada_Main is

        NUM_BUTTONS    : constant := 5;
        actual_buttons : Integer := 0;

        procedure Draw_Mouse_Button( but : Integer; down : Boolean ) is

            type Float_Array is array (Integer range <>) of Float;

            offset : constant Float_Array(0..NUM_BUTTONS-1) := (0.0, 70.0, 35.0, 105.0, 140.0);
            grey   : Allegro_Color;
            black  : Allegro_Color;
            x, y   : Float;
        begin
            x := 400.0 + offset(but);
            y := 130.0;

            grey := Al_Map_RGB( 16#e0#, 16#e0#, 16#e0# );
            black := Al_Map_RGB( 0, 0, 0 );

            Al_Draw_Filled_Rectangle( x, y, x + 27.0, y + 42.0, grey );
            Al_Draw_Rectangle( x + 0.5, y + 0.5, x + 26.5, y + 41.5, black, 0.0 );
            if down then
                Al_Draw_Filled_Rectangle( x + 2.0, y + 2.0, x + 25.0, y + 40.0, black );
            end if;
        end Draw_Mouse_Button;

        ----------------------------------------------------------------------------

        type Bool_Array is array (Integer range <>) of Boolean;

        display   : A_Allegro_Display;
        cursor    : A_Allegro_Bitmap;
        queue     : A_Allegro_Event_Queue;
        event     : Allegro_Event;
        font      : A_Allegro_Font;
        mx,
        my,
        mz,
        mw,
        mmx,
        mmy,
        mmz,
        mmw       : Integer := 0;
        precision : Integer := 1;
        inDisplay : Boolean := True;
        buttons   : Bool_Array(0..NUM_BUTTONS-1) := (others => False);
        p         : Float := 0.0;
        black     : Allegro_Color;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Error initializing primitives addon" );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Error installing mouse" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Error initializing IIO addon" );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        actual_buttons := Al_Get_Mouse_Num_Buttons;
        if actual_buttons > NUM_BUTTONS then
            actual_buttons := NUM_BUTTONS;
        end if;

        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Al_Hide_Mouse_Cursor( display );

        cursor := Al_Load_Bitmap( "data/cursor.tga" );
        if cursor = null then
            Abort_Example( "Error loading cursor.tga" );
        end if;

        font := Al_Load_Font( "data/fixed_font.tga", 1, 0 );
        if font = null then
            Abort_Example( "data/fixed_font.tga not found" );
        end if;

        black := Al_Map_RGB_f( 0.0, 0.0, 0.0 );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, al_get_mouse_event_source );
        Al_Register_Event_Source( queue, al_get_keyboard_event_source );
        Al_Register_Event_Source( queue, al_get_display_event_source( display ) );

        loop
            if Al_Is_Event_Queue_Empty( queue ) then
                Al_Clear_To_Color( Al_Map_RGB( 16#ff#, 16#ff#, 16#c0# ) );
                for i in 0..actual_buttons - 1 loop
                    Draw_Mouse_Button( i, buttons(i) );
                end loop;
                Al_Draw_Bitmap( cursor, Float(mx), Float(my), 0 );
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                Al_Draw_Text( font, black, 5.0, 5.0, 0, "dx" & mmx'Img & ", dy" & mmy'Img & ", dz" & mmz'Img & ", dw" & mmw'Img );
                Al_Draw_Text( font, black, 5.0, 25.0, 0, "x" & mx'Img & ", y" & my'Img & ", z" & mz'Img & ", w" & mw'Img );
                Al_Draw_Text( font, black, 5.0, 45.0, 0, "p = " & p'Img );
                if inDisplay then
                    Al_Draw_Text( font, black, 5.0, 65.0, 0, "in" );
                else
                    Al_Draw_Text( font, black, 5.0, 65.0, 0, "out" );
                end if;
                Al_Draw_Text( font, black, 5.0, 85.0, 0, "wheel precision (PgUp/PgDn)" & precision'Img );
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                mmx := 0;
                mmy := 0;
                mmz := 0;
                Al_Flip_Display;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_MOUSE_AXES =>
                    mx := event.mouse.x;
                    my := event.mouse.y;
                    mz := event.mouse.z;
                    mw := event.mouse.w;
                    mmx := event.mouse.dx;
                    mmy := event.mouse.dy;
                    mmz := event.mouse.dz;
                    mmw := event.mouse.dw;
                    p := event.mouse.pressure;

                when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                    if event.mouse.button - 1 < NUM_BUTTONS then
                        buttons(Integer(event.mouse.button) - 1) := True;
                    end if;
                    p := event.mouse.pressure;

                when ALLEGRO_EVENT_MOUSE_BUTTON_UP =>
                    if event.mouse.button - 1 < NUM_BUTTONS then
                        buttons(Integer(event.mouse.button) - 1) := False;
                    end if;
                    p := event.mouse.pressure;

                when ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY =>
                    inDisplay := True;

                when ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY =>
                    inDisplay := False;

                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exit;
                    end if;

                when ALLEGRO_EVENT_KEY_CHAR =>
                    if event.keyboard.keycode = ALLEGRO_KEY_PGUP then
                        precision := precision + 1;
                        Al_Set_Mouse_Wheel_Precision( precision );
                    elsif event.keyboard.keycode = ALLEGRO_KEY_PGDN then
                        precision := Integer'Max( 1, precision - 1 );
                        Al_Set_Mouse_Wheel_Precision( precision );
                    end if;

                when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                    Al_Acknowledge_Resize( event.display.source );

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    exit;

                when others =>
                    null;
            end case;
        end loop;

        Al_Destroy_Event_Queue( queue );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Mouse_Events;
