
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

--
--    Example program for the Allegro library, by Peter Wang.
--
--    This program demonstrates a simple use of mixer postprocessing callbacks.
--
package body Ex_Mixer_PP is

    procedure Ada_Main is

        FPS     : constant := 60.0;

        rms_l   : Float := 0.0;
        pragma Volatile( rms_l );
        rms_r   : Float := 0.0;
        pragma Volatile( rms_r );

        display : A_Allegro_Display;
        dbuf    : A_Allegro_Bitmap;
        bmp     : A_Allegro_Bitmap;
        theta   : Float;

        ----------------------------------------------------------------------------

        procedure Update_Meter( buf : Address; samples : unsigned; data : Address );
        pragma Convention( C, Update_Meter );

        procedure Update_Meter( buf : Address; samples : unsigned; data : Address ) is
            pragma Unreferenced( data );

            type Float_Array is array(Integer range <>) of Float;

            fbuf  : Float_Array(0..Integer(samples) * 2 - 1);
            for fbuf'Address use buf;
            sum_l : Float := 0.0;
            sum_r : Float := 0.0;
        begin
            for i in 0..Integer(samples)-1 loop
                sum_l := sum_l + fbuf(i*2)   * fbuf(i*2);
                sum_r := sum_r + fbuf(i*2+1) * fbuf(i*2+1);
            end loop;

            rms_l := Sqrt( sum_l / Float(samples) );
            rms_r := Sqrt( sum_r / Float(samples) );
        end Update_Meter;

        ----------------------------------------------------------------------------

        procedure Draw is
            sw : constant Float := Float(Al_Get_Bitmap_Width( bmp ));
            sh : constant Float := Float(Al_Get_Bitmap_Height( bmp ));
            dw : constant Float := Float(Al_Get_Bitmap_Width( dbuf ));
            dh : constant Float := Float(Al_Get_Bitmap_Height( dbuf ));
            dx : constant Float := dw / 2.0;
            dy : constant Float := dh / 2.0;
            db_l,
            db_r,
            db,
            scale,
            disp  : Float;
        begin
            -- Whatever looks okay.
            if rms_l > 0.0 and then rms_r > 0.0 then
                db_l := 20.0 * Log( rms_l / 20.0E-6, 10.0 );
                db_r := 20.0 * Log( rms_r / 20.0E-6, 10.0 );
                db := (db_l + db_r) / 2.0;
                scale := db / 20.0;
                disp := (rms_l + rms_r) * 200.0;
            else
                db_l  := 0.0;
                db_r  := 0.0;
                db    := 0.0;
                scale := 0.0;
                disp  := 0.0;
            end if;

            Al_Set_Target_Bitmap( dbuf );
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Filled_Rectangle( 0.0, 0.0,
                                      Float(Al_Get_Bitmap_Width( dbuf )),
                                      Float(Al_Get_Bitmap_Height( dbuf )),
                                      Al_Map_RGBA_f( 0.8, 0.3, 0.1, 0.06 ) );
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Tinted_Scaled_Rotated_Bitmap( bmp,
                                                  Al_Map_RGBA_f( 0.8, 0.3, 0.1, 0.2 ),
                                                  sw / 2.0, sh / 2.0, dx, dy - disp,
                                                  scale, scale, theta, 0 );

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            Al_Set_Target_Backbuffer( display );
            Al_Draw_Bitmap( dbuf, 0.0, 0.0, 0 );

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Line( 10.0, dh - db_l, 10.0, dh, Al_Map_RGB_f( 1.0, 0.6, 0.2 ), 6.0 );
            Al_Draw_Line( 20.0, dh - db_r, 20.0, dh, Al_Map_RGB_f( 1.0, 0.6, 0.2 ), 6.0 );

            Al_Flip_Display;

            theta := theta - (rms_l + rms_r) * 0.1;
        end Draw;

        ----------------------------------------------------------------------------

        procedure Main_Loop is
            queue  : A_Allegro_Event_Queue;
            event  : aliased Allegro_Event;
            redraw : Boolean := True;
        begin
            queue := Al_Create_Event_Queue;
            Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
            Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

            theta := 0.0;

            loop
                if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                   Draw;
                   redraw := False;
                end if;

                if not Al_Wait_For_Event_Timed( queue, event'Access, 1.0 / FPS ) then
                    redraw := True;
                else
                    if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                        exit;
                    elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
                          event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
                    then
                        exit;
                    end if;
                end if;
            end loop;

            Al_Destroy_Event_Queue( queue );
        end Main_Loop;

        ----------------------------------------------------------------------------

        filename : Unbounded_String := To_Unbounded_String( "data/title_music.ogg" );
        voice    : A_Allegro_Voice;
        mixer    : A_Allegro_Mixer;
        stream   : A_Allegro_Audio_Stream;
    begin
        if Argument_Count > 0 then
            filename := To_Unbounded_String( Argument( 1 ) );
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon" );
        elsif not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon" );
        elsif not Al_Init_Acodec_Addon then
            Abort_Example( "Could not init audio codec addon" );
        elsif not Al_Install_Keyboard then
            Abort_Example( "Could not init audio codec addon" );
        end if;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        dbuf := Al_Create_Bitmap( 640, 480 );

        bmp := Al_Load_Bitmap( "data/mysha.pcx" );
        if bmp = null then
            Abort_Example( "Could not load data/mysha.pcx" );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound." );
        end if;

        voice := Al_Create_Voice( 44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2 );
        if voice = null then
            Abort_Example( "Could not create voice." );
        end if;

        mixer := Al_Create_Mixer( 44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2 );
        if mixer = null then
            Abort_Example( "Could not create mixer." );
        end if;

        if not Al_Attach_Mixer_To_Voice( mixer, voice ) then
            Abort_Example( "Al_Attach_Mixer_To_Voice failed." );
        end if;

        stream := Al_Load_Audio_Stream( To_String( filename ), 4, 2048 );
        if stream = null then
            filename := To_Unbounded_String( "data/welcome.wav" );
            stream := Al_Load_Audio_Stream( To_String( filename ), 4, 2048 );
        end if;
        if stream = null then
            Abort_Example( "Could not load '" & To_String( filename ) & "'" );
        end if;

        if not Al_Set_Audio_Stream_Playmode( stream, ALLEGRO_PLAYMODE_LOOP ) then
            Abort_Example( "Al_Set_Audio_Stream_Playmode failed." );
        end if;
        if not Al_Attach_Audio_Stream_To_Mixer( stream, mixer ) then
            Abort_Example( "Al_Attach_Audio_Stream_To_Mixer failed." );
        end if;

        if not Al_Set_Mixer_Postprocess_Callback( mixer, Update_Meter'Unrestricted_Access, Null_Address ) then
            Abort_Example( "Al_Set_Mixer_Postprocess_Callback failed." );
        end if;

        Main_Loop;

        Al_Destroy_Audio_Stream( stream );
        Al_Destroy_Mixer( mixer );
        Al_Destroy_Voice( voice );
        Al_Uninstall_Audio;

        Al_Destroy_Bitmap( dbuf );
        Al_Destroy_Bitmap( bmp );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Mixer_pp;
