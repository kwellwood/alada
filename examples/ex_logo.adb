
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Configuration;             use Allegro.Configuration;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Fonts.TTF;                 use Allegro.Fonts.TTF;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.State;                     use Allegro.State;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.UTF8;                      use Allegro.UTF8;
with Common;                            use Common;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with GNAT.Strings;                      use GNAT.Strings;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

-- Demo program which creates a logo by direct pixel manipulation of
-- bitmaps. Also uses alpha blending to create a real-time flash
-- effect (likely not visible with displays using memory bitmaps as it
-- is too slow).
--
package body Ex_Logo is

    procedure Ada_Main is

        type String_Array is array(Natural range <>) of String_Access;

        logo,
        logo_flash : A_Allegro_Bitmap;
        logo_x,
        logo_y     : Integer;
        font       : A_Allegro_Font;
        cursor     : aliased Integer;
        selection  : Integer;
        regenerate : Boolean := False;
        editing    : Boolean := False;
        config     : A_Allegro_Config;
        white      : Allegro_Color;
        anim       : Long_Float;

        ------------------------------------------------------------------------

        function Clamp( x : Float ) return Float is
        begin
            return Float'Min( 1.0, Float'Max( 0.0, x ) );
        end Clamp;

        ------------------------------------------------------------------------

        param_names : constant String_Array(0..8) := (new String'("text"),
                                                      new String'("font"),
                                                      new String'("size"),
                                                      new String'("shadow"),
                                                      new String'("blur"),
                                                      new String'("factor"),
                                                      new String'("red"),
                                                      new String'("green"),
                                                      new String'("blue"));

        ------------------------------------------------------------------------

        -- Note: To regenerate something close to the official Allegro logo,
        -- you need to obtain the non-free "Utopia Regular Italic" font. Then
        -- replace "DejaVuSans.ttf" with "putri.pfa" below.
        --
        param_values : String_Array(0..8) := (new String'("Allegro"),
                                              new String'("data/DejaVuSans.ttf"),
                                              new String'("140"),
                                              new String'("10"),
                                              new String'("2"),
                                              new String'("0.5"),
                                              new String'("1.1"),
                                              new String'("1.5"),
                                              new String'("5"));

        ------------------------------------------------------------------------

        -- Generates a bitmap with transparent background and the logo text.
        -- The bitmap will have screen size. If 'bumpmap' is not NULL, it will
        -- contain another bitmap which is a white, blurred mask of the logo
        -- which we use for the flash effect.
        --
        function Generate_Logo( text          : String;
                                fontname      : String;
                                font_size     : Integer;
                                shadow_offset : Float;
                                blur_radius   : Float;
                                blur_factor   : Float;
                                light_red,
                                light_green,
                                light_blue    : Float;
                                bumpmap       : access A_Allegro_Bitmap
                              ) return A_Allegro_Bitmap is
            transparent : constant Allegro_Color := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.0 );
            xp, yp      : Integer;
            w, h,
            br, bw,
            dw, dh      : Integer;
            c           : Allegro_Color;
            logofont    : A_Allegro_Font;
            state       : aliased Allegro_State;
            blur,
            light,
            logo        : A_Allegro_Bitmap;
            left, right,
            top, bottom : Integer;
            pragma Warnings( Off );
            lock1,
            lock2       : A_Allegro_Locked_Region;
            pragma Warnings( On );
            cx, cy      : Float;
        begin
            dw := Al_Get_Bitmap_Width( Al_Get_Target_Bitmap );
            dh := Al_Get_Bitmap_Height( Al_Get_Target_Bitmap );

            cx := Float(dw) * 0.5;
            cy := Float(dh) * 0.5;

            logofont := Al_Load_Font( fontname, -font_size, 0 );
            if logofont = null then
                logofont := Al_Load_Font( "data/dejavusans.ttf", -font_size, 0 );
                if logofont = null then
                    Abort_Example( "Could not load data/dejavusans.ttf" );
                end if;
            end if;
            Al_Get_Text_Dimensions( logofont, text, xp, yp, w, h );

            Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP or ALLEGRO_STATE_BLENDER );

            -- Cheap blur effect to create a bump map.
            blur := Al_Create_Bitmap( dw, dh );
            Al_Set_Target_Bitmap( blur );
            Al_Clear_To_Color( transparent );
            br := Integer(blur_radius);
            bw := br * 2 + 1;
            c := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 / (Float(bw * bw) * blur_factor) );
            Al_Set_Separate_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA,
                                     ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
            for i in -br..br loop
                for j in -br..br loop
                    Al_Draw_Text( logofont, c,
                                  cx - Float(xp) * 0.5 - Float(w) * 0.5 + Float(i),
                                  cy - Float(yp) * 0.5 - Float(h) * 0.5 + Float(j), 0, text );

                end loop;
            end loop;

            left := Integer(cx - Float(xp) * 0.5 - Float(w) * 0.5 - Float(br) + Float(xp));
            top := Integer(cy - Float(yp) * 0.5 - Float(h) * 0.5 - Float(br) + Float(yp));
            right := left + w + br * 2;
            bottom := top + h + br * 2;

            if left < 0 then
                left := 0;
            end if;
            if top < 0 then
                top := 0;
            end if;
            if right > dw - 1 then
                right := dw - 1;
            end if;
            if bottom > dh - 1 then
                bottom := dh - 1;
            end if;

            -- Cheap light effect.
            light := Al_Create_Bitmap( dw, dh );
            Al_Set_Target_Bitmap( light );
            Al_Clear_To_Color( transparent );
            lock1 := Al_Lock_Bitmap( blur, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY );
            lock2 := Al_Lock_Bitmap_Region( light,
                                            left, top, 1 + right - left, 1 + bottom - top,
                                            ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY );
            for y in top..bottom loop
                for x in left..right loop
                    declare
                        r1, g1, b1, a1 : Float;
                        r2, g2, b2, a2 : Float;
                        r, g, b, a     : Float;
                        d              : Float;
                        c              : Allegro_Color := Al_Get_Pixel( blur, x, y );
                        c1             : constant Allegro_Color := Al_Get_Pixel( blur, x - 1, y - 1 );
                        c2             : constant Allegro_Color := Al_Get_Pixel( blur, x + 1, y + 1 );
                    begin
                        Al_Unmap_RGBA_f( c, r, g, b, a );
                        Al_Unmap_RGBA_f( c1, r1, g1, b1, a1 );
                        Al_Unmap_RGBA_f( c2, r2, g2, b2, a2 );

                        d := r2 - r1 + 0.5;
                        r := Clamp( d * light_red );
                        g := Clamp( d * light_green );
                        b := Clamp( d * light_blue );

                        c := Al_Map_RGBA_f( r, g, b, a );
                        Al_Put_Pixel( x, y, c );
                    end;
                end loop;
            end loop;
            Al_Unlock_Bitmap( light );
            Al_Unlock_Bitmap( blur );

            if bumpmap /= null then
                bumpmap.all := blur;
            else
                Al_Destroy_Bitmap( blur );
            end if;

            -- Create final logo
            logo := Al_Create_Bitmap( dw, dh );
            Al_Set_Target_Bitmap( logo );
            Al_Clear_To_Color( transparent );

            -- Draw a shadow.
            c := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.5 / 9.0 );
            Al_Set_Separate_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA,
                                     ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
            for i in -1..1 loop
                for j in -1..1 loop
                    Al_Draw_Text( logofont, c,
                                  cx - Float(xp) * 0.5 - Float(w) * 0.5 + shadow_offset + Float(i),
                                  cy - Float(yp) * 0.5 - Float(h) * 0.5 + shadow_offset + Float(j),
                                  0, text );
                end loop;
            end loop;

            -- Then draw the lit text we made before on top.
            Al_Set_Separate_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA,
                                     ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Bitmap( light, 0.0, 0.0, 0 );
            Al_Destroy_Bitmap( light );

            Al_Restore_State( state );
            Al_Destroy_Font( logofont );

            return logo;
        end Generate_Logo;

        ------------------------------------------------------------------------

        -- Draw the checkerboard background.
        procedure Draw_Background is
            type Color_Array is array (Integer range <>) of Allegro_Color;
            c : Color_Array(0..1);
        begin
            c(0) := Al_Map_RGBA( 16#aa#, 16#aa#, 16#aa#, 16#ff# );
            c(1) := Al_Map_RGBA( 16#99#, 16#99#, 16#99#, 16#ff# );

            for i in 0..640 / 16 - 1 loop
                for j in 0..480 / 16 - 1 loop
                    Al_Draw_Filled_Rectangle( Float(i * 16), Float(j * 16),
                                              Float(i * 16 + 16), Float(j * 16 + 16),
                                              c( (i + j) mod 2) );
                end loop;
            end loop;
        end Draw_Background;

        ------------------------------------------------------------------------

        -- Print out the current logo parameters.
        procedure Print_Parameters is
            normal : constant Allegro_Color := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 );
            light  : constant Allegro_Color := Al_Map_RGBA_f( 0.0, 0.0, 1.0, 1.0 );
            label  : constant Allegro_Color := Al_Map_RGBA_f( 0.2, 0.2, 0.2, 1.0 );
            state  : Allegro_State;
            th     : Integer;
        begin
            Al_Store_State( state, ALLEGRO_STATE_BLENDER );

            th := Al_Get_Font_Line_Height( font ) + 3;
            for i in param_names'Range loop
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                Al_Draw_Text( font, label, 2.0, Float(2 + i * th), 0, param_names(i).all );
            end loop;
            for i in param_names'Range loop
                declare
                    x, y : Float;
                begin
                    y := Float(2 + i * th);
                    Al_Draw_Filled_Rectangle( 75.0, y, 375.0, y + Float(th - 2),
                                              Al_Map_RGBA_f( 0.5, 0.5, 0.5, 0.5 ) );
                    if i = selection then
                        Al_Draw_Text( font, light, 75.0, y, 0, param_values(i).all );
                    else
                        Al_Draw_Text( font, normal, 75.0, y, 0, param_values(i).all );
                    end if;

                    if i = selection and then
                       editing and then
                       (Long_Integer(Al_Get_Time * 2.0) mod 2) = 1
                    then
                        x := 75.0 + Float(Al_Get_Text_Width( font, param_values(i).all ));
                        Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                        Al_Draw_Line( x, y, x, y + Float(th), white, 0.0 );
                    end if;
                end;
            end loop;

            al_set_blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Text( font, normal, 400.0, 2.0, 0, "R - Randomize" );
            Al_Draw_Text( font, normal, 400.0, Float(2 + th), 0, "S - Save as logo.png" );

            Al_Draw_Text( font, normal, 2.0, Float(480 - th * 2 - 2), 0,
                          "To modify, press Return, then enter value, " &
                          "then Return again." );
            Al_Draw_Text( font, normal, 2.0, Float(480 - th - 2), 0,
                          "Use cursor up/down to select the value to modify." );
            Al_Restore_State( state );
        end Print_Parameters;

        ------------------------------------------------------------------------

        function Rnum( min, max : Float ) return String is
            function Rnd is new GNAT.Random_Numbers.Random_Float( Float );
            gen : Generator;
        begin
            Reset( gen );
            return Float'Image( min + Rnd( gen ) * (max - min) );
        end Rnum;

        ------------------------------------------------------------------------

        procedure Randomize is
        begin
            Free( param_values(3) );
            Free( param_values(4) );
            Free( param_values(5) );
            Free( param_values(6) );
            Free( param_values(7) );
            Free( param_values(8) );
            param_values(3) := new String'(Rnum( 2.0, 12.0 ));
            param_values(4) := new String'(Rnum( 1.0, 8.0 ));
            param_values(5) := new String'(Rnum( 0.1, 1.0 ));
            param_values(6) := new String'(Rnum( 0.0, 5.0 ));
            param_values(7) := new String'(Rnum( 0.0, 5.0 ));
            param_values(8) := new String'(Rnum( 0.0, 5.0 ));
            regenerate := True;
        end Randomize;

        ------------------------------------------------------------------------

        procedure Save is
        begin
            if Al_Save_Bitmap( "logo.png", logo ) then
                null;
            end if;
        end Save;

        ------------------------------------------------------------------------

        procedure Mouse_Click( x, y : Integer ) is
            th  : constant Integer := Al_Get_Font_Line_Height( font ) + 3;
            sel : constant Integer := (y - 2) / th;
        begin
            if x < 400 then
                for i in param_names'Range loop
                    if sel = i then
                        selection := i;
                        cursor := 0;
                        editing := True;
                    end if;
                end loop;
            elsif x < 500 then
                if sel = 0 then
                    Randomize;
                elsif sel = 1 then
                    Save;
                end if;
            end if;
        end Mouse_Click;

        ------------------------------------------------------------------------

        procedure Render is
            t : constant Long_Float := Al_Get_Time;
        begin
            if regenerate then
                Al_Destroy_Bitmap( logo );
                Al_Destroy_Bitmap( logo_flash );
                regenerate := False;
            end if;
            if logo = null then
                declare

                    function To_Float( str : String; def : Float ) return Float is
                    begin
                        return Float'Value( str );
                    exception
                         when Constraint_Error =>
                             return def;
                    end To_Float;

                    fullflash  : aliased A_Allegro_Bitmap;
                    fulllogo   : A_Allegro_Bitmap;
                    crop       : A_Allegro_Bitmap;
                    lock       : A_Allegro_Locked_Region;
                    pragma Warnings( Off, lock );
                    left       : Integer := 640;
                    top        : Integer := 480;
                    right      : Integer := -1;
                    bottom     : Integer := -1;
                    c          : Allegro_Color;
                    r, g, b, a : Float;
                begin
                    -- Generate a new logo.
                    fulllogo := Generate_Logo( param_values(0).all,
                                               param_values(1).all,
                                               Integer(To_Float( param_values(2).all, 140.0 )),
                                               To_Float( param_values(3).all, 10.0 ),
                                               To_Float( param_values(4).all, 2.0 ),
                                               To_Float( param_values(5).all, 0.5 ),
                                               To_Float( param_values(6).all, 1.1 ),
                                               To_Float( param_values(7).all, 1.5 ),
                                               To_Float( param_values(8).all, 5.0 ),
                                               fullflash'Access );

                    -- Crop out the non-transparent part.
                    lock := Al_Lock_Bitmap( fulllogo, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY );
                    for y in 0..479 loop
                        for x in 0..639 loop
                            c := Al_Get_Pixel( fulllogo, x, y );
                            Al_Unmap_RGBA_f( c, r, g, b, a );
                            if a > 0.0 then
                                if x < left then
                                    left := x;
                                end if;
                                if y < top then
                                    top := y;
                                end if;
                                if x > right then
                                    right := x;
                                end if;
                                if y > bottom then
                                    bottom := y;
                                end if;
                            end if;
                        end loop;
                    end loop;
                    Al_Unlock_Bitmap( fulllogo );

                    if left = 640 then
                        left := 0;
                    end if;
                    if top = 480 then
                        top := 0;
                    end if;
                    if right < left then
                        right := left;
                    end if;
                    if bottom < top then
                        bottom := top;
                    end if;

                    crop := Al_Create_Sub_Bitmap( fulllogo, left, top,
                                                  1 + right - left,
                                                  1 + bottom - top );
                    logo := Al_Clone_Bitmap( crop );
                    Al_Destroy_Bitmap( crop );
                    Al_Destroy_Bitmap( fulllogo );

                    crop := al_create_sub_bitmap( fullflash, left, top,
                                                  1 + right - left,
                                                  1 + bottom - top );
                    logo_flash := Al_Clone_Bitmap( crop );
                    Al_Destroy_Bitmap( crop );
                    Al_Destroy_Bitmap( fullflash );

                    logo_x := left;
                    logo_y := top;
                end;

                anim := t;
            end if;
            Draw_Background;

            -- For half a second, display our flash animation.
            if t - anim < 0.5 then
                declare
                    f     : constant Float := sin( Float(ALLEGRO_PI * ((t - anim) / 0.5)) );
                    c     : constant Allegro_Color := Al_Map_RGB_f( f * 0.3, f * 0.3, f * 0.3 );
                    state : Allegro_State;
                begin
                    Al_Store_State( state, ALLEGRO_STATE_BLENDER );
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Tinted_Bitmap( logo, Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 - f ), Float(logo_x), Float(logo_y), 0 );
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                    for j in -1..1 loop
                        for i in -1..1 loop
                            Al_Draw_Tinted_Bitmap( logo_flash, c,
                                                   Float(logo_x + i * 2),
                                                   Float(logo_y + j * 2), 0 );
                        end loop;
                    end loop;
                    Al_Restore_State( state );
                end;
            else
                Al_Draw_Bitmap( logo, Float(logo_x), Float(logo_y), 0 );
            end if;

            Print_Parameters;
        end Render;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
        queue   : A_Allegro_Event_Queue;
        event   : Allegro_Event;
        redraw  : Integer := 0;
        quit    : Boolean := False;
        ch      : Integer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not initialise Allegro" );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon" );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon" );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Init_TTF_Addon then
            Abort_Example( "Could not init TTF addon" );
        end if;
        Init_Platform_Specific;

        white := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display" );
        end if;
        Al_Set_Window_Title( display, "Allegro Logo Generator" );
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;

        -- Read logo parameters from logo.ini (if it exists).
        config := Al_Load_Config_File( "logo.ini" );
        if config = null then
            config := Al_Create_Config;
        end if;
        for i in param_names'Range loop
            declare
                value : constant String := Al_Get_Config_Value( config, "logo", param_names(i).all );
            begin
                if value'Length > 0 then
                    param_values(i) := new String'(value);
                end if;
            end;
        end loop;

        font := Al_Load_Font( "data/DejaVuSans.ttf", 12, 0 );
        if font = null then
            Abort_Example( "Could not load font" );
        end if;

        timer := Al_Create_Timer( 1.0 / 60.0 );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );
        while not quit loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                case event.keyboard.keycode is
                    when ALLEGRO_KEY_ESCAPE =>
                        quit := True;
                    when ALLEGRO_KEY_ENTER =>
                        if editing then
                            regenerate := True;
                            editing := False;
                        else
                            cursor := param_values(selection).all'Length;
                            editing := True;
                        end if;
                    when ALLEGRO_KEY_UP =>
                        if selection > 0 then
                            selection := selection - 1;
                            cursor := param_values(selection).all'Length;
                            editing := False;
                         end if;
                    when ALLEGRO_KEY_DOWN =>
                        if param_names(selection + 1) /= null then
                            selection := selection + 1;
                            cursor := param_values(selection).all'Length;
                            editing := False;
                        end if;
                    when others =>
                        ch := event.keyboard.unichar;
                        if editing then
                            if event.keyboard.keycode = ALLEGRO_KEY_BACKSPACE then
                                if cursor > 0 then
                                    declare
                                        u : A_Allegro_Ustr := Al_Ustr_New( param_values(selection).all );
                                    begin
                                        if Al_Ustr_Prev( u, cursor'Access ) then
                                            Al_Ustr_Remove_Chr( u, cursor );
                                            Free( param_values(selection) );
                                            param_values(selection) := new String'(Al_Cstr( u ));
                                        end if;
                                        Al_Ustr_Free( u );
                                    end;
                                end if;
                            elsif ch >= 32 then
                                declare
                                    u : A_Allegro_Ustr := Al_Ustr_New( param_values(selection).all );
                                begin
                                    cursor := cursor + Integer(Al_Ustr_Set_Chr( u, cursor, Integer_32(ch) ));
                                    if Al_Ustr_Set_Chr( u, cursor, 0 ) = 0 then
                                        null;
                                    end if;
                                    Free( param_values(selection) );
                                    param_values(selection) := new String'(Al_Cstr( u ));
                                    Al_Ustr_Free( u );
                                end;
                            end if;
                        else
                            if ch = Character'Pos( 'r' ) then
                                Randomize;
                            elsif ch = Character'Pos( 's' ) then
                                Save;
                            end if;
                        end if;
                end case;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                if event.mouse.button = 1 then
                    Mouse_Click( event.mouse.x, event.mouse.y );
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := redraw + 1;
            end if;

            if redraw > 0 and then Al_Is_Event_Queue_Empty( queue ) then
                redraw := 0;

                Render;

                Al_Flip_Display;
            end if;
        end loop;

        -- Write modified parameters back to logo.ini.
        for i in param_names'Range loop
            Al_Set_Config_Value( config, "logo", param_names(i).all, param_values(i).all );
        end loop;
        if Al_Save_Config_File( "logo.ini", config ) then
           null;
        end if;
        Al_Destroy_Config( config );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Logo;
