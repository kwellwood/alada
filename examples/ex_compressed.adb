
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;

package body Ex_Compressed is

    type Bitmap_Type is
        record
            bmp        : A_Allegro_Bitmap;
            clone      : A_Allegro_Bitmap;
            decomp     : A_Allegro_Bitmap;
            lock_clone : A_Allegro_Bitmap;
            format     : Allegro_Pixel_Format;
            name       : access String;
        end record;

    type Bitmap_Type_Array is array (Integer range <>) of Bitmap_Type;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        filename   : Unbounded_String := To_Unbounded_String( "data/mysha.pcx" );
        display    : A_Allegro_Display;
        timer      : A_Allegro_Timer;
        queue      : A_Allegro_Event_Queue;
        font       : A_Allegro_Font;
        bkg        : A_Allegro_Bitmap;
        redraw     : Boolean := True;
        cur_bitmap : Integer := 0;
        compare    : Boolean := False;
        bitmaps    : Bitmap_Type_Array := ((format => ALLEGRO_PIXEL_FORMAT_ANY, name => new String'("Uncompressed"), others => <>),
                                           (format => ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT1, name => new String'("DXT1"), others => <>),
                                           (format => ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT3, name => new String'("DXT3"), others => <>),
                                           (format => ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT5, name => new String'("DXT5"), others => <>));
        t0, t1     : Long_Float;
        event      : aliased Allegro_Event;
    begin
        if Argument_Count > 0 then
            filename := To_Unbounded_String( Argument( 1 ) );
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if Argument_Count > 1 then
            Al_Set_New_Display_Adapter( Integer'Value( Argument( 2 ) ) );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init Allegro IIO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        for ii in bitmaps'Range loop
            Al_Set_New_Bitmap_Format( bitmaps(ii).format );

            -- Load
            t0 := Al_Get_Time;
            bitmaps(ii).bmp := Al_Load_Bitmap( To_String( filename ) );
            t1 := Al_Get_Time;

            if bitmaps(ii).bmp = null then
                Abort_Example( To_String( filename ) & " not found or failed to load" );
            end if;
            Log_Print( bitmaps(ii).name.all & " load time: " & Long_Float'Image( t1 - t0 ) );

            -- Clone
            t0 := Al_Get_Time;
            bitmaps(ii).clone := Al_Clone_Bitmap( bitmaps(ii).bmp );
            t1 := Al_Get_Time;

            if bitmaps(ii).clone = null then
                Abort_Example( "Couldn't clone " & bitmaps(ii).name.all );
            end if;
            Log_Print( bitmaps(ii).name.all & " clone time: " & Long_Float'Image( t1 - t0 ) );

            -- Decompress
            Al_Set_New_Bitmap_Format( ALLEGRO_PIXEL_FORMAT_ANY );
            t0 := Al_Get_Time;
            bitmaps(ii).decomp := Al_Clone_Bitmap( bitmaps(ii).bmp );
            t1 := Al_Get_Time;

            if bitmaps(ii).decomp = null then
                Abort_Example( "Couldn't decompress " & bitmaps(ii).name.all );
            end if;
            Log_Print( bitmaps(ii).name.all & " decompress time: " & Long_Float'Image( t1 - t0 ) );

            -- RW lock
            Al_Set_New_Bitmap_Format( bitmaps(ii).format );
            t0 := Al_Get_Time;
            bitmaps(ii).lock_clone := Al_Clone_Bitmap( bitmaps(ii).bmp );
            t1 := Al_Get_Time;

            if bitmaps(ii).lock_clone = null then
                Abort_Example( "Couldn't clone " & bitmaps(ii).name.all );
            end if;
            Log_Print( bitmaps(ii).name.all & " decompress time: " & Long_Float'Image( t1 - t0 ) );

            if Al_Get_Bitmap_Width( bitmaps(ii).bmp ) > 128 and Al_Get_Bitmap_Height( bitmaps(ii).bmp ) > 128 then
                declare
                    bitmap_format : constant Allegro_Pixel_Format := Al_Get_Bitmap_Format( bitmaps(ii).bmp );
                    block_width   : constant Integer := Al_Get_Pixel_Block_Width( bitmap_format );
                    block_height  : constant Integer := Al_Get_Pixel_Block_Height( bitmap_format );
                    region        : A_Allegro_Locked_Region;
                    pragma Warnings( Off, region );
                begin
                    -- Lock and unlock it, hopefully causing a no-op operation
                    region := Al_Lock_Bitmap_Region_Blocked( bitmaps(ii).lock_clone,
                                                             16 / block_width, 16 / block_height,
                                                             64 / block_width, 64 / block_height,
                                                             ALLEGRO_LOCK_READWRITE );
                    Al_Unlock_Bitmap( bitmaps(ii).lock_clone );
                end;
            end if;
        end loop;

        bkg := Al_Load_Bitmap( "data/bkg.png" );
        if bkg = null then
            Abort_Example( "data/bkg.png not found or failed to load" );
        end if;

        Al_Set_New_Bitmap_Format( ALLEGRO_PIXEL_FORMAT_ANY );
        font := Al_Create_Builtin_Font;
        timer := Al_Create_Timer( 1.0 / 30.0 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    exit;
                when ALLEGRO_EVENT_TIMER =>
                    redraw := True;
                when ALLEGRO_EVENT_KEY_DOWN =>
                    case event.keyboard.keycode is
                        when ALLEGRO_KEY_LEFT =>
                            cur_bitmap := (cur_bitmap - 1 + bitmaps'Length) mod bitmaps'Length;
                        when ALLEGRO_KEY_RIGHT =>
                            cur_bitmap := (cur_bitmap + 1) mod bitmaps'Length;
                        when ALLEGRO_KEY_SPACE =>
                            compare := True;
                        when ALLEGRO_KEY_ESCAPE =>
                            exit;
                        when others => null;
                     end case;
                when ALLEGRO_EVENT_KEY_UP =>
                    if event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        compare := False;
                    end if;
                when others => null;
            end case;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                declare
                    w   : constant Float := Float(Al_Get_Bitmap_Width( bitmaps(bitmaps'First + cur_bitmap).bmp ));
                    h   : constant Float := Float(Al_Get_Bitmap_Height( bitmaps(bitmaps'First + cur_bitmap).bmp ));
                    idx : constant Integer := (if compare then 0 else cur_bitmap);
                begin
                    redraw := False;
                    Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
                    Al_Draw_Bitmap( bkg, 0.0, 0.0, 0 );
                    Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 5.0, 5.0, ALLEGRO_ALIGN_LEFT,
                                  "SPACE to compare. Arrows to switch. Format: " & bitmaps(bitmaps'First + idx).name.all );
                    Al_Draw_Bitmap( bitmaps(bitmaps'First + idx).bmp, 0.0, 20.0, 0 );
                    Al_Draw_Bitmap( bitmaps(bitmaps'First + idx).clone, w, 20.0, 0 );
                    Al_Draw_Bitmap( bitmaps(bitmaps'First + idx).decomp, 0.0, 20.0 + h, 0 );
                    Al_Draw_Bitmap( bitmaps(bitmaps'First + idx).lock_clone, w, 20.0 + h, 0 );
                    Al_Flip_Display;
                end;
            end if;
        end loop;

        Al_Destroy_Bitmap( bkg );
        for ii in bitmaps'Range loop
            Al_Destroy_Bitmap( bitmaps(ii).bmp );
        end loop;

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Compressed;

