
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Expose is

    procedure Ada_Main is
        display : A_Allegro_Display;
        bitmap  : A_Allegro_Bitmap;
        timer   : A_Allegro_Timer;
        queue   : A_Allegro_Event_Queue;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Failed to init Allegro primitives addon." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Failed to init Allegro IIO addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Failed to install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Failed to install mouse." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE or ALLEGRO_GENERATE_EXPOSE_EVENTS );
        Al_Set_New_Display_Option( ALLEGRO_SINGLE_BUFFER, 1, ALLEGRO_REQUIRE );
        display := Al_Create_Display( 320, 200 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        bitmap := Al_Load_Bitmap( "data/mysha.pcx" );
        if bitmap = null then
            Abort_Example( "mysha.pcx not found or failed to load" );
        end if;
        Al_Draw_Bitmap( bitmap, 0.0, 0.0, 0 );
        Al_Flip_Display;

        timer := Al_Create_Timer( 0.1 );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, al_get_keyboard_event_source );
        Al_Register_Event_Source( queue, al_get_mouse_event_source );
        Al_Register_Event_Source( queue, al_get_display_event_source(display ) );
        Al_Register_Event_Source( queue, al_get_timer_event_source(timer ) );
        Al_Start_Timer( timer );

        loop
            declare
                event : aliased Allegro_Event;
            begin
                Al_Wait_For_Event( queue, event );
                case event.any.typ is
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        exit;
                    when ALLEGRO_EVENT_KEY_DOWN =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            exit;
                        end if;
                    when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                        if Al_Acknowledge_Resize( event.display.source ) then
                            null;
                        end if;
                    when ALLEGRO_EVENT_DISPLAY_EXPOSE =>
                        declare
                            x : constant Float := Float(event.display.y);
                            y : constant Float := Float(event.display.x);
                            w : constant Float := Float(event.display.width);
                            h : constant Float := Float(event.display.height);
                        begin
                            -- Draw a red rectangle over the damaged area.
                            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                            Al_Draw_Filled_Rectangle( x, y, x + w, y + h, Al_Map_RGBA_f( 1.0, 0.0, 0.0, 1.0 ) );
                            Al_Flip_Display;
                        end;
                    when ALLEGRO_EVENT_TIMER =>
                        -- Slowly restore the original bitmap.
                        Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
                        declare
                            y : Integer;
                            x : Integer;
                        begin
                            y := 0;
                            loop
                                exit when y >= Al_Get_Display_Height( display );
                                x := 0;
                                loop
                                    exit when x >= Al_Get_Display_Width( display );
                                    Al_Draw_Tinted_Bitmap( bitmap,
                                                           Al_Map_RGBA_f( 1.0, 1.0, 1.0, 0.1 ),
                                                           Float(x), Float(y), 0 );
                                    x := x + 320;
                                end loop;
                                y := y + 200;
                            end loop;
                            Al_Flip_Display;
                        end;
                    when others =>
                        null;
                end case;
            end;
        end loop;

        Al_Destroy_Event_Queue( queue );
        Al_Destroy_Bitmap( bitmap );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Expose;
