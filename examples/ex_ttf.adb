
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Configuration;             use Allegro.Configuration;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Fonts.TTF;                 use Allegro.Fonts.TTF;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.UTF8;                      use Allegro.UTF8;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

package body Ex_TTF is

    procedure Ada_Main is

       MAX_RANGES : constant := 256;

        font_file : Unbounded_String := To_Unbounded_String( "data/DejaVuSans.ttf" );

        type Example is
            record
                fps                : Long_Float := 0.0;
                f1, f2, f3, f4, f5 : A_Allegro_Font;
                f_alex             : A_Allegro_Font;
                config             : A_Allegro_Config;
                ranges_count       : Integer := 0;
            end record;

        ex : Example;

        ------------------------------------------------------------------------

        procedure Print_Ranges( f : A_Allegro_Font ) is
            ranges : Font_Ranges(0..MAX_RANGES-1);
            count  : Integer;
        begin
            Al_Get_Font_Ranges( f, ranges, count );
            for i in ranges'Range loop
                Log_Print( "range" & i'Img & ":" & ranges(i).first'Img & " -" & ranges(i).last'Img & " (" &
                           Integer'Image( 1 + ranges(i).last - ranges(i).first ) & " glyph" &
                           (if ranges(i).first = ranges(i).last then "" else "s") & ")" );
            end loop;
        end Print_Ranges;

        ------------------------------------------------------------------------

        function Get_String( key : String ) return String is
            cfg : constant String := Al_Get_Config_Value( ex.config, "", key );
        begin
            if cfg'Length > 0 then
                return cfg;
            else
                return key;
            end if;
        end Get_String;

        ------------------------------------------------------------------------

        function Ustr_At( str : A_Allegro_Ustr; index : Integer ) return Integer_32
        is (Al_Ustr_Get( str, Al_Ustr_Offset( str, index ) ));

        ------------------------------------------------------------------------

        procedure Render is
            white      : constant Allegro_Color := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
            black      : constant Allegro_Color := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 );
            red        : constant Allegro_Color := Al_Map_RGBA_f( 1.0, 0.0, 0.0, 1.0 );
            green      : constant Allegro_Color := Al_Map_RGBA_f( 0.0, 0.5, 0.0, 1.0 );
            blue       : constant Allegro_Color := Al_Map_RGBA_f( 0.1, 0.2, 1.0, 1.0 );
            purple     : constant Allegro_Color := Al_Map_RGBA_f( 0.3, 0.1, 0.2, 1.0 );
            x, y,
            w, h,
            as, de,
            xpos, ypos : Float;
            target_w,
            target_h   : Integer;
            info,
            sub_info   : aliased Allegro_Ustr_Info;
            u          : A_Allegro_Ustr;
            tulip           : constant A_Allegro_Ustr := Al_Ustr_New( "Tulip" );
            dimension_text  : constant A_Allegro_Ustr := Al_Ustr_New( "Tulip" );
            vertical_text   : constant A_Allegro_Ustr := Al_Ustr_New( "Rose." );
            dimension_label : constant A_Allegro_Ustr := Al_Ustr_New( "(dimensions)" );
            prev_cp         : Integer_32 := -1;
        begin
            Al_Clear_To_Color( white );

            Al_Hold_Bitmap_Drawing( True );

            Al_Draw_Text( ex.f1, black, 50.0,  20.0, 0, "Tulip (kerning)" );
            Al_Draw_Text( ex.f2, black, 50.0,  80.0, 0, "Tulip (no kerning)" );

            x := 50.0;
            y := 140.0;
            for index in 0..Al_Ustr_Length( dimension_text )-1 loop
                declare
                    cp : constant Integer_32 := Ustr_At( dimension_text, Integer(index) );
                    bbx, bby, bbw, bbh : aliased Integer;
                begin
                    Al_Get_Glyph_Dimensions( ex.f2, cp, bbx, bby, bbw, bbh );
                    Al_Draw_Rectangle( x + Float(bbx) + 0.5, y + Float(bby) + 0.5, x + Float(bbx + bbw) - 0.5, y + Float(bby + bbh) - 0.5, blue, 1.0 );
                    Al_Draw_Rectangle( x + 0.5, y + 0.5, x + Float(bbx + bbw) - 0.5, y + Float(bby + bbh) - 0.5, green, 1.0 );
                    Al_Draw_Glyph( ex.f2, purple, x, y, cp );
                    x := x + Float(Al_Get_Glyph_Advance( ex.f2, cp, ALLEGRO_NO_KERNING ));
                end;
            end loop;
            Al_Draw_Line( 50.5, y + 0.5, x + 0.5, y + 0.5, red, 1.0 );

            for index in 0..(Al_Ustr_Length( dimension_label )-1) loop
                declare
                    cp : constant Integer_32 := Ustr_At( dimension_label, Integer(index) );
                    g  : aliased ALLEGRO_GLYPH;
                begin
                    if Al_Get_Glyph( ex.f2, prev_cp, cp, g'Access ) then
                        Al_Draw_Tinted_Bitmap_Region( g.bitmap, black, Float(g.x), Float(g.y), Float(g.w), Float(g.h), x + 10.0 + Float(g.kerning + g.offset_x), y + Float(g.offset_y), 0 );
                        x := x + Float(g.advance);
                    end if;
                    prev_cp := cp;
                end;
            end loop;

            Al_Draw_Text( ex.f3, black, 50.0, 200.0, 0, "This font has a size of 12 pixels, the one above has 48 pixels." );

            Al_Hold_Bitmap_Drawing( False );
            Al_Hold_Bitmap_Drawing( True );

            Al_Draw_Text( ex.f3, red, 50.0, 220.0, 0, "The color can simply be changed. ? <- fallback glyph" );

            Al_Hold_Bitmap_Drawing( False );
            Al_Hold_Bitmap_Drawing( True );

            Al_Draw_Text( ex.f3, green, 50.0, 240.0, 0, "Some unicode symbols:" );
            Al_Draw_Text( ex.f3, green, 50.0, 260.0, 0, Get_String( "symbols1" ) );
            Al_Draw_Text( ex.f3, green, 50.0, 280.0, 0, Get_String( "symbols2" ) );
            Al_Draw_Text( ex.f3, green, 50.0, 300.0, 0, Get_String( "symbols3" ) );

            u := Al_Ref_Cstr( info'Access, Get_String( "substr1" ) );
            Al_Draw_Ustr( ex.f3, green, 50.0, 320.0, 0, Al_Ref_Ustr( sub_info'Access, u, Al_Ustr_Offset( u, 0 ), Al_Ustr_Offset( u, 6 ) ) );
            u := Al_Ref_Cstr( info'Access, Get_String( "substr2" ) );
            Al_Draw_Ustr( ex.f3, green, 50.0, 340.0, 0, Al_Ref_Ustr( sub_info'Access, u, Al_Ustr_Offset( u, 7 ), Al_Ustr_Offset( u, 11 ) ) );
            u := Al_Ref_Cstr( info'Access, Get_String( "substr3" ) );
            Al_Draw_Ustr( ex.f3, green, 50.0, 360.0, 0, Al_Ref_Ustr( sub_info'Access, u, Al_Ustr_Offset( u, 4 ), Al_Ustr_Offset( u, 11 ) ) );
            u := Al_Ref_Cstr( info'Access, Get_String( "substr4" ) );
            Al_Draw_Ustr( ex.f3, green, 50.0, 380.0, 0, Al_Ref_Ustr( sub_info'Access, u, Al_Ustr_Offset( u, 0 ), Al_Ustr_Offset( u, 11 ) ) );

            Al_Draw_Text( ex.f5, black, 50.0, 395.0, 0, "forced monochrome" );

            -- Glyph rendering tests.
            Al_Draw_Text( ex.f3, red, 50.0, 410.0, 0, "Glyph adv Tu:" & Al_Get_Glyph_Advance( ex.f3, Character'Pos( 'T' ), Character'Pos( 'u' ) )'Img & ", draw: " );

            x := 50.0;
            y := 425.0;
            for index in 0..Al_Ustr_Length( tulip )-1 loop
                declare
                    cp : constant Integer_32 := Ustr_At( tulip, Integer(index) );
                begin
                    -- Use al_get_glyph_advance for the stride, with no kerning.
                    Al_Draw_Glyph( ex.f3, red, x, y, cp );
                    x := x + Float(Al_Get_Glyph_Advance( ex.f3, cp, ALLEGRO_NO_KERNING ));
                end;
            end loop;

            x := 50.0;
            y := 440.0;
            -- First draw a red string using al_draw_text, that should be hidden
            -- completely by the same text drawing in green per glyph
            -- using al_draw_glyph and al_get_glyph_advance below.
            Al_Draw_Ustr( ex.f3, red, x, y, 0, tulip );
            for index in 0..Al_Ustr_Length( tulip )-1 loop
                declare
                    cp  : constant Integer_32 := Ustr_At( tulip, Integer(index) );
                    ncp : constant Integer_32 := (if index < (Al_Ustr_Length( tulip ) - 1) then Ustr_At( tulip, Integer(index + 1) ) else ALLEGRO_NO_KERNING);
                begin
                    -- Use al_get_glyph_advance for the stride and apply kerning.
                    Al_Draw_Glyph( ex.f3, green, x, y, cp );
                    x := x + Float(Al_Get_Glyph_Advance( ex.f3, cp, ncp ));
                end;
            end loop;

            x := 50.0;
            y := 466.0;
            Al_Draw_Ustr( ex.f3, red, x, y, 0, tulip );
            for index in 0..Al_Ustr_Length( tulip )-1 loop
                declare
                    cp : constant Integer_32 := Ustr_At( tulip, Integer(index) );
                    bbx, bby, bbw, bbh : aliased Integer;
                begin
                    Al_Get_Glyph_Dimensions( ex.f3, cp, bbx, bby, bbw, bbh );
                    Al_Draw_Glyph( ex.f3, blue, x, y, cp );
                    x := x + Float(bbx + bbw);
                end;
            end loop;

            x := 10.0;
            y := 30.0;
            for index in 0..Al_Ustr_Length( vertical_text )-1 loop
                declare
                    cp : constant Integer_32 := Ustr_At( vertical_text, Integer(index) );
                    bbx, bby, bbw, bbh : aliased Integer;
                begin
                    -- Use al_get_glyph_dimensions for the height to apply.
                    Al_Get_Glyph_Dimensions( ex.f3, cp, bbx, bby, bbw, bbh );
                    Al_Draw_Glyph( ex.f3, green, x, y, cp );
                    y := y + Float(bby + bbh);
                end;
            end loop;

            x := 30.0;
            y := 30.0;
            for index in 0..Al_Ustr_Length( vertical_text )-1 loop
                declare
                    cp : constant Integer_32 := Ustr_At( vertical_text, Integer(index) );
                    bbx, bby, bbw, bbh : Integer;
                begin
                    -- Use al_get_glyph_dimensions for the height to apply, here bby is
                    -- omited for the wrong result.
                    Al_Get_Glyph_Dimensions( ex.f3, cp, bbx, bby, bbw, bbh );
                    Al_Draw_Glyph( ex.f3, red, x, y, cp );
                    y := y + Float(bbh);
                end;
            end loop;

            Al_Hold_Bitmap_Drawing( False );
            target_w := Al_Get_Bitmap_Width( Al_Get_Target_Bitmap );
            target_h := Al_Get_Bitmap_Height( Al_Get_Target_Bitmap );

            xpos := Float(target_w) - 10.0;
            ypos := Float(target_h) - 10.0;
            declare
                xi, yi, wi, hi : Integer;
            begin
                Al_Get_Text_Dimensions( ex.f4, "Allegro", xi, yi, wi, hi );
                x := Float(xi);
                y := Float(yi);
                w := Float(wi);
                h := Float(hi);
            end;
            as := Float(Al_Get_Font_Ascent( ex.f4 ));
            de := Float(Al_Get_Font_Descent( ex.f4 ));
            xpos := xpos - w;
            ypos := ypos - h;
            x := x + xpos;
            y := y + ypos;

            Al_Draw_Rectangle( x, y, x + w - 0.5, y + h - 0.5, black, 0.0 );
            Al_Draw_Line( x + 0.5, y + as + 0.5, x + w - 0.5, y + as + 0.5, black, 0.0 );
            Al_Draw_Line (x + 0.5, y + as + de + 0.5, x + w - 0.5, y + as + de + 0.5, black, 0.0 );

            Al_Hold_Bitmap_Drawing( True );
            Al_Draw_Text( ex.f4, blue, xpos, ypos, 0, "Allegro" );
            Al_Hold_Bitmap_Drawing( False );

            Al_Hold_Bitmap_Drawing( True );

            Al_Draw_Text( ex.f3, black, Float(target_w), 0.0, ALLEGRO_ALIGN_RIGHT,
                          ex.fps'Img & " FPS" );

            Al_Draw_Text( ex.f3, black, 0.0, 0.0, 0, To_String( font_file ) & ":" & ex.ranges_count'Img & " unicode ranges" );

            Al_Hold_Bitmap_Drawing( False );
        end Render;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
        queue   : A_Allegro_Event_Queue;
        redraw  : Integer := 0;
        t       : Long_Float := 0.0;
        dt      : Long_Float;
        event   : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log_Monospace;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Init_TTF_Addon then
            Abort_Example( "Could not init TTF addon." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        if Argument_Count = 1 then
            font_file := To_Unbounded_String( Argument(1) );
        end if;

        ex.f1 := Al_Load_Font( To_String( font_file ), 48, 0 );
        ex.f2 := Al_Load_Font( To_String( font_file ), 48, ALLEGRO_TTF_NO_KERNING );
        ex.f3 := Al_Load_Font( To_String( font_file ), 12, 0 );
        -- Specifying negative values means we specify the glyph height
        -- in pixels, not the usual font size.
        --
        ex.f4 := Al_Load_Font( To_String( font_file ), -140, 0 );
        ex.f5 := Al_Load_Font( To_String( font_file ), 12, ALLEGRO_TTF_MONOCHROME );

        declare
            ranges : constant Unicode_Range_Array := (1 => (16#1F40A#, 16#1F40A#));
            icon   : constant A_Allegro_Bitmap := Al_Load_Bitmap( "data/icon.png" );
            glyph  : A_Allegro_Bitmap;
        begin
            if icon = null then
                Abort_Example( "Couldn't load data/icon.png." );
            end if;
            glyph := Al_Create_Bitmap( 50, 50 );
            Al_Set_Target_Bitmap( glyph );
            Al_Clear_To_Color( Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.0 ) );
            Al_Draw_Rectangle( 0.5, 0.5, 49.5, 49.5, Al_Map_RGB_f( 1.0, 1.0, 0.0 ), 1.0 );
            Al_Draw_Bitmap( icon, 1.0, 1.0, 0 );
            Al_Set_Target_Backbuffer( display );
            ex.f_alex := Al_Grab_Font_From_Bitmap( glyph, ranges );
        end;

        if ex.f1 = null or else
           ex.f2 = null or else
           ex.f3 = null or else
           ex.f4 = null or else
           ex.f_alex = null
        then
            Abort_Example( "Could not load font: " & To_String( font_file ) );
        end if;

        Al_Set_Fallback_Font( ex.f3, ex.f_alex );

        ex.ranges_count := Al_Get_Font_Ranges( ex.f1 );
        Print_Ranges( ex.f1 );

        ex.config := Al_Load_Config_File( "data/ex_ttf.ini" );
        if ex.config = null then
            Abort_Example( "Could not data/ex_ttf.ini" );
        end if;

        timer := Al_Create_Timer( 1.0 / 60.0 );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );
        loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
                  event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
            then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := redraw + 1;
            end if;

            while redraw > 0 and then Al_Is_Event_Queue_Empty( queue ) loop
                redraw := redraw - 1;

                dt := Al_Get_Time;
                Render;
                dt := Al_Get_Time - dt;

                t := 0.99 * t + 0.01 * dt;

                ex.fps := 1.0 / t;
                Al_Flip_Display;
            end loop;
        end loop;

        Al_Destroy_Font( ex.f1 );
        Al_Destroy_Font( ex.f2 );
        Al_Destroy_Font( ex.f3 );
        Al_Destroy_Font( ex.f4 );
        Al_Destroy_Font( ex.f5 );
        Al_Destroy_Config( ex.config );

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_TTF;
