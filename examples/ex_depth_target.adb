
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.State;                     use Allegro.State;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Depth_Target is

    FPS : constant := 60.0;

    type Example_Rec is
        record
            target_depth          : A_Allegro_Bitmap;
            target_no_depth       : A_Allegro_Bitmap;
            target_no_multisample : A_Allegro_Bitmap;
            font                  : A_Allegro_Font;
            t                     : Long_Long_Float;
            direct_speed_measure  : Long_Long_Float;
        end record;

    example : Example_Rec;

    ----------------------------------------------------------------------------

    procedure Draw_On_Target( target : A_Allegro_Bitmap ) is
        state     : Allegro_State;
        transform : Allegro_Transform;
    begin
        Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP or
                               ALLEGRO_STATE_TRANSFORM or
                               ALLEGRO_STATE_PROJECTION_TRANSFORM );

        Al_Set_Target_Bitmap( target );
        Al_Clear_To_Color( Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.0 ) );
        Al_Clear_Depth_Buffer( 1.0 );

        Al_Identity_Transform( transform );
        Al_Translate_Transform_3d( transform, 0.0, 0.0, 0.0 );
        Al_Orthographic_Transform( transform,
                                   -1.0, 1.0, -1.0, 1.0, -1.0, 1.0 );
        Al_Use_Projection_Transform( transform );

        Al_Draw_Filled_Rectangle( -0.75, -0.5, 0.75, 0.5, Al_Color_Name( "blue" ) );

        Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 1 );

        for i in 0..23 loop
            for j in 0..1 loop
                Al_Identity_Transform( transform );
                Al_Translate_Transform_3d( transform, 0.0, 0.0, Float(j) * 0.01 );
                Al_Rotate_Transform_3d( transform, 0.0, 1.0, 0.0,
                                        ALLEGRO_PI * (Float(i) / 12.0 + Float(example.t) / 2.0 ) );
                Al_Rotate_Transform_3d( transform, 1.0, 0.0, 0.0, ALLEGRO_PI * 0.25 );
                Al_Use_Transform( transform );
                if j = 0 then
                    Al_Draw_Filled_Rectangle( 0.0, -0.5, 0.5, 0.5, (if i mod 2 = 0 then
                                              Al_Color_Name( "yellow" ) else Al_Color_Name( "red" )) );
                else
                    Al_Draw_Filled_Rectangle( 0.0, -0.5, 0.5, 0.5, (if i mod 2 = 0 then
                                              Al_Color_Name( "goldenrod" ) else Al_Color_Name( "maroon" )) );
                end if;
            end loop;
        end loop;

        Al_Set_Render_State( ALLEGRO_DEPTH_TEST, 0 );

        Al_Identity_Transform( transform );
        Al_Use_Transform( transform );

        Al_Draw_Line(  0.9,  0.0,  1.0,  0.0, Al_Color_Name( "black" ), 0.05 );
        Al_Draw_Line( -0.9,  0.0, -1.0,  0.0, Al_Color_Name( "black" ), 0.05 );
        Al_Draw_Line(  0.0,  0.9,  0.0,  1.0, Al_Color_Name( "black" ), 0.05 );
        Al_Draw_Line(  0.0, -0.9,  0.0, -1.0, Al_Color_Name( "black" ), 0.05 );

        Al_Restore_State( state );
    end Draw_On_Target;

    ----------------------------------------------------------------------------

    procedure Redraw is
        w         : constant Float := 512.0;
        h         : constant Float := 512.0;
        black     : constant Allegro_Color := Al_Color_Name( "black" );
        transform : Allegro_Transform;
    begin
        Draw_On_Target( example.target_depth );
        Draw_On_Target( example.target_no_depth );
        Draw_On_Target( example.target_no_multisample );

        Al_Clear_To_Color( Al_Color_Name( "green" ) );

        Al_Identity_Transform( transform );
        Al_Use_Transform( transform );
        Al_Translate_Transform( transform, -128.0, -128.0 );
        Al_Rotate_Transform( transform, Float(example.t * ALLEGRO_PI / 3.0) );
        Al_Translate_Transform( transform, 512.0 + 128.0, 128.0 );
        Al_Use_Transform( transform );
        Al_Draw_Bitmap( example.target_no_depth, 0.0, 0.0, 0 );
        Al_Draw_Text( example.font, black, 0.0, 0.0, 0, "no depth" );

        Al_Identity_Transform( transform );
        Al_Use_Transform( transform );
        Al_Translate_Transform( transform, -128.0, -128.0 );
        Al_Rotate_Transform( transform, Float(example.t * ALLEGRO_PI / 3.0) );
        Al_Translate_Transform( transform, 512.0 + 128.0, 256.0 + 128.0 );
        Al_Use_Transform( transform );
        Al_Draw_Bitmap( example.target_no_multisample, 0.0, 0.0, 0 );
        Al_Draw_Text( example.font, black, 0.0, 0.0, 0, "no multisample" );

        Al_Identity_Transform( transform );
        Al_Use_Transform( transform );
        Al_Translate_Transform( transform, -256.0, -256.0 );
        Al_Rotate_Transform( transform, Float(example.t * ALLEGRO_PI / 3.0) );
        Al_Translate_Transform( transform, 256.0, 256.0 );
        Al_Use_Transform( transform );
        Al_Draw_Bitmap( example.target_depth, 0.0, 0.0, 0 );

        Al_Draw_Line( 30.0, h / 2.0, 60.0, h / 2.0, black, 12.0 );
        Al_Draw_Line( w - 30.0, h / 2.0, w - 60.0, h / 2.0, black, 12.0 );
        Al_Draw_Line( w / 2.0, 30.0, w / 2.0, 60.0, black, 12.0 );
        Al_Draw_Line( w / 2.0, h - 30.0, w / 2.0, h - 60.0, black, 12.0 );
        Al_Draw_Text( example.font, black, 30.0, h / 2.0 - 16.0, 0, "back buffer" );
        Al_Draw_Text( example.font, black, 0.0, h / 2.0 + 10.0, 0, "bitmap" );

        Al_Identity_Transform( transform );
        Al_Use_Transform( transform );
        Al_Draw_Text( example.font, black, w, 0.0,
                      ALLEGRO_ALIGN_RIGHT, Float'Image( Float(1.0 / example.direct_speed_measure) ) & " FPS" );
    end Redraw;

    ----------------------------------------------------------------------------

    procedure Update is
    begin
        example.t := example.t + (1.0 / FPS);
    end Update;

    ----------------------------------------------------------------------------

    procedure Init is
    begin
        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR );
        Al_Set_New_Bitmap_Depth( 16 );
        Al_Set_New_Bitmap_Samples( 4 );
        example.target_depth := Al_Create_Bitmap( 512, 512 );
        Al_Set_New_Bitmap_Depth( 0 );
        example.target_no_depth := Al_Create_Bitmap( 256, 256 );
        Al_Set_New_Bitmap_Samples( 0 );
        example.target_no_multisample := Al_Create_Bitmap( 256, 256 );
    end Init;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        display     : A_Allegro_Display;
        w           : constant Integer := 512 + 256;
        h           : constant Integer := 512;
        done        : Boolean := False;
        need_redraw : Boolean := True;
        t           : Long_Float;
        event       : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Failed to init IIO addon." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Error initializing font addon." );
        end if;
        example.font := Al_Create_Builtin_Font;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Error initializing primitives addon." );
        end if;

        Init_Platform_Specific;

        Al_Set_New_Display_Option( ALLEGRO_DEPTH_SIZE, 16, ALLEGRO_SUGGEST );
        Al_Set_New_Display_Flags( ALLEGRO_PROGRAMMABLE_PIPELINE or ALLEGRO_OPENGL );

        display := Al_Create_Display( w, h );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / FPS );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        Al_Start_Timer( timer );

        t := -Al_Get_Time;

        while not done loop
            if need_redraw then
                t := t + Al_Get_Time;
                example.direct_speed_measure := Long_Long_Float(t);
                t := -Al_Get_Time;
                Redraw;
                Al_Flip_Display;
                need_redraw := False;
            end if;

            loop
                Al_Wait_For_Event( queue, event );
                case event.any.typ is
                    when ALLEGRO_EVENT_KEY_CHAR =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            done := True;
                        end if;

                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        done := True;

                    when ALLEGRO_EVENT_TIMER =>
                        Update;
                        need_redraw := True;

                    when others =>
                        null;
                end case;
                if Al_Is_Event_Queue_Empty( queue ) then
                    exit;
                end if;
            end loop;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Depth_Target;
