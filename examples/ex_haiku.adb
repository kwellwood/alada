
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Ada.Unchecked_Deallocation;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Interfaces;                        use Interfaces;

--
--    Haiku - A Musical Instrument, by Mark Oates.
--
--    Allegro example version by Peter Wang.
--
--    It demonstrates use of the audio functions, and other things besides.
--

-- This version leaves out some things from Mark's original version:
-- the nice title sequence, text labels and mouse cursors.
--
package body Ex_Haiku is

    procedure Ada_Main is

        PI    : constant := ALLEGRO_PI;
        TWOPI : constant := ALLEGRO_PI * 2.0;

        type Elem_Type is (
            TYPE_EARTH,
            TYPE_WIND,
            TYPE_WATER,
            TYPE_FIRE,
            TYPE_NONE
        );
        subtype Element_Type is Elem_Type range TYPE_EARTH..TYPE_FIRE;

        type Img_Type is (
            IMG_EARTH,
            IMG_WIND,
            IMG_WATER,
            IMG_FIRE,
            IMG_BLACK,
            IMG_DROPSHADOW,
            IMG_GLOW,
            IMG_GLOW_OVERLAY,
            IMG_AIR_EFFECT,
            IMG_WATER_DROPS,
            IMG_FLAME,
            IMG_MAIN_FLAME
        );

        type Interp is (
            INTERP_LINEAR,
            INTERP_FAST,
            INTERP_DOUBLE_FAST,
            INTERP_SLOW,
            INTERP_DOUBLE_SLOW,
            INTERP_SLOW_IN_OUT,
            INTERP_BOUNCE
        );

        MAX_ANIMS : constant := 10;

        type A_Float is access all Float;

        type Anim_Type is
            record
                lval       : A_Float;  -- null if unused
                start_val  : Float;
                end_val    : Float;
                func       : Interp;
                start_time,
                end_time   : Float;
            end record;

        type Anim_Array is array(0..MAX_ANIMS-1) of aliased Anim_Type;

        type Sprite is
            record
                image               : Img_Type;        -- IMG_
                x, scale_x, align_x : aliased Float;
                y, scale_y, align_y : aliased Float;
                angle               : aliased Float;
                r, g, b             : aliased Float;
                opacity             : aliased Float;
                anims               : aliased Anim_Array;      -- keep it simple
            end record;
        type A_Sprite is access all Sprite;

        type Token_Type is
            record
                typ   : Elem_Type;    -- TYPE_
                x, y  : Float;
                pitch : Integer;      -- 0..NUM_PITCH-1
                bot   : aliased Sprite;
                top   : aliased Sprite;
            end record;
        type A_Token is access all Token_Type;

        type Flair;
        type A_Flair is access all Flair;

        type Flair is
            record
                next     : A_Flair;
                end_time : Float;
                spr      : aliased Sprite;
            end record;

        ------------------------------------------------------------------------
        -- Globals                                                            --
        ------------------------------------------------------------------------

        NUM_PITCH  : constant := 8;
        TOKENS_X   : constant := 16;
        TOKENS_Y   : constant := NUM_PITCH;
        NUM_TOKENS : constant := TOKENS_X * TOKENS_Y;

        type Image_Array is array(Img_Type) of A_Allegro_Bitmap;
        type Sample_Array_2D is array(Element_Type, Integer range 0..NUM_PITCH-1) of A_Allegro_Sample;
        type Token_Array is array(0..NUM_TOKENS-1) of A_Token;
        type Token_Elem_Array is array(Element_Type) of A_Token;
        type Color_Elem_Array is array(Element_Type) of Allegro_Color;

        display        : A_Allegro_Display;
        refresh_timer  : A_Allegro_Timer;
        playback_timer : A_Allegro_Timer;

        images          : Image_Array;
        element_samples : Sample_Array_2D;
        select_sample   : A_Allegro_Sample;

        tokens          : Token_Array;
        buttons         : Token_Elem_Array;
        glow            : aliased Sprite;
        glow_overlay    : aliased Sprite;
        glow_color      : Color_Elem_Array;
        flairs          : A_Flair := null;    -- linked list
        hover_token     : A_Token := null;
        selected_button : A_Token := null;
        playback_column : Integer := 0;

        screen_w               : constant := 1024;
        screen_h               : constant := 600;
        game_board_x           : constant := 100.0;
        token_size             : constant := 64;
        token_scale            : constant := 0.8;
        button_size            : constant := 64;
        button_unsel_scale     : constant := 0.8;
        button_sel_scale       : constant := 1.1;
        dropshadow_unsel_scale : constant := 0.6;
        dropshadow_sel_scale   : constant := 0.9;
        refresh_rate           : constant := 30.0;
        playback_period        : constant := 2.7333;

        HAIKU_DATA : constant String := "data/haiku/";

        ------------------------------------------------------------------------
        -- Init                                                               --
        ------------------------------------------------------------------------

        procedure Load_Images is
        begin
            images(IMG_EARTH)        := Al_Load_Bitmap( HAIKU_DATA & "earth4.png" );
            images(IMG_WIND)         := Al_Load_Bitmap( HAIKU_DATA & "wind3.png" );
            images(IMG_WATER)        := Al_Load_Bitmap( HAIKU_DATA & "water.png" );
            images(IMG_FIRE)         := Al_Load_Bitmap( HAIKU_DATA & "fire.png" );
            images(IMG_BLACK)        := Al_Load_Bitmap( HAIKU_DATA & "black_bead_opaque_A.png" );
            images(IMG_DROPSHADOW)   := Al_Load_Bitmap( HAIKU_DATA & "dropshadow.png" );
            images(IMG_AIR_EFFECT)   := Al_Load_Bitmap( HAIKU_DATA & "air_effect.png" );
            images(IMG_WATER_DROPS)  := Al_Load_Bitmap( HAIKU_DATA & "water_droplets.png" );
            images(IMG_FLAME)        := Al_Load_Bitmap( HAIKU_DATA & "flame2.png" );
            images(IMG_MAIN_FLAME)   := Al_Load_Bitmap( HAIKU_DATA & "main_flame2.png" );
            images(IMG_GLOW)         := Al_Load_Bitmap( HAIKU_DATA & "healthy_glow.png" );
            images(IMG_GLOW_OVERLAY) := Al_Load_Bitmap( HAIKU_DATA & "overlay_pretty.png" );

            for i in images'Range loop
                if images(i) = null then
                    Abort_Example( "Error loading image " & i'Img & "." );
                end if;
            end loop;
        end Load_Images;

        ------------------------------------------------------------------------

        procedure Load_Samples is
            type Elem_String_Array is array(Element_Type) of String(1..5);
            base : constant Elem_String_Array := ("earth", "air  ", "water", "fire ");
        begin
            for t in Element_Type'Range loop
                for p in 0..NUM_PITCH-1 loop
                    declare
                        name : constant String := HAIKU_DATA & Trim( base(t), Right ) & "_" & Trim( p'Img, Left ) & ".ogg";
                    begin
                        element_samples(t, p) := Al_Load_Sample( name );
                        if element_samples(t, p) = null then
                            Abort_Example( "Error loading " & name );
                        end if;
                    end;
                end loop;
            end loop;

            select_sample := Al_Load_Sample( HAIKU_DATA & "select.ogg" );
            if select_sample = null then
                Abort_Example( "Error loading select.ogg" );
            end if;
        end Load_Samples;

        ------------------------------------------------------------------------

        procedure Init_Sprite( spr     : in out Sprite;
                               image   : Img_Type;
                               x, y    : Float;
                               scale   : Float;
                               opacity : Float ) is
        begin
            spr.image := image;
            spr.x := x;
            spr.y := y;
            spr.scale_x := scale;
            spr.scale_y := scale;
            spr.align_x := 0.5;
            spr.align_y := 0.5;
            spr.angle := 0.0;
            spr.r := 1.0;
            spr.g := 1.0;
            spr.b := 1.0;
            spr.opacity := opacity;
            for i in spr.anims'Range loop
                spr.anims(i).lval := null;
            end loop;
        end Init_Sprite;

        ------------------------------------------------------------------------

        procedure Init_Tokens is
            token_w : constant Float := Float(token_size) * token_scale;
            token_x : constant Float := game_board_x + token_w / 2.0;
            token_y : constant Float := 80.0;
            tx, ty : Integer;
            px, py : Float;
        begin
            for i in tokens'Range loop
                tx := i mod TOKENS_X;
                ty := i / TOKENS_X;
                px := token_x + Float(tx) * token_w;
                py := token_y + Float(ty) * token_w;

                tokens(i) := new Token_Type;
                tokens(i).typ := TYPE_NONE;
                tokens(i).x := px;
                tokens(i).y := py;
                tokens(i).pitch := NUM_PITCH - 1 - ty;

                pragma Assert( tokens(i).pitch >= 0 and then
                               tokens(i).pitch < NUM_PITCH,
                               "Bad token pitch" );

                Init_Sprite( tokens(i).bot, IMG_BLACK, px, py, token_scale, 0.4 );
                Init_Sprite( tokens(i).top, IMG_BLACK, px, py, token_scale, 0.0 );
            end loop;
        end Init_Tokens;

        ------------------------------------------------------------------------

        procedure Init_Buttons is
            type Elem_Float_Array is array(Element_Type) of Float;
            dist : constant Elem_Float_Array := (-1.5, -0.5, 0.5, 1.5);
            x, y : Float;
        begin
            for i in Element_Type'Range loop
                x := Float(screen_w / 2) + 150.0 * dist(i);
                y := Float(screen_h - 80);

                buttons(i) := new Token_Type;
                buttons(i).typ := i;
                buttons(i).x := x;
                buttons(i).y := y;
                Init_Sprite( buttons(i).bot, IMG_DROPSHADOW, x, y, dropshadow_unsel_scale, 0.4 );
                buttons(i).bot.align_y := 0.0;
                Init_Sprite( buttons(i).top, Img_Type'Val( Element_Type'Pos( i ) ), x, y, button_unsel_scale, 1.0 );
            end loop;
        end Init_Buttons;

        ------------------------------------------------------------------------

        procedure Init_Glow is
        begin
            Init_Sprite( glow, IMG_GLOW, Float(screen_w/2), Float(screen_h), 1.0, 1.0 );
            glow.align_y := 1.0;
            glow.r := 0.0;
            glow.g := 0.0;
            glow.b := 0.0;

            Init_Sprite( glow_overlay, IMG_GLOW_OVERLAY, 0.0, 0.0, 1.0, 1.0 );
            glow_overlay.align_x := 0.0;
            glow_overlay.align_y := 0.0;
            glow_overlay.r := 0.0;
            glow_overlay.g := 0.0;
            glow_overlay.b := 0.0;

            glow_color(TYPE_EARTH) := Al_Map_RGB( 16#6b#, 16#8e#, 16#23# ); -- olivedrab
            glow_color(TYPE_WIND)  := Al_Map_RGB( 16#ad#, 16#d8#, 16#e6# ); -- lightblue
            glow_color(TYPE_WATER) := Al_Map_RGB( 16#41#, 16#69#, 16#e1# ); -- royalblue
            glow_color(TYPE_FIRE)  := Al_Map_RGB( 16#ff#, 16#00#, 16#00# ); -- red
        end Init_Glow;

        ------------------------------------------------------------------------
        -- Flairs                                                             --
        ------------------------------------------------------------------------

        function Make_Flair( image : Img_Type; x, y : Float; end_time : Float ) return A_Sprite is
            fl : A_Flair := new Flair;
        begin
            Init_Sprite( fl.spr, image, x, y, 1.0, 1.0 );
            fl.end_time := end_time;
            fl.next := flairs;
            flairs := fl;
            return fl.spr'Access;
        end Make_Flair;

        ------------------------------------------------------------------------

        procedure Delete( fl : in out A_Flair ) is
            procedure Free is new Ada.Unchecked_Deallocation( Flair, A_Flair );
        begin
            if fl /= null then
                Free( fl );
            end if;
        end Delete;

        ------------------------------------------------------------------------

        procedure Free_Old_Flairs( now : Float ) is
            prev, fl, next : A_Flair;
        begin
            prev := null;
            fl := flairs;
            while fl /= null loop
                next := fl.next;
                if fl.end_time > now then
                    prev := fl;
                else
                    if prev /= null then
                        prev.next := next;
                    else
                        flairs := next;
                    end if;
                    Delete( fl );
                end if;
                fl := next;
            end loop;
        end Free_Old_Flairs;

        ------------------------------------------------------------------------

        procedure Free_All_Flairs is
            next : A_Flair;
        begin
            while flairs /= null loop
                next := flairs.next;
                Delete( flairs );
                flairs := next;
            end loop;
        end Free_All_Flairs;

        ------------------------------------------------------------------------
        -- Animations                                                         --
        ------------------------------------------------------------------------

        procedure Fix_Conflicting_Anims( grp        : in out Sprite;
                                         lval       : A_Float;
                                         start_time : Float;
                                         start_val  : Float ) is
        begin
            for i in grp.anims'Range loop
                if grp.anims(i).lval = lval then
                    -- If an old animation would overlap with the new one, truncate it
                    -- and make it converge to the new animation's starting value.
                    --
                    if grp.anims(i).end_time > start_time then
                        grp.anims(i).end_time := start_time;
                        grp.anims(i).end_val := start_val;
                    end if;

                    -- Cancel any old animations which are scheduled to start after the
                    -- new one, or which have been reduced to nothing.
                    --
                    if grp.anims(i).start_time >= start_time or else
                       grp.anims(i).start_time >= grp.anims(i).end_time
                    then
                        grp.anims(i).lval := null;
                    end if;
                end if;
            end loop;
        end Fix_Conflicting_Anims;

        ------------------------------------------------------------------------

        procedure Anim_Full( spr                : A_Sprite;
                             lval               : A_Float;
                             start_val, end_val : Float;
                             func               : Interp;
                             dely               : Float;
                             duration           : Float ) is
            start_time : Float;
        begin
            start_time := Float(Al_Get_Time) + dely;
            Fix_Conflicting_Anims( spr.all, lval, start_time, start_val );

            for i in spr.anims'Range loop
                if spr.anims(i).lval = null then
                    spr.anims(i).lval := lval;
                    spr.anims(i).start_val := start_val;
                    spr.anims(i).end_val := end_val;
                    spr.anims(i).func := func;
                    spr.anims(i).start_time := start_time;
                    spr.anims(i).end_time := start_time + duration;
                    exit;
                end if;
            end loop;
        end Anim_Full;

        ------------------------------------------------------------------------

        procedure Anim( spr       : A_Sprite;
                        lval      : A_Float;
                        start_val,
                        end_val   : Float;
                        func      : Interp;
                        duration  : Float ) is
        begin
            Anim_Full( spr, lval, start_val, end_val, func, 0.0, duration );
        end Anim;

        ------------------------------------------------------------------------

        procedure Anim_To( spr      : A_Sprite;
                           lval     : A_Float;
                           end_val  : Float;
                           func     : Interp;
                           duration : Float ) is
        begin
            Anim_Full( spr, lval, lval.all, end_val, func, 0.0, duration );
        end Anim_To;

        ------------------------------------------------------------------------

        procedure Anim_Delta( spr      : A_Sprite;
                              lval     : A_Float;
                              delt     : Float;
                              func     : Interp;
                              duration : Float ) is
        begin
            Anim_Full( spr, lval, lval.all, lval.all + delt, func, 0.0, duration );
        end Anim_Delta;

        ------------------------------------------------------------------------

        procedure Anim_Tint( spr      : A_Sprite;
                             color    : Allegro_Color;
                             func     : Interp;
                             duration : Float ) is
            r, g, b : Float;
        begin
            Al_Unmap_RGB_f( color, r, g, b );
            Anim_To( spr, spr.r'Access, r, func, duration );
            Anim_To( spr, spr.g'Access, g, func, duration );
            Anim_To( spr, spr.b'Access, b, func, duration );
        end Anim_Tint;

        ------------------------------------------------------------------------

        function Interpolate( func : Interp; t : Float ) return Float is
            t2 : Float;
        begin
            case func is
                when INTERP_LINEAR =>
                    return t;

                when INTERP_FAST =>
                    return -t * (t - 2.0);

                when INTERP_DOUBLE_FAST =>
                    t2 := t - 1.0;
                    return t2 * t2 * t2 + 1.0;

                when INTERP_SLOW =>
                    return t * t;

                when INTERP_DOUBLE_SLOW =>
                    return t * t * t;

                when INTERP_SLOW_IN_OUT =>
                    -- Quadratic easing in/out - acceleration until halfway, then deceleration
                    declare
                        b : constant Float := 0.0;
                        c : constant Float := 1.0;
                        d : constant Float := 1.0;
                    begin
                        t2 := t / (d / 2.0);
                        if t2 < 1.0 then
                            return (c / 2.0) * t2 * t2 + b;
                        else
                            t2 := t2 - 1.0;
                            return -c / 2.0 * (t2 * (t2 - 2.0) - 1.0) + b;
                        end if;
                    end;

                when INTERP_BOUNCE =>
                    -- BOUNCE EASING: exponentially decaying parabolic bounce
                    -- t: current time, b: beginning value, c: change in position, d: duration
                    -- bounce easing out
                    if t < 1.0 / 2.75 then
                        return 7.5625 * t * t;
                    elsif t < 2.0 / 2.75 then
                        t2 := t - (1.5 / 2.75);
                        return 7.5625 * t2 * t2 + 0.75;
                    elsif t < 2.5 / 2.75 then
                         t2 := t - 2.25 / 2.75;
                         return 7.5625 * t2 * t2 + 0.9375;
                    else
                        t2 := t - 2.625 / 2.75;
                        return 7.5625 * t2 * t2 + 0.984375;
                    end if;

            end case;
        end Interpolate;

        ------------------------------------------------------------------------

        procedure Update_Anim( anim : in out Anim_Type; now : Float ) is
            dt, t, rang : Float;
        begin
            if anim.lval = null then
                return;
            end if;

            if now < anim.start_time then
                return;
            end if;

            dt := now - anim.start_time;
            t := dt / (anim.end_time - anim.start_time);

            if t >= 1.0 then
                -- animation has run to completion
                anim.lval.all := anim.end_val;
                anim.lval := null;
                return;
            end if;

            rang := anim.end_val - anim.start_val;
            anim.lval.all := anim.start_val + Interpolate( anim.func, t ) * rang;
        end Update_Anim;

        ------------------------------------------------------------------------

        procedure Update_Sprite_Anims( spr : in out Sprite; now : Float ) is
        begin
            for i in spr.anims'Range loop
                if spr.anims(i).lval /= null then
                Update_Anim( spr.anims(i), now );
                end if;
            end loop;
        end Update_Sprite_Anims;

        ------------------------------------------------------------------------

        procedure Update_Token_Anims( token : A_Token; now : Float ) is
        begin
            Update_Sprite_Anims( token.bot, now );
            Update_Sprite_Anims( token.top, now );
        end Update_Token_Anims;

        ------------------------------------------------------------------------

        procedure Update_Anims( now : Float ) is
            fl : A_Flair;
        begin
            for i in tokens'Range loop
                Update_Token_Anims( tokens(i), now );
            end loop;

            for i in buttons'Range loop
                Update_Token_Anims( buttons(i), now );
            end loop;

            Update_Sprite_Anims( glow, now );
            Update_Sprite_Anims( glow_overlay, now );

            fl := flairs;
            while fl /= null loop
                Update_Sprite_Anims( fl.spr, now );
                fl := fl.next;
            end loop;
        end Update_Anims;

        ------------------------------------------------------------------------
        -- Drawing                                                            --
        ------------------------------------------------------------------------

        procedure Draw_Sprite( spr : in out Sprite ) is
            bmp    : A_Allegro_Bitmap;
            tint   : Allegro_Color;
            cx, cy : Float;
        begin
            bmp := images(spr.image);
            cx := spr.align_x * Float(Al_Get_Bitmap_Width( bmp ));
            cy := spr.align_y * Float(Al_Get_Bitmap_Height( bmp ));
            tint := Al_Map_RGBA_f( spr.r, spr.g, spr.b, spr.opacity );

            Al_Draw_Tinted_Scaled_Rotated_Bitmap( bmp, tint, cx, cy,
                                                  spr.x, spr.y, spr.scale_x,
                                                  spr.scale_y, spr.angle, 0 );
        end Draw_Sprite;

        ------------------------------------------------------------------------

        procedure Draw_Token( token : A_Token ) is
        begin
            Draw_Sprite( token.bot );
            Draw_Sprite( token.top );
        end Draw_Token;

        ------------------------------------------------------------------------

        procedure Draw_Screen is
            fl : A_Flair;
        begin
            Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_ONE );

            Draw_Sprite( glow );
            Draw_Sprite( glow_overlay );

            for i in tokens'Range loop
                Draw_Token( tokens(i) );
            end loop;

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );

            for i in buttons'Range loop
                Draw_Token( buttons(i) );
            end loop;

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_ONE );

            fl := flairs;
            while fl /= null loop
                Draw_Sprite( fl.spr );
                fl := fl.next;
            end loop;

            Al_Flip_Display;
        end Draw_Screen;

        ------------------------------------------------------------------------
        -- Playback                                                           --
        ------------------------------------------------------------------------

        procedure Spawn_Wind_Effects( x, y : Float ) is
            now : constant Float := Float(Al_Get_Time);
            spr : A_Sprite;
        begin
            spr := Make_Flair( IMG_AIR_EFFECT, x, y, now + 1.0 );
            Anim( spr, spr.scale_x'Access, 0.9, 1.3, INTERP_FAST, 1.0 );
            Anim( spr, spr.scale_y'Access, 0.9, 1.3, INTERP_FAST, 1.0 );
            Anim( spr, spr.opacity'Access, 1.0, 0.0, INTERP_FAST, 1.0 );

            spr := Make_Flair( IMG_AIR_EFFECT, x, y, now + 1.2 );
            Anim( spr, spr.opacity'Access, 1.0, 0.0, INTERP_LINEAR, 1.2 );
            Anim( spr, spr.scale_x'Access, 1.1, 1.5, INTERP_FAST, 1.2 );
            Anim( spr, spr.scale_y'Access, 1.1, 0.5, INTERP_FAST, 1.2 );
            Anim_Delta( spr, spr.x'Access, 10.0, INTERP_FAST, 1.2 );
        end Spawn_Wind_Effects;

        ------------------------------------------------------------------------

        procedure Spawn_Fire_Effects( x, y : Float ) is
            now : constant Float := Float(Al_Get_Time);
            spr : A_Sprite;
        begin
            spr := Make_Flair( IMG_MAIN_FLAME, x, y, now + 0.8 );
            spr.align_y := 0.75;
            Anim_Full( spr, spr.scale_x'Access, 0.2, 1.3, INTERP_BOUNCE, 0.0, 0.4 );
            Anim_Full( spr, spr.scale_y'Access, 0.2, 1.3, INTERP_BOUNCE, 0.0, 0.4 );
            Anim_Full( spr, spr.scale_x'Access, 1.3, 1.4, INTERP_BOUNCE, 0.4, 0.5 );
            Anim_Full( spr, spr.scale_y'Access, 1.3, 1.4, INTERP_BOUNCE, 0.4, 0.5 );
            Anim_Full( spr, spr.opacity'Access, 1.0, 0.0, INTERP_FAST, 0.3, 0.5 );

            for i in 0..2 loop
                spr := Make_Flair( IMG_FLAME, x, y, now + 0.7 );
                spr.align_x := 1.3;
                spr.angle := TWOPI / 3.0 * Float(i);

                Anim_Delta( spr, spr.angle'Access, -PI, INTERP_DOUBLE_FAST, 0.7 );
                Anim( spr, spr.opacity'Access, 1.0, 0.0, INTERP_SLOW, 0.7 );
                Anim( spr, spr.scale_x'Access, 0.2, 1.0, INTERP_FAST, 0.7 );
                Anim( spr, spr.scale_y'Access, 0.2, 1.0, INTERP_FAST, 0.7 );
            end loop;
        end Spawn_Fire_Effects;

        ------------------------------------------------------------------------

        function Random_Sign return Float is
            gen : Generator;
        begin
            Reset( gen );
            if Integer'(Random( gen )) mod 2 = 0 then
                return -1.0;
            else
                return 1.0;
            end if;
        end Random_Sign;

        ------------------------------------------------------------------------

        function Random_Float( min, max : Float ) return Float is
            gen : Generator;
        begin
            Reset( gen );
            return Float'(Random( gen )) * (max - min) + min;
        end Random_Float;

        ------------------------------------------------------------------------

        procedure Spawn_Water_Effects( x, y : Float ) is
            now          : constant Float := Float(Al_Get_Time);
            max_duration : constant Float := 1.0;
            spr          : A_Sprite;
        begin
            spr := Make_Flair( IMG_WATER, x, y, now + max_duration );
            Anim( spr, spr.scale_x'Access, 1.0, 2.0, INTERP_FAST, 0.5 );
            Anim( spr, spr.scale_y'Access, 1.0, 2.0, INTERP_FAST, 0.5 );
            Anim( spr, spr.opacity'Access, 0.5, 0.0, INTERP_FAST, 0.5 );

            for i in 0..8 loop
                spr := Make_Flair( IMG_WATER_DROPS, x, y, now + max_duration );
                spr.scale_x := Random_Float( 0.3, 1.2 ) * Random_Sign;
                spr.scale_y := Random_Float( 0.3, 1.2 ) * Random_Sign;
                spr.angle := Random_Float( 0.0, TWOPI );
                spr.r := Random_Float( 0.0, 0.6 );
                spr.g := Random_Float( 0.4, 0.6 );
                spr.b := 1.0;

                if i = 0 then
                    Anim_To( spr, spr.opacity'Access, 0.0, INTERP_LINEAR, max_duration );
                else
                    Anim_To( spr, spr.opacity'Access, 0.0, INTERP_DOUBLE_SLOW, Random_Float( 0.7, 1.0 ) * max_duration );
                end if;
                Anim_To( spr, spr.scale_x'Access, Random_Float( 0.8, 3.0 ), INTERP_FAST, Random_Float( 0.7, 1.0 ) * max_duration );
                Anim_To( spr, spr.scale_y'Access, Random_Float( 0.8, 3.0 ), INTERP_FAST, Random_Float( 0.7, 1.0 ) * max_duration );
                Anim_Delta( spr, spr.x'Access, Random_Float( 0.0, 20.0 ) * max_duration * Random_Sign, INTERP_FAST, Random_Float( 0.7, 1.0 ) * max_duration );
                Anim_Delta( spr, spr.y'Access, Random_Float( 0.0, 20.0 ) * max_duration * Random_Sign, INTERP_FAST, Random_Float( 0.7, 1.0 ) * max_duration );
            end loop;
        end Spawn_Water_Effects;

        ------------------------------------------------------------------------

        procedure Play_Element( typ : Element_Type; pitch : Integer; vol, pan : Float ) is
        begin
            if not Al_Play_Sample( element_samples(typ, pitch), vol, pan, 1.0, ALLEGRO_PLAYMODE_ONCE, null ) then
                null;
            end if;
        end Play_Element;

        ------------------------------------------------------------------------

        procedure Activate_Token( token : A_Token ) is
            sc : constant Float := token_scale;
        begin
            case token.typ is
                when TYPE_EARTH =>
                    Play_Element( TYPE_EARTH, token.pitch, 0.8, 0.0 );
                    Anim( token.top'Access, token.top.scale_x'Access, token.top.scale_x + 0.4, token.top.scale_x, INTERP_FAST, 0.3 );
                    Anim( token.top'Access, token.top.scale_y'Access, token.top.scale_y + 0.4, token.top.scale_y, INTERP_FAST, 0.3 );

                when TYPE_WIND =>
                    Play_Element( TYPE_WIND, token.pitch, 0.8, 0.0 );
                    Anim_Full( token.top'Access, token.top.scale_x'Access, sc * 1.0, sc * 0.8, INTERP_SLOW_IN_OUT, 0.0, 0.5 );
                    Anim_Full( token.top'Access, token.top.scale_x'Access, sc * 0.8, sc * 1.0, INTERP_SLOW_IN_OUT, 0.5, 0.8 );
                    Anim_Full( token.top'Access, token.top.scale_y'Access, sc * 1.0, sc * 0.8, INTERP_SLOW_IN_OUT, 0.0, 0.5 );
                    Anim_Full( token.top'Access, token.top.scale_y'Access, sc * 0.8, sc * 1.0, INTERP_SLOW_IN_OUT, 0.5, 0.8 );
                    Spawn_Wind_Effects( token.top.x, token.top.y );

                when TYPE_WATER =>
                    Play_Element( TYPE_WATER, token.pitch, 0.7, 0.5 );
                    Anim_Full( token.top'Access, token.top.scale_x'Access, sc * 1.3, sc * 0.8, INTERP_BOUNCE, 0.0, 0.5 );
                    Anim_Full( token.top'Access, token.top.scale_x'Access, sc * 0.8, sc * 1.0, INTERP_BOUNCE, 0.5, 0.5 );
                    Anim_Full( token.top'Access, token.top.scale_y'Access, sc * 0.8, sc * 1.3, INTERP_BOUNCE, 0.0, 0.5 );
                    Anim_Full( token.top'Access, token.top.scale_y'Access, sc * 1.3, sc * 1.0, INTERP_BOUNCE, 0.5, 0.5 );
                    Spawn_Water_Effects( token.top.x, token.top.y );

                when TYPE_FIRE =>
                    Play_Element( TYPE_FIRE, token.pitch, 0.8, 0.0 );
                    Anim( token.top'Access, token.top.scale_x'Access, sc * 1.3, sc, INTERP_SLOW_IN_OUT, 1.0 );
                    Anim( token.top'Access, token.top.scale_y'Access, sc * 1.3, sc, INTERP_SLOW_IN_OUT, 1.0 );
                    Spawn_Fire_Effects( token.top.x, token.top.y );

                when TYPE_NONE =>
                    pragma Assert( False, "Bad value in Activate_Token" );
                    null;

            end case;
        end Activate_Token;

        ------------------------------------------------------------------------

        procedure Update_Playback is
        begin
            for y in 0..TOKENS_Y-1 loop
                Activate_Token( tokens(y * TOKENS_X + playback_column) );
            end loop;

            playback_column := playback_column + 1;
            if playback_column >= TOKENS_X then
                playback_column := 0;
            end if;
        end Update_Playback;

        ------------------------------------------------------------------------
        -- Control                                                            --
        ------------------------------------------------------------------------

        function Is_Touched( token : A_Token;
                             size  : Float;
                             x, y  : Float ) return Boolean is
            half : constant Float := size / 2.0;
        begin
            return token.x - half <= x and then
                   x < token.x + half and then
                   token.y - half <= y and then
                   y < token.y + half;
        end Is_Touched;

        ------------------------------------------------------------------------

        function Get_Touched_Token( x, y : Float ) return A_Token is
        begin
            for i in tokens'Range loop
                if Is_Touched( tokens(i), Float(token_size), x, y ) then
                    return tokens(i);
                end if;
            end loop;
            return null;
        end Get_Touched_Token;

        ------------------------------------------------------------------------

        function Get_Touched_Button( x, y : Float ) return A_Token is
        begin
            for i in buttons'Range loop
                if Is_Touched( buttons(i), Float(button_size), x, y ) then
                    return buttons(i);
                end if;
            end loop;
            return null;
        end Get_Touched_Button;

        ------------------------------------------------------------------------

--          procedure Select_Token( token : A_Token ) is
--          begin
--              if token.typ = TYPE_NONE and then selected_button /= null then
--                  token.top.image := Img_Type'Val( Element_Type'Pos( selected_button.typ ) );
--                  Anim_To( token.top'Access, token.top.opacity'Access, 1.0, INTERP_FAST, 0.15 );
--                  token.typ := selected_button.typ;
--              end if;
--          end Select_Token;

        ------------------------------------------------------------------------

        procedure Unselect_Token( token : A_Token ) is
        begin
            if token.typ /= TYPE_NONE then
                Anim_Full( token.top'Access, token.top.opacity'Access, token.top.opacity,
                           0.0, INTERP_SLOW, 0.15, 0.15 );
                token.typ := TYPE_NONE;
            end if;
        end Unselect_Token;

        ------------------------------------------------------------------------

        procedure Select_Token( token : A_Token ) is
            prev_type : Elem_Type;
        begin
            if selected_button = null then
                return;
            end if;

            prev_type := token.typ;
            Unselect_Token( token );

            -- Unselect only if same type, for touch input.
            if prev_type /= selected_button.typ then
                token.top.image := Img_Type'Val( Element_Type'Pos( selected_button.typ ) );
                Anim_To( token.top'Access, token.top.opacity'Access, 1.0, INTERP_FAST, 0.15 );
                token.typ := selected_button.typ;
            end if;
        end Select_Token;

        ------------------------------------------------------------------------

        procedure Unselect_All_Tokens is
        begin
            for i in tokens'Range loop
                Unselect_Token( tokens(i) );
            end loop;
        end Unselect_All_Tokens;

        ------------------------------------------------------------------------

        procedure Change_Healthy_Glow( typ : Element_Type; x : Float ) is
        begin
            Anim_Tint( glow'Access, glow_color(typ), INTERP_SLOW_IN_OUT, 3.0 );
            Anim_To( glow'Access, glow.x'Access, x, INTERP_SLOW_IN_OUT, 3.0 );

            Anim_Tint( glow_overlay'Access, glow_color(typ), INTERP_SLOW_IN_OUT, 4.0 );
            Anim_To( glow_overlay'Access, glow_overlay.opacity'Access, 1.0, INTERP_SLOW_IN_OUT, 4.0 );
        end Change_Healthy_Glow;

        ------------------------------------------------------------------------

        procedure Select_Button( button : A_Token ) is
            spr : A_Sprite;
        begin
            if button = selected_button then
                return;
            end if;

            if selected_button /= null then
                spr := selected_button.top'Access;
                Anim_To( spr, spr.scale_x'Access, button_unsel_scale, INTERP_SLOW, 0.3 );
                Anim_To( spr, spr.scale_y'Access, button_unsel_scale, INTERP_SLOW, 0.3 );
                Anim_To( spr, spr.opacity'Access, 0.5, INTERP_DOUBLE_SLOW, 0.2 );

                spr := selected_button.bot'Access;
                Anim_To( spr, spr.scale_x'Access, dropshadow_unsel_scale, INTERP_SLOW, 0.3 );
                Anim_To( spr, spr.scale_y'Access, dropshadow_unsel_scale, INTERP_SLOW, 0.3 );
            end if;

            selected_button := button;

            spr := button.top'Access;
            Anim_To( spr, spr.scale_x'Access, button_sel_scale, INTERP_FAST, 0.3 );
            Anim_To( spr, spr.scale_y'Access, button_sel_scale, INTERP_FAST, 0.3 );
            Anim_To( spr, spr.opacity'Access, 1.0, INTERP_FAST, 0.3 );

            spr := button.bot'Access;
            Anim_To( spr, spr.scale_x'Access, dropshadow_sel_scale, INTERP_FAST, 0.3 );
            Anim_To( spr, spr.scale_y'Access, dropshadow_sel_scale, INTERP_FAST, 0.3 );

            Change_Healthy_Glow( button.typ, button.x );

            if not Al_Play_Sample( select_sample, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, null ) then
                null;
            end if;
        end Select_Button;

        ------------------------------------------------------------------------

        procedure On_Mouse_Down( x, y : Float; mbut : Unsigned_32 ) is
            token  : A_Token;
            button : A_Token;
        begin
            if mbut = 1 then
                token := Get_Touched_Token( x, y );
                if token /= null then
                    Select_Token( token );
                else
                    button := Get_Touched_Button( x, y );
                    if button /= null then
                        Select_Button( button );
                    end if;
                end if;
            elsif mbut = 2 then
                token := Get_Touched_Token( x, y );
                if token /= null then
                    Unselect_Token( token );
                end if;
            end if;
        end On_Mouse_Down;

        ------------------------------------------------------------------------

        procedure On_Mouse_Axes( x, y : Float ) is
            token : constant A_Token := Get_Touched_Token( x, y );
        begin
            if token = hover_token then
                return;
            end if;

            if hover_token /= null then
                Anim_To( hover_token.bot'Access, hover_token.bot.opacity'Access, 0.4, INTERP_DOUBLE_SLOW, 0.2 );
            end if;

            hover_token := token;

            if hover_token /= null then
                Anim_To( hover_token.bot'Access, hover_token.bot.opacity'Access, 0.7, INTERP_FAST, 0.2 );
            end if;
        end On_Mouse_Axes;

        ------------------------------------------------------------------------

        procedure Main_Loop( queue : A_Allegro_Event_Queue ) is
            event  : aliased Allegro_Event;
            redraw : Boolean := True;
        begin
            loop
                if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                    declare
                        now : constant Float := Float(Al_Get_Time);
                    begin
                        Free_Old_Flairs( now );
                        Update_Anims( now );
                        Draw_Screen;
                        redraw := False;
                    end;
                end if;

                Al_Wait_For_Event( queue, event );

                if event.timer.source = refresh_timer then
                    redraw := True;
                elsif event.timer.source = playback_timer then
                    Update_Playback;
                elsif event.any.typ = ALLEGRO_EVENT_MOUSE_AXES then
                    On_Mouse_Axes( Float(event.mouse.x), Float(event.mouse.y) );
                elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                    On_Mouse_Down( Float(event.mouse.x), Float(event.mouse.y), event.mouse.button );
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                    exit;
                elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exit;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_C then
                        Unselect_All_Tokens;
                    end if;
                end if;
            end loop;
        end Main_Loop;

        ------------------------------------------------------------------------

        queue : A_Allegro_Event_Queue;
    begin
        if not Al_Initialize then
            Abort_Example( "Error initializing Allegro." );
        end if;
        if not Al_Install_Audio or else not Al_Reserve_Samples( 128 ) then
            Abort_Example( "Error initializing audio." );
        end if;
        if not Al_Init_Acodec_Addon then
            Abort_Example( "Error initializing audio codecs." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Error initializing image addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR );

        display := Al_Create_Display( screen_w, screen_h );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;
        Al_Set_Window_Title( display, "Haiku - A Musical Instrument" );

        Load_Images;
        Load_Samples;

        Init_Tokens;
        Init_Buttons;
        Init_Glow;
        Select_Button( buttons(TYPE_EARTH) );

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard" );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Error installing mouse" );
        end if;

        refresh_timer := Al_Create_Timer( 1.0 / refresh_rate );
        playback_timer := Al_Create_Timer( playback_period / Long_Float(TOKENS_X) );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( refresh_timer ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( playback_timer ) );

        Al_Start_Timer( refresh_timer );
        Al_Start_Timer( playback_timer );

        Main_Loop( queue );

        Free_All_Flairs;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Haiku;
