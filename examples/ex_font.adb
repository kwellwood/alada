
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Font is

    procedure Ada_Main is

        EURO : constant String := "\xe2\x82\xac";
        --EURO : constant Character := Ada.Characters.Latin_9.Euro_Sign;

        procedure Wait_For_Esc( display : A_Allegro_Display ) is
            queue        : A_Allegro_Event_Queue;
            screen_clone : A_Allegro_Bitmap;
            event        : aliased Allegro_Event;
       begin
            if not Al_Install_Keyboard then
                Abort_Example( "Cannot install keyboard" );
            end if;
            queue := Al_Create_Event_Queue;
            Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
            Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
            screen_clone := Al_Clone_Bitmap( Al_Get_Target_Bitmap );

            loop
                Al_Wait_For_Event( queue, event );
                if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                    exit;
                elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exit;
                    end if;
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_EXPOSE then
                    declare
                        x : constant Float := Float(event.display.x);
                        y : constant Float := Float(event.display.y);
                        w : constant Float := Float(event.display.width);
                        h : constant Float := Float(event.display.height);
                    begin
                        Al_Draw_Bitmap_Region( screen_clone, x, y, w, h, x, y, 0 );
                        Al_Update_Display_Region( Integer(x), Integer(y), Integer(w), Integer(h) );
                    end;
                end if;
            end loop;

            Al_Destroy_Bitmap( screen_clone );
            Al_Destroy_Event_Queue( queue );
        end Wait_For_Esc;

        ------------------------------------------------------------------------

        ranges : constant Unicode_Range_Array := ((16#0020#, 16#007F#),  -- ASCII
                                                  (16#00A1#, 16#00FF#),  -- Latin 1
                                                  (16#0100#, 16#017F#),  -- Extended-A
                                                  (16#20AC#, 16#20AC#)); -- Euro

        display     : A_Allegro_Display;
        bitmap      : A_Allegro_Bitmap;
        font_bitmap : A_Allegro_Bitmap;
        f1, f2, f3  : A_Allegro_Font;
        x, y        : Float;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        Al_Set_New_Display_Option( ALLEGRO_SINGLE_BUFFER, 1, ALLEGRO_SUGGEST );
        Al_Set_New_Display_Flags( ALLEGRO_GENERATE_EXPOSE_EVENTS );
        display := Al_Create_Display( 640, 400 );
        if display = null then
            Abort_Example( "Failed to create display" );
        end if;
        bitmap := Al_Load_Bitmap( "data/mysha.pcx" );
        if bitmap = null then
            Abort_Example( "Failed to load mysha.pcx" );
        end if;

        f1 := Al_Load_Font( "data/bmpfont.tga", 0, 0 );
        if f1 = null then
            Abort_Example( "Failed to load bmpfont.tga" );
        end if;

        font_bitmap := Al_Load_Bitmap( "data/a4_font.tga" );
        if font_bitmap = null then
            Abort_Example( "Failed to load a4_font.tga" );
        end if;
        f2 := Al_Grab_Font_From_Bitmap( font_bitmap, ranges );

        f3 := Al_Create_Builtin_Font;
        if f3 = null then
            Abort_Example( "Failed to create builtin font." );
        end if;
        Init_Platform_Specific;

        -- Draw background
        Al_Draw_Scaled_Bitmap( bitmap, 0.0, 0.0, 320.0, 240.0, 0.0, 0.0, 640.0, 480.0, 0 );

        -- Draw red text
        Al_Draw_Text( f1, Al_Map_RGB( 255, 0, 0 ), 10.0, 10.0, 0, "red" );

        -- Draw green text
        Al_Draw_Text( f1, Al_Map_RGB( 0, 255, 0 ), 120.0, 10.0, 0, "green" );

        -- Draw a unicode symbol
        Al_Draw_Text( f2, Al_Map_RGB( 0, 0, 255 ), 60.0, 60.0, 0, "Mysha's 0.02 " & EURO );

        -- Draw a yellow text with the builtin font
        Al_Draw_Text( f3, Al_Map_RGB( 255, 255, 0 ), 20.0, 200.0,
                      ALLEGRO_ALIGN_CENTER, "a string from builtin font data" );

        -- Draw all individual glyphs the f2 font's range in rainbow colors.
        --
        x := 10.0;
        y := 300.0;
        Al_Draw_Text( f3, Al_Map_RGB (0, 255, 255 ), x,  y - 20.0, 0, "Draw glyphs: " );
        for r in 0..3 loop
            declare
                start   : constant Integer_32 := ranges(ranges'First + r).first;
                stop    : constant Integer_32 := ranges(ranges'First + r).last;
                width   : Integer;
                r, g, b : Float;
            begin
                for index in start..stop-1 loop
                    -- Use al_get_glyph_advance for the stride.
                    width := Al_Get_Glyph_Advance( f2, index, ALLEGRO_NO_KERNING );
                    r := abs(sin( ALLEGRO_PI * Float(index) * 36.0 / 360.0)) * 255.0;
                    g := abs(sin( ALLEGRO_PI * Float(index + 12) * 36.0 / 360.0)) * 255.0;
                    b := abs(sin( ALLEGRO_PI * Float(index + 24) * 36.0 / 360.0)) * 255.0;
                    Al_Draw_Glyph( f2, Al_Map_RGB_f( r, g, b ), x, y, index );
                    x := x + Float(width);
                    if x > Float(Al_Get_Display_Width( display ) - 10) then
                        x := 10.0;
                        y := y + Float(Al_Get_Font_Line_Height( f2 ));
                    end if;
                end loop;
            end;
        end loop;
        Al_Flip_Display;

        Wait_For_Esc( display );

        Al_Destroy_Bitmap( bitmap );
        Al_Destroy_Bitmap( font_bitmap );
        Al_Destroy_Font( f1 );
        Al_Destroy_Font( f2 );
        Al_Destroy_Font( f3 );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Font;
