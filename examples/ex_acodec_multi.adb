
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Text_IO;                       use Ada.Text_IO;
with Ada.Unchecked_Deallocation;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with GNAT.OS_Lib;

--
-- Milan Mimica
-- Audio example that plays multiple files at the same time
-- Originlly derived from the ex_acodec example.
--
package body Ex_Acodec_Multi is

    procedure Ada_Main is
        type Allegro_Sample_Array is array (Integer range <>) of A_Allegro_Sample;
        type A_Allegro_Sample_Array is access all Allegro_Sample_Array;

        procedure Free is new Ada.Unchecked_Deallocation( Allegro_Sample_Array,
                                                          A_Allegro_Sample_Array );

        type Allegro_Sample_Instance_Array is array (Integer range <>) of A_Allegro_Sample_Instance;
        type A_Allegro_Sample_Instance_Array is access all Allegro_Sample_Instance_Array;

        procedure Free is new Ada.Unchecked_Deallocation( Allegro_Sample_Instance_Array,
                                                          A_Allegro_Sample_Instance_Array );

        sample_data    : A_Allegro_Sample_Array;
        sample         : A_Allegro_Sample_Instance_Array;
        mixer          : A_Allegro_Mixer;
        voice          : A_Allegro_Voice;
        longest_sample : Float;
        sample_time    : Float;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if Argument_Count < 1 then
            Log_Print( "This example needs to be run from the command line." );
            Log_Print( "Usage: ex_acodec_multi {audio_files}" );
            Close_Log( True );
            GNAT.OS_Lib.OS_Exit( 1 );
        end if;

        if not Al_Init_Acodec_Addon then
            Abort_Example( "Could not init audio codecs." );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound!" );
        end if;

        sample := new Allegro_Sample_Instance_Array(1..Argument_Count);
        sample_data := new Allegro_Sample_Array(1..Argument_Count);

        -- a voice is used for playback
        voice := Al_Create_Voice( 44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2 );
        if voice = null then
            Abort_Example( "Could not create ALLEGRO_VOICE from sample" );
        end if;

        mixer := Al_Create_Mixer( 44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2 );
        if mixer = null then
            Abort_Example( "al_create_mixer failed." );
        end if;

        if not Al_Attach_Mixer_To_Voice( mixer, voice ) then
            Abort_Example( "al_attach_mixer_to_voice failed." );
        end if;

        for i in 1..Argument_Count loop
            sample(i) := null;

            -- loads the entire sound file from disk into sample data
            sample_data(i) := al_load_sample( Argument( i ) );
            if sample_data(i) = null then
                Abort_Example( "Could not load sample from '" & Argument( i ) & "'!" );
            end if;

            if sample_data(i) /= null then
                sample(i) := Al_Create_Sample_Instance( sample_data(i) );
                if sample(i) = null then
                    Log_Print( "Could not create sample!" );
                    Al_Destroy_Sample( sample_data(i) );
                end if;

                if sample_data(i) /= null and then
                   not Al_Attach_Sample_Instance_To_Mixer( sample(i), mixer )
                then
                    Log_Print( "al_attach_sample_instance_to_mixer failed." );
                end if;
            end if;
        end loop;

        longest_sample := 0.0;

        for i in 1..Argument_Count loop
            if sample(i) /= null then
                -- play each sample once
                if not Al_Play_Sample_Instance( sample(i) ) then
                    Log_Print( "Al_Play_Sample_Instance failed" );
                end if;

                sample_time := Al_Get_Sample_Instance_Time( sample(i) );
                Log_Print( "Playing '" & Argument( i ) & "' (" & sample_time'Img & " seconds)" );

                if sample_time > longest_sample then
                    longest_sample := sample_time;
                end if;
            end if;
        end loop;

        Al_Rest( Long_Float(longest_sample*1.1) );

        Log_Print( "Done" );

        for i in 1..Argument_Count loop
            -- free the memory allocated when creating the sample + voice
            if sample(i) /= null then
                if not Al_Stop_Sample_Instance( sample(i) ) then
                    Put_Line( "Failed to stop sample" & i'Img );
                end if;
                Al_Destroy_Sample_Instance( sample(i) );
                Al_Destroy_Sample( sample_data(i) );
            end if;
        end loop;
        Al_Destroy_Mixer( mixer );
        Al_Destroy_Voice( voice );

        Free( sample );
        Free( sample_data );

        Al_Uninstall_Audio;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Acodec_Multi;
