
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Drawpixels is

    procedure Ada_Main is
        WIDTH      : constant := 640;
        HEIGHT     : constant := 480;
        NUM_STARS  : constant := 300;
        TARGET_FPS : constant := 9999;

        type Point is
            record
                x, y : Float;
            end record;

        type Point_Array is array (Integer range <>) of Point;

        type Color_Array is array (Integer range <>) of Allegro_Color;

        type Float_Array is array (Integer range <>) of Float;

        speeds        : constant Float_Array(0..2) := (0.005, 0.05, 0.15);
        display       : A_Allegro_Display;
        key_state     : aliased Allegro_Keyboard_State;
        stars         : Point_Array(0..NUM_STARS-1);
        colors        : Color_Array(0..2);
        start,
        now,
        elapsed,
        frame_count   : Long_Float;
        total_frames  : Integer := 0;
        program_start : Long_Float;
        length        : Long_Float;
        lr            : A_Allegro_Locked_Region;
            pragma Warnings( Off, lr );
        gen           : Generator;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        display := Al_Create_Display( WIDTH, HEIGHT );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        colors(0) := Al_Map_RGBA( 255, 100, 255, 128 );
        colors(1) := Al_Map_RGBA( 255, 100, 100, 255 );
        colors(2) := Al_Map_RGBA( 100, 100, 255, 255 );

        Reset( gen );
        for layer in 0..2 loop
            for star in 0..NUM_STARS/3-1 loop
                declare
                    s : constant Integer := layer * (NUM_STARS / 3) + star;
                begin
                    stars(s).x := Float(Unsigned_32'(Random( gen )) mod WIDTH);
                    stars(s).y := Float(Unsigned_32'(Random( gen )) mod HEIGHT);
                end;
            end loop;
        end loop;

        start := Al_Get_Time * 1000.0;
        now := start;
        elapsed := 0.0;
        frame_count := 0.0;
        program_start := Al_Get_Time;

        loop
            if frame_count < 1000.0 / Long_Float(TARGET_FPS) then
                frame_count := frame_count + elapsed;
            else
                frame_count := frame_count - 1000.0 / Long_Float(TARGET_FPS);
                Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );

                for star in 0..NUM_STARS-3 loop
                    Al_Draw_Pixel( stars(0 + star).x, stars(0 + star).y, colors(0) );
                end loop;

                lr := Al_Lock_Bitmap( Al_Get_Backbuffer( display ), ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READWRITE );

                for layer in 1..2 loop
                    for star in 0..NUM_STARS/3-1 loop
                        declare
                            s : constant Integer := layer * (NUM_STARS / 3) + star;
                        begin
                            -- put_pixel ignores blending
                            Al_Put_Pixel( Integer(stars(s).x), Integer(stars(s).y),
                                          colors(layer) );
                        end;
                    end loop;
                end loop;

                -- Check that dots appear at the window extremes.
                declare
                    x : constant Integer := WIDTH - 1;
                    y : constant Integer := HEIGHT - 1;
                begin
                    Al_Put_Pixel( 0, 0, Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );
                    Al_Put_Pixel( X, 0, Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );
                    Al_Put_Pixel( 0, Y, Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );
                    Al_Put_Pixel( X, Y, Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );

                    Al_Unlock_Bitmap( Al_Get_Backbuffer( display ) );
                    Al_Flip_Display;
                    total_frames := total_frames + 1;
                end;
            end if;

            now := Al_Get_Time * 1000.0;
            elapsed := now - start;
            start := now;

            for layer in 0..2 loop
                for star in 0..NUM_STARS/3-1 loop
                    declare
                        s : constant Integer := layer * (NUM_STARS / 3) + star;
                    begin
                        stars(s).y := stars(s).y - speeds(layer) * Float(elapsed);
                        if stars(s).y < 0.0 then
                            stars(s).x := Float(Unsigned_32'(Random( gen )) mod WIDTH);
                            stars(s).y := Float(HEIGHT);
                        end if;
                    end;
                end loop;
            end loop;

            Al_Rest( 0.001 );

            Al_Get_Keyboard_State( key_state );
            exit when Al_Key_Down( key_state, ALLEGRO_KEY_ESCAPE );
        end loop;

        length := Al_Get_Time - program_start;

        if length /= 0.0 then
            Log_Print( Integer'Image( Integer(Long_Float(total_frames) / length) ) & " FPS" );
        end if;

        Al_Destroy_Display( display );

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Drawpixels;
