
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Fs_Window is

    procedure Ada_Main is

        display : A_Allegro_Display;
        picture : A_Allegro_Bitmap;
        queue   : A_Allegro_Event_Queue;
        font    : A_Allegro_Font;
        big     : Boolean := False;

        ------------------------------------------------------------------------

        procedure Redraw is
            w     : constant Float := Float(Al_Get_Display_Width( display ));
            h     : constant Float := Float(Al_Get_Display_Height( display ));
            pw    : constant Float := Float(Al_Get_Bitmap_Width( picture ));
            ph    : constant Float := Float(Al_Get_Bitmap_Height( picture ));
            th    : constant Float := Float(Al_Get_Font_Line_Height( font ));
            cx    : constant Float := (w - pw) * 0.5;
            cy    : constant Float := (h - ph) * 0.5;
            white : constant Allegro_Color := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
            color : Allegro_Color;
        begin
            color := Al_Map_RGB_f( 0.8, 0.7, 0.9 );
            Al_Clear_To_Color( color );

            color := Al_Map_RGB( 255, 0, 0 );
            Al_Draw_Line( 0.0, 0.0, w, h, color, 0.0 );
            Al_Draw_Line( 0.0, h, w, 0.0, color, 0.0 );

            Al_Draw_Bitmap( picture, cx, cy, 0 );

            Al_Draw_Text( font, white, w / 2.0, cy + ph, ALLEGRO_ALIGN_CENTRE, "Press Space to toggle fullscreen" );
            Al_Draw_Text( font, white, w / 2.0, cy + ph + th, ALLEGRO_ALIGN_CENTRE, "Press Enter to toggle window size" );
            if (Al_Get_Display_Flags( display ) and ALLEGRO_FULLSCREEN_WINDOW) > 0 then
                Al_Draw_Text( font, white, w / 2.0, cy + ph + th * 2.0, ALLEGRO_ALIGN_CENTRE,
                              "Window:" & Al_Get_Display_Width( display )'Img & " x" &
                              Al_Get_Display_Height( display )'Img & " (fullscreen)" );
            else
                Al_Draw_Text( font, white, w / 2.0, cy + ph + th * 2.0, ALLEGRO_ALIGN_CENTRE,
                              "Window:" & Al_Get_Display_Width( display )'Img & " x" &
                              Al_Get_Display_Height( display )'Img & " (not fullscreen)" );
            end if;

            Al_Flip_Display;
        end Redraw;

        ------------------------------------------------------------------------

        procedure Run is
            event : aliased Allegro_Event;
            quit  : Boolean := False;
        begin
            while not quit loop
                while Al_Get_Next_Event( queue, event'Access ) loop
                    if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                        quit := True;
                    elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            quit := True;
                        elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                            if not Al_Set_Display_Flag( display,
                                                        ALLEGRO_FULLSCREEN_WINDOW,
                                                        not ((Al_Get_Display_Flags( display ) and ALLEGRO_FULLSCREEN_WINDOW) > 0) )
                            then
                                Abort_Example( "Failed to toggle ALLEGRO_FULLSCREEN_WINDOW" );
                            end if;
                            Redraw;
                        elsif event.keyboard.keycode = ALLEGRO_KEY_ENTER then
                            big := not big;
                            if big then
                                if not Al_Resize_Display( display, 800, 600 ) then
                                    Abort_Example( "Failed to resize to 800x600" );
                                end if;
                            else
                                if not Al_Resize_Display( display, 640, 480 ) then
                                    Abort_Example( "Failed to resize to 800x600" );
                                end if;
                            end if;
                            Redraw;
                        end if;
                    end if;
                end loop;

                -- FIXME: Lazy timing
                Al_Rest( 0.02 );
                Redraw;
            end loop;
        end Run;

        ------------------------------------------------------------------------

    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_FULLSCREEN_WINDOW );
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        picture := Al_Load_Bitmap( "data/mysha.pcx" );
        if picture = null then
            Abort_Example( "mysha.pcx not found" );
        end if;

        font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        if font = null then
            Abort_Example( "data/fixed_font.tga not found." );
        end if;

        Redraw;
        Run;

        Al_Destroy_Display( display );
        Al_Destroy_Event_Queue( queue );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Fs_Window;
