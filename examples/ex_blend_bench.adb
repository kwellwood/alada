
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Real_Time;                     use Ada.Real_Time;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.State;                     use Allegro.State;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;

--
-- Benchmark for memory blenders
--
--
package body Ex_Blend_Bench is

    procedure Ada_Main is
        -- Do a few un-timed runs to switch CPU to performance mode and cache
        -- data and so on - seems to make the results more stable here.
        -- Also used to guess the number of timed iterations.
        --
        WARMUP : constant := 100;

        -- How many seconds the timing should approximately take - a fixed
        -- number of iterations is not enough on very fast systems but takes
        -- too long on slow systems.
        --
        TEST_TIME : constant := 5.0;

        type Blit_Mode is (
            ALL_BLIT,
            PLAIN_BLIT,
            SCALED_BLIT,
            ROTATE_BLIT
        );

        type String_Array is array(Blit_Mode) of access String;

        names : constant String_Array := (new String'(""),
                                          new String'("Plain blit"),
                                          new String'("Scaled blit"),
                                          new String'("Rotated blit"));

        display : A_Allegro_Display;

        ------------------------------------------------------------------------

        procedure Step( mode : Blit_Mode; b2 : A_Allegro_Bitmap ) is
        begin
            case mode is
                when ALL_BLIT =>
                    null;
                when PLAIN_BLIT =>
                    Al_Draw_Bitmap( b2, 0.0, 0.0, 0 );
                when SCALED_BLIT =>
                    Al_Draw_Scaled_Bitmap( b2, 0.0, 0.0, 320.0, 200.0, 0.0, 0.0, 640.0, 480.0, 0 );
                when ROTATE_BLIT =>
                    Al_Draw_Scaled_Rotated_Bitmap( b2, 10.0, 10.0, 10.0, 10.0, 2.0, 2.0, ALLEGRO_PI / 30.0, 0 );
            end case;
        end Step;

        ------------------------------------------------------------------------

        procedure Do_Test( mode : Blit_Mode ) is
            state  : aliased Allegro_State;
            b1, b2 : A_Allegro_Bitmap;
            REPEAT : Integer;
            t0, t1 : Time;
        begin
            Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );

            b1 := Al_Load_Bitmap( "data/mysha.pcx" );
            if b1 = null then
                Abort_Example( "Error loading data/mysha.pcx" );
            end if;

            b2 := Al_Load_Bitmap( "data/allegro.pcx" );
            if b2 = null then
                Abort_Example( "Error loading data/mysha.pcx" );
            end if;

            Al_Set_Target_Bitmap( b1 );
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Step( mode, b2 );

            -- Display the blended bitmap to the screen so we can see something.
            Al_Store_State( state, ALLEGRO_STATE_ALL );
            Al_Set_Target_Backbuffer( display );
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            Al_Draw_Bitmap( b1, 0.0, 0.0, 0 );
            Al_Flip_Display;
            Al_Restore_State( state );

            Log_Print( "Benchmark: " & names(mode).all );
            Log_Print( "Please wait..." );

            -- Do warmup run and estimate required runs for real test.
            t0 := Clock;
            for i in 1..WARMUP loop
                Step( mode, b2 );
            end loop;
            t1 := Clock;
            REPEAT := Integer(TEST_TIME * 100.0 / To_Duration( t1 - t0 ));

            -- Do the real test.
            t0 := Clock;
            for i in 1..REPEAT loop
                Step( mode, b2 );
            end loop;
            t1 := Clock;

            Log_Print( "Time =" & To_Duration( t1 - t0 )'Img & " [s]," & REPEAT'Img & " steps" );
            Log_Print( names(mode).all & ":" & Integer'Image( Integer(Float(REPEAT) / Float(To_Duration( t1 - t0 ))) ) & " FPS" );
            Log_Print( "Done" );

            Al_Destroy_Bitmap( b1 );
            Al_Destroy_Bitmap( b2 );
        end Do_Test;

        ------------------------------------------------------------------------

        mode : Blit_Mode := ALL_BLIT;
    begin
        if Argument_Count = 1 then
            mode := Blit_Mode'Value( Argument( 1 ) );
            if not mode'Valid then
                mode := ALL_BLIT;
            end if;
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro" );
        end if;

        Open_Log;

        if not Al_Init_Image_Addon then
            Abort_Example( "Error initializing image IO addon" );
        elsif not Al_Init_Primitives_Addon then
            Abort_Example( "Error initializing primitives addon" );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        if mode = ALL_BLIT then
            for mode in PLAIN_BLIT..ROTATE_BLIT loop
                Do_Test( mode );
            end loop;
        else
            Do_Test( mode );
        end if;

        Al_Destroy_Display( display );

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Blend_Bench;
