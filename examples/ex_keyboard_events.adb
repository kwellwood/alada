
with Ada.Integer_Text_IO;               use Ada.Integer_Text_IO;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
--    Example program for the Allegro library, by Peter Wang.
--    Updated by Ryan Dickie.
--
--    This program tests keyboard events.
--
package body Ex_Keyboard_Events is

    procedure Ada_Main is

        WIDTH    : constant := 640;
        HEIGHT   : constant := 480;

        -- globals
        event_queue : A_Allegro_Event_Queue;
        display     : A_Allegro_Display;

        ------------------------------------------------------------------------

        procedure Log_Key( how       : String;
                           keycode   : Integer;
                           unichar   : Integer;
                           modifiers : Unsigned_32 ) is

            function Pad8( str : String ) return String is
            begin
                if str'Length < 8 then
                    return str & ((8 - str'Length) * ' ');
                end if;
                return str;
            end Pad8;

            function NPad( n : Integer; len : Integer ) return String is
                str : constant String := Trim( Integer'Image( n ), Left );
            begin
                if str'Length < len then
                    return ((len - str'Length) * '0') & str;
                end if;
                return str;
            end NPad;

            key_name : constant String := Al_Keycode_To_Name( keycode );
            unich    : Character := Character'Val( unichar );
            hex      : String(1..8) := (others => ' ');
        begin
            if unich < ' ' then
                unich := ' ';
            end if;
            Put( to => hex, item => Integer(modifiers), Base => 16 );
            Log_Print( Pad8( how ) & "  code=" & NPad( keycode, 3 ) & ", char='" &
                       unich & "' (" & NPad( unichar, 4 ) & "), modifiers=" &
                       Pad8( Trim( hex, Right ) ) & ", [" & key_name & "]" );
        end Log_Key;

        ------------------------------------------------------------------------

        -- main_loop:
        --  The main loop of the program.  Here we wait for events to come in from
        --  any one of the event sources and react to each one accordingly.  While
        --  there are no events to react to the program sleeps and consumes very
        --  little CPU time.  See main() to see how the event sources and event queue
        --  are set up.
        --
        procedure Main_Loop is
            event : Allegro_Event;
        begin
            Log_Print( "Focus on the main window (black) and press keys to see events." );
            Log_Print( "Escape quits.");

            loop
                -- Take the next event out of the event queue, and store it in `event'.
                Al_Wait_For_Event( event_queue, event );

                -- Check what type of event we got and act accordingly.  ALLEGRO_EVENT
                -- is a union type and interpretation of its contents is dependent on
                -- the event type, which is given by the 'type' field.
                --
                -- Each event also comes from an event source and has a timestamp.
                -- These are accessible through the 'any.source' and 'any.timestamp'
                -- fields respectively, e.g. 'event.any.timestamp'
                --
                case event.any.typ is

                    -- ALLEGRO_EVENT_KEY_DOWN - a keyboard key was pressed.
                    --
                    when ALLEGRO_EVENT_KEY_DOWN =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            return;
                        end if;
                        Log_Key( "KEY_DOWN", event.keyboard.keycode, 0, 0 );

                    -- ALLEGRO_EVENT_KEY_UP - a keyboard key was released.
                    --
                    when ALLEGRO_EVENT_KEY_UP =>
                        Log_Key( "KEY_UP", event.keyboard.keycode, 0, 0 );

                    -- ALLEGRO_EVENT_KEY_CHAR - a character was typed or repeated.
                    --
                    when ALLEGRO_EVENT_KEY_CHAR =>
                        if To_Ada( event.keyboard.repeat ) then
                            Log_Key( "repeat",
                                     event.keyboard.keycode,
                                     event.keyboard.unichar,
                                     event.keyboard.modifiers );
                        else
                            Log_Key( "KEY_CHAR",
                                     event.keyboard.keycode,
                                     event.keyboard.unichar,
                                     event.keyboard.modifiers );
                        end if;

                    -- ALLEGRO_EVENT_DISPLAY_CLOSE - the window close button was pressed.
                    --
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        return;

                    -- ALLEGRO_EVENT_DISPLAY_SWITCH_OUT - the app switched to background
                    when ALLEGRO_EVENT_DISPLAY_SWITCH_OUT =>
                        Al_Clear_Keyboard_State( event.display.source );
                        Log_Print( "Cleared keyboard state" );

                    -- We received an event of some type we don't know about.
                    -- Just ignore it.
                    --
                    when others =>
                        null;

                end case;
            end loop;
        end Main_Loop;

        ------------------------------------------------------------------------

    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log_Monospace;

        display := Al_Create_Display( WIDTH, HEIGHT );
        if display = null then
            Abort_Example( "al_create_display failed" );
        end if;
        Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
        Al_Flip_Display;

        if not Al_Install_Keyboard then
            Abort_Example( "al_install_keyboard failed" );
        end if;

        event_queue := Al_Create_Event_Queue;
        if event_queue = null then
            Abort_Example( "al_create_event_queue failed" );
        end if;

        Al_Register_Event_Source( event_queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( event_queue, Al_Get_Display_Event_Source( display ) );

        Main_Loop;

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Keyboard_Events;
