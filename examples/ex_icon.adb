
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Icon is

    procedure Ada_Main is
        display : A_Allegro_Display;
        icon1,
        icon2   : A_Allegro_Bitmap;
        queue   : A_Allegro_Event_Queue;
        timer   : A_Allegro_Timer;
        u, v    : Integer;
        event   : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 320, 200 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
        Al_Flip_Display;

        -- First icon: Read from file.
        icon1 := Al_Load_Bitmap( "data/icon.tga" );
        if icon1 = null then
            Abort_Example( "icon.tga not found" );
        end if;

        -- Second icon: Create it.
        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        icon2 := Al_Create_Bitmap( 16, 16 );
        Al_Set_Target_Bitmap( icon2 );
        for i in 0..255 loop
            u := i mod 16;
            v := i / 16;
            Al_Put_Pixel( u, v, Al_Map_RGB_f( Float(u) / 15.0, Float(v) / 15.0, 1.0 ) );
        end loop;
        Al_Set_Target_Backbuffer( display );

        Al_Set_Window_Title( display, "Changing icon example" );

        timer := Al_Create_Timer( 0.5 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( queue, event );

            if event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
               event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
            then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                if event.timer.count mod 2 = 1 then
                    Al_Set_Display_Icon( display, icon2 );
                else
                    Al_Set_Display_Icon( display, icon1 );
                end if;
            end if;
        end loop;

        Al_Uninstall_System;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Icon;
