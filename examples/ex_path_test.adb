
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Paths;                     use Allegro.Paths;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with GNAT.OS_Lib;

--
--    Example program for the Allegro library.
--
--    Stress test path routines.
--
package body Ex_Path_Test is

    procedure Ada_Main is

        type Test_Access is access procedure;

        type Test_Array is array (Positive range <>) of Test_Access;

        error : Integer := 0;

        ------------------------------------------------------------------------

        function Check( x : A_Allegro_Path ) return A_Allegro_Path is
        begin
            if x /= null then
                Log_Print( "OK" );
            else
                Log_Print( "FAIL" );
                error := error + 1;
            end if;
            return x;
        end Check;

        ------------------------------------------------------------------------

        procedure Check( x : Boolean ) is
        begin
            if x then
                Log_Print( "OK" );
            else
                Log_Print( "FAIL" );
                error := error + 1;
            end if;
        end Check;

        ------------------------------------------------------------------------

        procedure Check_Eq( l, r : String ) is
        begin
            Check( l = r );
        end Check_Eq;

        ------------------------------------------------------------------------

        -- Test al_create_path, al_get_path_num_components, al_get_path_component,
        -- al_get_path_drive, al_get_path_filename, al_destroy_path.
        --
        procedure t1 is
            path : A_Allegro_Path;
        begin
            path := Check( Al_Create_Path( "" ) );
            Al_Destroy_Path( path );

            path := Check( Al_Create_Path( "" ) );
            Check( Al_Get_Path_Num_Components( path ) = 0 );
            Check_Eq( Al_Get_Path_Drive( path ), "" );
            Check_Eq( Al_Get_Path_Filename( path ), "" );
            Al_Destroy_Path( path );

            -- . is a directory component.
            path := Check( Al_Create_Path( "." ) );
            Check( Al_Get_Path_Num_Components( path ) = 1 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), "." );
            Check_Eq( Al_Get_Path_Drive( path ), "" );
            Check_Eq( Al_Get_Path_Filename( path ), "" );
            Al_Destroy_Path( path );

            -- .. is a directory component.
            path := Check( Al_Create_Path( ".." ) );
            Check( Al_Get_Path_Num_Components( path ) = 1 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), ".." );
            Check_Eq( Al_Get_Path_Drive( path ), "" );
            Check_Eq( Al_Get_Path_Filename( path ), "" );
            Al_Destroy_Path( path );

            -- Relative path.
            path := Check( Al_Create_Path( "abc/def/.." ) );
            Check( Al_Get_Path_Num_Components( path ) = 3 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "def" );
            Check_Eq( Al_Get_Path_Component( path, 2 ), ".." );
            Check_Eq( Al_Get_Path_Drive( path ), "" );
            Check_Eq( Al_Get_Path_Filename( path ), "" );
            Al_Destroy_Path( path );

            -- Absolute path.
            path := Check( Al_Create_Path( "/abc/def/.." ) );
            Check( Al_Get_Path_Num_Components( path ) = 4 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), "" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 2 ), "def" );
            Check_Eq( Al_Get_Path_Component( path, 3 ), ".." );
            Check_Eq( Al_Get_Path_Drive( path ), "" );
            Check_Eq( Al_Get_Path_Filename( path ), "" );
            Al_Destroy_Path( path );

            -- Directories + filename.
            path := Check( Al_Create_Path( "/abc/def/ghi" ) );
            Check( Al_Get_Path_Num_Components( path ) = 3 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), "" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 2 ), "def" );
            Check_Eq( Al_Get_Path_Drive( path ), "" );
            Check_Eq( Al_Get_Path_Filename( path ), "ghi" );
            Al_Destroy_Path( path );
        end t1;

        ------------------------------------------------------------------------

        -- Test parsing UNC paths.
        procedure t2 is
    #if WINDOWS'Defined then
            path : A_Allegro_Path;
    #end if;
        begin
    #if WINDOWS'Defined then
            -- The mixed slashes are deliberate.
            -- Good paths.
            path := Check( Al_Create_Path( "//server\share name/dir/filename" ) );
            Check_Eq( Al_Get_Path_Drive( path ), "//server" );
            Check( Al_Get_Path_Num_Components( path ) = 2 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), "share name" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "dir" );
            Check_Eq( Al_Get_Path_Filename( path ), "filename" );
            Al_Destroy_Path( path );

            -- Bad paths.
            Check( Al_Create_Path( "//" ) = null );
            Check( Al_Create_Path( "//filename" ) = null );
            Check( Al_Create_Path( "///share/name/filename" ) = null );
    #else
            Log_Print( "Skipping t2, Windows-only test" );
    #end if;
        end t2;

        ------------------------------------------------------------------------

        -- Test parsing drive letter paths.
        procedure t3 is
    #if WINDOWS'Defined then
            path : A_Allegro_Path;
    #end if;
        begin
    #if WINDOWS'Defined then
            -- The mixed slashes are deliberate.

            path := Check( Al_Create_Path( "c:abc\def/ghi" ) );
            Check_Eq( Al_Get_Path_Drive( path ), "c:" );
            Check( Al_Get_Path_Num_Components( path ) = 2 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "def" );
            Check_Eq( Al_Get_Path_Filename( path ), "ghi" );
            Check_Eq( Al_Path_Cstr( path, '\' ), "c:abc\def\ghi" );
            Al_Destroy_Path( path );

            path := Check( Al_Create_Path( "c:\abc/def\ghi" ) );
            Check_Eq( Al_Get_Path_Drive( path ), "c:" );
            Check( Al_Get_Path_Num_Components( path ) = 3 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), "" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 2 ), "def" );
            Check_Eq( Al_Get_Path_Filename( path ), "ghi" );
            Check_Eq( Al_Path_Cstr( path, '\' ), "c:\abc\def\ghi" );
            Al_Destroy_Path( path );
    #else
            Log_Print( "Skipping t3, Windows-only test" );
    #end if;
        end t3;

        ------------------------------------------------------------------------

        -- Test al_append_path_component.
        procedure t4 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "" );

            Check( Al_Get_Path_Num_Components( path ) = 0 );

            Al_Append_Path_Component( path, "abc" );
            Al_Append_Path_Component( path, "def" );
            Al_Append_Path_Component( path, "ghi" );

            Check( Al_Get_Path_Num_Components( path ) = 3 );

            Check_Eq( Al_Get_Path_Component( path, 0 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "def" );
            Check_Eq( Al_Get_Path_Component( path, 2 ), "ghi" );

            Check_Eq( Al_Get_Path_Component( path, -1 ), "ghi" );
            Check_Eq( Al_Get_Path_Component( path, -2 ), "def" );
            Check_Eq( Al_Get_Path_Component( path, -3 ), "abc" );

            Al_Destroy_Path( path );
        end t4;

        ------------------------------------------------------------------------

        -- Test al_replace_path_component.
        procedure t5 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "" );

            Al_Append_Path_Component( path, "abc" );
            Al_Append_Path_Component( path, "INKY" );
            Al_Append_Path_Component( path, "def" );
            Al_Append_Path_Component( path, "BLINKY" );
            Al_Append_Path_Component( path, "ghi" );

            Check( Al_Get_Path_Num_Components( path ) = 5 );

            Al_Replace_Path_Component( path, 1, "PINKY" );
            Al_Replace_Path_Component( path, -2, "CLYDE" );

            Check( Al_Get_Path_Num_Components( path ) = 5 );

            Check_Eq( Al_Get_Path_Component( path, 0 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "PINKY" );
            Check_Eq( Al_Get_Path_Component( path, 2 ), "def" );
            Check_Eq( Al_Get_Path_Component( path, 3 ), "CLYDE" );
            Check_Eq( Al_Get_Path_Component( path, 4 ), "ghi" );

            Al_Destroy_Path( path );
        end t5;

        ------------------------------------------------------------------------

        -- Test al_remove_path_component.
        procedure t6 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "" );

            Al_Append_Path_Component( path, "abc" );
            Al_Append_Path_Component( path, "INKY" );
            Al_Append_Path_Component( path, "def" );
            Al_Append_Path_Component( path, "BLINKY" );
            Al_Append_Path_Component( path, "ghi" );

            Check( Al_Get_Path_Num_Components( path ) = 5 );

            Al_Remove_Path_Component( path, 1 );
            Check( Al_Get_Path_Num_Components( path ) = 4 );

            Al_Remove_Path_Component( path, -2 );
            Check( Al_Get_Path_Num_Components( path ) = 3 );

            Check_Eq( Al_Get_Path_Component( path, 0 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "def" );
            Check_Eq( Al_Get_Path_Component( path, 2 ), "ghi" );

            Al_Destroy_Path( path );
        end t6;

        ------------------------------------------------------------------------

        -- Test al_insert_path_component.
        procedure t7 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "INKY/BLINKY/" );

            Al_Insert_Path_Component( path, 0, "abc" );
            Al_Insert_Path_Component( path, 2, "def" );
            Al_Insert_Path_Component( path, 4, "ghi" );

            Check( Al_Get_Path_Num_Components( path ) = 5 );
            Check_Eq( Al_Get_Path_Component( path, 0 ), "abc" );
            Check_Eq( Al_Get_Path_Component( path, 1 ), "INKY" );
            Check_Eq( Al_Get_Path_Component( path, 2 ), "def" );
            Check_Eq( Al_Get_Path_Component( path, 3 ), "BLINKY" );
            Check_Eq( Al_Get_Path_Component( path, 4 ), "ghi" );

            Al_Destroy_Path( path );
        end t7;

        ------------------------------------------------------------------------

        -- Test al_get_path_tail, al_drop_path_tail.
        procedure t8 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "" );

            Check( Al_Get_Path_Tail( path ) = "" );

            Al_Append_Path_Component( path, "abc" );
            Al_Append_Path_Component( path, "def" );
            Al_Append_Path_Component( path, "ghi" );
            Check_Eq( Al_Get_Path_Tail( path ), "ghi" );

            Al_Drop_Path_Tail( path );
            Check_Eq( Al_Get_Path_Tail( path ), "def" );

            Al_Drop_Path_Tail(path );
            Al_Drop_Path_Tail(path );
            Check( Al_Get_Path_Tail( path ) = "" );

            -- Drop tail from already empty path.
            Al_Drop_Path_Tail(path );
            Check( Al_Get_Path_Tail( path ) = "" );

            Al_Destroy_Path( path );
        end t8;

        ------------------------------------------------------------------------

        -- Test al_set_path_drive, al_set_path_filename, al_path_cstr.
        --
        procedure t9 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "" );

            Check_Eq( Al_Path_Cstr( path, '/' ), "" );

            -- Drive letters.
            Al_Set_Path_Drive( path, "c:" );
            Check_Eq( Al_Path_Cstr( path, '/' ), "c:" );
            Check_Eq( Al_Get_Path_Drive( path ), "c:" );

            Al_Set_Path_Drive( path, "d:" );
            Check_Eq( Al_Path_Cstr( path, '/' ), "d:" );

            -- Plus directory components.
            Al_Append_Path_Component( path, "abc" );
            Al_Append_Path_Component( path, "def" );
            Check_Eq( Al_Path_Cstr( path, '/' ), "d:abc/def/" );

            -- Plus filename.
            Al_Set_Path_Filename( path, "uvw" );
            Check_Eq( Al_Path_Cstr( path, '/' ), "d:abc/def/uvw" );
            Check_Eq( Al_Get_Path_Filename( path ), "uvw" );

            -- Replace filename.
            Al_Set_Path_Filename( path, "xyz" );
            Check_Eq( Al_Path_Cstr( path, '/' ), "d:abc/def/xyz" );

            -- Remove drive.
            Al_Set_Path_Drive( path, "" );
            Check_Eq( Al_Path_Cstr( path, '/' ), "abc/def/xyz" );

            -- Remove filename.
            Al_Set_Path_Filename( path, "" );
            Check_Eq( Al_Path_Cstr( path, '/' ), "abc/def/" );

            Al_Destroy_Path( path );
        end t9;

        ------------------------------------------------------------------------

        -- Test al_join_paths.
        procedure t10 is
            path1 : A_Allegro_Path;
            path2 : A_Allegro_Path;
        begin
            -- Both empty.
            path1 := Al_Create_Path( "" );
            path2 := Al_Create_Path( "" );
            Check( Al_Join_Paths( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path1, '/' ), "" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );

            -- Both just filenames.
            path1 := Al_Create_Path( "file1" );
            path2 := Al_Create_Path( "file2" );
            Check( Al_Join_Paths( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path1, '/' ), "file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );

            -- Both relative paths.
            path1 := Al_Create_Path( "dir1a/dir1b/file1" );
            path2 := Al_Create_Path( "dir2a/dir2b/file2" );
            Check( Al_Join_Paths( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path1, '/' ),
                      "dir1a/dir1b/dir2a/dir2b/file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );

    #if WINDOWS'Defined then
            -- Both relative paths with drive letters.
            path1 := Al_Create_Path( "d:dir1a/dir1b/file1" );
            path2 := Al_Create_Path( "e:dir2a/dir2b/file2" );
            Check( Al_Join_Paths( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path1, '/' ), "d:dir1a/dir1b/dir2a/dir2b/file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );
    #else
            Log_Print( "Skipping, Windows only" );
    #end if;

            -- Path1 absolute, path2 relative.
            path1 := Al_Create_Path( "/dir1a/dir1b/file1" );
            path2 := Al_Create_Path( "dir2a/dir2b/file2" );
            Check( Al_Join_Paths( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path1, '/' ), "/dir1a/dir1b/dir2a/dir2b/file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );

            -- Both paths absolute.
            path1 := Al_Create_Path( "/dir1a/dir1b/file1" );
            path2 := Al_Create_Path( "/dir2a/dir2b/file2" );
            Check( not Al_Join_Paths( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path1, '/' ), "/dir1a/dir1b/file1" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );
        end t10;

        ------------------------------------------------------------------------

        -- Test al_rebase_path.
        procedure t11 is
            path1 : A_Allegro_Path;
            path2 : A_Allegro_Path;
        begin
            -- Both empty.
            path1 := Al_Create_Path( "" );
            path2 := Al_Create_Path( "" );
            Check( Al_Rebase_Path( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path2, '/' ), "" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );

            -- Both just filenames.
            path1 := Al_Create_Path( "file1" );
            path2 := Al_Create_Path( "file2" );
            Check( Al_Rebase_Path( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path2, '/' ), "file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );

            -- Both relative paths.
            path1 := Al_Create_Path( "dir1a/dir1b/file1" );
            path2 := Al_Create_Path( "dir2a/dir2b/file2" );
            Check( Al_Rebase_Path( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path2, '/' ),
                      "dir1a/dir1b/dir2a/dir2b/file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );

    #if WINDOWS'Defined then
            -- Both relative paths with drive letters.
            path1 := Al_Create_Path( "d:dir1a/dir1b/file1" );
            path2 := Al_Create_Path( "e:dir2a/dir2b/file2" );
            Check( Al_Rebase_Path( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path2, '/' ), "d:dir1a/dir1b/dir2a/dir2b/file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );
    #else
            Log_Print( "Skipping, Windows only" );
    #end if;

            -- Path1 absolute, path2 relative.
            path1 := Al_Create_Path( "/dir1a/dir1b/file1" );
            path2 := Al_Create_Path( "dir2a/dir2b/file2" );
            Check( Al_Rebase_Path( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path2, '/' ), "/dir1a/dir1b/dir2a/dir2b/file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );

            -- Both paths absolute.
            path1 := Al_Create_Path( "/dir1a/dir1b/file1" );
            path2 := Al_Create_Path( "/dir2a/dir2b/file2" );
            Check( not Al_Rebase_Path( path1, path2 ) );
            Check_Eq( Al_Path_Cstr( path2, '/' ), "/dir2a/dir2b/file2" );
            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );
        end t11;

        ------------------------------------------------------------------------

        -- Test al_set_path_extension, al_get_path_extension.
        procedure t12 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "" );

            -- Get null extension.
            Check_Eq( Al_Get_Path_Extension( path ), "" );

            -- Set extension on null filename.
            Check( not Al_Set_Path_Extension( path, "ext" ) );
            Check_Eq( Al_Get_Path_Filename( path ), "" );

            -- Set extension on extension-less filename.
            Al_Set_Path_Filename( path, "abc" );
            Check( Al_Set_Path_Extension( path, ".ext" ) );
            Check_Eq( Al_Get_Path_Filename( path ), "abc.ext" );

            -- Replacing extension.
            Al_Set_Path_Filename( path, "abc.def" );
            Check( Al_Set_Path_Extension( path, ".ext" ) );
            Check_Eq( Al_Get_Path_Filename( path ), "abc.ext" );
            Check_Eq( Al_Get_Path_Extension( path ), ".ext" );

            -- Filename with multiple dots.
            Al_Set_Path_Filename( path, "abc.def.ghi" );
            Check( Al_Set_Path_Extension(path, ".ext" ) );
            Check_Eq( Al_Get_Path_Filename( path ), "abc.def.ext" );
            Check_Eq( Al_Get_Path_Extension(path ), ".ext" );

            Al_Destroy_Path( path );
        end t12;

        ------------------------------------------------------------------------

        -- Test al_get_path_basename.
        procedure t13 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "" );

            -- No filename.
            Al_Set_Path_Filename( path, "" );
            Check_Eq( Al_Get_Path_Basename( path ), "" );

            -- No extension.
            Al_Set_Path_Filename( path, "abc" );
            Check_Eq( Al_Get_Path_Basename( path ), "abc" );

            -- Filename with a single dot.
            Al_Set_Path_Filename( path, "abc.ext" );
            Check_Eq( Al_Get_Path_Basename( path ), "abc" );

            -- Filename with multiple dots.
            Al_Set_Path_Filename( path, "abc.def.ghi" );
            Check_Eq( Al_Get_Path_Basename( path ), "abc.def" );

            Al_Destroy_Path( path );
        end t13;

        ------------------------------------------------------------------------

        -- Test al_clone_path.
        procedure t14 is
            path1 : A_Allegro_Path;
            path2 : A_Allegro_Path;
        begin
            path1 := Al_Create_Path( "/abc/def/ghi" );
            path2 := Al_Clone_Path( path1 );

            Check_Eq( Al_Path_Cstr( path1, '/' ), Al_Path_Cstr( path2, '/' ) );

            Al_Replace_Path_Component( path2, 2, "DEF" );
            Al_Set_Path_Filename( path2, "GHI" );
            Check_Eq( Al_Path_Cstr( path1, '/' ), "/abc/def/ghi" );
            Check_Eq( Al_Path_Cstr( path2, '/' ), "/abc/DEF/GHI" );

            Al_Destroy_Path( path1 );
            Al_Destroy_Path( path2 );
        end t14;

        ------------------------------------------------------------------------

        procedure t15 is
        begin
            -- nothing
            Log_Print( "Skipping empty test..." );
        end t15;

        ------------------------------------------------------------------------

        procedure t16 is
        begin
            -- nothing
            Log_Print( "Skipping empty test..." );
        end t16;

        ------------------------------------------------------------------------

        -- Test al_make_path_canonical.
        procedure t17 is
            path : A_Allegro_Path;
        begin
            path := Al_Create_Path( "/../.././abc/./def/../../ghi/jkl" );
            Check( Al_Make_Path_Canonical( path ) );
            Check( Al_Get_Path_Num_Components( path ) = 6 );
            Check_Eq( Al_Path_Cstr( path, '/' ), "/abc/def/../../ghi/jkl" );
            Al_Destroy_Path( path );

            path := Al_Create_Path( "../.././abc/./def/../../ghi/jkl" );
            Check( Al_Make_Path_Canonical( path ) );
            Check( Al_Get_Path_Num_Components( path ) = 7 );
            Check_Eq( Al_Path_Cstr( path, '/' ), "../../abc/def/../../ghi/jkl" );
            Al_Destroy_Path( path );
        end t17;

        ------------------------------------------------------------------------

        all_tests : constant Test_Array := (t1'Access,  t2'Access,  t3'Access,
                                            t4'Access,  t5'Access,  t6'Access,
                                            t7'Access,  t8'Access,  t9'Access,
                                            t10'Access, t11'Access, t12'Access,
                                            t13'Access, t14'Access, t15'Access,
                                            t16'Access, t17'Access);

    begin
        if not Al_Initialize then
            Abort_Example( "Could not initialize Allegro." );
        end if;
        Open_Log;

        if Argument_Count = 0 then
            for i in all_tests'Range loop
                Log_Print( "# t" & i'Img );
                Log_Print( "" );
                all_tests(i).all;
                Log_Print( "" );
            end loop;
        else
            declare
                i : constant Integer := Integer'Value( Argument( 1 ) );
            begin
                if i in all_tests'Range then
                    all_tests(i).all;
                end if;
            end;
        end if;
        Log_Print( "Done" );

        if error > 0 then
            Log_Print( error'Img & " errors" );
        end if;

        Close_Log( True );

        if error > 0 then
            GNAT.OS_Lib.OS_Exit( 1 );
        end if;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Path_Test;
