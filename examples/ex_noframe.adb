
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Noframe is

    procedure Ada_Main is
        display : A_Allegro_Display;
        bitmap  : A_Allegro_Bitmap;
        events  : A_Allegro_Event_Queue;
        event   : Allegro_Event;
        down    : Boolean := False;
        down_x,
        down_y  : Integer := 0;
        timer   : A_Allegro_Timer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image IO addon" );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_FRAMELESS );
        display := Al_Create_Display( 300, 200 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        bitmap := Al_Load_Bitmap( "data/fakeamp.bmp" );
        if bitmap = null then
            Abort_Example( "Error loading fakeamp.bmp" );
        end if;

        timer := Al_Create_Timer( 1.0 / 30.0 );

        events := Al_Create_Event_Queue;
        Al_Register_Event_Source( events, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( events, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( events, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( events, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( events, event );
            case event.any.typ is
                when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                    if event.mouse.button = 1 and then event.mouse.x > 0 then
                        down := True;
                        down_x := event.mouse.x;
                        down_y := event.mouse.y;
                    end if;
                    if event.mouse.button = 2 then
                        if not Al_Set_Display_Flag( display, ALLEGRO_FRAMELESS,
                                                    (Al_Get_Display_Flags( display ) and ALLEGRO_FRAMELESS) = 0 ) then
                            null;
                        end if;
                    end if;
                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    exit;
                when ALLEGRO_EVENT_MOUSE_BUTTON_UP =>
                    if event.mouse.button = 1 then
                        down := False;
                    end if;
                when ALLEGRO_EVENT_MOUSE_AXES =>
                   if down then
                       declare
                           cx, cy : aliased Integer := 0;
                       begin
                           if Al_Get_Mouse_Cursor_Position( cx'Access, cy'Access ) then
                               Al_Set_Window_Position( display, cx - down_x, cy - down_y );
                           end if;
                       end;
                   end if;
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exit;
                    end if;
                when ALLEGRO_EVENT_TIMER =>
                    Al_Draw_Bitmap( bitmap, 0.0, 0.0, 0 );
                    Al_Flip_Display;
                when others =>
                    null;
            end case;
        end loop;

        Al_Destroy_Timer( timer );
        Al_Destroy_Event_Queue( events );
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Noframe;
