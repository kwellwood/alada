
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Multisample_Target is

    FPS : constant := 60.0;

    type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

    type Example_Rec is
        record
            targets       : Bitmap_Array(0..3);
            bitmap_normal : A_Allegro_Bitmap;
            bitmap_filter : A_Allegro_Bitmap;
            sub           : A_Allegro_Bitmap;
            font          : A_Allegro_Font;
            bitmap_x      : Float_Array(0..7);
            bitmap_y      : Float_Array(0..7);
            bitmap_t      : Float := 0.0;
            step_t        : Integer := 0;
            step_x        : Integer := 0;
            step_y        : Integer := 0;
            step          : Bitmap_Array(0..3);
            update_step   : Boolean := False;
        end record;

    example : Example_Rec;

    ----------------------------------------------------------------------------

    function Create_Bitmap return A_Allegro_Bitmap is
        checkers_size : constant := 8;
        bitmap_size   : constant := 24;
        bitmap        : A_Allegro_Bitmap;
        locked        : A_Allegro_Locked_Region;
    begin
        bitmap := Al_Create_Bitmap( bitmap_size, bitmap_size );
        locked := Al_Lock_Bitmap( bitmap, ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_READWRITE );
        declare
            type Pixel_Data is array (Integer range 0..locked.pitch*bitmap_size) of Unsigned_8;
            rgba : Pixel_Data;
            for rgba'Address use locked.data;
            p    : constant Integer := locked.pitch;
            c    : Unsigned_8;
        begin
            for y in 0..bitmap_size-1 loop
                for x in 0..bitmap_size-1 loop
                    c := (Unsigned_8((x / checkers_size) + (y / checkers_size)) and 16#01#) * 255;
                    rgba(y * p + x * 4 + 0) := 0;
                    rgba(y * p + x * 4 + 1) := 0;
                    rgba(y * p + x * 4 + 2) := 0;
                    rgba(y * p + x * 4 + 3) := c;
                end loop;
            end loop;
        end;
        Al_Unlock_Bitmap( bitmap );
        return bitmap;
    end Create_Bitmap;

    ----------------------------------------------------------------------------

    procedure Init is
        memory : A_Allegro_Bitmap;
    begin
        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        memory := Create_Bitmap;
        Al_Set_New_Bitmap_Flags( 0 );

        Al_Set_New_Bitmap_Samples( 0 );
        example.targets(0) := Al_Create_Bitmap( 300, 200 );
        example.targets(1) := Al_Create_Bitmap( 300, 200 );
        Al_Set_New_Bitmap_Samples( 4 );
        example.targets(2) := Al_Create_Bitmap( 300, 200 );
        example.targets(3) := Al_Create_Bitmap( 300, 200 );
        Al_Set_New_Bitmap_Samples( 0 );

        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR );
        example.bitmap_filter := Al_Clone_Bitmap( memory );
        Al_Set_New_Bitmap_Flags( 0 );
        example.bitmap_normal := Al_Clone_Bitmap( memory );

        example.sub := Al_Create_Sub_Bitmap( memory, 0, 0, 30, 30 );
        example.step(0) := Al_Create_Bitmap( 30, 30 );
        example.step(1) := Al_Create_Bitmap( 30, 30 );
        example.step(2) := Al_Create_Bitmap( 30, 30 );
        example.step(3) := Al_Create_Bitmap( 30, 30 );
    end Init;

    ----------------------------------------------------------------------------

    procedure Bitmap_Move is
        a, s : Float;
    begin
        example.bitmap_t := example.bitmap_t + 1.0;
        for i in 0..7 loop
            a := 2.0 * ALLEGRO_PI * Float(i) / 16.0;
            s := Sin( (example.bitmap_t + Float(i * 40)) / 180.0 * ALLEGRO_PI );
            s := s * 90.0;
            example.bitmap_x(i) := 100.0 + s * Cos( a );
            example.bitmap_y(i) := 100.0 + s * Sin( a );
        end loop;
    end Bitmap_Move;

    ----------------------------------------------------------------------------

    procedure Draw( text : String; bitmap : A_Allegro_Bitmap ) is
        a : Float;
        s : Integer;
        c : Allegro_Color;
    begin
        Al_Clear_To_Color( Al_Color_Name( "white" ) );

        Al_Draw_Text( example.font, Al_Map_RGB( 0, 0, 0 ), 0.0, 0.0, 0, text );

        for i in 0..15 loop
            a := 2.0 * ALLEGRO_PI * Float(i) / 16.0;
            c := Al_Color_HSV( Float(i) * 360.0 / 16.0, 1.0, 1.0 );
            Al_Draw_Line( 100.0 + Cos( a ) * 10.0, 100.0 + Sin( a ) * 10.0,
                          100.0 + Cos( a ) * 90.0, 100.0 + Sin( a ) * 90.0, c, 3.0 );
        end loop;

        for i in 0..7 loop
            a := 2.0 * ALLEGRO_PI * Float(i) / 16.0;
            s := Al_Get_Bitmap_Width( bitmap );
            Al_Draw_Rotated_Bitmap( bitmap, Float(s) / 2.0, Float(s) / 2.0,
                                    example.bitmap_x(i), example.bitmap_y(i), a, 0 );
        end loop;
    end Draw;

    ----------------------------------------------------------------------------

    procedure Redraw is
        x, y : Integer;
    begin
        Al_Set_Target_Bitmap( example.targets(1) );
        Draw( "filtered", example.bitmap_filter );
        Al_Set_Target_Bitmap( example.targets(0) );
        Draw( "no filter", example.bitmap_normal );
        Al_Set_Target_Bitmap( example.targets(3) );
        Draw( "filtered, multi-sample x4", example.bitmap_filter );
        Al_Set_Target_Bitmap( example.targets(2) );
        Draw( "no filter, multi-sample x4", example.bitmap_normal );

        x := example.step_x;
        y := example.step_y;

        if example.update_step then
            for i in 0..3 loop
                Al_Set_Target_Bitmap( example.step(i) );
                Al_Reparent_Bitmap( example.sub, example.targets(i), x, y, 30, 30 );
                Al_Draw_Bitmap( example.sub, 0.0, 0.0, 0 );
            end loop;
            example.update_step := False;
        end if;

        Al_Set_Target_Backbuffer( Al_Get_Current_Display );
        Al_Clear_To_Color( Al_Map_RGB_f( 0.5, 0.0, 0.0 ) );
        Al_Draw_Bitmap( example.targets(0),  20.0,  20.0, 0 );
        Al_Draw_Bitmap( example.targets(1),  20.0, 240.0, 0 );
        Al_Draw_Bitmap( example.targets(2), 340.0,  20.0, 0 );
        Al_Draw_Bitmap( example.targets(3), 340.0, 240.0, 0 );

        Al_Draw_Scaled_Rotated_Bitmap( example.step(0), 15.0, 15.0, 320.0 - 50.0, 220.0 - 50.0, 4.0, 4.0, 0.0, 0 );
        Al_Draw_Scaled_Rotated_Bitmap( example.step(1), 15.0, 15.0, 320.0 - 50.0, 440.0 - 50.0, 4.0, 4.0, 0.0, 0 );
        Al_Draw_Scaled_Rotated_Bitmap( example.step(2), 15.0, 15.0, 640.0 - 50.0, 220.0 - 50.0, 4.0, 4.0, 0.0, 0 );
        Al_Draw_Scaled_Rotated_Bitmap( example.step(3), 15.0, 15.0, 640.0 - 50.0, 440.0 - 50.0, 4.0, 4.0, 0.0, 0 );
    end Redraw;

    ----------------------------------------------------------------------------

    procedure Update is
    begin
        Bitmap_Move;
        if example.step_t = 0 then
            example.step_x := Integer(example.bitmap_x(1)) - 15;
            example.step_y := Integer(example.bitmap_y(1)) - 15;
            example.step_t := 15;
            example.update_step := True;
        end if;
        example.step_t := example.step_t - 1;
    end Update;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        display     : A_Allegro_Display;
        w           : constant Integer := 20 + 300 + 20 + 300 + 20;
        h           : constant Integer := 20 + 200 + 20 + 200 + 20;
        done        : Boolean := False;
        need_redraw : Boolean := True;
        event       : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Failed to init IIO addon." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Error initializing font addon." );
        end if;
        example.font := Al_Create_Builtin_Font;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Error initializing primitives addon." );
        end if;

        Init_Platform_Specific;

        -- multisampling on non-backbuffer bitmaps requires OpenGL as of 5.2.3
        Al_Set_New_Display_Flags( ALLEGRO_OPENGL );

        display := Al_Create_Display( w, h );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / FPS );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        Al_Start_Timer( timer );

        while not done loop
            if need_redraw then
                Redraw;
                Al_Flip_Display;
                need_redraw := False;
            end if;

            loop
                Al_Wait_For_Event( queue, event );
                case event.any.typ is
                    when ALLEGRO_EVENT_KEY_CHAR =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            done := True;
                        end if;

                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        done := True;

                    when ALLEGRO_EVENT_TIMER =>
                        Update;
                        need_redraw := True;

                    when others =>
                        null;
                end case;
                if Al_Is_Event_Queue_Empty( queue ) then
                    exit;
                end if;
            end loop;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Multisample_Target;

