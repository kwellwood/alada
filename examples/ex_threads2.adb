
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Threads;                   use Allegro.Threads;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with GNAT.OS_Lib;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

--
--    Example program for the Allegro library, by Peter Wang.
--
--    In this example, threads render to their own memory buffers, while the
--    main thread handles events and drawing (copying from the memory buffers
--    to the display).
--
--    Click on an image to pause its thread.
--
package body Ex_Threads2 is

    procedure Ada_Main is

        -- feel free to bump these up
        NUM_THREADS    : constant := 9;
        IMAGES_PER_ROW : constant := 3;

        -- size of each fractal image
        W : constant := 120;
        H : constant := 120;

        type ThreadInfo is
            record
                bitmap      : A_Allegro_Bitmap;
                mutex       : A_Allegro_Mutex;
                cond        : A_Allegro_Cond;
                is_paused   : Boolean;
                gen         : Generator;
                target_x,
                target_y    : double;
            end record;

        type Viewport_Type is
            record
                centre_x,
                centre_y,
                x_extent,
                y_extent,
                zoom     : double;
            end record;

        type Palette_Array is array(0..255, 0..3) of Unsigned_8;

        type ThreadInfo_Array is array (Integer range <>) of ThreadInfo;

        thread_info : ThreadInfo_Array(0..NUM_THREADS-1);

        type U8_Array is array (Integer range <>) of Unsigned_8;

        sin_lut : U8_Array(0..255);

        ------------------------------------------------------------------------

        function cabs2( re, im : double ) return double is
        begin
            return re * re + im * im;
        end cabs2;

        ------------------------------------------------------------------------

        function Mandel( cre, cim : double; max_iter : Integer ) return Integer is
            Z_MAX2 : constant Float := 4.0;
            zre    : double := cre;
            zim    : double := cim;
            z1re,
            z1im   : double;
        begin
            for iter in 0..max_iter-1 loop
                z1re := zre * zre - zim * zim;
                z1im := 2.0 * zre * zim;
                z1re := z1re + cre;
                z1im := z1im + cim;
                if cabs2( z1re, z1im ) > double(Z_MAX2) then
                    return iter + 1;  -- outside set
                end if;
                zre := z1re;
                zim := z1im;
            end loop;

           return 0;     -- inside set
        end Mandel;

        ------------------------------------------------------------------------

        procedure Random_Palette( palette : in out Palette_Array; gen : Generator ) is
            rmax : constant Unsigned_8 := 128 + Unsigned_8(Float'(Random( gen )) * 128.0);
            gmax : constant Unsigned_8 := 128 + Unsigned_8(Float'(Random( gen )) * 128.0);
            bmax : constant Unsigned_8 := 128 + Unsigned_8(Float'(Random( gen )) * 128.0);
        begin
            for i in 0..255 loop
                palette(i, 0) := Unsigned_8(Integer(rmax) * i / 256);
                palette(i, 1) := Unsigned_8(Integer(gmax) * i / 256);
                palette(i, 2) := Unsigned_8(Integer(bmax) * i / 256);
            end loop;
       end Random_Palette;

        ------------------------------------------------------------------------

        procedure Draw_Mandel_Line( bitmap   : A_Allegro_Bitmap;
                                    viewport : Viewport_Type;
                                    palette  : Palette_Array;
                                    y        : Integer ) is
            lr     : A_Allegro_Locked_Region;
            xlower,
            ylower : double;
            xscale,
            yscale : double;
            im, re : double;
            w, h   : Integer;
            n      : constant Integer := 512 / (2 ** Natural(viewport.zoom));
            i      : Integer;
            v      : Unsigned_8;
        begin
            w := Al_Get_Bitmap_Width( bitmap );
            h := Al_Get_Bitmap_Height( bitmap );

            lr := Al_Lock_Bitmap_Region( bitmap, 0, y, w, 1,
                                         ALLEGRO_PIXEL_FORMAT_ANY_24_NO_ALPHA,
                                         ALLEGRO_LOCK_WRITEONLY );
            if lr = null then
                Abort_Example( "draw_mandel_line: al_lock_bitmap_region failed" );
            end if;

            xlower := viewport.centre_x - viewport.x_extent / 2.0 * viewport.zoom;
            ylower := viewport.centre_y - viewport.y_extent / 2.0 * viewport.zoom;
            xscale := viewport.x_extent / double(w) * viewport.zoom;
            yscale := viewport.y_extent / double(h) * viewport.zoom;

            re := xlower;
            im := ylower + double(y) * yscale;

            declare
                rgb : U8_Array(0..w*3-1);
                for rgb'Address use lr.data;
                pos : Integer := 0;
            begin
                for x in 0..w-1 loop
                    i := Mandel( re, im, n );
                    v := sin_lut(Integer(i * 64 / n));

                    rgb(pos+0) := palette(Integer(v), 0);
                    rgb(pos+1) := palette(Integer(v), 1);
                    rgb(pos+2) := palette(Integer(v), 2);
                    pos := pos + 3;

                    re := re + xscale;
                end loop;
            end;

            Al_Unlock_Bitmap( bitmap );
        end Draw_Mandel_Line;

        ------------------------------------------------------------------------

        function Thread_Func( thr : A_Allegro_Thread; arg : Address ) return Address;
        pragma Convention( C, Thread_Func );

        function Thread_Func( thr : A_Allegro_Thread; arg : Address ) return Address is
            info     : ThreadInfo;
            for info'Address use arg;
            pragma Import( Ada, info );
            viewport : Viewport_Type;
            palette  : Palette_Array;
            y        : Integer := 0;
            h        : constant Integer := Al_Get_Bitmap_Height( info.bitmap );
        begin
            viewport.centre_x := info.target_x;
            viewport.centre_y := info.target_y;
            viewport.x_extent := 3.0;
            viewport.y_extent := 3.0;
            viewport.zoom := 1.0;
            info.target_x := 0.0;
            info.target_y := 0.0;
            Reset( info.gen );

            while not Al_Get_Thread_Should_Stop( thr ) loop
                Al_Lock_Mutex( info.mutex );

                while info.is_paused loop
                    Al_Wait_Cond( info.cond, info.mutex );

                    -- We might be awoken because the program is terminating.
                    if Al_Get_Thread_Should_Stop( thr ) then
                        exit;
                    end if;
                end loop;

                if not info.is_paused then
                    if y = 0 then
                        Random_Palette( palette, info.gen );
                    end if;

                    Draw_Mandel_Line( info.bitmap, viewport, palette, y );

                    y := y + 1;
                    if y >= h then
                        y := 0;
                        viewport.centre_x := viewport.centre_x + viewport.zoom * viewport.x_extent * info.target_x;
                        viewport.centre_y := viewport.centre_y + viewport.zoom * viewport.y_extent * info.target_y;
                        info.target_x := 0.0;
                        info.target_y := 0.0;
                        viewport.zoom := viewport.zoom * 0.99;
                    end if;
                end if;

                Al_Unlock_Mutex( info.mutex );
                Al_Rest( 0.0 );
            end loop;

            return Null_Address;
        end Thread_Func;

        ------------------------------------------------------------------------

        procedure Show_Images is
            x, y : Integer := 0;
        begin
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            for i in 0..NUM_THREADS-1 loop
                -- for lots of threads, this is not good enough
                Al_Lock_Mutex( thread_info(i).mutex );
                Al_Draw_Bitmap( thread_info(i).bitmap, Float(x * W), Float(y * H), 0 );
                Al_Unlock_Mutex( thread_info(i).mutex );

                x := x + 1;
                if x = IMAGES_PER_ROW then
                    x := 0;
                    y := y + 1;
                end if;
            end loop;
            Al_Flip_Display;
        end Show_Images;

        ------------------------------------------------------------------------

        procedure Set_Target( n : Integer; x, y : double ) is
        begin
            if n in thread_info'Range then
                thread_info(n).target_x := x;
                thread_info(n).target_y := y;
            end if;
        end Set_Target;

        ------------------------------------------------------------------------

        procedure Toggle_Pausedness( n : Integer ) is
        begin
            Al_Lock_Mutex( thread_info(n).mutex );
            thread_info(n).is_paused := not thread_info(n).is_paused;
            Al_Broadcast_Cond( thread_info(n).cond );
            Al_Unlock_Mutex( thread_info(n).mutex );
        end Toggle_Pausedness;

        ------------------------------------------------------------------------

        type Thread_Array is array (Integer range <>) of A_Allegro_Thread;

        thread    : Thread_Array(0..NUM_THREADS-1);
        display   : A_Allegro_Display;
        timer     : A_Allegro_Timer;
        queue     : A_Allegro_Event_Queue;
        event     : Allegro_Event;
        need_draw : Boolean := False;
        n         : Integer;
    begin
        for i in sin_lut'Range loop
            sin_lut(i) := Unsigned_8(128 + Integer(127.0 * Sin( Float(i) / 8.0 )));
        end loop;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        display := Al_Create_Display( W * IMAGES_PER_ROW, H * NUM_THREADS / IMAGES_PER_ROW );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;
        timer := Al_Create_Timer( 1.0 / 3.0 );
        if timer = null then
            Abort_Example( "Error creating timer" );
        end if;
        queue := Al_Create_Event_Queue;
        if queue = null then
            Abort_Example( "Error creating event queue" );
        end if;
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        -- Note:
        -- Right now, A5 video displays can only be accessed from the thread which
        -- created them (at least for OpenGL). To lift this restriction, we could
        -- keep track of the current OpenGL context for each thread and make all
        -- functions accessing the display check for it.. not sure it's worth the
        -- additional complexity though.
        --
        Al_Set_New_Bitmap_Format( ALLEGRO_PIXEL_FORMAT_RGB_888 );
        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        for i in 0..NUM_THREADS-1 loop
            thread_info(i).bitmap := Al_Create_Bitmap( W, H );
            if thread_info(i).bitmap = null then
                GNAT.OS_Lib.OS_Exit( 1 );
            end if;
            thread_info(i).mutex := Al_Create_Mutex;
            if thread_info(i).mutex = null then
                GNAT.OS_Lib.OS_Exit( 1 );
            end if;
            thread_info(i).cond := Al_Create_Cond;
            if thread_info(i).cond = null then
                GNAT.OS_Lib.OS_Exit( 1 );
            end if;
            thread_info(i).is_paused := False;
            thread(i) := Al_Create_Thread( Thread_Func'Unrestricted_Access, thread_info(i)'Address );
            if thread(i) = null then
                GNAT.OS_Lib.OS_Exit( 1 );
            end if;
        end loop;
        Set_Target( 0, -0.56062033041600878303, -0.56064322926933807256 );
        Set_Target( 1, -0.57798076669230014080, -0.63449861991138123418 );
        Set_Target( 2,  0.36676836392830602929, -0.59081385302214906030 );
        Set_Target( 3, -1.48319283039401317303, -0.00000000200514696273 );
        Set_Target( 4, -0.74052910500707636032,  0.18340899525730713915 );
        Set_Target( 5,  0.25437906525768350097, -0.00046678223345789554 );
        Set_Target( 6, -0.56062033041600878303,  0.56064322926933807256 );
        Set_Target( 7, -0.57798076669230014080,  0.63449861991138123418 );
        Set_Target( 8,  0.36676836392830602929,  0.59081385302214906030 );

        for i in 0..NUM_THREADS-1 loop
            Al_Start_Thread( thread(i) );
        end loop;
        Al_Start_Timer( timer );

        need_draw := True;
        loop
            if need_draw and then Al_Is_Event_Queue_Empty( queue ) then
                Show_Images;
                need_draw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_TIMER then
                need_draw := True;
            elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                n := (event.mouse.y / H) * IMAGES_PER_ROW + (event.mouse.x / W);
                if n < NUM_THREADS then
                    declare
                        x, y : double;
                    begin
                        x := double(event.mouse.x) - (double(event.mouse.x) / double(W)) * double(W);
                        y := double(event.mouse.y) - (double(event.mouse.y) / double(H)) * double(H);
                        -- Center to the mouse click position.
                        if thread_info(n).is_paused then
                            thread_info(n).target_x := x / double(W) - 0.5;
                            thread_info(n).target_y := y / double(H) - 0.5;
                        end if;
                        Toggle_Pausedness( n );
                    end;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_EXPOSE then
                need_draw := True;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                end if;
                need_draw := True;
            end if;
        end loop;

        for i in 0..NUM_THREADS-1 loop
            -- Set the flag to stop the thread.  The thread might be waiting on a
            -- condition variable, so signal the condition to force it to wake up.
            --
            Al_Set_Thread_Should_Stop( thread(i) );
            Al_Lock_Mutex( thread_info(i).mutex );
            Al_Broadcast_Cond( thread_info(i).cond );
            Al_Unlock_Mutex( thread_info(i).mutex );

            -- al_destroy_thread() implicitly joins the thread, so this call is not
            -- strictly necessary.
            --
            Al_Join_Thread( thread(i) );
            Al_Destroy_Thread( thread(i) );
        end loop;

        Al_Destroy_Event_Queue( queue );
        Al_Destroy_Timer( timer );
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Threads2;
