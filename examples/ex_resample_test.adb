
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

-- Resamping test. Probably should integreate into test_driver somehow
package body Ex_Resample_Test is

    SAMPLES_PER_BUFFER : constant := 1024;

    N : constant := 2;

    type Int_Array is array (Integer range <>) of Integer;
    frequency : Int_Array(0..N-1);

    type Double_Array is array (Integer range <>) of double;
    samplepos : Double_Array(0..N-1);

    type Audio_Stream_Array is array (Integer range <>) of A_Allegro_Audio_Stream;
    stream : Audio_Stream_Array(0..N-1);

    display : A_Allegro_Display;

    type Float_Array is array (Integer range <>) of Float;
    waveform        : Float_Array(0..639);
    waveform_buffer : Float_Array(0..639);

    ----------------------------------------------------------------------------

    procedure Mainloop is
        queue  : A_Allegro_Event_Queue;
        event  : aliased Allegro_Event;
        timer  : A_Allegro_Timer;
        buf    : Address;
        pitch  : constant double := 440.0;
        m      : Integer := 0;
        redraw : Boolean := False;
        c      : Allegro_Color;
    begin
        for i in 0..N-1 loop
            frequency(i) := 22050 * (2 ** Natural(double(i) / double(N)));
            stream(i) := Al_Create_Audio_Stream( 4, SAMPLES_PER_BUFFER,
                                                 unsigned(frequency(i)),
                                                 ALLEGRO_AUDIO_DEPTH_FLOAT32,
                                                 ALLEGRO_CHANNEL_CONF_1 );
            if stream(i) = null then
                Abort_Example( "Could not create stream." );
                return;
            end if;

            if not Al_Attach_Audio_Stream_To_Mixer( stream(i), Al_Get_Default_Mixer ) then
                Abort_Example( "Could not attach stream to mixer." );
            end if;
        end loop;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        for i in 0..N-1 loop
            Al_Register_Event_Source( queue, Al_Get_Audio_Stream_Event_Source( stream(i) ) );
        end loop;
        if textlog /= null then
            Al_Register_Event_Source( queue, Al_Get_Native_Text_Log_Event_Source( textlog ) );
        end if;

        Log_Print( "Generating" & N'Img & " sine waves of different sampling quality" );
        Log_Print( "If Allegro's resampling is correct there should be little variation" );

        timer := Al_Create_Timer( 1.0 / 60.0 );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        Al_Start_Timer( timer );
        while m < 60 * frequency(0) / SAMPLES_PER_BUFFER * N loop
            Al_Wait_For_Event( queue, event );

            if event.any.typ = ALLEGRO_EVENT_AUDIO_STREAM_FRAGMENT then
                for si in 0..N-1 loop
                    buf := Al_Get_Audio_Stream_Fragment( stream(si) );
                    if buf /= Null_Address then

                        declare
                            fbuf : Float_Array(0..SAMPLES_PER_BUFFER-1);
                            for fbuf'Address use buf;
                        begin
                            for i in 0..SAMPLES_PER_BUFFER-1 loop
                                fbuf(i) := sin( Float(samplepos(si) / double(frequency(si)) * pitch * ALLEGRO_PI * 2.0) ) / Float(N);
                                samplepos(si) := samplepos(si) + 1.0;
                            end loop;
                        end;

                        if not Al_Set_Audio_Stream_Fragment( stream(si), buf ) then
                            Log_Print( "Error setting stream fragment." );
                        end if;

                        m := m + 1;
                        Log_Put( Trim( si'Img, Left ) );
                        if m mod 60 = 0 then
                            Log_Print( "" );
                        end if;
                    end if;
                end loop;
            end if;

            if event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := True;
            end if;

            if event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
               event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
            then
                exit;
            end if;

            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            end if;

            if event.any.typ = ALLEGRO_EVENT_NATIVE_DIALOG_CLOSE then
                exit;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                c := Al_Map_RGB( 0, 0, 0 );
                Al_Clear_To_Color( Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );

                for i in 0..639 loop
                    Al_Draw_Pixel( Float(i), 50.0 + waveform(i) * 50.0, c );
                end loop;

                Al_Flip_Display;
                redraw := False;
            end if;
        end loop;

        for si in 0..N-1 loop
             Al_Drain_Audio_Stream( stream(si) );
        end loop;

        Log_Print( "" );

        Al_Destroy_Event_Queue( queue );
    end Mainloop;

    ----------------------------------------------------------------------------

    pos : Integer := waveform_buffer'First;

    procedure Update_Waveform( buf : Address; samples : unsigned; data : Address );
    pragma Convention( C, Update_Waveform );

    procedure Update_Waveform( buf : Address; samples : unsigned; data : Address ) is
        fbuf : Float_Array(0..(640 - 1) * 2);
        for fbuf'Address use buf;
        n : unsigned := samples;
        pragma Unreferenced( data );
    begin
        -- Yes, we could do something more advanced, but an oscilloscope of the
        -- first 640 samples of each buffer is enough for our purpose here.
        --
        if n > 640 then
            n := 640;
        end if;

        for i in 0..n - 1 loop
            waveform_buffer(pos) := fbuf(Integer(i) * 2);
            pos := pos + 1;
            if pos = 640 then
                waveform(0..639) := waveform_buffer(0..639);
                pos := 0;
                exit;
            end if;
        end loop;
    end Update_Waveform;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        Open_Log;

        display := Al_Create_Display( 640, 100 );
            if display = null then
            Abort_Example( "Could not create display." );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound." );
        end if;
        if not Al_Reserve_Samples( N ) then
            Abort_Example( "Could not reserve" & N'Img & " samples." );
        end if;

        if not Al_Set_Mixer_Postprocess_Callback( Al_Get_Default_Mixer,
                                                  Update_Waveform'Unrestricted_Access,
                                                  Null_Address )
        then
            Abort_Example( "Could not set postprocess callback" );
        end if;

        Mainloop;

        Close_Log( False );

        for i in 0..N-1 loop
            Al_Destroy_Audio_Stream( stream(i) );
        end loop;
        Al_Uninstall_Audio;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Resample_Test;
