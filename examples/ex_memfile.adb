
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Unchecked_Deallocation;
with Allegro;                           use Allegro;
with Allegro.File_IO;                   use Allegro.File_IO;
with Allegro.Memfile;                   use Allegro.Memfile;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with GNAT.OS_Lib;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C_Streams;              use Interfaces.C_Streams;

--
--      Example program for the Allegro library.
--
--      Test memfile addon.
--
package body Ex_Memfile is

    procedure Ada_Main is

        type Byte_Buffer is array (Integer range <>) of Unsigned_8;
        type A_Byte_Buffer is access all Byte_Buffer;
        procedure Free is new Ada.Unchecked_Deallocation( Byte_Buffer, A_Byte_Buffer );

        data_size : constant := 1024;
        data      : A_Byte_Buffer;
        memfile   : A_Allegro_File;
        i         : Integer := 0;
        str       : Unbounded_String;

        ------------------------------------------------------------------------

        procedure Exit_With_Error is
        begin
            Al_Fclose( memfile );
            Free( data );
            Close_Log( True );
            GNAT.OS_Lib.OS_Exit( 1 );
        end Exit_With_Error;

    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        data := new Byte_Buffer(0..data_size - 1);

        Log_Print( "Creating memfile" );
        memfile := Al_Open_Memfile( data.all'Address, data'Length, "rw" );
        if memfile = null then
            Log_Print( "Error opening Memfile: " );
            Exit_With_Error;
        end if;

        Log_Print( "Writing data to memfile" );
        for i in 0..data_size / 4 - 1 loop
            if Al_Fwrite32le( memfile, Integer_32(i) ) < 4 then
                Log_Print( "Failed to write" & i'Img & " to memfile" );
                Exit_With_Error;
            end if;
        end loop;

        if not Al_Fseek( memfile, 0, ALLEGRO_SEEK_SET ) then
            Log_Print( "Failed to seek to 0" );
            Exit_With_Error;
        end if;

        Log_Print( "Reading and testing data from memfile" );
        for i in 0..data_size / 4 - 1 loop
            declare
                ret : constant Integer_32 := Al_Fread32le( memfile );
            begin
                if ret /= Integer_32(i) or else Al_Feof( memfile ) then
                    Log_Print( "Item" & i'Img & " failed to verify, got" & ret'Img );
                    Exit_With_Error;
                end if;
            end;
        end loop;

        if Al_Feof( memfile ) then
            Log_Print( "EOF indicator prematurely set!" );
            Exit_With_Error;
        end if;

        -- testing the ungetc buffer
        if not Al_Fseek( memfile, 0, ALLEGRO_SEEK_SET ) then
            Log_Print( "Failed to seek to 0" );
        end if;

        while Al_Fungetc( memfile, i ) /= EOF loop
            i := i + 1;
        end loop;
        Log_Print( "Length of ungetc buffer:" & i'Img );

        if Al_Ftell( memfile ) /= Integer_64(-i) then
            Log_Print( "Current position is not correct. Expected " & Integer'Image( -i ) );
            Log_Print( ", but got" & Al_Ftell( memfile )'Img );
            Exit_With_Error;
        end if;

        while i > 0 loop
            i := i - 1;
            if i /= Al_Fgetc( memfile ) then
                Log_Print( "Failed to verify ungetc data." );
                Exit_With_Error;
            end if;
        end loop;

        if Al_Ftell( memfile ) /= 0 then
            Log_Print( "Current position is not correct after reading back the ungetc buffer" );
            Log_Print( "Expected 0, but got" & Al_Ftell( memfile )'Img );
            Exit_With_Error;
        end if;

        Al_Fputs( memfile, "legro rocks!" );
        Al_Fseek( memfile, 0, ALLEGRO_SEEK_SET );
        Al_Fungetc( memfile, 'l' );
        Al_Fungetc( memfile, 'A' );
        str := To_Unbounded_String( Al_Fgets( memfile, 15 ) );
        if str /= "Allegro rocks!" then
            Log_Print( "Expected to see 'Allegro rocks!' but got '" & To_String( str ) & "' instead" );
            Log_Print( "(Maybe the ungetc buffer isn't big enough.)" );
            Exit_With_Error;
        end if;

        Log_Print( "Done." );

        Al_Fclose( memfile );
        Free( data );
        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Memfile;
