
with Allegro;                           use Allegro;
with Allegro.System;                    use Allegro.System;
with Allegro.Paths;                     use Allegro.Paths;
with Common;                            use Common;

package body Ex_Get_Path is

    procedure Ada_Main is

        procedure Show_Path( id : System_Path; label : String ) is
            path : A_Allegro_Path;
        begin
            path := Al_Get_Standard_Path( id );
            if path /= null then
                Log_Print( label & ": " & Al_Path_Cstr( path, ALLEGRO_NATIVE_PATH_SEP ) );
            else
                Log_Print( label & ": <none>" );
            end if;
            Al_Destroy_Path( path );
        end Show_Path;

    begin
        -- defaults to blank
        Al_Set_Org_Name( "liballeg.org" );

        -- defaults to the exename, set it here to remove the .exe on windows
        Al_Set_App_Name( "ex_get_path" );

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        Open_Log;

        for pass in 1..3 loop
            if pass = 1 then
                Log_Print( "With default exe name:" );
            elsif pass = 2 then
                Log_Print( "Overriding exe name to blahblah" );
                Al_Set_Exe_Name( "blahblah" );
            elsif pass = 3 then
                Log_Print("Overriding exe name to /tmp/blahblah.exe:");
                Al_Set_Exe_Name( "/tmp/blahblah.exe" );
            end if;

            Show_Path( ALLEGRO_RESOURCES_PATH,      "RESOURCES_PATH" );
            Show_Path( ALLEGRO_TEMP_PATH,           "TEMP_PATH" );
            Show_Path( ALLEGRO_USER_DATA_PATH,      "USER_DATA_PATH" );
            Show_Path( ALLEGRO_USER_SETTINGS_PATH,  "USER_SETTINGS_PATH" );
            Show_Path( ALLEGRO_USER_HOME_PATH,      "USER_HOME_PATH" );
            Show_Path( ALLEGRO_USER_DOCUMENTS_PATH, "USER_DOCUMENTS_PATH" );
            Show_Path( ALLEGRO_EXENAME_PATH,        "EXENAME_PATH" );
        end loop;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Get_Path;
