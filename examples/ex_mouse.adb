
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Mouse.Cursors;             use Allegro.Mouse.Cursors;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;

package body Ex_Mouse is

    procedure Ada_Main is

        NUM_BUTTONS : constant := 3;

        ------------------------------------------------------------------------

        procedure Draw_Mouse_Button( but : Integer; down : Boolean ) is
            type Int_Array is array (Positive range <>) of Integer;
            offset : constant Int_Array := (0, 70, 35);
            grey,
            black  : Allegro_Color;
            x      : constant Float := 400.0 + Float(offset(but));
            y      : constant Float := 130.0;
        begin
            grey := Al_Map_RGB( 16#e0#, 16#e0#, 16#e0# );
            black := Al_Map_RGB( 0, 0, 0 );

            Al_Draw_Filled_Rectangle( x, y, x + 27.0, y + 42.0, grey );
            Al_Draw_Rectangle( x + 0.5, y + 0.5, x + 26.5, y + 41.5, black, 0.0 );
            if down then
                Al_Draw_Filled_Rectangle( x + 2.0, y + 2.0, x + 25.0, y + 40.0, black );
            end if;
        end Draw_Mouse_Button;

        ------------------------------------------------------------------------

        display  : A_Allegro_Display;
        cursor   : A_Allegro_Bitmap;
        msestate : Allegro_Mouse_State;
        kbdstate : Allegro_Keyboard_State;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not initialize primitives addon" );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon" );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Al_Hide_Mouse_Cursor( display );

        cursor := Al_Load_Bitmap( "data/cursor.tga" );
        if cursor = null then
            Abort_Example( "Error loading cursor.tga" );
        end if;

        loop
            Al_Get_Mouse_State( msestate );
            Al_Get_Keyboard_State( kbdstate );

            Al_Clear_To_Color( Al_Map_RGB( 16#ff#, 16#ff#, 16#c0# ) );
            for i in 1..NUM_BUTTONS loop
                Draw_Mouse_Button( i, Al_Mouse_Button_Down( msestate, i ) );
            end loop;
            Al_Draw_Bitmap( cursor, Float(msestate.x), Float(msestate.y), 0 );
            Al_Flip_Display;

            Al_Rest( 0.005 );
            exit when Al_Key_Down( kbdstate, ALLEGRO_KEY_ESCAPE );
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Mouse;
