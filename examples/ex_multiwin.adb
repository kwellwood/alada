
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Multiwin is

    procedure Ada_Main is

        W : constant := 640;
        H : constant := 400;

        type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;
        type Display_Array is array (Integer range <>) of A_Allegro_Display;

        display  : Display_Array(0..1);
        event    : aliased Allegro_Event;
        events   : A_Allegro_Event_Queue;
        pictures : Bitmap_Array(0..1);
        target   : A_Allegro_Bitmap;
        width,
        height   : Integer;
        done     : Boolean := False;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon" );
        end if;

        events := Al_Create_Event_Queue;

        Al_Set_New_Display_Flags( ALLEGRO_WINDOWED or ALLEGRO_RESIZABLE );

        -- Create two windows.
        display(0) := Al_Create_Display( W, H );
        if display(0) = null then
            Abort_Example( "Error creating display" );
        end if;
        pictures(0) := Al_Load_Bitmap( "data/mysha.pcx" );
        if pictures(0) = null then
            Abort_Example( "failed to load mysha.pcx" );
        end if;

        display(1) := Al_Create_Display( W, H );
        if display(1) = null then
            Abort_Example( "Error creating display" );
        end if;
        pictures(1) := Al_Load_Bitmap( "data/allegro.pcx" );
        if pictures(1) = null then
            Abort_Example( "failed to load allegro.pcx" );
        end if;

        -- This is only needed since we want to receive resize events.
        Al_Register_Event_Source( events, Al_Get_Display_Event_Source( display(0) ) );
        Al_Register_Event_Source( events, Al_Get_Display_Event_Source( display(1) ) );
        Al_Register_Event_Source( events, Al_Get_Keyboard_Event_Source );

        loop
            -- read input
            while not done and then Al_Is_Event_Queue_Empty( events ) = False loop
                if not Al_Get_Next_Event( events, event'Access ) then
                    Abort_Example( "Error receiving event" );
                end if;
                if event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    end if;
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_RESIZE then
                    Al_Acknowledge_Resize( event.display.source );
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_SWITCH_IN then
                    for i in display'Range loop
                        if display(i) = event.display.source then
                            Log_Print( "Display" & i'Img & " switching in" );
                            exit;
                        end if;
                    end loop;
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_SWITCH_OUT then
                    for i in display'Range loop
                        if display(i) = event.display.source then
                            Log_Print( "Display" & i'Img & " switching out" );
                            exit;
                        end if;
                    end loop;
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                    for i in display'Range loop
                        if display(i) = event.display.source then
                            display(i) := null;
                        end if;
                    end loop;
                    Al_Destroy_Display( event.display.source );
                    done := True;
                    for i in display'Range loop
                        if display(i) /= null then
                            done := False;
                        end if;
                    end loop;
                end if;
            end loop;

            exit when done;

            for i in display'Range loop
                if display(i) /= null then
                    target := Al_Get_Backbuffer( display(i) );
                    width := Al_Get_Bitmap_Width( target );
                    height := Al_Get_Bitmap_Height( target );

                    Al_Set_Target_Bitmap( target );
                    Al_Draw_Scaled_Bitmap( pictures(0), 0.0, 0.0,
                                           Float(Al_Get_Bitmap_Width( pictures(0) )),
                                           Float(Al_Get_Bitmap_Height( pictures(0) )),
                                           0.0, 0.0,
                                           Float(width / 2), Float(height),
                                           0 );
                    Al_Draw_Scaled_Bitmap( pictures(1), 0.0, 0.0,
                                           Float(Al_Get_Bitmap_Width( pictures(1) )),
                                           Float(Al_Get_Bitmap_Height( pictures(1) )),
                                           Float(width / 2), 0.0,
                                           Float(width / 2), Float(height),
                                           0 );
                    Al_Flip_Display;
                end if;
            end loop;

            Al_Rest( 0.001 );
        end loop;

        Al_Destroy_Bitmap( pictures(0) );
        Al_Destroy_Bitmap( pictures(1) );
        Al_Destroy_Display( display(0) );
        Al_Destroy_Display( display(1) );

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Multiwin;
