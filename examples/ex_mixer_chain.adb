
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with GNAT.OS_Lib;

--
--    Example program for the Allegro library.
--
--    Test chaining mixers to mixers.
--
package body Ex_Mixer_Chain is

    procedure Ada_Main is

        type Mixer_Array is array(Integer range <>) of A_Allegro_Mixer;
        type Sample_Instance_Array is array (Integer range <>) of A_Allegro_Sample_Instance;
        type Sample_Array is array (Integer range <>) of A_Allegro_Sample;

        voice           : A_ALLEGRO_VOICE;
        mixer           : A_ALLEGRO_MIXER;
        submixer        : Mixer_Array(0..1);
        sample          : Sample_Instance_Array(0..1);
        sample_data     : Sample_Array(0..1);
        sample_time     : Float;
        max_sample_time : Float;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if Argument_Count /= 2 then
            Log_Print( "This example needs to be run from the command line." );
            Log_Print( "Usage: ex_mixer_chain.exe file1 file2" );
            Close_Log( True );
            GNAT.OS_Lib.OS_Exit( 0 );
        end if;

        if not Al_Init_Acodec_Addon then
            Abort_Example( "Could not init codecs!" );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound!" );
        end if;

        voice := Al_Create_Voice( 44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2 );
        if voice = null then
            Abort_Example( "Could not create ALLEGRO_VOICE." );
        end if;

        mixer := Al_Create_Mixer( 44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2 );
        submixer(0) := Al_Create_Mixer( 44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2 );
        submixer(1) := Al_Create_Mixer( 44100, ALLEGRO_AUDIO_DEPTH_FLOAT32, ALLEGRO_CHANNEL_CONF_2 );
        if mixer = null or else submixer(0) = null or else submixer(1) = null then
            Abort_Example( "al_create_mixer failed." );
        end if;

        if not Al_Attach_Mixer_To_Voice( mixer, voice ) then
            Abort_Example( "al_attach_mixer_to_voice failed." );
        end if;

        for i in sample_data'Range loop
            declare
                filename : constant String := Argument( i + 1 );
            begin
                sample_data(i) := Al_Load_Sample( filename );
                if sample_data(i) = null then
                    Abort_Example( "Could not load sample from '" & filename & "'!" );
                end if;
                sample(i) := Al_Create_Sample_Instance( null );
                if sample(i) = null then
                    Abort_Example( "al_create_sample failed." );
                end if;
                if not Al_Set_Sample( sample(i), sample_data(i) ) then
                    Abort_Example( "al_set_sample_ptr failed." );
                end if;
                if not Al_Attach_Sample_Instance_To_Mixer( sample(i), submixer(i) ) then
                    Abort_Example( "al_attach_sample_instance_to_mixer failed." );
                end if;
                if not Al_Attach_Mixer_To_Mixer( submixer(i), mixer ) then
                    Abort_Example( "al_attach_mixer_to_mixer failed." );
                end if;
            end;
        end loop;

        -- Play sample in looping mode.
        for i in sample'Range loop
            if not Al_Set_Sample_Instance_Playmode( sample(i), ALLEGRO_PLAYMODE_LOOP ) then
                null;
            end if;
            if not Al_Play_Sample_Instance( sample(i) ) then
                null;
            end if;
        end loop;

        max_sample_time := Al_Get_Sample_Instance_Time( sample(0) );
        sample_time := Al_Get_Sample_Instance_Time( sample(1) );
        if sample_time > max_sample_time then
            max_sample_time := sample_time;
        end if;

        Log_Put( "Playing..." );

        Al_Rest( Long_Float(max_sample_time) );

        if not Al_Set_Sample_Instance_Gain( sample(0), 0.5 ) then
            null;
        end if;
        Al_Rest( Long_Float(max_sample_time) );

        if not Al_Set_Sample_Instance_Gain( sample(1), 0.25 ) then
            null;
        end if;
        Al_Rest( Long_Float(max_sample_time) );

        if not Al_Stop_Sample_Instance( sample(0) ) then
            null;
        end if;
        if not Al_Stop_Sample_Instance( sample(1) ) then
            null;
        end if;
        Log_Print( "Done" );

        -- Free the memory allocated.
        for i in sample'Range loop
            if not Al_Set_Sample( sample(i), null ) then
                null;
            end if;
            Al_Destroy_Sample( sample_data(i) );
            Al_Destroy_Sample_Instance( sample(i) );
            Al_Destroy_Mixer( submixer(i) );
        end loop;
        Al_Destroy_Mixer( mixer );
        Al_Destroy_Voice( voice );

        Al_Uninstall_Audio;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Mixer_Chain;
