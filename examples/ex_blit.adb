
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Streams;                       use Ada.Streams;
with Ada.Unchecked_Deallocation;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.State;                     use Allegro.State;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;
with System.Storage_Elements;           use System.Storage_Elements;

package body Ex_Blit is

    procedure Ada_Main is
        type Long_Float_Array is array (Integer range <>) of Long_Float;

        type Example is
            record
                pattern    : A_Allegro_Bitmap;
                font       : A_Allegro_Font;
                queue      : A_Allegro_Event_Queue;
                background,
                text,
                white      : Allegro_Color;
                timer      : Long_Float_Array(0..3) := (others => 0.0);
                counter    : Long_Float_Array(0..3) := (others => 0.0);
                targetFPS  : Integer := 0;
                text_x,
                text_y     : Float := 0.0;
            end record;

        ex : Example;

        ------------------------------------------------------------------------

        procedure memcpy( S1 : System.Address; S2 : System.Address; N : size_t );
        pragma Import (C, memcpy, "memcpy");

        type A_Stream is access all Stream_Element_Array;

        procedure Free is new Ada.Unchecked_Deallocation( Stream_Element_Array, A_Stream );

        ------------------------------------------------------------------------

        function Example_Bitmap( w, h : Integer ) return A_Allegro_Bitmap is
            mx      : constant Float := Float(w) * 0.5;
            my      : constant Float := Float(h) * 0.5;
            state   : aliased Allegro_State;
            lock    : A_Allegro_Locked_Region;
            pragma Warnings( Off, lock );
            pattern : A_Allegro_Bitmap;
            a, d,
            sat,
            hue     : Float;
        begin
            pattern := Al_Create_Bitmap( w, h );
            Al_Store_State( state, ALLEGRO_STATE_TARGET_BITMAP );
            Al_Set_Target_Bitmap( pattern );
            lock := Al_Lock_Bitmap( pattern, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_WRITEONLY );

            for i in 0..w-1 loop
                for j in 0..h-1 loop
                    if Float(i) - mx /= 0.0 or else Float(j) - my /= 0.0 then
                        a := Arctan( Y => Float(i) - mx, X => Float(j) - my );
                    end if;
                    d := Sqrt( ((Float(i) - mx) ** 2) + ((Float(j) - my) ** 2) );
                    sat := ((1.0 - (1.0 / (1.0 + d * 0.1))) ** 5);
                    hue := 3.0 * a * 180.0 / ALLEGRO_PI;
                    hue := (hue / 360.0 - Float'Floor( hue / 360.0 )) * 360.0;
                    Al_Put_Pixel( i, j, Al_Color_HSV( hue, sat, 1.0 ) );
                end loop;
            end loop;

            Al_Put_Pixel( 0, 0, Al_Map_RGB( 0, 0, 0 ) );
            Al_Unlock_Bitmap( pattern );
            Al_Restore_State( state );
            return pattern;
        end Example_Bitmap;

        ------------------------------------------------------------------------

        procedure Set_XY( x, y : Float ) is
        begin
            ex.text_x := x;
            ex.text_y := y;
        end Set_XY;

        ------------------------------------------------------------------------

        procedure Get_XY( x, y : out Float ) is
        begin
            x := ex.text_x;
            y := ex.text_y;
        end Get_XY;

        ------------------------------------------------------------------------

        procedure Print( message : String ) is
        begin
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Text( ex.font, ex.text, ex.text_x, ex.text_y, 0, message );
            ex.text_y := ex.text_y + Float(Al_Get_Font_Line_Height( ex.font ));
        end Print;

        ------------------------------------------------------------------------

        procedure Start_Timer( i : Integer ) is
        begin
            ex.timer(i) := ex.timer(i) - Al_Get_Time;
            ex.counter(i) := ex.counter(i) + 1.0;
        end Start_Timer;

        ------------------------------------------------------------------------

        procedure Stop_Timer( i : Integer ) is
        begin
            ex.timer(i) := ex.timer(i) + Al_Get_Time;
        end Stop_Timer;

        ------------------------------------------------------------------------

        function Get_FPS( i : Integer ) return Integer is
        begin
            if ex.timer(i) = 0.0 then
                return 0;
            end if;
            return Integer(Long_Float'Min( Long_Float(Integer'Last - 1), ex.counter(i) / ex.timer(i) ));
        end Get_FPS;

        ------------------------------------------------------------------------

        procedure Draw is
            iw     : constant Integer := Al_Get_Bitmap_Width( ex.pattern );
            ih     : constant Integer := Al_Get_Bitmap_Height( ex.pattern );
            x, y   : Float;
            temp   : A_Allegro_Bitmap;
            screen : A_Allegro_Bitmap;
            lock   : A_Allegro_Locked_Region;
            size   : Integer;
            format : Allegro_Pixel_Format;
            data   : A_Stream;
        begin
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );

            Al_Clear_To_Color( ex.background );

            screen := Al_Get_Target_Bitmap;

            Set_XY( 8.0, 8.0 );

            -- Test 1.
            -- Disabled: drawing to same bitmap is not supported.
            --
            --Print( "Screen -> Screen (" & Integer'Image( Integer(Get_FPS( 0 )) ) & " fps ) " );
            --Get_XY( x, y );
            --Al_Draw_Bitmap( ex.pattern, x, y, 0 );
            --
            --Start_Timer( 0 );
            --Al_Draw_Bitmap_Region( screen, x, y, iw, ih, x + 8 + iw, y, 0 );
            --Stop_Timer( 0 );
            --Set_xy( x, y + ih );

            -- Test 2.
            Print( "Screen -> Bitmap -> Screen (" & Integer'Image( Get_FPS( 1 ) ) & " fps ) " );
            Get_XY( x, y );
            Al_Draw_Bitmap( ex.pattern, x, y, 0 );

            temp := Al_Create_Bitmap( iw, ih );
            Al_Set_Target_Bitmap( temp );
            Al_Clear_To_Color( Al_Map_RGBA_f( 1.0, 0.0, 0.0, 1.0 ) );
            Start_Timer( 1 );
            Al_Draw_Bitmap_Region( screen, x, y, Float(iw), Float(ih), 0.0, 0.0, 0 );

            Al_Set_Target_Bitmap( screen );
            Al_Draw_Bitmap( temp, x + Float(8 + iw), y, 0 );
            Stop_Timer( 1 );
            Set_xy( x, y + Float(ih) );

            Al_Destroy_Bitmap( temp );

            -- Test 3.
            Print( "Screen -> Memory -> Screen (" & Integer'Image( Get_FPS( 2 ) ) & " fps ) " );
            Get_XY( x, y );
            Al_Draw_Bitmap( ex.pattern, x, y, 0 );

            Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
            temp := Al_Create_Bitmap( iw, ih );
            Al_Set_Target_Bitmap( temp );
            Al_Clear_To_Color( Al_Map_RGBA_f( 1.0, 0.0, 0.0, 1.0 ) );
            Start_Timer( 2 );
            Al_Draw_Bitmap_Region( screen, x, y, Float(iw), Float(ih), 0.0, 0.0, 0 );

            Al_Set_Target_Bitmap( screen );
            Al_Draw_Bitmap( temp, x + Float(8 + iw), y, 0 );
            Stop_Timer( 2 );
            Set_xy( x, y + Float(ih) );

            Al_Destroy_Bitmap( temp );
            Al_Set_New_Bitmap_Flags( ALLEGRO_VIDEO_BITMAP );

            -- Test 4.
            Print( "Screen -> Locked -> Screen (" & Integer'Image( Get_FPS( 3 ) ) & " fps ) " );
            Get_XY( x, y );
            Al_Draw_Bitmap( ex.pattern, x, y, 0 );

            Start_Timer( 3 );
            lock := Al_Lock_Bitmap_Region( screen, Integer(x), Integer(y), iw, ih, ALLEGRO_PIXEL_FORMAT_ANY, ALLEGRO_LOCK_READONLY );
            format := lock.format;
            size := lock.pixel_size;

            data := new Stream_Element_Array(Stream_Element_Offset(0)..Stream_Element_Offset(size * iw * ih - 1));
            for i in 0..ih-1 loop
                --for (i = 0; i < ih; i++)
                --    memcpy((char*)data + i * size * iw, (char*)lock->data + i * lock->pitch, size * iw);
                memcpy( data(data'First + Stream_Element_Offset(i * size * iw))'Address,
                        lock.data + Storage_Offset(i * lock.pitch),
                        size_t(size * iw) );
            end loop;
            Al_Unlock_Bitmap( screen );

            lock := Al_Lock_Bitmap_Region( screen, Integer(x) + 8 + iw, Integer(y), iw, ih, format, ALLEGRO_LOCK_WRITEONLY );
            for i in 0..ih-1 loop
                --for (i = 0; i < ih; i++)
                --    memcpy((char*)lock->data + i * lock->pitch, (char*)data + i * size * iw, size * iw);
                memcpy( lock.data + Storage_Offset(i * lock.pitch),
                        data(data'First + Stream_Element_Offset(i * size * iw))'Address,
                        size_t(size * iw) );
            end loop;
            Al_Unlock_Bitmap( screen );
            Free( data );

            Stop_Timer( 3 );
            Set_xy( x, y + Float(ih) );
        end Draw;

        ------------------------------------------------------------------------

        procedure Tick is
        begin
            Draw;
            Al_Flip_Display;
        end Tick;

        ------------------------------------------------------------------------

        procedure Run is
            event     : aliased Allegro_Event;
            need_draw : Boolean := True;
        begin
            loop
                if need_draw and then Al_Is_Event_Queue_Empty( ex.queue ) then
                    Tick;
                    need_draw := False;
                end if;

                Al_Wait_For_Event( ex.queue, event );

                case event.any.typ is
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        return;

                    when ALLEGRO_EVENT_KEY_DOWN =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            return;
                        end if;

                    when ALLEGRO_EVENT_TIMER =>
                        need_draw := True;

                    when others =>
                        null;
                end case;
            end loop;
        end Run;

        ------------------------------------------------------------------------

        procedure Init is
        begin
            ex.targetFPS := 60;
            ex.font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
            if ex.font = null then
                Abort_Example( "data/fixed_font.tga not found." );
            end if;
            ex.background := Al_Color_Name( "beige" );
            ex.text := Al_Color_Name( "black" );
            ex.white := Al_Color_Name( "white" );
            ex.pattern := Example_Bitmap( 100, 100 );
        end Init;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not install Allegro IIO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / Long_Float(ex.targetFPS) );

        ex.queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( ex.queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Display_Event_Source( display));
        Al_Register_Event_Source( ex.queue, Al_Get_Timer_Event_Source( timer));

        Al_Start_Timer( timer );
        Run;

        Al_Destroy_Timer( timer );
        Al_Destroy_Event_Queue( ex.queue );
        Al_Destroy_Bitmap( ex.pattern );
        Al_Destroy_Font( ex.font );
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Blit;
