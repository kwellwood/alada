
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Shaders;                   use Allegro.Shaders;
with Allegro.System;                    use Allegro.System;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;

--
-- Example program for the Allegro library.
--
-- Test that shaders are applied per target bitmap.
--
package body Ex_Shader_Target is

    tints : constant Float_Array(0..11) :=
    (
        4.0, 0.0, 1.0,
        0.0, 4.0, 1.0,
        1.0, 0.0, 4.0,
        4.0, 4.0, 1.0
    );

    type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

    ----------------------------------------------------------------------------

    procedure Choose_Shader_Source( platform : Allegro_Shader_Platform;
                                    vsource  : in out Unbounded_String;
                                    psource  : in out Unbounded_String ) is
    begin
        if platform = ALLEGRO_SHADER_HLSL then
            vsource := To_Unbounded_String( "data/ex_shader_vertex.hlsl" );
            psource := To_Unbounded_String( "data/ex_shader_pixel.hlsl" );
        elsif platform = ALLEGRO_SHADER_GLSL then
            vsource := To_Unbounded_String( "data/ex_shader_vertex.glsl" );
            psource := To_Unbounded_String( "data/ex_shader_pixel.glsl" );
        else
            -- shouldn't happen
            vsource := To_Unbounded_String( "" );
            psource := To_Unbounded_String( "" );
        end if;
    end Choose_Shader_Source;

    ----------------------------------------------------------------------------

    function Make_Region( parent : A_Allegro_Bitmap;
                          x, y   : Integer;
                          w, h   : Integer;
                          shader : A_Allegro_Shader ) return A_Allegro_Bitmap is
        sub : constant A_Allegro_Bitmap := Al_Create_Sub_Bitmap( parent, x, y, w, h );
    begin
        if sub /= null then
            Al_Set_Target_Bitmap( sub );
            Al_Use_Shader( shader );
            -- Not bothering to restore old target bitmap.
        end if;
        return sub;
    end Make_Region;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display    : A_Allegro_Display;
        image      : A_Allegro_Bitmap;
        backbuffer : A_Allegro_Bitmap;
        region     : Bitmap_Array(0..3);
        shader     : A_Allegro_Shader;
        vsource    : Unbounded_String;
        psource    : Unbounded_String;
        t          : Allegro_Transform;
        s          : Allegro_Keyboard_State;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Display_Flags( ALLEGRO_PROGRAMMABLE_PIPELINE );
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        image := Al_Load_Bitmap( "data/mysha.pcx" );
        if image = null then
            Abort_Example( "Could not load image." );
        end if;

        -- Create the shader.
        shader := Al_Create_Shader( ALLEGRO_SHADER_AUTO );
        if shader = null then
            Abort_Example( "Could not create shader." );
        end if;
        Choose_Shader_Source( Al_Get_Shader_Platform( shader ), vsource, psource );
        if Length( vsource ) = 0 or else Length( psource ) = 0 then
            Abort_Example( "Could not load source files." );
        end if;
        if not Al_Attach_Shader_Source_File( shader, ALLEGRO_VERTEX_SHADER, To_String( vsource ) ) then
             Abort_Example( "al_attach_shader_source_file failed: " & Al_Get_Shader_Log( shader ) );
        end if;
        if not Al_Attach_Shader_Source_File( shader, ALLEGRO_PIXEL_SHADER, To_String( psource ) ) then
            Abort_Example( "al_attach_shader_source_file failed: " & Al_Get_Shader_Log( shader ) );
        end if;
        if not Al_Build_Shader( shader ) then
            Abort_Example( "al_build_shader failed: " & Al_Get_Shader_Log( shader ) );
        end if;

        -- Create four sub-bitmaps of the backbuffer sharing a shader.
        backbuffer := Al_Get_Backbuffer( display );
        region(0) := Make_Region( backbuffer, 0, 0, 320, 200, shader );
        region(1) := Make_Region( backbuffer, 320, 0, 320, 200, shader );
        region(2) := Make_Region( backbuffer, 0, 240, 320, 200, shader );
        region(3) := Make_Region( backbuffer, 320, 240, 320, 200, shader );
        if region(0) = null or region(1) = null or region(2) = null or region(3) = null then
            Abort_Example( "make_region failed" );
        end if;

        -- Apply a transformation to the last region (the current target).
        Al_Identity_Transform( t );
        Al_Scale_Transform( t, 2.0, 2.0 );
        Al_Translate_Transform( t, -160.0, -100.0 );
        Al_Use_Transform( t );

        loop
            Al_Get_Keyboard_State( s );
            if Al_Key_Down( s, ALLEGRO_KEY_ESCAPE ) then
                exit;
            end if;

            for i in region'Range loop
                -- When we change the target bitmap, the shader that was last used on
                -- that bitmap is automatically in effect.  All of our region
                -- sub-bitmaps use the same shader so we need to set the tint variable
                -- each time, as it was clobbered when drawing to the previous region.
                --
                Al_Set_Target_Bitmap( region(i) );
                Al_Set_Shader_Float_Vector( "tint", 3, tints(tints'First+i*3..tints'First+i*3+2) );
                Al_Draw_Bitmap( image, 0.0, 0.0, 0 );
            end loop;

            Al_Set_Target_Backbuffer( display );
            Al_Draw_Tinted_Bitmap( image, Al_Map_RGBA_f( 0.5, 0.5, 0.5, 0.5 ), 320.0 / 2.0, 240.0 / 2.0, 0 );

            Al_Flip_Display;
        end loop;

        Al_Set_Target_Backbuffer( display );
        Al_Use_Shader( null );
        Al_Destroy_Shader( shader );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Shader_Target;
