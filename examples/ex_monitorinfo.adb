
with Allegro;                           use Allegro;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Displays.Fullscreen;       use Allegro.Displays.Fullscreen;
with Allegro.Displays.Monitors;         use Allegro.Displays.Monitors;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;

package body Ex_Monitorinfo is

    procedure Ada_Main is
        info         : Allegro_Monitor_Info;
        num_adapters : Integer;
        dpi          : Integer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        num_adapters := Al_Get_Num_Video_Adapters;

        Log_Print( num_adapters'Img & " adapters found..." );

        for i in 0..num_adapters - 1 loop
            Al_Get_Monitor_Info( i, info );
            Log_Put( "Adapter" & i'Img & ": " );
            dpi := Al_Get_Monitor_Dpi( i );
            Log_Print( "(" & info.x1'Img & "," & info.y1'Img & ") - (" & info.x2'Img & "," & info.y2'Img & ") - dpi:" & dpi'Img );
            Al_Set_New_Display_Adapter( i );
            Log_Print( "   Available fullscreen display modes:" );
            for j in 0..Al_Get_Num_Display_Modes - 1 loop
                declare
                    mode : Allegro_Display_Mode;
                begin
                    Al_Get_Display_Mode( j, mode );
                    Log_Print( "   Mode" & j'Img & ":" & mode.width'Img &
                               " x" & mode.height'Img & "," & mode.refresh_rate'Img &
                               " Hz" );
                end;
            end loop;
        end loop;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Monitorinfo;
