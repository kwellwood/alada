
with Ada.Unchecked_Deallocation;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Native_Dialogs;            use Allegro.Native_Dialogs;
with Allegro.System;                    use Allegro.System;
with Allegro.Threads;                   use Allegro.Threads;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro_Ids;                       use Allegro_Ids;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System;                            use System;

--
--    Example program for the Allegro library.
--
--    The native file dialog addon only supports a blocking interface.  This
--    example makes the blocking call from another thread, using a user event
--    source to communicate back to the main program.
--
package body Ex_Native_Filechooser is

    procedure Ada_Main is

        -- To communicate from a separate thread, we need a user event.
        ASYNC_DIALOG_EVENT1 : constant AL_ID := Allegro_Get_Event_Type( 'e', 'N', 'F', '1' );
        ASYNC_DIALOG_EVENT2 : constant AL_ID := Allegro_Get_Event_Type( 'e', 'N', 'F', '2' );

        type AsyncDialog is
            record
                display      : A_Allegro_Display;
                file_dialog  : A_Allegro_Filechooser;
                event_source : A_Allegro_Event_Source := new Allegro_Event_Source;
                thread       : A_Allegro_Thread;
            end record;
        type A_AsyncDialog is access all AsyncDialog;

        textlog : A_Allegro_Textlog;

        ------------------------------------------------------------------------

        procedure Message( str : String ) is
        begin
#if WINDOWS'Defined then
            Al_Append_Native_Text_Log( textlog, str & ASCII.CR & ASCII.LF );
#else
            Al_Append_Native_Text_Log( textlog, str & ASCII.LF );
#end if;
        end Message;

        ------------------------------------------------------------------------

        function Async_File_Dialog_Thread_Func( thread : A_Allegro_Thread;
                                                arg    : Address ) return Address;
        pragma Convention( C, Async_File_Dialog_Thread_Func );

        -- Our thread to show the native file dialog.
        function Async_File_Dialog_Thread_Func( thread : A_Allegro_Thread;
                                                arg    : Address ) return Address is
            pragma Unreferenced( thread );
            data : AsyncDialog;
                for data'Address use arg;
            pragma Import( Ada, data );
            event : Allegro_Event;
        begin
            --(void)thread;

            -- The next line is the heart of this example - we display the
            -- native file dialog.
            --
            if not Al_Show_Native_File_Dialog( data.display, data.file_dialog ) then
                Abort_Example( "Could not show native dialog" );
            end if;

            -- We emit an event to let the main program know that the thread has
            -- finished.
            --
            event.user.typ := To_Unsigned_32( ASYNC_DIALOG_EVENT1 );
            Al_Emit_User_Event( data.event_source, event, null );

            return Null_Address;
        end Async_File_Dialog_Thread_Func;

        ------------------------------------------------------------------------

        function Message_Box_Thread( thread : A_Allegro_Thread;
                                     arg    : Address ) return Address;
        pragma Convention( C, Message_Box_Thread );

        -- A thread to show the message boxes.
        function Message_Box_Thread( thread : A_Allegro_Thread;
                                     arg    : Address ) return Address is
            pragma Unreferenced( thread );
            data : AsyncDialog;
                for data'Address use arg;
            pragma Import( Ada, data );
            event  : Allegro_Event;
            button : Integer;
        begin
            --(void)thread;

            button := Al_Show_Native_Message_Box( data.display,
                                                  "Warning",
                                                  "Click Detected",
                                                  "That does nothing. Stop clicking there.",
                                                  "Oh no!|Don't press|Ok",
                                                  ALLEGRO_MESSAGEBOX_WARN );
            if button = 2 then
                button := Al_Show_Native_Message_Box( data.display,
                                                      "Error", "Hey!",
                                                      "Stop it! I told you not to click there.",
                                                      "", ALLEGRO_MESSAGEBOX_ERROR );
            end if;
            event.user.typ := To_Unsigned_32( ASYNC_DIALOG_EVENT2 );
            Al_Emit_User_Event( data.event_source, event, null );

            return Null_Address;
        end Message_Box_Thread;

        ------------------------------------------------------------------------

        -- Function to start the new thread.
        function Spawn_Async_File_Dialog( display      : A_Allegro_Display;
                                          initial_path : String ) return A_AsyncDialog is
            data : constant A_AsyncDialog := new AsyncDialog;
        begin
            data.file_dialog := Al_Create_Native_File_Dialog( initial_path, "Choose files", "", ALLEGRO_FILECHOOSER_MULTIPLE );
            Al_Init_User_Event_Source( data.event_source );
            data.display := display;
            data.thread := Al_Create_Thread( Async_File_Dialog_Thread_Func'Unrestricted_Access, data.all'Address );
            Al_Start_Thread( data.thread );
            return data;
        end Spawn_Async_File_Dialog;

        ------------------------------------------------------------------------

        function Spawn_Async_Message_Dialog( display : A_Allegro_Display ) return A_AsyncDialog is
            data : constant A_AsyncDialog := new AsyncDialog;
        begin
            Al_Init_User_Event_Source( data.event_source );
            data.display := display;
            data.thread := Al_Create_Thread( Message_Box_Thread'Unrestricted_Access, data.all'Address );
            Al_Start_Thread( data.thread );
            return data;
        end Spawn_Async_Message_Dialog;

        ------------------------------------------------------------------------

        procedure Stop_Async_Dialog( data : in out A_AsyncDialog ) is
            procedure Free is new Ada.Unchecked_Deallocation( AsyncDialog, A_AsyncDialog );
            procedure Free is new Ada.Unchecked_Deallocation( Allegro_Event_Source,
                                                              A_Allegro_Event_Source );
        begin
            if data /= null then
                Al_Destroy_Thread( data.thread );
                Al_Destroy_User_Event_Source( data.event_source );
                Free( data.event_source );
                if data.file_dialog /= null then
                    Al_Destroy_Native_File_Dialog( data.file_dialog );
                end if;
                Free( data );
            end if;
        end Stop_Async_Dialog;

        ------------------------------------------------------------------------

        -- Helper function to display the result from a file dialog.
        procedure Show_Files_List( dialog : A_Allegro_Filechooser;
                                   font   : A_Allegro_Font;
                                   info   : Allegro_Color ) is
            target : constant A_Allegro_Bitmap := Al_Get_Target_Bitmap;
            count  : constant Integer := Al_Get_Native_File_Dialog_Count( dialog );
            th     : constant Integer := Al_Get_Font_Line_Height( font );
            x      : constant Float := Float(Al_Get_Bitmap_Width( target ) / 2);
            y      : constant Float := Float(Al_Get_Bitmap_Height( target ) / 2 - (count * th) / 2);
        begin
            for i in 0..count-1 loop
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                Al_Draw_Text( font, info, x, y + Float(i * th),
                              ALLEGRO_ALIGN_CENTRE,
                              Al_Get_Native_File_Dialog_Path( dialog, i ) );
            end loop;
        end Show_Files_List;

        ------------------------------------------------------------------------

        display     : A_Allegro_Display;
        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        font        : A_Allegro_Font;
        background,
        active,
        inactive,
        info        : Allegro_Color;
        old_dialog  : A_AsyncDialog;
        cur_dialog  : A_AsyncDialog;
        message_box : A_AsyncDialog;
        redraw      : Boolean := False;
        close_log   : Boolean := False;
        button      : Integer := 0;
        message_log : Boolean := True;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Native_Dialog_Addon then
            Abort_Example( "Could not init native dialog addon." );
        end if;

        textlog := Al_Open_Native_Text_Log( "Log", 0 );
        Message( "Starting up log window." );

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon" );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        background := Al_Color_Name( "white" );
        active := Al_Color_Name( "black" );
        inactive := Al_Color_Name( "gray" );
        info := Al_Color_Name( "red" );

        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;

        Message( "Creating 640x480 window..." );

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Message( "failure." );
            Abort_Example( "Error creating display" );
        end if;
        Message( "success." );

        Message( "Loading font 'data/fixed_font.tga'..." );
        font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        if font = null then
            Message( "failure." );
            Abort_Example( "Error loading data/fixed_font.tga" );
        end if;
        Message( "success." );

        timer := Al_Create_Timer( 1.0 / 30.0 );

    <<restart>>
        Message( "Starting main loop." );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        if textlog /= null then
            Al_Register_Event_Source( queue, Al_Get_Native_Text_Log_Event_Source( textlog ) );
        end if;
        Al_Start_Timer( timer );

        loop
            declare
                h     : constant Float := Float(Al_Get_Display_Height( display ));
                event : Allegro_Event;
            begin
                Al_Wait_For_Event( queue, event );
                if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE and then cur_dialog = null then
                    exit;

                elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE and then cur_dialog = null then
                        exit;
                    end if;

                -- When a mouse button is pressed, and no native dialog is
                -- shown already, we show a new one.
                --
                elsif event.any.typ = ALLEGRO_EVENT_MOUSE_BUTTON_DOWN then
                    Message( "Mouse clicked at" & event.mouse.x'Img & "," & event.mouse.y'Img & "." );
                    if event.mouse.y > 30 then
                        if event.mouse.y > Integer(h) - 30 then
                            message_log := not message_log;
                            if message_log then
                                textlog := Al_Open_Native_Text_Log( "Log", 0 );
                                if textlog /= null then
                                    Al_Register_Event_Source( queue, Al_Get_Native_Text_Log_Event_Source( textlog ) );
                                end if;
                            else
                                close_log := True;
                            end if;
                        elsif message_box = null then
                            message_box := Spawn_Async_Message_Dialog( display );
                            Al_Register_Event_Source( queue, message_box.event_source );
                        end if;
                    elsif cur_dialog = null then
                        -- If available, use the path from the last dialog as
                        -- initial path for the new one.
                        --
                        if old_dialog /= null then
                            cur_dialog := Spawn_Async_File_Dialog( display,
                                                                   Al_Get_Native_File_Dialog_Path( old_dialog.file_dialog, 0 ) );
                        else
                            cur_dialog := Spawn_Async_File_Dialog( display, "" );
                        end if;
                        Al_Register_Event_Source( queue, cur_dialog.event_source );
                    end if;

                -- We receive this event from the other thread when the dialog is
                -- closed.
                --
                elsif event.any.typ = To_Unsigned_32( ASYNC_DIALOG_EVENT1 ) then
                    Al_Unregister_Event_Source( queue, cur_dialog.event_source );

                    -- If files were selected, we replace the old files list.
                    -- Otherwise the dialog was cancelled, and we keep the old results.
                    --
                    if Al_Get_Native_File_Dialog_Count( cur_dialog.file_dialog ) > 0 then
                        if old_dialog /= null then
                            Stop_Async_Dialog( old_dialog );
                        end if;
                        old_dialog := cur_dialog;
                        cur_dialog := null;
                    else
                        Stop_Async_Dialog( cur_dialog );
                    end if;

                elsif event.any.typ = To_Unsigned_32( ASYNC_DIALOG_EVENT2 ) then
                    Al_Unregister_Event_Source( queue, message_box.event_source );
                    Stop_Async_Dialog( message_box );

                elsif event.any.typ = ALLEGRO_EVENT_NATIVE_DIALOG_CLOSE then
                    close_log := True;

                elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                    redraw := True;

                end if;

                if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                    declare
                        x : constant Float := Float(Al_Get_Display_Width( display ) / 2);
                        y : constant Float := 0.0;

                        function Active_Color return Allegro_Color is
                        begin
                            if cur_dialog /= null then
                                return inactive;
                            end if;
                            return active;
                        end Active_Color;

                        function Log_Text return String is
                        begin
                            if message_log then
                                return "Close Message Log";
                            end if;
                            return "Open Message Log";
                        end Log_Text;

                    begin
                        redraw := False;
                        Al_Clear_To_Color( background );
                        Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                        Al_Draw_Text( font, Active_Color, x, y, ALLEGRO_ALIGN_CENTRE, "Open" );
                        Al_Draw_Text( font, Active_Color, x, h - 30.0, ALLEGRO_ALIGN_CENTRE, Log_Text );
                        if old_dialog /= null then
                            Show_Files_List( old_dialog.file_dialog, font, info );
                        end if;
                        Al_Flip_Display;
                    end;
                end if;

                if close_log and then textlog /= null then
                    close_log := False;
                    message_log := False;
                    Al_Unregister_Event_Source( queue, Al_Get_Native_Text_Log_Event_Source( textlog ) );
                    Al_Close_Native_Text_Log( textlog );
                    textlog := null;
                end if;
            end;
        end loop;

        Message( "Exiting." );

        Al_Destroy_Event_Queue( queue );

        button := Al_Show_Native_Message_Box( display,
                                              "Warning",
                                              "Are you sure?",
                                              "If you click yes then this example will inevitably close." &
                                              " This is your last chance to rethink your decision." &
                                              " Do you really want to quit?",
                                              "",
                                              ALLEGRO_MESSAGEBOX_YES_NO or ALLEGRO_MESSAGEBOX_QUESTION );
        if button /= 1 then
            goto restart;
        end if;

        Stop_Async_Dialog( old_dialog );
        Stop_Async_Dialog( cur_dialog );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Native_Filechooser;
