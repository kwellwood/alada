
with Ada.Unchecked_Conversion;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Threads;                   use Allegro.Threads;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.UTF8;                      use Allegro.UTF8;
with Interfaces;                        use Interfaces;
with Common;                            use Common;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with System;                            use System;
with System.Address_Image;

package body Ex_Loading_Thread is

    type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

    load_total : constant Integer := 100;
    load_count : Integer := 0;
    bitmaps    : Bitmap_Array(0..99);
    mutex      : A_Allegro_Mutex;

    task type Loading_Thread is

        entry Stop;

    end Loading_Thread;
    type A_Loading_Thread is access all Loading_Thread;

    task body Loading_Thread is

        function Rand is new GNAT.Random_Numbers.Random_Discrete( Unsigned_8 );

        font  : A_Allegro_Font;
        text  : Allegro_Color;
        color : Allegro_Color;
        bmp   : A_Allegro_Bitmap;
        gen   : Generator;
        done  : Boolean := False;
    begin
        font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        text := Al_Map_RGB( 255, 255, 255 );

        Reset( gen );

        -- In this example we load mysha.pcx 100 times to simulate loading
        -- many bitmaps.
        load_count := 0;
        while load_count < load_total or else done loop
            color := Al_Map_Rgb( Rand( gen, 0, 63 ), Rand( gen, 0, 63 ), Rand( gen, 0, 63 ) );

            select
                accept Stop do
                    done := True;
                end Stop;

            else
                bmp := Al_Load_Bitmap( "data/mysha.pcx" );

                -- Simulate different contents.
                Al_Set_Target_Bitmap( bmp );
                Al_Draw_Filled_Rectangle( 0.0, 0.0, 320.0, 200.0, color );
                Al_Draw_Text( font, text, 0.0, 0.0, 0, "bitmap" & Integer'Image( 1 + load_count ) );
                Al_Set_Target_Bitmap(NULL);

                -- Allow the main thread to see the completed bitmap.
                Al_Lock_Mutex( mutex );
                bitmaps(bitmaps'First + load_count) := bmp;
                load_count := load_count + 1;
                Al_Unlock_Mutex( mutex );

                -- Simulate that it's slow.
                Al_Rest( 0.05 );
            end select;
        end loop;

        Al_Destroy_Font( font );

        if not done then
            accept Stop;
        end if;
    end Loading_Thread;

    ----------------------------------------------------------------------------

    procedure Print_Bitmap_Flags( bitmap : A_Allegro_Bitmap ) is
        ustr : A_Allegro_Ustr := Al_Ustr_New( "" );
    begin
        if (Al_Get_Bitmap_Flags( bitmap ) and ALLEGRO_VIDEO_BITMAP) /= 0 then
            Al_Ustr_Append_Cstr( ustr, " VIDEO" );
        end if;
        if (Al_Get_Bitmap_Flags( bitmap ) and ALLEGRO_MEMORY_BITMAP) /= 0 then
            Al_Ustr_Append_Cstr( ustr, " MEMORY" );
        end if;
        if (Al_Get_Bitmap_Flags( bitmap ) and ALLEGRO_CONVERT_BITMAP) /= 0 then
            Al_Ustr_Append_Cstr( ustr, " CONVERT" );
        end if;

        Al_Ustr_Trim_Ws( ustr );
        Al_Ustr_Find_Replace_Cstr( ustr, 0, " ", " | " );

        Log_Put( Al_Cstr( ustr ) );
        Al_Ustr_Free( ustr );
    end Print_Bitmap_Flags;

    ----------------------------------------------------------------------------

    procedure Ada_Main is

        function Image( bmp : A_Allegro_Bitmap ) return String is
        begin
            if bmp = null then
                return "null";
            end if;
            return System.Address_Image( bmp.all'Address );
        end Image;

        display        : A_Allegro_Display;
        timer          : A_Allegro_Timer;
        queue          : A_Allegro_Event_Queue;
        event          : Allegro_Event;
        redraw         : Boolean := True;
        font           : A_Allegro_Font;
        spin, spin2    : A_Allegro_Bitmap;
        current_bitmap : Integer := 0;
        loaded_bitmap  : Integer := 0;
        thread         : A_Loading_Thread;

        x, y           : Float;
        color          : Allegro_Color;
        t              : Long_Float;
        bw             : Integer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        Init_Platform_Specific;

        Open_Log;

        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        spin := Al_Load_Bitmap( "data/cursor.tga" );
        Log_Print( "default bitmap without display: " & Image( spin ) );

        Al_Set_New_Bitmap_Flags( ALLEGRO_VIDEO_BITMAP );
        spin2 := Al_Load_Bitmap( "data/cursor.tga" );
        Log_Print( "video bitmap without display: " & Image( spin2 ) );

        Log_Put( Image( spin ) & " before create_display: " );
        Print_Bitmap_Flags( spin );
        Log_Print( "" );

        display := Al_Create_Display( 64, 64 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        spin2 := Al_Load_Bitmap( "data/cursor.tga" );
        Log_Print( "video bitmap with display: " & Image( spin2 ) );

        Log_Put( Image( spin ) & " after create_display: " );
        Print_Bitmap_Flags( spin );
        Log_Print( "" );

        Log_Put( Image( spin2 ) & " after create_display: " );
        Print_Bitmap_Flags( spin2 );
        Log_Print( "" );

        Al_Destroy_Display( display );

        Log_Put( Image( spin ) & " after destroy_display: " );
        Print_Bitmap_Flags( spin );
        Log_Print( "" );

        Log_Put( Image( spin2 ) & " after destroy_display: " );
        Print_Bitmap_Flags( spin2 );
        Log_Print( "" );

        display := Al_Create_Display( 640, 480 );

        Log_Put( Image( spin ) & " after create_display: " );
        Print_Bitmap_Flags( spin );
        Log_Print( "" );

        Log_Put( Image( spin2 ) & " after create_display: " );
        Print_Bitmap_Flags( spin2 );
        Log_Print( "" );

        font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );

        mutex := Al_Create_Mutex;
        thread := new Loading_Thread;

        timer := Al_Create_Timer( 1.0 / 30.0 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( queue, event );

            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := True;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                x := 20.0;
                y := 320.0;
                color := Al_Map_RGB( 0, 0, 0 );
                t := Al_Current_Time;

                redraw := False;
                Al_Clear_To_Color( Al_Map_RGB_f( 0.5, 0.6, 1.0 ) );

                Al_Draw_Text( font, color, x + 40.0, y, 0, "Loading " & Integer'Image( 100 * load_count / load_total ) & "%" );

                Al_Lock_Mutex( mutex );
                if loaded_bitmap < load_count then
                    -- This will convert any video bitmaps without a display
                    -- (all the bitmaps being loaded in the loading_thread) to
                    -- video bitmaps we can use in the main thread.
                    --
                    Al_Convert_Bitmap( bitmaps(bitmaps'First + loaded_bitmap) );
                    loaded_bitmap := loaded_bitmap + 1;
                end if;
                Al_Unlock_Mutex( mutex );

                if current_bitmap < loaded_bitmap then
                    Al_Draw_Bitmap( bitmaps(bitmaps'First + current_bitmap), 0.0, 0.0, 0 );
                    if current_bitmap + 1 < loaded_bitmap then
                        current_bitmap := current_bitmap + 1;
                    end if;

                    for i in 0..current_bitmap loop
                        bw := Al_Get_Bitmap_Width( bitmaps(bitmaps'First + i) );
                        Al_Draw_Scaled_Rotated_Bitmap( bitmaps(bitmaps'First + i),
                                                       0.0, 0.0, Float(i mod 20) * 640.0 / 20.0, 360.0 + Float(i / 20) * 24.0,
                                                       32.0 / Float(bw), 32.0 / Float(bw), 0.0, 0 );
                    end loop;
                end if;

                if loaded_bitmap < load_total then
                    Al_Draw_Scaled_Rotated_Bitmap( spin, 16.0, 16.0, x, y, 1.0, 1.0, Float(t) * ALLEGRO_PI * 2.0, 0 );
                end if;

                Al_Flip_Display;
            end if;
        end loop;

        thread.Stop;
        while not thread'Terminated loop
            delay 0.1;
        end loop;

        Al_Destroy_Mutex( mutex );
        Al_Destroy_Font( font );
        Al_Destroy_Display( display );

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Loading_Thread;

