
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Ada.Text_IO;                       use Ada.Text_IO;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Configuration;             use Allegro.Configuration;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.State;                     use Allegro.State;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;
with System.Storage_Elements;           use System.Storage_Elements;

-- Tests some drawing primitives.
package body Ex_Draw is

    procedure Ada_Main is

        type double_array is array (Integer range <>) of double;

        type Example is
            record
                font       : A_Allegro_Font;
                queue      : A_Allegro_Event_Queue;
                background,
                text,
                white,
                foreground : Allegro_Color;
                outline    : Allegro_Color;
                pattern    : A_Allegro_Bitmap;
                zoom       : A_Allegro_Bitmap;
                timer      : double_array(0..3) := (others => 0.0);
                counter    : double_array(0..4) := (others => 0.0);
                FPS        : Integer;
                text_x,
                text_y     : Float := 0.0;
                software   : Boolean := False;
                samples    : Unsigned_32 := 0;
                what       : Integer := 0;
                thickness  : Integer := 0;
            end record;

        ex : Example;

        type A_String is access all String;
        type String_Array is array (Integer range <>) of A_String;

        names : constant String_Array := String_Array'(
            0 => new String'("filled rectangles"),
            1 => new String'("rectangles"),
            2 => new String'("filled circles"),
            3 => new String'("circles"),
            4 => new String'("lines")
        );

        ------------------------------------------------------------------------

        procedure Draw_Pattern( b : A_Allegro_Bitmap ) is
            w       : constant Integer := Al_Get_Bitmap_Width( b );
            h       : constant Integer := Al_Get_Bitmap_Height( b );
            format  : constant Allegro_Pixel_Format := ALLEGRO_PIXEL_FORMAT_BGR_888;
            light   : constant Allegro_Color := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
            dark    : constant Allegro_Color := Al_Map_RGB_f( 1.0, 0.9, 0.8 );
            c       : Allegro_Color;
            lock    : A_Allegro_Locked_Region;
            data    : Address;
        begin
            lock := Al_Lock_Bitmap( b, format, ALLEGRO_LOCK_WRITEONLY );
            data := lock.data;
            for y in 0..h-1 loop
                for x in 0..w-1 loop
                    declare
                        pixel   : Storage_Array(0..2);
                        for pixel'Address use data + Storage_Offset(x * 3);
                        r, g, b : Unsigned_8;
                    begin
                        if (x + y) mod 2 = 1 then
                            c := light;
                        else
                            c := dark;
                        end if;

                        Al_Unmap_RGB( c, r, g, b );
                        pixel(0) := Storage_Element(r);
                        pixel(1) := Storage_Element(g);
                        pixel(2) := Storage_Element(b);
                    end;
                end loop;
                data := data + Storage_Offset(lock.pitch);
            end loop;
            Al_Unlock_Bitmap( b );
        end Draw_Pattern;

        ------------------------------------------------------------------------

        procedure Set_XY( x, y : Float ) is
        begin
            ex.text_x := x;
            ex.text_y := y;
        end Set_XY;

        ------------------------------------------------------------------------

        procedure Print( str : String ) is
            th    : constant Integer := Al_Get_Font_Line_Height( ex.font );
            state : aliased Allegro_State;
        begin
            Al_Store_State( state, ALLEGRO_STATE_BLENDER );

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            Al_Draw_Text( ex.font, ex.text, ex.text_x, ex.text_y, 0, str );

            Al_Restore_State( state );

            ex.text_y := ex.text_y + Float(th);
        end Print;

        ------------------------------------------------------------------------

        procedure Primitive( l, t, r, b : Float; color : Allegro_Color; never_Fill : Boolean ) is
            cx : constant Float := (l + r) / 2.0;
            cy : constant Float := (t + b) / 2.0;
            rx : constant Float := (r - l) / 2.0;
            ry : constant Float := (b - t) / 2.0;
            w  : Integer := ex.what;
            tk : Float;
        begin
            if never_fill then
                tk := 0.0;
            else
                tk := Float(ex.thickness);
            end if;
            if never_fill then
                if w = 0 then
                    w := 1;
                elsif w = 2 then
                    w := 3;
                end if;
            end if;
            case w is
                when 0 => Al_Draw_Filled_Rectangle( l, t, r, b, color );
                when 1 => Al_Draw_Rectangle( l, t, r, b, color, tk );
                when 2 => Al_Draw_Filled_Ellipse( cx, cy, rx, ry, color );
                when 3 => Al_Draw_Ellipse( cx, cy, rx, ry, color, tk );
                when 4 => Al_Draw_Line( l, t, r, b, color, tk );
                when others => null;
            end case;
        end Primitive;

        ------------------------------------------------------------------------

        procedure Draw is
            type Float_Array is array (Integer range <>) of Float;

            w         : constant Integer := Al_Get_Bitmap_Width( ex.zoom );
            h         : constant Integer := Al_Get_Bitmap_Height( ex.zoom );
            screen    : constant A_Allegro_Bitmap := Al_Get_Target_Bitmap;
            rects_num : constant Integer := 16;
            x, y      : Float;
            cx, cy    : Integer;
            cw, ch    : Integer;
            mem       : A_Allegro_Bitmap;
            rects     : Float_Array(0..16*4-1);
        begin
            for j in 0..3 loop
                for i in 0..3 loop
                    rects((j * 4 + i) * 4 + 0) := 2.0 + Float(i) * 0.25 + Float(i) * 7.0;
                    rects((j * 4 + i) * 4 + 1) := 2.0 + Float(j) * 0.25 + Float(j) * 7.0;
                    rects((j * 4 + i) * 4 + 2) := 2.0 + Float(i) * 0.25 + Float(i) * 7.0 + 5.0;
                    rects((j * 4 + i) * 4 + 3) := 2.0 + Float(j) * 0.25 + Float(j) * 7.0 + 5.0;
                end loop;
            end loop;

            Al_Get_Clipping_Rectangle( cx, cy, cw, ch );
            Al_Clear_To_Color( ex.background );

            Set_XY( 8.0, 0.0 );
            Print( "Drawing " & names(ex.what).all & " (press SPACE to change)" );

            Set_XY( 8.0, 16.0 );
            Print( "Original" );

            Set_XY( 80.0, 16.0 );
            Print( "Enlarged x 16" );

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );

            if ex.software then
                Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
                Al_Set_New_Bitmap_Format( Al_Get_Bitmap_Format( Al_Get_Target_Bitmap ) );
                mem := Al_Create_Bitmap( w, h );
                Al_Set_Target_Bitmap( mem );
                x := 0.0;
                y := 0.0;
            else
                mem := null;
                x := 8.0;
                y := 40.0;
            end if;
            Al_Draw_Bitmap( ex.pattern, x, y, 0 );

            -- Draw the test scene.
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
            declare
                rgba       : Allegro_Color := ex.foreground;
                r, g, b, a : Float;
            begin
                Al_Unmap_RGBA_f( rgba, r, g, b, a );
                a := a * 0.5;
                rgba := Al_Map_RGBA_f( r, g, b, a );
                for i in 0..rects_num-1 loop
                    Primitive( x + rects(i * 4 + 0), y + rects(i * 4 + 1),
                               x + rects(i * 4 + 2), y + rects(i * 4 + 3),
                               rgba, False );
                end loop;
            end;

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );

            if ex.software then
                Al_Set_Target_Bitmap( screen );
                x := 8.0;
                y := 40.0;
                Al_Draw_Bitmap( mem, x, y, 0 );
                Al_Destroy_Bitmap( mem );
            end if;

            -- Grab screen contents into our bitmap.
            Al_Set_Target_Bitmap( ex.zoom );
            Al_Draw_Bitmap_Region( screen, x, y, Float(w), Float(h), 0.0, 0.0, 0 );
            Al_Set_Target_Bitmap( screen );

            -- Draw it enlarged.
            x := 80.0;
            y := 40.0;
            Al_Draw_Scaled_Bitmap( ex.zoom, 0.0, 0.0, Float(w), Float(h), x, y, Float(w * 16), Float(h * 16), 0 );

            -- Draw outlines.
            for i in 0..rects_num-1 loop
                Primitive( x + rects(i * 4 + 0) * 16.0, y + rects(i * 4 + 1) * 16.0,
                           x + rects(i * 4 + 2) * 16.0, y + rects(i * 4 + 3) * 16.0,
                           ex.outline, True );
            end loop;

            Set_XY( 8.0, 640.0 - 48.0 );
            Print( "Thickness:" & Integer'Image( ex.thickness ) & " (press T to change)" );
            if ex.software then
                Print( "Drawing with: software (press S to change)" );
            else
                Print( "Drawing with: hardware (press S to change)" );
            end if;
            Print( "Supersampling:" & ex.samples'Img & " (edit ex_draw.ini to change)" );

            -- FIXME: doesn't work
            -- al_get_display_option(ALLEGRO_SAMPLE_BUFFERS));
        end Draw;

        ------------------------------------------------------------------------

        procedure Tick is
        begin
            Draw;
            Al_Flip_Display;
        end Tick;

        ------------------------------------------------------------------------

        procedure Run is
            event     : aliased Allegro_Event;
            need_draw : Boolean := True;
        begin
            loop
                if need_draw and then Al_Is_Event_Queue_Empty( ex.queue ) then
                    Tick;
                    need_draw := False;
                end if;

                Al_Wait_For_Event( ex.queue, event );

                case event.any.typ is
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        return;

                    when ALLEGRO_EVENT_KEY_DOWN =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            return;
                        elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                            ex.what := (ex.what + 1) mod 5;
                        elsif event.keyboard.keycode = ALLEGRO_KEY_S then
                            ex.software := not ex.software;
                        elsif event.keyboard.keycode = ALLEGRO_KEY_T then
                            ex.thickness := (ex.thickness + 1) mod 2;
                        end if;

                    when ALLEGRO_EVENT_TIMER =>
                        need_draw := True;

                    when others =>
                        null;

                end case;
            end loop;
        end Run;

        ------------------------------------------------------------------------

        procedure Init is
        begin
            ex.FPS := 60;

            ex.font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
            if ex.font = null then
                Abort_Example( "data/fixed_font.tga not found." );
            end if;

            ex.background := Al_Color_Name( "beige" );
            ex.foreground := Al_Color_Name( "black" );
            ex.outline := Al_Color_Name( "red" );
            ex.text := Al_Color_Name( "blue" );
            ex.white := Al_Color_Name( "white" );
            ex.pattern := Al_Create_Bitmap( 32, 32 );
            ex.zoom := Al_Create_Bitmap( 32, 32 );
            Draw_Pattern( ex.pattern );
        end Init;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
        config  : A_Allegro_Config;
        value   : Unbounded_String;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init Allegro primitives addon." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init Allegro IIO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        -- Read supersampling info from ex_draw.ini.
        ex.samples := 0;
        config := Al_Load_Config_File( "ex_draw.ini" );
        if config = null then
            config := Al_Create_Config;
        end if;
        value := To_Unbounded_String( Al_Get_Config_Value( config, "settings", "samples" ) );
        if Length( value ) > 0 then
            ex.samples := Unsigned_32'Value( To_String( value ) );
        end if;
        Al_Set_Config_Value( config, "settings", "samples", To_String( value ) );
        if not Al_Save_Config_File( "ex_draw.ini", config ) then
            Put_Line( "Failed to write ex_draw.ini" );
        end if;
        Al_Destroy_Config( config );

        if ex.samples > 0 then
            Al_Set_New_Display_Option( ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_REQUIRE );
            Al_Set_New_Display_Option( ALLEGRO_SAMPLES, ex.samples, ALLEGRO_SUGGEST );
        end if;

        display := Al_Create_Display( 640, 640 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / Long_Float(ex.FPS) );

        ex.queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( ex.queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( ex.queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );
        Run;

        Al_Destroy_Event_Queue( ex.queue );
        Al_Destroy_Timer( timer );
        Al_Destroy_Bitmap( ex.zoom );
        Al_Destroy_Bitmap( ex.pattern );
        Al_Destroy_Font( ex.font );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Draw;
