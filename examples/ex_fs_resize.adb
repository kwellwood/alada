
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with GNAT.Random_Numbers;               use GNAT.Random_Numbers;
with Interfaces;                        use Interfaces;

package body Ex_Fs_Resize is

    procedure Ada_Main is

        NUM_RESOLUTIONS : constant := 4;

        type Resolution is
            record
                w, h : Integer;
            end record;

        type Res_Array is array (Integer range <>) of Resolution;

        res : constant Res_Array(0..3) := ((640, 480),
                                           (800, 600),
                                           (1024, 768),
                                           (1280, 1024));

        cur_res : Integer := 0;

        ------------------------------------------------------------------------

        procedure Redraw( picture : A_Allegro_Bitmap ) is
            target : constant A_Allegro_Bitmap := Al_Get_Target_Bitmap;
            w      : constant Float := Float(Al_Get_Bitmap_Width( target ));
            h      : constant Float := Float(Al_Get_Bitmap_Height( target ));
            color  : Allegro_Color;
            gen    : Generator;
        begin
            Reset( gen );
            color := Al_Map_RGB( Unsigned_8(128 + (Unsigned_32'(Random( gen )) mod 128)),
                                 Unsigned_8(128 + (Unsigned_32'(Random( gen )) mod 128)),
                                 Unsigned_8(128 + (Unsigned_32'(Random( gen )) mod 128)) );
            Al_Clear_To_Color( color );

            color := Al_Map_RGB( 255, 0, 0 );
            Al_Draw_Line( 0.0, 0.0, w, h, color, 0.0 );
            Al_Draw_Line( 0.0, h, w, 0.0, color, 0.0 );

            Al_Draw_Bitmap( picture, 0.0, 0.0, 0 );
            Al_Flip_Display;
        end Redraw;

        ------------------------------------------------------------------------

        procedure Main_Loop( display : A_Allegro_Display;
                             picture : A_Allegro_Bitmap ) is
            queue    : A_Allegro_Event_Queue;
            event    : aliased Allegro_Event;
            can_draw : Boolean;
            new_res  : Integer;
        begin
            queue := Al_Create_Event_Queue;
            Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
            Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

            can_draw := True;

            loop
                if Al_Is_Event_Queue_Empty( queue ) and then can_draw then
                    Redraw( picture );
                end if;
                Al_Wait_For_Event( queue, event );

                if event.any.typ = ALLEGRO_EVENT_DISPLAY_LOST then
                    Log_Print( "Display lost" );
                    can_draw := False;
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_FOUND then
                    Log_Print( "Display found" );
                    can_draw := True;
                elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        exit;
                    end if;

                    new_res := cur_res;

                    if event.keyboard.unichar = Character'Pos('+') or else
                       event.keyboard.unichar = Character'Pos(' ') or else
                       event.keyboard.keycode = ALLEGRO_KEY_ENTER
                    then
                        new_res := (new_res + 1) mod NUM_RESOLUTIONS;
                    elsif event.keyboard.unichar = Character'Pos('-') then
                        new_res := new_res - 1;
                        if new_res < 0 then
                            new_res := NUM_RESOLUTIONS - 1;
                        end if;
                    end if;

                    if new_res /= cur_res then
                        cur_res := new_res;
                        Log_Print( "Switching to" & res(cur_res).w'Img & " x" & res(cur_res).h'Img & "... " );
                        if Al_Resize_Display( display, res(cur_res).w, res(cur_res).h ) then
                            Log_Print( "Succeeded." );
                        else
                            Log_Print( "Failed. current resolution:" & Al_Get_Display_Width( display )'Img & " x" & Al_Get_Display_Height( display )'Img );
                        end if;
                    end if;
                end if;
            end loop;

            Al_Destroy_Event_Queue( queue );
        end Main_Loop;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        picture : A_Allegro_Bitmap;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon." );
        end if;
        Init_Platform_Specific;

        Open_Log_Monospace;

        if Argument_Count = 1 then
            Al_Set_New_Display_Adapter( Integer'Value( Argument(1) ) );
        end if;

        Al_Set_New_Display_Flags( ALLEGRO_FULLSCREEN or ALLEGRO_GENERATE_EXPOSE_EVENTS );
        display := Al_Create_Display( res(cur_res).w, res(cur_res).h );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        picture := Al_Load_Bitmap( "data/mysha.pcx" );
        if picture = null then
            Abort_Example( "mysha.pcx not found" );
        end if;

        Main_Loop( display, picture );

        Al_Destroy_Bitmap( picture );

        -- Destroying the fullscreen display restores the original screen
        -- resolution.  Shutting down Allegro would automatically destroy the
        -- display, too.
        --
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Fs_Resize;
