
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Audio.Codecs;              use Allegro.Audio.Codecs;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

-- Shows the ability to play a sample without a mixer.
package body Ex_Kcm_Direct is

    procedure Ada_Main is
        voice  : A_Allegro_Voice;
        sample : A_Allegro_Sample_Instance;
    begin
        if not Al_Initialize then
          Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if Argument_Count < 1 then
            Log_Print( "This example needs to be run from the command line." );
            Log_Print( "Usage: ex_kcm_direct.exe {audio_files}" );
            Close_Log( True );
            return;
        end if;

        if not Al_Init_Acodec_Addon then
            Abort_Example( "Could not init audio codecs" );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound!" );
        end if;

        for i in 1..Argument_Count loop
            declare
                sample_data : A_Allegro_Sample;
                filename    : constant String := Argument( i );
                chan        : Allegro_Channel_Conf;
                depth       : Allegro_Audio_Depth;
                freq        : unsigned;
                sample_time : Float := 0.0;

            begin
                -- Load the entire sound file from disk.
                sample_data := Al_Load_Sample( filename );
                if sample_data = null then
                    Log_Print( "Could not load sample from '" & filename & "'!" );
                else

                    sample := Al_Create_Sample_Instance( null );
                    if sample = null then
                        Abort_Example( "al_create_sample failed." );
                    end if;

                    if not Al_Set_Sample( sample, sample_data ) then
                        Log_Print( "al_set_sample failed." );
                    else
                        depth := Al_Get_Sample_Instance_Depth( sample );
                        chan := Al_Get_Sample_Instance_Channels( sample );
                        freq := Al_Get_Sample_Instance_Frequency( sample );
                        if depth < ALLEGRO_AUDIO_DEPTH_UINT8 then
                            Log_Print( "Loaded sample:" & Integer(8 + Allegro_Audio_Depth'Pos( depth ) * 8)'Img &
                                       "-bit depth, " & chan'Img & " channels,"
                                       & freq'Img & " Hz" );
                        else
                            Log_Print( "Loaded sample: 0-bit depth, " & chan'Img &
                                       " channels," & freq'Img & " Hz" );
                        end if;
                        Log_Print( "Trying to create a voice with the same specs... " );
                        voice := Al_Create_Voice( freq, depth, chan );
                        if voice = null then
                            Abort_Example( "Could not create ALLEGRO_VOICE." );
                        end if;
                        Log_Print( "done." );

                        if not Al_Attach_Sample_Instance_To_Voice( sample, voice ) then
                            Abort_Example( "al_attach_sample_instance_to_voice failed." );
                        end if;

                        -- Play sample in looping mode.
                        if not Al_Set_Sample_Instance_Playmode( sample, ALLEGRO_PLAYMODE_LOOP ) then
                            null;
                        end if;
                        if not Al_Play_Sample_Instance( sample ) then
                            null;
                      end if;

                        sample_time := Al_Get_Sample_Instance_Time( sample );
                        Log_Put( "Playing '" & filename & "' (" & sample_time'Img & " seconds) 3 times" );

                        Al_Rest( Long_Float(sample_time * 3.0) );

                        if not Al_Stop_Sample_Instance( sample ) then
                            null;
                        end if;
                        Log_Print( "" );

                        -- Free the memory allocated.
                        if not Al_Set_Sample( sample, null ) then
                            null;
                        end if;
                        Al_Destroy_Sample( sample_data );
                        Al_Destroy_Sample_Instance( sample );
                        Al_Destroy_Voice( voice );
                    end if;
                end if;
            end;
        end loop;

        Al_Uninstall_Audio;

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Kcm_Direct;
