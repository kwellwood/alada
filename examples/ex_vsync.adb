
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Configuration;             use Allegro.Configuration;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

-- Tests vsync.
--
package body Ex_Vsync is

    procedure Ada_Main is

        vsync, fullscreen, frequency, bar_width : Integer;

        ------------------------------------------------------------------------

        function Option( config : A_Allegro_Config; name : String; v : Integer ) return Integer is
            value : constant String := Al_Get_Config_Value( config, "settings", name );
            ret   : Integer := v;
        begin
            if value'Length > 0 then
                ret := Integer'Value( value );
            end if;
            Al_Set_Config_Value( config, "settings", name, Trim( ret'Img, Left ) );
            return ret;
        end Option;

        ------------------------------------------------------------------------

        function Display_Warning( queue : A_Allegro_Event_Queue; font : A_Allegro_Font ) return Boolean is
            event   : aliased Allegro_Event;
            display : constant A_Allegro_Display := Al_Get_Current_Display;
            x       : constant Float := Float(Al_Get_Display_Width( display )) / 2.0;
            y       : Float;
            h       : constant Float := Float(Al_Get_Font_Line_Height( font ));
            white   : constant Allegro_Color := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
        begin
            loop
                -- Convert from 200 px on 480px high screen to same relative position
                -- given actual display height.
                y := 5.0 / 12.0 * Float(Al_Get_Display_Height( display ));
                Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
                Al_Draw_Text( font, white, x, y, ALLEGRO_ALIGN_CENTRE,
                              "Do not continue if you suffer from photosensitive epilepsy" );
                Al_Draw_Text( font, white, x, y + 15.0, ALLEGRO_ALIGN_CENTRE,
                              "or simply hate sliding bars.");
                Al_Draw_Text( font, white, x, y + 40.0, ALLEGRO_ALIGN_CENTRE,
                              "Press Escape to quit or Enter to continue.");

                y := y + 100.0;
                Al_Draw_Text( font, white, x, y, ALLEGRO_ALIGN_CENTRE, "Parameters from ex_vsync.ini:" );
                y := y + h;
                Al_Draw_Text( font, white, x, y, ALLEGRO_ALIGN_CENTRE, "vsync:" & vsync'Img );
                y := y + h;
                Al_Draw_Text( font, white, x, y, ALLEGRO_ALIGN_CENTRE, "fullscreen:" & fullscreen'Img );
                y := y + h;
                Al_Draw_Text( font, white, x, y, ALLEGRO_ALIGN_CENTRE, "frequency:" & frequency'Img );
                y := y + h;
                Al_Draw_Text( font, white, x, y, ALLEGRO_ALIGN_CENTRE, "bar width:" & bar_width'Img );

                Al_Flip_Display;

                Al_Wait_For_Event( queue, event );
                if event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        return True;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_ENTER then
                        return False;
                    end if;
                elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                    return True;
                end if;
            end loop;
        end Display_Warning;

        ------------------------------------------------------------------------

        display        : A_Allegro_Display;
        font           : A_Allegro_Font;
        config         : A_Allegro_Config;
        queue          : A_Allegro_Event_Queue;
        write          : Boolean := False;
        quit           : Boolean := False;
        right          : Boolean := True;
        bar_position   : Integer := 0;
        step_size      : constant Integer := 3;
        display_width  : Integer;
        display_height : Integer;
        event          : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init Allegro primitives addon." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image IO addon" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        if not Al_Install_Mouse then
           Abort_Example( "Could not install mouse" );
        end if;

        -- Read parameters from ex_vsync.ini.
        config := Al_Load_Config_File( "ex_vsync.ini" );
        if config = null then
            config := Al_Create_Config;
            write := True;
        end if;

        -- 0 -> Driver chooses.
        -- 1 -> Force vsync on.
        -- 2 -> Force vsync off.
        --
        vsync := Option( config, "vsync", 0 );

        fullscreen := Option( config, "fullscreen", 0 );
        frequency := Option( config, "frequency", 0 );
        bar_width := Option( config, "bar_width", 10 );

        -- Write the file back (so a template is generated on first run).
        if write then
            if Al_Save_Config_File( "ex_vsync.ini", config ) then
                null;
            end if;
        end if;
        Al_Destroy_Config( config );

        -- Vsync 1 means force on, 2 means forced off.
        if vsync > 0 then
            Al_Set_New_Display_Option( ALLEGRO_VSYNC, Unsigned_32(vsync), ALLEGRO_SUGGEST );
        end if;

        -- Force fullscreen mode.
        if fullscreen > 0 then
            Al_Set_New_Display_Flags( ALLEGRO_FULLSCREEN_WINDOW );
            -- Set a monitor frequency.
            if frequency > 0 then
                Al_Set_New_Display_Refresh_Rate( frequency );
            end if;
        end if;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        font := Al_Load_Font( "data/a4_font.tga", 0, 0 );
        if font = null then
            Abort_Example( "Failed to load a4_font.tga" );
        end if;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, al_get_display_event_source( display ) );

        quit := Display_Warning( queue, font );
        Al_Flush_Event_Queue( queue );

        display_width := Al_Get_Display_Width( display );
        display_height := Al_Get_Display_Height( display );

        while not quit loop

            -- With vsync, this will appear as a bar moving smoothly left to right.
            -- Without vsync, it will appear that there are many bars moving left to right.
            -- More importantly, if you view it in fullscreen, the slanting of the bar will
            -- appear more exaggerated as it is now much taller.
            if right then
                bar_position := bar_position + step_size;
            else
                bar_position := bar_position - step_size;
            end if;

            if right and then bar_position >= display_width - bar_width then
                bar_position := display_width - bar_width;
                right := False;
            elsif not right and then bar_position <= 0 then
                bar_position := 0;
                right := True;
            end if;

            Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
            Al_Draw_Filled_Rectangle( Float(bar_position), 0.0, Float(bar_position + bar_width - 1), Float(display_height - 1), Al_Map_RGB_f( 1.0, 1.0, 1.0 ) );

            Al_Flip_Display;

            while Al_Get_Next_Event( queue, event'Access ) loop
                case event.any.typ is
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        quit := True;

                    when ALLEGRO_EVENT_KEY_DOWN =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            quit := True;
                        end if;

                    when others =>
                        null;
                end case;
            end loop;
            -- Let's not go overboard and limit flipping at 1000 Hz. Without
            -- this my system locks up and requires a hard reboot :P
            --
            -- Limiting this to 500hz so the bar doesn't move too fast. We're
            -- no longer in epilepsy mode (I hope).
            --
            Al_Rest( 0.002 );
        end loop;

        Al_Destroy_Font( font );
        Al_Destroy_Event_Queue( queue );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Vsync;
