
with Allegro;                           use Allegro;
with Allegro.File_IO;                   use Allegro.File_IO;
with Allegro.File_System;               use Allegro.File_System;
with Allegro.Paths;                     use Allegro.Paths;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

--
--  ex_file_slice - Use slices to pack many objects into a single file.
--
--  This example packs two strings into a single file, and then uses a
--  file slice to open them one at a time. While this usage is contrived,
--  the same principle can be used to pack multiple images (for example)
--  into a single file, and later read them back via Allegro's image loader.
--
package body Ex_File_Slice is

    procedure Ada_Main is

        BUFFER_SIZE : constant := 1024;

        ------------------------------------------------------------------------

        procedure Pack_Object( file : A_Allegro_File; object : Address; len : Integer_32 ) is
        begin
            -- First write the length of the object, so we know how big to make
            -- the slice when it is opened later.
            Al_Fwrite32le( file, len );
            Al_Fwrite( file, object, size_t(len) );
        end Pack_Object;

        ------------------------------------------------------------------------

        function Get_Next_Chunk( file : A_Allegro_File ) return A_Allegro_File is
            length : Integer_32;
        begin
            -- Reads the length of the next chunk, and if not at end of file,
            -- returns a slice that represents that portion of the file.
            length := Al_Fread32le( file );
            if not Al_Feof( file ) then
                return Al_Fopen_Slice( file, size_t(length), "rw" );
            end if;
            return null;
        end Get_Next_Chunk;

        ------------------------------------------------------------------------

        master, slice : A_Allegro_File;
        tmp_path      : aliased A_Allegro_Path := Al_Create_Path( "" );

        first_string  : String := "Hello, World!";
        second_string : String := "The quick brown fox jumps over the lazy dog.";
        buffer        : String(1..BUFFER_SIZE);
        ok            : Boolean;
        pragma Warnings( Off, ok );
    begin
        if not Al_Initialize then
            Abort_Example( "Error initializing Allegro" );
        end if;

        Open_Log;

        master := Al_Make_Temp_File( "ex_file_slice_XXXX", tmp_path'Access );
        if master = null then
            Abort_Example( "Unable to create temporary file" );
        end if;

        -- Pack both strings into the master file.
        Pack_Object( master, first_string(first_string'First)'Address, first_string'Length );
        Pack_Object( master, second_string(second_string'First)'Address, second_string'Length );

        -- Seek back to the beginning of the file, as if we had just opened it
        Al_Fseek( master, 0, ALLEGRO_SEEK_SET );

        -- Loop through the main file, opening a slice for each object
        slice := Get_Next_Chunk( master );
        while slice /= null loop
            -- Note: While the slice is open, we must avoid using the master file!
            -- If you were dealing with packed images, this is where you would pass 'slice'
            -- to al_load_bitmap_f().

            if Al_Fsize( slice ) < BUFFER_SIZE then
                declare
                    len : Integer_64;
                begin
                    len := Al_Fsize( slice );

                    -- We could have used al_fgets(), but just to show that the file slice
                    -- is constrained to the string object, we'll read the entire slice.
                    if Al_Fread( slice, buffer(buffer'First)'Address, size_t(Al_Fsize( slice )) ) = size_t(len) then
                        Log_Print( "Chunk of size" & Al_Fsize(slice)'Img & ": '" & buffer(1..Integer(Al_Fsize( slice ))) & "'" );
                    else
                        Log_Print( "Incorrect chunk size:" & len'Img );
                    end if;
                end;
            end if;

            -- The slice must be closed before the next slice is opened. Closing
            -- the slice will advanced the master file to the end of the slice.
            Al_Fclose( slice );

            slice := Get_Next_Chunk( master );
        end loop;

        Al_Fclose( master );

        ok := Al_Remove_Filename( Al_Path_Cstr( tmp_path, '/' ) );

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_File_Slice;
