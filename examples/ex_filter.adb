
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System;                            use System;
with System.Storage_Elements;           use System.Storage_Elements;

package body Ex_Filter is

    procedure Ada_Main is

        FPS : constant := 60;

        type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

        type Example_Rec is
            record
                display : A_Allegro_Display;
                font    : A_Allegro_Font;
                bitmaps : Bitmap_Array(0..2*9-1);
                bg,
                fg,
                info    : Allegro_Color;
                bitmap  : Integer := 0;
                ticks   : Integer := 0;
            end record;

        example : Example_Rec;

        type Flags_Array is array (Integer range <>) of Allegro_Bitmap_Flags;

        filter_flags : constant Flags_Array(0..5) := (0,
                                                      ALLEGRO_MIN_LINEAR,
                                                      ALLEGRO_MIPMAP,
                                                      ALLEGRO_MIN_LINEAR or ALLEGRO_MIPMAP,
                                                      0,
                                                      ALLEGRO_MAG_LINEAR);

        type A_String is access all String;
        type String_Array is array (Integer range <>) of A_String;

        filter_text : constant String_Array(0..3) := (new String'("nearest"),
                                                      new String'("linear"),
                                                      new String'("nearest mipmap"),
                                                      new String'("linear mipmap"));

        ------------------------------------------------------------------------

        procedure Update is
        begin
            example.ticks := example.ticks + 1;
        end Update;

        ------------------------------------------------------------------------

        procedure Redraw is
            w : constant Integer := Al_Get_Display_Width( example.display );
            h : constant Integer := Al_Get_Display_Height( example.display );
        begin
            Al_Clear_To_Color( example.bg );

            for i in 0..5 loop
                declare
                    x     : constant Float := Float((i / 2) * w / 3 + w / 6);
                    y     : constant Float := Float((i mod 2) * h / 2 + h / 4);
                    bmp   : constant A_Allegro_Bitmap := example.bitmaps(example.bitmap * 9 + i);
                    bw    : constant Float := Float(Al_Get_Bitmap_Width( bmp ));
                    bh    : constant Float := Float(Al_Get_Bitmap_Height( bmp ));
                    t     : constant Float := 1.0 - 2.0 * abs ((Float(example.ticks mod (FPS * 16)) / 16.0 / Float(FPS)) - 0.5);
                    angle : constant Float := Float(example.ticks) * ALLEGRO_PI * 2.0 / Float(FPS) / 8.0;
                    scale : Float;
                begin
                    if i < 4 then
                        scale := 1.0 - t * 0.9;
                    else
                        scale := 1.0 + t * 9.0;
                    end if;

                    Al_Draw_Text( example.font, example.fg, x, y - 64.0 - 14.0, ALLEGRO_ALIGN_CENTRE, filter_text(i mod 4).all );

                    Al_Set_Clipping_Rectangle( Integer(x) - 64, Integer(y) - 64, 128, 128 );
                    Al_Draw_Scaled_Rotated_Bitmap( bmp, bw / 2.0, bh / 2.0, x, y, scale, scale, angle, 0 );
                    Al_Set_Clipping_Rectangle( 0, 0, w, h );
                end;
            end loop;
            Al_Draw_Text( example.font, example.info, Float(w) / 2.0,
                          Float(h) - 14.0, ALLEGRO_ALIGN_CENTRE,
                          "press space to change" );
        end Redraw;

        ------------------------------------------------------------------------

        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        event       : aliased Allegro_Event;
        w           : constant Integer := 640;
        h           : constant Integer := 480;
        done        : Boolean := False;
        need_redraw : Boolean := True;
        mysha       : A_Allegro_Bitmap;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Failed to init IIO addon." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        example.display := Al_Create_Display( w, h );
        if example.display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        if not Al_Install_Mouse then
            Abort_Example( "Error installing mouse." );
        end if;

        example.font := Al_Load_Font( "data/fixed_font.tga", 0, 0 );
        if example.font = null then
            Abort_Example( "Error loading data/fixed_font.tga" );
        end if;

        Init_Platform_Specific;

        mysha := Al_Load_Bitmap( "data/mysha256x256.png" );
        if mysha = null then
            Abort_Example( "Error loading data/mysha256x256.png" );
        end if;

        for i in 0..5 loop
            declare
                lock : A_Allegro_Locked_Region;
                data : Address;
                c    : Storage_Element;
            begin
                -- Only power-of-two bitmaps can have mipmaps.
                Al_Set_New_Bitmap_Flags( filter_flags(i) );
                example.bitmaps(0 * 9 + i) := Al_Create_Bitmap( 1024, 1024 );
                example.bitmaps(1 * 9 + i) := Al_Clone_Bitmap( mysha );
                lock := Al_Lock_Bitmap( example.bitmaps(0 * 9 + i),
                                        ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE,
                                        ALLEGRO_LOCK_WRITEONLY );
                data := lock.data;
                for y in 0..1023 loop
                    for x in 0..1023 loop
                        declare
                            pixel : Storage_Array(0..3);
                            for pixel'Address use data + Storage_Offset(x * 4);
                        begin
                            c := 0;
                            -- if (((x >> 2) & 1) ^ ((y >> 2) & 1)) c = 255;
                            if ((Shift_Right( Unsigned_64(x), 2 ) and 1) ** Natural(Shift_Right( Unsigned_64(y), 2 ) and 1)) /= 0 then
                                c := 255;
                            end if;
                            pixel(0) := c;
                            pixel(1) := c;
                            pixel(2) := c;
                            pixel(3) := 255;
                        end;
                    end loop;
                    data := data + Storage_Offset(lock.pitch);
                end loop;
            end;
            Al_Unlock_Bitmap( example.bitmaps(0 * 9 + i) );
        end loop;

        example.bg := Al_Map_RGB_f( 0.0, 0.0, 0.0 );
        example.fg := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
        example.info := Al_Map_RGB_f( 0.5, 0.5, 1.0 );

        timer := Al_Create_Timer( 1.0 / Long_Float(FPS) );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( example.display ) );

        Al_Start_Timer( timer );

        while not done loop
            if need_redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Redraw;
                Al_Flip_Display;
                need_redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                        done := True;
                    elsif event.keyboard.keycode = ALLEGRO_KEY_SPACE then
                        example.bitmap := (example.bitmap + 1) mod 2;
                    end if;

                when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                    done := True;

                when ALLEGRO_EVENT_TIMER =>
                    Update;
                    need_redraw := True;

                when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                    example.bitmap := (example.bitmap + 1) mod 2;

                when others =>
                    null;

            end case;
        end loop;

        for i in 0..5 loop
            Al_Destroy_Bitmap( example.bitmaps(0 * 9 + i) );
            Al_Destroy_Bitmap( example.bitmaps(1 * 9 + i) );
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Filter;
