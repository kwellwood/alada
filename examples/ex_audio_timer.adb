
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Streams;                       use Ada.Streams;
with Allegro;                           use Allegro;
with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

--
--    Example program for the Allegro library.
--
package body Ex_Audio_Timer is

    procedure Ada_Main is

        RESERVED_SAMPLES : constant := 16;
        PERIOD           : constant := 5;

        display     : A_Allegro_Display;
        font        : A_Allegro_Font;
        ping        : A_Allegro_Sample;
        timer       : A_Allegro_Timer;
        event_queue : A_Allegro_Event_Queue;

        ------------------------------------------------------------------------

        function Create_Sample_S16( freq : Integer; len : Integer ) return A_Allegro_Sample is
            buf : constant access Stream_Element_Array := new Stream_Element_Array(1..Stream_Element_Offset(freq*len*2));
        begin
            return Al_Create_Sample( buf(buf'First)'Address, buf'Length, Unsigned(freq), ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_1, True );
        end Create_Sample_S16;

        ------------------------------------------------------------------------

        -- Adapted from SPEED.
        function Generate_Ping return A_Allegro_Sample is
            osc1, osc2, vol, ramp : Float;
            len : Integer;
        begin
            -- ping consists of two sine waves
            len := 8192;
            ping := Create_Sample_S16( 22050, len );
            if ping = null then
                return null;
            end if;

            declare
                type Int16_Array is array (0..len-1) of Integer_16;
                p : Int16_Array;
                for p'Address use Al_Get_Sample_Data( ping );
            begin
                osc1 := 0.0;
                osc2 := 0.0;

                for i in p'Range loop
                    vol := Float(len - i) / Float(len) * 4000.0;
                    ramp := Float(i) / Float(len) * 8.0;
                    if ramp < 1.0 then
                        vol := vol * ramp;
                    end if;

                    p(i) := Integer_16((Sin( osc1 ) + Sin( osc2 ) - 1.0) * vol);

                    osc1 := osc1 + 0.1;
                    osc2 := osc2 + 0.15;
                end loop;
            end;

            return ping;
        end Generate_Ping;

        ------------------------------------------------------------------------

        trans      : aliased Allegro_Transform;
        event      : aliased Allegro_Event;
        c          : Allegro_Color;
        bps        : Long_Float := 4.0;
        redraw     : Boolean := False;
        last_timer : Natural := 0;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        font := Al_Create_Builtin_Font;
        if font = null then
            Abort_Example( "Could not create font" );
        end if;

        if not Al_Install_Audio then
            Abort_Example( "Could not init sound" );
        end if;

        if not Al_Reserve_Samples( RESERVED_SAMPLES ) then
            Abort_Example( "Could not set up voice and mixer" );
        end if;

        ping := Generate_Ping;
        if ping = null then
            Abort_Example( "Could not generate sample" );
        end if;

        timer := Al_Create_Timer( 1.0 / bps );
        Al_Set_Timer_Count( timer, -1 );

        event_queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( event_queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( event_queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Identity_Transform( trans );
        Al_Scale_Transform( trans, 16.0, 16.0 );
        Al_Use_Transform( trans );

        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( event_queue, event );
            if event.any.typ = ALLEGRO_EVENT_TIMER then
                if not Al_Play_Sample( ping, 1.0, 0.0, (21.0 / 20.0) ** Natural(event.timer.count mod PERIOD), ALLEGRO_PLAYMODE_ONCE, null ) then
                    Log_Print( "Not enough reserved samples." );
                end if;
                redraw := True;
                last_timer := Natural(event.timer.count);
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                elsif event.keyboard.unichar = Character'Pos( '+' ) or else event.keyboard.unichar = Character'Pos( '=' ) then
                    if bps < 32.0 then
                        bps := bps + 1.0;
                        Al_Set_Timer_Speed( timer, 1.0 / bps );
                    end if;
                elsif event.keyboard.unichar = Character'Pos( '-' ) then
                    if bps > 1.0 then
                        bps := bps - 1.0;
                        Al_Set_Timer_Speed( timer, 1.0 / bps );
                    end if;
                end if;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( event_queue ) then
                if last_timer mod PERIOD = 0 then
                    c := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
                else
                    c := Al_Map_RGB_f( 0.5, 0.5, 1.0 );
                end if;

                Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );
                Al_Draw_Text( font, c, 640.0/32.0, 480.0/32.0 - 4.0, ALLEGRO_ALIGN_CENTRE, last_timer'Img );
                Al_Flip_Display;
            end if;
        end loop;

        Al_Destroy_Event_Queue( event_queue );
        Al_Destroy_Timer( timer );
        Al_Destroy_Font( font );
        Al_Destroy_Display( display );
        Al_Stop_Samples;
        Al_Destroy_Sample( ping );
        Al_Uninstall_Audio;
        Al_Uninstall_Keyboard;

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Audio_Timer;
