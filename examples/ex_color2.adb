
with Ada.Float_Text_IO;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings;                       use Ada.Strings;
with Ada.Strings.Fixed;                 use Ada.Strings.Fixed;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Color.Spaces;              use Allegro.Color.Spaces;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System;                            use System;
with System.Storage_Elements;           use System.Storage_Elements;

package body Ex_Color2 is

    FPS : constant := 60.0;

    function Format( x : Float; precision : Integer ) return String is
        str : String(1..18) := (others => ' ');
    begin
        Ada.Float_Text_IO.Put( str, x, Aft => precision, Exp => 0 );
        return Trim( str, Both );
    end Format;

    type Color_Rec is
        record
            rgb     : Allegro_Color;
            l, a, b : Float;
        end record;

    type Color_Rec_Array is array (Integer range <>) of Color_Rec;

    type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

    type Example_Rec is
        record
            font     : A_Allegro_Font;
            lab      : Bitmap_Array(0..511);
            black    : Allegro_Color;
            white    : Allegro_Color;
            color    : Color_Rec_Array(0..1);
            mx, my   : Integer := 0;
            mb       : Integer := 0;   -- 1 = clicked, 2 = released, 3 = pressed
            slider   : Integer := 0;
            top_y    : Integer := 0;
            left_x   : Integer := 0;
            slider_x : Integer := 0;
            half_x   : Integer := 0;
        end record;

    example : Example_Rec;

    ----------------------------------------------------------------------------

    procedure Draw_Lab( l : Integer ) is
        rg    : A_Allegro_Locked_Region;
        a, b  : Float;
        rgb   : Allegro_Color;
        red,
        green,
        blue  : Float;
    begin
        if example.lab(l) /= null then
            return;
        end if;

        example.lab(l) := Al_Create_Bitmap( 512, 512 );
        rg := Al_Lock_Bitmap( example.lab(l),
                              ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE, ALLEGRO_LOCK_WRITEONLY );

        for y in 0..511 loop
            for x in 0..511 loop
                a := (Float(x) - 511.0 / 2.0) / (511.0 / 2.0);
                b := (Float(y) - 511.0 / 2.0) / (511.0 / 2.0);
                b := -b;
                rgb := Al_Color_LAB( Float(l) / 511.0, a, b );
                if not Al_Is_Color_Valid( rgb ) then
                    rgb := Al_Map_RGB_f( 0.0, 0.0, 0.0 );
                end if;

                Al_Unmap_RGB_f( rgb, red, green, blue );
                red   := Float'Floor( 255.0 * red   );
                green := Float'Floor( 255.0 * green );
                blue  := Float'Floor( 255.0 * blue  );

                declare
                    type Pixel is array (Integer range 0..3) of Unsigned_8;
                    p : Pixel;
                    for p'Address use rg.data + Storage_OFfset((rg.pitch * y) + (x * 4));
                begin
                    p(0) := Unsigned_8(red);
                    p(1) := Unsigned_8(green);
                    p(2) := Unsigned_8(blue);
                    p(3) := 255;
                end;
            end loop;
        end loop;
        Al_Unlock_Bitmap( example.lab(l) );
    end Draw_Lab;

    ----------------------------------------------------------------------------

    procedure Draw_Range( ci : Integer ) is
        l     : constant Float := example.color(ci).l;
        cx    : constant Integer := Integer((example.color(ci).a * 511.0 / 2.0) + 511.0 / 2.0);
        cy    : constant Integer := Integer((-example.color(ci).b * 511.0 / 2.0) + 511.0 / 2.0);
        r     : Integer := 2;
        found : Boolean := False;
        x, y  : Integer;
        a, b  : Float;
        rf,
        gf,
        bf    : Float;
        rgb   : Allegro_Color;
        d     : Long_Float;
    begin
        -- Loop around the center in outward circles, starting with a 3 x 3
        -- rectangle, then 5 x 5, then 7 x 7, and so on.
        -- Each loop has four sides, top, right, bottom, left. For example
        -- these are the four loops for the 5 x 5 case:
        -- 1 2 3 4 .
        -- .       .
        -- .       .
        -- .       .
        -- . . . . .
        --
        -- o o o o 1
        -- .       2
        -- .       3
        -- .       4
        -- . . . . .
        --
        -- o o o o o
        -- .       o
        -- .       o
        -- . 3 2 1 1
        --
        -- o o o o o
        -- 4       o
        -- 3       o
        -- 2       o
        -- 1 o o o o
        --
        -- 1654321
        --
        loop
            found := False;
            x := cx - r / 2;
            y := cy - r / 2;

           for i in 0..r*4-1 loop
               if i < r then
                   x := x + 1;
               elsif i < r * 2 then
                   y := y + 1;
               elsif i < r * 3 then
                   x := x - 1;
               else
                   y := y - 1;
               end if;

               a := (Float(x) - 511.0 / 2.0) / (511.0 / 2.0);
               b := (Float(y) - 511.0 / 2.0) / (511.0 / 2.0);

               b := -b;
               Al_Color_LAB_To_RGB( l, a, b, rf, gf, bf );
               if not (rf < 0.0 or rf > 1.0 or gf < 0.0 or gf > 1.0 or bf < 0.0 or bf > 1.0) then
                   rgb := Al_Map_RGBA_f( rf, gf, bf, 1.0 );
                   d := Al_Color_Distance_ciede2000( rgb, example.color(ci).rgb );
                   if d <= 0.05 then
                       if d > 0.04 then
                           Al_Draw_Pixel( Float(example.half_x * ci) + Float(example.left_x + x),
                                          Float(example.top_y + y), example.white );
                       end if;
                       found := True;
                   end if;
               end if;
           end loop;
           exit when not found;
           r := r + 2;
           exit when r > 128;
        end loop;
    end Draw_Range;

    ----------------------------------------------------------------------------

    procedure Init is
    begin
        example.black := Al_Map_RGB_f( 0.0, 0.0, 0.0 );
        example.white := Al_Map_RGB_f( 1.0, 1.0, 1.0 );
        example.left_x := 120;
        example.top_y := 48;
        example.slider_x := 48;
        example.color(0).l := 0.5;
        example.color(1).l := 0.5;
        example.color(0).a := 0.2;
        example.half_x := Al_Get_Display_Width( Al_Get_Current_Display ) / 2;
    end Init;

    ----------------------------------------------------------------------------

    procedure Draw_Axis( x, y, a, a2, l, tl : Float;
                         label              : String;
                         ticks              : Integer;
                         num1, num2, num    : Float ) is
        x2, y2 : Float;
        lv     : Float;
    begin
        Al_Draw_Line( x, y, x + l * Cos( a ), y - l * Sin( a ), example.white, 1.0 );
        for i in 0..ticks-1 loop
            x2 := x + l * Cos( a ) * Float(i) / Float(ticks - 1);
            y2 := y - l * Sin( a ) * Float(i) / Float(ticks - 1);
            Al_Draw_Line( x2, y2, x2 + tl * Cos( a2 ), y2 - tl * Sin( a2 ), example.white, 1.0 );
            Al_Draw_Text( example.font, example.white,
                          Float'Rounding( x2 + (tl + 2.0) * Cos( a2 ) ),
                          Float'Rounding( y2 - (tl + 2.0) * Sin( a2 ) ),
                          ALLEGRO_ALIGN_RIGHT, Format(
                          num1 + Float(i) * (num2 - num1) / Float(ticks - 1), 2 ) );
        end loop;
        lv := (num - num1) * l / (num2 - num1);
        Al_Draw_Filled_Circle(x + lv * Cos( a ), y - lv * Sin( a ), 8.0, example.white );
        Al_Draw_Text( example.font, example.white,
                      Float'Rounding( x + (l + 4.0) * Cos( a ) ) - 24.0,
                      Float'Rounding( y - (l + 4.0) * Sin( a ) ) - 12.0, 0, label );
    end Draw_Axis;

    ----------------------------------------------------------------------------

    procedure Redraw is
        w       : constant Integer := Al_Get_Display_Width( Al_Get_Current_Display );
        h       : constant Integer := Al_Get_Display_Height( Al_Get_Current_Display );
        cx      : Integer;
        l, a, b : Float;
        rgb     : Allegro_Color;
        c       : Allegro_Color;
        rgb_r, rgb_g, rgb_b : Float;
    begin
        Al_Clear_To_Color( example.black );

        for ci in 0..1 loop
            cx := w / 2 * ci;

            l := example.color(ci).l;
            a := example.color(ci).a;
            b := example.color(ci).b;

            rgb := example.color(ci).rgb;

            Draw_Lab( Integer(l * 511.0) );

            Al_Draw_Bitmap( example.lab(Integer(l * 511.0)), Float(cx + example.left_x), Float(example.top_y), 0 );

            Draw_Axis( Float(cx + example.left_x), Float(example.top_y) + 512.5, 0.0, ALLEGRO_PI / (-2.0), 512.0, 16.0,
                       "a*", 11, -1.0, 1.0, a );
            Draw_Axis( Float(cx + example.left_x) - 0.5, Float(example.top_y) + 512.0, ALLEGRO_PI / 2.0, ALLEGRO_PI,
                       512.0, 16.0, "b*", 11, -1.0, 1.0, b );

            Al_Unmap_RGB_f( rgb, rgb_r, rgb_g, rgb_b );

            Al_Draw_Text( example.font, example.white, Float(cx + example.left_x) + 36.0, 8.0, 0, "L*a*b* = " &
                          Format( l, 2 ) & "/" & Format( a, 2 ) & "/" & Format( b, 2 ) & " sRGB = " &
                          Format( rgb_r, 2 ) & "/" & Format( rgb_g, 2 ) & "/" & Format( rgb_b, 2 ) );
            Draw_Axis( Float(cx + example.slider_x) - 0.5, Float(example.top_y) + 512.0, ALLEGRO_PI / 2.0, ALLEGRO_PI,
                       512.0, 4.0, "L*", 3, 0.0, 1.0, l );

            c := Al_Map_RGB_f( rgb_r, rgb_g, rgb_b );
            Al_Draw_Filled_Rectangle( Float(cx), Float(h) - 128.0, Float(cx + w / 2), Float(h), c );

            Draw_Range( ci );
        end loop;

        Al_Draw_Text( example.font, example.white, Float(example.left_x) + 36.0, 28.0, 0,
                      "Lab colors visible in sRGB" );
        Al_Draw_Text( example.font, example.white, Float(example.half_x + example.left_x) + 36.0, 28.0, 0,
                      "ellipse shows CIEDE2000 between 0.4 and 0.5" );

        Al_Draw_Line( Float(w / 2), 0.0, Float(w / 2), Float(h) - 128.0, example.white, 4.0 );

        declare
            function Get_R( c : Allegro_Color ) return Float is r, g, b : Float; begin Al_Unmap_RGB_f( c, r, g, b ); return r; end Get_R;
            function Get_G( c : Allegro_Color ) return Float is r, g, b : Float; begin Al_Unmap_RGB_f( c, r, g, b ); return g; end Get_G;
            function Get_B( c : Allegro_Color ) return Float is r, g, b : Float; begin Al_Unmap_RGB_f( c, r, g, b ); return b; end Get_B;
            dr    : constant Float := Get_R( example.color(0).rgb ) - Get_R( example.color(1).rgb );
            dg    : constant Float := Get_G( example.color(0).rgb ) - Get_G( example.color(1).rgb );
            db    : Float := Get_B( example.color(0).rgb ) - Get_B( example.color(1).rgb );
            drgb  : constant Float := Sqrt( dr * dr + dg * dg + db * db );
            dl    : constant Float := example.color(0).l - example.color(1).l;
            da    : constant Float := example.color(0).a - example.color(1).a;
            dlab  : Float;
            d2000 : Long_Float;
        begin
            db := example.color(0).b - example.color(1).b;
            dlab := Sqrt( da * da + db * db + dl * dl );
            d2000 := Al_Color_Distance_Ciede2000( example.color(0).rgb,
                                                  example.color(1).rgb );
            Al_Draw_Text( example.font, example.white, Float(w / 2), Float(h) - 64.0,
                          ALLEGRO_ALIGN_CENTER, "dRGB = " & Format( drgb, 2 ) );
            Al_Draw_Text( example.font, example.white, Float(w / 2), Float(h) - 64.0 + 12.0,
                          ALLEGRO_ALIGN_CENTER, "dLab = " & Format( dlab, 2 ) );
            Al_Draw_Text( example.font, example.white, Float(w / 2), Float(h) - 64.0 + 24.0,
                          ALLEGRO_ALIGN_CENTER, "CIEDE2000 = " & Format( Float(d2000), 2 ) );
        end;

        Al_Draw_Rectangle( Float(w / 2) - 80.0, Float(h) - 70.0, Float(w / 2) + 80.0, Float(h) - 24.0,
                           example.white, 4.0 );
    end Redraw;

    ----------------------------------------------------------------------------

    procedure Update is
        s  : Integer;
        mx : Integer;
        l  : Integer;
        a  : Integer;
        b  : Integer;
        ci : Integer;
    begin
        if example.mb = 1 or example.mb = 3 then
            if example.mb = 1 then
                s := 0;
                mx := example.mx;
                if example.mx >= example.half_x then
                    mx := mx - example.half_x;
                    s := s + 2;
                end if;
                if mx > example.left_x then
                    s := s + 1;
                end if;
                example.slider := s;
            end if;

            if example.slider = 0 or example.slider = 2 then
               l := Integer'Min( Integer'Max( 0, example.my - example.top_y ), 511 );
               example.color(example.slider / 2).l := 1.0 - Float(l) / 511.0;
            else
                ci := example.slider / 2;
                a := example.mx - example.left_x - ci * example.half_x;
                b := example.my - example.top_y;
                b := 511 - b;
                a := Integer'Min( Integer'Max( 0, a ), 511 );
                b := Integer'Min( Integer'Max( 0, b ), 511 );
                example.color(ci).a := 2.0 * Float(a) / 511.0 - 1.0;
                example.color(ci).b := 2.0 * Float(b) / 511.0 - 1.0;
            end if;
        end if;

         if example.mb = 1 then example.mb := 3; end if;
         if example.mb = 2 then example.mb := 0; end if;

         for ci in 0..1 loop
             example.color(ci).rgb := Al_Color_Lab( example.color(ci).l,
                                                    example.color(ci).a, example.color(ci).b );
         end loop;
    end Update;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        timer       : A_Allegro_Timer;
        queue       : A_Allegro_Event_Queue;
        display     : A_Allegro_Display;
        w           : constant Integer := 1280;
        h           : constant Integer := 720;
        done        : Boolean := False;
        need_redraw : Boolean := True;
        event       : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Failed to init font addon." );
        end if;
        example.font := Al_Create_Builtin_Font;

        Init_Platform_Specific;

        display := Al_Create_Display( w, h );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Failed to init keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Failed to init mouse." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Failed to init primitives addon." );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / FPS );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Register_Event_Source( queue, al_get_display_event_source( display ) );

        Al_Start_Timer( timer );

        while not done loop
            if need_redraw then
                Redraw;
                Al_Flip_Display;
                need_redraw := False;
            end if;

            loop
                Al_Wait_For_Event( queue, event );
                case event.any.typ is
                    when ALLEGRO_EVENT_KEY_CHAR =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            done := True;
                        end if;

                    when ALLEGRO_EVENT_MOUSE_AXES =>
                        example.mx := event.mouse.x;
                        example.my := event.mouse.y;

                    when ALLEGRO_EVENT_MOUSE_BUTTON_DOWN =>
                        example.mb := 1;

                    when ALLEGRO_EVENT_MOUSE_BUTTON_UP =>
                        example.mb := 2;

                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        done := True;

                    when ALLEGRO_EVENT_TIMER =>
                        Update;
                        need_redraw := True;

                    when others =>
                        null;
                end case;
                exit when Al_Is_Event_Queue_Empty( queue );
            end loop;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Color2;
