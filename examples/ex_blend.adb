
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with System;                            use System;
with System.Storage_Elements;           use System.Storage_Elements;

package body Ex_Blend is

    procedure Ada_Main is

        type A_String is access all String;
        type String_Array is array (Natural range <>) of A_String;

        blend_names : constant String_Array := String_Array'( new String'("ZERO"),
                                                              new String'("ONE"),
                                                              new String'("ALPHA"),
                                                              new String'("INVERSE") );

        blend_vnames : constant String_Array := String_Array'( new String'("ZERO"),
                                                               new String'("ONE"),
                                                               new String'("ALPHA"),
                                                               new String'("INVER") );

        type Mode_Array is array (Natural range <>) of Allegro_Blend_Mode;

        blend_modes : constant Mode_Array := Mode_Array'( ALLEGRO_ZERO,
                                                 ALLEGRO_ONE,
                                                 ALLEGRO_ALPHA,
                                                 ALLEGRO_INVERSE_ALPHA );

        -- A structure holding all variables of our example program.
        type Example is
            record
                example      : A_Allegro_Bitmap;
                offscreen    : A_Allegro_Bitmap;
                memory       : A_Allegro_Bitmap;
                myfont       : A_Allegro_Font;
                queue        : A_Allegro_Event_Queue;
                image        : Integer := 0;
                mode         : Integer := 0;
                BUTTONS_X    : Integer := 0;
                last_second  : Long_Float := 0.0;
                frames_accum : Integer := 0;
                fps          : Long_Float := 0.0;
                targetFPS    : Integer := 0;
            end record;

        ex : Example;

        -- Print some text with a shadow.
        procedure Print( x, y : Float; vertical : Boolean; message : String ) is
            h     : constant Integer := Al_Get_Font_Line_Height( ex.myfont );
            color : Allegro_Color;
        begin
            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );

            for j in 0..1 loop
                if j = 0 then
                    color := Al_Map_RGB( 0, 0, 0 );
                else
                    color := Al_Map_RGB( 255, 255, 255 );
                end if;

                if vertical then
                    for i in 0..message'Length - 1 loop
                        Al_Draw_Text( ex.myfont, color,
                                      x + 1.0 - Float(j), y + 1.0 - Float(j) + Float(h * i),
                                      0, message(message'First+i..message'First+i) );
                    end loop;
                else
                    Al_Draw_Text( ex.myfont, color,
                                  x + 1.0 - Float(j), y + 1.0 - Float(j),
                                  0, message );
                end if;
            end loop;
        end Print;

        ------------------------------------------------------------------------

        -- Create an example bitmap.
        function Create_Example_Bitmap return A_Allegro_Bitmap is
            bitmap : A_Allegro_Bitmap;
            locked : A_Allegro_Locked_Region;
        begin
            bitmap := Al_Create_Bitmap( 100, 100 );
            locked := Al_Lock_Bitmap( bitmap, ALLEGRO_PIXEL_FORMAT_ABGR_8888, ALLEGRO_LOCK_WRITEONLY );

            declare
                data : Address := locked.data;
                x, y : Integer;
                r    : Float;
                rc   : Float;
            begin
                for j in 0..99 loop
                    for i in 0..99 loop
                        x := i - 50;
                        y := j - 50;
                        r := Sqrt( Float(x * x + y * y) );
                        rc := 1.0 - r / 50.0;
                        if rc < 0.0 then
                            rc := 0.0;
                        end if;
                        declare
                            pixel : Storage_Array(0..3);
                            for pixel'Address use data + Storage_Offset(i * 4);
                        begin
                            pixel(0) := Storage_Element(i * 255 / 100);
                            pixel(1) := Storage_Element(j * 255 / 100);
                            pixel(2) := Storage_Element(rc * 255.0);
                            pixel(3) := Storage_Element(rc * 255.0);
                        end;
                    end loop;
                    data := data + Storage_Offset(locked.pitch);
                end loop;
            end;

            Al_Unlock_Bitmap( bitmap );

            return bitmap;
        end Create_Example_Bitmap;

        ------------------------------------------------------------------------

        -- Draw our example scene.
        procedure Draw is
            type Color_Array is array (Integer range <>) of Allegro_Color;

            --------------------------------------------------------------------

            function Star( a, b : Integer ) return String is
            begin
                if a = b then
                    return "*";
                else
                    return " ";
                end if;
            end Star;

            --------------------------------------------------------------------

            target : constant A_Allegro_Bitmap := Al_Get_Target_Bitmap;
            x      : constant Float := 40.0;
            y      : constant Float := 40.0;
            test   : Color_Array(0..4);
        begin
            Al_Clear_To_Color( Al_Map_RGB_f( 0.5, 0.5, 0.5 ) );

            test(0) := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
            test(1) := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 0.5 );
            test(2) := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 0.25 );
            test(3) := Al_Map_RGBA_f( 1.0, 0.0, 0.0, 0.75 );
            test(4) := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 0.0 );

            Print( x, 0.0, False, "D  E  S  T  I  N  A  T  I  O  N  (" & Integer'Image( Integer(ex.fps) ) & " fps )" );
            Print( 0.0, y, True, "S O U R C E" );
            for i in blend_names'Range loop
                Print( x + Float((i - blend_names'First) * 110), 20.0, False, blend_names(i).all );
                Print( 20.0, y + Float((i - blend_names'First) * 110), True, blend_vnames(i).all );
            end loop;

            Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
            if ex.mode >= 1 and then ex.mode <= 5 then
                Al_Set_Target_Bitmap( ex.offscreen );
                Al_Clear_To_Color( test(ex.mode - 1) );
            end if;

            if ex.mode >= 6 and then ex.mode <= 10 then
                Al_Set_Target_Bitmap( ex.memory );
                Al_Clear_To_Color( test(ex.mode - 6) );
            end if;

            for j in 0..3 loop
                for i in 0..3 loop
                    Al_Set_Blender( ALLEGRO_ADD, blend_modes(blend_modes'First+j), blend_modes(blend_modes'First+i) );
                    if ex.image = 0 then
                        Al_Draw_Bitmap( ex.example, x + Float(i * 110), y + Float(j * 110), 0 );
                    elsif ex.image >= 1 and then ex.image <= 6 then
                        Al_Draw_Filled_Rectangle( x + Float(i * 110), y + Float(j * 110),
                                                  x + Float(i * 110 + 100), y + Float(j * 110 + 100),
                                                  test(ex.image - 1) );
                    end if;
                end loop;
            end loop;

            if ex.mode >= 1 and then ex.mode <= 5 then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                Al_Set_Target_Bitmap( target );
                Al_Draw_Bitmap_Region( ex.offscreen, x, y, 430.0, 430.0, x, y, 0 );
            end if;
            if ex.mode >= 6 and then ex.mode <= 10 then
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                Al_Set_Target_Bitmap( target );
                Al_Draw_Bitmap_Region( ex.memory, x, y, 430.0, 430.0, x, y, 0 );
            end if;

            Print( Float(ex.BUTTONS_X), 20.0 * 1.0, False, "What to draw");
            Print( Float(ex.BUTTONS_X), 20.0 * 2.0, False, Star( ex.image, 0 ) & " Picture" );
            Print( Float(ex.BUTTONS_X), 20.0 * 3.0, False, Star( ex.image, 1 ) & " Rec1 (1/1/1/1)" );
            Print( Float(ex.BUTTONS_X), 20.0 * 4.0, False, Star( ex.image, 2 ) & " Rec2 (1/1/1/.5)" );
            Print( Float(ex.BUTTONS_X), 20.0 * 5.0, False, Star( ex.image, 3 ) & " Rec3 (1/1/1/.25)" );
            Print( Float(ex.BUTTONS_X), 20.0 * 6.0, False, Star( ex.image, 4 ) & " Rec4 (1/0/0/.75)" );
            Print( Float(ex.BUTTONS_X), 20.0 * 7.0, False, Star( ex.image, 5 ) & " Rec5 (0/0/0/0)" );

            Print( Float(ex.BUTTONS_X), 20.0 * 9.0, False, "Where to draw" );
            Print( Float(ex.BUTTONS_X), 20.0 * 10.0, False, Star( ex.mode, 0 ) & " screen" );

            Print( Float(ex.BUTTONS_X), 20.0 * 11.0, False, Star( ex.mode, 1 ) & " offscreen1" );
            Print( Float(ex.BUTTONS_X), 20.0 * 12.0, False, Star( ex.mode, 2 ) & " offscreen2" );
            Print( Float(ex.BUTTONS_X), 20.0 * 13.0, False, Star( ex.mode, 3 ) & " offscreen3" );
            Print( Float(ex.BUTTONS_X), 20.0 * 14.0, False, Star( ex.mode, 4 ) & " offscreen4" );
            Print( Float(ex.BUTTONS_X), 20.0 * 15.0, False, Star( ex.mode, 5 ) & " offscreen5" );

            Print( Float(ex.BUTTONS_X), 20.0 * 16.0, False, Star( ex.mode, 6 ) & " memory1" );
            Print( Float(ex.BUTTONS_X), 20.0 * 17.0, False, Star( ex.mode, 7 ) & " memory2" );
            Print( Float(ex.BUTTONS_X), 20.0 * 18.0, False, Star( ex.mode, 8 ) & " memory3" );
            Print( Float(ex.BUTTONS_X), 20.0 * 19.0, False, Star( ex.mode, 9 ) & " memory4" );
            Print( Float(ex.BUTTONS_X), 20.0 * 20.0, False, Star( ex.mode, 10 ) & " memory5" );
        end Draw;

        ------------------------------------------------------------------------

        -- Called a fixed amount of times per second.
        procedure Tick is
            t : Long_Float;
        begin
            -- Count frames during the last second or so.
            t := Al_Get_Time;
            if t >= ex.last_second + 1.0 then
                ex.fps := Long_Float(ex.frames_accum) / (t - ex.last_second);
                ex.frames_accum := 0;
                ex.last_second := t;
            end if;

            Draw;
            Al_Flip_Display;
            ex.frames_accum := ex.frames_accum + 1;
        end Tick;

        ------------------------------------------------------------------------

        -- Run our test.
        procedure Run is
            event     : aliased Allegro_Event;
            x, y      : Integer;
            need_draw : Boolean := True;
            button    : Integer;
        begin
            loop
                -- Perform frame skipping so we don't fall behind the timer events.
                if need_draw and then Al_Is_Event_Queue_Empty( ex.queue ) then
                    Tick;
                    need_draw := False;
                end if;

                Al_Wait_For_Event( ex.queue, event );

                case event.any.typ is
                    -- Was the X button on the window pressed?
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        return;

                    -- Was a key pressed?
                    when ALLEGRO_EVENT_KEY_DOWN =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            return;
                        end if;

                    -- Is it time for the next timer tick?
                    when ALLEGRO_EVENT_TIMER =>
                        need_draw := True;

                    -- Mouse click?
                    when ALLEGRO_EVENT_MOUSE_BUTTON_UP =>
                        x := event.mouse.x;
                        y := event.mouse.y;
                        if x >= ex.BUTTONS_X then
                            button := y / 20;
                            if    button = 2 then
                                ex.image := 0;
                            elsif button = 3 then
                                ex.image := 1;
                            elsif button = 4 then
                                ex.image := 2;
                            elsif button = 5 then
                                ex.image := 3;
                            elsif button = 6 then
                                ex.image := 4;
                            elsif button = 7 then
                                ex.image := 5;

                            elsif button = 10 then
                                ex.mode := 0;

                            elsif button = 11 then
                                ex.mode := 1;
                            elsif button = 12 then
                                ex.mode := 2;
                            elsif button = 13 then
                                ex.mode := 3;
                            elsif button = 14 then
                                ex.mode := 4;
                            elsif button = 15 then
                                ex.mode := 5;

                            elsif button = 16 then
                                ex.mode := 6;
                            elsif button = 17 then
                                ex.mode := 7;
                            elsif button = 18 then
                                ex.mode := 8;
                            elsif button = 19 then
                                ex.mode := 9;
                            elsif button = 20 then
                                ex.mode := 10;
                            end if;
                        end if;

                    when others =>
                        null;
                end case;
            end loop;
        end Run;

        ------------------------------------------------------------------------

        -- Initialize the example.
        procedure Init is
        begin
            ex.BUTTONS_X := 40 + 110 * 4;
            ex.targetFPS := 60;
            ex.image := 0;
            ex.mode := 0;

            ex.myfont := Al_Load_Font( "data/font.tga", 0, 0 );
            if ex.myfont = null then
                Abort_Example( "data/font.tga not found" );
            end if;

            ex.example := Create_Example_Bitmap;

            ex.offscreen := Al_Create_Bitmap( 640, 480 );
            Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
            ex.memory := Al_Create_Bitmap( 640, 480 );
        end Init;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init Allegro primitives addon." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init Allegro IIO addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        Init;

        timer := Al_Create_Timer( 1.0 / Long_Float(ex.targetFPS) );

        ex.queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( ex.queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Mouse_Event_Source );
        Al_Register_Event_Source( ex.queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( ex.queue, Al_Get_Timer_Event_Source( timer ) );

        Al_Start_Timer( timer );

        Run;

        Al_Stop_Timer( timer );
        Al_Destroy_Event_Queue( ex.queue );
        Al_Destroy_Timer( timer );

        Al_Destroy_Bitmap( ex.memory );
        Al_Destroy_Bitmap( ex.offscreen );
        Al_Destroy_Bitmap( ex.example );
        Al_Destroy_Font( ex.myfont );

        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Blend;
