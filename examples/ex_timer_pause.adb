
with Ada.Text_IO;                       use Ada.Text_IO;
with Allegro;                           use Allegro;
with Allegro.Events;                    use Allegro.Events;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;

-- A test of pausing/resuming a timer.
--
-- 1. Create two 5s long timers.
-- 2. Let each run for 2s, then stop each for 2s.
-- 3. Call al_resume_timer on timer1 and al_start_timer on timer2
-- 4. Wait for timer events
--
-- timer1 should finish before timer2, as it was resumed rather than restarted
--
package body Ex_Timer_Pause is

    procedure Ada_Main is
        dur       : constant Long_Float := 5.0;    -- timer lasts for 5 seconds
        pre_pause : constant Long_Float := 2.0;    -- how long to wait before pausing
        pause     : constant Long_Float := 2.0;    -- how long to pause timer for
        timer1,
        timer2    : A_Allegro_Timer;
        queue     : A_Allegro_Event_Queue;
        ev        : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Put_Line( "Creating a pair of " & dur'Img & "s timers" );
        queue := Al_Create_Event_Queue;
        timer1 := Al_Create_Timer( dur );
        timer2 := Al_Create_Timer( dur );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer1 ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer2 ) );

        Put_Line( "Starting both timers at: " & Long_Float'Image( Al_Get_Time * 100.0 ) & "s" );
        Al_Start_Timer( timer1 );
        Al_Start_Timer( timer2 );
        Al_Rest( pre_pause );

        Put_Line( "Pausing timers at: " & Long_Float'Image( Al_Get_Time * 100.0 ) & "s" );
        Al_Stop_Timer( timer1 );
        Al_Stop_Timer( timer2 );
        Al_Rest( pause );

        Put_Line( "Resume timer1 at: " & Long_Float'Image( Al_Get_Time * 100.0 ) & "s" );
        Al_Resume_Timer( timer1 );

        Put_Line( "Restart timer2 at: " & Long_Float'Image( Al_Get_Time * 100.0 ) & "s" );
        Al_Start_Timer( timer2 );

        Al_Wait_For_Event( queue, ev );
        Put_Line( "Timer" & (if Al_Get_Timer_Event_Source( timer1 ) = ev.any.source then "1" else "2") &
                  " finished at: " & Long_Float'Image( Al_Get_Time * 100.0 ) & "s" );

        Al_Wait_For_Event( queue, ev );
        Put_Line( "Timer" & (if Al_Get_Timer_Event_Source( timer1 ) = ev.any.source then "1" else "2") &
                  " finished at: " & Long_Float'Image( Al_Get_Time * 100.0 ) & "s" );

        Al_Destroy_Event_Queue( queue );
        Al_Destroy_Timer( timer1 );
        Al_Destroy_Timer( timer2 );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Timer_Pause;
