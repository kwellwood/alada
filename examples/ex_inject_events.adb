
with Allegro;                           use Allegro;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;

-- Ryan Roden-Corrent
-- Example that injects regular (non-user-type) allegro events into a queue.
-- This could be useful for 'faking' certain event sources.
-- For example, you could imitate joystick events without a * joystick.
--
-- Based on the ex_user_events.c example.
package body Ex_Inject_Events is

    procedure Ada_Main is
        fake_src            : A_Allegro_Event_Source := new Allegro_Event_Source;
        queue               : A_Allegro_Event_Queue;
        fake_keydown_event,
        fake_joystick_event : Allegro_Event;
        event               : Allegro_Event;
    begin
        if not Al_Initialize then
            Delete( fake_src );
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        -- register our 'fake' event source with the queue
        Al_Init_User_Event_Source( fake_src );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, fake_src );

        -- fake a joystick event
        fake_joystick_event.any.typ := ALLEGRO_EVENT_JOYSTICK_AXIS;
        fake_joystick_event.joystick.stick := 1;
        fake_joystick_event.joystick.axis := 0;
        fake_joystick_event.joystick.pos := 0.5;
        Al_Emit_User_Event( fake_src, fake_joystick_event, null );

        -- fake a keyboard event
        fake_keydown_event.any.typ := ALLEGRO_EVENT_KEY_DOWN;
        fake_keydown_event.keyboard.keycode := ALLEGRO_KEY_ENTER;
        Al_Emit_User_Event( fake_src, fake_keydown_event, null );

        -- poll for the events we injected
        while not Al_Is_Event_Queue_Empty( queue ) loop
            Al_Wait_For_Event( queue, event );

            case event.any.typ is
                when ALLEGRO_EVENT_KEY_DOWN =>
                    Log_Print( "Got keydown:" & event.keyboard.keycode'Img );

                when ALLEGRO_EVENT_JOYSTICK_AXIS =>
                    Log_Print( "Got joystick axis: stick=" & event.joystick.stick'Img &
                               " axis=" & event.joystick.axis'Img &
                               " pos=" & event.joystick.pos'Img );

                when others => null;
            end case;
        end loop;

     Al_Destroy_User_Event_Source( fake_src );
     Delete( fake_src );
     Al_Destroy_Event_Queue( queue );

     Log_Print( "Done." );
     Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Inject_Events;
