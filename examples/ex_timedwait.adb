
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

--
--    Example program for the Allegro library, by Peter Wang.
--
--    Test timed version of al_wait_for_event().
--
package body Ex_Timedwait is

    procedure Ada_Main is

        ------------------------------------------------------------------------

        procedure Test_Relative_Timeout( queue : A_Allegro_Event_Queue ) is
            event : aliased Allegro_Event;
            shade : Float := 0.1;
        begin
            loop
                if Al_Wait_For_Event_Timed( queue, event'Access, 0.1 ) then
                    if event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            return;
                        else
                            shade := 0.1;
                        end if;
                    end if;
                else
                   -- timed out
                   shade := shade + 0.1;
                   if shade > 1.0 then
                       shade := 1.0;
                   end if;
                end if;

                Al_Clear_To_Color( Al_Map_RGBA_f( 0.5 * shade, 0.25 * shade, shade, 0.0 ) );
                Al_Flip_Display;
            end loop;
        end Test_Relative_Timeout;

        ------------------------------------------------------------------------

        procedure Test_Absolute_Timeout( queue : A_Allegro_Event_Queue ) is
            timeout : Allegro_Timeout;
            event   : aliased Allegro_Event;
            shade   : Float := 0.1;
            ret     : Boolean;
        begin
            loop
                Al_Init_Timeout( timeout, 0.1 );
                ret := Al_Wait_For_Event_Until( queue, event'Access, timeout );
                while ret loop
                    if event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            return;
                        else
                            shade := 0.1;
                        end if;
                    end if;
                    ret := Al_Wait_For_Event_Until( queue, event'Access, timeout );
                end loop;

                if not ret then
                    -- timed out
                    shade := shade + 0.1;
                    if shade > 1.0 then
                        shade := 1.0;
                    end if;
                end if;

                Al_Clear_To_Color( Al_Map_RGBA_f( shade, 0.5 * shade, 0.25 * shade, 0.0 ) );
                Al_Flip_Display;
            end loop;
        end Test_Absolute_Timeout;

        ------------------------------------------------------------------------

        dpy   : A_Allegro_Display;
        queue : A_Allegro_Event_Queue;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;

        dpy := Al_Create_Display( 640, 480 );
        if dpy = null then
            Abort_Example( "Unable to set any graphic mode" );
        end if;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        Test_Relative_Timeout( queue );
        Test_Absolute_Timeout( queue );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Timedwait;
