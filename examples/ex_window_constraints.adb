
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

-- Test program for Allegro.
--
-- Constrains the window to a minimum and or maximum.
--
package body Ex_Window_Constraints is

    procedure Ada_Main is
        display : A_Allegro_Display;
        bmp     : A_Allegro_Bitmap;
        f       : A_Allegro_Font;
        queue   : A_Allegro_Event_Queue;
        event   : Allegro_Event;
        redraw  : Boolean;

        min_w   : constant Integer := 640;
        min_h   : constant Integer := 480;
        max_w   : constant Integer := 800;
        max_h   : constant Integer := 600;
        ret_min_w, ret_min_h,
        ret_max_w, ret_max_h       : Integer;
        constr_min_w, constr_min_h,
        constr_max_w, constr_max_h : Boolean := True;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init IIO addon" );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE or ALLEGRO_GENERATE_EXPOSE_EVENTS );
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Unable to set any graphic mode" );
        end if;

        bmp := Al_Load_Bitmap( "data/mysha.pcx" );
        if bmp = null then
            Abort_Example( "Unable to load image" );
        end if;

        f := Al_Load_Font( "data/a4_font.tga", 0, 0 );
        if f = null then
            Abort_Example( "Failed to load a4_font.tga" );
        end if;

        if not Al_Set_Window_Constraints( display, min_w, min_h, max_w, max_h ) then
            Abort_Example( "Unable to set window constraints." );
        end if;

        Al_Apply_Window_Constraints( display, True );

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );

        redraw := True;
        loop
            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                Al_Clear_To_Color( Al_Map_RGB( 255, 0, 0 ) );
                Al_Draw_Scaled_Bitmap( bmp,
                                       0.0, 0.0, Float(Al_Get_Bitmap_Width( bmp )), Float(Al_Get_Bitmap_Height( bmp )),
                                       0.0, 0.0, Float(Al_Get_Display_Width( display )), Float(Al_Get_Display_Height( display )),
                                       0);

                -- Display screen resolution
                Al_Draw_Text( f, Al_Map_RGB( 255, 255, 255 ), 0.0, 0.0, 0,
                              "Resolution:" & Al_Get_Display_Width( display )'Img & " x" &
                              Al_Get_Display_Height( display )'Img );

                Al_Get_Window_Constraints( display, ret_min_w, ret_min_h,
                                           ret_max_w, ret_max_h );

                Al_Draw_Text( f, Al_Map_RGB( 255, 255, 255 ), 0.0,
                              Float(Al_Get_Font_Line_Height( f )), 0,
                              "Min Width:" & ret_min_w'Img & ", Min Height:" & ret_min_h'Img &
                              ", Max Width:" & ret_max_w'Img & ", Max Height:" & ret_max_h'Img );

                Al_Draw_Text( f, Al_Map_RGB( 255, 255, 255 ), 0.0,
                              Float(Al_Get_Font_Line_Height(f) * 2), 0,
                              "Toggle Restriction: Min Width: Z, Min Height: X, Max Width: C, Max Height: V" );

                Al_Flip_Display;
                redraw := False;
            end if;

            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_RESIZE then
                al_acknowledge_resize(event.display.source);
                redraw := True;
            end if;
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_EXPOSE then
                redraw := True;
            end if;
            if event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                elsif event.keyboard.keycode = ALLEGRO_KEY_Z then
                    constr_min_w := not constr_min_w;
                elsif event.keyboard.keycode = ALLEGRO_KEY_X then
                    constr_min_h := not constr_min_h;
                elsif event.keyboard.keycode = ALLEGRO_KEY_C then
                    constr_max_w := not constr_max_w;
                elsif event.keyboard.keycode = ALLEGRO_KEY_V then
                    constr_max_h := not constr_max_h;
                end if;

                redraw := True;

                if not Al_Set_Window_Constraints( display,
                                                  (if constr_min_w then min_w else 0),
                                                  (if constr_min_h then min_h else 0),
                                                  (if constr_max_w then max_w else 0),
                                                  (if constr_max_h then max_h else 0) )
                then
                    Abort_Example( "Unable to set window constraints." );
                end if;
                Al_Apply_Window_Constraints( display, True );
            end if;
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            end if;
        end loop;

        Al_Destroy_Bitmap( bmp );
        Al_Destroy_Display( display );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Window_Constraints;
