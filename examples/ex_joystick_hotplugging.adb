
with Allegro;                           use Allegro;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Joystick;                  use Allegro.Joystick;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System.Address_Image;

package body Ex_Joystick_Hotplugging is

    procedure Print_Joystick_Info( joy : A_Allegro_Joystick ) is
        n : Integer;
    begin
        if joy = null then
            return;
        end if;

        Log_Print( "Joystick: '" & Al_Get_Joystick_Name( joy ) & "'" );

        Log_Put( "  Buttons:" );
        n := Al_Get_Joystick_Num_Buttons( joy );
        for i in 0..n-1 loop
            Log_Put( " '" & Al_Get_Joystick_Button_Name( joy, i ) & "'" );
        end loop;
        Log_Print( "" );

        n := Al_Get_Joystick_Num_Sticks( joy );
        for i in 0..n-1 loop
            Log_Print( "  Stick" & i'Img & ": '" & Al_Get_Joystick_Stick_Name( joy, i ) & "'" );

            for a in 0..Al_Get_Joystick_Num_Axes( joy, i )-1 loop
                Log_Print( "    Axis" & a'Img & ": '" & Al_Get_Joystick_Axis_Name( joy, i, a ) & "'" );
            end loop;
        end loop;
    end Print_Joystick_Info;

    ----------------------------------------------------------------------------

    procedure Draw( curr_joy : A_Allegro_Joystick ) is
        x        : constant := 100.0;
        y        : constant := 100.0;
        joystate : Allegro_Joystick_State;
    begin
        Al_Clear_To_Color( Al_Map_RGB( 0, 0, 0 ) );

        if curr_joy /= null then
            Al_Get_Joystick_State( curr_joy, joystate );
            for i in 0..Al_Get_Joystick_Num_Sticks(curr_joy)-1 loop
                Al_Draw_Filled_Circle(
                    x + joystate.stick(i).axis(0) * 20.0 + Float(i) * 80.0,
                    y + joystate.stick(i).axis(1) * 20.0,
                    20.0, Al_Map_RGB( 255, 255, 255 )
                );
            end loop;
            for i in 0..Al_Get_Joystick_Num_Buttons( curr_joy )-1 loop
                if joystate.button(i) /= 0 then
                    Al_Draw_Filled_Circle(
                          Float(i) * 20.0 + 10.0, 400.0, 9.0, Al_Map_RGB( 255, 255, 255 )
                    );
                end if;
            end loop;
        end if;

        Al_Flip_Display;
    end Draw;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        num_joysticks : Integer := 0;
        queue         : A_Allegro_Event_Queue;
        curr_joy      : A_Allegro_Joystick;
        display       : A_Allegro_Display;
        event         : aliased Allegro_Event;
        n             : Integer;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Install_Joystick then
            Abort_Example( "Could not init joysticks." );
        end if;

        if not Al_Install_Keyboard then
            Abort_Example( "al_install_keyboard failed" );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;

        Open_Log;

        display := Al_Create_Display( 640, 480 );
        if display = null then
           Abort_Example( "Could not create display." );
        end if;

        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Joystick_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        num_joysticks := Al_Get_Num_Joysticks;
        Log_Print( "Num joysticks:" & num_joysticks'Img );

        if num_joysticks > 0 then
            curr_joy := Al_Get_Joystick( 0 );
            Print_Joystick_Info( curr_joy );
        else
            curr_joy := null;
        end if;

        Draw( curr_joy );

        loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_KEY_DOWN and then
                  event.keyboard.keycode = ALLEGRO_KEY_ESCAPE
            then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                n := event.keyboard.unichar - Character'Pos( '0' );
                if n >= 0 and n < num_joysticks then
                    curr_joy := Al_Get_Joystick( n );
                    Log_Print( "switching to joystick" & n'Img );
                    Print_Joystick_Info( curr_joy );
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_JOYSTICK_CONFIGURATION then
                if Al_Reconfigure_Joysticks then
                    null;
                end if;
                num_joysticks := Al_Get_Num_Joysticks;
                Log_Print( "after reconfiguration num joysticks =" & num_joysticks'Img );
                if curr_joy /= null then
                    Log_Print( "current joystick is: " & (if Al_Get_Joystick_Active(curr_joy) then "active" else "inactive") );
                end if;
                curr_joy := Al_Get_Joystick( 0 );
            elsif event.any.typ = ALLEGRO_EVENT_JOYSTICK_AXIS then
                Log_Print( "axis event from " & System.Address_Image( event.joystick.id.all'Address ) & ", stick" & event.joystick.stick'Img & ", axis" & event.joystick.axis'Img );
            elsif event.any.typ = ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN then
               Log_Print( "button down event" & event.joystick.button'Img & " from " & System.Address_Image( event.joystick.id.all'Address ) );
            elsif event.any.typ = ALLEGRO_EVENT_JOYSTICK_BUTTON_UP then
               Log_Print( "button up event" & event.joystick.button'Img & " from " & System.Address_Image( event.joystick.id.all'Address ) );
            end if;

            Draw( curr_joy );
        end loop;

        Close_Log( False );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Joystick_Hotplugging;
