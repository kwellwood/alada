
with Ada.Command_Line;                  use Ada.Command_Line;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Transform is

    procedure Ada_Main is

        filename         : Unbounded_String := To_Unbounded_String( "data/mysha.pcx" );
        display          : A_Allegro_Display;
        buffer,
        bitmap,
        subbitmap,
        buffer_subbitmap : A_Allegro_Bitmap;
        overlay          : A_Allegro_Bitmap;
        timer            : A_Allegro_Timer;
        queue            : A_Allegro_Event_Queue;
        event            : Allegro_Event;
        transform        : Allegro_Transform;
        software         : Boolean := False;
        redraw           : Boolean := False;
        blend            : Boolean := False;
        use_subbitmap    : Boolean := True;
        w, h             : Float;
        font             : A_Allegro_Font;
        soft_font        : A_Allegro_Font;
        t                : Long_Float;
        tint             : Allegro_Color;
    begin
        if Argument_Count = 1 then
            filename := To_Unbounded_String( Argument(1) );
        end if;

        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse." );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        Init_Platform_Specific;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        subbitmap := Al_Create_Sub_Bitmap( Al_Get_Backbuffer( display ), 50, 50, 640 - 50, 480 - 50);
        overlay := Al_Create_Sub_Bitmap( Al_Get_Backbuffer( display ), 100, 100, 300, 50);

        Al_Set_Window_Title( display, To_String( filename ) );

        bitmap := Al_Load_Bitmap( To_String( filename ) );
        if bitmap = null then
            Abort_Example( To_String( filename ) & " not found or failed to load" );
        end if;
        font := Al_Load_Font( "data/bmpfont.tga", 0, 0 );
        if font = null then
            Abort_Example( "data/bmpfont.tga not found or failed to load" );
        end if;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );
        buffer := Al_Create_Bitmap( 640, 480 );
        buffer_subbitmap := Al_Create_Sub_Bitmap( buffer, 50, 50, 640 - 50, 480 - 50 );

        soft_font := Al_Load_Font( "data/bmpfont.tga", 0, 0 );
        if soft_font = null then
            Abort_Example( "data/bmpfont.tga not found or failed to load" );
        end if;

        timer := Al_Create_Timer( 1.0 / 60.0 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer );

        w := Float(Al_Get_Bitmap_Width( bitmap ));
        h := Float(Al_Get_Bitmap_Height( bitmap ));

        Al_Set_Target_Bitmap( overlay );
        Al_Identity_Transform( transform );
        Al_Rotate_Transform( transform, -0.06 );
        Al_Use_Transform( transform );

        loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                if event.keyboard.keycode = ALLEGRO_KEY_S then
                    software := not software;
                    if software then
                        -- Restore identity transform on display bitmap.
                        declare
                            identity : Allegro_Transform;
                        begin
                            Al_Identity_Transform( identity );
                            Al_Use_Transform( identity );
                        end;
                    end if;
                elsif event.keyboard.keycode = ALLEGRO_KEY_L then
                    blend := not blend;
                elsif event.keyboard.keycode = ALLEGRO_KEY_B then
                    use_subbitmap := not use_subbitmap;
                elsif event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := True;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                t := 3.0 + Al_Get_Time;
                redraw := False;

                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                if blend then
                    tint := Al_Map_RGBA_f( 0.5, 0.5, 0.5, 0.5 );
                else
                    tint := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
                end if;

                if software then
                    if use_subbitmap then
                        Al_Set_Target_Bitmap( buffer );
                        Al_Clear_To_Color( Al_Map_RGB_f( 1.0, 0.0, 0.0 ) );
                        Al_Set_Target_Bitmap( buffer_subbitmap );
                    else
                        Al_Set_Target_Bitmap( buffer );
                    end if;
                else
                    if use_subbitmap then
                        Al_Set_Target_Backbuffer( display );
                        Al_Clear_To_Color( Al_Map_RGB_f( 1.0, 0.0, 0.0 ) );
                        Al_Set_Target_Bitmap( subbitmap );
                    else
                        Al_Set_Target_Backbuffer( display );
                    end if;
                end if;

                -- Set the transformation on the target bitmap.
                Al_Identity_Transform( transform );
                Al_Translate_Transform( transform, -640.0 / 2.0, -480.0 / 2.0 );
                Al_Scale_Transform( transform, 0.15 + sin( Float(t) / 5.0 ), 0.15 + cos( Float(t) / 5.0 ) );
                Al_Rotate_Transform( transform, Float(t) / 50.0 );
                Al_Translate_Transform( transform, 640.0 / 2.0, 480.0 / 2.0 );
                Al_Use_Transform( transform );

                -- Draw some stuff
                Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );
                Al_Draw_Tinted_Bitmap( bitmap, tint, 0.0, 0.0, 0 );
                Al_Draw_Tinted_Scaled_Bitmap( bitmap, tint, w / 4.0,
                                              h / 4.0, w / 2.0,
                                              h / 2.0, w, 0.0,
                                              w / 2.0, h / 4.0, 0 );
                Al_Draw_Tinted_Bitmap_Region( bitmap, tint, w / 4.0,
                                              h / 4.0, w / 2.0,
                                              h / 2.0, 0.0, h,
                                              ALLEGRO_FLIP_VERTICAL );
                Al_Draw_Tinted_Scaled_Rotated_Bitmap( bitmap, tint, w / 2.0,
                                                      h / 2.0,
                                                      w + w / 2.0,
                                                      h + h / 2.0, 0.7,
                                                      0.7, 0.3, 0 );
                Al_Draw_Pixel( w + w / 2.0, h + h / 2.0, Al_Map_RGB_f( 0.0, 1.0, 0.0 ) );
                Al_Put_Pixel( Integer(w + w / 2.0 + 2.0), Integer(h + h / 2.0 + 2.0), Al_Map_RGB_f( 0.0, 1.0, 1.0 ) );
                Al_Draw_Circle( w, h, 50.0, Al_Map_RGB_f( 1.0, 0.5, 0.0 ), 3.0 );

                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ZERO );
                if software then
                   Al_Draw_Text( soft_font, Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 ),
                                 640.0 / 2.0, 430.0, ALLEGRO_ALIGN_CENTRE,
                                 "Software Rendering" );
                   Al_Set_Target_Backbuffer( display );
                   Al_Draw_Bitmap( buffer, 0.0, 0.0, 0 );
                else
                   Al_Draw_Text( font, Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 ),
                                 640.0 / 2.0, 430.0, ALLEGRO_ALIGN_CENTRE,
                                 "Hardware Rendering" );
                end if;

                -- Each target bitmap has its own transformation matrix, so this
                -- overlay is unaffected by the transformations set earlier.
                --
                Al_Set_Target_Bitmap( overlay );
                Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_ONE );
                Al_Draw_Text( font, Al_Map_RGBA_f( 1.0, 1.0, 0.0, 1.0 ), 0.0, 10.0, ALLEGRO_ALIGN_LEFT, "hello!" );

                Al_Set_Target_Backbuffer( display );
                Al_Flip_Display;
            end if;
        end loop;

        Al_Destroy_Bitmap( bitmap );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Transform;
