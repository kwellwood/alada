
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Shaders;                   use Allegro.Shaders;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Shader_Multitex is

    type Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

    function Load_Bitmap( filename : String ) return A_Allegro_Bitmap is
        bitmap : constant A_Allegro_Bitmap := Al_Load_Bitmap( filename );
    begin
        if bitmap = null then
            Abort_Example( filename & " not found or failed to load" );
        end if;
       return bitmap;
    end Load_Bitmap;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        display    : A_Allegro_Display;
        timer      : A_Allegro_Timer;
        queue      : A_Allegro_Event_Queue;
        bitmap     : Bitmap_Array(0..1);
        redraw     : Boolean := True;
        shader     : A_Allegro_Shader;
        t          : Float := 0.0;
        pixel_file : Unbounded_String;
        event      : Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard." );
        end if;
        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init image addon." );
        end if;
        Init_Platform_Specific;

        Al_Set_New_Bitmap_Flags( ALLEGRO_MIN_LINEAR or ALLEGRO_MAG_LINEAR or ALLEGRO_MIPMAP );
        Al_Set_New_Display_Option( ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST );
        Al_Set_New_Display_Option( ALLEGRO_SAMPLES, 4, ALLEGRO_SUGGEST );
        Al_Set_New_Display_Flags( ALLEGRO_PROGRAMMABLE_PIPELINE );
        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;

        bitmap(0) := Load_Bitmap( "data/mysha.pcx" );
        bitmap(1) := Load_Bitmap( "data/obp.jpg" );

        -- Create the shader.
        shader := Al_Create_Shader( ALLEGRO_SHADER_AUTO );
        if shader = null then
            Abort_Example( "Could not create shader." );
        end if;

        if Al_Get_Shader_Platform( shader ) = ALLEGRO_SHADER_GLSL then
            pixel_file := To_Unbounded_String( "data/ex_shader_multitex_pixel.glsl" );
        else
            pixel_file := To_Unbounded_String( "data/ex_shader_multitex_pixel.hlsl" );
        end if;

        if not Al_Attach_Shader_Source( shader, ALLEGRO_VERTEX_SHADER, Al_Get_Default_Shader_Source( ALLEGRO_SHADER_AUTO, ALLEGRO_VERTEX_SHADER ) ) then
            Abort_Example( "al_attach_shader_source for vertex shader failed: " & Al_Get_Shader_Log( shader ) );
        end if;
        if not Al_Attach_Shader_Source_File( shader, ALLEGRO_PIXEL_SHADER, To_String( pixel_file ) ) then
            Abort_Example( "al_attach_shader_source_file for pixel shader failed: " & Al_Get_Shader_Log( shader ) );
        end if;
        if not Al_Build_Shader( shader ) then
            Abort_Example( "al_build_shader failed: " & Al_Get_Shader_Log( shader ) );
        end if;

        if not Al_Use_Shader( shader ) then
            Abort_Example( "Using shader failed" );
        end if;

        timer := Al_Create_Timer( 1.0 / 60.0 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( queue, event );
            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_CHAR then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := True;
                t := t + 1.0;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                declare
                    dw, dh : Float;
                    scale  : constant Float := 1.0 + 100.0 * (1.0 + Sin( t * ALLEGRO_PI * 2.0 / 60.0 / 10.0 ));
                    angle  : constant Float := ALLEGRO_PI * 2.0 * t / 60.0 / 15.0;
                    x      : constant Float := 120.0 - 20.0 * Cos( ALLEGRO_PI * 2.0 * t / 60.0 / 25.0 );
                    y      : constant Float := 120.0 - 20.0 * Sin( ALLEGRO_PI * 2.0 * t / 60.0 / 25.0 );
                begin
                    dw := Float(Al_Get_Display_Width( display ));
                    dh := Float(Al_Get_Display_Height( display ));

                    redraw := False;
                    Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.0 ) );

                    -- We set a second bitmap for texture unit 1. Unit 0 will have
                    -- the normal texture which al_draw_*_bitmap will set up for us.
                    -- We then draw the bitmap like normal, except it will use the
                    -- custom shader.
                    --
                    Al_Set_Shader_Sampler( "tex2", bitmap(1), 1 );
                    Al_Draw_Scaled_Rotated_Bitmap( bitmap(0), x, y, dw / 2.0, dh / 2.0, scale, scale, angle, 0 );

                    Al_Flip_Display;
                end;
            end if;
        end loop;

        Al_Use_Shader( null );

	Al_Destroy_Bitmap( bitmap(0) );
	Al_Destroy_Bitmap( bitmap(1) );
        Al_Destroy_Shader( shader );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Shader_Multitex;
