
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;
with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
                                        use Allegro.Drawing.Primitives.Locked_Vertex_Arrays;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Transformations;           use Allegro.Transformations;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;

package body Ex_Vertex_Buffer is

    -- An example comparing different ways of drawing polygons, including usage of
    -- vertex buffers.

    FPS : constant := 60.0;
    FRAME_TAU : constant := 60.0;
    NUM_VERTICES : constant := 4096;

    type Method is
        record
            x, y          : Float := 0.0;
            vbuff         : A_Allegro_Vertex_Buffer;
            vertices      : A_Allegro_Vertex_Array;
            name          : Unbounded_String;
            flags         : Unsigned_32;
            frame_average : Float := 0.0;
        end record;

    type Method_Array is array (Integer range <>) of Method;

    ----------------------------------------------------------------------------

    function Get_Color( ii : Integer ) return Allegro_Color is
        frac : constant Float := Float(ii) / FLOAT(NUM_VERTICES);
        t    : constant Long_Float := Al_Get_Time;

        function Theta( period : Float ) return Float is
        begin
            return ((Float(t) / period + frac) * 2.0 * ALLEGRO_PI);
        end Theta;

    begin
        return Al_Map_RGB_f( Sin( Theta( 5.0 ) ) / 2.0 + 1.0,
                             Cos( Theta( 1.1 ) ) / 2.0 + 1.0,
                             Sin( Theta( 3.4 ) / 2.0 ) / 2.0 + 1.0 );
    end Get_Color;

    ----------------------------------------------------------------------------

    procedure Draw_Method( md           : in out Method;
                           font         : A_Allegro_Font;
                           new_vertices : A_Allegro_Vertex_Array ) is
        t          : Allegro_Transform;
        start_time : Long_Float;
        new_fps    : Float;
        c          : Allegro_Color;
        pragma Warnings( Off, c );
    begin
        Al_Identity_Transform( t );
        Al_Translate_Transform( t, md.x, md.y );
        Al_Use_Transform( t );

        Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 0.0, -50.0,
                      ALLEGRO_ALIGN_CENTRE, To_String( md.name ) &
                      (if (md.flags and ALLEGRO_PRIM_BUFFER_READWRITE) /= 0 then "+read/write" else "+write-only") );

        start_time := Al_Get_Time;
        if md.vbuff /= null then
            if new_vertices /= null then
                declare
                    lock_mem : Locked_Vertex_Ptr;
                begin
                    lock_mem := Al_Lock_Vertex_Buffer( md.vbuff, 0, NUM_VERTICES, ALLEGRO_LOCK_WRITEONLY );
                    for i in new_vertices'Range loop
                        lock_mem.all := new_vertices(i);
                        Increment( lock_mem );
                    end loop;
                    Al_Unlock_Vertex_Buffer( md.vbuff );
                end;
            end if;
            Al_Draw_Vertex_Buffer( md.vbuff, 0, NUM_VERTICES, ALLEGRO_PRIM_TRIANGLE_STRIP );
        elsif md.vertices /= null then
            Al_Draw_Prim( md.vertices.all, null, ALLEGRO_PRIM_TRIANGLE_STRIP );
        end if;

        -- Force the completion of the previous commands by reading from screen
        c := Al_Get_Pixel( Al_Get_Backbuffer( Al_Get_Current_Display ), 0, 0 );

        new_fps := Float(1.0 / (Al_Get_Time - start_time));
        md.frame_average := md.frame_average + (new_fps - md.frame_average) / FRAME_TAU;

        if md.vbuff /= null or else md.vertices /= null then
            Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 0.0, 0.0 ), 0.0, 0.0, ALLEGRO_ALIGN_CENTRE,
                          Integer'Image( Integer(md.frame_average) ) & " FPS" );
        else
            Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 0.0, 0.0 ), 0.0, 0.0, ALLEGRO_ALIGN_CENTRE, "N/A" );
        end if;

        Al_Identity_Transform( t );
        Al_Use_Transform( t );
    end Draw_Method;

    ----------------------------------------------------------------------------

    procedure Ada_Main is
        timer           : A_Allegro_Timer;
        queue           : A_Allegro_Event_Queue;
        Display         : A_Allegro_Display;
        font            : A_Allegro_Font;
        w               : Integer := 640;
        h               : Integer := 480;
        done            : Boolean := False;
        need_redraw     : Boolean := True;
        background      : Boolean := False;
        dynamic_buffers : Boolean := False;
        num_x, num_y    : constant Integer := 3;
        spacing_x       : constant Integer := 200;
        spacing_y       : constant Integer := 150;
        vertices        : A_Allegro_Vertex_Array;
    begin
        if not Al_Initialize then
            Abort_Example( "Failed to init Allegro." );
        end if;

        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon." );
        end if;

        Al_Set_New_Display_Option( ALLEGRO_SUPPORTED_ORIENTATIONS,
                                   ALLEGRO_DISPLAY_ORIENTATION_ALL, ALLEGRO_SUGGEST );
        display := Al_Create_Display( w, h );
        if display = null then
            Abort_Example( "Error creating display." );
        end if;

        w := Al_Get_Display_Width( display );
        h := Al_Get_Display_Height( display );

        if not Al_Install_Keyboard then
            Abort_Example( "Error installing keyboard." );
        end if;

        font := Al_Create_Builtin_Font;
        timer := Al_Create_Timer( 1.0 / FPS );
        queue := Al_Create_Event_Queue;

        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );

        vertices := new Allegro_Vertex_Array(0..NUM_VERTICES-1);
        Al_Calculate_Arc( vertices(vertices'First).x'Address, Allegro_Vertex'Size / 8, 0.0, 0.0, 80.0, 30.0, 0.0, 2.0 * ALLEGRO_PI, 10.0, NUM_VERTICES / 2 );
        for ii in vertices'Range loop
            vertices(ii).z := 0.0;
            vertices(ii).color := Get_Color( ii );
        end loop;

        declare
            function GetX( n : Integer ) return Float is (Float((w - (num_x - 1) * spacing_x) / 2 + n * spacing_x));
            function GetY( n : Integer ) return Float is (Float((h - (num_y - 1) * spacing_y) / 2 + n * spacing_y));

            methods : Method_Array := (
                (GetX( 1 ), GetY( 0 ), null, vertices, To_Unbounded_String( "No buffer" ), 0, 0.0),

                (GetX( 0 ), GetY( 1 ), null, null, To_Unbounded_String( "STREAM" ), ALLEGRO_PRIM_BUFFER_STREAM or ALLEGRO_PRIM_BUFFER_READWRITE, 0.0),
                (GetX( 1 ), GetY( 1 ), null, null, To_Unbounded_String( "STATIC" ), ALLEGRO_PRIM_BUFFER_STATIC or ALLEGRO_PRIM_BUFFER_READWRITE, 0.0),
                (GetX( 2 ), GetY( 1 ), null, null, To_Unbounded_String( "DYNAMIC" ), ALLEGRO_PRIM_BUFFER_DYNAMIC or ALLEGRO_PRIM_BUFFER_READWRITE, 0.0),

                (GetX( 0 ), GetY( 2 ), null, null, To_Unbounded_String( "STREAM" ), ALLEGRO_PRIM_BUFFER_STREAM, 0.0),
                (GetX( 1 ), GetY( 2 ), null, null, To_Unbounded_String( "STATIC" ), ALLEGRO_PRIM_BUFFER_STATIC, 0.0),
                (GetX( 2 ), GetY( 2 ), null, null, To_Unbounded_String( "DYNAMIC" ), ALLEGRO_PRIM_BUFFER_DYNAMIC, 0.0)
            );

            event : Allegro_Event;
        begin
            for ii in methods'Range loop
                methods(ii).frame_average := 1.0;
                if methods(ii).vertices = null then
                    methods(ii).vbuff := Al_Create_Vertex_Buffer( vertices.all, methods(ii).flags );
                end if;
            end loop;

            Al_Start_Timer( timer );

            while not done loop
                if not background and then need_redraw and then Al_Is_Event_Queue_Empty( queue ) then
                    Al_Clear_To_Color( Al_Map_RGB_f( 0.0, 0.0, 0.2 ) );

                    for ii in methods'Range loop
                        Draw_Method( methods(ii), font, (if dynamic_buffers then vertices else null) );
                    end loop;

                    Al_Draw_Text( font, Al_Map_RGB_f( 1.0, 1.0, 1.0 ), 10.0, 10.0, 0,
                                  "Dynamic (D): " & dynamic_buffers'Img );

                    Al_Flip_Display;
                    need_redraw := False;
                end if;

                Al_Wait_For_Event( queue, event );
                case event.any.typ is
                    when ALLEGRO_EVENT_KEY_DOWN =>
                        if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                            done := True;
                        elsif event.keyboard.keycode = ALLEGRO_KEY_D then
                            dynamic_buffers := not dynamic_buffers;
                        end if;
                    when ALLEGRO_EVENT_DISPLAY_CLOSE =>
                        done := True;
                    when ALLEGRO_EVENT_DISPLAY_HALT_DRAWING =>
                        background := True;
                        Al_Acknowledge_Drawing_Halt( event.display.source );
                    when ALLEGRO_EVENT_DISPLAY_RESIZE =>
                        Al_Acknowledge_Resize( event.display.source );
                    when ALLEGRO_EVENT_DISPLAY_RESUME_DRAWING =>
                        background := False;
                    when ALLEGRO_EVENT_TIMER =>
                        for ii in vertices'Range loop
                            vertices(ii).color := Get_Color( ii );
                        end loop;
                        need_redraw := True;
                    when others => null;
                end case;
            end loop;

            for ii in methods'Range loop
                Al_Destroy_Vertex_Buffer( methods(ii).vbuff );
            end loop;

            Al_Destroy_Font( font );
            Delete( vertices );
        end;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Vertex_Buffer;
