
with Ada.Command_Line;                  use Ada.Command_Line;
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Image_IO;          use Allegro.Bitmaps.Image_IO;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Image_Addon;               use Allegro.Image_Addon;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Common;                            use Common;

-- Image conversion example
package body Ex_Convert is

    procedure Ada_Main is
        bitmap : A_Allegro_Bitmap;
        t0, t1 : Long_Float;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;

        Open_Log;

        if Argument_Count < 2 then
            Log_Print( "This example needs to be run from the command line." );
            Log_Print( "Usage: ex_convert <infile> <outfile>" );
            Log_Print( "    Possible file types: BMP PCX PNG TGA" );
            Close_Log( True );
            return;
        end if;

        if not Al_Init_Image_Addon then
            Abort_Example( "Could not init Allegro IIO addon." );
        end if;

        Al_Set_New_Bitmap_Format( ALLEGRO_PIXEL_FORMAT_ARGB_8888 );
        Al_Set_New_Bitmap_Flags( ALLEGRO_MEMORY_BITMAP );

        bitmap := Al_Load_Bitmap_Flags( Argument(1), ALLEGRO_NO_PREMULTIPLIED_ALPHA );
        if bitmap = null then
            Log_Print( "Error loading input file" );
            Close_Log( True );
            return;
        end if;

        t0 := Al_Get_Time;
        if not Al_Save_Bitmap( Argument(2), bitmap ) then
            Log_Print( "Error saving bitmap" );
            Close_Log( True );
            return;
        end if;
        t1 := Al_Get_Time;
        Log_Print( "Saving took" & Integer'Image( Integer( (t1 - t0) * 1000.0 ) ) &
                   " milliseconds" );

        Al_Destroy_Bitmap( bitmap );

        Close_Log( True );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Convert;
