
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Drawing;           use Allegro.Bitmaps.Drawing;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Fonts;                     use Allegro.Fonts;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.System;                    use Allegro.System;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;
with System;                            use System;
with System.Storage_Elements;           use System.Storage_Elements;

package body Ex_Premulalpha is

    procedure Ada_Main is
        display : A_Allegro_Display;
        tex1,
        tex2    : A_Allegro_Bitmap;
        timer   : A_Allegro_Timer;
        queue   : A_Allegro_Event_Queue;
        redraw  : Boolean := True;
        lock    : A_Allegro_Locked_Region;
        font    : A_Allegro_Font;
        event   : aliased Allegro_Event;
    begin
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Font_Addon then
            Abort_Example( "Could not init Allegro font addon." );
        end if;

        if not Al_Install_Mouse then
            Abort_Example( "Could not install mouse" );
        end if;
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;

        display := Al_Create_Display( 640, 480 );
        if display = null then
            Abort_Example( "Error creating display" );
        end if;

        font := Al_Create_Builtin_Font;

        tex1 := Al_Create_Bitmap( 8, 8 );
        lock := Al_Lock_Bitmap( tex1, ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE, ALLEGRO_LOCK_WRITEONLY );
        declare
            p    : Address := lock.data;
            lp   : Address;
        begin
            for y in 0..7 loop
                lp := p;
                for x in 0..7 loop
                    declare
                        pixel : Storage_Array(0..3);
                        for pixel'Address use p;
                    begin
                        if x = 0 or else y = 0 or else x = 7 or else y = 7 then
                            pixel(0) := 0;
                            pixel(1) := 0;
                            pixel(2) := 0;
                            pixel(3) := 0;
                        else
                            pixel(0) := 0;
                            pixel(1) := 255;
                            pixel(2) := 0;
                            pixel(3) := 255;
                        end if;
                    end;
                    p := p + Storage_Offset(4);
                end loop;
                p := lp + Storage_Offset(lock.pitch);
            end loop;
        end;
        Al_Unlock_Bitmap( tex1 );

        Al_Set_New_Bitmap_Flags( ALLEGRO_MAG_LINEAR );
        tex2 := Al_Clone_Bitmap( tex1 );

        timer := Al_Create_Timer( 1.0 / 30.0 );
        queue := Al_Create_Event_Queue;
        Al_Register_Event_Source( queue, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( queue, Al_Get_Display_Event_Source( display ) );
        Al_Register_Event_Source( queue, Al_Get_Timer_Event_Source( timer ) );
        Al_Start_Timer( timer );

        loop
            Al_Wait_For_Event( queue, event );

            if event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                if event.keyboard.keycode = ALLEGRO_KEY_ESCAPE then
                    exit;
                end if;
            elsif event.any.typ = ALLEGRO_EVENT_TIMER then
                redraw := True;
            end if;

            if redraw and then Al_Is_Event_Queue_Empty( queue ) then
                declare
                    th     : constant Float := Float(Al_Get_Font_Line_Height( font ));
                    color  : constant Allegro_Color := Al_Map_RGB_f( 0.0, 0.0, 0.0 );
                    color2 : constant Allegro_Color := Al_Map_RGB_f( 1.0, 0.0, 0.0 );
                    color3 : constant Allegro_Color := Al_Map_RGB_f( 0.0, 0.5, 0.0 );
                    x      : constant Float := 8.0;
                    y      : Float := 60.0;
                    a      : Float;
                    t      : Long_Float := Al_Get_Time;
                begin
                    t := t / 10.0;
                    a := Float(t - Long_Float'Floor( t ));
                    a := a * 2.0 * ALLEGRO_PI;

                    redraw := False;
                    Al_Clear_To_Color( Al_Map_RGB_f( 0.5, 0.6, 1.0 ) );

                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Text( font, color, x, y, 0, "not premultiplied" );
                    Al_Draw_Text( font, color, x, y + th, 0, "no filtering" );
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Scaled_Rotated_Bitmap( tex1, 4.0, 4.0, x + 320.0, y, 8.0, 8.0, a, 0 );

                    y := y + 120.0;

                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Text( font, color, x, y, 0, "not premultiplied" );
                    Al_Draw_Text( font, color, x, y + th, 0, "mag linear filtering" );
                    Al_Draw_Text( font, color2, x + 400.0, y, 0, "wrong dark border" );
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Scaled_Rotated_Bitmap( tex2, 4.0, 4.0, x + 320.0, y, 8.0, 8.0, a, 0 );

                    y := y + 120.0;

                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Text( font, color, x, y, 0, "premultiplied alpha" );
                    Al_Draw_Text( font, color, x, y + th, 0, "no filtering" );
                    Al_Draw_Text( font, color, x + 400.0, y, 0, "no difference" );
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Scaled_Rotated_Bitmap( tex1, 4.0, 4.0, x + 320.0, y, 8.0, 8.0, a, 0 );

                    y := y + 120.0;

                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Text( font, color, x, y, 0, "premultiplied alpha" );
                    Al_Draw_Text( font, color, x, y + th, 0, "mag linear filtering" );
                    Al_Draw_Text( font, color3, x + 400.0, y, 0, "correct color" );
                    Al_Set_Blender( ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA );
                    Al_Draw_Scaled_Rotated_Bitmap( tex2, 4.0, 4.0, x + 320.0, y, 8.0, 8.0, a, 0 );

                    Al_Flip_Display;
                end;
            end if;
        end loop;

        Al_Destroy_Font( font );
        Al_Destroy_Bitmap( tex1 );
        Al_Destroy_Bitmap( tex2 );
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Premulalpha;
