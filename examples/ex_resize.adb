
with Allegro;                           use Allegro;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Drawing;                   use Allegro.Drawing;
with Allegro.Drawing.Primitives;        use Allegro.Drawing.Primitives;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.System;                    use Allegro.System;
with Allegro.Timers;                    use Allegro.Timers;
with Common;                            use Common;
with Interfaces;                        use Interfaces;

package body Ex_Resize is

    procedure Ada_Main is

        procedure Redraw is
            black,
            white : Allegro_Color;
            w, h  : FLoat;
        begin
            white := Al_Map_RGBA_f( 1.0, 1.0, 1.0, 1.0 );
            black := Al_Map_RGBA_f( 0.0, 0.0, 0.0, 1.0 );

            Al_Clear_To_Color( white );
            w := Float(Al_Get_Bitmap_Width( Al_Get_Target_Bitmap ));
            h := Float(Al_Get_Bitmap_Height( Al_Get_Target_Bitmap ));
            Al_Draw_Line( 0.0, h, w / 2.0, 0.0, black, 0.0 );
            Al_Draw_Line( w / 2.0, 0.0, w, h, black, 0.0 );
            Al_Draw_Line( w / 4.0, h / 2.0, 3.0 * w / 4.0, h / 2.0, black, 0.0 );
            Al_Flip_Display;
        end Redraw;

        ------------------------------------------------------------------------

        display : A_Allegro_Display;
        timer   : A_Allegro_Timer;
        events  : A_Allegro_Event_Queue;
        event   : aliased Allegro_Event;
        rs, s   : Integer := 100;
        resize  : Boolean := False;
    begin
        -- Initialize Allegro and create an event queue.
        if not Al_Initialize then
            Abort_Example( "Could not init Allegro." );
        end if;
        if not Al_Init_Primitives_Addon then
            Abort_Example( "Could not init primitives addon" );
        end if;
        events := Al_Create_Event_Queue;

        -- Setup a display driver and register events from it.
        Al_Set_New_Display_Flags( ALLEGRO_RESIZABLE );
        display := Al_Create_Display( rs, rs );
        if display = null then
            Abort_Example( "Could not create display." );
        end if;
        Al_Register_Event_Source( events, Al_Get_Display_Event_Source( display ) );

        timer := Al_Create_Timer( 0.1 );
        Al_Start_Timer( timer );

        -- Setup a keyboard driver and register events from it.
        if not Al_Install_Keyboard then
            Abort_Example( "Could not install keyboard" );
        end if;
        Al_Register_Event_Source( events, Al_Get_Keyboard_Event_Source );
        Al_Register_Event_Source( events, Al_Get_Timer_Event_Source( timer ) );

        -- Display a pulsating window until a key or the closebutton is pressed.
        Redraw;
        loop
            if resize then
                rs := rs + 10;
                if rs = 300 then
                    rs := 100;
                end if;
                s := rs;
                if s > 200 then
                    s := 400 - s;
                end if;
                if Al_Resize_Display( display, s, s ) then
                    null;
                end if;
                Redraw;
                resize := False;
            end if;
            Al_Wait_For_Event( events, event );

            if event.any.typ = ALLEGRO_EVENT_TIMER then
                resize := True;
            elsif event.any.typ = ALLEGRO_EVENT_DISPLAY_CLOSE then
                exit;
            elsif event.any.typ = ALLEGRO_EVENT_KEY_DOWN then
                exit;
            end if;
        end loop;
    end Ada_Main;

begin
    Set_Ada_Main( Ada_Main'Access );
end Ex_Resize;
