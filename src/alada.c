
#include "allegro5/allegro.h"
#include "allegro5/allegro_native_dialog.h"

/* AlAda Main --------------------------------------------------------------- */

/*
 * OS X uses a macro to rename 'main' to '_al_mangled_main', which is then
 * called by the real main in AllegroMonolith-5.2. In the case of Windows, this
 * is the real application main. In both cases, this is the place where we need
 * to initialize the Ada runtime and begin the Ada application code.
 */

void adainit();
void adafinal();
void alada_main();   

extern int main(int argc, char* argv[])
{
    // store the application arguments for the GNAT runtime
    extern int    gnat_argc;
    extern char** gnat_argv;
    extern char** gnat_envp;
    gnat_argc = argc;
    gnat_argv = argv;
    gnat_envp = NULL;

    adainit();       // Ada runtime elaboration
    alada_main();    // Ada main procedure
    adafinal();      // Ada runtime finalization

    extern int gnat_exit_status;
    return gnat_exit_status;
}

/* Allegro.System ----------------------------------------------------------- */

extern bool al_initialize(void) { return al_init(); }

extern void al_append_native_text_log_s(ALLEGRO_TEXTLOG* textlog, const char* str)
{
    al_append_native_text_log(textlog, str);
}
