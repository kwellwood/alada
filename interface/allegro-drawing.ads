--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;
with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Basic drawing routines
package Allegro.Drawing is

    -- Basic routines

    procedure Al_Clear_Depth_Buffer( x : Float );
    pragma Import( C, Al_Clear_Depth_Buffer, "al_clear_depth_buffer" );

    procedure Al_Clear_To_Color( color : Allegro_Color );
    pragma Import( C, Al_Clear_To_Color, "al_clear_to_color" );

    procedure Al_Draw_Pixel( x, y : Float; color : Allegro_Color );
    pragma Import( C, Al_Draw_Pixel, "al_draw_pixel" );

    -- Render States

    type Allegro_Render_State is private;
    ALLEGRO_ALPHA_TEST       : constant Allegro_Render_State;
    ALLEGRO_WRITE_MASK       : constant Allegro_Render_State;
    ALLEGRO_DEPTH_TEST       : constant Allegro_Render_State;
    ALLEGRO_DEPTH_FUNCTION   : constant Allegro_Render_State;
    ALLEGRO_ALPHA_FUNCTION   : constant Allegro_Render_State;
    ALLEGRO_ALPHA_TEST_VALUE : constant Allegro_Render_State;

    subtype Allegro_Render_Function is Unsigned_32;
    ALLEGRO_RENDER_NEVER         : constant Allegro_Render_Function;
    ALLEGRO_RENDER_ALWAYS        : constant Allegro_Render_Function;
    ALLEGRO_RENDER_LESS          : constant Allegro_Render_Function;
    ALLEGRO_RENDER_EQUAL         : constant Allegro_Render_Function;
    ALLEGRO_RENDER_LESS_EQUAL    : constant Allegro_Render_Function;
    ALLEGRO_RENDER_GREATER       : constant Allegro_Render_Function;
    ALLEGRO_RENDER_NOT_EQUAL     : constant Allegro_Render_Function;
    ALLEGRO_RENDER_GREATER_EQUAL : constant Allegro_Render_Function;

    subtype Allegro_Write_Mask_Flags is Unsigned_32;
    ALLEGRO_MASK_RED   : constant Allegro_Write_Mask_Flags;
    ALLEGRO_MASK_GREEN : constant Allegro_Write_Mask_Flags;
    ALLEGRO_MASK_BLUE  : constant Allegro_Write_Mask_Flags;
    ALLEGRO_MASK_ALPHA : constant Allegro_Write_Mask_Flags;
    ALLEGRO_MASK_DEPTH : constant Allegro_Write_Mask_Flags;
    ALLEGRO_MASK_RGB   : constant Allegro_Write_Mask_Flags;
    ALLEGRO_MASK_RGBA  : constant Allegro_Write_Mask_Flags;

    procedure Al_Set_Render_State( state : Allegro_Render_State; value : Unsigned_32 );
    pragma Import( C, Al_Set_Render_State, "al_set_render_state" );

private

    type Allegro_Render_State is new Integer;
    ALLEGRO_ALPHA_TEST       : constant Allegro_Render_State := 16;
    ALLEGRO_WRITE_MASK       : constant Allegro_Render_State := 17;
    ALLEGRO_DEPTH_TEST       : constant Allegro_Render_State := 18;
    ALLEGRO_DEPTH_FUNCTION   : constant Allegro_Render_State := 19;
    ALLEGRO_ALPHA_FUNCTION   : constant Allegro_Render_State := 20;
    ALLEGRO_ALPHA_TEST_VALUE : constant Allegro_Render_State := 21;

    ALLEGRO_RENDER_NEVER         : constant Allegro_Render_Function := 0;
    ALLEGRO_RENDER_ALWAYS        : constant Allegro_Render_Function := 1;
    ALLEGRO_RENDER_LESS          : constant Allegro_Render_Function := 2;
    ALLEGRO_RENDER_EQUAL         : constant Allegro_Render_Function := 3;
    ALLEGRO_RENDER_LESS_EQUAL    : constant Allegro_Render_Function := 4;
    ALLEGRO_RENDER_GREATER       : constant Allegro_Render_Function := 5;
    ALLEGRO_RENDER_NOT_EQUAL     : constant Allegro_Render_Function := 6;
    ALLEGRO_RENDER_GREATER_EQUAL : constant Allegro_Render_Function := 7;

    ALLEGRO_MASK_RED   : constant Allegro_Write_Mask_Flags := 2#00001#;
    ALLEGRO_MASK_GREEN : constant Allegro_Write_Mask_Flags := 2#00010#;
    ALLEGRO_MASK_BLUE  : constant Allegro_Write_Mask_Flags := 2#00100#;
    ALLEGRO_MASK_ALPHA : constant Allegro_Write_Mask_Flags := 2#01000#;
    ALLEGRO_MASK_DEPTH : constant Allegro_Write_Mask_Flags := 2#10000#;
    ALLEGRO_MASK_RGB   : constant Allegro_Write_Mask_Flags := ALLEGRO_MASK_RED or ALLEGRO_MASK_GREEN or ALLEGRO_MASK_BLUE;
    ALLEGRO_MASK_RGBA  : constant Allegro_Write_Mask_Flags := ALLEGRO_MASK_RGB or ALLEGRO_MASK_ALPHA;

end Allegro.Drawing;
