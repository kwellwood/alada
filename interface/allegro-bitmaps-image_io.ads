--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.File_IO;                   use Allegro.File_IO;

-- Allegro 5.2.5 - Image I/O routines
package Allegro.Bitmaps.Image_IO is

    subtype Bitmap_Loader_Flags is Unsigned_32;

    ALLEGRO_KEEP_BITMAP_FORMAT     : constant Bitmap_Loader_Flags;
    ALLEGRO_NO_PREMULTIPLIED_ALPHA : constant Bitmap_Loader_Flags;
    ALLEGRO_KEEP_INDEX             : constant Bitmap_Loader_Flags;

    type Allegro_IIO_Loader_Function is access
        function( filename : String; flags : Bitmap_Loader_Flags ) return Allegro_Bitmap;
    pragma Convention( C, Allegro_IIO_Loader_Function );

    type Allegro_IIO_FS_Loader_Function is access
        function( fp : A_Allegro_File; flags : Bitmap_Loader_Flags ) return Allegro_Bitmap;
    pragma Convention( C, Allegro_IIO_FS_Loader_Function );

    type Allegro_IIO_Saver_Function is access
        function( filename : String; bitmap : A_Allegro_Bitmap ) return Bool;
    pragma Convention( C, Allegro_IIO_Saver_Function );

    type Allegro_IIO_FS_Saver_Function is access
        function( fp : A_Allegro_File; bitmap : A_Allegro_Bitmap ) return Bool;
    pragma Convention( C, Allegro_IIO_FS_Saver_Function );

    type Allegro_IIO_Identifier_Function is access
        function( f : A_Allegro_File ) return Bool;
    pragma Convention( C, Allegro_IIO_Identifier_Function );

    function Al_Register_Bitmap_Loader( ext    : String;
                                        loader : Allegro_IIO_Loader_Function ) return Boolean;

    function Al_Register_Bitmap_Saver( ext   : String;
                                       saver : Allegro_IIO_Saver_Function ) return Boolean;

    function Al_Register_Bitmap_Loader_f( ext       : String;
                                          fs_loader : Allegro_IIO_FS_Loader_Function ) return Boolean;

    function Al_Register_Bitmap_Saver_f( ext      : String;
                                         fs_saver : Allegro_IIO_FS_Saver_Function ) return Boolean;

    function Al_Register_Bitmap_Identifier( ext        : String;
                                            identifier : Allegro_IIO_Identifier_Function ) return Boolean;

    function Al_Load_Bitmap( filename : String ) return A_Allegro_Bitmap;

    function Al_Load_Bitmap_Flags( filename : String; flags : Bitmap_Loader_Flags ) return A_Allegro_Bitmap;

    function Al_Load_Bitmap_f( fp    : A_Allegro_File;
                               ident : String ) return A_Allegro_Bitmap;

    function Al_Load_Bitmap_Flags_f( fp    : A_Allegro_File;
                                     ident : String;
                                     flags : Bitmap_Loader_Flags ) return A_Allegro_Bitmap;

    function Al_Save_Bitmap( filename : String;
                             bitmap   : A_Allegro_Bitmap ) return Boolean;

    procedure Al_Save_Bitmap( filename : String; bitmap : A_Allegro_Bitmap );

    function Al_Save_Bitmap_f( fp     : A_Allegro_File;
                               ident  : String;
                               bitmap : A_Allegro_Bitmap ) return Boolean;

    procedure Al_Save_Bitmap_f( fp     : A_Allegro_File;
                                ident  : String;
                                bitmap : A_Allegro_Bitmap );

    function Al_Identify_Bitmap( filename : String ) return String;

    function Al_Identify_Bitmap_f( fp : A_Allegro_File ) return String;

private

    ALLEGRO_KEEP_BITMAP_FORMAT     : constant Bitmap_Loader_Flags := 16#0002#;
    ALLEGRO_NO_PREMULTIPLIED_ALPHA : constant Bitmap_Loader_Flags := 16#0200#;
    ALLEGRO_KEEP_INDEX             : constant Bitmap_Loader_Flags := 16#0800#;

end Allegro.Bitmaps.Image_IO;
