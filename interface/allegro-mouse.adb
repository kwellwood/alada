--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Mouse is

    function Al_Install_Mouse return Boolean is

        function C_Al_Install_Mouse return Bool;
        pragma Import( C, C_Al_Install_Mouse, "al_install_mouse" );

    begin
        return To_Ada( C_Al_Install_Mouse );
    end Al_Install_Mouse;

    ----------------------------------------------------------------------------

    function Al_Is_Mouse_Installed return Boolean is

        function C_Al_Is_Mouse_Installed return Bool;
        pragma Import( C, C_Al_Is_Mouse_Installed, "al_is_mouse_installed" );

    begin
       return To_Ada( C_Al_Is_Mouse_Installed );
    end Al_Is_Mouse_Installed;

    ----------------------------------------------------------------------------

    function Al_Mouse_Button_Down( state : Allegro_Mouse_State; button : Integer ) return Boolean is

        function C_Al_Mouse_Button_Down( state : Allegro_Mouse_State; button : Integer ) return Bool;
        pragma Import( C, C_Al_Mouse_Button_Down, "al_mouse_button_down" );

    begin
        return To_Ada( C_Al_Mouse_Button_Down( state, button ) );
    end Al_Mouse_Button_Down;

    ----------------------------------------------------------------------------

    function Al_Set_Mouse_XY( display : A_Allegro_Display; x, y : Integer ) return Boolean is

        function C_Al_Set_Mouse_XY( display : A_Allegro_Display; x, y : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Mouse_XY, "al_set_mouse_xy" );

    begin
        return To_Ada( C_Al_Set_Mouse_XY( display, x, y ) );
    end Al_Set_Mouse_XY;

    ----------------------------------------------------------------------------

    function Al_Set_Mouse_Z( z : Integer ) return Boolean is

        function C_Al_Set_Mouse_Z( z : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Mouse_Z, "al_set_mouse_z" );

    begin
        return To_Ada( C_Al_Set_Mouse_Z( z ) );
    end Al_Set_Mouse_Z;

    ----------------------------------------------------------------------------

    function Al_Set_Mouse_W( w : Integer ) return Boolean is

        function C_Al_Set_Mouse_W( w : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Mouse_W, "al_set_mouse_w" );

    begin
        return To_Ada( C_Al_Set_Mouse_W( w ) );
    end Al_Set_Mouse_W;

    ----------------------------------------------------------------------------

    function Al_Set_Mouse_Axis( axis : Integer; value : Integer ) return Boolean is

        function C_Al_Set_Mouse_Axis( axis : Integer; value : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Mouse_Axis, "al_set_mouse_axis" );

    begin
        return To_Ada( C_Al_Set_Mouse_Axis( axis, value ) );
    end Al_Set_Mouse_Axis;

    ----------------------------------------------------------------------------

    function Al_Get_Mouse_Cursor_Position( ret_x, ret_y : access Integer ) return Boolean is

        function C_Al_Get_Mouse_Cursor_Position( ret_x, ret_y : access Integer ) return Bool;
        pragma Import( C, C_Al_Get_Mouse_Cursor_Position, "al_get_mouse_cursor_position" );

    begin
        return To_Ada( C_Al_Get_Mouse_Cursor_Position( ret_x, ret_y ) );
    end Al_Get_Mouse_Cursor_Position;

    ----------------------------------------------------------------------------

    function Al_Grab_Mouse( display : A_Allegro_Display ) return Boolean is

        function C_Al_Grab_Mouse( display : A_Allegro_Display ) return Bool;
        pragma Import( C, C_Al_Grab_Mouse, "al_grab_mouse" );

    begin
        return To_Ada( C_Al_Grab_Mouse( display ) );
    end Al_Grab_Mouse;

    ----------------------------------------------------------------------------

    function Al_Ungrab_Mouse return Boolean is

        function C_Al_Ungrab_Mouse return Bool;
        pragma Import( C, C_Al_Ungrab_Mouse, "al_ungrab_mouse" );

    begin
        return To_Ada( C_Al_Ungrab_Mouse );
    end Al_Ungrab_Mouse;

end Allegro.Mouse;
