--
-- Copyright (c) 2013-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Audio is

    function Al_Install_Audio return Boolean is

        function C_Al_Install_Audio return Bool;
        pragma Import( C, C_Al_Install_Audio, "al_install_audio" );

    begin
        return To_Ada( C_Al_Install_Audio );
    end Al_Install_Audio;

    ----------------------------------------------------------------------------

    function Al_Is_Audio_Installed return Boolean is

        function C_Al_Is_Audio_Installed return Bool;
        pragma Import( C, C_Al_Is_Audio_Installed, "al_is_audio_installed" );

    begin
        return To_Ada( C_Al_Is_Audio_Installed );
    end Al_Is_Audio_Installed;

    ----------------------------------------------------------------------------

    function Al_Reserve_Samples( reserve_samples : Integer ) return Boolean is

        function C_Al_Reserve_Samples( reserve_samples : Integer ) return Bool;
        pragma Import( C, C_Al_Reserve_Samples, "al_reserve_samples" );

    begin
        return To_Ada( C_Al_Reserve_Samples( reserve_samples ) );
    end Al_Reserve_Samples;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Voice( voice : in out A_Allegro_Voice ) is

        procedure C_Al_Destroy_Voice( voice : A_Allegro_Voice );
        pragma Import( C, C_Al_Destroy_Voice, "al_destroy_voice" );

    begin
        if voice /= null then
            C_Al_Destroy_Voice( voice );
            voice := null;
        end if;
    end Al_Destroy_Voice;

    ----------------------------------------------------------------------------

    function Al_Attach_Audio_Stream_To_Voice( stream : A_Allegro_Audio_Stream;
                                              voice  : A_Allegro_Voice ) return Boolean is

        function C_Al_Attach_Audio_Stream_To_Voice( stream : A_Allegro_Audio_Stream;
                                                    voice  : A_Allegro_Voice ) return Bool;
        pragma Import( C, C_Al_Attach_Audio_Stream_To_Voice,
                          "al_attach_audio_stream_to_voice" );

    begin
        return To_Ada( C_Al_Attach_Audio_Stream_To_Voice( stream, voice ) );
    end Al_Attach_Audio_Stream_To_Voice;

    ----------------------------------------------------------------------------

    function Al_Attach_Mixer_To_Voice( mixer : A_Allegro_Mixer;
                                       voice : A_Allegro_Voice ) return Boolean is

        function C_Al_Attach_Mixer_To_Voice( mixer : A_Allegro_Mixer;
                                             voice : A_Allegro_Voice ) return Bool;
        pragma Import( C, C_Al_Attach_Mixer_To_Voice,
                          "al_attach_mixer_to_voice" );

    begin
        return To_Ada( C_Al_Attach_Mixer_To_Voice( mixer, voice ) );
    end Al_Attach_Mixer_To_Voice;

    ----------------------------------------------------------------------------

    function Al_Attach_Sample_Instance_To_Voice( stream : A_Allegro_Sample_Instance;
                                                 voice  : A_Allegro_Voice ) return Boolean is

        function C_Al_Attach_Sample_Instance_To_Voice( stream : A_Allegro_Sample_Instance;
                                                       voice  : A_Allegro_Voice ) return Bool;
        pragma Import( C, C_Al_Attach_Sample_Instance_To_Voice,
                          "al_attach_sample_instance_to_voice" );

    begin
        return To_Ada( C_Al_Attach_Sample_Instance_To_Voice( stream, voice ) );
    end Al_Attach_Sample_Instance_To_Voice;

    ----------------------------------------------------------------------------

    function Al_Get_Voice_Playing( voice : A_Allegro_Voice ) return Boolean is

        function C_Al_Get_Voice_Playing( voice : A_Allegro_Voice ) return Bool;
        pragma Import( C, C_Al_Get_Voice_Playing, "al_get_voice_playing" );

    begin
        return To_Ada( C_Al_Get_Voice_Playing( voice ) );
    end Al_Get_Voice_Playing;

    ----------------------------------------------------------------------------

    function Al_Set_Voice_Playing( voice : A_Allegro_Voice; val : Boolean ) return Boolean is

        function C_Al_Set_Voice_Playing( voice : A_Allegro_Voice; val : Bool ) return Bool;
        pragma Import( C, C_Al_Set_Voice_Playing, "al_set_voice_playing" );

    begin
        return To_Ada( C_Al_Set_Voice_Playing( voice, Boolean'Pos( val ) ) );
    end Al_Set_Voice_Playing;

    ----------------------------------------------------------------------------

    function Al_Set_Voice_Position( voice : A_Allegro_Voice; val : unsigned ) return Boolean is

        function C_Al_Set_Voice_Position( voice : A_Allegro_Voice; val : unsigned ) return Bool;
        pragma Import( C, C_Al_Set_Voice_Position, "al_set_voice_position" );

    begin
        return To_Ada( C_Al_Set_Voice_Position( voice, val ) );
    end Al_Set_Voice_Position;

    ----------------------------------------------------------------------------

    function Al_Create_Sample( buf       : Address;
                               samples   : unsigned;
                               freq      : unsigned;
                               depth     : Allegro_Audio_Depth;
                               chan_conf : Allegro_Channel_Conf;
                               free_buf  : Boolean ) return A_Allegro_Sample is

        function C_Al_Create_Sample( buf       : Address;
                                     samples   : unsigned;
                                     freq      : unsigned;
                                     depth     : Allegro_Audio_Depth;
                                     chan_conf : Allegro_Channel_Conf;
                                     free_buf  : Bool ) return A_Allegro_Sample;
        pragma Import( C, C_Al_Create_Sample, "al_create_sample" );

    begin
        return C_Al_Create_Sample( buf, samples, freq, depth, chan_conf,
                                   Boolean'Pos( free_buf ) );
    end Al_Create_Sample;

    ----------------------------------------------------------------------------

    procedure C_Al_Destroy_Sample( spl : A_Allegro_Sample );
    pragma Import( C, C_Al_Destroy_Sample, "al_destroy_sample" );

    procedure Al_Destroy_Sample( spl : in out A_Allegro_Sample ) is
    begin
        if spl /= null then
            C_Al_Destroy_Sample( spl );
            spl := null;
        end if;
    end Al_Destroy_Sample;

    ----------------------------------------------------------------------------

    function Al_Play_Sample( data     : A_Allegro_Sample;
                             gain     : Float;
                             pan      : Float;
                             speed    : Float;
                             playMode : Allegro_Playmode;
                             ret_id   : access Allegro_Sample_Id ) return Boolean is

        function C_Al_Play_Sample( data     : A_Allegro_Sample;
                                   gain     : Float;
                                   pan      : Float;
                                   speed    : Float;
                                   playMode : Allegro_Playmode;
                                   ret_id   : access Allegro_Sample_Id ) return Bool;
        pragma Import( C, C_Al_Play_Sample, "al_play_sample" );

    begin
        return To_Ada( C_Al_Play_Sample( data, gain, pan, speed, playMode, ret_id ) );
    end Al_Play_Sample;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Sample_Instance( spl : in out A_Allegro_Sample_Instance ) is

        procedure C_Al_Destroy_Sample_Instance( spl : A_Allegro_Sample_Instance );
        pragma Import( C, C_Al_Destroy_Sample_Instance, "al_destroy_sample_instance" );

    begin
        if spl /= null then
            C_Al_Destroy_Sample_Instance( spl );
            spl := null;
        end if;
    end Al_Destroy_Sample_Instance;

    ----------------------------------------------------------------------------

    function Al_Play_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Boolean is

        function C_Al_Play_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Bool;
        pragma Import( C, C_Al_Play_Sample_Instance, "al_play_sample_instance" );

    begin
        return To_Ada( C_Al_Play_Sample_Instance( spl ) );
    end Al_Play_Sample_Instance;

    ----------------------------------------------------------------------------

    function Al_Stop_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Boolean is

        function C_Al_Stop_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Bool;
        pragma Import( C, C_Al_Stop_Sample_Instance, "al_stop_sample_instance" );

    begin
        return To_Ada( C_Al_Stop_Sample_Instance( spl ) );
    end Al_Stop_Sample_Instance;

    ----------------------------------------------------------------------------

    function Al_Set_Sample_Instance_Channel_Matrix( spl : A_Allegro_Sample_Instance; matrix : Float_Array ) return Boolean is
        function C_Al_Set_Sample_Instance_Channel_Matrix( spl : A_Allegro_Sample_Instance; matrix : Address ) return Bool;
        pragma Import( C, C_Al_Set_Sample_Instance_Channel_Matrix, "al_set_sample_instance_channel_matrix" );
    begin
        return To_Ada( C_Al_Set_Sample_Instance_Channel_Matrix( spl, matrix(matrix'First)'Address ) );
    end Al_Set_Sample_Instance_Channel_Matrix;

    ----------------------------------------------------------------------------

    function Al_Set_Sample_Instance_Length( spl : A_Allegro_Sample_Instance;
                                            val : unsigned ) return Boolean is

        function C_Al_Set_Sample_Instance_Length( spl : A_Allegro_Sample_Instance;
                                                  val : unsigned ) return Bool;
        pragma Import( C, C_Al_Set_Sample_Instance_Length, "al_set_sample_instance_length" );

    begin
        return To_Ada( C_Al_Set_Sample_Instance_Length( spl, val ) );
    end Al_Set_Sample_Instance_Length;

    ----------------------------------------------------------------------------

    function Al_Set_Sample_Instance_Position( spl : A_Allegro_Sample_Instance;
                                              val : unsigned ) return Boolean is

        function C_Al_Set_Sample_Instance_Position( spl : A_Allegro_Sample_Instance;
                                                    val : unsigned ) return Bool;
        pragma Import( C, C_Al_Set_Sample_Instance_Position, "al_set_sample_instance_position" );

    begin
        return To_Ada( C_Al_Set_Sample_Instance_Position( spl, val ) );
    end Al_Set_Sample_Instance_Position;

    ----------------------------------------------------------------------------

    function Al_Set_Sample_Instance_Speed( spl : A_Allegro_Sample_Instance;
                                           val : Float ) return Boolean is

        function C_Al_Set_Sample_Instance_Speed( spl : A_Allegro_Sample_Instance;
                                                 val : Float ) return Bool;
        pragma Import( C, C_Al_Set_Sample_Instance_Speed, "al_set_sample_instance_speed" );

    begin
        return To_Ada( C_Al_Set_Sample_Instance_Speed( spl, val ) );
    end Al_Set_Sample_Instance_Speed;

    ----------------------------------------------------------------------------

    function Al_Set_Sample_Instance_Gain( spl : A_Allegro_Sample_Instance;
                                          val : Float ) return Boolean is

        function C_Al_Set_Sample_Instance_Gain( spl : A_Allegro_Sample_Instance;
                                                val : Float ) return Bool;
        pragma Import( C, C_Al_Set_Sample_Instance_Gain, "al_set_sample_instance_gain" );

    begin
        return To_Ada( C_Al_Set_Sample_Instance_Gain( spl, val ) );
    end Al_Set_Sample_Instance_Gain;

    ----------------------------------------------------------------------------

    function Al_Set_Sample_Instance_Pan( spl : A_Allegro_Sample_Instance;
                                         val : Float ) return Boolean is

        function C_Al_Set_Sample_Instance_Pan( spl : A_Allegro_Sample_Instance;
                                               val : Float ) return Bool;
        pragma Import( C, C_Al_Set_Sample_Instance_Pan, "al_set_sample_instance_pan" );

    begin
        return To_Ada( C_Al_Set_Sample_Instance_Pan( spl, val ) );
    end Al_Set_Sample_Instance_Pan;

    ----------------------------------------------------------------------------

    function Al_Set_Sample_Instance_Playmode( spl : A_Allegro_Sample_Instance;
                                              val : Allegro_Playmode ) return Boolean is

        function C_Al_Set_Sample_Instance_Playmode( spl : A_Allegro_Sample_Instance;
                                                    val : Allegro_Playmode ) return Bool;
        pragma Import( C, C_Al_Set_Sample_Instance_Playmode, "al_set_sample_instance_playmode" );

    begin
        return To_Ada( C_Al_Set_Sample_Instance_Playmode( spl, val ) );
    end Al_Set_Sample_Instance_Playmode;

    ----------------------------------------------------------------------------

    function Al_Get_Sample_Instance_Playing( spl : A_Allegro_Sample_Instance ) return Boolean is

        function C_Al_Get_Sample_Instance_Playing( spl : A_Allegro_Sample_Instance ) return Bool;
        pragma Import( C, C_Al_Get_Sample_Instance_Playing, "al_get_sample_instance_playing" );

    begin
        return To_Ada( C_Al_Get_Sample_Instance_Playing( spl ) );
    end Al_Get_Sample_Instance_Playing;

    ----------------------------------------------------------------------------

    function Al_Set_Sample_Instance_Playing( spl : A_Allegro_Sample_Instance;
                                             val : Boolean ) return Boolean is

        function C_Al_Set_Sample_Instance_Playing( spl : A_Allegro_Sample_Instance;
                                                   val : Bool ) return Bool;
        pragma Import( C, C_Al_Set_Sample_Instance_Playing, "al_set_sample_instance_playing" );

    begin
        return To_Ada( C_Al_Set_Sample_Instance_Playing( spl, Boolean'Pos( val ) ) );
    end Al_Set_Sample_Instance_Playing;

    ----------------------------------------------------------------------------

    procedure Al_Set_Sample_Instance_Playing( spl : A_Allegro_Sample_Instance;
                                              val : Boolean ) is
    begin
        if Al_Set_Sample_Instance_Playing( spl, val ) then
            null;
        end if;
    end Al_Set_Sample_Instance_Playing;

    ----------------------------------------------------------------------------

    function Al_Get_Sample_Instance_Attached( spl : A_Allegro_Sample_Instance ) return Boolean is

        function C_Al_Get_Sample_Instance_Attached( spl : A_Allegro_Sample_Instance ) return Bool;
        pragma Import( C, C_Al_Get_Sample_Instance_Attached, "al_get_sample_instance_attached" );

    begin
        return To_Ada( C_Al_Get_Sample_Instance_Attached( spl ) );
    end Al_Get_Sample_Instance_Attached;

    ----------------------------------------------------------------------------

    function Al_Detach_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Boolean is

        function C_Al_Detach_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Bool;
        pragma Import( C, C_Al_Detach_Sample_Instance, "al_detach_sample_instance" );

    begin
        return To_Ada( C_Al_Detach_Sample_Instance( spl ) );
    end Al_Detach_Sample_Instance;

    ----------------------------------------------------------------------------

    procedure Al_Detach_Sample_Instance( spl : A_Allegro_Sample_Instance ) is
    begin
        if Al_Detach_Sample_Instance( spl ) then
            null;
        end if;
    end Al_Detach_Sample_Instance;

    ----------------------------------------------------------------------------

    function Al_Set_Sample( spl  : A_Allegro_Sample_Instance;
                            data : A_Allegro_Sample ) return Boolean is

        function C_Al_Set_Sample( spl  : A_Allegro_Sample_Instance;
                                  data : A_Allegro_Sample ) return Bool;
        pragma Import( C, C_Al_Set_Sample, "al_set_sample" );

    begin
        return To_Ada( C_Al_Set_Sample( spl, data ) );
    end Al_Set_Sample;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Mixer( mixer : in out A_Allegro_Mixer ) is

        procedure C_Al_Destroy_Mixer( mixer : A_Allegro_Mixer );
        pragma Import( C, C_Al_Destroy_Mixer, "al_destroy_mixer" );

    begin
        if mixer /= null then
            C_Al_Destroy_Mixer( mixer );
            mixer := null;
        end if;
    end Al_Destroy_Mixer;

    ----------------------------------------------------------------------------

    function Al_Set_Default_Mixer( mixer : A_Allegro_Mixer ) return Boolean is

        function C_Al_Set_Default_Mixer( mixer : A_Allegro_Mixer ) return Bool;
        pragma Import( C, C_Al_Set_Default_Mixer, "al_set_default_mixer" );

    begin
        return To_Ada( C_Al_Set_Default_Mixer( mixer ) );
    end Al_Set_Default_Mixer;

    ----------------------------------------------------------------------------

    procedure Al_Set_Default_Mixer( mixer : A_Allegro_Mixer ) is
    begin
        if Al_Set_Default_Mixer( mixer ) then
            null;
        end if;
    end Al_Set_Default_Mixer;

    ----------------------------------------------------------------------------

    function Al_Restore_Default_Mixer return Boolean is

        function C_Al_Restore_Default_Mixer return Bool;
        pragma Import( C, C_Al_Restore_Default_Mixer, "al_restore_default_mixer" );

    begin
        return To_Ada( C_Al_Restore_Default_Mixer );
    end Al_Restore_Default_Mixer;

    ----------------------------------------------------------------------------

    function Al_Attach_Mixer_To_Mixer( stream : A_Allegro_Mixer;
                                       mixer  : A_Allegro_Mixer ) return Boolean is

        function C_Al_Attach_Mixer_To_Mixer( stream : A_Allegro_Mixer;
                                             mixer  : A_Allegro_Mixer ) return Bool;
        pragma Import( C, C_Al_Attach_Mixer_To_Mixer, "al_attach_mixer_to_mixer" );

    begin
        return To_Ada( C_Al_Attach_Mixer_To_Mixer( stream, mixer ) );
    end Al_Attach_Mixer_To_Mixer;

    ----------------------------------------------------------------------------

    function Al_Attach_Sample_Instance_To_Mixer( stream : A_Allegro_Sample_Instance;
                                                 mixer  : A_Allegro_Mixer ) return Boolean is

        function C_Al_Attach_Sample_Instance_To_Mixer( stream : A_Allegro_Sample_Instance;
                                                       mixer  : A_Allegro_Mixer ) return Bool;
        pragma Import( C, C_Al_Attach_Sample_Instance_To_Mixer, "al_attach_sample_instance_to_mixer" );

    begin
        return To_Ada( C_Al_Attach_Sample_Instance_To_Mixer( stream, mixer ) );
    end Al_Attach_Sample_Instance_To_Mixer;

    ----------------------------------------------------------------------------

    function Al_Attach_Audio_Stream_To_Mixer( stream : A_Allegro_Audio_Stream;
                                              mixer  : A_Allegro_Mixer ) return Boolean is

        function C_Al_Attach_Audio_Stream_To_Mixer( stream : A_Allegro_Audio_Stream;
                                                    mixer  : A_Allegro_Mixer ) return Bool;
        pragma Import( C, C_Al_Attach_Audio_Stream_To_Mixer, "al_attach_audio_stream_to_mixer" );

    begin
        return To_Ada( C_Al_Attach_Audio_Stream_To_Mixer( stream, mixer ) );
    end Al_Attach_Audio_Stream_To_Mixer;

    ----------------------------------------------------------------------------

    function Al_Set_Mixer_Frequency( mixer : A_Allegro_Mixer;
                                     val   : unsigned ) return Boolean is

        function C_Al_Set_Mixer_Frequency( mixer : A_Allegro_Mixer;
                                           val   : unsigned ) return Bool;
        pragma Import( C, C_Al_Set_Mixer_Frequency, "al_set_mixer_frequency" );

    begin
        return To_Ada( C_Al_Set_Mixer_Frequency( mixer, val ) );
    end Al_Set_Mixer_Frequency;

    ----------------------------------------------------------------------------

    function Al_Set_Mixer_Quality( mixer : A_Allegro_Mixer;
                                   val   : Allegro_Mixer_Quality ) return Boolean is

        function C_Al_Set_Mixer_Quality( mixer : A_Allegro_Mixer;
                                         val   : Allegro_Mixer_Quality ) return Bool;
        pragma Import( C, C_Al_Set_Mixer_Quality, "al_set_mixer_quality" );

    begin
        return To_Ada( C_Al_Set_Mixer_Quality( mixer, val ) );
    end Al_Set_Mixer_Quality;

    ----------------------------------------------------------------------------

    function Al_Set_Mixer_Gain( mixer : A_Allegro_Mixer;
                                val   : Float ) return Boolean is

        function C_Al_Set_Mixer_Gain( mixer : A_Allegro_Mixer;
                                      val   : Float ) return Bool;
        pragma Import( C, C_Al_Set_Mixer_Gain, "al_set_mixer_gain" );

    begin
        return To_Ada( C_Al_Set_Mixer_Gain( mixer, val ) );
    end Al_Set_Mixer_Gain;

    ----------------------------------------------------------------------------

    function Al_Get_Mixer_Playing( mixer : A_Allegro_Mixer ) return Boolean is

        function C_Al_Get_Mixer_Playing( mixer : A_Allegro_Mixer ) return Bool;
        pragma Import( C, C_Al_Get_Mixer_Playing, "al_get_mixer_playing" );

    begin
        return To_Ada( C_Al_Get_Mixer_Playing( mixer ) );
    end Al_Get_Mixer_Playing;

    ----------------------------------------------------------------------------

    function Al_Set_Mixer_Playing( mixer : A_Allegro_Mixer; val : Boolean ) return Boolean is

        function C_Al_Set_Mixer_Playing( mixer : A_Allegro_Mixer;
                                         val   : Bool ) return Bool;
        pragma Import( C, C_Al_Set_Mixer_Playing, "al_set_mixer_playing" );

    begin
        return To_Ada( C_Al_Set_Mixer_Playing( mixer, Boolean'Pos( val ) ) );
    end Al_Set_Mixer_Playing;

    ----------------------------------------------------------------------------

    function Al_Get_Mixer_Attached( mixer : A_Allegro_Mixer ) return Boolean is

        function C_Al_Get_Mixer_Attached( mixer : A_Allegro_Mixer ) return Bool;
        pragma Import( C, C_Al_Get_Mixer_Attached, "al_get_mixer_attached" );

    begin
        return To_Ada( C_Al_Get_Mixer_Attached( mixer ) );
    end Al_Get_Mixer_Attached;

    ----------------------------------------------------------------------------

    function Al_Detach_Mixer( mixer : A_Allegro_Mixer ) return Boolean is

        function C_Al_Detach_Mixer( mixer : A_Allegro_Mixer ) return Bool;
        pragma Import( C, C_Al_Detach_Mixer, "al_detach_mixer" );

    begin
        return To_Ada( C_Al_Detach_Mixer( mixer ) );
    end Al_Detach_Mixer;

    ----------------------------------------------------------------------------

    procedure Al_Detach_Mixer( mixer : A_Allegro_Mixer ) is
        ignored : Boolean;
        pragma Unreferenced( ignored );
    begin
        ignored := Al_Detach_Mixer( mixer );
    end Al_Detach_Mixer;

    ----------------------------------------------------------------------------

    function Al_Set_Mixer_Postprocess_Callback( mixer : A_Allegro_Mixer;
                                                cb    : A_Mixer_Postprocess_Callback;
                                                data  : Address ) return Boolean is

        function C_Al_Set_Mixer_Postprocess_Callback( mixer : A_Allegro_Mixer;
                                                      cb    : A_Mixer_Postprocess_Callback;
                                                      data  : Address ) return Bool;
        pragma Import( C, C_Al_Set_Mixer_Postprocess_Callback, "al_set_mixer_postprocess_callback" );

    begin
        return To_Ada( C_Al_Set_Mixer_Postprocess_Callback( mixer, cb, data ) );
    end Al_Set_Mixer_Postprocess_Callback;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Audio_Stream( stream : in out A_Allegro_Audio_Stream ) is

        procedure C_Al_Destroy_Audio_Stream( stream : A_Allegro_Audio_Stream );
        pragma Import( C, C_Al_Destroy_Audio_Stream, "al_destroy_audio_stream" );

    begin
        if stream /= null then
            C_Al_Destroy_Audio_Stream( stream );
            stream := null;
        end if;
    end Al_Destroy_Audio_Stream;

    ----------------------------------------------------------------------------

    function Al_Rewind_Audio_Stream( stream : A_Allegro_Audio_Stream ) return Boolean is

        function C_Al_Rewind_Audio_Stream( stream : A_Allegro_Audio_Stream ) return Bool;
        pragma Import( C, C_Al_Rewind_Audio_Stream, "al_rewind_audio_stream" );

    begin
        return To_Ada( C_Al_Rewind_Audio_Stream( stream ) );
    end Al_Rewind_Audio_Stream;

    ----------------------------------------------------------------------------

    function Al_Set_Audio_Stream_Speed( stream : A_Allegro_Audio_Stream;
                                        val    : Float ) return Boolean is

        function C_Al_Set_Audio_Stream_Speed( stream : A_Allegro_Audio_Stream;
                                              val    : Float ) return Bool;
        pragma Import( C, C_Al_Set_Audio_Stream_Speed, "al_set_audio_stream_speed" );

    begin
        return To_Ada( C_Al_Set_Audio_Stream_Speed( stream, val ) );
    end Al_Set_Audio_Stream_Speed;

    ----------------------------------------------------------------------------

    function Al_Set_Audio_Stream_Gain( stream : A_Allegro_Audio_Stream;
                                       val    : Float ) return Boolean is

        function C_Al_Set_Audio_Stream_Gain( stream : A_Allegro_Audio_Stream;
                                             val    : Float ) return Bool;
        pragma Import( C, C_Al_Set_Audio_Stream_Gain, "al_set_audio_stream_gain" );

    begin
        return To_Ada( C_Al_Set_Audio_Stream_Gain( stream, val ) );
    end Al_Set_Audio_Stream_Gain;

    ----------------------------------------------------------------------------

    function Al_Set_Audio_Stream_Pan( stream : A_Allegro_Audio_Stream;
                                      val    : Float ) return Boolean is

        function C_Al_Set_Audio_Stream_Pan( stream : A_Allegro_Audio_Stream;
                                            val    : Float ) return Bool;
        pragma Import( C, C_Al_Set_Audio_Stream_Pan, "al_set_audio_stream_pan" );

    begin
        return To_Ada( C_Al_Set_Audio_Stream_Pan( stream, val ) );
    end Al_Set_Audio_Stream_Pan;

    ----------------------------------------------------------------------------

    function Al_Get_Audio_Stream_Playing( stream : A_Allegro_Audio_Stream ) return Boolean is

        function C_Al_Get_Audio_Stream_Playing( stream : A_Allegro_Audio_Stream ) return Bool;
        pragma Import( C, C_Al_Get_Audio_Stream_Playing, "al_get_audio_stream_playing" );

    begin
        return To_Ada( C_Al_Get_Audio_Stream_Playing( stream ) );
    end Al_Get_Audio_Stream_Playing;

    ----------------------------------------------------------------------------

    function Al_Set_Audio_Stream_Playing( stream : A_Allegro_Audio_Stream;
                                          val    : Boolean ) return Boolean is

        function C_Al_Set_Audio_Stream_Playing( stream : A_Allegro_Audio_Stream;
                                                val    : Bool ) return Bool;
        pragma Import( C, C_Al_Set_Audio_Stream_Playing, "al_set_audio_stream_playing" );

    begin
        return To_Ada( C_Al_Set_Audio_Stream_Playing( stream, Boolean'Pos( val ) ) );
    end Al_Set_Audio_Stream_Playing;

    ----------------------------------------------------------------------------

    procedure Al_Set_Audio_Stream_Playing( stream : A_Allegro_Audio_Stream;
                                           val    : Boolean ) is
    begin
        if Al_Set_Audio_Stream_Playing( stream, val ) then
            null;
        end if;
    end Al_Set_Audio_Stream_Playing;

    ----------------------------------------------------------------------------

    function Al_Set_Audio_Stream_Playmode( stream : A_Allegro_Audio_Stream;
                                           val    : Allegro_Playmode ) return Boolean is

        function C_Al_Set_Audio_Stream_Playmode( stream : A_Allegro_Audio_Stream;
                                                 val    : Allegro_Playmode ) return Bool;
        pragma Import( C, C_Al_Set_Audio_Stream_Playmode, "al_set_audio_stream_playmode" );

    begin
        return To_Ada( C_Al_Set_Audio_Stream_Playmode( stream, val ) );
    end Al_Set_Audio_Stream_Playmode;

    ----------------------------------------------------------------------------

    procedure Al_Set_Audio_Stream_Playmode( stream : A_Allegro_Audio_Stream;
                                            val    : Allegro_Playmode ) is
        ok : Boolean;
        pragma Warnings( Off, ok );
    begin
        ok := Al_Set_Audio_Stream_Playmode( stream, val );
    end Al_Set_Audio_Stream_Playmode;

    ----------------------------------------------------------------------------

    function Al_Get_Audio_Stream_Attached( stream : A_Allegro_Audio_Stream ) return Boolean is

        function C_Al_Get_Audio_Stream_Attached( stream : A_Allegro_Audio_Stream ) return Bool;
        pragma Import( C, C_Al_Get_Audio_Stream_Attached, "al_get_audio_stream_attached" );

    begin
        return To_Ada( C_Al_Get_Audio_Stream_Attached( stream ) );
    end Al_Get_Audio_Stream_Attached;

    ----------------------------------------------------------------------------

    function Al_Detach_Audio_Stream( stream : A_Allegro_Audio_Stream ) return Boolean is

        function C_Al_Detach_Audio_Stream( stream : A_Allegro_Audio_Stream ) return Bool;
        pragma Import( C, C_Al_Detach_Audio_Stream, "al_detach_audio_stream" );

    begin
        return To_Ada( C_Al_Detach_Audio_Stream( stream ) );
    end Al_Detach_Audio_Stream;

    ----------------------------------------------------------------------------

    procedure Al_Detach_Audio_Stream( stream : A_Allegro_Audio_Stream ) is
    begin
        if Al_Detach_Audio_Stream( stream ) then
            null;
        end if;
    end Al_Detach_Audio_Stream;

    ----------------------------------------------------------------------------

    function Al_Set_Audio_Stream_Fragment( stream : A_Allegro_Audio_Stream;
                                           val    : Address ) return Boolean is

        function C_Al_Set_Audio_Stream_Fragment( stream : A_Allegro_Audio_Stream;
                                                 val    : Address ) return Bool;
        pragma Import( C, C_Al_Set_Audio_Stream_Fragment, "al_set_audio_stream_fragment" );

    begin
        return To_Ada( C_Al_Set_Audio_Stream_Fragment( stream, val ) );
    end Al_Set_Audio_Stream_Fragment;

    ----------------------------------------------------------------------------

    function Al_Set_Audio_Stream_Channel_Matrix( stream : A_Allegro_Audio_Stream; matrix : Float_Array ) return Boolean is
        function C_Al_Set_Audio_Stream_Channel_Matrix( stream : A_Allegro_Audio_Stream; matrix : Address ) return Bool;
        pragma Import( C, C_Al_Set_Audio_Stream_Channel_Matrix, "al_set_audio_stream_channel_matrix" );
    begin
        return To_Ada( C_Al_Set_Audio_Stream_Channel_Matrix( stream, matrix(matrix'First)'Address ) );
    end Al_Set_Audio_Stream_Channel_Matrix;

    ----------------------------------------------------------------------------

    function Al_Seek_Audio_Stream_Secs( stream : A_Allegro_Audio_Stream;
                                        time   : double ) return Boolean is

        function C_Al_Seek_Audio_Stream_Secs( stream : A_Allegro_Audio_Stream;
                                              time   : double ) return Bool;
        pragma Import( C, C_Al_Seek_Audio_Stream_Secs, "al_seek_audio_stream_secs" );

    begin
        return To_Ada( C_Al_Seek_Audio_Stream_Secs( stream, time ) );
    end Al_Seek_Audio_Stream_Secs;

    ----------------------------------------------------------------------------

    function Al_Set_Audio_Stream_Loop_Secs( stream : A_Allegro_Audio_Stream;
                                        start  : double;
                                        finish : double ) return Boolean is

        function C_Al_Set_Audio_Stream_Loop_Secs( stream : A_Allegro_Audio_Stream;
                                                  start  : double;
                                                  finish : double ) return Bool;
        pragma Import( C, C_Al_Set_Audio_Stream_Loop_Secs, "al_set_audio_stream_loop_secs" );

    begin
        return To_Ada( C_Al_Set_Audio_Stream_Loop_Secs( stream, start, finish ) );
    end Al_Set_Audio_Stream_Loop_Secs;

    ----------------------------------------------------------------------------

    function Al_Register_Sample_Loader( ext    : String;
                                        loader : A_Allegro_Sample_Loader ) return Boolean is

        function C_Al_Register_Sample_Loader( ext    : C.char_array;
                                              loader : A_Allegro_Sample_Loader ) return Bool;
        pragma Import( C, C_Al_Register_Sample_Loader, "al_register_sample_loader" );

    begin
        return To_Ada( C_Al_Register_Sample_Loader( To_C( ext ), loader ) );
    end Al_Register_Sample_Loader;

    ----------------------------------------------------------------------------

    function Al_Register_Sample_Loader_f( ext    : String;
                                          loader : A_Allegro_Sample_Loader_f ) return Boolean is

        function C_Al_Register_Sample_Loader_f( ext    : C.char_array;
                                                loader : A_Allegro_Sample_Loader_f ) return Bool;
        pragma Import( C, C_Al_Register_Sample_Loader_f, "al_register_sample_loader_f" );

    begin
        return To_Ada( C_Al_Register_Sample_Loader_f( To_C( ext ), loader ) );
    end Al_Register_Sample_Loader_f;

    ----------------------------------------------------------------------------

    function Al_Register_Sample_Saver( ext   : String;
                                       saver : A_Allegro_Sample_Saver ) return Boolean is

        function C_Al_Register_Sample_Saver( ext   : C.char_array;
                                             saver : A_Allegro_Sample_Saver ) return Bool;
        pragma Import( C, C_Al_Register_Sample_Saver, "al_register_sample_saver" );

    begin
        return To_Ada( C_Al_Register_Sample_Saver( To_C( ext ), saver ) );
    end Al_Register_Sample_Saver;

    ----------------------------------------------------------------------------

    function Al_Register_Sample_Saver_f( ext   : String;
                                         saver : A_Allegro_Sample_Saver_f ) return Boolean is

        function C_Al_Register_Sample_Saver_f( ext   : C.char_array;
                                               saver : A_Allegro_Sample_Saver_f ) return Bool;
        pragma Import( C, C_Al_Register_Sample_Saver_f, "al_register_sample_saver_f" );

    begin
        return To_Ada( C_Al_Register_Sample_Saver_f( To_C( ext ), saver ) );
    end Al_Register_Sample_Saver_f;

    ----------------------------------------------------------------------------

    function Al_Register_Audio_Stream_Loader( ext           : String;
                                              stream_loader : A_Allegro_Audio_Stream_Loader ) return Boolean is

        function C_Al_Register_Audio_Stream_Loader( ext           : C.char_array;
                                                    stream_loader : A_Allegro_Audio_Stream_Loader ) return Bool;
        pragma Import( C, C_Al_Register_Audio_Stream_Loader, "al_register_audio_stream_loader" );

    begin
        return To_Ada( C_Al_Register_Audio_Stream_Loader( To_C( ext ), stream_loader ) );
    end Al_Register_Audio_Stream_Loader;

    ----------------------------------------------------------------------------

    function Al_Register_Audio_Stream_Loader_f( ext           : String;
                                                stream_loader : A_Allegro_Audio_Stream_Loader_f ) return Boolean is

        function C_Al_Register_Audio_Stream_Loader_f( ext           : C.char_array;
                                                      stream_loader : A_Allegro_Audio_Stream_Loader_f ) return Bool;
        pragma Import( C, C_Al_Register_Audio_Stream_Loader_f, "al_register_audio_stream_loader_f" );

    begin
        return To_Ada( C_Al_Register_Audio_Stream_Loader_f( To_C( ext ), stream_loader ) );
    end Al_Register_Audio_Stream_Loader_f;

    ----------------------------------------------------------------------------

    function Al_Load_Sample( filename : String ) return A_Allegro_Sample is

        function C_Al_Load_Sample( filename : C.char_array ) return A_Allegro_Sample;
        pragma Import( C, C_Al_Load_Sample, "al_load_sample" );

    begin
        return C_Al_Load_Sample( To_C( filename ) );
    end Al_Load_Sample;

    ----------------------------------------------------------------------------

    function Al_Load_Sample_f( fp : A_Allegro_File; ident : String ) return A_Allegro_Sample is

        function C_Al_Load_Sample_f( fp    : A_Allegro_File;
                                     ident : C.char_array ) return A_Allegro_Sample;
        pragma Import( C, C_Al_Load_Sample_f, "al_load_sample_f" );

    begin
        return C_Al_Load_Sample_f( fp, To_C( ident ) );
    end Al_Load_Sample_f;

    ----------------------------------------------------------------------------

    function Al_Load_Audio_Stream( filename     : String;
                                   buffer_count : size_t;
                                   samples      : unsigned ) return A_Allegro_Audio_Stream is

        function C_Al_Load_Audio_Stream( filename     : C.char_array;
                                         buffer_count : size_t;
                                         samples      : unsigned ) return A_Allegro_Audio_Stream;
        pragma Import( C, C_Al_Load_Audio_Stream, "al_load_audio_stream" );

    begin
        return C_Al_Load_Audio_Stream( To_C( filename ), buffer_count, samples );
    end Al_Load_Audio_Stream;

    ----------------------------------------------------------------------------

    function Al_Load_Audio_Stream_f( fp           : A_Allegro_File;
                                     ident        : String;
                                     buffer_count : size_t;
                                     samples      : unsigned ) return A_Allegro_Audio_Stream is

        function C_Al_Load_Audio_Stream_f( fp           : A_Allegro_File;
                                           ident        : C.char_array;
                                           buffer_count : size_t;
                                           samples      : unsigned ) return A_Allegro_Audio_Stream;
        pragma Import( C, C_Al_Load_Audio_Stream_f, "al_load_audio_stream_f" );

    begin
        return C_Al_Load_Audio_Stream_f( fp, To_C( ident ), buffer_count, samples );
    end Al_Load_Audio_Stream_f;

    ----------------------------------------------------------------------------

     function Al_Save_Sample( filename : String; spl : A_Allegro_Sample ) return Boolean is

        function C_Al_Save_Sample( filename : C.char_array;
                                   spl      : A_Allegro_Sample) return Bool;
        pragma Import( C, C_Al_Save_Sample, "al_save_sample" );

    begin
        return To_Ada( C_Al_Save_Sample( To_C( filename ), spl ) );
    end Al_Save_Sample;

    ----------------------------------------------------------------------------

     function Al_Save_Sample_f( fp    : A_Allegro_File;
                                ident : String;
                                spl   : A_Allegro_Sample ) return Boolean is

        function C_Al_Save_Sample_f( fp    : A_Allegro_File;
                                     ident : C.char_array;
                                     spl   : A_Allegro_Sample) return Bool;
        pragma Import( C, C_Al_Save_Sample_f, "al_save_sample_f" );

    begin
        return To_Ada( C_Al_Save_Sample_f( fp, To_C( ident ), spl ) );
    end Al_Save_Sample_f;

end Allegro.Audio;
