--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Color;                     use Allegro.Color;

-- Allegro 5.2.5 - Blending routines
package Allegro.Blending is

    type Allegro_Blend_Operations is private;
    ALLEGRO_ADD            : constant Allegro_Blend_Operations;
    ALLEGRO_SRC_MINUS_DEST : constant Allegro_Blend_Operations;
    ALLEGRO_SRC_MINUS_DST  : constant Allegro_Blend_Operations;
    ALLEGRO_DEST_MINUS_SRC : constant Allegro_Blend_Operations;
    ALLEGRO_DST_MINUS_SRC  : constant Allegro_Blend_Operations;

    type Allegro_Blend_Mode is private;
    ALLEGRO_ZERO                : constant Allegro_Blend_Mode;
    ALLEGRO_ONE                 : constant Allegro_Blend_Mode;
    ALLEGRO_ALPHA               : constant Allegro_Blend_Mode;
    ALLEGRO_INVERSE_ALPHA       : constant Allegro_Blend_Mode;
    ALLEGRO_SRC_COLOR           : constant Allegro_Blend_Mode;
    ALLEGRO_DST_COLOR           : constant Allegro_Blend_Mode;
    ALLEGRO_DEST_COLOR          : constant Allegro_Blend_Mode;
    ALLEGRO_INVERSE_SRC_COLOR   : constant Allegro_Blend_Mode;
    ALLEGRO_INVERSE_DEST_COLOR  : constant Allegro_Blend_Mode;
    ALLEGRO_CONST_COLOR         : constant Allegro_Blend_Mode;
    ALLEGRO_INVERSE_CONST_COLOR : constant Allegro_Blend_Mode;

    procedure Al_Get_Blender( op  : out Allegro_Blend_Operations;
                              src : out Allegro_Blend_Mode;
                              dst : out Allegro_Blend_Mode );
    pragma Import( C, Al_Get_Blender, "al_get_blender" );

    function Al_Get_Blend_Color return Allegro_Color;
    pragma Import( C, Al_Get_Blend_Color, "al_get_blend_color" );

    procedure Al_Get_Separate_Blender( op        : out Allegro_Blend_Operations;
                                       src       : out Allegro_Blend_Mode;
                                       dst       : out Allegro_Blend_Mode;
                                       alpha_op  : out Allegro_Blend_Operations;
                                       alpha_src : out Allegro_Blend_Mode;
                                       alpha_dst : out Allegro_Blend_Mode );
    pragma Import( C, Al_Get_Separate_Blender, "al_get_separate_blender" );

    procedure Al_Set_Blender( op  : Allegro_Blend_Operations;
                              src : Allegro_Blend_Mode;
                              dst : Allegro_Blend_Mode );
    pragma Import( C, Al_Set_Blender, "al_set_blender" );

    procedure Al_Set_Blend_Color( color : Allegro_Color );
    pragma Import( C, Al_Set_Blend_Color, "al_set_blend_color" );

    procedure Al_Set_Separate_Blender( op        : Allegro_Blend_Operations;
                                       src       : Allegro_Blend_Mode;
                                       dst       : Allegro_Blend_Mode;
                                       alpha_op  : Allegro_Blend_Operations;
                                       alpha_src : Allegro_Blend_Mode;
                                       alpha_dst : Allegro_Blend_Mode );
    pragma Import( C, Al_Set_Separate_Blender, "al_set_separate_blender" );

private

    type Allegro_Blend_Operations is new Integer;
    ALLEGRO_ADD            : constant Allegro_Blend_Operations := 0;
    ALLEGRO_SRC_MINUS_DEST : constant Allegro_Blend_Operations := 1;
    ALLEGRO_SRC_MINUS_DST  : constant Allegro_Blend_Operations := ALLEGRO_SRC_MINUS_DEST;
    ALLEGRO_DEST_MINUS_SRC : constant Allegro_Blend_Operations := 2;
    ALLEGRO_DST_MINUS_SRC  : constant Allegro_Blend_Operations := ALLEGRO_DEST_MINUS_SRC;

    type Allegro_Blend_Mode is new Integer;
    ALLEGRO_ZERO                : constant Allegro_Blend_Mode := 0;
    ALLEGRO_ONE                 : constant Allegro_Blend_Mode := 1;
    ALLEGRO_ALPHA               : constant Allegro_Blend_Mode := 2;
    ALLEGRO_INVERSE_ALPHA       : constant Allegro_Blend_Mode := 3;
    ALLEGRO_SRC_COLOR           : constant Allegro_Blend_Mode := 4;
    ALLEGRO_DST_COLOR           : constant Allegro_Blend_Mode := 5;
    ALLEGRO_DEST_COLOR          : constant Allegro_Blend_Mode := ALLEGRO_DST_COLOR;
    ALLEGRO_INVERSE_SRC_COLOR   : constant Allegro_Blend_Mode := 6;
    ALLEGRO_INVERSE_DEST_COLOR  : constant Allegro_Blend_Mode := 7;
    ALLEGRO_CONST_COLOR         : constant Allegro_Blend_Mode := 8;
    ALLEGRO_INVERSE_CONST_COLOR : constant Allegro_Blend_Mode := 9;

end Allegro.Blending;
