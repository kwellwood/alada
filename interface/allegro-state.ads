--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - State
package Allegro.State is

    type Allegro_State is private;
    type A_Allegro_State is access all Allegro_State;

    type Allegro_State_Flags is new Unsigned_32;

    ALLEGRO_STATE_NEW_DISPLAY_PARAMETERS : constant Allegro_State_Flags := 16#0001#;
    ALLEGRO_STATE_NEW_BITMAP_PARAMETERS  : constant Allegro_State_Flags := 16#0002#;
    ALLEGRO_STATE_DISPLAY                : constant Allegro_State_Flags := 16#0004#;
    ALLEGRO_STATE_TARGET_BITMAP          : constant Allegro_State_Flags := 16#0008#;
    ALLEGRO_STATE_BLENDER                : constant Allegro_State_Flags := 16#0010#;
    ALLEGRO_STATE_NEW_FILE_INTERFACE     : constant Allegro_State_Flags := 16#0020#;
    ALLEGRO_STATE_TRANSFORM              : constant Allegro_State_Flags := 16#0040#;
    ALLEGRO_STATE_PROJECTION_TRANSFORM   : constant Allegro_State_Flags := 16#0100#;
    ALLEGRO_STATE_BITMAP                 : constant Allegro_State_Flags := ALLEGRO_STATE_TARGET_BITMAP or
                                                                           ALLEGRO_STATE_NEW_BITMAP_PARAMETERS;
    ALLEGRO_STATE_ALL                    : constant Allegro_State_Flags := 16#ffff#;

    procedure Al_Store_State( state : in out Allegro_State; flags : Allegro_State_Flags );
    pragma Import( C, Al_Store_State, "al_store_state" );

    procedure Al_Restore_State( state : in out Allegro_State );
    pragma Import( C, Al_Restore_State, "al_restore_state" );

    function Al_Get_Errno return Integer;
    pragma Import( C, Al_Get_Errno, "al_get_errno" );

    procedure Al_Set_Errno( errnum : Integer );
    pragma Import( C, Al_Set_Errno, "al_set_errno" );

private

    type Allegro_State is
        record
            -- internally, a thread-local structure is placed here
            tls : String(1..1024);
        end record;
    pragma Convention( C, Allegro_State );

end Allegro.State;
