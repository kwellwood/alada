--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Events is

    function Allegro_Event_Type_Is_User( t : Allegro_Event_Type ) return Boolean is
    begin
        return t >= 512;
    end Allegro_Event_Type_Is_User;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Event_Queue( queue : in out A_Allegro_Event_Queue ) is

        procedure C_Al_Destroy_Event_Queue( queue : A_Allegro_Event_Queue );
        pragma Import( C, C_Al_Destroy_Event_Queue, "al_destroy_event_queue" );

    begin
        if queue /= null then
            C_Al_Destroy_Event_Queue( queue );
            queue := null;
        end if;
    end Al_Destroy_Event_Queue;

    ----------------------------------------------------------------------------

    function Al_Is_Event_Source_Registered( queue  : A_Allegro_Event_Queue;
                                            source : A_Allegro_Event_Source ) return Boolean is
        function C_Al_Is_Event_Source_Registered( queue  : A_Allegro_Event_Queue;
                                                  source : A_Allegro_Event_Source ) return Bool;
        pragma Import( C, C_Al_Is_Event_Source_Registered, "al_is_event_source_registered" );
    begin
        return C_Al_Is_Event_Source_Registered( queue, source ) = B_TRUE;
    end Al_Is_Event_Source_Registered;

    ----------------------------------------------------------------------------

    procedure Al_Pause_Event_Queue( queue : A_Allegro_Event_Queue; paused : Boolean ) is
        procedure C_Al_Pause_Event_Queue( queue : A_Allegro_Event_Queue; paused : Bool );
        pragma Import( C, C_Al_Pause_Event_Queue, "al_pause_event_queue" );
    begin
        C_Al_Pause_Event_Queue( queue, (if paused then B_TRUE else B_FALSE) );
    end Al_Pause_Event_Queue;

    ----------------------------------------------------------------------------

    function Al_Is_Event_Queue_Paused( queue : A_Allegro_Event_Queue ) return Boolean is
        function C_Al_Is_Event_Queue_Paused( queue : A_Allegro_Event_Queue ) return Bool;
        pragma Import( C, C_Al_Is_Event_Queue_Paused, "al_is_event_queue_paused" );
    begin
        return C_Al_Is_Event_Queue_Paused( queue ) = B_TRUE;
    end Al_Is_Event_Queue_Paused;

    ----------------------------------------------------------------------------

    function Al_Is_Event_Queue_Empty( queue : A_Allegro_Event_Queue ) return Boolean is

        function C_Al_Is_Event_Queue_Empty( queue : A_Allegro_Event_Queue ) return Bool;
        pragma Import( C, C_Al_Is_Event_Queue_Empty, "al_is_event_queue_empty" );

    begin
        return To_Ada( C_Al_Is_Event_Queue_Empty( queue ) );
    end Al_Is_Event_Queue_Empty;

    ----------------------------------------------------------------------------

    function Al_Get_Next_Event( queue     : A_Allegro_Event_Queue;
                                ret_event : access Allegro_Event ) return Boolean is

        function C_Al_Get_Next_Event( queue     : A_Allegro_Event_Queue;
                                      ret_event : access Allegro_Event ) return Bool;
        pragma Import( C, C_Al_Get_Next_Event, "al_get_next_event" );

    begin
        return To_Ada( C_Al_Get_Next_Event( queue, ret_event ) );
    end Al_Get_Next_Event;

    ----------------------------------------------------------------------------

    function Al_Peek_Next_Event( queue     : A_Allegro_Event_Queue;
                                 ret_event : access Allegro_Event ) return Boolean is

        function C_Al_Peek_Next_Event( queue     : A_Allegro_Event_Queue;
                                       ret_event : access Allegro_Event ) return Bool;
        pragma Import( C, C_Al_Peek_Next_Event, "al_peek_next_event" );

    begin
        return To_Ada( C_Al_Peek_Next_Event( queue, ret_event ) );
    end Al_Peek_Next_Event;

    ----------------------------------------------------------------------------

    function Al_Drop_Next_Event( queue : A_Allegro_Event_Queue ) return Boolean is

        function C_Al_Drop_Next_Event( queue : A_Allegro_Event_Queue ) return Bool;
        pragma Import( C, C_Al_Drop_Next_Event, "al_drop_next_event" );

    begin
        return To_Ada( C_Al_Drop_Next_Event( queue ) );
    end Al_Drop_Next_Event;

    ----------------------------------------------------------------------------

    function Al_Wait_For_Event_Timed( queue     : A_Allegro_Event_Queue;
                                      ret_event : access Allegro_Event;
                                      secs      : Float ) return Boolean is

        function C_Al_Wait_For_Event_Timed( queue     : A_Allegro_Event_Queue;
                                            ret_event : access Allegro_Event;
                                            secs      : Float ) return Bool;
        pragma Import( C, C_Al_Wait_For_Event_Timed, "al_wait_for_event_timed" );

    begin
        return To_Ada( C_Al_Wait_For_Event_Timed( queue, ret_event, secs ) );
    end Al_Wait_For_Event_Timed;

    ----------------------------------------------------------------------------

    function Al_Wait_For_Event_Until( queue     : A_Allegro_Event_Queue;
                                      ret_event : access Allegro_Event;
                                      timeout   : Allegro_Timeout ) return Boolean is

        function C_Al_Wait_For_Event_Until( queue     : A_Allegro_Event_Queue;
                                            ret_event : access Allegro_Event;
                                            timeout   : Allegro_Timeout ) return Bool;
        pragma Import( C, C_Al_Wait_For_Event_Until, "al_wait_for_event_until" );

    begin
        return To_Ada( C_Al_Wait_For_Event_Until( queue, ret_event, timeout ) );
    end Al_Wait_For_Event_Until;

    ----------------------------------------------------------------------------

    function Al_Emit_User_Event( src        : A_Allegro_Event_Source;
                                 event      : Allegro_Event;
                                 destructor : A_User_Event_Destructor ) return Boolean is

        function C_Al_Emit_User_Event( src        : A_Allegro_Event_Source;
                                       event      : Allegro_Event;
                                       destructor : A_User_Event_Destructor ) return Bool;
        pragma Import( C, C_Al_Emit_User_Event, "al_emit_user_event" );

    begin
        return To_Ada( C_Al_Emit_User_Event( src, event, destructor ) );
    end Al_Emit_User_Event;

    ----------------------------------------------------------------------------

    procedure Al_Emit_User_Event( src        : A_Allegro_Event_Source;
                                  event      : Allegro_Event;
                                  destructor : A_User_Event_Destructor ) is
    begin
        if Al_Emit_User_Event( src, event, destructor ) then
            null;
        end if;
    end Al_Emit_User_Event;

end Allegro.Events;
