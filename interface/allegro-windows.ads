--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Interfaces.C;                      use Interfaces.C;

-- Allegro 5.2.5 - Windows platform-specific routines
package Allegro.Windows is

--  function Al_Get_Win_Window_Handle( display : A_Allegro_Display ) return HWND;

    type A_Win_Callback is
        access function( display : A_Allegro_Display;
                         message : unsigned;
                         wparam  : size_t;
                         lparam  : long;
                         result  : access long
                       ) return Boolean;

    function Al_Win_Add_Window_Callback( display  : A_Allegro_Display;
                                         callback : A_Win_Callback
                                       ) return Boolean;

    function Al_Win_Remove_Window_Callback( display  : A_Allegro_Display;
                                            callback : A_Win_Callback ) return Boolean;

end Allegro.Windows;
