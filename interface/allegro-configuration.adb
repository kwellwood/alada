--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

package body Allegro.Configuration is

    procedure Al_Destroy_Config( config : in out A_Allegro_Config ) is

        procedure C_Al_Destroy_Config( config : A_Allegro_Config );
        pragma Import( C, C_Al_Destroy_Config, "al_destroy_config" );

    begin
        if config /= null then
            C_Al_Destroy_Config( config );
            config := null;
        end if;
    end Al_Destroy_Config;

    ----------------------------------------------------------------------------

    function Al_Load_Config_File( filename : String ) return A_Allegro_Config is

        function C_Al_Load_Config_File( filename : C.char_array  ) return A_Allegro_Config;
        pragma Import( C, C_Al_Load_Config_File, "al_load_config_file" );

    begin
        return C_Al_Load_Config_File( To_C( filename ) );
    end Al_Load_Config_File;

    ----------------------------------------------------------------------------

    function Al_Save_Config_File( filename : String;
                                  config   : A_Allegro_Config ) return Boolean is

        function C_Al_Save_Config_File( filename : C.char_array;
                                        config   : A_Allegro_Config ) return Bool;
        pragma Import( C, C_Al_Save_Config_File, "al_save_config_file" );

    begin
        return To_Ada( C_Al_Save_Config_File( To_C( filename ), config ) );
    end Al_Save_Config_File;

    ----------------------------------------------------------------------------

    function Al_Save_Config_File_f( file   : A_Allegro_File;
                                    config : A_Allegro_Config ) return Boolean is

       function C_Al_Save_Config_File_f( file   : A_Allegro_File;
                                         config : A_Allegro_Config ) return Bool;
       pragma Import( C, C_Al_Save_Config_File_f, "al_save_config_file_f" );

    begin
        return To_Ada( C_Al_Save_Config_File_f( file, config ) );
    end Al_Save_Config_File_f;

    ----------------------------------------------------------------------------

    procedure Al_Add_Config_Section( config : A_Allegro_Config;
                                     name   : String ) is

        procedure C_Al_Add_Config_Section( config : A_Allegro_Config;
                                           name   : C.char_array );
        pragma Import( C, C_Al_Add_Config_Section, "al_add_config_section" );

    begin
        C_Al_Add_Config_Section( config, To_C( name ) );
    end Al_Add_Config_Section;

    ----------------------------------------------------------------------------

    procedure Al_Add_Config_Comment( config  : A_Allegro_Config;
                                     section : String;
                                     comment : String ) is

        procedure C_Al_Add_Config_Comment( config  : A_Allegro_Config;
                                           section : C.char_array;
                                           comment : C.char_array );
        pragma Import( C, C_Al_Add_Config_Comment, "al_add_config_comment" );

    begin
        C_Al_Add_Config_Comment( config, To_C( section ), To_C( comment ) );
    end Al_Add_Config_Comment;

    ----------------------------------------------------------------------------

    function Al_Get_Config_Value( config  : A_Allegro_Config;
                                  section : String;
                                  key     : String ) return String is

        function C_Al_Get_Config_Value( config  : A_Allegro_Config;
                                        section : C.char_array;
                                        key     : C.char_array ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Config_Value, "al_get_config_value" );

        result : C.Strings.chars_ptr;
    begin
        result := C_Al_Get_Config_Value( config, To_C( section ), To_C( key ) );
        if result /= Null_Ptr then
            return To_Ada( Value( result ) );
        else
            return "";
        end if;
    end Al_Get_Config_Value;

    ----------------------------------------------------------------------------

    procedure Al_Set_Config_Value( config  : A_Allegro_Config;
                                   section : String;
                                   key     : String;
                                   value   : String ) is

        procedure C_Al_Set_Config_Value( config  : A_Allegro_Config;
                                         section : C.char_array;
                                         key     : C.char_array;
                                         value   : C.char_array );
        pragma Import( C, C_Al_Set_Config_Value, "al_set_config_value" );

    begin
        C_Al_Set_Config_Value( config, To_C( section ), To_C( key ), To_C( value ) );
    end Al_Set_Config_Value;

    ----------------------------------------------------------------------------

    function Al_Get_First_Config_Section( config   : A_Allegro_Config;
                                          iterator : access A_Allegro_Config_Section
                                        ) return String is

        function C_Al_Get_First_Config_Section( config   : A_Allegro_Config;
                                                iterator : access A_Allegro_Config_Section ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_First_Config_Section, "al_get_first_config_section" );

        val : C.Strings.chars_ptr;
    begin
        val := C_Al_Get_First_Config_Section( config, iterator );
        if val /= C.Strings.Null_Ptr then
            return To_Ada( Value( val ) );
        end if;
        return "";
    end Al_Get_First_Config_Section;

    ----------------------------------------------------------------------------

    function Al_Get_Next_Config_Section( iterator : access A_Allegro_Config_Section ) return String is

        function C_Al_Get_Next_Config_Section( iterator : access A_Allegro_Config_Section ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Next_Config_Section, "al_get_next_config_section" );

        val : C.Strings.chars_ptr;
    begin
        val := C_Al_Get_Next_Config_Section( iterator );
        if val /= C.Strings.Null_Ptr then
            return To_Ada( Value( val ) );
        end if;
        return "";
    end Al_Get_Next_Config_Section;

    ----------------------------------------------------------------------------

    function Al_Get_First_Config_Entry( config   : A_Allegro_Config;
                                          section  : String;
                                          iterator : access A_Allegro_Config_Entry
                                        ) return String is

        function C_Al_Get_First_Config_Entry( config   : A_Allegro_Config;
                                              section  : C.char_array;
                                              iterator : access A_Allegro_Config_Entry
                                            ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_First_Config_Entry, "al_get_first_config_entry" );

        val : C.Strings.chars_ptr;
    begin
        val := C_Al_Get_First_Config_Entry( config, To_C( section ), iterator );
        if val /= C.Strings.Null_Ptr then
            return To_Ada( Value( val ) );
        end if;
        return "";
    end Al_Get_First_Config_Entry;

    ----------------------------------------------------------------------------

    function Al_Get_Next_Config_Entry( iterator : access A_Allegro_Config_Entry ) return String is

        function C_Al_Get_Next_Config_Entry( iterator : access A_Allegro_Config_Entry ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Next_Config_Entry, "al_get_next_config_entry" );

        val : C.Strings.chars_ptr;
    begin
        val := C_Al_Get_Next_Config_Entry( iterator );
        if val /= C.Strings.Null_Ptr then
            return To_Ada( Value( val ) );
        end if;
        return "";
    end Al_Get_Next_Config_Entry;

    ----------------------------------------------------------------------------

    function Al_Remove_Config_Section( config  : A_Allegro_Config;
                                       section : String ) return Boolean is
        function C_Al_Remove_Config_Section( config  : A_Allegro_Config;
                                             section : C.char_array ) return Bool;
        pragma Import( C, C_Al_Remove_Config_Section, "al_remove_config_section" );
    begin
        return C_Al_Remove_Config_Section( config, To_C( section ) ) = B_TRUE;
    end Al_Remove_Config_Section;

    ----------------------------------------------------------------------------

    procedure Al_Remove_Config_Section( config : A_Allegro_Config; section : String ) is
        tmp : Boolean;
        pragma Warnings( Off, tmp );
    begin
        tmp := Al_Remove_Config_Section( config, section );
    end Al_Remove_Config_Section;

    ----------------------------------------------------------------------------

    function Al_Remove_Config_Key( config  : A_Allegro_Config;
                                   section : String;
                                   key     : String ) return Boolean is
        function C_Al_Remove_Config_Key( config  : A_Allegro_Config;
                                         section : C.char_array;
                                         key     : C.char_array ) return Bool;
        pragma Import( C, C_Al_Remove_Config_Key, "al_remove_config_key" );
    begin
        return C_Al_Remove_Config_Key( config, To_C( section ), To_C( key ) ) = B_TRUE;
    end Al_Remove_Config_Key;

    ----------------------------------------------------------------------------

    procedure Al_Remove_Config_Key( config  : A_Allegro_Config;
                                    section : String;
                                    key     : String ) is
        tmp : Boolean;
        pragma Warnings( Off, tmp );
    begin
        tmp := Al_Remove_Config_Key( config, section, key );
    end Al_Remove_Config_Key;

end Allegro.Configuration;
