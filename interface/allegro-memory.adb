--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Memory is

    function Al_Malloc( n : C.size_t ) return Address is

        function C_Al_Malloc( n    : C.size_t;
                              line : Integer;
                              file : C.char_array;
                              func : C.char_array ) return Address;
        pragma Import( C, C_Al_Malloc, "al_malloc_with_context" );

    begin
        return C_Al_Malloc( n, 0, To_C( "allegro-memory.adb" ), To_C( "Al_Malloc" ) );
    end Al_Malloc;

    ----------------------------------------------------------------------------

    procedure Al_Free( ptr : Address ) is

        procedure C_Al_Free( ptr  : Address;
                             line : Integer;
                             file : C.char_array;
                             func : C.char_array );
        pragma Import( C, C_Al_Free, "al_free_with_context" );

    begin
        C_Al_Free( ptr, 0, To_C( "allegro-memory.adb" ), To_C( "Al_Free" ) );
    end Al_Free;

    ----------------------------------------------------------------------------

   function Al_Realloc( ptr : Address; n : C.size_t ) return Address is

       function C_Al_Realloc( ptr  : Address;
                              n    : C.size_t;
                              line : Integer;
                              file : C.char_array;
                              func : C.char_array ) return Address;
        pragma Import( C, C_Al_Realloc, "al_realloc_with_context" );

   begin
       return C_Al_Realloc( ptr, n, 0, To_C( "allegro-memory.adb" ), To_C( "Al_Realloc" ) );
   end Al_Realloc;

    ----------------------------------------------------------------------------

    function Al_Calloc( count : C.size_t; n : C.size_t ) return Address is

       function C_Al_Calloc( count  : C.size_t;
                             n      : C.size_t;
                             line   : Integer;
                             file   : C.char_array;
                             func   : C.char_array ) return Address;
        pragma Import( C, C_Al_Calloc, "al_calloc_with_context" );

    begin
        return C_Al_Calloc( count, n, 0, To_C( "allegro-memory.adb" ), To_C( "Al_Calloc" ) );
    end Al_Calloc;

end Allegro.Memory;
