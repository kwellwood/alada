--
-- Copyright (c) 2013-2018 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.File_IO is

    function Al_Fopen( path : String; mode : String ) return A_Allegro_File is

        function C_Al_Fopen( path : C.char_array; mode : C.char_array ) return A_Allegro_File;
        pragma Import( C, C_Al_Fopen, "al_fopen" );

    begin
        return C_Al_Fopen( To_C( path ), To_C( mode ) );
    end Al_Fopen;

    ----------------------------------------------------------------------------

    function Al_Fopen_Slice( f            : A_Allegro_File;
                             initial_size : size_t;
                             mode         : String ) return A_Allegro_File is

        function C_Al_Fopen_Slice( f            : A_Allegro_File;
                                   initial_size : size_t;
                                   mode         : C.char_array ) return A_Allegro_File;
        pragma Import( C, C_Al_Fopen_Slice, "al_fopen_slice" );

    begin
        return C_Al_Fopen_Slice( f, initial_size, To_C( mode ) );
    end Al_Fopen_Slice;

    ----------------------------------------------------------------------------

    function Al_Fopen_Interface( vt   : A_Allegro_File_Interface;
                                 path : String;
                                 mode : String ) return A_Allegro_File is

        function C_Al_Fopen_Interface( vt   : A_Allegro_File_Interface;
                                       path : C.char_array;
                                       mode : C.char_array ) return A_Allegro_File;
        pragma Import( C, C_Al_Fopen_Interface, "al_fopen_interface" );

    begin
        return C_Al_Fopen_Interface( vt, To_C( path ), To_C( mode ) );
    end Al_Fopen_Interface;

    ----------------------------------------------------------------------------

    function Al_Fclose( f : A_Allegro_File ) return Boolean is
        function C_Al_Fclose( f : A_Allegro_File ) return Bool;
        pragma Import( C, C_Al_Fclose, "al_fclose" );
    begin
        return C_Al_Fclose( f ) = B_TRUE;
    end Al_Fclose;

    ----------------------------------------------------------------------------

    procedure Al_Fclose( f : in out A_Allegro_File ) is
        tmp : Boolean;
        pragma Warnings( off, tmp );
    begin
        tmp := Al_Fclose( f );
        f := null;
    end Al_Fclose;

    ----------------------------------------------------------------------------

    procedure Al_Fwrite( f : A_Allegro_File; ptr : Address; size : size_t ) is
        wrote : size_t;
        pragma Warnings( Off, wrote );
    begin
        wrote := Al_Fwrite( f, ptr, size );
    end Al_Fwrite;

    ----------------------------------------------------------------------------

    function Al_Fflush( f : A_Allegro_File ) return Boolean is

        function C_Al_Fflush( f : A_Allegro_File ) return Bool;
        pragma Import( C, C_Al_Fflush, "al_fflush" );

    begin
        return To_Ada( C_Al_Fflush( f ) );
    end Al_Fflush;

    ----------------------------------------------------------------------------

    function Al_Fseek( f : A_Allegro_File; offset : Integer_64; whence : Allegro_Seek ) return Boolean is

        function C_Al_Fseek( f : A_Allegro_File; offset : Integer_64; whence : Allegro_Seek ) return Bool;
        pragma Import( C, C_Al_Fseek, "al_fseek" );

    begin
        return To_Ada( C_Al_Fseek( f, offset, whence ) );
    end Al_Fseek;

    ----------------------------------------------------------------------------

    procedure Al_Fseek( f : A_Allegro_File; offset : Integer_64; whence : Allegro_Seek ) is
    begin
        if Al_Fseek( f, offset, whence ) then
            null;
        end if;
    end Al_Fseek;

    ----------------------------------------------------------------------------

    function Al_Feof( f : A_Allegro_File ) return Boolean is

        function C_Al_Feof( f : A_Allegro_File ) return Bool;
        pragma Import( C, C_Al_Feof, "al_feof" );

    begin
        return To_Ada( C_Al_Feof( f ) );
    end Al_Feof;

    ----------------------------------------------------------------------------

    function Al_Ferrmsg( f : A_Allegro_File ) return String is

        function C_Al_Ferrmsg( f : A_Allegro_File ) return chars_ptr;
        pragma Import( C, C_Al_Ferrmsg, "al_ferrmsg" );

        str : constant chars_ptr := C_Al_Ferrmsg( f );
    begin
        if str /= Null_Ptr then
            return Value( str );
        end if;
        return "";
    end Al_Ferrmsg;

    ----------------------------------------------------------------------------

    procedure Al_Fungetc( f : A_Allegro_File; c : Character ) is
        ok : Integer;
        pragma Warnings( Off, ok );
    begin
        ok := Al_Fungetc( f, Character'Pos( c ) );
    end Al_Fungetc;

    ----------------------------------------------------------------------------

    procedure Al_Fungetc( f : A_Allegro_File; c : Unsigned_8 ) is
        ok : Integer;
        pragma Warnings( Off, ok );
    begin
        ok := Al_Fungetc( f, Integer(c) );
    end Al_Fungetc;

    ----------------------------------------------------------------------------

    procedure Al_Fwrite32le( f : A_Allegro_File; l : Integer_32 ) is
        wrote : size_t;
        pragma Warnings( Off, wrote );
    begin
        wrote := Al_Fwrite32le( f, l );
    end Al_Fwrite32le;

    ----------------------------------------------------------------------------

    function Al_Fgets( f : A_Allegro_File; max : Positive := 1024 ) return String is

        function C_Al_Fgets( f   : A_Allegro_File;
                             p   : C.char_array;
                             max : size_t ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Fgets, "al_fgets" );

        tmp    : aliased C.char_array(1..C.size_t(max));
        pragma Warnings( Off, tmp );    -- ignore 'read but not assigned'
        result : C.Strings.chars_ptr;
    begin
        result := C_Al_Fgets( f, tmp, tmp'Length );
        if result /= C.Strings.Null_Ptr then
            return To_Ada( Value( result ) );
        else
            return "";
        end if;
    end Al_Fgets;

    ----------------------------------------------------------------------------

    function Al_Fputs( f : A_Allegro_File; p : String ) return Integer is

        function C_Al_Fputs( f : A_Allegro_File; p : C.char_array ) return Integer;
        pragma Import( C, C_Al_Fputs, "al_fputs" );

    begin
        return C_Al_Fputs( f, To_C( p ) );
    end Al_Fputs;

    ----------------------------------------------------------------------------

    procedure Al_Fputs( f : A_Allegro_File; p : String ) is
        ret : Integer;
        pragma Warnings( Off, ret );
    begin
        ret := Al_Fputs( f, p );
    end Al_Fputs;

    ----------------------------------------------------------------------------

    function Al_Fopen_Fd( fd : Integer; mode : String ) return A_Allegro_File is

        function C_Al_Fopen_Fd( fd : Integer; mode : C.char_array ) return A_Allegro_File;
        pragma Import( C, C_Al_Fopen_Fd, "al_fopen_fd" );

    begin
        return C_Al_Fopen_Fd( fd, To_C( mode ) );
    end Al_Fopen_Fd;

    ----------------------------------------------------------------------------

    function Al_Make_Temp_File( template : String;
                                ret_path : access A_Allegro_Path ) return A_Allegro_File is

        function C_Al_Make_Temp_File( template : C.char_array;
                                      ret_path : access A_Allegro_Path ) return A_Allegro_File;
        pragma Import( C, C_Al_Make_Temp_File, "al_make_temp_file" );

    begin
        return C_Al_Make_Temp_File( To_C( template ), ret_path );
    end Al_Make_Temp_File;

end Allegro.File_IO;
