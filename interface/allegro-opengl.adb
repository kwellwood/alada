--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;

package body Allegro.OpenGL is

    function Al_Have_OpenGL_Extension( extension : String ) return Boolean is
        function C_Al_Have_OpenGL_Extension( extension : C.char_array ) return Bool;
        pragma Import( C, C_Al_Have_OpenGL_Extension, "al_have_opengl_extension" );
    begin
        return C_Al_Have_OpenGL_Extension( To_C( extension ) ) = B_TRUE;
    end Al_Have_OpenGL_Extension;

    ----------------------------------------------------------------------------

    function Al_Get_OpenGL_Proc_Address( name : String ) return Address is
        function C_Al_Get_OpenGL_Proc_Address( name : C.char_array ) return Address;
        pragma Import( C, C_Al_Get_OpenGL_Proc_Address, "al_get_opengl_proc_address" );
    begin
        return C_Al_Get_OpenGL_Proc_Address( To_C( name ) );
    end Al_Get_OpenGL_Proc_Address;

    ----------------------------------------------------------------------------

    function Al_Get_OpenGL_Texture_Size( bitmap : A_Allegro_Bitmap;
                                         w, h   : access Integer ) return Boolean is
        function C_Al_Get_OpenGL_Texture_Size( bitmap : A_Allegro_Bitmap;
                                               w, h   : access Integer ) return Bool;
        pragma Import( C, C_Al_Get_OpenGL_Texture_Size, "al_get_opengl_texture_size" );
    begin
        return C_Al_Get_OpenGL_Texture_Size( bitmap, w, h ) = B_TRUE;
    end Al_Get_OpenGL_Texture_Size;

end Allegro.OpenGL;
