--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Color is

    function Image( apf : Allegro_Pixel_Format ) return String is
    begin
        return Allegro_Pixel_Format'Image( apf );
    end Image;

end Allegro.Color;
