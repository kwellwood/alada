--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.File_IO;                   use Allegro.File_IO;
with Interfaces;                        use Interfaces;
with System;                            use System;

-- Allegro 5.2.5 - Memfile interface
package Allegro.Memfile is

    function Al_Open_Memfile( mem  : Address;
                              size : Integer_64;
                              mode : String ) return A_Allegro_File;

    function Al_Get_Allegro_Memfile_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Memfile_Version, "al_get_allegro_memfile_version" );

end Allegro.Memfile;
