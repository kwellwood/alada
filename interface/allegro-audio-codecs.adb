--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Audio.Codecs is

    function Al_Init_ACodec_Addon return Boolean is

        function C_Al_Init_ACodec_Addon return Bool;
        pragma Import( C, C_Al_Init_ACodec_Addon, "al_init_acodec_addon" );

    begin
        return To_Ada( C_Al_Init_ACodec_Addon );
    end Al_Init_ACodec_Addon;

end Allegro.Audio.Codecs;
