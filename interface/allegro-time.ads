--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Time routines
package Allegro.Time is

    type Allegro_Timeout is private;

    function Al_Get_Time return Long_Float;
    pragma Import( C, Al_Get_Time, "al_get_time" );

    function Al_Current_Time return Long_Float renames Al_Get_Time;

    procedure Al_Init_Timeout( timeout : in out Allegro_Timeout; seconds : Long_Float );
    pragma Import( C, Al_Init_Timeout, "al_init_timeout" );

    procedure Al_Rest( seconds : Long_Float );
    pragma Import( C, Al_Rest, "al_rest" );

private

    type Allegro_Timeout is
        record
            pad1 : Unsigned_64;
            pad2 : Unsigned_64;
        end record;
    pragma Convention( C, Allegro_Timeout );

end Allegro.Time;
