--
-- Copyright (c) 2016-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Conversion;
with System;                            use System;

package body Allegro.Windows is

    subtype UINT is Interfaces.C.unsigned;         -- "unsigned int"
    subtype WPARAM_Type  is Interfaces.C.size_t;   -- "unsigned int" OR "unsigned int64" (64 bits when compiled for x64)
    subtype LPARAM_Type  is Interfaces.C.long;     -- "long" OR "int64" (64 bits when compiled for x64)
    subtype LRESULT_Type is LPARAM_Type;           -- identical to LPARAM_Type

    type C_Win_Callback is access function( display  : A_Allegro_Display;
                                            message  : UINT;
                                            wparam   : WPARAM_Type;
                                            lparam   : LPARAM_Type;
                                            result   : access LRESULT_Type;
                                            userdata : Address ) return Bool;
    pragma Convention( C, C_Win_Callback );

    ----------------------------------------------------------------------------

    function Callback_Wrapper( display  : A_Allegro_Display;
                               message  : UINT;
                               wparam   : WPARAM_Type;
                               lparam   : LPARAM_Type;
                               result   : access LRESULT_Type;
                               userdata : Address ) return Bool with Convention => C;

    function Callback_Wrapper( display  : A_Allegro_Display;
                               message  : UINT;
                               wparam   : WPARAM_Type;
                               lparam   : LPARAM_Type;
                               result   : access LRESULT_Type;
                               userdata : Address ) return Bool is

        function Ada_Callback( display : A_Allegro_Display;
                               message : unsigned;
                               wparam  : size_t;
                               lparam  : long;
                               result  : access long ) return Boolean with Import, Convention => Ada, Address => userdata;

    begin
        return (if Ada_Callback( display, message, wparam, lparam, result ) then B_TRUE else B_FALSE);
    end Callback_Wrapper;

    --==========================================================================

    function Al_Win_Add_Window_Callback( display  : A_Allegro_Display;
                                         callback : A_Win_Callback
                                       ) return Boolean is

        function C_Al_Win_Add_Window_Callback( display  : A_Allegro_Display;
                                               callback : C_Win_Callback;
                                               userdata : Address ) return Bool;
        pragma Import( C, C_Al_Win_Add_Window_Callback, "al_win_add_window_callback" );

    begin
        return To_Ada( C_Al_Win_Add_Window_Callback( display, Callback_Wrapper'Access, callback.all'Address ) );
    end Al_Win_Add_Window_Callback;

    ----------------------------------------------------------------------------

    function Al_Win_Remove_Window_Callback( display  : A_Allegro_Display;
                                            callback : A_Win_Callback ) return Boolean is

        function C_Al_Win_Remove_Window_Callback( display  : A_Allegro_Display;
                                                  callback : C_Win_Callback;
                                                  userdata : Address ) return Bool;
        pragma Import( C, C_Al_Win_Remove_Window_Callback, "al_win_remove_window_callback" );

    begin
        return To_Ada( C_Al_Win_Remove_Window_Callback( display, Callback_Wrapper'Access, callback.all'Address ) );
    end Al_Win_Remove_Window_Callback;

end Allegro.Windows;
