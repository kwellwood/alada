--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro_Ids;                       use Allegro_Ids;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Joystick;                  use Allegro.Joystick;
with Allegro.Keyboard;                  use Allegro.Keyboard;
with Allegro.Mouse;                     use Allegro.Mouse;
with Allegro.Time;                      use Allegro.Time;
with Allegro.Timers;                    use Allegro.Timers;
with Allegro.Touch_Input;               use Allegro.Touch_Input;
with Interfaces;                        use Interfaces;
with System;                            use System;

-- Allegro 5.2.5 - Event system and events
package Allegro.Events is

    -- Event types

    subtype Allegro_Event_Type is Unsigned_32;

    ALLEGRO_EVENT_JOYSTICK_AXIS          : constant Allegro_Event_Type := 1;
    ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN   : constant Allegro_Event_Type := 2;
    ALLEGRO_EVENT_JOYSTICK_BUTTON_UP     : constant Allegro_Event_Type := 3;
    ALLEGRO_EVENT_JOYSTICK_CONFIGURATION : constant Allegro_Event_Type := 4;

    ALLEGRO_EVENT_KEY_DOWN               : constant Allegro_Event_Type := 10;
    ALLEGRO_EVENT_KEY_CHAR               : constant Allegro_Event_Type := 11;
    ALLEGRO_EVENT_KEY_UP                 : constant Allegro_Event_Type := 12;

    ALLEGRO_EVENT_MOUSE_AXES             : constant Allegro_Event_Type := 20;
    ALLEGRO_EVENT_MOUSE_BUTTON_DOWN      : constant Allegro_Event_Type := 21;
    ALLEGRO_EVENT_MOUSE_BUTTON_UP        : constant Allegro_Event_Type := 22;
    ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY    : constant Allegro_Event_Type := 23;
    ALLEGRO_EVENT_MOUSE_LEAVE_DISPLAY    : constant Allegro_Event_Type := 24;
    ALLEGRO_EVENT_MOUSE_WARPED           : constant Allegro_Event_Type := 25;

    ALLEGRO_EVENT_TIMER                  : constant Allegro_Event_Type := 30;

    ALLEGRO_EVENT_DISPLAY_EXPOSE         : constant Allegro_Event_Type := 40;
    ALLEGRO_EVENT_DISPLAY_RESIZE         : constant Allegro_Event_Type := 41;
    ALLEGRO_EVENT_DISPLAY_CLOSE          : constant Allegro_Event_Type := 42;
    ALLEGRO_EVENT_DISPLAY_LOST           : constant Allegro_Event_Type := 43;
    ALLEGRO_EVENT_DISPLAY_FOUND          : constant Allegro_Event_Type := 44;
    ALLEGRO_EVENT_DISPLAY_SWITCH_IN      : constant Allegro_Event_Type := 45;
    ALLEGRO_EVENT_DISPLAY_SWITCH_OUT     : constant Allegro_Event_Type := 46;
    ALLEGRO_EVENT_DISPLAY_ORIENTATION    : constant Allegro_Event_Type := 47;
    ALLEGRO_EVENT_DISPLAY_HALT_DRAWING   : constant Allegro_Event_Type := 48;
    ALLEGRO_EVENT_DISPLAY_RESUME_DRAWING : constant Allegro_Event_Type := 49;

    ALLEGRO_EVENT_TOUCH_BEGIN            : constant Allegro_Event_Type := 50;
    ALLEGRO_EVENT_TOUCH_END              : constant Allegro_Event_Type := 51;
    ALLEGRO_EVENT_TOUCH_MOVE             : constant Allegro_Event_Type := 52;
    ALLEGRO_EVENT_TOUCH_CANCEL           : constant Allegro_Event_Type := 53;

    ALLEGRO_EVENT_DISPLAY_CONNECTED      : constant Allegro_Event_Type := 60;
    ALLEGRO_EVENT_DISPLAY_DISCONNECTED   : constant Allegro_Event_Type := 61;

    ----------------------------------------------------------------------------

    -- Type-specific event structures

    type Allegro_Any_Event is
        record
            typ    : Allegro_Event_Type;
            source : A_Allegro_Event_Source;
        end record;
    pragma Convention( C, Allegro_Any_Event );

    type Allegro_Display_Event is
        record
            typ         : Allegro_Event_Type;
            source      : A_Allegro_Display;
            timestamp   : Long_Float;
            x, y        : Integer;
            width,
            height      : Integer;
            orientation : Allegro_Display_Orientation;
        end record;
    pragma Convention( C, Allegro_Display_Event );

    type Allegro_Joystick_Event is
        record
            typ       : Allegro_Event_Type;
            source    : A_Allegro_Joystick;
            timestamp : Long_Float;
            id        : A_Allegro_Joystick;
            stick     : Integer;
            axis      : Integer;
            pos       : Float;
            button    : Integer;
        end record;
    pragma Convention( C, Allegro_Joystick_Event );

    type Allegro_Keyboard_Event is
        record
            typ       : Allegro_Event_Type;
            source    : A_Allegro_Keyboard;
            timestamp : Long_Float;
            display   : A_Allegro_Display;
            keycode   : Integer;
            unichar   : Integer;
            modifiers : Unsigned_32;
            repeat    : Bool;
        end record;
    pragma Convention( C, Allegro_Keyboard_Event );

    type Allegro_Mouse_Event is
        record
            typ            : Allegro_Event_Type;
            source         : A_Allegro_Mouse;
            timestamp      : Long_Float;
            display        : A_Allegro_Display;
            x, y, z, w     : Integer;
            dx, dy, dz, dw : Integer;
            button         : Unsigned_32;
            pressure       : Float;
        end record;
    pragma Convention( C, Allegro_Mouse_Event );

    type Allegro_Timer_Event is
        record
            typ       : Allegro_Event_Type;
            source    : A_Allegro_Timer;
            timestamp : Long_Float;
            count     : Unsigned_64;
            error     : Long_Float;
        end record;
    pragma Convention( C, Allegro_Timer_Event );

    type Allegro_Touch_Event is
        record
            typ       : Allegro_Event_Type;
            source    : A_Allegro_Touch_Input;
            timestamp : Long_Float;
            display   : A_Allegro_Display;
            id        : Integer;  -- Identifier of the event, always positive number.
            x, y      : Float;    -- Touch position on the screen in 1:1 resolution.
            dx, dy    : Float;    -- Relative touch position.
            primary   : Bool;     -- True, if touch is a primary one (usually first one).
        end record;
    pragma Convention( C, Allegro_Touch_Event );

    type Allegro_User_Event is
        record
            typ            : Allegro_Event_Type;
            source         : A_Allegro_Event_Source;
            timestamp      : Long_Float;
            internal_descr : Address;
            data1          : Address;
            data2          : Address;
            data3          : Address;
            data4          : Address;
        end record;
    pragma Convention( C, Allegro_User_Event );
    type A_Allegro_User_Event is access all Allegro_User_Event;

    ----------------------------------------------------------------------------

    -- Event structure

    type Allegro_Event(t : Allegro_Event_Type := 0) is
        record
            case t is
                when 0 =>
                    any      : Allegro_Any_Event;
                when 1..4 =>
                    joystick : Allegro_Joystick_Event;
                when 10..12 =>
                    keyboard : Allegro_Keyboard_Event;
                when 20..25 =>
                    mouse    : Allegro_Mouse_Event;
                when 30 =>
                    timer    : Allegro_Timer_Event;
                when 40..49 =>
                    display  : Allegro_Display_Event;
                when 50..53 =>
                    touch    : Allegro_Touch_Event;
                when others =>
                    user     : Allegro_User_Event;
            end case;
        end record;
     pragma Unchecked_Union( Allegro_Event );
     pragma Convention( C, Allegro_Event );

    ----------------------------------------------------------------------------

    --    1 <= t < 512  - builtin events
    --  512 <= t < 1024 - reserved user events (for addons)
    -- 1024 <= t        - unreserved user events
    function Allegro_Event_Type_Is_User( t : Allegro_Event_Type ) return Boolean with Inline_Always;

    function Allegro_Get_Event_Type( a, b, c, d : Character ) return AL_ID renames Allegro_Ids.To_AL_ID;

    -- Event queues

    type Allegro_Event_Queue is limited private;
    type A_Allegro_Event_Queue is access all Allegro_Event_Queue;

    function Al_Create_Event_Queue return A_Allegro_Event_Queue;
    pragma Import( C, Al_Create_Event_Queue, "al_create_event_queue" );

    procedure Al_Destroy_Event_Queue( queue : in out A_Allegro_Event_Queue );

    function Al_Is_Event_Source_Registered( queue  : A_Allegro_Event_Queue;
                                            source : A_Allegro_Event_Source ) return Boolean;

    procedure Al_Register_Event_Source( queue  : A_Allegro_Event_Queue;
                                        source : A_Allegro_Event_Source );
    pragma Import( C, Al_Register_Event_Source, "al_register_event_source" );

    procedure Al_Unregister_Event_Source( queue  : A_Allegro_Event_Queue;
                                          source : A_Allegro_Event_Source );
    pragma Import( C, Al_Unregister_Event_Source, "al_unregister_event_source" );

    procedure Al_Pause_Event_Queue( queue : A_Allegro_Event_Queue; paused : Boolean );

    function Al_Is_Event_Queue_Paused( queue : A_Allegro_Event_Queue ) return Boolean;

    function Al_Is_Event_Queue_Empty( queue : A_Allegro_Event_Queue ) return Boolean;

    function Al_Event_Queue_Is_Empty( queue : A_Allegro_Event_Queue ) return Boolean renames Al_Is_Event_Queue_Empty;

    function Al_Get_Next_Event( queue     : A_Allegro_Event_Queue;
                                ret_event : access Allegro_Event ) return Boolean;

    function Al_Peek_Next_Event( queue     : A_Allegro_Event_Queue;
                                 ret_event : access Allegro_Event ) return Boolean;

    function Al_Drop_Next_Event( queue : A_Allegro_Event_Queue ) return Boolean;

    procedure Al_Flush_Event_Queue( queue : A_Allegro_Event_Queue );
    pragma Import( C, Al_Flush_Event_Queue, "al_flush_event_queue" );

    procedure Al_Wait_For_Event( queue     : A_Allegro_Event_Queue;
                                 ret_event : in out Allegro_Event );
    pragma Import( C, Al_Wait_For_Event, "al_wait_for_event" );

    function Al_Wait_For_Event_Timed( queue     : A_Allegro_Event_Queue;
                                      ret_event : access Allegro_Event;
                                      secs      : Float ) return Boolean;

    function Al_Wait_For_Event_Until( queue     : A_Allegro_Event_Queue;
                                      ret_event : access Allegro_Event;
                                      timeout   : Allegro_Timeout ) return Boolean;

    -- User events

    procedure Al_Init_User_Event_Source( src : A_Allegro_Event_Source );
    pragma Import( C, Al_Init_User_Event_Source, "al_init_user_event_source" );

    procedure Al_Destroy_User_Event_Source( source : A_Allegro_Event_Source );
    pragma Import( C, Al_Destroy_User_Event_Source, "al_destroy_user_event_source" );

    type A_User_Event_Destructor is access
        procedure( event : A_Allegro_User_Event );
    pragma Convention( C, A_User_Event_Destructor );

    function Al_Emit_User_Event( src        : A_Allegro_Event_Source;
                                 event      : Allegro_Event;
                                 destructor : A_User_Event_Destructor ) return Boolean;

    -- Calls Al_Emit_User_Event, ignoring any errors.
    procedure Al_Emit_User_Event( src        : A_Allegro_Event_Source;
                                  event      : Allegro_Event;
                                  destructor : A_User_Event_Destructor );

    procedure Al_Unref_User_Event( event : A_Allegro_User_Event );
    pragma Import( C, Al_Unref_User_Event, "al_unref_user_event" );

    procedure Al_Set_Event_Source_Data( source : A_Allegro_Event_Source;
                                        data   : Address );
    pragma Import( C, Al_Set_Event_Source_Data, "al_set_event_source_data" );

    function Al_Get_Event_Source_Data( source : A_Allegro_Event_Source ) return Address;
    pragma Import( C, Al_Get_Event_Source_Data, "al_get_event_source_data" );

private

    type Allegro_Event_Queue is limited null record;
    pragma Convention( C, Allegro_Event_Queue );

end Allegro.Events;
