--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Bitmaps.Locking is

    procedure Al_Lock_Bitmap( bitmap : A_Allegro_Bitmap;
                              format : Allegro_Pixel_Format;
                              flags  : Allegro_Lock_Mode ) is
        lock : A_Allegro_Locked_Region;
        pragma Warnings( Off, lock );
    begin
        lock := Al_Lock_Bitmap( bitmap, format, flags );
    end Al_Lock_Bitmap;

    ----------------------------------------------------------------------------

    procedure Al_Lock_Bitmap_Region( bitmap        : A_Allegro_Bitmap;
                                     x, y          : Integer;
                                     width, height : Integer;
                                     format        : Allegro_Pixel_Format;
                                     flags         : Allegro_Lock_Mode ) is
        lock : A_Allegro_Locked_Region;
        pragma Warnings( Off, lock );
    begin
        lock := Al_Lock_Bitmap_Region( bitmap, x, y, width, height, format, flags );
    end Al_Lock_Bitmap_Region;

    ----------------------------------------------------------------------------

    function Al_Is_Bitmap_Locked( bitmap : A_Allegro_Bitmap ) return Boolean is

        function C_Al_Is_Bitmap_Locked( bitmap : A_Allegro_Bitmap ) return Bool;
        pragma Import( C, C_Al_Is_Bitmap_Locked, "al_is_bitmap_locked" );

    begin
        return To_Ada( C_Al_Is_Bitmap_Locked( bitmap ) );
    end Al_Is_Bitmap_Locked;

end Allegro.Bitmaps.Locking;
