--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Displays;                  use Allegro.Displays;

-- Allegro 5.2.5 - Direct3D routines
package Allegro.Direct3D is

    type DIRECT3DDEVICE9 is limited private;
    type LPDIRECT3DDEVICE9 is access all DIRECT3DDEVICE9;

    type DIRECT3DTEXTURE9 is limited private;
    type LPDIRECT3DTEXTURE9 is access all DIRECT3DTEXTURE9;

    function Al_Get_D3D_Device( display : A_Allegro_Display ) return LPDIRECT3DDEVICE9;
    pragma Import( C, Al_Get_D3D_Device, "al_get_d3d_device" );

    function Al_Get_D3D_System_Texture( bitmap : A_Allegro_Bitmap ) return LPDIRECT3DTEXTURE9;
    pragma Import( C, Al_Get_D3D_System_Texture, "al_get_d3d_system_texture" );

    function Al_Get_D3D_Video_Texture( bitmap : A_Allegro_Bitmap ) return LPDIRECT3DTEXTURE9;
    pragma Import( C, Al_Get_D3D_Video_Texture, "al_get_d3d_video_texture" );

    function Al_Have_D3D_Non_Pow2_Texture_Support return Boolean;

    function Al_Have_D3D_Non_Square_Texture_Support return Boolean;

    procedure Al_Get_D3D_Texture_Position( bitmap : A_Allegro_Bitmap;
                                           u, v   : access Integer );
    pragma Import( C, Al_Get_D3D_Texture_Position, "al_get_d3d_texture_position" );

    function Al_Get_D3D_Texture_Size( bitmap        : A_Allegro_Bitmap;
                                      width, height : access Integer ) return Boolean;

    function Al_Is_D3D_Device_Lost( display : A_Allegro_Display ) return Boolean;

    type A_Display_Callback is access procedure( display : A_Allegro_Display );

    procedure Al_Set_D3D_Device_Release_Callback( callback : A_Display_Callback );

    procedure Al_Set_D3D_Device_Restore_Callback( callback : A_Display_Callback );

private

    type DIRECT3DDEVICE9 is limited null record;
    pragma Convention( C, DIRECT3DDEVICE9 );

    type DIRECT3DTEXTURE9 is limited null record;
    pragma Convention( C, DIRECT3DTEXTURE9 );

end Allegro.Direct3D;
