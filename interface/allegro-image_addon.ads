--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Image I/O addon
package Allegro.Image_Addon is

    function Al_Init_Image_Addon return Boolean;

    procedure Al_Shutdown_Image_Addon;
    pragma Import( C, Al_Shutdown_Image_Addon, "al_shutdown_image_addon" );

    function Al_Get_Allegro_Image_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Image_Version, "al_get_allegro_image_version" );

end Allegro.Image_Addon;
