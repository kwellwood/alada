--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Shaders;                   use Allegro.Shaders;
with Interfaces;                        use Interfaces;
with System;                            use System;

-- Allegro 5.2.5 - OpenGL routines
package Allegro.OpenGL is

    subtype GLuint is Unsigned_32;

    type ALLEGRO_OPENGL_VARIANT is new Integer;
    ALLEGRO_DESKTOP_OPENGL : constant ALLEGRO_OPENGL_VARIANT;
    ALLEGRO_OPENGL_ES      : constant ALLEGRO_OPENGL_VARIANT;

    function Al_Get_OpenGL_Version return Unsigned_32;
    pragma Import( C, Al_Get_OpenGL_Version, "al_get_opengl_version" );

    function Al_Have_OpenGL_Extension( extension : String ) return Boolean;

    function Al_Get_OpenGL_Proc_Address( name : String ) return Address;

    -- Will not be implemented.
--  AL_FUNC(ALLEGRO_OGL_EXT_LIST*, al_get_opengl_extension_list, (void));

    function Al_Get_OpenGL_Texture( bitmap : A_Allegro_Bitmap ) return GLuint;
    pragma Import( C, Al_Get_OpenGL_Texture, "al_get_opengl_texture" );

    procedure Al_Remove_OpenGL_FBO( bitmap : A_Allegro_Bitmap );
    pragma Import( C, Al_Remove_OpenGL_FBO, "al_remove_opengl_fbo" );

    function Al_Get_OpenGL_FBO( bitmap : A_Allegro_Bitmap ) return GLuint;
    pragma Import( C, Al_Get_OpenGL_FBO, "al_get_opengl_fbo" );

    function Al_Get_OpenGL_Texture_Size( bitmap : A_Allegro_Bitmap;
                                         w, h   : access Integer ) return Boolean;

    procedure Al_Get_OpenGL_Texture_Position( bitmap : A_Allegro_Bitmap;
                                              u, v   : access Integer );
    pragma Import( C, Al_Get_OpenGL_Texture_Position, "al_get_opengl_texture_position" );

    function Al_Get_OpenGL_Program_Object( shader : A_Allegro_Shader ) return GLuint;
    pragma Import( C, Al_Get_OpenGL_Program_Object, "al_get_opengl_program_object" );

    procedure Al_Set_Current_OpenGL_Context( display : A_Allegro_Display );
    pragma Import( C, Al_Set_Current_OpenGL_Context, "al_set_current_opengl_context" );

    function Al_Get_OpenGL_Variant return ALLEGRO_OPENGL_VARIANT;
    pragma Import( C, Al_Get_OpenGL_Variant, "al_get_opengl_variant" );

private

    ALLEGRO_DESKTOP_OPENGL : constant ALLEGRO_OPENGL_VARIANT := 0;
    ALLEGRO_OPENGL_ES      : constant ALLEGRO_OPENGL_VARIANT := 1;

end Allegro.OpenGL;
