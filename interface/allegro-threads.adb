--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Threads is

    procedure Al_Join_Thread( thread : A_Allegro_Thread ) is

        procedure C_Al_Join_Thread( thread : A_Allegro_Thread; ret_value : in out Address );
        pragma Import( C, C_Al_Join_Thread, "al_join_thread" );

        ret : Address;
        pragma Warnings( Off, ret );
    begin
        C_Al_Join_Thread( thread, ret );
    end Al_Join_Thread;

    ----------------------------------------------------------------------------

    function Al_Get_Thread_Should_Stop( thread : A_Allegro_Thread ) return Boolean is

        function C_Al_Get_Thread_Should_Stop( thread : A_Allegro_Thread ) return Bool;
        pragma Import( C, C_Al_Get_Thread_Should_Stop, "al_get_thread_should_stop" );

    begin
        return To_Ada( C_Al_Get_Thread_Should_Stop( thread ) );
    end Al_Get_Thread_Should_Stop;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Thread( thread : in out A_Allegro_Thread ) is

        procedure C_Al_Destroy_Thread( thread : A_Allegro_Thread );
        pragma Import( C, C_Al_Destroy_Thread, "al_destroy_thread" );

    begin
        if thread /= null then
            C_Al_Destroy_Thread( thread );
            thread := null;
        end if;
    end Al_Destroy_Thread;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Mutex( mutex : in out A_Allegro_Mutex ) is

        procedure C_Al_Destroy_Mutex( mutex : A_Allegro_Mutex );
        pragma Import( C, C_Al_Destroy_Mutex, "al_destroy_mutex" );

    begin
        if mutex /= null then
            C_Al_Destroy_Mutex( mutex );
            mutex := null;
        end if;
    end Al_Destroy_Mutex;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Cond( cond : in out A_Allegro_Cond ) is

        procedure C_Al_Destroy_Cond( cond : A_Allegro_Cond );
        pragma Import( C, C_Al_Destroy_Cond, "al_destroy_cond" );

    begin
        if cond /= null then
            C_Al_Destroy_Cond( cond );
            cond := null;
        end if;
    end Al_Destroy_Cond;

end Allegro.Threads;
