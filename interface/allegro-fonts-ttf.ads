--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.File_IO;                   use Allegro.File_IO;

-- Allegro 5.2.5 - TTF fonts addon
package Allegro.Fonts.TTF is

    -- Initialization

    function Al_Init_TTF_Addon return Boolean;

    procedure Al_Shutdown_TTF_Addon;
    pragma Import( C, Al_Shutdown_TTF_Addon, "al_shutdown_ttf_addon" );

    function Al_Get_Allegro_TTF_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_TTF_Version, "al_get_allegro_ttf_version" );

    -- Loading routines

    ALLEGRO_TTF_NO_KERNING  : constant Font_Loading_Flags := 1;
    ALLEGRO_TTF_MONOCHROME  : constant Font_Loading_Flags := 2;
    ALLEGRO_TTF_NO_AUTOHINT : constant Font_Loading_Flags := 4;

    function Al_Load_TTF_Font( filename : String;
                               size     : Integer;
                               flags    : Font_Loading_Flags ) return A_Allegro_Font;

    -- consumes 'file'
    function Al_Load_TTF_Font_f( file     : A_Allegro_File;
                                 filename : String;
                                 size     : Integer;
                                 flags    : Font_Loading_Flags ) return A_Allegro_Font;

    function Al_Load_TTF_Font_Stretch( filename : String;
                                       w, h     : Integer;
                                       flags    : Font_Loading_Flags ) return A_Allegro_Font;

    -- consumes 'file'
    function Al_Load_TTF_Font_Stretch_f( file     : A_Allegro_File;
                                         filename : String;
                                         w, h     : Integer;
                                         flags    : Font_Loading_Flags ) return A_Allegro_Font;

end Allegro.Fonts.TTF;
