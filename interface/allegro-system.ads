--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Configuration;             use Allegro.Configuration;
with Allegro.Paths;                     use Allegro.Paths;
with Allegro_Ids;                       use Allegro_Ids;
with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - System routines
-- Missing:
--   al_register_assert_handler
package Allegro.System is

    function Al_Get_Allegro_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Version, "al_get_allegro_version" );

    -- AlAda only: Returns the string format of a standard Allegro version,
    -- following the format "major.minor.patch". The same method of packing a
    -- version number into 32 bits used by Al_Get_Allegro_Version is also used
    -- by some Allegro addons.
    function Al_Version_To_String( version : Unsigned_32 ) return String;

    -- AlAda only: Returns the Allegro 5 copyright string
    function Al_Copyright return String;

    function Al_Get_App_Name return String;

    function Al_Get_Org_Name return String;

    type System_Path is private;
    ALLEGRO_RESOURCES_PATH      : constant System_Path;
    ALLEGRO_TEMP_PATH           : constant System_Path;
    ALLEGRO_USER_DATA_PATH      : constant System_Path;
    ALLEGRO_USER_HOME_PATH      : constant System_Path;
    ALLEGRO_USER_SETTINGS_PATH  : constant System_Path;
    ALLEGRO_USER_DOCUMENTS_PATH : constant System_Path;
    ALLEGRO_EXENAME_PATH        : constant System_Path;

    function Al_Get_Standard_Path( id : System_Path ) return A_Allegro_Path;
    pragma Import( C, Al_Get_Standard_Path, "al_get_standard_path" );

    function Al_Get_System_Config return A_Allegro_Config;
    pragma Import( C, Al_Get_System_Config, "al_get_system_config" );

    ALLEGRO_SYSTEM_ID_UNKNOWN     : constant AL_ID := To_AL_ID( 0 );
    ALLEGRO_SYSTEM_ID_XGLX        : constant AL_ID := To_AL_ID( 'X', 'G', 'L', 'X' );
    ALLEGRO_SYSTEM_ID_WINDOWS     : constant AL_ID := To_AL_ID( 'W', 'I', 'N', 'D' );
    ALLEGRO_SYSTEM_ID_MACOSX      : constant AL_ID := To_AL_ID( 'O', 'S', 'X', ' ' );
    ALLEGRO_SYSTEM_ID_ANDROID     : constant AL_ID := To_AL_ID( 'A', 'N', 'D', 'R' );
    ALLEGRO_SYSTEM_ID_IPHONE      : constant AL_ID := To_AL_ID( 'I', 'P', 'H', 'O' );
    ALLEGRO_SYSTEM_ID_GP2XWIZ     : constant AL_ID := To_AL_ID( 'W', 'I', 'Z', ' ' );
    ALLEGRO_SYSTEM_ID_RASPBERRYPI : constant AL_ID := To_AL_ID( 'R', 'A', 'S', 'P' );
    ALLEGRO_SYSTEM_ID_SDL         : constant AL_ID := To_AL_ID( 'S', 'D', 'L', '2' );

    function Al_Get_System_Id return AL_ID;
    pragma Import( C, Al_Get_System_Id, "al_get_system_id" );

    procedure Al_Set_Exe_Name( path : String );

    procedure Al_Set_Org_Name( org_name : String );

    procedure Al_Set_App_Name( app_name : String );

    function Al_Initialize return Boolean;

    procedure Al_Uninstall_System;
    pragma Import( C, Al_Uninstall_System, "al_uninstall_system" );

    function Al_Is_System_Installed return Boolean;

private

    type System_Path is new Integer;
    ALLEGRO_RESOURCES_PATH      : constant System_Path := 0;
    ALLEGRO_TEMP_PATH           : constant System_Path := 1;
    ALLEGRO_USER_DATA_PATH      : constant System_Path := 2;
    ALLEGRO_USER_HOME_PATH      : constant System_Path := 3;
    ALLEGRO_USER_SETTINGS_PATH  : constant System_Path := 4;
    ALLEGRO_USER_DOCUMENTS_PATH : constant System_Path := 5;
    ALLEGRO_EXENAME_PATH        : constant System_Path := 6;

end Allegro.System;
