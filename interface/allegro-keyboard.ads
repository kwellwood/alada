--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Keyboard routines
package Allegro.Keyboard is

    ALLEGRO_KEY_A            : constant Integer := 1;
    ALLEGRO_KEY_B            : constant Integer := 2;
    ALLEGRO_KEY_C            : constant Integer := 3;
    ALLEGRO_KEY_D            : constant Integer := 4;
    ALLEGRO_KEY_E            : constant Integer := 5;
    ALLEGRO_KEY_F            : constant Integer := 6;
    ALLEGRO_KEY_G            : constant Integer := 7;
    ALLEGRO_KEY_H            : constant Integer := 8;
    ALLEGRO_KEY_I            : constant Integer := 9;
    ALLEGRO_KEY_J            : constant Integer := 10;
    ALLEGRO_KEY_K            : constant Integer := 11;
    ALLEGRO_KEY_L            : constant Integer := 12;
    ALLEGRO_KEY_M            : constant Integer := 13;
    ALLEGRO_KEY_N            : constant Integer := 14;
    ALLEGRO_KEY_O            : constant Integer := 15;
    ALLEGRO_KEY_P            : constant Integer := 16;
    ALLEGRO_KEY_Q            : constant Integer := 17;
    ALLEGRO_KEY_R            : constant Integer := 18;
    ALLEGRO_KEY_S            : constant Integer := 19;
    ALLEGRO_KEY_T            : constant Integer := 20;
    ALLEGRO_KEY_U            : constant Integer := 21;
    ALLEGRO_KEY_V            : constant Integer := 22;
    ALLEGRO_KEY_W            : constant Integer := 23;
    ALLEGRO_KEY_X            : constant Integer := 24;
    ALLEGRO_KEY_Y            : constant Integer := 25;
    ALLEGRO_KEY_Z            : constant Integer := 26;

    ALLEGRO_KEY_0            : constant Integer := 27;
    ALLEGRO_KEY_1            : constant Integer := 28;
    ALLEGRO_KEY_2            : constant Integer := 29;
    ALLEGRO_KEY_3            : constant Integer := 30;
    ALLEGRO_KEY_4            : constant Integer := 31;
    ALLEGRO_KEY_5            : constant Integer := 32;
    ALLEGRO_KEY_6            : constant Integer := 33;
    ALLEGRO_KEY_7            : constant Integer := 34;
    ALLEGRO_KEY_8            : constant Integer := 35;
    ALLEGRO_KEY_9            : constant Integer := 36;

    ALLEGRO_KEY_PAD_0        : constant Integer := 37;
    ALLEGRO_KEY_PAD_1        : constant Integer := 38;
    ALLEGRO_KEY_PAD_2        : constant Integer := 39;
    ALLEGRO_KEY_PAD_3        : constant Integer := 40;
    ALLEGRO_KEY_PAD_4        : constant Integer := 41;
    ALLEGRO_KEY_PAD_5        : constant Integer := 42;
    ALLEGRO_KEY_PAD_6        : constant Integer := 43;
    ALLEGRO_KEY_PAD_7        : constant Integer := 44;
    ALLEGRO_KEY_PAD_8        : constant Integer := 45;
    ALLEGRO_KEY_PAD_9        : constant Integer := 46;

    ALLEGRO_KEY_F1           : constant Integer := 47;
    ALLEGRO_KEY_F2           : constant Integer := 48;
    ALLEGRO_KEY_F3           : constant Integer := 49;
    ALLEGRO_KEY_F4           : constant Integer := 50;
    ALLEGRO_KEY_F5           : constant Integer := 51;
    ALLEGRO_KEY_F6           : constant Integer := 52;
    ALLEGRO_KEY_F7           : constant Integer := 53;
    ALLEGRO_KEY_F8           : constant Integer := 54;
    ALLEGRO_KEY_F9           : constant Integer := 55;
    ALLEGRO_KEY_F10          : constant Integer := 56;
    ALLEGRO_KEY_F11          : constant Integer := 57;
    ALLEGRO_KEY_F12          : constant Integer := 58;

    ALLEGRO_KEY_ESCAPE       : constant Integer := 59;
    ALLEGRO_KEY_TILDE        : constant Integer := 60;
    ALLEGRO_KEY_MINUS        : constant Integer := 61;
    ALLEGRO_KEY_EQUALS       : constant Integer := 62;
    ALLEGRO_KEY_BACKSPACE    : constant Integer := 63;
    ALLEGRO_KEY_TAB          : constant Integer := 64;
    ALLEGRO_KEY_OPENBRACE    : constant Integer := 65;
    ALLEGRO_KEY_CLOSEBRACE   : constant Integer := 66;
    ALLEGRO_KEY_ENTER        : constant Integer := 67;
    ALLEGRO_KEY_SEMICOLON    : constant Integer := 68;
    ALLEGRO_KEY_QUOTE        : constant Integer := 69;
    ALLEGRO_KEY_BACKSLASH    : constant Integer := 70;
    ALLEGRO_KEY_BACKSLASH2   : constant Integer := 71; -- DirectInput calls this DIK_OEM_102: "< > | on UK/Germany keyboards"
    ALLEGRO_KEY_COMMA        : constant Integer := 72;
    ALLEGRO_KEY_FULLSTOP     : constant Integer := 73;
    ALLEGRO_KEY_SLASH        : constant Integer := 74;
    ALLEGRO_KEY_SPACE        : constant Integer := 75;

    ALLEGRO_KEY_INSERT       : constant Integer := 76;
    ALLEGRO_KEY_DELETE       : constant Integer := 77;
    ALLEGRO_KEY_HOME         : constant Integer := 78;
    ALLEGRO_KEY_END          : constant Integer := 79;
    ALLEGRO_KEY_PGUP         : constant Integer := 80;
    ALLEGRO_KEY_PGDN         : constant Integer := 81;
    ALLEGRO_KEY_LEFT         : constant Integer := 82;
    ALLEGRO_KEY_RIGHT        : constant Integer := 83;
    ALLEGRO_KEY_UP           : constant Integer := 84;
    ALLEGRO_KEY_DOWN         : constant Integer := 85;

    ALLEGRO_KEY_PAD_SLASH    : constant Integer := 86;
    ALLEGRO_KEY_PAD_ASTERISK : constant Integer := 87;
    ALLEGRO_KEY_PAD_MINUS    : constant Integer := 88;
    ALLEGRO_KEY_PAD_PLUS     : constant Integer := 89;
    ALLEGRO_KEY_PAD_DELETE   : constant Integer := 90;
    ALLEGRO_KEY_PAD_ENTER    : constant Integer := 91;

    ALLEGRO_KEY_PRINTSCREEN  : constant Integer := 92;
    ALLEGRO_KEY_PAUSE        : constant Integer := 93;

    ALLEGRO_KEY_ABNT_C1      : constant Integer := 94;
    ALLEGRO_KEY_YEN          : constant Integer := 95;
    ALLEGRO_KEY_KANA         : constant Integer := 96;
    ALLEGRO_KEY_CONVERT      : constant Integer := 97;
    ALLEGRO_KEY_NOCONVERT    : constant Integer := 98;
    ALLEGRO_KEY_AT           : constant Integer := 99;
    ALLEGRO_KEY_CIRCUMFLEX   : constant Integer := 100;
    ALLEGRO_KEY_COLON2       : constant Integer := 101;
    ALLEGRO_KEY_KANJI        : constant Integer := 102;

    ALLEGRO_KEY_PAD_EQUALS   : constant Integer := 103;  -- MacOS X
    ALLEGRO_KEY_BACKQUOTE    : constant Integer := 104;  -- MacOS X
    ALLEGRO_KEY_SEMICOLON2   : constant Integer := 105;  -- MacOS X
    ALLEGRO_KEY_COMMAND      : constant Integer := 106;  -- MacOS X

    ALLEGRO_KEY_BACK         : constant Integer := 107;  -- Android back key
    ALLEGRO_KEY_VOLUME_UP    : constant Integer := 108;
    ALLEGRO_KEY_VOLUME_DOWN  : constant Integer := 109;

    -- Android game keys
    ALLEGRO_KEY_SEARCH       : constant Integer := 110;
    ALLEGRO_KEY_DPAD_CENTER  : constant Integer := 111;
    ALLEGRO_KEY_BUTTON_X     : constant Integer := 112;
    ALLEGRO_KEY_BUTTON_Y     : constant Integer := 113;
    ALLEGRO_KEY_DPAD_UP      : constant Integer := 114;
    ALLEGRO_KEY_DPAD_DOWN    : constant Integer := 115;
    ALLEGRO_KEY_DPAD_LEFT    : constant Integer := 116;
    ALLEGRO_KEY_DPAD_RIGHT   : constant Integer := 117;
    ALLEGRO_KEY_SELECT       : constant Integer := 118;
    ALLEGRO_KEY_START        : constant Integer := 119;
    ALLEGRO_KEY_BUTTON_L1    : constant Integer := 120;
    ALLEGRO_KEY_BUTTON_R1    : constant Integer := 121;
    ALLEGRO_KEY_BUTTON_L2    : constant Integer := 122;
    ALLEGRO_KEY_BUTTON_R2    : constant Integer := 123;
    ALLEGRO_KEY_BUTTON_A     : constant Integer := 124;
    ALLEGRO_KEY_BUTTON_B     : constant Integer := 125;
    ALLEGRO_KEY_THUMBL       : constant Integer := 126;
    ALLEGRO_KEY_THUMBR       : constant Integer := 127;

    ALLEGRO_KEY_UNKNOWN      : constant Integer := 128;

    ALLEGRO_KEY_LSHIFT       : constant Integer := 215;
    ALLEGRO_KEY_RSHIFT       : constant Integer := 216;
    ALLEGRO_KEY_LCTRL        : constant Integer := 217;
    ALLEGRO_KEY_RCTRL        : constant Integer := 218;
    ALLEGRO_KEY_ALT          : constant Integer := 219;
    ALLEGRO_KEY_ALTGR        : constant Integer := 220;
    ALLEGRO_KEY_LWIN         : constant Integer := 221;
    ALLEGRO_KEY_RWIN         : constant Integer := 222;
    ALLEGRO_KEY_MENU         : constant Integer := 223;
    ALLEGRO_KEY_SCROLLLOCK   : constant Integer := 224;
    ALLEGRO_KEY_NUMLOCK      : constant Integer := 225;
    ALLEGRO_KEY_CAPSLOCK     : constant Integer := 226;

    ALLEGRO_KEY_MODIFIERS : constant Integer := 215;
    ALLEGRO_KEY_MAX       : constant Integer := 227;

    KEYMOD_SHIFT      : constant Unsigned_32 := 16#0001#;
    KEYMOD_CTRL       : constant Unsigned_32 := 16#0002#;
    KEYMOD_ALT        : constant Unsigned_32 := 16#0004#;
    KEYMOD_LWIN       : constant Unsigned_32 := 16#0008#;
    KEYMOD_RWIN       : constant Unsigned_32 := 16#0010#;
    KEYMOD_MENU       : constant Unsigned_32 := 16#0020#;
    KEYMOD_ALTGR      : constant Unsigned_32 := 16#0040#;
    KEYMOD_COMMAND    : constant Unsigned_32 := 16#0080#;
    KEYMOD_SCROLLLOCK : constant Unsigned_32 := 16#0100#;
    KEYMOD_NUMLOCK    : constant Unsigned_32 := 16#0200#;
    KEYMOD_CAPSLOCK   : constant Unsigned_32 := 16#0400#;
    KEYMOD_INALTSEQ   : constant Unsigned_32 := 16#0800#;
    KEYMOD_ACCENT1    : constant Unsigned_32 := 16#1000#;
    KEYMOD_ACCENT2    : constant Unsigned_32 := 16#2000#;
    KEYMOD_ACCENT3    : constant Unsigned_32 := 16#4000#;
    KEYMOD_ACCENT4    : constant Unsigned_32 := 16#8000#;

    type Allegro_Keyboard is limited private;
    type A_Allegro_Keyboard is access all Allegro_Keyboard;

    type Int_Array is array (Integer range <>) of Unsigned_32;
    pragma Convention( C, Int_Array );

    type Allegro_Keyboard_State is
        record
            display  : A_Allegro_Display;
            internal : Int_Array(0..(ALLEGRO_KEY_MAX+31)/32-1);  -- Allegro internal
        end record;
    pragma Convention( C, Allegro_Keyboard_State );

    ----------------------------------------------------------------------------

    -- Keyboard routines

    function Al_Install_Keyboard return Boolean;

    procedure Al_Uninstall_Keyboard;
    pragma Import( C, Al_Uninstall_Keyboard, "al_uninstall_keyboard" );

    function Al_Is_Keyboard_Installed return Boolean;

    function Al_Set_Keyboard_Leds( leds : Integer ) return Boolean;

    function Al_Keycode_To_Name( keycode : Integer ) return String;

    procedure Al_Get_Keyboard_State( ret_state : in out Allegro_Keyboard_State );
    pragma Import( C, Al_Get_Keyboard_State, "al_get_keyboard_state" );

    procedure Al_Clear_Keyboard_State( display : A_Allegro_Display );
    pragma Import( C, Al_Clear_Keyboard_State, "al_clear_keyboard_state" );

    function Al_Key_Down( state   : Allegro_Keyboard_State;
                          keycode : Integer ) return Boolean;

    function Al_Get_Keyboard_Event_Source return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Keyboard_Event_Source, "al_get_keyboard_event_source" );

    -- Returns the allegro keycode for a character. This is not part of Allegro.
    function To_Allegro_Keycode( str : String ) return Integer;

private

    type Allegro_Keyboard is limited null record;
    pragma Convention( C, Allegro_Keyboard );

end Allegro.Keyboard;
