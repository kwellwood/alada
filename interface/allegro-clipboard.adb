--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Memory;                    use Allegro.Memory;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Pointers;

package body Allegro.Clipboard is

    package C_Strings is new Interfaces.C.Pointers( size_t, char, char_array, char'Val( 0 ) );
    use C_Strings;

    --==========================================================================

    function Al_Get_Clipboard_Text( display : A_Allegro_Display ) return String is
        function C_Al_Get_Clipboard_Text( display : A_Allegro_Display ) return C_Strings.Pointer;
        pragma Import( C, C_Al_Get_Clipboard_Text, "al_get_clipboard_text" );

        text : C_Strings.Pointer;
    begin
        text := C_Al_Get_Clipboard_Text( display );
        return result : constant String := To_Ada( Value( text ) ) do
            Al_Free( text.all'Address );
        end return;
    end Al_Get_Clipboard_Text;

    ----------------------------------------------------------------------------

    function Al_Get_Clipboard_Text( display : A_Allegro_Display ) return Unbounded_String is
        function C_Al_Get_Clipboard_Text( display : A_Allegro_Display ) return C_Strings.Pointer;
        pragma Import( C, C_Al_Get_Clipboard_Text, "al_get_clipboard_text" );

        text   : constant C_Strings.Pointer := C_Al_Get_Clipboard_Text( display );
        result : constant Unbounded_String := To_Unbounded_String( To_Ada( Value( text ) ) );
    begin
        Al_Free( text.all'Address );
        return result;
    end Al_Get_Clipboard_Text;

    ----------------------------------------------------------------------------

    function Al_Set_Clipboard_Text( display : A_Allegro_Display; text : String ) return Boolean is
        function C_Al_Set_Clipboard_Text( display : A_Allegro_Display; text : char_array ) return Bool;
        pragma Import( C, C_Al_Set_Clipboard_Text, "al_set_clipboard_text" );
    begin
        return C_Al_Set_Clipboard_Text( display, To_C( text ) ) = B_TRUE;
    end Al_Set_Clipboard_Text;

    ----------------------------------------------------------------------------

    procedure Al_Set_Clipboard_Text( display : A_Allegro_Display; text : String ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Clipboard_Text( display, text );
    end Al_Set_Clipboard_Text;

    ----------------------------------------------------------------------------

    function Al_Set_Clipboard_Text( display : A_Allegro_Display; text : Unbounded_String ) return Boolean is
        function C_Al_Set_Clipboard_Text( display : A_Allegro_Display; text : char_array ) return Bool;
        pragma Import( C, C_Al_Set_Clipboard_Text, "al_set_clipboard_text" );
    begin
        return C_Al_Set_Clipboard_Text( display, To_C( To_String( text ) ) ) = B_TRUE;
    end Al_Set_Clipboard_Text;

    ----------------------------------------------------------------------------

    procedure Al_Set_Clipboard_Text( display : A_Allegro_Display; text : Unbounded_String ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Clipboard_Text( display, text );
    end Al_Set_Clipboard_Text;

    ----------------------------------------------------------------------------

    function Al_Clipboard_Has_Text( display : A_Allegro_Display ) return Boolean is
        function C_Al_Clipboard_Has_Text( display : A_Allegro_Display ) return Bool;
        pragma Import( C, C_Al_Clipboard_Has_Text, "al_clipboard_has_text" );
    begin
        return C_Al_Clipboard_Has_Text( display ) = B_TRUE;
    end Al_Clipboard_Has_Text;

end Allegro.Clipboard;
