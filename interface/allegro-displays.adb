--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;
with System;                            use System;

package body Allegro.Displays is

    procedure Al_Get_New_Display_Option( option     : Display_Option;
                                         value      : out Integer;
                                         importance : out Option_Importance ) is

        function C_Al_Get_New_Display_Option( option     : Display_Option;
                                              importance : access Option_Importance ) return Integer;
        pragma Import( C, C_Al_Get_New_Display_Option, "al_get_new_display_option" );

        tmp : aliased Option_Importance;
    begin
        value := C_Al_Get_New_Display_Option( option, tmp'Access );
        importance := tmp;
    end Al_Get_New_Display_Option;

    ----------------------------------------------------------------------------

    function Al_Get_New_Window_Title return String is
        function C_Al_Get_New_Window_Title return chars_ptr;
        pragma Import( C, C_Al_Get_New_Window_Title, "al_get_new_window_title" );
    begin
        return Value( C_Al_Get_New_Window_Title );
    end Al_Get_New_Window_Title;

    ----------------------------------------------------------------------------

    procedure Al_Set_New_Window_Title( title : String ) is
        procedure C_Al_Set_New_Window_Title( title : char_array );
        pragma Import( C, C_Al_Set_New_Window_Title, "al_set_new_window_title" );
    begin
        C_Al_Set_New_Window_Title( To_C( title ) );
    end Al_Set_New_Window_Title;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Display( display : in out A_Allegro_Display ) is

        procedure C_Al_Destroy_Display( display : A_Allegro_Display );
        pragma Import( C, C_Al_Destroy_Display, "al_destroy_display" );

    begin
        if display /= null then
            C_Al_Destroy_Display( display );
            display := null;
        end if;
    end Al_Destroy_Display;

    ----------------------------------------------------------------------------

    procedure Al_Get_Window_Constraints( display      : A_Allegro_Display;
                                         min_w, min_h : out Integer;
                                         max_w, max_h : out Integer ) is

        function C_Al_Get_Window_Constraints( display      : A_Allegro_Display;
                                              min_w, min_h : access Integer;
                                              max_w, max_h : access Integer ) return Bool;
        pragma Import( C, C_Al_Get_Window_Constraints, "al_get_window_constraints" );

        minw, minh, maxw, maxh : aliased Integer;
    begin
        if C_Al_Get_Window_Constraints( display,
                                        minw'Access, minh'Access,
                                        maxw'Access, maxh'Access ) = B_TRUE
        then
            min_w := minw;
            min_h := minh;
            max_w := maxw;
            max_h := maxh;
        else
            min_w := 0;
            min_h := 0;
            max_w := 0;
            max_h := 0;
        end if;
    end Al_Get_Window_Constraints;

    ----------------------------------------------------------------------------

    function Al_Set_Display_Flag( display : A_Allegro_Display;
                                  flag    : Display_Flags;
                                  onoff   : Boolean ) return Boolean is

        function C_Al_Set_Display_Flag( display : A_Allegro_Display;
                                        flag    : Display_Flags;
                                        onoff   : Bool ) return Bool;
        pragma Import( C, C_Al_Set_Display_Flag, "al_set_display_flag" );

    begin
        return To_Ada( C_Al_Set_Display_Flag( display, flag, Boolean'Pos( onoff ) ) );
    end Al_Set_Display_Flag;

    ----------------------------------------------------------------------------

    procedure Al_Set_Display_Flag( display : A_Allegro_Display;
                                   flag    : Display_Flags;
                                   onoff   : Boolean ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Display_Flag( display, flag, onoff );
    end Al_Set_Display_Flag;

    ----------------------------------------------------------------------------

    procedure Al_Set_Display_Icons( display : A_Allegro_Display;
                                    icons   : Allegro_Bitmap_Array ) is

        procedure C_Al_Set_Display_Icons( display   : A_Allegro_Display;
                                          num_icons : Integer;
                                          icon      : Address );
        pragma Import( C, C_Al_Set_Display_Icons, "al_set_display_icons" );

    begin
        C_Al_Set_Display_Icons( display, icons'Length, icons(icons'First)'Address );
    end Al_Set_Display_Icons;

    ----------------------------------------------------------------------------

    function Al_Set_Window_Constraints( display      : A_Allegro_Display;
                                        min_w, min_h : Integer;
                                        max_w, max_h : Integer ) return Boolean is
        function C_Al_Set_Window_Constraints( display      : A_Allegro_Display;
                                              min_w, min_h : Integer;
                                              max_w, max_h : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Window_Constraints, "al_set_window_constraints" );
    begin
        return C_Al_Set_Window_Constraints( display, min_w, min_h, max_w, max_h ) = B_TRUE;
    end Al_Set_Window_Constraints;

    ----------------------------------------------------------------------------

    procedure Al_Set_Window_Constraints( display      : A_Allegro_Display;
                                         min_w, min_h : Integer;
                                         max_w, max_h : Integer ) is
        tmp : Boolean;
        pragma Warnings( Off, tmp );
    begin
        tmp := Al_Set_Window_Constraints( display, min_w, min_h, max_w, max_h );
    end Al_Set_Window_Constraints;

    ----------------------------------------------------------------------------

    procedure Al_Apply_Window_Constraints( display : A_Allegro_Display; onoff : Boolean ) is
        procedure C_Al_Apply_Window_Constraints( display : A_Allegro_Display; onoff : Bool );
        pragma Import( C, C_Al_Apply_Window_Constraints, "al_apply_window_constraints" );
    begin
        C_Al_Apply_Window_Constraints( display, Boolean'Pos( onoff ) );
    end Al_Apply_Window_Constraints;

    ----------------------------------------------------------------------------

    procedure Al_Set_Window_Title( display : A_Allegro_Display; title : String ) is

        procedure C_Al_Set_Window_Title( display : A_Allegro_Display;
                                         title   : C.char_array );
        pragma Import( C, C_Al_Set_Window_Title, "al_set_window_title" );

    begin
        C_Al_Set_Window_Title( display, To_C( title ) );
    end Al_Set_Window_Title;

    ----------------------------------------------------------------------------

    function Al_Wait_For_Vsync return Boolean is

        function C_Al_Wait_For_Vsync return Bool;
        pragma Import( C, C_Al_Wait_For_Vsync, "al_wait_for_vsync" );

    begin
        return To_Ada( C_Al_Wait_For_Vsync );
    end Al_Wait_For_Vsync;

    ----------------------------------------------------------------------------

    procedure Al_Hold_Bitmap_Drawing( hold : Boolean ) is

        procedure C_Al_Hold_Bitmap_Drawing( hold : Bool );
        pragma Import( C, C_Al_Hold_Bitmap_Drawing, "al_hold_bitmap_drawing" );

    begin
        C_Al_Hold_Bitmap_Drawing( Boolean'Pos( hold ) );
    end Al_Hold_Bitmap_Drawing;

    ----------------------------------------------------------------------------

    function Al_Is_Bitmap_Drawing_Held return Boolean is

        function C_Al_Is_Bitmap_Drawing_Held return Bool;
        pragma Import( C, C_Al_Is_Bitmap_Drawing_Held, "al_is_bitmap_drawing_held" );

    begin
        return To_Ada( C_Al_Is_Bitmap_Drawing_Held );
    end Al_Is_Bitmap_Drawing_Held;

    ----------------------------------------------------------------------------

    function Al_Inhibit_Screensaver( inhibit : Boolean ) return Boolean is

        function C_Al_Inhibit_Screensaver( inhibit : Bool ) return Bool;
        pragma Import( C, C_Al_Inhibit_Screensaver, "al_inhibit_screensaver" );

    begin
        return To_Ada( C_Al_Inhibit_Screensaver( Boolean'Pos( inhibit ) ) );
    end Al_Inhibit_Screensaver;

    ----------------------------------------------------------------------------

    function Al_Is_Compatible_Bitmap( bitmap : A_Allegro_Bitmap ) return Boolean is

        function C_Al_Is_Compatible_Bitmap( bitmap : A_Allegro_Bitmap ) return Bool;
        pragma Import( C, C_Al_Is_Compatible_Bitmap, "al_is_compatible_bitmap" );

    begin
        return To_Ada( C_Al_Is_Compatible_Bitmap( bitmap ) );
    end Al_Is_Compatible_Bitmap;

    ----------------------------------------------------------------------------

    function Al_Acknowledge_Resize( display : A_Allegro_Display ) return Boolean is

        function C_Al_Acknowledge_Resize( display : A_Allegro_Display ) return Bool;
        pragma Import( C, C_Al_Acknowledge_Resize, "al_acknowledge_resize" );

    begin
        return To_Ada( C_Al_Acknowledge_Resize( display ) );
    end Al_Acknowledge_Resize;

    ----------------------------------------------------------------------------

    procedure Al_Acknowledge_Resize( display : A_Allegro_Display ) is
    begin
        if Al_Acknowledge_Resize( display ) then
            null;
        end if;
    end Al_Acknowledge_Resize;

    ----------------------------------------------------------------------------

    function Al_Resize_Display( display       : A_Allegro_Display;
                                width, height : Integer ) return Boolean is

        function C_Al_Resize_Display( display       : A_Allegro_Display;
                                      width, height : Integer ) return Bool;
        pragma Import( C, C_Al_Resize_Display, "al_resize_display" );

    begin
        return To_Ada( C_Al_Resize_Display( display, width, height ) );
    end Al_Resize_Display;

    ----------------------------------------------------------------------------

    procedure Al_Resize_Display( display       : A_Allegro_Display;
                                 width, height : Integer ) is
    begin
        if Al_Resize_Display( display, width, height ) then
            null;
        end if;
    end Al_Resize_Display;

end Allegro.Displays;
