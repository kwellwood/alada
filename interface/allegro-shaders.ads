--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;
with Allegro.Transformations;           use Allegro.Transformations;

-- Allegro 5.2.5 - Shaders routines
package Allegro.Shaders is

    type Allegro_Shader is limited private;
    type A_Allegro_Shader is access all Allegro_Shader;

    type Allegro_Shader_Type is private;
    ALLEGRO_VERTEX_SHADER : constant Allegro_Shader_Type;
    ALLEGRO_PIXEL_SHADER  : constant Allegro_Shader_Type;

    type Allegro_Shader_Platform is private;
    ALLEGRO_SHADER_AUTO : constant Allegro_Shader_Platform;
    ALLEGRO_SHADER_GLSL : constant Allegro_Shader_Platform;
    ALLEGRO_SHADER_HLSL : constant Allegro_Shader_Platform;

    -- Shader variable names
    ALLEGRO_SHADER_VAR_COLOR           : constant String := "al_color";
    ALLEGRO_SHADER_VAR_POS             : constant String := "al_pos";
    ALLEGRO_SHADER_VAR_PROJVIEW_MATRIX : constant String := "al_projview_matrix";
    ALLEGRO_SHADER_VAR_TEX             : constant String := "al_tex";
    ALLEGRO_SHADER_VAR_TEXCOORD        : constant String := "al_texcoord";
    ALLEGRO_SHADER_VAR_TEX_MATRIX      : constant String := "al_tex_matrix";
    ALLEGRO_SHADER_VAR_USER_ATTR       : constant String := "al_user_attr_";
    ALLEGRO_SHADER_VAR_USE_TEX         : constant String := "al_use_tex";
    ALLEGRO_SHADER_VAR_USE_TEX_MATRIX  : constant String := "al_use_tex_matrix";

    function Al_Create_Shader( platform : Allegro_Shader_Platform ) return A_Allegro_Shader;
    pragma Import( C, Al_Create_Shader, "al_create_shader" );

    function Al_Attach_Shader_Source( shader : A_Allegro_Shader;
                                      typ    : Allegro_Shader_Type;
                                      source : String ) return Boolean;

    procedure Al_Attach_Shader_Source( shader : A_Allegro_Shader;
                                       typ    : Allegro_Shader_Type;
                                       source : String );

    function Al_Attach_Shader_Source_File( shader   : A_Allegro_Shader;
                                           typ      : Allegro_Shader_Type;
                                           filename : String ) return Boolean;

    function Al_Build_Shader( shader : A_Allegro_Shader ) return Boolean;

    procedure Al_Build_Shader( shader : A_Allegro_Shader );

    function Al_Get_Shader_Log( shader : A_Allegro_Shader ) return String;

    function Al_Get_Shader_Platform( shader : A_Allegro_Shader ) return Allegro_Shader_Platform;
    pragma Import( C, Al_Get_Shader_Platform, "al_get_shader_platform" );

    function Al_Use_Shader( shader : A_Allegro_Shader ) return Boolean;

    procedure Al_Use_Shader( shader : A_Allegro_Shader );

    procedure Al_Destroy_Shader( shader : in out A_Allegro_Shader );

    function Al_Set_Shader_Sampler( name : String; bitmap : A_Allegro_Bitmap; unit : Integer ) return Boolean;

    procedure Al_Set_Shader_Sampler( name : String; bitmap : A_Allegro_Bitmap; unit : Integer );

    function Al_Set_Shader_Matrix( name : String; matrix : Allegro_Transform ) return Boolean;

    procedure Al_Set_Shader_Matrix( name : String; matrix : Allegro_Transform );

    function Al_Set_Shader_Int( name : String; i : Integer ) return Boolean;

    procedure Al_Set_Shader_Int( name : String; i : Integer );

    function Al_Set_Shader_Float( name : String; f : Float ) return Boolean;

    procedure Al_Set_Shader_Float( name : String; f : Float );

    function Al_Set_Shader_Int_Vector( name : String; num_components : Integer; i : Integer_Array ) return Boolean;
    pragma Precondition( i'Length > 0 );
    pragma Precondition( i'Length mod num_components = 0 );

    procedure Al_Set_Shader_Int_Vector( name : String; num_components : Integer; i : Integer_Array );
    pragma Precondition( i'Length > 0 );
    pragma Precondition( i'Length mod num_components = 0 );

    function Al_Set_Shader_Int_Vector( name : String; i : Integer_Array ) return Boolean;
    pragma Precondition( i'Length > 0 );

    procedure Al_Set_Shader_Int_Vector( name : String; i : Integer_Array );
    pragma Precondition( i'Length > 0 );

    function Al_Set_Shader_Float_Vector( name : String; num_components : Integer; f : Float_Array ) return Boolean;
    pragma Precondition( f'Length > 0 );
    pragma Precondition( f'Length mod num_components = 0 );

    procedure Al_Set_Shader_Float_Vector( name : String; num_components : Integer; f : Float_Array );
    pragma Precondition( f'Length > 0 );
    pragma Precondition( f'Length mod num_components = 0 );

    function Al_Set_Shader_Float_Vector( name : String; f : Float_Array ) return Boolean;
    pragma Precondition( f'Length > 0 );

    procedure Al_Set_Shader_Float_Vector( name : String; f : Float_Array );
    pragma Precondition( f'Length > 0 );

    function Al_Set_Shader_Float_Vector( name : String; color : Allegro_Color ) return Boolean;

    procedure Al_Set_Shader_Float_Vector( name : String; color : Allegro_Color );

    function Al_Set_Shader_Bool( name : String; b : Boolean ) return Boolean;

    procedure Al_Set_Shader_Bool( name : String; b : Boolean );

    function Al_Get_Default_Shader_Source( platform : Allegro_Shader_Platform;
                                           typ      : Allegro_Shader_Type ) return String;

private

    type Allegro_Shader is limited null record;

    type Allegro_Shader_Type is new Integer;
    ALLEGRO_VERTEX_SHADER : constant Allegro_Shader_Type := 1;
    ALLEGRO_PIXEL_SHADER  : constant Allegro_Shader_Type := 2;

    type Allegro_Shader_Platform is new Integer;
    ALLEGRO_SHADER_AUTO : constant Allegro_Shader_Platform := 0;
    ALLEGRO_SHADER_GLSL : constant Allegro_Shader_Platform := 1;
    ALLEGRO_SHADER_HLSL : constant Allegro_Shader_Platform := 2;

end Allegro.Shaders;
