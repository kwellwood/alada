--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Paths;                     use Allegro.Paths;
with Allegro.UTF8;                      use Allegro.UTF8;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;
with System;                            use System;

-- Allegro 5.2.5 - File I/O
package Allegro.File_IO is

    type Allegro_File is limited private;
    type A_Allegro_File is access all Allegro_File;

    type Allegro_Seek is (ALLEGRO_SEEK_SET, ALLEGRO_SEEK_CUR, ALLEGRO_SEEK_END);
    pragma Convention( C, Allegro_Seek );

    type fi_fopen     is access function( path : C.Strings.chars_ptr; mode : C.Strings.chars_ptr ) return Address;
    type fi_fclose    is access function( handle : A_Allegro_File ) return Bool;
    type fi_fread     is access function( f : A_Allegro_File; ptr : Address; size : size_t ) return size_t;
    type fi_fwrite    is access function( f : A_Allegro_File; ptr : Address; size : size_t ) return size_t;
    type fi_fflush    is access function( f : A_Allegro_File ) return Bool;
    type fi_ftell     is access function( f : A_Allegro_File ) return Integer_64;
    type fi_fseek     is access function( f : A_Allegro_File; offset : Integer_64; whence : Allegro_Seek ) return Bool;
    type fi_feof      is access function( f : A_Allegro_File ) return Bool;
    type fi_ferror    is access function( f : A_Allegro_File ) return Integer;
    type fi_ferrmsg   is access function( f : A_Allegro_File ) return C.Strings.chars_ptr;
    type fi_fclearerr is access procedure( f : A_Allegro_File );
    type fi_fungetc   is access function( f : A_Allegro_File; c : Integer ) return Integer;
    type fi_fsize     is access function( f : A_Allegro_File ) return off_t;

    pragma Convention( C, fi_fopen );
    pragma Convention( C, fi_fclose );
    pragma Convention( C, fi_fread );
    pragma Convention( C, fi_fwrite );
    pragma Convention( C, fi_fflush );
    pragma Convention( C, fi_ftell );
    pragma Convention( C, fi_fseek );
    pragma Convention( C, fi_feof );
    pragma Convention( C, fi_ferror );
    pragma Convention( C, fi_ferrmsg );
    pragma Convention( C, fi_fclearerr );
    pragma Convention( C, fi_fungetc );
    pragma Convention( C, fi_fsize );

    type Allegro_File_Interface is
        record
            fopen     : fi_fopen;
            fclose    : fi_fclose;
            fread     : fi_fread;
            fwrite    : fi_fwrite;
            fflush    : fi_fflush;
            ftell     : fi_ftell;
            fseek     : fi_fseek;
            feof      : fi_feof;
            ferror    : fi_ferror;
            ferrmsg   : fi_ferrmsg;
            fclearerr : fi_fclearerr;
            fungetc   : fi_fungetc;
            fsize     : fi_fsize;
        end record;
    pragma Convention( C, Allegro_File_Interface );
    type A_Allegro_File_Interface is access all Allegro_File_Interface;
    pragma No_Strict_Aliasing( A_Allegro_File_Interface );

    ----------------------------------------------------------------------------

    function Al_Fopen( path : String; mode : String ) return A_Allegro_File;

    function Al_Fopen_Slice( f            : A_Allegro_File;
                             initial_size : size_t;
                             mode         : String ) return A_Allegro_File;

    function Al_Fopen_Interface( vt   : A_Allegro_File_Interface;
                                 path : String;
                                 mode : String ) return A_Allegro_File;

    function Al_Fclose( f : A_Allegro_File ) return Boolean;

    -- Ignores any error
    procedure Al_Fclose( f : in out A_Allegro_File );
    pragma Postcondition( f = null );

    function Al_Fread( f : A_Allegro_File; ptr : Address; size : size_t ) return size_t;
    pragma Import( C, Al_Fread, "al_fread" );

    function Al_Fwrite( f : A_Allegro_File; ptr : Address; size : size_t ) return size_t;
    pragma Import( C, Al_Fwrite, "al_fwrite" );

    -- Calls Al_Fwrite32le, ignoring any errors;
    procedure Al_Fwrite( f : A_Allegro_File; ptr : Address; size : size_t );

    function Al_Fflush( f : A_Allegro_File ) return Boolean;

    function Al_Ftell( f : A_Allegro_File ) return Integer_64;    -- return off_t
    pragma Import( C, Al_Ftell, "al_ftell" );

    function Al_Fseek( f : A_Allegro_File; offset : Integer_64; whence : Allegro_Seek ) return Boolean;

    -- Calls Al_Fseek, ignoring any errors.
    procedure Al_Fseek( f : A_Allegro_File; offset : Integer_64; whence : Allegro_Seek );

    function Al_Feof( f : A_Allegro_File ) return Boolean;

    function Al_Ferror( f : A_Allegro_File ) return Integer;
    pragma Import( C, Al_Ferror, "al_ferror" );

    function Al_Ferrmsg( f : A_Allegro_File ) return String;

    procedure Al_Fclearerr( f : A_Allegro_File );
    pragma Import( C, Al_Fclearerr, "al_fclearerr" );

    function Al_Fungetc( f : A_Allegro_File; c : Integer ) return Integer;
    pragma Import( C, Al_Fungetc, "al_fungetc" );

    -- Calls Al_Fungetc, ignoring any errors.
    procedure Al_Fungetc( f : A_Allegro_File; c : Character );

    -- Calls Al_Fungetc, ignoring any errors.
    procedure Al_Fungetc( f : A_Allegro_File; c : Unsigned_8 );

    function Al_Fsize( f : A_Allegro_File ) return Integer_64;
    pragma Import( C, Al_Fsize, "al_fsize" );

    function Al_Fgetc( f : A_Allegro_File ) return Integer;
    pragma Import( C, Al_Fgetc, "al_fgetc" );

    function Al_Fputc( f : A_Allegro_File; c : Integer ) return Integer;
    pragma Import( C, Al_Fputc, "al_fputc" );

    function Al_Fread16le( f : A_Allegro_File ) return Integer_16;
    pragma Import( C, Al_Fread16le, "al_fread16le" );

    function Al_Fread16be( f : A_Allegro_File ) return Integer_16;
    pragma Import( C, Al_Fread16be, "al_fread16be" );

    function Al_Fwrite16le( f : A_Allegro_File; w : Integer_16 ) return size_t;
    pragma Import( C, Al_Fwrite16le, "al_fwrite16le" );

    function Al_Fwrite16be( f : A_Allegro_File; w : Integer_16 ) return size_t;
    pragma Import( C, Al_Fwrite16be, "al_fwrite16be" );

    function Al_Fread32le( f : A_Allegro_File ) return Integer_32;
    pragma Import( C, Al_Fread32le, "al_fread32le" );

    function Al_Fread32be( f : A_Allegro_File ) return Integer_32;
    pragma Import( C, Al_Fread32be, "al_fread32be" );

    function Al_Fwrite32le( f : A_Allegro_File; l : Integer_32 ) return size_t;
    pragma Import( C, Al_Fwrite32le, "al_fwrite32le" );

    -- Calls Al_Fwrite32le, ignoring any errors;
    procedure Al_Fwrite32le( f : A_Allegro_File; l : Integer_32 );

    function Al_Fwrite32be( f : A_Allegro_File; l : Integer_32 ) return size_t;
    pragma Import( C, Al_Fwrite32be, "al_fwrite32be" );

    function Al_Fgets( f : A_Allegro_File; max : Positive := 1024 ) return String;

    function Al_Fget_Ustr( f : A_Allegro_File ) return A_Allegro_Ustr;
    pragma Import( C, Al_Fget_Ustr, "al_fget_ustr" );

    function Al_Fputs( f : A_Allegro_File; p : String ) return Integer;

    -- Calls Al_Fputs, ignoring any errors.
    procedure Al_Fputs( f : A_Allegro_File; p : String );

    -- Will not implement- variable argument list
    --AL_FUNC(int, al_fprintf, (ALLEGRO_FILE *f, const char *format, ...));

    -- Will not implement- variable argument list
    --AL_FUNC(int, al_vfprintf, (ALLEGRO_FILE *f, const char* format, va_list args));

    -- Standard I/O specific routines

    function Al_Fopen_Fd( fd : Integer; mode : String ) return A_Allegro_File;

    function Al_Make_Temp_File( template : String; ret_path : access A_Allegro_Path ) return A_Allegro_File;

    -- Alternative file streams

    procedure Al_Set_New_File_Interface( file_interface : A_Allegro_File_Interface );
    pragma Import( C, Al_Set_New_File_Interface, "al_set_new_file_interface" );

    procedure Al_Set_Standard_File_Interface;
    pragma Import( C, Al_Set_Standard_File_Interface, "al_set_standard_file_interface" );

    function Al_Get_New_File_Interface return A_Allegro_File_Interface;
    pragma Import( C, Al_Get_New_File_Interface, "al_get_new_file_interface" );

    function Al_Create_File_Handle( vt : A_Allegro_File_Interface; userdata : Address ) return A_Allegro_File;
    pragma Import( C, Al_Create_File_Handle, "al_create_file_handle" );

    function Al_Get_File_Userdata( f : A_Allegro_File ) return Address;
    pragma Import( C, Al_Get_File_Userdata, "al_get_file_userdata" );

private

    type Allegro_File is limited null record;
    pragma Convention( C, Allegro_File );

end Allegro.File_IO;
