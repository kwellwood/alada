--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Text_IO;
with Ada.Unchecked_Deallocation;

package body Allegro is

    procedure Delete( s : in out A_Allegro_Event_Source ) is
        procedure Free is new Ada.Unchecked_Deallocation( Allegro_Event_Source, A_Allegro_Event_Source );
    begin
        Free( s );
    end Delete;

    ----------------------------------------------------------------------------

    function To_Ada( b : Bool ) return Boolean is
    begin
        return b /= 0;
    end To_Ada;

    ----------------------------------------------------------------------------

    procedure Set_Ada_Main( main : Ada_Main_Procedure ) is
    begin
        if ada_main = null then
            ada_main := main;
        else
            Ada.Text_IO.Put_Line( "Multiple main procedures" );
            raise Program_Error with "Multiple main procedures";
        end if;
    end Set_Ada_Main;

    ----------------------------------------------------------------------------

    -- This is exported to C to be called by main() in libalada.a
    procedure AlAda_Main;
    pragma Export( C, AlAda_Main, "alada_main" );

    procedure AlAda_Main is
    begin
        if ada_main /= null then
            ada_main.all;
        else
            Ada.Text_IO.Put_Line( "No main procedure" );
            raise Program_Error with "No main procedure";
        end if;
    end AlAda_Main;

end Allegro;
