--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C.Strings;              use Interfaces.C.Strings;

package body Allegro.Fonts is

    function Al_Init_Font_Addon return Boolean is

        function C_Al_Init_Font_Addon return Bool;
        pragma Import( C, C_Al_Init_Font_Addon, "al_init_font_addon" );

    begin
        return To_Ada( C_Al_Init_Font_Addon );
    end Al_Init_Font_Addon;

    ----------------------------------------------------------------------------

    function Al_Load_Font( filename : String;
                           size     : Integer;
                           flags    : Font_Loading_Flags ) return A_Allegro_Font is

        function C_Al_Load_Font( filename : C.char_array;
                                 size     : Integer;
                                 flags    : Font_Loading_Flags ) return A_Allegro_Font;
        pragma Import( C, C_Al_Load_Font, "al_load_font" );

    begin
        return C_Al_Load_Font( To_C( filename ), size, flags );
    end Al_Load_Font;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Font( f : in out A_Allegro_Font ) is

        procedure C_Al_Destroy_Font( f : A_Allegro_Font );
        pragma Import( C, C_Al_Destroy_Font, "al_destroy_font" );

    begin
        if f /= null then
            C_Al_Destroy_Font( f );
            f := null;
        end if;
    end Al_Destroy_Font;

    ----------------------------------------------------------------------------

    function Al_Register_Font_Loader( ext : String; load : A_Loader_Proc ) return Boolean is

        function C_Al_Register_Font_Loader( ext  : C.char_array;
                                            load : A_Loader_Proc ) return Bool;
        pragma Import( C, C_Al_Register_Font_Loader, "al_register_font_loader" );

    begin
        return To_Ada( C_Al_Register_Font_Loader( To_C( ext ), load ) );
    end Al_Register_Font_Loader;

    ----------------------------------------------------------------------------

    procedure Al_Get_Font_Ranges( font   : A_Allegro_Font;
                                  ranges : in out Font_Ranges;
                                  total  : out Integer ) is

        function C_Al_Get_Font_Ranges( font         : A_Allegro_Font;
                                       ranges_count : Integer;
                                       ranges       : Address ) return Integer;
        pragma Import( C, C_Al_Get_Font_Ranges, "al_get_font_ranges" );

    begin
        total := C_Al_Get_Font_Ranges( font, ranges'Length, ranges(ranges'First)'Address );
    end Al_Get_Font_Ranges;

    ----------------------------------------------------------------------------

    function Al_Get_Font_Ranges( font : A_Allegro_Font ) return Integer is
        function C_Al_Get_Font_Ranges( font         : A_Allegro_Font;
                                       ranges_count : Integer;
                                       ranges       : Address ) return Integer;
        pragma Import( C, C_Al_Get_Font_Ranges, "al_get_font_ranges" );
    begin
        return C_Al_Get_Font_Ranges( font, 0, Null_Address );
    end Al_Get_Font_Ranges;

    ----------------------------------------------------------------------------

    function Al_Get_Glyph_Dimensions( f         : A_Allegro_Font;
                                      codepoint : Integer_32;
                                      bbx, bby  : access Integer;
                                      bbw, bbh  : access Integer ) return Boolean is

        function C_Al_Get_Glyph_Dimensions( f         : A_Allegro_Font;
                                            codepoint : Integer_32;
                                            bbx, bby  : access Integer;
                                            bbw, bbh  : access Integer ) return Bool;
        pragma Import( C, C_Al_Get_Glyph_Dimensions, "al_get_glyph_dimensions" );

    begin
        return To_Ada( C_Al_Get_Glyph_Dimensions( f, codepoint, bbx, bby, bbw, bbh ) );
    end Al_Get_Glyph_Dimensions;

    ----------------------------------------------------------------------------

    procedure Al_Get_Glyph_Dimensions( f         : A_Allegro_Font;
                                       codepoint : Integer_32;
                                       bbx, bby  : out Integer;
                                       bbw, bbh  : out Integer ) is

        function C_Al_Get_Glyph_Dimensions( f         : A_Allegro_Font;
                                            codepoint : Integer_32;
                                            bbx, bby  : access Integer;
                                            bbw, bbh  : access Integer ) return Bool;
        pragma Import( C, C_Al_Get_Glyph_Dimensions, "al_get_glyph_dimensions" );

        bx, by, bw, bh : aliased Integer;
    begin
        if C_Al_Get_Glyph_Dimensions( f, codepoint, bx'Access, by'Access, bw'Access, bh'Access ) = B_TRUE then
            bbx := bx;
            bby := by;
            bbw := bw;
            bbh := bh;
        else
            bbx := 0;
            bby := 0;
            bbw := 0;
            bbh := 0;
        end if;
    end Al_Get_Glyph_Dimensions;

    ----------------------------------------------------------------------------

    function Al_Get_Glyph_Advance( f          : A_Allegro_Font;
                                   codepoint1 : Character;
                                   codepoint2 : Character ) return Integer is
    begin
        return Al_Get_Glyph_Advance( f, Character'Pos( codepoint1 ), Character'Pos( codepoint2 ) );
    end Al_Get_Glyph_Advance;

    ----------------------------------------------------------------------------

    function Al_Get_Glyph( f              : A_Allegro_Font;
                           prev_codepoint,
                           codepoint      : Integer_32;
                           glyph          : access Allegro_Glyph ) return Boolean is
        function C_Al_Get_Glyph( f              : A_Allegro_Font;
                                 prev_codepoint,
                                 codepoint      : Integer_32;
                                 glyph          : Address ) return Bool;
        pragma Import( C, C_Al_Get_Glyph, "al_get_glyph" );
    begin
        return To_Ada( C_Al_Get_Glyph( f, prev_codepoint, codepoint, glyph.all'Address ) );
    end Al_Get_Glyph;

    ----------------------------------------------------------------------------

    function Al_Get_Text_Width( f : A_Allegro_Font; str : String ) return Integer is

        function C_Al_Get_Text_Width( f : A_Allegro_Font; str : C.char_array ) return Integer;
        pragma Import( C, C_Al_Get_Text_Width, "al_get_text_width" );

    begin
        return C_Al_Get_Text_Width( f, To_C( str ) );
    end Al_Get_Text_Width;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Text( font  : A_Allegro_Font;
                            color : Allegro_Color;
                            x, y  : Float;
                            flags : Font_Draw_Flags;
                            text  : String ) is

        procedure C_Al_Draw_Text( font  : A_Allegro_Font;
                                  color : Allegro_Color;
                                  x, y  : Float;
                                  flags : Font_Draw_Flags;
                                  text  : C.char_array );
        pragma Import( C, C_Al_Draw_Text, "al_draw_text" );

    begin
        C_Al_Draw_Text( font, color, x, y, flags, To_C( text ) );
    end Al_Draw_Text;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Justified_Text( font   : A_Allegro_Font;
                                      color  : Allegro_Color;
                                      x1, x2 : Float;
                                      y      : Float;
                                      diff   : Float;
                                      flags  : Font_Draw_Flags;
                                      text   : String ) is

        procedure C_Al_Draw_Justified_Text( font   : A_Allegro_Font;
                                  color  : Allegro_Color;
                                  x1, x2 : Float;
                                  y      : Float;
                                  diff   : Float;
                                  flags  : Font_Draw_Flags;
                                  text   : C.char_array );
        pragma Import( C, C_Al_Draw_Justified_Text, "al_draw_justified_text" );

    begin
        C_Al_Draw_Justified_Text( font, color, x1, x2, y, diff, flags, To_C( text ) );
    end Al_Draw_Justified_Text;

    ----------------------------------------------------------------------------

    procedure Al_Get_Text_Dimensions( f    : A_Allegro_Font;
                                      text : String;
                                      bbx  : out Integer;
                                      bby  : out Integer;
                                      bbw  : out Integer;
                                      bbh  : out Integer ) is

        procedure C_Al_Get_Text_Dimensions( f    : A_Allegro_Font;
                                            text : C.char_array;
                                            bbx  : access Integer;
                                            bby  : access Integer;
                                            bbw  : access Integer;
                                            bbh  : access Integer );
        pragma Import( C, C_Al_Get_Text_Dimensions, "al_get_text_dimensions" );

        abbx, abby, abbw, abbh : aliased Integer;
    begin
        C_Al_Get_Text_Dimensions( f, To_C( text ), abbx'Access, abby'Access,
                                  abbw'Access, abbh'Access );
        bbx := abbx;
        bby := abby;
        bbw := abbw;
        bbh := abbh;
    end Al_Get_Text_Dimensions;

    ----------------------------------------------------------------------------

    procedure Al_Get_Ustr_Dimensions( f    : A_Allegro_Font;
                                      text : A_Allegro_Ustr;
                                      bbx  : out Integer;
                                      bby  : out Integer;
                                      bbw  : out Integer;
                                      bbh  : out Integer ) is

        procedure C_Al_Get_Ustr_Dimensions( f    : A_Allegro_Font;
                                            text : A_Allegro_Ustr;
                                            bbx  : access Integer;
                                            bby  : access Integer;
                                            bbw  : access Integer;
                                            bbh  : access Integer );
        pragma Import( C, C_Al_Get_Ustr_Dimensions, "al_get_ustr_dimensions" );

        abbx, abby, abbw, abbh : aliased Integer;
    begin
        C_Al_Get_Ustr_Dimensions( f, text, abbx'Access, abby'Access, abbw'Access,
                                  abbh'Access );
        bbx := abbx;
        bby := abby;
        bbw := abbw;
        bbh := abbh;
    end Al_Get_Ustr_Dimensions;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Multiline_Text( font        : A_Allegro_Font;
                                      color       : Allegro_Color;
                                      x, y        : Float;
                                      max_width   : Float;
                                      line_height : Float;
                                      flags       : Font_Draw_Flags;
                                      text        : String ) is

        procedure C_Al_Draw_Multiline_Text( font        : A_Allegro_Font;
                                            color       : Allegro_Color;
                                            x, y        : Float;
                                            max_width   : Float;
                                            line_height : Float;
                                            flags       : Font_Draw_Flags;
                                            text        : C.char_array );
        pragma Import( C, C_Al_Draw_Multiline_Text, "al_draw_multiline_text" );

    begin
        C_Al_Draw_Multiline_Text( font, color, x, y, max_width, line_height, flags, To_C( text ) );
    end Al_Draw_Multiline_Text;

    ----------------------------------------------------------------------------

    procedure Al_Do_Multiline_Text( font      : A_Allegro_Font;
                                    max_width : Integer;
                                    text      : String;
                                    cb        : access function( line_num : Integer;
                                                                 line     : String;
                                                                 extra    : Address ) return Boolean;
                                    extra     : Address ) is

       type C_Multiline_Text_Callback is access
            function( line_num : Integer;
                      line     : C.Strings.chars_ptr;
                      size     : Integer;
                      extra    : Address ) return Bool;
        pragma Convention( C, C_Multiline_Text_Callback );

        function Examine( line_num : Integer;
                          line     : C.Strings.chars_ptr;
                          size     : Integer;
                          extra    : Address ) return Bool;
        pragma Convention( C, Examine );

        function Examine( line_num : Integer;
                          line     : C.Strings.chars_ptr;
                          size     : Integer;
                          extra    : Address ) return Bool is
        begin
            return (if cb( line_num, Value( line, size_t(size) ), extra ) then B_TRUE else B_FALSE);
        end Examine;

        procedure C_Al_Do_Multiline_Text( font      : A_Allegro_Font;
                                          max_width : Integer;
                                          text      : C.char_array;
                                          cb        : C_Multiline_Text_Callback;
                                          extra     : Address );
        pragma Import( C, C_Al_Do_Multiline_Text, "al_do_multiline_text" );

    begin
        C_Al_Do_Multiline_Text( font, max_width, To_C( text ), Examine'Access, extra );
    end Al_Do_Multiline_Text;

    ----------------------------------------------------------------------------

    procedure Al_Do_Multiline_Ustr( font      : A_Allegro_Font;
                                    max_width : Integer;
                                    text      : A_Allegro_Ustr;
                                    cb        : access function( line_num : Integer;
                                                                 line     : A_Allegro_Ustr;
                                                                 extra    : Address ) return Boolean;
                                    extra     : Address ) is

       type C_Multiline_Ustr_Callback is access
            function( line_num : Integer;
                      line     : A_Allegro_Ustr;
                      extra    : Address ) return Bool;
        pragma Convention( C, C_Multiline_Ustr_Callback );

        function Examine( line_num : Integer;
                          line     : A_Allegro_Ustr;
                          extra    : Address ) return Bool;
        pragma Convention( C, Examine );

        function Examine( line_num : Integer;
                          line     : A_Allegro_Ustr;
                          extra    : Address ) return Bool is
        begin
            return (if cb( line_num, line, extra ) then B_TRUE else B_FALSE);
        end Examine;

        procedure C_Al_Do_Multiline_Ustr( font      : A_Allegro_Font;
                                          max_width : Integer;
                                          text      : A_Allegro_Ustr;
                                          cb        : C_Multiline_Ustr_Callback;
                                          extra     : Address );
        pragma Import( C, C_Al_Do_Multiline_Ustr, "al_do_multiline_ustr" );

    begin
        C_Al_Do_Multiline_Ustr( font, max_width, text, Examine'Access, extra );
    end Al_Do_Multiline_Ustr;

    ----------------------------------------------------------------------------

    function Al_Grab_Font_From_Bitmap( bmp    : A_Allegro_Bitmap;
                                       ranges : Unicode_Range_Array
                                     ) return A_Allegro_Font is

        function C_Al_Grab_Font_From_Bitmap( bmp    : A_Allegro_Bitmap;
                                             n      : Integer;
                                             ranges : Address ) return A_Allegro_Font;
        pragma Import( C, C_Al_Grab_Font_From_Bitmap, "al_grab_font_from_bitmap" );

    begin
        return C_Al_Grab_Font_From_Bitmap( bmp, ranges'Length, ranges(ranges'First)'Address );
    end Al_Grab_Font_From_Bitmap;

    ----------------------------------------------------------------------------

    function Al_Load_Bitmap_Font( filename : String ) return A_Allegro_Font is

        function C_Al_Load_Bitmap_Font( filename : C.char_array ) return A_Allegro_Font;
        pragma Import( C, C_Al_Load_Bitmap_Font, "al_load_bitmap_font" );

    begin
        return C_Al_Load_Bitmap_Font( To_C( filename ) );
    end Al_Load_Bitmap_Font;

    ----------------------------------------------------------------------------

    function Al_Load_Bitmap_Font_Flags( filename : String;
                                        flags    : Font_Loading_Flags ) return A_Allegro_Font is

        function C_Al_Load_Bitmap_Font_Flags( filename : C.char_array;
                                              flags    : Font_Loading_Flags ) return A_Allegro_Font;
        pragma Import( C, C_Al_Load_Bitmap_Font_Flags, "al_load_bitmap_font_flags" );

    begin
        return C_Al_Load_Bitmap_Font_Flags( To_C( filename ), flags );
    end Al_Load_Bitmap_Font_Flags;

end Allegro.Fonts;
