--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Audio;                     use Allegro.Audio;
with Allegro.Events;                    use Allegro.Events;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Streaming video addon
package Allegro.Video is

    -- Initialization

    function Al_Init_Video_Addon return Boolean;

    procedure Al_Shutdown_Video_Addon;
    pragma Import( C, Al_Shutdown_Video_Addon, "al_shutdown_video_addon" );

    function Al_Get_Allegro_Video_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Video_Version, "al_get_allegro_video_version" );

    -- Video types

    type Allegro_Video is limited private;
    type A_Allegro_Video is access all Allegro_Video;

    ALLEGRO_EVENT_VIDEO_FRAME_SHOW : constant Allegro_Event_Type := 550;
    ALLEGRO_EVENT_VIDEO_FINISHED   : constant Allegro_Event_Type := 551;

    type Allegro_Video_Position_Type is (
        ALLEGRO_VIDEO_POSITION_ACTUAL,
        ALLEGRO_VIDEO_POSITION_VIDEO_DECODE,
        ALLEGRO_VIDEO_POSITION_AUDIO_DECODE
    );
    pragma Convention( C, Allegro_Video_Position_Type );

    -- Streaming video routines

    function Al_Open_Video( filename : String ) return A_Allegro_Video;

    procedure Al_Close_Video( video : in out A_Allegro_Video );

    procedure Al_Start_Video( video : A_Allegro_Video; mixer : A_Allegro_Mixer );
    pragma Import( C, Al_Start_Video, "al_start_video" );

    procedure Al_Start_Video_With_Voice( video : A_Allegro_Video; voice : A_Allegro_Voice );
    pragma Import( C, Al_Start_Video_With_Voice, "al_start_video_with_voice" );

    function Al_Get_Video_Event_Source( video : A_Allegro_Video ) return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Video_Event_Source, "al_get_video_event_source" );

    procedure Al_Set_Video_Playing( video : A_Allegro_Video; playing : Boolean );

    function Al_Is_Video_Playing( video : A_Allegro_Video ) return Boolean;

    function Al_Get_Video_Audio_Rate( video : A_Allegro_Video ) return Long_Float;
    pragma Import( C, Al_Get_Video_Audio_Rate, "al_get_video_audio_rate" );

    function Al_Get_Video_FPS( video : A_Allegro_Video ) return Long_Float;
    pragma Import( C, Al_Get_Video_FPS, "al_get_video_fps" );

    function Al_Get_Video_Scaled_Width( video : A_Allegro_Video ) return Float;
    pragma Import( C, Al_Get_Video_Scaled_Width, "al_get_video_scaled_width" );

    function Al_Get_Video_Scaled_Height( video : A_Allegro_Video ) return Float;
    pragma Import( C, Al_Get_Video_Scaled_Height, "al_get_video_scaled_height" );

    function Al_Get_Video_Frame( video : A_Allegro_Video ) return A_Allegro_Bitmap;
    pragma Import( C, Al_Get_Video_Frame, "al_get_video_frame" );

    function Al_Get_Video_Position( video : A_Allegro_Video; which : Allegro_Video_Position_Type ) return Long_Float;
    pragma Import( C, Al_Get_Video_Position, "al_get_video_position" );

    function Al_Seek_Video( video : A_Allegro_Video; pos_in_seconds : Long_Float ) return Boolean;

    procedure Al_Seek_Video( video : A_Allegro_Video; pos_in_seconds : Long_Float );

private

    type Allegro_Video is limited null record;
    pragma Convention( C, Allegro_Video );

end Allegro.Video;
