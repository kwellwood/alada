--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Unchecked_Deallocation;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Bitmaps.Locking;           use Allegro.Bitmaps.Locking;
with Interfaces.C.Pointers;
with System;                            use System;

-- Allegro 5.2.5 - Primitives addon
package Allegro.Drawing.Primitives is

    -- Initialization

    function Al_Init_Primitives_Addon return Boolean;

    procedure Al_Shutdown_Primitives_Addon;
    pragma Import( C, Al_Shutdown_Primitives_Addon, "al_shutdown_primitives_addon" );

    function Al_Get_Allegro_Primitives_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Primitives_Version, "al_get_allegro_primitives_version" );

    -- High level drawing structures

    type Point_2D is
        record
            x, y : Float;
        end record;
    pragma Convention( C, Point_2D );

    type Point_2D_Array is array (Natural range <>) of aliased Point_2D;
    pragma Convention( C, Point_2D_Array );

    type Allegro_Line_Join is new Integer;
    ALLEGRO_LINE_JOIN_NONE  : constant Allegro_Line_Join := 0;
    ALLEGRO_LINE_JOIN_BEVEL : constant Allegro_Line_Join := 1;
    ALLEGRO_LINE_JOIN_ROUND : constant Allegro_Line_Join := 2;
    ALLEGRO_LINE_JOIN_MITER : constant Allegro_Line_Join := 3;
    ALLEGRO_LINE_JOIN_MITRE : constant Allegro_Line_Join := ALLEGRO_LINE_JOIN_MITER;

    type Allegro_Line_Cap is new Integer;
    ALLEGRO_LINE_CAP_NONE     : constant Allegro_Line_Cap := 0;
    ALLEGRO_LINE_CAP_SQUARE   : constant Allegro_Line_Cap := 1;
    ALLEGRO_LINE_CAP_ROUND    : constant Allegro_Line_Cap := 2;
    ALLEGRO_LINE_CAP_TRIANGLE : constant Allegro_Line_Cap := 3;
    ALLEGRO_LINE_CAP_CLOSED   : constant Allegro_Line_Cap := 4;

    type Int_Array is array (Natural range <>) of aliased Integer_32;
    pragma Convention( C, Int_Array );

    package Locked_Int_Arrays is new Interfaces.C.Pointers( Natural, Integer_32, Int_Array, 0 );
    subtype Locked_Int_Ptr is Locked_Int_Arrays.Pointer;

    type Short_Array is array (Natural range <>) of aliased Integer_16;
    pragma Convention( C, Short_Array );

    package Locked_Short_Arrays is new Interfaces.C.Pointers( Natural, Integer_16, Short_Array, 0 );
    subtype Locked_Short_Ptr is Locked_Short_Arrays.Pointer;

    -- High level primitives

    procedure Al_Draw_Line( x1, y1,
                            x2, y2    : Float;
                            color     : Allegro_Color;
                            thickness : Float );
    pragma Import( C, Al_Draw_Line, "al_draw_line" );

    procedure Al_Draw_Triangle( x1, y1,
                                x2, y2,
                                x3, y3    : Float;
                                color     : Allegro_Color;
                                thickness : Float );
    pragma Import( C, Al_Draw_Triangle, "al_draw_triangle" );

    procedure Al_Draw_Rectangle( x1, y1,
                                 x2, y2    : Float;
                                 color     : Allegro_Color;
                                 thickness : Float );
    pragma Import( C, Al_Draw_Rectangle, "al_draw_rectangle" );

    procedure Al_Draw_Rounded_Rectangle( x1, y1,
                                         x2, y2    : Float;
                                         rx, ry    : Float;
                                         color     : Allegro_Color;
                                         thickness : Float );
    pragma Import( C, Al_Draw_Rounded_Rectangle, "al_draw_rounded_rectangle" );

    procedure Al_Calculate_Arc( dest         : Address;
                                stride       : Integer;
                                cx, cy       : Float;
                                rx, ry       : Float;
                                start_theta  : Float;
                                delta_theta  : Float;
                                thickness    : Float;
                                num_segments : Integer );
    pragma Import( C, Al_Calculate_Arc, "al_calculate_arc" );

    procedure Al_Calculate_Arc( dest        : Point_2D_Array;
                                cx, cy      : Float;
                                rx, ry      : Float;
                                start_theta : Float;
                                delta_theta : Float;
                                thickness   : Float );
    pragma Precondition( dest'Length > 0 );
    pragma Precondition( thickness <= 0.0 or else dest'Length mod 2 = 0 );

    procedure Al_Draw_Circle( cx, cy    : Float;
                              r         : Float;
                              color     : Allegro_Color;
                              thickness : Float );
    pragma Import( C, Al_Draw_Circle, "al_draw_circle" );

    procedure Al_Draw_Ellipse( cx, cy    : Float;
                               rx, ry    : Float;
                               color     : Allegro_Color;
                               thickness : Float );
    pragma Import( C, Al_Draw_Ellipse, "al_draw_ellipse" );

    procedure Al_Draw_Arc( cx, cy      : Float;
                           r           : Float;
                           start_theta : Float;
                           delta_theta : Float;
                           color       : Allegro_Color;
                           thickness   : Float );
    pragma Import( C, Al_Draw_Arc, "al_draw_arc" );

    procedure Al_Draw_Elliptical_Arc( cx, cy      : Float;
                                      rx, ry      : Float;
                                      start_theta : Float;
                                      delta_theta : Float;
                                      color       : Allegro_Color;
                                      thickness   : Float );
    pragma Import( C, Al_Draw_Elliptical_Arc, "al_draw_elliptical_arc" );

    procedure Al_Draw_Pieslice( cx, cy      : Float;
                                r           : Float;
                                start_theta : Float;
                                delta_theta : Float;
                                color       : Allegro_Color;
                                thickness   : Float );
    pragma Import( C, Al_Draw_Pieslice, "al_draw_pieslice" );

    procedure Al_Calculate_Spline( dest         : Address;
                                   stride       : Integer;
                                   points       : Address;
                                   thickness    : Float;
                                   num_segments : Integer );
    pragma Import( C, Al_Calculate_Spline, "al_calculate_spline" );

    procedure Al_Calculate_Spline( dest      : Point_2D_Array;
                                   points    : Point_2D_Array;
                                   thickness : Float );
    pragma Precondition( dest'Length > 0 );
    pragma Precondition( thickness <= 0.0 or else dest'Length mod 2 = 0 );
    pragma Precondition( points'Length = 4 );

    procedure Al_Draw_Spline( points    : Address;
                              color     : Allegro_Color;
                              thickness : Float );
    pragma Import( C, Al_Draw_Spline, "al_draw_spline" );

    procedure Al_Draw_Spline( points    : Point_2D_Array;
                              color     : Allegro_Color;
                              thickness : Float );
    pragma Precondition( points'Length = 4 );

    procedure Al_Calculate_Ribbon( dest          : Address;
                                   dest_stride   : Integer;
                                   points        : Address;
                                   points_stride : Integer;
                                   thickness     : Float;
                                   num_segments  : Integer );
    pragma Import( C, Al_Calculate_Ribbon, "al_calculate_ribbon" );

    procedure Al_Calculate_Ribbon( dest      : Point_2D_Array;
                                   points    : Point_2D_Array;
                                   thickness : Float );
    pragma Precondition( dest'Length > 0 );
    pragma Precondition( points'Length > 0 );
    pragma Precondition( (thickness <= 0.0 and then dest'Length = points'Length) or else
                         (thickness >  0.0 and then dest'Length = points'Length * 2) );

    procedure Al_Draw_Ribbon( points       : Address;
                              stride       : Integer;
                              color        : Allegro_Color;
                              thickness    : Float;
                              num_segments : Integer );
    pragma Import( C, Al_Draw_Ribbon, "al_draw_ribbon" );

    procedure Al_Draw_Ribbon( points    : Point_2D_Array;
                              color     : Allegro_Color;
                              thickness : Float );
    pragma Precondition( points'Length > 0 );

    procedure Al_Draw_Filled_Triangle( x1, y1,
                                       x2, y2,
                                       x3, y3 : Float;
                                       color  : Allegro_Color );
    pragma Import( C, Al_Draw_Filled_Triangle, "al_draw_filled_triangle" );

    procedure Al_Draw_Filled_Rectangle( x1, y1,
                                        x2, y2 : Float;
                                        color  : Allegro_Color );
    pragma Import( C, Al_Draw_Filled_Rectangle, "al_draw_filled_rectangle" );

    procedure Al_Draw_Filled_Ellipse( cx, cy : Float;
                                      rx, ry : Float;
                                      color  : Allegro_Color );
    pragma Import( C, Al_Draw_Filled_Ellipse, "al_draw_filled_ellipse" );

    procedure Al_Draw_Filled_Circle( cx, cy : Float;
                                     r      : Float;
                                     color  : Allegro_Color );
    pragma Import( C, Al_Draw_Filled_Circle, "al_draw_filled_circle" );

    procedure Al_Draw_Filled_Pieslice( cx, cy      : Float;
                                       r           : Float;
                                       start_theta : Float;
                                       delta_theta : Float;
                                       color       : Allegro_Color );
    pragma Import( C, Al_Draw_Filled_Pieslice, "al_draw_filled_pieslice" );

    procedure Al_Draw_Filled_Rounded_Rectangle( x1, y1 : Float;
                                                x2, y2 : Float;
                                                rx, ry : Float;
                                                color  : Allegro_Color );
    pragma Import( C, Al_Draw_Filled_Rounded_Rectangle, "al_draw_filled_rounded_rectangle" );

    procedure Al_Draw_Polyline( vertices    : Address;
                                stride      : Integer;
                                count       : Integer;
                                join_style  : Allegro_Line_Join;
                                cap_style   : Allegro_Line_Cap;
                                color       : Allegro_Color;
                                thickness   : Float;
                                miter_limit : Float );

    procedure Al_Draw_Polyline( vertices    : Point_2D_Array;
                                join_style  : Allegro_Line_Join;
                                cap_style   : Allegro_Line_Cap;
                                color       : Allegro_Color;
                                thickness   : Float;
                                miter_limit : Float );

    procedure Al_Draw_Polygon( vertices    : Point_2D_Array;
                               join_style  : Allegro_Line_Join;
                               cap_style   : Allegro_Line_Cap;
                               color       : Allegro_Color;
                               thickness   : Float;
                               miter_limit : Float );

    procedure Al_Draw_Filled_Polygon( vertices : Point_2D_Array;
                                      color    : Allegro_Color );

    procedure Al_Draw_Filled_Polygon_With_Holes( vertices      : Point_2D_Array;
                                                 vertex_counts : Integer_Array;
                                                 color         : Allegro_Color );
    pragma Precondition( vertex_counts'Length > 1 );
    pragma Precondition( vertex_counts(vertex_counts'First) >= 3 );
    pragma Precondition( vertex_counts(vertex_counts'Last) = 0 );

    -- Utilities for high level primitives

    function Al_Triangulate_Polygon( vertices      : Point_2D_Array;
                                     vertex_counts : Integer_Array;
                                     emit_triangle : access procedure( indexPoint1 : Integer;
                                                                       indexPoint2 : Integer;
                                                                       indexPoint3 : Integer;
                                                                       userData    : Address );
                                     userData      : Address ) return Boolean;

    -- Low level drawing structures

    type Allegro_Vertex is
        record
            x, y, z : Float;
            u, v    : Float;
            color   : Allegro_Color;
        end record;
    pragma Convention( C, Allegro_Vertex );

    type Allegro_Vertex_Array is array (Natural range <>) of aliased Allegro_Vertex;
    pragma Convention( C, Allegro_Vertex_Array );

    type A_Allegro_Vertex_Array is access all Allegro_Vertex_Array;

    procedure Delete is new Ada.Unchecked_Deallocation( Allegro_Vertex_Array, A_Allegro_Vertex_Array );

    package Locked_Vertex_Arrays is new Interfaces.C.Pointers( Natural, Allegro_Vertex, Allegro_Vertex_Array, (0.0, 0.0, 0.0, 0.0, 0.0, color => <>) );
    subtype Locked_Vertex_Ptr is Locked_Vertex_Arrays.Pointer;

    type Allegro_Prim_Attr is (
        ALLEGRO_PRIM_NONE,
        ALLEGRO_PRIM_POSITION,
        ALLEGRO_PRIM_COLOR_ATTR,
        ALLEGRO_PRIM_TEX_COORD,
        ALLEGRO_PRIM_TEX_COORD_PIXEL,
        ALLEGRO_PRIM_USER_ATTR
    );
    pragma Convention( C, Allegro_Prim_Attr );

    type Allegro_Prim_Storage is (
        ALLEGRO_PRIM_FLOAT_2,
        ALLEGRO_PRIM_FLOAT_3,
        ALLEGRO_PRIM_SHORT_2,
        ALLEGRO_PRIM_FLOAT_1,
        ALLEGRO_PRIM_FLOAT_4,
        ALLEGRO_PRIM_UBYTE_4,
        ALLEGRO_PRIM_SHORT_4,
        ALLEGRO_PRIM_NORMALIZED_UBYTE_4,
        ALLEGRO_PRIM_NORMALIZED_SHORT_2,
        ALLEGRO_PRIM_NORMALIZED_SHORT_4,
        ALLEGRO_PRIM_NORMALIZED_USHORT_2,
        ALLEGRO_PRIM_NORMALIZED_USHORT_4,
        ALLEGRO_PRIM_HALF_FLOAT_2,
        ALLEGRO_PRIM_HALF_FLOAT_4
    );
    pragma Convention( C, Allegro_Prim_Storage );

    ALLEGRO_PRIM_UNUSED : constant Allegro_Prim_Storage := Allegro_Prim_Storage'First; -- == 0

    type Allegro_Vertex_Element is
        record
            attribute : Allegro_Prim_Attr;
            storage   : Allegro_Prim_Storage;
            offset    : Integer;
        end record;
    pragma Convention( C, Allegro_Vertex_Element );

    LAST_ALLEGRO_VERTEX_ELEMENT : constant Allegro_Vertex_Element := (ALLEGRO_PRIM_NONE, ALLEGRO_PRIM_UNUSED, 0);

    type Allegro_Vertex_Element_Array is array (Natural range <>) of aliased Allegro_Vertex_Element;
    pragma Convention( C, Allegro_Vertex_Element_Array );

    type A_Allegro_Vertex_Element_Array is access all Allegro_Vertex_Element_Array;

    type Allegro_Vertex_Decl is limited private;
    type A_Allegro_Vertex_Decl is access all Allegro_Vertex_Decl;
    pragma Convention( C, A_Allegro_Vertex_Decl );

    type Allegro_Prim_Type is (
        ALLEGRO_PRIM_LINE_LIST,
        ALLEGRO_PRIM_LINE_STRIP,
        ALLEGRO_PRIM_LINE_LOOP,
        ALLEGRO_PRIM_TRIANGLE_LIST,
        ALLEGRO_PRIM_TRIANGLE_STRIP,
        ALLEGRO_PRIM_TRIANGLE_FAN,
        ALLEGRO_PRIM_POINT_LIST,
        ALLEGRO_PRIM_NUM_TYPES
    );
    pragma Convention( C, Allegro_Prim_Type );

    type Vertex_Index_Array is new Integer_Array;

    type A_Vertex_Index_Array is access all Vertex_Index_Array;

    procedure Delete is new Ada.Unchecked_Deallocation( Vertex_Index_Array, A_Vertex_Index_Array );

    -- Low level drawing routines

    function Al_Create_Vertex_Decl( elements : Address;
                                    stride   : Integer ) return A_Allegro_Vertex_Decl;
    pragma Import( C, Al_Create_Vertex_Decl, "al_create_vertex_decl" );

    function Al_Create_Vertex_Decl( elements : Allegro_Vertex_Element_Array;
                                    stride   : Positive ) return A_Allegro_Vertex_Decl;
    pragma Precondition( elements'Length > 0 );

    procedure Al_Destroy_Vertex_Decl( decl : in out A_Allegro_Vertex_Decl );

    procedure Al_Draw_Prim( vertices : Address;
                            decl     : A_Allegro_Vertex_Decl;
                            texture  : A_Allegro_Bitmap;
                            start    : Integer;
                            stop     : Integer;
                            primType : Allegro_Prim_Type );
    pragma Import( C, Al_Draw_Prim, "al_draw_prim" );

    -- Same as Al_Draw_Prim above, using the default Allegro_Vertex structure.
    procedure Al_Draw_Prim( vertices : Allegro_Vertex_Array;
                            texture  : A_Allegro_Bitmap;
                            start    : Integer;
                            stop     : Integer;
                            primType : Allegro_Prim_Type );

    -- Same as Al_Draw_Prim above, drawing all the vertices in the array.
    procedure Al_Draw_Prim( vertices : Allegro_Vertex_Array;
                            texture  : A_Allegro_Bitmap;
                            primType : Allegro_Prim_Type );

    function Al_Draw_Indexed_Prim( vertices : Address;
                                   decl     : A_Allegro_Vertex_Decl;
                                   texture  : A_Allegro_Bitmap;
                                   indices  : Vertex_Index_Array;
                                   primType : Allegro_Prim_Type ) return Integer;

    -- Same as Al_Draw_Indexed_Prim above, but errors are ignored.
    procedure Al_Draw_Indexed_Prim( vertices : Address;
                                    decl     : A_Allegro_Vertex_Decl;
                                    texture  : A_Allegro_Bitmap;
                                    indices  : Vertex_Index_Array;
                                    primType : Allegro_Prim_Type );

    -- Same as Al_Draw_Index_Prim above, using the default Allegro_Vertex structure.
    procedure Al_Draw_Indexed_Prim( vertices : Allegro_Vertex_Array;
                                    texture  : A_Allegro_Bitmap;
                                    indices  : Vertex_Index_Array;
                                    primType : Allegro_Prim_Type );

    -- Vertex buffers

    subtype Allegro_Prim_Buffer_Flags is Unsigned_32;
    ALLEGRO_PRIM_BUFFER_STREAM    : constant Allegro_Prim_Buffer_Flags := 16#1#;
    ALLEGRO_PRIM_BUFFER_STATIC    : constant Allegro_Prim_Buffer_Flags := 16#2#;
    ALLEGRO_PRIM_BUFFER_DYNAMIC   : constant Allegro_Prim_Buffer_Flags := 16#4#;
    ALLEGRO_PRIM_BUFFER_READWRITE : constant Allegro_Prim_Buffer_Flags := 16#8#;

    type Allegro_Vertex_Buffer is limited private;
    type A_Allegro_Vertex_Buffer is access all Allegro_Vertex_Buffer;

    function Al_Create_Vertex_Buffer( decl         : A_Allegro_Vertex_Decl;
                                      initial_data : Address;
                                      num_vertices : Integer;
                                      flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Vertex_Buffer;
    pragma Import( C, Al_Create_Vertex_Buffer, "al_create_vertex_buffer" );

    function Al_Create_Vertex_Buffer( initial_data : Allegro_Vertex_Array;
                                      flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Vertex_Buffer;

    function Al_Create_Vertex_Buffer( length : Integer;
                                      flags  : Allegro_Prim_Buffer_Flags ) return A_Allegro_Vertex_Buffer;

    procedure Al_Destroy_Vertex_Buffer( buffer : in out A_Allegro_Vertex_Buffer );

    function Al_Lock_Vertex_Buffer( buffer : A_Allegro_Vertex_Buffer;
                                    offset : Integer;
                                    length : Integer;
                                    flags  : Allegro_Lock_Mode ) return Address;

    function Al_Lock_Vertex_Buffer( buffer : A_Allegro_Vertex_Buffer;
                                    offset : Integer;
                                    length : Integer;
                                    flags  : Allegro_Lock_Mode ) return Locked_Vertex_Ptr;

    procedure Al_Unlock_Vertex_Buffer( buffer : A_Allegro_Vertex_Buffer );
    pragma Import( C, Al_Unlock_Vertex_Buffer, "al_unlock_vertex_buffer" );

    function Al_Get_Vertex_Buffer_Size( buffer : A_Allegro_Vertex_Buffer ) return Integer;
    pragma Import( C, Al_Get_Vertex_Buffer_Size, "al_get_vertex_buffer_size" );

    function Al_Draw_Vertex_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                    texture       : A_Allegro_Bitmap;
                                    start         : Integer;
                                    stop          : Integer;
                                    typ           : Allegro_Prim_Type ) return Integer;
    pragma Import( C, Al_Draw_Vertex_Buffer, "al_draw_vertex_buffer" );

    procedure Al_Draw_Vertex_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                     texture       : A_Allegro_Bitmap;
                                     start         : Integer;
                                     stop          : Integer;
                                     typ           : Allegro_Prim_Type );

    function Al_Draw_Vertex_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                    start         : Integer;
                                    stop          : Integer;
                                    typ           : Allegro_Prim_Type ) return Integer;

    procedure Al_Draw_Vertex_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                     start         : Integer;
                                     stop          : Integer;
                                     typ           : Allegro_Prim_Type );

    -- Index buffers

    type Allegro_Index_Buffer is limited private;
    type A_Allegro_Index_Buffer is access all Allegro_Index_Buffer;

    function Al_Create_Index_Buffer( initial_data : access Int_Array;
                                     num_indices  : Integer;
                                     flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer;

    function Al_Create_Index_Buffer( initial_data : access Short_Array;
                                     num_indices  : Integer;
                                     flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer;

    function Al_Create_Index_Buffer( index_size   : Integer;
                                     num_indices  : Integer;
                                     flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer;
    pragma Precondition( index_size = 2 or index_size = 4 );

    procedure Al_Destroy_Index_Buffer( buffer : in out A_Allegro_Index_Buffer );

    function Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                   offset : Integer;
                                   length : Integer;
                                   flags  : Allegro_Lock_Mode ) return Address;

    function Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                   offset : Integer;
                                   length : Integer;
                                   flags  : Allegro_Lock_Mode ) return Locked_Int_Ptr;

    function Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                   offset : Integer;
                                   length : Integer;
                                   flags  : Allegro_Lock_Mode ) return Locked_Short_Ptr;

    procedure Al_Unlock_Index_Buffer( buffer : A_Allegro_Index_Buffer );
    pragma Import( C, Al_Unlock_Index_Buffer, "al_unlock_index_buffer" );

    function Al_Get_Index_Buffer_Size( buffer : A_Allegro_Index_Buffer ) return Integer;
    pragma Import( C, Al_Get_Index_Buffer_Size, "al_get_index_buffer_size" );

    function Al_Draw_Indexed_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                     texture       : A_Allegro_Bitmap;
                                     index_buffer  : A_Allegro_Index_Buffer;
                                     start         : Integer;
                                     stop          : Integer;
                                     typ           : Allegro_Prim_Type ) return Integer;
    pragma Import( C, Al_Draw_Indexed_Buffer, "al_draw_indexed_buffer" );

    procedure Al_Draw_Indexed_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                      texture       : A_Allegro_Bitmap;
                                      index_buffer  : A_Allegro_Index_Buffer;
                                      start         : Integer;
                                      stop          : Integer;
                                      typ           : Allegro_Prim_Type );

    -- Custom primitives

    procedure Al_Draw_Soft_Line( v1, v2 : access Allegro_Vertex;
                                 state  : Address;
                                 first  : access procedure( state : Address; x, y : Integer; v1, v2 : access Allegro_Vertex );
                                 step   : access procedure( state : Address; isMinor : Boolean );
                                 draw   : access procedure( state : Address; x, y : Integer ) );

    procedure Al_Draw_Soft_Triangle( v1, v2, v3 : access Allegro_Vertex;
                                     state      : Address;
                                     init       : access procedure( state : Address; v1, v2, v3 : access Allegro_Vertex );
                                     first      : access procedure( state : Address; x, y, minor, major : Integer );
                                     step       : access procedure( state : Address; isMinor : Boolean );
                                     draw       : access procedure( state : Address; x1, y, x2 : Integer ) );

private

    type Allegro_Vertex_Decl is limited null record;
    pragma Convention( C, Allegro_Vertex_Decl );

    type Allegro_Vertex_Buffer is limited null record;
    pragma Convention( C, Allegro_Vertex_Buffer );

    type Allegro_Index_Buffer is limited null record;
    pragma Convention( C, Allegro_Index_Buffer );

end Allegro.Drawing.Primitives;
