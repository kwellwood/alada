--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Touch_Input is

    function Al_Is_Touch_Input_Installed return Boolean is
        function C_Al_Is_Touch_Input_Installed return Bool;
        pragma Import( C, C_Al_Is_Touch_Input_Installed, "al_is_touch_input_installed" );
    begin
        return C_Al_Is_Touch_Input_Installed = B_TRUE;
    end Al_Is_Touch_Input_Installed;

     ---------------------------------------------------------------------------

    function Al_Install_Touch_Input return Boolean is
        function C_Al_Install_Touch_Input return Bool;
        pragma Import( C, C_Al_Install_Touch_Input, "al_install_touch_input" );
    begin
        return C_Al_Install_Touch_Input = B_TRUE;
    end Al_Install_Touch_Input;

end Allegro.Touch_Input;
