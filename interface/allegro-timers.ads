--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Timer routines
package Allegro.Timers is

    type Allegro_Timer is limited private;
    type A_Allegro_Timer is access all Allegro_Timer;

    function Allegro_USecs_To_Secs( x : Long_Float ) return Long_Float with Inline_Always;

    function Allegro_MSecs_To_Secs( x : Long_Float ) return Long_Float with Inline_Always;

    function Allegro_BPS_To_Secs( x : Long_Float ) return Long_Float with Inline_Always;

    function Allegro_BPM_To_Secs( x : Long_Float ) return Long_Float with Inline_Always;

    function Al_Create_Timer( speed_secs : Long_Float ) return A_Allegro_Timer;
    pragma Import( C, Al_Create_Timer, "al_create_timer" );

    procedure Al_Destroy_Timer( timer : in out A_Allegro_Timer );

    procedure Al_Start_Timer( timer : A_Allegro_Timer );
    pragma Import( C, Al_Start_Timer, "al_start_timer" );

    procedure Al_Stop_Timer( timer : A_Allegro_Timer );
    pragma Import( C, Al_Stop_Timer, "al_stop_timer" );

    procedure Al_Resume_Timer( timer : A_Allegro_Timer );
    pragma Import( C, Al_Resume_Timer, "al_resume_timer" );

    function Al_Get_Timer_Started( timer : A_Allegro_Timer ) return Boolean;

    function Al_Get_Timer_Count( timer : A_Allegro_Timer ) return Integer_64;
    pragma Import( C, Al_Get_Timer_Count, "al_get_timer_count" );

    procedure Al_Set_Timer_Count( timer : A_Allegro_Timer; count : Integer_64 );
    pragma Import( C, Al_Set_Timer_Count, "al_set_timer_count" );

    procedure Al_Add_Timer_Count( timer : A_Allegro_Timer; diff : Integer_64 );
    pragma Import( C, Al_Add_Timer_Count, "al_add_timer_count" );

    function Al_Get_Timer_Speed( timer : A_Allegro_Timer ) return Long_Float;
    pragma Import( C, Al_Get_Timer_Speed, "al_get_timer_speed" );

    procedure Al_Set_Timer_Speed( timer      : A_Allegro_Timer;
                                  speed_secs : Long_Float );
    pragma Import( C, Al_Set_Timer_Speed, "al_set_timer_speed" );

    function Al_Get_Timer_Event_Source( timer : A_Allegro_Timer ) return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Timer_Event_Source, "al_get_timer_event_source" );

private

    type Allegro_Timer is limited null record;
    pragma Convention( C, Allegro_Timer );

end Allegro.Timers;
