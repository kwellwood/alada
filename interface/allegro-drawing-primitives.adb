--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;

package body Allegro.Drawing.Primitives is

    function Al_Init_Primitives_Addon return Boolean is

        function C_Al_Init_Primitives_Addon return Bool;
        pragma Import( C, C_Al_Init_Primitives_Addon, "al_init_primitives_addon" );

    begin
        return To_Ada( C_Al_Init_Primitives_Addon );
    end Al_Init_Primitives_Addon;

    ----------------------------------------------------------------------------

    procedure Al_Calculate_Arc( dest         : Point_2D_Array;
                                cx, cy       : Float;
                                rx, ry       : Float;
                                start_theta  : Float;
                                delta_theta  : Float;
                                thickness    : Float ) is
        segments : Integer;
    begin
        if thickness <= 0.0 then
            segments := dest'Length;
        else
            -- thickness > 0 requires twice as many points per segment
            segments := (dest'Length - 1) / 2;
        end if;
        Al_Calculate_Arc( dest(dest'First)'Address, Point_2D'Size / 8,
                          cx, cy, rx, ry, start_theta, delta_theta,
                          thickness, segments );
    end Al_Calculate_Arc;

    ----------------------------------------------------------------------------

    procedure Al_Calculate_Spline( dest      : Point_2D_Array;
                                   points    : Point_2D_Array;
                                   thickness : Float ) is
        segments : Integer;
    begin
        if thickness <= 0.0 then
            segments := dest'Length;
        else
            -- thickness > 0 requires twice as many points per segment
            segments := (dest'Length - 1) / 2;
        end if;
        Al_Calculate_Spline( dest(dest'First)'Address, Point_2D'Size / 8,
                             points(points'First)'Address, thickness, segments );
    end Al_Calculate_Spline;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Spline( points    : Point_2D_Array;
                              color     : Allegro_Color;
                              thickness : Float ) is
    begin
        Al_Draw_Spline( points(points'First)'Address, color, thickness );
    end Al_Draw_Spline;

    ----------------------------------------------------------------------------

    procedure Al_Calculate_Ribbon( dest      : Point_2D_Array;
                                   points    : Point_2D_Array;
                                   thickness : Float ) is
    begin
        Al_Calculate_Ribbon( dest(dest'First)'Address, Point_2D'Size / 8,
                             points(points'First)'Address, Point_2D'Size / 8,
                             thickness, points'Length );
    end Al_Calculate_Ribbon;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Ribbon( points    : Point_2D_Array;
                              color     : Allegro_Color;
                              thickness : Float ) is
    begin
        Al_Draw_Ribbon( points(points'First)'Address, Point_2D'Size / 8,
                        color, thickness, points'Length );
    end Al_Draw_Ribbon;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Polyline( vertices    : Address;
                                stride      : Integer;
                                count       : Integer;
                                join_style  : Allegro_Line_Join;
                                cap_style   : Allegro_Line_Cap;
                                color       : Allegro_Color;
                                thickness   : Float;
                                miter_limit : Float ) is
        procedure C_Al_Draw_Polyline( vertices      : Address;
                                      vertex_stride : Integer;
                                      vertex_count  : Integer;
                                      join_style    : Allegro_Line_Join;
                                      cap_style     : Allegro_Line_Cap;
                                      color         : Allegro_Color;
                                      thickness     : Float;
                                      miter_limit   : Float );
        pragma Import( C, C_Al_Draw_Polyline, "al_draw_polyline" );
    begin
        C_Al_Draw_Polyline( vertices,
                            stride,
                            count,
                            join_style,
                            cap_style,
                            color,
                            thickness,
                            miter_limit );
    end Al_Draw_Polyline;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Polyline( vertices    : Point_2D_Array;
                                join_style  : Allegro_Line_Join;
                                cap_style   : Allegro_Line_Cap;
                                color       : Allegro_Color;
                                thickness   : Float;
                                miter_limit : Float ) is
    begin
        Al_Draw_Polyline( vertices(vertices'First)'Address,
                          Point_2D'Size / 8,
                          vertices'Length,
                          join_style,
                          cap_style,
                          color,
                          thickness,
                          miter_limit );
    end Al_Draw_Polyline;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Polygon( vertices    : Point_2D_Array;
                               join_style  : Allegro_Line_Join;
                               cap_style   : Allegro_Line_Cap;
                               color       : Allegro_Color;
                               thickness   : Float;
                               miter_limit : Float ) is
        procedure C_Al_Draw_Polygon( vertices      : Address;
                                     vertex_count  : Integer;
                                     join_style    : Allegro_Line_Join;
                                     cap_style     : Allegro_Line_Cap;
                                     color         : Allegro_Color;
                                     thickness     : Float;
                                     miter_limit   : Float );
        pragma Import( C, C_Al_Draw_Polygon, "al_draw_polygon" );
    begin
        C_Al_Draw_Polygon( vertices(vertices'First)'Address,
                           vertices'Length,
                           join_style,
                           cap_style,
                           color,
                           thickness,
                           miter_limit );
    end Al_Draw_Polygon;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Filled_Polygon( vertices : Point_2D_Array;
                                      color    : Allegro_Color ) is
        procedure C_Al_Draw_Filled_Polygon( vertices     : Address;
                                            vertex_count : Integer;
                                            color        : Allegro_Color );
        pragma Import( C, C_Al_Draw_Filled_Polygon, "al_draw_filled_polygon" );
    begin
        C_Al_Draw_Filled_Polygon( vertices(vertices'First)'Address, vertices'Length, color );
    end Al_Draw_Filled_Polygon;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Filled_Polygon_With_Holes( vertices      : Point_2D_Array;
                                                 vertex_counts : Integer_Array;
                                                 color         : Allegro_Color ) is
        procedure C_Al_Draw_Filled_Polygon_With_Holes( vertices      : Address;
                                                       vertex_counts : Address;
                                                       color         : Allegro_Color );
        pragma Import( C, C_Al_Draw_Filled_Polygon_With_Holes, "al_draw_filled_polygon_with_holes" );
    begin
        -- note: the supplied vertex_counts must end with a zero!
        C_Al_Draw_Filled_Polygon_With_Holes( vertices(vertices'First)'Address,
                                             vertex_counts(vertex_counts'First)'Address,
                                             color );
    end Al_Draw_Filled_Polygon_With_Holes;

    ----------------------------------------------------------------------------

    function Al_Triangulate_Polygon( vertices      : Point_2D_Array;
                                     vertex_counts : Integer_Array;
                                     emit_triangle : access procedure( indexPoint1 : Integer;
                                                                       indexPoint2 : Integer;
                                                                       indexPoint3 : Integer;
                                                                       userData    : Address );
                                     userData      : Address ) return Boolean is

       type A_C_Emit_Triangle is access
            procedure( pointIndex1,
                       pointIndex2,
                       pointIndex3 : Integer;
                       userData    : Address );
        pragma Convention( C, A_C_Emit_Triangle );

        procedure C_Emit_Triangle( pointIndex1,
                                   pointIndex2,
                                   pointIndex3 : Integer;
                                   userData    : Address );
        pragma Convention( C, C_Emit_Triangle );

        procedure C_Emit_Triangle( pointIndex1,
                                   pointIndex2,
                                   pointIndex3 : Integer;
                                   userData    : Address ) is
        begin
            emit_triangle( pointIndex1, pointIndex2, pointIndex3, userData );
        end C_Emit_Triangle;

        function C_Al_Triangulate_Polygon( vertices      : Address;
                                           vertex_stride : size_t;
                                           vertex_counts : Address;
                                           emit_triangle : A_C_Emit_Triangle;
                                           userdata      : Address ) return Bool;
        pragma Import( C, C_Al_Triangulate_Polygon, "al_triangulate_polygon" );

    begin
        return C_Al_Triangulate_Polygon( vertices(vertices'First)'Address,
                                         Point_2D'Size / 8,
                                         vertex_counts(vertex_counts'First)'Address,
                                         C_Emit_Triangle'Access,
                                         userData ) = B_TRUE;
    end Al_Triangulate_Polygon;

    ----------------------------------------------------------------------------

    function Al_Create_Vertex_Decl( elements : Allegro_Vertex_Element_Array;
                                    stride   : Positive ) return A_Allegro_Vertex_Decl is
        elems : aliased Allegro_Vertex_Element_Array(elements'First..elements'Last + 1);
    begin
        elems(elems'First..elems'Last - 1) := elements;

        -- zero out the last element to indicate the end of the array
        elems(elems'Last).attribute := Allegro_Prim_Attr'First;
        elems(elems'Last).storage := Allegro_Prim_Storage'First;
        elems(elems'Last).offset := 0;

        return Al_Create_Vertex_Decl( elems(elems'First)'Address, stride );
    end Al_Create_Vertex_Decl;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Vertex_Decl( decl : in out A_Allegro_Vertex_Decl ) is

        procedure C_Al_Destroy_Vertex_Decl( decl : A_Allegro_Vertex_Decl );
        pragma Import( C, C_Al_Destroy_Vertex_Decl, "al_destroy_vertex_decl" );

    begin
        if decl /= null then
            C_Al_Destroy_Vertex_Decl( decl );
            decl := null;
        end if;
    end Al_Destroy_Vertex_Decl;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Prim( vertices : Allegro_Vertex_Array;
                            texture  : A_Allegro_Bitmap;
                            start    : Integer;
                            stop     : Integer;
                            primType : Allegro_Prim_Type ) is
    begin
        Al_Draw_Prim( vertices(vertices'First)'Address, null, texture, start,
                      stop, primType );
    end Al_Draw_Prim;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Prim( vertices : Allegro_Vertex_Array;
                            texture  : A_Allegro_Bitmap;
                            primType : Allegro_Prim_Type ) is
    begin
        Al_Draw_Prim( vertices(vertices'First)'Address, null, texture, 0,
                      vertices'Length, primType );
    end Al_Draw_Prim;

    ----------------------------------------------------------------------------

    function Al_Draw_Indexed_Prim( vertices : Address;
                                   decl     : A_Allegro_Vertex_Decl;
                                   texture  : A_Allegro_Bitmap;
                                   indices  : Vertex_Index_Array;
                                   primType : Allegro_Prim_Type ) return Integer is

        function C_Al_Draw_Indexed_Prim( vertices : Address;
                                         decl     : A_Allegro_Vertex_Decl;
                                         texture  : A_Allegro_Bitmap;
                                         indices  : Address;
                                         num_vtx  : Integer;
                                         primType : Allegro_Prim_Type ) return Integer;
        pragma Import( C, C_Al_Draw_Indexed_Prim, "al_draw_indexed_prim" );

    begin
        return C_Al_Draw_Indexed_Prim( vertices, decl, texture,
                                       indices(indices'First)'Address,
                                       indices'Length, primType );
    end Al_Draw_Indexed_Prim;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Indexed_Prim( vertices : Address;
                                    decl     : A_Allegro_Vertex_Decl;
                                    texture  : A_Allegro_Bitmap;
                                    indices  : Vertex_Index_Array;
                                    primType : Allegro_Prim_Type ) is
    begin
        if Al_Draw_Indexed_Prim( vertices, decl, texture, indices, primType ) = 0 then
            null;
        end if;
    end Al_Draw_Indexed_Prim;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Indexed_Prim( vertices : Allegro_Vertex_Array;
                                    texture  : A_Allegro_Bitmap;
                                    indices  : Vertex_Index_Array;
                                    primType : Allegro_Prim_Type ) is
    begin
        Al_Draw_Indexed_Prim( vertices(vertices'First)'Address, null, texture,
                              indices, primType );
    end Al_Draw_Indexed_Prim;

    ----------------------------------------------------------------------------

    function Al_Create_Vertex_Buffer( initial_data : Allegro_Vertex_Array;
                                      flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Vertex_Buffer is
    begin
        return Al_Create_Vertex_Buffer( null,
                                        initial_data(initial_data'First)'Address,
                                        initial_data'Length,
                                        flags );
    end Al_Create_Vertex_Buffer;

    ----------------------------------------------------------------------------

    function Al_Create_Vertex_Buffer( length : Integer;
                                      flags  : Allegro_Prim_Buffer_Flags ) return A_Allegro_Vertex_Buffer is
    begin
        return Al_Create_Vertex_Buffer( null, Null_Address, length, flags );
    end Al_Create_Vertex_Buffer;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Vertex_Buffer( buffer : in out A_Allegro_Vertex_Buffer ) is
        procedure C_Al_Destroy_Vertex_Buffer( buffer : A_Allegro_Vertex_Buffer );
        pragma Import( C, C_Al_Destroy_Vertex_Buffer, "al_destroy_vertex_buffer" );
    begin
        if buffer /= null then
            C_Al_Destroy_Vertex_Buffer( buffer );
            buffer := null;
        end if;
    end Al_Destroy_Vertex_Buffer;

    ----------------------------------------------------------------------------

    function Al_Lock_Vertex_Buffer( buffer : A_Allegro_Vertex_Buffer;
                                    offset : Integer;
                                    length : Integer;
                                    flags  : Allegro_Lock_Mode ) return Address is
        function C_Al_Lock_Vertex_Buffer( buffer : A_Allegro_Vertex_Buffer;
                                          offset : Integer;
                                          length : Integer;
                                          flags  : Allegro_Lock_Mode ) return Address;
        pragma Import( C, C_Al_Lock_Vertex_Buffer, "al_lock_vertex_buffer" );
    begin
        return C_Al_Lock_Vertex_Buffer( buffer, offset, length, flags );
    end Al_Lock_Vertex_Buffer;

    ----------------------------------------------------------------------------

    function Al_Lock_Vertex_Buffer( buffer : A_Allegro_Vertex_Buffer;
                                    offset : Integer;
                                    length : Integer;
                                    flags  : Allegro_Lock_Mode ) return Locked_Vertex_Ptr is
        function C_Al_Lock_Vertex_Buffer( buffer : A_Allegro_Vertex_Buffer;
                                          offset : Integer;
                                          length : Integer;
                                          flags  : Allegro_Lock_Mode ) return Locked_Vertex_Ptr;
        pragma Import( C, C_Al_Lock_Vertex_Buffer, "al_lock_vertex_buffer" );
    begin
        return C_Al_Lock_Vertex_Buffer( buffer, offset, length, flags );
    end Al_Lock_Vertex_Buffer;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Vertex_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                     texture       : A_Allegro_Bitmap;
                                     start         : Integer;
                                     stop          : Integer;
                                     typ           : Allegro_Prim_Type ) is
        ignored : Integer;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Draw_Vertex_Buffer( vertex_buffer, texture, start, stop, typ );
    end Al_Draw_Vertex_Buffer;

    ----------------------------------------------------------------------------

    function Al_Draw_Vertex_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                    start         : Integer;
                                    stop          : Integer;
                                    typ           : Allegro_Prim_Type ) return Integer is
    begin
        return Al_Draw_Vertex_Buffer( vertex_buffer, null, start, stop, typ );
    end Al_Draw_Vertex_Buffer;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Vertex_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                     start         : Integer;
                                     stop          : Integer;
                                     typ           : Allegro_Prim_Type ) is
        ignored : Integer;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Draw_Vertex_Buffer( vertex_buffer, start, stop, typ );
    end Al_Draw_Vertex_Buffer;

    ----------------------------------------------------------------------------

    function Al_Create_Index_Buffer( initial_data : access Int_Array;
                                     num_indices  : Integer;
                                     flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer is
        function C_Al_Create_Index_Buffer( index_size   : Integer;
                                           initial_data : Address;
                                           num_indices  : Integer;
                                           flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer;
        pragma Import( C, C_Al_Create_Index_Buffer, "al_create_index_buffer" );
    begin
        if initial_data /= null then
            return C_Al_Create_Index_Buffer( 4,
                                             initial_data(initial_data'First)'Address,
                                             num_indices,
                                             flags );
        end if;
        return C_Al_Create_Index_Buffer( 4,
                                         Null_Address,
                                         num_indices,
                                         flags );
    end Al_Create_Index_Buffer;

    ----------------------------------------------------------------------------

    function Al_Create_Index_Buffer( initial_data : access Short_Array;
                                     num_indices  : Integer;
                                     flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer is
        function C_Al_Create_Index_Buffer( index_size   : Integer;
                                           initial_data : Address;
                                           num_indices  : Integer;
                                           flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer;
        pragma Import( C, C_Al_Create_Index_Buffer, "al_create_index_buffer" );
    begin
        if initial_data /= null then
            return C_Al_Create_Index_Buffer( 2,
                                             initial_data(initial_data'First)'Address,
                                             num_indices,
                                             flags );
        end if;
        return C_Al_Create_Index_Buffer( 2,
                                         Null_Address,
                                         num_indices,
                                         flags );
    end Al_Create_Index_Buffer;

    ----------------------------------------------------------------------------

    function Al_Create_Index_Buffer( index_size   : Integer;
                                     num_indices  : Integer;
                                     flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer is
        function C_Al_Create_Index_Buffer( index_size   : Integer;
                                           initial_data : Address;
                                           num_indices  : Integer;
                                           flags        : Allegro_Prim_Buffer_Flags ) return A_Allegro_Index_Buffer;
        pragma Import( C, C_Al_Create_Index_Buffer, "al_create_index_buffer" );
    begin
        return C_Al_Create_Index_Buffer( index_size, Null_Address, num_indices, flags );
    end Al_Create_Index_Buffer;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Index_Buffer( buffer : in out A_Allegro_Index_Buffer ) is
        procedure C_Al_Destroy_Index_Buffer( buffer : A_Allegro_Index_Buffer );
        pragma Import( C, C_Al_Destroy_Index_Buffer, "al_destroy_index_buffer" );
    begin
        if buffer /= null then
            C_Al_Destroy_Index_Buffer( buffer );
            buffer := null;
        end if;
    end Al_Destroy_Index_Buffer;

    ----------------------------------------------------------------------------

    function Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                   offset : Integer;
                                   length : Integer;
                                   flags  : Allegro_Lock_Mode ) return Address is
        function C_Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                         offset : Integer;
                                         length : Integer;
                                         flags  : Allegro_Lock_Mode ) return Address;
        pragma Import( C, C_Al_Lock_Index_Buffer, "al_lock_index_buffer" );
    begin
        return C_Al_Lock_Index_Buffer( buffer, offset, length, flags );
    end Al_Lock_Index_Buffer;

    ----------------------------------------------------------------------------

    function Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                   offset : Integer;
                                   length : Integer;
                                   flags  : Allegro_Lock_Mode ) return Locked_Int_Ptr is
        function C_Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                         offset : Integer;
                                         length : Integer;
                                         flags  : Allegro_Lock_Mode ) return Locked_Int_Ptr;
        pragma Import( C, C_Al_Lock_Index_Buffer, "al_lock_index_buffer" );
    begin
        return C_Al_Lock_Index_Buffer( buffer, offset, length, flags );
    end Al_Lock_Index_Buffer;

    ----------------------------------------------------------------------------

    function Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                   offset : Integer;
                                   length : Integer;
                                   flags  : Allegro_Lock_Mode ) return Locked_Short_Ptr is
        function C_Al_Lock_Index_Buffer( buffer : A_Allegro_Index_Buffer;
                                         offset : Integer;
                                         length : Integer;
                                         flags  : Allegro_Lock_Mode ) return Locked_Short_Ptr;
        pragma Import( C, C_Al_Lock_Index_Buffer, "al_lock_index_buffer" );
    begin
        return C_Al_Lock_Index_Buffer( buffer, offset, length, flags );
    end Al_Lock_Index_Buffer;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Indexed_Buffer( vertex_buffer : A_Allegro_Vertex_Buffer;
                                      texture       : A_Allegro_Bitmap;
                                      index_buffer  : A_Allegro_Index_Buffer;
                                      start         : Integer;
                                      stop          : Integer;
                                      typ           : Allegro_Prim_Type ) is
        ignored : Integer;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Draw_Indexed_Buffer( vertex_buffer, texture, index_buffer, start, stop, typ );
    end Al_Draw_Indexed_Buffer;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Soft_Line( v1, v2 : access Allegro_Vertex;
                                 state  : Address;
                                 first  : access procedure( state : Address; x, y : Integer; v1, v2 : access Allegro_Vertex );
                                 step   : access procedure( state : Address; isMinor : Boolean );
                                 draw   : access procedure( state : Address; x, y : Integer ) ) is

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        type A_C_First is access procedure( state : Address; x, y : Integer; v1, v2 : access Allegro_Vertex );
        pragma Convention( C, A_C_First );

        type A_C_Step is access procedure( state : Address; isMinor : Integer );
        pragma Convention( C, A_C_Step );

        type A_C_Draw is access procedure( state : Address; x, y : Integer );
        pragma Convention( C, A_C_Draw );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        procedure C_First( state : Address; x, y : Integer; v1, v2 : access Allegro_Vertex );
        pragma Convention( C, C_First );

        procedure C_Step( state : Address; isMinor : Integer );
        pragma Convention( C, C_Step );

        procedure C_Draw( state : Address; x, y : Integer );
        pragma Convention( C, C_Draw );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        procedure C_First( state : Address; x, y : Integer; v1, v2 : access Allegro_Vertex ) is
        begin
            first( state, x, y, v1, v2 );
        end C_First;

        procedure C_Step( state : Address; isMinor : Integer ) is
        begin
            step( state, isMinor /= 0 );
        end C_Step;

        procedure C_Draw( state : Address; x, y : Integer ) is
        begin
            draw( state, x, y );
        end C_Draw;

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        procedure C_Al_Draw_Soft_Line( v1, v2 : access Allegro_Vertex;
                                       state  : Address;
                                       first  : A_C_First;
                                       step   : A_C_Step;
                                       draw   : A_C_Draw );
        pragma Import( C, C_Al_Draw_Soft_Line, "al_draw_soft_line" );

    begin
        C_Al_Draw_Soft_Line( v1, v2, state, C_First'Access, C_Step'Access, C_Draw'Access );
    end Al_Draw_Soft_Line;

    ----------------------------------------------------------------------------

    procedure Al_Draw_Soft_Triangle( v1, v2, v3 : access Allegro_Vertex;
                                     state      : Address;
                                     init       : access procedure( state : Address; v1, v2, v3 : access Allegro_Vertex );
                                     first      : access procedure( state : Address; x, y, minor, major : Integer );
                                     step       : access procedure( state : Address; isMinor : Boolean );
                                     draw       : access procedure( state : Address; x1, y, x2 : Integer ) ) is

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        type A_C_Init is access procedure( state : Address; v1, v2, v3 : access Allegro_Vertex );
        pragma Convention( C, A_C_Init );

        type A_C_First is access procedure( state : Address; x, y, minor, major : Integer );
        pragma Convention( C, A_C_First );

        type A_C_Step is access procedure( state : Address; isMinor : Integer );
        pragma Convention( C, A_C_Step );

        type A_C_Draw is access procedure( state : Address; x1, y, x2 : Integer );
        pragma Convention( C, A_C_Draw );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        procedure C_Init( state : Address; v1, v2, v3 : access Allegro_Vertex );
        pragma Convention( C, C_Init );

        procedure C_First( state : Address; x, y, minor, major : Integer );
        pragma Convention( C, C_First );

        procedure C_Step( state : Address; isMinor : Integer );
        pragma Convention( C, C_Step );

        procedure C_Draw( state : Address; x1, y, x2 : Integer );
        pragma Convention( C, C_Draw );

        -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        procedure C_Init( state : Address; v1, v2, v3 : access Allegro_Vertex ) is
        begin
            init( state, v1, v2, v3 );
        end C_Init;

        procedure C_First( state : Address; x, y, minor, major : Integer ) is
        begin
            first( state, x, y, minor, major );
        end C_First;

        procedure C_Step( state : Address; isMinor : Integer ) is
        begin
            step( state, isMinor /= 0 );
        end C_Step;

        procedure C_Draw( state : Address; x1, y, x2 : Integer ) is
        begin
            draw( state, x1, y, x2 );
        end C_Draw;

        procedure C_Al_Draw_Soft_Triangle( v1, v2, v3 : access Allegro_Vertex;
                                           state      : Address;
                                           init       : A_C_Init;
                                           first      : A_C_First;
                                           step       : A_C_Step;
                                           draw       : A_C_Draw );
        pragma Import( C, C_Al_Draw_Soft_Triangle, "al_draw_soft_triangle" );
    begin
        C_Al_Draw_Soft_Triangle( v1, v2, v3, state, C_Init'Access, C_First'Access, C_Step'Access, C_Draw'Access );
    end Al_Draw_Soft_Triangle;

end Allegro.Drawing.Primitives;
