--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- Allegro 5.2.5 - Audio codecs addon
package Allegro.Audio.Codecs is

    function Al_Init_ACodec_Addon return Boolean;

    function Al_Get_Allegro_Acodec_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Acodec_Version, "al_get_allegro_acodec_version" );

end Allegro.Audio.Codecs;
