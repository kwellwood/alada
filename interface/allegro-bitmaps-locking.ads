--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with System;                            use System;

-- Allegro 5.2.5 - Bitmap locking routines
package Allegro.Bitmaps.Locking is

    type Allegro_Lock_Mode is private;
    ALLEGRO_LOCK_READWRITE : constant Allegro_Lock_Mode;
    ALLEGRO_LOCK_READONLY  : constant Allegro_Lock_Mode;
    ALLEGRO_LOCK_WRITEONLY : constant Allegro_Lock_Mode;

    type Allegro_Locked_Region is
        record
            data       : Address;
            format     : Allegro_Pixel_Format;
            pitch      : Integer;
            pixel_size : Integer;
        end record;
    pragma Convention( C, Allegro_Locked_Region );
    type A_Allegro_Locked_Region is access all Allegro_Locked_Region;

    function Al_Lock_Bitmap( bitmap : A_Allegro_Bitmap;
                             format : Allegro_Pixel_Format;
                             flags  : Allegro_Lock_Mode ) return A_Allegro_Locked_Region;
    pragma Import( C, Al_Lock_Bitmap, "al_lock_bitmap" );

    -- Wrapper for Al_Lock_Bitmap function that doesn't return the lock.
    procedure Al_Lock_Bitmap( bitmap : A_Allegro_Bitmap;
                              format : Allegro_Pixel_Format;
                              flags  : Allegro_Lock_Mode );

    function Al_Lock_Bitmap_Region( bitmap        : A_Allegro_Bitmap;
                                    x, y          : Integer;
                                    width, height : Integer;
                                    format        : Allegro_Pixel_Format;
                                    flags         : Allegro_Lock_Mode ) return A_Allegro_Locked_Region;
    pragma Import( C, Al_Lock_Bitmap_Region, "al_lock_bitmap_region" );

    -- Wrapper for Al_Lock_Bitmap_Region function that doesn't return the lock.
    procedure Al_Lock_Bitmap_Region( bitmap        : A_Allegro_Bitmap;
                                     x, y          : Integer;
                                     width, height : Integer;
                                     format        : Allegro_Pixel_Format;
                                     flags         : Allegro_Lock_Mode );

    function Al_Lock_Bitmap_Blocked( bitmap : A_Allegro_Bitmap;
                                     flags  : Allegro_Lock_Mode ) return A_Allegro_Locked_Region;
    pragma Import( C, Al_Lock_Bitmap_Blocked, "al_lock_bitmap_blocked" );

    function Al_Lock_Bitmap_Region_Blocked( bitmap       : A_Allegro_Bitmap;
                                            x_block,
                                            y_block      : Integer;
                                            width_block,
                                            height_block : Integer;
                                            flags        : Allegro_Lock_Mode ) return A_Allegro_Locked_Region;
    pragma Import( C, Al_Lock_Bitmap_Region_Blocked, "al_lock_bitmap_region_blocked" );

    procedure Al_Unlock_Bitmap( bitmap : A_Allegro_Bitmap );
    pragma Import( C, Al_Unlock_Bitmap, "al_unlock_bitmap" );

    function Al_Is_Bitmap_Locked( bitmap : A_Allegro_Bitmap ) return Boolean;

private

    type Allegro_Lock_Mode is new Integer;
    ALLEGRO_LOCK_READWRITE : constant Allegro_Lock_Mode := 0;
    ALLEGRO_LOCK_READONLY  : constant Allegro_Lock_Mode := 1;
    ALLEGRO_LOCK_WRITEONLY : constant Allegro_Lock_Mode := 2;

end Allegro.Bitmaps.Locking;
