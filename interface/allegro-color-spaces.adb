--
-- Copyright (c) 2013-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;
with System;                            use System;

package body Allegro.Color.Spaces is

    function Al_Is_Color_Valid( color : Allegro_Color ) return Boolean is
        function C_Al_Is_Color_Valid( color : Allegro_Color ) return Bool;
        pragma Import( C, C_Al_Is_Color_Valid, "al_is_color_valid" );
    begin
        return To_Ada( C_Al_Is_Color_Valid( color ) );
    end Al_Is_Color_Valid;

    ----------------------------------------------------------------------------

    function Al_Color_Distance_CIEDE2000( color1, color2 : Allegro_Color ) return Long_Float is
        function C_Al_Color_Distance_CIEDE2000( color1, color2 : Allegro_Color ) return Interfaces.C.double;
        pragma Import( C, C_Al_Color_Distance_CIEDE2000, "al_color_distance_ciede2000" );
    begin
        return Long_Float(C_Al_Color_Distance_CIEDE2000( color1, color2 ));
    end Al_Color_Distance_CIEDE2000;

    ----------------------------------------------------------------------------

    function Al_Color_HTML( str : String ) return Allegro_Color is

        function C_Al_Color_HTML( str : C.char_array ) return Allegro_Color;
        pragma Import( C, C_Al_Color_HTML, "al_color_html" );

    begin
        return C_Al_Color_HTML( To_C( str ) );
    end Al_Color_HTML;

    ----------------------------------------------------------------------------

    function Al_Color_HTML_To_RGB( str : String; red, green, blue : access Float ) return Boolean is

        function C_Al_Color_HTML_To_RGB( str              : C.char_array;
                                         red, green, blue : access Float ) return Bool;
        pragma Import( C, C_Al_Color_HTML_To_RGB, "al_color_html_to_rgb" );

    begin
        return To_Ada( C_Al_Color_HTML_To_RGB( To_C( str ), red, green, blue ) );
    end Al_Color_HTML_To_RGB;

    ----------------------------------------------------------------------------

    function Al_Color_RGB_To_HTML( red, green, blue : Float ) return String is

        procedure C_Al_Color_RGB_To_HTML( red, green, blue : Float;
                                          str              : Address );
        pragma Import( C, C_Al_Color_RGB_To_HTML, "al_color_rgb_to_html" );

        str : aliased String(1..8);
    begin
        C_Al_Color_RGB_To_HTML( red, green, blue, str(str'First)'Address );
        return str(str'First..str'Last-1);   -- don't return the nul terminator
    end Al_Color_RGB_To_HTML;

    ----------------------------------------------------------------------------

    function Al_Color_Name( name : String ) return Allegro_Color is

        function C_Al_Color_Name( name : C.char_array ) return Allegro_Color;
        pragma Import( C, C_Al_Color_Name, "al_color_name" );

    begin
        return C_Al_Color_Name( To_C( name ) );
    end Al_Color_Name;

    ----------------------------------------------------------------------------

    function Al_Color_Name_To_RGB( name : String; r, g, b : access Float ) return Boolean is

        function C_Al_Color_Name_To_RGB( name    : C.char_array;
                                         r, g, b : access Float ) return Bool;
        pragma Import( C, C_Al_Color_Name_To_RGB, "al_color_name_to_rgb" );

    begin
        return To_Ada( C_Al_Color_Name_To_RGB( To_C( name ), r, g, b ) );
    end Al_Color_Name_To_RGB;

    ----------------------------------------------------------------------------

    function Al_Color_RGB_To_Name( r, g, b : Float ) return String is

        function C_Al_Color_RGB_To_Name( r, g, b : Float ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Color_RGB_To_Name, "al_color_rgb_to_name" );

    begin
        return To_Ada( Value( C_Al_Color_RGB_To_Name( r, g, b ) ) );
    end Al_Color_RGB_To_Name;

end Allegro.Color.Spaces;
