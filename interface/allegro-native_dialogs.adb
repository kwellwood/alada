--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

package body Allegro.Native_Dialogs is

    function Al_Init_Native_Dialog_Addon return Boolean is

        function C_Al_Init_Native_Dialog_Addon return Bool;
        pragma Import( C, C_Al_Init_Native_Dialog_Addon, "al_init_native_dialog_addon" );

    begin
        return To_Ada( C_Al_Init_Native_Dialog_Addon );
    end Al_Init_Native_Dialog_Addon;

    ----------------------------------------------------------------------------

    function Al_Create_Native_File_Dialog( initial_path : String;
                                           title        : String;
                                           patterns     : String;
                                           mode         : Filechooser_Flags
                                         ) return A_Allegro_Filechooser is

        function C_Al_Create_Native_File_Dialog( initial_path : C.char_array;
                                                 title        : C.char_array;
                                                 patterns     : C.char_array;
                                                 mode         : Filechooser_Flags
                                               ) return A_Allegro_Filechooser;
        pragma Import( C, C_Al_Create_Native_File_Dialog, "al_create_native_file_dialog" );

    begin
        return C_Al_Create_Native_File_Dialog( To_C( initial_path ),
                                               To_C( title ),
                                               To_C( patterns ),
                                               mode );
    end Al_Create_Native_File_Dialog;

    ----------------------------------------------------------------------------

    function Al_Show_Native_File_Dialog( display : A_Allegro_Display;
                                         dialog  : A_Allegro_Filechooser ) return Boolean is

        function C_Al_Show_Native_File_Dialog( display : A_Allegro_Display;
                                               dialog  : A_Allegro_Filechooser ) return Bool;
        pragma Import( C, C_Al_Show_Native_File_Dialog, "al_show_native_file_dialog" );

    begin
        return To_Ada( C_Al_Show_Native_File_Dialog( display, dialog ) );
    end Al_Show_Native_File_Dialog;

    ----------------------------------------------------------------------------

    function Al_Get_Native_File_Dialog_Path( dialog : A_Allegro_Filechooser;
                                             index  : Natural ) return String is

        function C_Al_Get_Native_File_Dialog_Path( dialog : A_Allegro_Filechooser;
                                                   index  : C.size_t
                                                 ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Native_File_Dialog_Path, "al_get_native_file_dialog_path" );

    begin
        if C_Al_Get_Native_File_Dialog_Path( dialog, C.size_t(index) ) /= Null_Ptr then
            return To_Ada( Value( C_Al_Get_Native_File_Dialog_Path( dialog, C.size_t(index) ) ) );
        end if;
        return "";
    end Al_Get_Native_File_Dialog_Path;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Native_File_Dialog( dialog : in out A_Allegro_Filechooser ) is

        procedure C_Al_Destroy_Native_File_Dialog( dialog : A_Allegro_Filechooser );
        pragma Import( C, C_Al_Destroy_Native_File_Dialog, "al_destroy_native_file_dialog" );

    begin
        if dialog /= null then
            C_Al_Destroy_Native_File_Dialog( dialog );
            dialog := null;
        end if;
    end Al_Destroy_Native_File_Dialog;

    ----------------------------------------------------------------------------

    function Al_Show_Native_Message_Box( display : A_Allegro_Display;
                                         title   : String;
                                         heading : String;
                                         text    : String;
                                         buttons : String;
                                         flags   : Messagebox_Flags ) return Integer is

        function C_Al_Show_Native_Message_Box( display : A_Allegro_Display;
                                               title   : C.char_array;
                                               heading : C.char_array;
                                               text    : C.char_array;
                                               buttons : C.char_array;
                                               flags   : Messagebox_Flags ) return Integer;
        pragma Import( C, C_Al_Show_Native_Message_Box, "al_show_native_message_box" );

        function C_Al_Show_Native_Message_Box_Simple( display : A_Allegro_Display;
                                                      title   : C.char_array;
                                                      heading : C.char_array;
                                                      text    : C.char_array;
                                                      buttons : Address;    -- pass null
                                                      flags   : Messagebox_Flags ) return Integer;
        pragma Import( C, C_Al_Show_Native_Message_Box_Simple, "al_show_native_message_box" );

    begin
        if buttons'Length > 0 then
            return C_Al_Show_Native_Message_Box( display, To_C( title ),
                                                 To_C( heading ), To_C( text ),
                                                 To_C( buttons ), flags );
        else
            return C_Al_Show_Native_Message_Box_Simple( display, To_C( title ),
                                                        To_C( heading ), To_C( text ),
                                                        Null_Address, flags );
        end if;
    end Al_Show_Native_Message_Box;

    ----------------------------------------------------------------------------

    procedure Al_Show_Native_Message_Box( display : A_Allegro_Display;
                                          title   : String;
                                          heading : String;
                                          text    : String;
                                          buttons : String;
                                          flags   : Messagebox_Flags ) is
        ignored : Integer;
    begin
        ignored := Al_Show_Native_Message_Box( display,
                                               title,
                                               heading,
                                               text,
                                               buttons,
                                               flags );
    end Al_Show_Native_Message_Box;

    ----------------------------------------------------------------------------

    function Al_Open_Native_Text_Log( title : String; flags : Textlog_Flags ) return A_Allegro_Textlog is

        function C_Al_Open_Native_Text_Log( title : C.char_array;
                                            flags : Textlog_Flags ) return A_Allegro_Textlog;
        pragma Import( C, C_Al_Open_Native_Text_Log, "al_open_native_text_log" );

    begin
        return C_Al_Open_Native_Text_Log( To_C( title ), flags );
    end Al_Open_Native_Text_Log;

    ----------------------------------------------------------------------------

    procedure Al_Close_Native_Text_Log( textlog : in out A_Allegro_Textlog ) is

        procedure C_Al_Close_Native_Text_Log( textlog : A_Allegro_Textlog );
        pragma Import( C, C_Al_Close_Native_Text_Log, "al_close_native_text_log" );

    begin
        if textlog /= null then
            C_Al_Close_Native_Text_Log( textlog );
            textlog := null;
        end if;
    end Al_Close_Native_Text_Log;

    ----------------------------------------------------------------------------

    function Al_Build_Menu( info : Allegro_Menu_Info_Array ) return A_Allegro_Menu is
        function C_Al_Build_Menu( info : Address ) return A_Allegro_Menu;
        pragma Import( C, C_Al_Build_Menu, "al_build_menu" );
    begin
        return C_Al_Build_Menu( info(info'First)'Address );
    end Al_Build_Menu;

    ----------------------------------------------------------------------------

    procedure Al_Append_Native_Text_Log( textlog : A_Allegro_Textlog; str : String ) is

        procedure C_Al_Append_Native_Text_Log( textlog : A_Allegro_Textlog;
                                               str     : C.char_array );
        pragma Import( C, C_Al_Append_Native_Text_Log, "al_append_native_text_log_s" );

    begin
        C_Al_Append_Native_Text_Log( textlog, To_C( str ) );
    end Al_Append_Native_Text_Log;

    ----------------------------------------------------------------------------

    function Al_Append_Menu_Item( parent  : A_Allegro_Menu;
                                  title   : String;
                                  id      : Unsigned_16;
                                  flags   : Menu_Item_Flags;
                                  icon    : A_Allegro_Bitmap;
                                  submenu : A_Allegro_Menu ) return Integer is

        function C_Al_Append_Menu_Item( parent  : A_Allegro_Menu;
                                        title   : C.Strings.chars_ptr;
                                        id      : Unsigned_16;
                                        flags   : Menu_Item_Flags;
                                        icon    : A_Allegro_Bitmap;
                                        submenu : A_Allegro_Menu ) return Integer;
        pragma Import( C, C_Al_Append_Menu_Item, "al_append_menu_item" );

        title2 : chars_ptr := (if title'Length > 0 then New_String( title ) else Null_Ptr);
    begin
        return i : constant Integer := C_Al_Append_Menu_Item( parent, title2, id, flags, icon, submenu ) do
            Free( title2 );
        end return;
    end Al_Append_Menu_Item;

    ----------------------------------------------------------------------------

    procedure Al_Append_Menu_Item( parent  : A_Allegro_Menu;
                                   title   : String;
                                   id      : Unsigned_16;
                                   flags   : Menu_Item_Flags;
                                   icon    : A_Allegro_Bitmap;
                                   submenu : A_Allegro_Menu ) is
        ignored : Integer;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Append_Menu_Item( parent, title, id, flags, icon, submenu );
    end Al_Append_Menu_Item;

    ----------------------------------------------------------------------------

    function Al_Insert_Menu_Item( parent  : A_Allegro_Menu;
                                  pos     : Integer;
                                  title   : String;
                                  id      : Unsigned_16;
                                  flags   : Menu_Item_Flags;
                                  icon    : A_Allegro_Bitmap;
                                  submenu : A_Allegro_Menu ) return Integer is

        function C_Al_Insert_Menu_Item( parent  : A_Allegro_Menu;
                                        pos     : Integer;
                                        title   : C.Strings.chars_ptr;
                                        id      : Unsigned_16;
                                        flags   : Menu_Item_Flags;
                                        icon    : A_Allegro_Bitmap;
                                        submenu : A_Allegro_Menu ) return Integer;
        pragma Import( C, C_Al_Insert_Menu_Item, "al_insert_menu_item" );

        title2 : chars_ptr := (if title'Length > 0 then New_String( title ) else Null_Ptr);
    begin
        return i : constant Integer := C_Al_Insert_Menu_Item( parent, pos, title2, id, flags, icon, submenu ) do
            Free( title2 );
        end return;
    end Al_Insert_Menu_Item;

    ----------------------------------------------------------------------------

    procedure Al_Insert_Menu_Item( parent  : A_Allegro_Menu;
                                   pos     : Integer;
                                   title   : String;
                                   id      : Unsigned_16;
                                   flags   : Menu_Item_Flags;
                                   icon    : A_Allegro_Bitmap;
                                   submenu : A_Allegro_Menu ) is
        ignored : Integer;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Insert_Menu_Item( parent, pos, title, id, flags, icon, submenu );
    end Al_Insert_Menu_Item;

    ----------------------------------------------------------------------------

    function Al_Remove_Menu_Item( menu : A_Allegro_Menu; pos : Integer ) return Boolean is
        function C_Al_Remove_Menu_Item( menu : A_Allegro_Menu; pos : Integer ) return Bool;
        pragma Import( C, C_Al_Remove_Menu_Item, "al_remove_menu_item" );
    begin
        return C_Al_Remove_Menu_Item( menu, pos ) = B_TRUE;
    end Al_Remove_Menu_Item;

    ----------------------------------------------------------------------------

    procedure Al_Remove_Menu_Item( menu : A_Allegro_Menu; pos : Integer ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Remove_Menu_Item( menu, pos );
    end Al_Remove_Menu_Item;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Menu( menu : in out A_Allegro_Menu ) is
        procedure C_Al_Destroy_Menu( menu : A_Allegro_Menu );
        pragma Import( C, C_Al_Destroy_Menu, "al_destroy_menu" );
    begin
        if menu /= null then
            C_Al_Destroy_Menu( menu );
            menu := null;
        end if;
    end Al_Destroy_Menu;

    ----------------------------------------------------------------------------

    function Al_Get_Menu_Item_Caption( menu : A_Allegro_Menu; pos : Integer ) return String is
        function C_Al_Get_Menu_Item_Caption( menu : A_Allegro_Menu; pos : Integer ) return chars_ptr;
        pragma Import( C, C_Al_Get_Menu_Item_Caption, "al_get_menu_item_caption" );
    begin
        return Value( C_Al_Get_Menu_Item_Caption( menu, pos ) );
    end Al_Get_Menu_Item_Caption;

    ----------------------------------------------------------------------------

    procedure Al_Set_Menu_Item_Caption( menu    : A_Allegro_Menu;
                                        pos     : Integer;
                                        caption : String ) is
        procedure C_Al_Set_Menu_Item_Caption( menu    : A_Allegro_Menu;
                                              pos     : Integer;
                                              caption : C.char_array );
        pragma Import( C, C_Al_Set_Menu_Item_Caption, "al_set_menu_item_caption" );
    begin
        C_Al_Set_Menu_Item_Caption( menu, pos, To_C( caption ) );
    end Al_Set_Menu_Item_Caption;

    ----------------------------------------------------------------------------

    function Al_Find_Menu_Item( haystack : A_Allegro_Menu;
                                id       : Unsigned_16;
                                menu     : access A_Allegro_Menu;
                                index    : access Integer ) return Boolean is
        function C_Al_Find_Menu_Item( haystack : A_Allegro_Menu;
                                      id       : Unsigned_16;
                                      menu     : access A_Allegro_Menu;
                                      index    : access Integer ) return Bool;
        pragma Import( C, C_Al_Find_Menu_Item, "al_find_menu_item" );
    begin
        return C_Al_Find_Menu_Item( haystack, id, menu, index ) = B_TRUE;
    end Al_Find_Menu_Item;

    ----------------------------------------------------------------------------

    function Al_Set_Display_Menu( display : A_Allegro_Display; menu : A_Allegro_Menu ) return Boolean is
        function C_Al_Set_Display_Menu( display : A_Allegro_Display; menu : A_Allegro_Menu ) return Bool;
        pragma Import( C, C_Al_Set_Display_Menu, "al_set_display_menu" );
    begin
        return C_Al_Set_Display_Menu( display, menu ) = B_TRUE;
    end Al_Set_Display_Menu;

    ----------------------------------------------------------------------------

    procedure Al_Set_Display_Menu( display : A_Allegro_Display; menu : A_Allegro_Menu ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Display_Menu( display, menu );
    end Al_Set_Display_Menu;

    ----------------------------------------------------------------------------

    function Al_Popup_Menu( popup : A_Allegro_Menu; display : A_Allegro_Display ) return Boolean is
        function C_Al_Popup_Menu( popup : A_Allegro_Menu; display : A_Allegro_Display ) return Bool;
        pragma Import( C, C_Al_Popup_Menu, "al_popup_menu" );
    begin
        return C_Al_Popup_Menu( popup, display ) = B_TRUE;
    end Al_Popup_Menu;

    ----------------------------------------------------------------------------

    procedure Al_Popup_Menu( popup : A_Allegro_Menu; display : A_Allegro_Display ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Popup_Menu( popup, display );
    end Al_Popup_Menu;

    ----------------------------------------------------------------------------

    procedure Al_Remove_Display_Menu( display : A_Allegro_Display ) is
        ignored : A_Allegro_Menu;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Remove_Display_Menu( display );
    end Al_Remove_Display_Menu;

end Allegro.Native_Dialogs;
