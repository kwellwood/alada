--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.File_IO;                   use Allegro.File_IO;

-- Allegro 5.2.5 - Configuration routines
package Allegro.Configuration is

    type Allegro_Config is limited private;
    type A_Allegro_Config is access all Allegro_Config;

    type Allegro_Config_Section is limited private;
    type A_Allegro_Config_Section is access all Allegro_Config_Section;

    type Allegro_Config_Entry is limited private;
    type A_Allegro_Config_Entry is access all Allegro_Config_Entry;

    function Al_Create_Config return A_Allegro_Config;
    pragma Import( C, Al_Create_Config, "al_create_config" );

    procedure Al_Destroy_Config( config : in out A_Allegro_Config );

    function Al_Load_Config_File( filename : String ) return A_Allegro_Config;

    function Al_Load_Config_File_f( file : A_Allegro_File ) return A_Allegro_Config;
    pragma Import( C, Al_Load_Config_File_f, "al_load_config_file_f" );

    function Al_Save_Config_File( filename : String; config : A_Allegro_Config ) return Boolean;

    function Al_Save_Config_File_f( file   : A_Allegro_File;
                                    config : A_Allegro_Config ) return Boolean;

    procedure Al_Add_Config_Section( config : A_Allegro_Config;
                                     name   : String );

    procedure Al_Add_Config_Comment( config  : A_Allegro_Config;
                                     section : String;
                                     comment : String );

    function Al_Get_Config_Value( config  : A_Allegro_Config;
                                  section : String;
                                  key     : String ) return String;

    procedure Al_Set_Config_Value( config  : A_Allegro_Config;
                                   section : String;
                                   key     : String;
                                   value   : String );

    function Al_Get_First_Config_Section( config   : A_Allegro_Config;
                                          iterator : access A_Allegro_Config_Section
                                        ) return String;

    function Al_Get_Next_Config_Section( iterator : access A_Allegro_Config_Section ) return String;

    function Al_Get_First_Config_Entry( config   : A_Allegro_Config;
                                          section  : String;
                                          iterator : access A_Allegro_Config_Entry
                                        ) return String;

    function Al_Get_Next_Config_Entry( iterator : access A_Allegro_Config_Entry ) return String;

    function Al_Merge_Config( cfg1, cfg2 : A_Allegro_Config ) return A_Allegro_Config;
    pragma Import( C, Al_Merge_Config, "al_merge_config" );

    procedure Al_Merge_Config_Into( master : A_Allegro_Config; add : A_Allegro_Config );
    pragma Import( C, Al_Merge_Config_Into, "al_merge_config_into" );

    function Al_Remove_Config_Section( config  : A_Allegro_Config;
                                       section : String ) return Boolean;

    procedure Al_Remove_Config_Section( config : A_Allegro_Config; section : String );

    function Al_Remove_Config_Key( config  : A_Allegro_Config;
                                   section : String;
                                   key     : String ) return Boolean;

    procedure Al_Remove_Config_Key( config  : A_Allegro_Config;
                                    section : String;
                                    key     : String );

private

    type Allegro_Config is limited null record;
    pragma Convention( C, Allegro_Config );

    type Allegro_Config_Section is limited null record;
    pragma Convention( C, Allegro_Config_Section );

    type Allegro_Config_Entry is limited null record;
    pragma Convention( C, Allegro_Config_Entry );

end Allegro.Configuration;
