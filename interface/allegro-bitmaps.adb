--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Bitmaps is

    procedure Al_Destroy_Bitmap( bitmap : in out A_Allegro_Bitmap ) is

        procedure C_Al_Destroy_Bitmap( bitmap : A_Allegro_Bitmap );
        pragma Import( C, C_Al_Destroy_Bitmap, "al_destroy_bitmap" );

    begin
        if bitmap /= null then
            C_Al_Destroy_Bitmap( bitmap );
            bitmap := null;
        end if;
    end Al_Destroy_Bitmap;

    ----------------------------------------------------------------------------

    function Al_Is_Sub_Bitmap( bitmap : A_Allegro_Bitmap ) return Boolean is

        function C_Al_Is_Sub_Bitmap( bitmap : A_Allegro_Bitmap ) return Bool;
        pragma Import( C, C_Al_Is_Sub_Bitmap, "al_is_sub_bitmap" );

    begin
        return To_Ada( C_Al_Is_Sub_Bitmap( bitmap ) );
    end Al_Is_Sub_Bitmap;

end Allegro.Bitmaps;
