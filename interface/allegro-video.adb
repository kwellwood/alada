--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;

package body Allegro.Video is

    function Al_Init_Video_Addon return Boolean is

        function C_Al_Init_Video_Addon return Bool;
        pragma Import( C, C_Al_Init_Video_Addon, "al_init_video_addon" );

    begin
        return C_Al_Init_Video_Addon = B_TRUE;
    end Al_Init_Video_Addon;

    ----------------------------------------------------------------------------

    function Al_Open_Video( filename : String ) return A_Allegro_Video is
        function C_Al_Open_Video( filename : C.char_array ) return A_Allegro_Video;
        pragma Import( C, C_Al_Open_Video, "al_open_video" );
    begin
        return C_Al_Open_Video( To_C( filename ) );
    end Al_Open_Video;

    ----------------------------------------------------------------------------

    procedure Al_Close_Video( video : in out A_Allegro_Video ) is
        procedure C_Al_Close_Video( video : A_Allegro_Video );
        pragma Import( C, C_Al_Close_Video, "al_close_video" );
    begin
        if video /= null then
            C_Al_Close_Video( video );
            video := null;
        end if;
    end Al_Close_Video;

    ----------------------------------------------------------------------------

    procedure Al_Set_Video_Playing( video : A_Allegro_Video; playing : Boolean ) is
        procedure C_Al_Set_Video_Playing( video : A_Allegro_Video; playing : Bool );
        pragma Import( C, C_Al_Set_Video_Playing, "al_set_video_playing" );
    begin
        C_Al_Set_Video_Playing( video, (if playing then B_TRUE else B_FALSE) );
    end Al_Set_Video_Playing;

    ----------------------------------------------------------------------------

    function Al_Is_Video_Playing( video : A_Allegro_Video ) return Boolean is
        function C_Al_Is_Video_Playing( video : A_Allegro_Video ) return Bool;
        pragma Import( C, C_Al_Is_Video_Playing, "al_is_video_playing" );
    begin
        return C_Al_Is_Video_Playing( video ) = B_TRUE;
    end Al_Is_Video_Playing;

    ----------------------------------------------------------------------------

    function Al_Seek_Video( video : A_Allegro_Video; pos_in_seconds : Long_Float ) return Boolean is
        function C_Al_Seek_Video( video : A_Allegro_Video; pos_in_seconds : Long_Float ) return Bool;
        pragma Import( C, C_Al_Seek_Video, "al_seek_video" );
    begin
        return C_Al_Seek_Video( video, pos_in_seconds ) = B_TRUE;
    end Al_Seek_Video;

    ----------------------------------------------------------------------------

    procedure Al_Seek_Video( video : A_Allegro_Video; pos_in_seconds : Long_Float ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Seek_Video( video, pos_in_seconds );
    end Al_Seek_Video;

end Allegro.Video;
