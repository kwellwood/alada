--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;

-- Allegro 5.2.5 - Touch input routines
package Allegro.Touch_Input is

    type Allegro_Touch_Input is limited private;
    type A_Allegro_Touch_Input is access all Allegro_Touch_Input;

    ALLEGRO_TOUCH_INPUT_MAX_TOUCH_COUNT : constant := 16;

    type Allegro_Touch_State is
        record
            id      : Integer := 0;
            x, y    : Float := 0.0;
            dx, dy  : Float := 0.0;
            primary : Boolean := False;
            display : A_Allegro_Display := null;
        end record;
    pragma Convention( C, Allegro_Touch_State );
    type A_Allegro_Touch_State is access all Allegro_Touch_State;

    type Allegro_Touch_State_Array is array (Integer range <>) of Allegro_Touch_State;

    type Allegro_Touch_Input_State is
        record
            touches : Allegro_Touch_State_Array(0..ALLEGRO_TOUCH_INPUT_MAX_TOUCH_COUNT-1);
        end record;
    pragma Convention( C, Allegro_Touch_Input_State );
    type A_Allegro_Touch_Input_State is access all Allegro_Touch_Input_State;

    -- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    function Al_Is_Touch_Input_Installed return Boolean;

    function Al_Install_Touch_Input return Boolean;

    procedure Al_Uninstall_Touch_Input;
    pragma Import( C, Al_Uninstall_Touch_Input, "al_uninstall_touch_input" );

    procedure Al_Get_Touch_Input_State( ret_state : access Allegro_Touch_Input_State );
    pragma Import( C, Al_Get_Touch_Input_State, "al_get_touch_input_state" );

    function Al_Get_Touch_Input_Event_Source return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Touch_Input_Event_Source, "al_get_touch_input_event_source" );

private

    type Allegro_Touch_Input is limited null record;

end Allegro.Touch_Input;
