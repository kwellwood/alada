--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Displays;                  use Allegro.Displays;
with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Mouse routines
package Allegro.Mouse is

    type Allegro_Mouse is limited private;
    type A_Allegro_Mouse is access all Allegro_Mouse;

    type Axes_Array is array(Integer range <>) of Integer;
    pragma Convention( C, Axes_Array );

    type Allegro_Mouse_State is
        record
            x, y      : Integer := 0;
            z, w      : Integer := 0;
            more_axes : Axes_Array(0..3) := (others => 0);
            buttons   : Unsigned_32 := 0;
            pressure  : Float := 0.0;
            display   : A_Allegro_Display;
        end record;
    pragma Convention( C, Allegro_Mouse_State );
    type A_Mouse_State is access all Allegro_Mouse_State;

    function Al_Install_Mouse return Boolean;

    procedure Al_Uninstall_Mouse;
    pragma Import( C, Al_Uninstall_Mouse, "al_uninstall_mouse" );

    function Al_Is_Mouse_Installed return Boolean;

    function Al_Get_Mouse_Num_Axes return Integer;
    pragma Import( C, Al_Get_Mouse_Num_Axes, "al_get_mouse_num_axes" );

    function Al_Get_Mouse_Num_Buttons return Integer;
    pragma Import( C, Al_Get_Mouse_Num_Buttons, "al_get_mouse_num_buttons" );

    procedure Al_Get_Mouse_State( ret_state : out Allegro_Mouse_State );
    pragma Import( C, Al_Get_Mouse_State, "al_get_mouse_state" );

    function Al_Get_Mouse_State_Axis( state : Allegro_Mouse_State; axis : Integer ) return Integer;
    pragma Import( C, Al_Get_Mouse_State_Axis, "al_get_mouse_state_axis" );

    function Al_Mouse_Button_Down( state : Allegro_Mouse_State; button : Integer ) return Boolean;

    function Al_Set_Mouse_XY( display : A_Allegro_Display; x, y : Integer ) return Boolean;

    function Al_Set_Mouse_Z( z : Integer ) return Boolean;

    function Al_Set_Mouse_W( w : Integer ) return Boolean;

    function Al_Set_Mouse_Axis( axis : Integer; value : Integer ) return Boolean;

    function Al_Get_Mouse_Cursor_Position( ret_x, ret_y : access Integer ) return Boolean;

    function Al_Grab_Mouse( display : A_Allegro_Display ) return Boolean;

    function Al_Ungrab_Mouse return Boolean;

    procedure Al_Set_Mouse_Wheel_Precision( precision : Integer );
    pragma Import( C, Al_Set_Mouse_Wheel_Precision, "al_set_mouse_wheel_precision" );

    function Al_Get_Mouse_Wheel_Precision return Integer;
    pragma Import( C, Al_Get_Mouse_Wheel_Precision, "al_get_mouse_wheel_precision" );

    function Al_Get_Mouse_Event_Source return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Mouse_Event_Source, "al_get_mouse_event_source" );

private

    type Allegro_Mouse is limited null record;
    pragma Convention( C, Allegro_Mouse );

end Allegro.Mouse;
