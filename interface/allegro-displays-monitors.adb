--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Displays.Monitors is

    function Al_Get_Monitor_Info( adapter : Integer; info : access Allegro_Monitor_Info ) return Boolean is

        function C_Al_Get_Monitor_Info( adapter : Integer; info : access Allegro_Monitor_Info ) return Bool;
        pragma Import( C, C_Al_Get_Monitor_Info, "al_get_monitor_info" );

    begin
        return To_Ada( C_Al_Get_Monitor_Info( adapter, info ) );
    end Al_Get_Monitor_Info;

    ----------------------------------------------------------------------------

    procedure Al_Get_Monitor_Info( adapter : Integer;
                                   info    : out Allegro_Monitor_Info;
                                   success : out Boolean ) is
        info2 : aliased Allegro_Monitor_Info;
    begin
        success := Al_Get_Monitor_Info( adapter, info2'Access );
        info := info2;
    end Al_Get_Monitor_Info;

    ----------------------------------------------------------------------------

    procedure Al_Get_Monitor_Info( adapter : Integer;
                                   info    : out Allegro_Monitor_Info ) is
        success : Boolean;
    begin
        Al_Get_Monitor_Info( adapter, info, success );
    end Al_Get_Monitor_Info;

end Allegro.Displays.Monitors;
