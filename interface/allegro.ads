--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- Allegro 5.2.5 - Base Allegro API package
package Allegro is

    pragma Elaborate_Body;

    type Allegro_Event_Source is limited private;
    type A_Allegro_Event_Source is access all Allegro_Event_Source;

    -- Frees the memory allocated for 's'. The Allegro_Event_Source, if
    -- initialized by Allegro, must be finalized with Al_Destroy_Event_Source()
    -- before calling this.
    procedure Delete( s : in out A_Allegro_Event_Source );

     -- Binding type for Allegro's "bool".
    type Bool is private;

    B_FALSE : constant Bool;
    B_TRUE  : constant Bool;     -- not for comparison, only for passing a value

    function To_Ada( b : Bool ) return Boolean with Inline_Always;

    subtype off_t is Long_Integer;

    type time_t is mod 2 ** 32;
    for time_t'Size use 32;

    ALLEGRO_PI : constant := 3.14159265358979;

    type Integer_Array is array (Natural range <>) of Integer;
    pragma Convention( C, Integer_Array );

    type Float_Array is array (Natural range <>) of Float;
    pragma Convention( C, Float_Array );

    type Ada_Main_Procedure is access procedure;

    -- Sets the main Ada procedure for the application at elaboration time.
    -- If it has already been set, a Program_Error exception will be raised.
    procedure Set_Ada_Main( main : Ada_Main_Procedure );

private

    ada_main : Ada_Main_Procedure;

    -- Allegro defines 'bool' as 1 byte
    type Bool is mod 2 ** 8;
    for Bool'Size use 8;

    B_FALSE : constant Bool := 0;
    B_TRUE  : constant Bool := 1;

    type IntArray is array (Integer range <>) of Integer;

    type Allegro_Event_Source is limited
        record
            pad : IntArray(0..31);
        end record;
    pragma Convention( C, Allegro_Event_Source );

end Allegro;
