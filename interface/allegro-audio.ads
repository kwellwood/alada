--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Events;                    use Allegro.Events;
with Allegro.File_IO;                   use Allegro.File_IO;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

-- Allegro 5.2.5 - Audio
package Allegro.Audio is

    -- Audio types

    type Allegro_Audio_Depth is (
        ALLEGRO_AUDIO_DEPTH_INT8,
        ALLEGRO_AUDIO_DEPTH_INT16,
        ALLEGRO_AUDIO_DEPTH_INT24,
        ALLEGRO_AUDIO_DEPTH_FLOAT32,
        ALLEGRO_AUDIO_DEPTH_UINT8,
        ALLEGRO_AUDIO_DEPTH_UINT16,
        ALLEGRO_AUDIO_DEPTH_UINT24
    );
    for Allegro_Audio_Depth use (
        ALLEGRO_AUDIO_DEPTH_INT8     =>  0,
        ALLEGRO_AUDIO_DEPTH_INT16    =>  1,
        ALLEGRO_AUDIO_DEPTH_INT24    =>  2,
        ALLEGRO_AUDIO_DEPTH_FLOAT32  =>  3,
        ALLEGRO_AUDIO_DEPTH_UINT8    =>  8,  -- ALLEGRO_AUDIO_DEPTH_INT8   or 8
        ALLEGRO_AUDIO_DEPTH_UINT16   =>  9,  -- ALLEGRO_AUDIO_DEPTH_INT24  or 8
        ALLEGRO_AUDIO_DEPTH_UINT24   => 10   -- ALLEGRO_AUDIO_DEPTH_UINT24 or 8
    );
    pragma Convention( C, Allegro_Audio_Depth );

    type Allegro_Audio_Pan is new Float;

    Allegro_Audio_Pan_None : constant Allegro_Audio_Pan := -1000.0;

    type Allegro_Channel_Conf is (
        ALLEGRO_CHANNEL_CONF_1,
        ALLEGRO_CHANNEL_CONF_2,
        ALLEGRO_CHANNEL_CONF_3,
        ALLEGRO_CHANNEL_CONF_4,
        ALLEGRO_CHANNEL_CONF_5_1,
        ALLEGRO_CHANNEL_CONF_6_1,
        ALLEGRO_CHANNEL_CONF_7_1
    );
    for Allegro_Channel_Conf use (
        ALLEGRO_CHANNEL_CONF_1   => 16#10#,
        ALLEGRO_CHANNEL_CONF_2   => 16#20#,
        ALLEGRO_CHANNEL_CONF_3   => 16#30#,
        ALLEGRO_CHANNEL_CONF_4   => 16#40#,
        ALLEGRO_CHANNEL_CONF_5_1 => 16#51#,
        ALLEGRO_CHANNEL_CONF_6_1 => 16#61#,
        ALLEGRO_CHANNEL_CONF_7_1 => 16#71#
    );
    pragma Convention( C, Allegro_Channel_Conf );

    ALLEGRO_MAX_CHANNELS : constant := 8;

    type Allegro_Mixer is limited private;
    type A_Allegro_Mixer is access all Allegro_Mixer;

    type Allegro_Mixer_Quality is (
        ALLEGRO_MIXER_QUALITY_POINT,
        ALLEGRO_MIXER_QUALITY_LINEAR,
        ALLEGRO_MIXER_QUALITY_CUBIC
    );
    for Allegro_Mixer_Quality use (
        ALLEGRO_MIXER_QUALITY_POINT  => 16#110#,
        ALLEGRO_MIXER_QUALITY_LINEAR => 16#111#,
        ALLEGRO_MIXER_QUALITY_CUBIC  => 16#112#
    );
    pragma Convention( C, Allegro_Mixer_Quality );

    type Allegro_Playmode is (
        ALLEGRO_PLAYMODE_ONCE,
        ALLEGRO_PLAYMODE_LOOP,
        ALLEGRO_PLAYMODE_BIDIR
    );
    for Allegro_Playmode use (
        ALLEGRO_PLAYMODE_ONCE  => 16#100#,
        ALLEGRO_PLAYMODE_LOOP  => 16#101#,
        ALLEGRO_PLAYMODE_BIDIR => 16#102#
    );
    pragma Convention( C, Allegro_Playmode );

    ALLEGRO_EVENT_AUDIO_STREAM_FRAGMENT   : constant Allegro_Event_Type := 513;
    ALLEGRO_EVENT_AUDIO_STREAM_FINISHED   : constant Allegro_Event_Type := 514;
    ALLEGRO_EVENT_AUDIO_RECORDER_FRAGMENT : constant Allegro_Event_Type := 515;

    type Allegro_Sample_Id is
        record
            index : Integer;
            id    : Integer;
        end record;
    pragma Convention( C, Allegro_Sample_Id );

    type Allegro_Sample is limited private;
    type A_Allegro_Sample is access all Allegro_Sample;

    type Allegro_Sample_Instance is limited private;
    type A_Allegro_Sample_Instance is access all Allegro_Sample_Instance;

    type Allegro_Audio_Stream is limited private;
    type A_Allegro_Audio_Stream is access all Allegro_Audio_Stream;

    type Allegro_Voice is limited private;
    type A_Allegro_Voice is access all Allegro_Voice;

    -- Setting up audio

    function Al_Install_Audio return Boolean;

    procedure Al_Uninstall_Audio;
    pragma Import( C, Al_Uninstall_Audio, "al_uninstall_audio" );

    function Al_Is_Audio_Installed return Boolean;

    function Al_Reserve_Samples( reserve_samples : Integer ) return Boolean;

    -- Misc audio functions

    function Al_Get_Allegro_Audio_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Audio_Version, "al_get_allegro_audio_version" );

    function Al_Get_Audio_Depth_Size( conf : Allegro_Audio_Depth ) return size_t;
    pragma Import( C, Al_Get_Audio_Depth_Size, "al_get_audio_depth_size" );

    function Al_Get_Channel_Count( conf : Allegro_Channel_Conf ) return size_t;
    pragma Import( C, Al_Get_Channel_Count, "al_get_channel_count" );

    -- Voice functions

    function Al_Get_Default_Voice return A_Allegro_Voice;
    pragma Import( C, Al_Get_Default_Voice, "al_get_default_voice" );

    procedure Al_Set_Default_Voice( voice : A_Allegro_Voice );
    pragma Import( C, Al_Set_Default_Voice, "al_set_default_voice" );

    function Al_Create_Voice( freq      : unsigned;
                              depth     : Allegro_Audio_Depth;
                              chan_conf : Allegro_Channel_Conf ) return A_Allegro_Voice;
    pragma Import( C, Al_Create_Voice, "al_create_voice" );

    procedure Al_Destroy_Voice( voice : in out A_Allegro_Voice );

    procedure Al_Detach_Voice( voice : A_Allegro_Voice );
    pragma Import( C, Al_Detach_Voice, "al_detach_voice" );

    function Al_Attach_Audio_Stream_To_Voice( stream : A_Allegro_Audio_Stream;
                                              voice  : A_Allegro_Voice ) return Boolean;

    function Al_Attach_Mixer_To_Voice( mixer : A_Allegro_Mixer;
                                       voice : A_Allegro_Voice ) return Boolean;

    function Al_Attach_Sample_Instance_To_Voice( stream : A_Allegro_Sample_Instance;
                                                 voice  : A_Allegro_Voice ) return Boolean;

    function Al_Get_Voice_Frequency( voice : A_Allegro_Voice ) return unsigned;
    pragma Import( C, Al_Get_Voice_Frequency, "al_get_voice_frequency" );

    function Al_Get_Voice_Depth( voice : A_Allegro_Voice ) return Allegro_Audio_Depth;
    pragma Import( C, Al_Get_Voice_Depth, "al_get_voice_depth" );

    function Al_Get_Voice_Channels( voice : A_Allegro_Voice ) return Allegro_Channel_Conf;
    pragma Import( C, Al_Get_Voice_Channels, "al_get_voice_channels" );

    function Al_Get_Voice_Playing( voice : A_Allegro_Voice ) return Boolean;

    function Al_Set_Voice_Playing( voice : A_Allegro_Voice; val : Boolean ) return Boolean;

    function Al_Get_Voice_Position( voice : A_Allegro_Voice ) return unsigned;
    pragma Import( C, Al_Get_Voice_Position, "al_get_voice_position" );

    function Al_Set_Voice_Position( voice : A_Allegro_Voice; val : unsigned ) return Boolean;

    -- Sample functions

    function Al_Create_Sample( buf       : Address;
                               samples   : unsigned;
                               freq      : unsigned;
                               depth     : Allegro_Audio_Depth;
                               chan_conf : Allegro_Channel_Conf;
                               free_buf  : Boolean ) return A_Allegro_Sample;

    procedure Al_Destroy_Sample( spl : in out A_Allegro_Sample );
    pragma Postcondition( spl = null );

    function Al_Play_Sample( data     : A_Allegro_Sample;
                             gain     : Float;
                             pan      : Float;
                             speed    : Float;
                             playMode : Allegro_Playmode;
                             ret_id   : access Allegro_Sample_Id ) return Boolean;

    procedure Al_Stop_Sample( spl_id : Allegro_Sample_Id );
    pragma Import( C, Al_Stop_Sample, "al_stop_sample" );

    procedure Al_Stop_Samples;
    pragma Import( C, Al_Stop_Samples, "al_stop_samples" );

    function Al_Get_Sample_Channels( spl : A_Allegro_Sample ) return Allegro_Channel_Conf;
    pragma Import( C, Al_Get_Sample_Channels, "al_get_sample_channels" );

    function Al_Get_Sample_Depth( spl : A_Allegro_Sample ) return Allegro_Audio_Depth;
    pragma Import( C, Al_Get_Sample_Depth, "al_get_sample_depth" );

    function Al_Get_Sample_Frequency( spl : A_Allegro_Sample ) return unsigned;
    pragma Import( C, Al_Get_Sample_Frequency, "al_get_sample_depth" );

    function Al_Get_Sample_Length( spl : A_Allegro_Sample ) return unsigned;
    pragma Import( C, Al_Get_Sample_Length, "al_get_sample_length" );

    function Al_Get_Sample_Data( spl : A_Allegro_Sample ) return Address;
    pragma Import( C, Al_Get_Sample_Data, "al_get_sample_data" );

    procedure Al_Fill_Silence( buf       : Address;
                               samples   : unsigned;
                               depth     : Allegro_Audio_Depth;
                               chan_conf : Allegro_Channel_Conf );
    pragma Import( C, Al_Fill_Silence, "al_fill_silence" );

    -- Sample instance functions

    function Al_Create_Sample_Instance( data : A_Allegro_Sample ) return A_Allegro_Sample_Instance;
    pragma Import( C, Al_Create_Sample_Instance, "al_create_sample_instance" );

    procedure Al_Destroy_Sample_Instance( spl : in out A_Allegro_Sample_Instance );

    function Al_Play_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Boolean;

    function Al_Stop_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Boolean;

    function Al_Get_Sample_Instance_Channels( spl : A_Allegro_Sample_Instance ) return Allegro_Channel_Conf;
    pragma Import( C, Al_Get_Sample_Instance_Channels, "al_get_sample_instance_channels" );

    function Al_Get_Sample_Instance_Depth( spl : A_Allegro_Sample_Instance ) return Allegro_Audio_Depth;
    pragma Import( C, Al_Get_Sample_Instance_Depth, "al_get_sample_instance_depth" );

    function Al_Get_Sample_Instance_Frequency( spl : A_Allegro_Sample_Instance ) return unsigned;
    pragma Import( C, Al_Get_Sample_Instance_Frequency, "al_get_sample_instance_frequency" );

    function Al_Get_Sample_Instance_Length( spl : A_Allegro_Sample_Instance ) return unsigned;
    pragma Import( C, Al_Get_Sample_Instance_Length, "al_get_sample_instance_length" );

    function Al_Set_Sample_Instance_Channel_Matrix( spl : A_Allegro_Sample_Instance; matrix : Float_Array ) return Boolean;

    function Al_Set_Sample_Instance_Length( spl : A_Allegro_Sample_Instance;
                                            val : unsigned ) return Boolean;

    function Al_Get_Sample_Instance_Position( spl : A_Allegro_Sample_Instance ) return unsigned;
    pragma Import( C, Al_Get_Sample_Instance_Position, "al_get_sample_instance_position" );

    function Al_Set_Sample_Instance_Position( spl : A_Allegro_Sample_Instance; val : unsigned ) return Boolean;

    function Al_Get_Sample_Instance_Speed( spl : A_Allegro_Sample_Instance ) return Float;
    pragma Import( C, Al_Get_Sample_Instance_Speed, "al_get_sample_instance_speed" );

    function Al_Set_Sample_Instance_Speed( spl : A_Allegro_Sample_Instance; val : Float ) return Boolean;

    function Al_Get_Sample_Instance_Gain( spl : A_Allegro_Sample_Instance ) return Float;
    pragma Import( C, Al_Get_Sample_Instance_Gain, "al_get_sample_instance_gain" );

    function Al_Set_Sample_Instance_Gain( spl : A_Allegro_Sample_Instance; val : Float ) return Boolean;

    function Al_Get_Sample_Instance_Pan( spl : A_Allegro_Sample_Instance ) return Float;
    pragma Import( C, Al_Get_Sample_Instance_Pan, "al_get_sample_instance_pan" );

    function Al_Set_Sample_Instance_Pan( spl : A_Allegro_Sample_Instance; val : Float ) return Boolean;

    function Al_Get_Sample_Instance_Time( spl : A_Allegro_Sample_Instance ) return Float;
    pragma Import( C, Al_Get_Sample_Instance_Time, "al_get_sample_instance_time" );

    function Al_Get_Sample_Instance_Playmode( spl : A_Allegro_Sample_Instance ) return Allegro_Playmode;
    pragma Import( C, Al_Get_Sample_Instance_Playmode, "al_get_sample_instance_playmode" );

    function Al_Set_Sample_Instance_Playmode( spl : A_Allegro_Sample_Instance;
                                              val : Allegro_Playmode ) return Boolean;

    function Al_Get_Sample_Instance_Playing( spl : A_Allegro_Sample_Instance ) return Boolean;

    function Al_Set_Sample_Instance_Playing( spl : A_Allegro_Sample_Instance;
                                             val : Boolean ) return Boolean;

    procedure Al_Set_Sample_Instance_Playing( spl : A_Allegro_Sample_Instance;
                                              val : Boolean );

    function Al_Get_Sample_Instance_Attached( spl : A_Allegro_Sample_Instance ) return Boolean;

    function Al_Detach_Sample_Instance( spl : A_Allegro_Sample_Instance ) return Boolean;

    procedure Al_Detach_Sample_Instance( spl : A_Allegro_Sample_Instance );

    function Al_Get_Sample( spl : A_Allegro_Sample_Instance ) return A_Allegro_Sample;
    pragma Import( C, Al_Get_Sample, "al_get_sample" );

    function Al_Set_Sample( spl  : A_Allegro_Sample_Instance;
                            data : A_Allegro_Sample ) return Boolean;

    function Al_Lock_Sample_Id( spl_id : access Allegro_Sample_Id ) return A_Allegro_Sample_Instance;
    pragma Import( C, Al_Lock_Sample_Id, "al_lock_sample_id" );

    procedure Al_Unlock_Sample_Id( spl_id : access Allegro_Sample_Id );
    pragma Import( C, Al_Unlock_Sample_Id, "al_unlock_sample_id" );

    -- Mixer functions

    function Al_Create_Mixer( freq      : unsigned;
                              depth     : Allegro_Audio_Depth;
                              chan_conf : Allegro_Channel_Conf ) return A_Allegro_Mixer;
    pragma Import( C, Al_Create_Mixer, "al_create_mixer" );

    procedure Al_Destroy_Mixer( mixer : in out A_Allegro_Mixer );

    function Al_Get_Default_Mixer return A_Allegro_Mixer;
    pragma Import( C, Al_Get_Default_Mixer, "al_get_default_mixer" );

    function Al_Set_Default_Mixer( mixer : A_Allegro_Mixer ) return Boolean;

    procedure Al_Set_Default_Mixer( mixer : A_Allegro_Mixer );

    function Al_Restore_Default_Mixer return Boolean;

    function Al_Attach_Mixer_To_Mixer( stream : A_Allegro_Mixer;
                                       mixer  : A_Allegro_Mixer ) return Boolean;

    function Al_Attach_Sample_Instance_To_Mixer( stream : A_Allegro_Sample_Instance;
                                                 mixer  : A_Allegro_Mixer ) return Boolean;

    function Al_Attach_Audio_Stream_To_Mixer( stream : A_Allegro_Audio_Stream;
                                              mixer  : A_Allegro_Mixer ) return Boolean;

    function Al_Get_Mixer_Frequency( mixer : A_Allegro_Mixer ) return unsigned;
    pragma Import( C, Al_Get_Mixer_Frequency, "al_get_mixer_frequency" );

    function Al_Set_Mixer_Frequency( mixer : A_Allegro_Mixer;
                                     val   : unsigned ) return Boolean;

    function Al_Get_Mixer_Channels( mixer : A_Allegro_Mixer ) return Allegro_Channel_Conf;
    pragma Import( C, Al_Get_Mixer_Channels, "al_get_mixer_channels" );

    function Al_Get_Mixer_Depth( mixer : A_Allegro_Mixer ) return Allegro_Audio_Depth;
    pragma Import( C, Al_Get_Mixer_Depth, "al_get_mixer_depth" );

    function Al_Get_Mixer_Quality( mixer : A_Allegro_Mixer ) return Allegro_Mixer_Quality;
    pragma Import( C, Al_Get_Mixer_Quality, "al_get_mixer_quality" );

    function Al_Set_Mixer_Quality( mixer : A_Allegro_Mixer;
                                   val   : Allegro_Mixer_Quality ) return Boolean;

    function Al_Get_Mixer_Gain( mixer : A_Allegro_Mixer ) return Float;
    pragma Import( C, Al_Get_Mixer_Gain, "al_get_mixer_gain" );

    function Al_Set_Mixer_Gain( mixer : A_Allegro_Mixer;
                                val   : Float ) return Boolean;

    function Al_Get_Mixer_Playing( mixer : A_Allegro_Mixer ) return Boolean;

    function Al_Set_Mixer_Playing( mixer : A_Allegro_Mixer; val : Boolean ) return Boolean;

    function Al_Get_Mixer_Attached( mixer : A_Allegro_Mixer ) return Boolean;

    function Al_Detach_Mixer( mixer : A_Allegro_Mixer ) return Boolean;

    procedure Al_Detach_Mixer( mixer : A_Allegro_Mixer );

    type A_Mixer_Postprocess_Callback is access
         procedure( buf : Address; samples : unsigned; data : Address );
    pragma Convention( C, A_Mixer_Postprocess_Callback );

    function Al_Set_Mixer_Postprocess_Callback( mixer : A_Allegro_Mixer;
                                                cb    : A_Mixer_Postprocess_Callback;
                                                data  : Address ) return Boolean;

    -- Stream functions

    function Al_Create_Audio_Stream( buffer_count : size_t;
                                     samples      : unsigned;
                                     freq         : unsigned;
                                     depth        : Allegro_Audio_Depth;
                                     chan_conf    : Allegro_Channel_Conf
                                   ) return A_Allegro_Audio_Stream;
    pragma Import( C, Al_Create_Audio_Stream, "al_create_audio_stream" );

    procedure Al_Destroy_Audio_Stream( stream : in out A_Allegro_Audio_Stream );

    function Al_Get_Audio_Stream_Event_Source( stream : A_Allegro_Audio_Stream ) return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Audio_Stream_Event_Source, "al_get_audio_stream_event_source" );

    procedure Al_Drain_Audio_Stream( stream : A_Allegro_Audio_Stream );
    pragma Import( C, Al_Drain_Audio_Stream, "al_drain_audio_stream" );

    function Al_Rewind_Audio_Stream( stream : A_Allegro_Audio_Stream ) return Boolean;

    function Al_Get_Audio_Stream_Frequency( stream : A_Allegro_Audio_Stream ) return unsigned;
    pragma Import( C, Al_Get_Audio_Stream_Frequency, "al_get_audio_stream_frequency" );

    function Al_Get_Audio_Stream_Channels( stream : A_Allegro_Audio_Stream ) return Allegro_Channel_Conf;
    pragma Import( C, Al_Get_Audio_Stream_Channels, "al_get_audio_stream_channels" );

    function Al_Get_Audio_Stream_Depth( stream : A_Allegro_Audio_Stream ) return Allegro_Audio_Depth;
    pragma Import( C, Al_Get_Audio_Stream_Depth, "al_get_audio_stream_depth" );

    function Al_Get_Audio_Stream_Length( stream : A_Allegro_Audio_Stream ) return unsigned;
    pragma Import( C, Al_Get_Audio_Stream_Length, "al_get_audio_stream_length" );

    function Al_Get_Audio_Stream_Speed( stream : A_Allegro_Audio_Stream ) return Float;
    pragma Import( C, Al_Get_Audio_Stream_Speed, "al_get_audio_stream_speed" );

    function Al_Set_Audio_Stream_Speed( stream : A_Allegro_Audio_Stream;
                                        val    : Float ) return Boolean;

    function Al_Get_Audio_Stream_Gain( stream : A_Allegro_Audio_Stream ) return Float;
    pragma Import( C, Al_Get_Audio_Stream_Gain, "al_get_audio_stream_gain" );

    function Al_Set_Audio_Stream_Gain( stream : A_Allegro_Audio_Stream;
                                       val    : Float ) return Boolean;

    function Al_Get_Audio_Stream_Pan( stream : A_Allegro_Audio_Stream ) return Float;
    pragma Import( C, Al_Get_Audio_Stream_Pan, "al_get_audio_stream_pan" );

    function Al_Set_Audio_Stream_Pan( stream : A_Allegro_Audio_Stream;
                                      val    : Float ) return Boolean;

    function Al_Get_Audio_Stream_Playing( stream : A_Allegro_Audio_Stream ) return Boolean;

    function Al_Set_Audio_Stream_Playing( stream : A_Allegro_Audio_Stream;
                                          val    : Boolean ) return Boolean;

    procedure Al_Set_Audio_Stream_Playing( stream : A_Allegro_Audio_Stream;
                                           val    : Boolean );

    function Al_Get_Audio_Stream_Playmode( stream : A_Allegro_Audio_Stream ) return Allegro_Playmode;
    pragma Import( C, Al_Get_Audio_Stream_Playmode, "al_get_audio_stream_playmode" );

    function Al_Set_Audio_Stream_Playmode( stream : A_Allegro_Audio_Stream;
                                           val    : Allegro_Playmode ) return Boolean;

    procedure Al_Set_Audio_Stream_Playmode( stream : A_Allegro_Audio_Stream;
                                            val    : Allegro_Playmode );

    function Al_Get_Audio_Stream_Attached( stream : A_Allegro_Audio_Stream ) return Boolean;

    function Al_Get_Audio_Stream_Played_Samples( stream : A_Allegro_Audio_Stream ) return Unsigned_64;
    pragma Import( C, Al_Get_Audio_Stream_Played_Samples, "al_get_audio_stream_played_samples" );

    function Al_Detach_Audio_Stream( stream : A_Allegro_Audio_Stream ) return Boolean;

    procedure Al_Detach_Audio_Stream( stream : A_Allegro_Audio_Stream );

    function Al_Get_Audio_Stream_Fragment( stream : A_Allegro_Audio_Stream ) return Address;
    pragma Import( C, Al_Get_Audio_Stream_Fragment, "al_get_audio_stream_fragment" );

    function Al_Set_Audio_Stream_Fragment( stream : A_Allegro_Audio_Stream;
                                           val    : Address ) return Boolean;

    function Al_Get_Audio_Stream_Fragments( stream : A_Allegro_Audio_Stream ) return unsigned;
    pragma Import( C, Al_Get_Audio_Stream_Fragments, "al_get_audio_stream_fragments" );

    function Al_Get_Available_Audio_Stream_Fragments( stream : A_Allegro_Audio_Stream ) return unsigned;
    pragma Import( C, Al_Get_Available_Audio_Stream_Fragments, "al_get_available_audio_stream_fragments" );

    function Al_Set_Audio_Stream_Channel_Matrix( stream : A_Allegro_Audio_Stream; matrix : Float_Array ) return Boolean;

    function Al_Seek_Audio_Stream_Secs( stream : A_Allegro_Audio_Stream;
                                        time   : double ) return Boolean;

    function Al_Get_Audio_Stream_Position_Secs( stream : A_Allegro_Audio_Stream ) return double;
    pragma Import( C, Al_Get_Audio_Stream_Position_Secs, "al_get_audio_stream_position_secs" );

    function Al_Get_Audio_Stream_Length_Secs( stream : A_Allegro_Audio_Stream ) return double;
    pragma Import( C, Al_Get_Audio_Stream_Length_Secs, "al_get_audio_stream_length_secs" );

    function Al_Set_Audio_Stream_Loop_Secs( stream : A_Allegro_Audio_Stream;
                                            start  : double;
                                            finish : double ) return Boolean;

    -- Audio file I/O

    type A_Allegro_Sample_Loader is access
        function ( filename : C.char_array ) return A_Allegro_Sample;
    pragma Convention( C, A_Allegro_Sample_Loader );

    function Al_Register_Sample_Loader( ext    : String;
                                        loader : A_Allegro_Sample_Loader ) return Boolean;

    type A_Allegro_Sample_Loader_f is access
        function ( fp : A_Allegro_File ) return A_Allegro_Sample;
    pragma Convention( C, A_Allegro_Sample_Loader_f );

    function Al_Register_Sample_Loader_f( ext    : String;
                                          loader : A_Allegro_Sample_Loader_f ) return Boolean;

    type A_Allegro_Sample_Saver is access
        function ( filename : C.char_array; spl : A_Allegro_Sample ) return Bool;
    pragma Convention( C, A_Allegro_Sample_Saver );

    function Al_Register_Sample_Saver( ext   : String;
                                       saver : A_Allegro_Sample_Saver ) return Boolean;

    type A_Allegro_Sample_Saver_f is access
        function ( fp : A_Allegro_File; spl : A_Allegro_Sample ) return Bool;
    pragma Convention( C, A_Allegro_Sample_Saver_f );

    function Al_Register_Sample_Saver_f( ext   : String;
                                         saver : A_Allegro_Sample_Saver_f ) return Boolean;

    type A_Allegro_Audio_Stream_Loader is access
        function ( filename     : C.char_array;
                   buffer_count : size_t;
                   samples      : unsigned ) return A_Allegro_Audio_Stream;
    pragma Convention( C, A_Allegro_Audio_Stream_Loader );

    function Al_Register_Audio_Stream_Loader( ext           : String;
                                              stream_loader : A_Allegro_Audio_Stream_Loader ) return Boolean;

    type A_Allegro_Audio_Stream_Loader_f is access
        function ( fp           : A_Allegro_File;
                   buffer_count : size_t;
                   samples      : unsigned ) return A_Allegro_Audio_Stream;
    pragma Convention( C, A_Allegro_Audio_Stream_Loader_f );

    function Al_Register_Audio_Stream_Loader_f( ext           : String;
                                                stream_loader : A_Allegro_Audio_Stream_Loader_f ) return Boolean;

    function Al_Load_Sample( filename : String ) return A_Allegro_Sample;

    -- does NOT consume 'fp'
    function Al_Load_Sample_f( fp : A_Allegro_File; ident : String ) return A_Allegro_Sample;

    function Al_Load_Audio_Stream( filename     : String;
                                   buffer_count : size_t;
                                   samples      : unsigned ) return A_Allegro_Audio_Stream;

    -- consumes 'fp'
    function Al_Load_Audio_Stream_f( fp           : A_Allegro_File;
                                     ident        : String;
                                     buffer_count : size_t;
                                     samples      : unsigned ) return A_Allegro_Audio_Stream;

    function Al_Save_Sample( filename : String; spl : A_Allegro_Sample ) return Boolean;

    function Al_Save_Sample_f( fp    : A_Allegro_File;
                               ident : String;
                               spl   : A_Allegro_Sample ) return Boolean;

private

    type Allegro_Mixer is limited null record;
    pragma Convention( C, Allegro_Mixer );

    type Allegro_Sample is limited null record;
    pragma Convention( C, Allegro_Sample );

    type Allegro_Sample_Instance is limited null record;
    pragma Convention( C, Allegro_Sample_Instance );

    type Allegro_Audio_Stream is limited null record;
    pragma Convention( C, Allegro_Audio_Stream );

    type Allegro_Voice is limited null record;
    pragma Convention( C, Allegro_Voice );

end Allegro.Audio;
