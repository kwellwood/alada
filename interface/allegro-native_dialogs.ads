--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Displays;                  use Allegro.Displays;
with Allegro.Events;                    use Allegro.Events;
with Interfaces;                        use Interfaces;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

-- Allegro 5.2.5 - Native dialogs addon
package Allegro.Native_Dialogs is

    function Al_Init_Native_Dialog_Addon return Boolean;

    procedure Al_Shutdown_Native_Dialog_Addon;
    pragma Import( C, Al_Shutdown_Native_Dialog_Addon, "al_shutdown_native_dialog_addon" );

    function Al_Get_Allegro_Native_Dialog_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Native_Dialog_Version, "al_get_allegro_native_dialog_version" );

    -- File chooser dialog

    type Allegro_Filechooser is limited private;
    type A_Allegro_Filechooser is access all Allegro_Filechooser;

    type Filechooser_Flags is new Unsigned_32;
    ALLEGRO_FILECHOOSER_NONE            : constant Filechooser_Flags := 0;
    ALLEGRO_FILECHOOSER_FILE_MUST_EXIST : constant Filechooser_Flags := 1;
    ALLEGRO_FILECHOOSER_SAVE            : constant Filechooser_Flags := 2;
    ALLEGRO_FILECHOOSER_FOLDER          : constant Filechooser_Flags := 4;
    ALLEGRO_FILECHOOSER_PICTURES        : constant Filechooser_Flags := 8;
    ALLEGRO_FILECHOOSER_SHOW_HIDDEN     : constant Filechooser_Flags := 16;
    ALLEGRO_FILECHOOSER_MULTIPLE        : constant Filechooser_Flags := 32;

    function Al_Create_Native_File_Dialog( initial_path : String;
                                           title        : String;
                                           patterns     : String;
                                           mode         : Filechooser_Flags
                                         ) return A_Allegro_Filechooser;

    function Al_Show_Native_File_Dialog( display : A_Allegro_Display;
                                         dialog  : A_Allegro_Filechooser ) return Boolean;

    function Al_Get_Native_File_Dialog_Count( dialog : A_Allegro_Filechooser ) return Integer;
    pragma Import( C, Al_Get_Native_File_Dialog_Count, "al_get_native_file_dialog_count" );

    function Al_Get_Native_File_Dialog_Path( dialog : A_Allegro_Filechooser;
                                             index  : Natural ) return String;

    procedure Al_Destroy_Native_File_Dialog( dialog : in out A_Allegro_Filechooser );

    -- Message box

    type Messagebox_Flags is new Unsigned_32;
    ALLEGRO_MESSAGEBOX_WARN      : constant Messagebox_Flags := 1;
    ALLEGRO_MESSAGEBOX_ERROR     : constant Messagebox_Flags := 2;
    ALLEGRO_MESSAGEBOX_OK_CANCEL : constant Messagebox_Flags := 4;
    ALLEGRO_MESSAGEBOX_YES_NO    : constant Messagebox_Flags := 8;
    ALLEGRO_MESSAGEBOX_QUESTION  : constant Messagebox_Flags := 16;

    function Al_Show_Native_Message_Box( display : A_Allegro_Display;
                                         title   : String;
                                         heading : String;
                                         text    : String;
                                         buttons : String;
                                         flags   : Messagebox_Flags ) return Integer;

    procedure Al_Show_Native_Message_Box( display : A_Allegro_Display;
                                          title   : String;
                                          heading : String;
                                          text    : String;
                                          buttons : String;
                                          flags   : Messagebox_Flags );

    -- Text log dialog

    type Allegro_Textlog is limited private;
    type A_Allegro_Textlog is access all Allegro_Textlog;

    type Textlog_Flags is new Unsigned_32;
    ALLEGRO_TEXTLOG_NO_CLOSE  : constant Textlog_Flags := 1;
    ALLEGRO_TEXTLOG_MONOSPACE : constant Textlog_Flags := 2;

    function Al_Open_Native_Text_Log( title : String; flags : Textlog_Flags ) return A_Allegro_Textlog;

    procedure Al_Close_Native_Text_Log( textlog : in out A_Allegro_Textlog );

    procedure Al_Append_Native_Text_Log( textlog : A_Allegro_Textlog; str : String );

    function Al_Get_Native_Text_Log_Event_Source( textlog : A_Allegro_Textlog ) return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Native_Text_Log_Event_Source, "al_get_native_text_log_event_source" );

    -- Menus

    type Menu_Item_Flags is new Unsigned_32;
    ALLEGRO_MENU_ITEM_ENABLED  : constant Menu_Item_Flags := 0;
    ALLEGRO_MENU_ITEM_CHECKBOX : constant Menu_Item_Flags := 1;
    ALLEGRO_MENU_ITEM_CHECKED  : constant Menu_Item_Flags := 2;
    ALLEGRO_MENU_ITEM_DISABLED : constant Menu_Item_Flags := 4;

    type Allegro_Menu_Info is
        record
            caption : C.Strings.chars_ptr := Null_Ptr;
            id      : Unsigned_16 := 0;
            flags   : Menu_Item_Flags := 0;
            icon    : A_Allegro_Bitmap := null;
        end record;
    pragma Convention( C, Allegro_Menu_Info );
    type A_Allegro_Menu_Info is access all Allegro_Menu_Info;

    type Allegro_Menu_Info_Array is array (Integer range <>) of aliased Allegro_Menu_Info;
    pragma Convention( C, Allegro_Menu_Info_Array );

    function Allegro_Start_Of_Menu( caption : String; id : Unsigned_16 ) return Allegro_Menu_Info
    is ((New_String( caption & "->" ), id, 0, null));

    function Allegro_Menu_Item( caption : String;
                                id      : Unsigned_16;
                                flags   : Menu_Item_Flags := 0;
                                icon    : A_Allegro_Bitmap := null ) return Allegro_Menu_Info
    is ((New_String( caption ), id, flags, icon ));

    ALLEGRO_MENU_SEPARATOR : constant Allegro_Menu_Info := (Null_Ptr, 16#FFFF#, 0, null);
    ALLEGRO_END_OF_MENU    : constant Allegro_Menu_Info := (Null_Ptr, 16#0000#, 0, null);

    type Allegro_Menu is limited private;
    type A_Allegro_Menu is access all Allegro_Menu;
    pragma Convention( C, A_Allegro_Menu );
    pragma No_Strict_Aliasing( A_Allegro_Menu );

    -- Creating / Modifying Menus

    function Al_Create_Menu return A_Allegro_Menu;
    pragma Import( C, Al_Create_Menu, "al_create_menu" );

    function Al_Create_Popup_Menu return A_Allegro_Menu;
    pragma Import( C, Al_Create_Popup_Menu, "al_create_popup_menu" );

    function Al_Build_Menu( info : Allegro_Menu_Info_Array ) return A_Allegro_Menu;

    function Al_Append_Menu_Item( parent  : A_Allegro_Menu;
                                  title   : String;
                                  id      : Unsigned_16;
                                  flags   : Menu_Item_Flags;
                                  icon    : A_Allegro_Bitmap;
                                  submenu : A_Allegro_Menu ) return Integer;

    procedure Al_Append_Menu_Item( parent  : A_Allegro_Menu;
                                   title   : String;
                                   id      : Unsigned_16;
                                   flags   : Menu_Item_Flags;
                                   icon    : A_Allegro_Bitmap;
                                   submenu : A_Allegro_Menu );

    function Al_Insert_Menu_Item( parent  : A_Allegro_Menu;
                                  pos     : Integer;
                                  title   : String;
                                  id      : Unsigned_16;
                                  flags   : Menu_Item_Flags;
                                  icon    : A_Allegro_Bitmap;
                                  submenu : A_Allegro_Menu ) return Integer;

    procedure Al_Insert_Menu_Item( parent  : A_Allegro_Menu;
                                   pos     : Integer;
                                   title   : String;
                                   id      : Unsigned_16;
                                   flags   : Menu_Item_Flags;
                                   icon    : A_Allegro_Bitmap;
                                   submenu : A_Allegro_Menu );

    function Al_Remove_Menu_Item( menu : A_Allegro_Menu; pos : Integer ) return Boolean;

    procedure Al_Remove_Menu_Item( menu : A_Allegro_Menu; pos : Integer );

    function Al_Clone_Menu( menu : A_Allegro_Menu ) return A_Allegro_Menu;
    pragma Import( C, Al_Clone_Menu, "al_clone_menu" );

    function Al_Clone_Menu_For_Popup( menu : A_Allegro_Menu ) return A_Allegro_Menu;
    pragma Import( C, Al_Clone_Menu_For_Popup, "al_clone_menu_for_popup" );

    procedure Al_Destroy_Menu( menu : in out A_Allegro_Menu );

    -- Menu Properties

    function Al_Get_Menu_Item_Caption( menu : A_Allegro_Menu; pos : Integer ) return String;

    procedure Al_Set_Menu_Item_Caption( menu    : A_Allegro_Menu;
                                        pos     : Integer;
                                        caption : String );

    function Al_Get_Menu_Item_Flags( menu : A_Allegro_Menu; pos : Integer ) return Menu_Item_Flags;
    pragma Import( C, Al_Get_Menu_Item_Flags, "al_get_menu_item_flags" );

    procedure Al_Set_Menu_Item_Flags( menu  : A_Allegro_Menu;
                                      pos   : Integer;
                                      flags : Menu_Item_Flags );
    pragma Import( C, Al_Set_Menu_Item_Flags, "al_set_menu_item_flags" );

    function Al_Get_Menu_Item_Icon( menu : A_Allegro_Menu; pos : Integer ) return A_Allegro_Bitmap;
    pragma Import( C, Al_Get_Menu_Item_Icon, "al_get_menu_item_icon" );

    procedure Al_Set_Menu_Item_Icon( menu : A_Allegro_Menu;
                                     pos  : Integer;
                                     icon : A_Allegro_Bitmap );
    pragma Import( C, Al_Set_Menu_Item_Icon, "al_set_menu_item_icon" );

    -- Querying Menus

    function Al_Find_Menu( haystack : A_Allegro_Menu; id : Unsigned_16 ) return A_Allegro_Menu;
    pragma Import( C, Al_Find_Menu, "al_find_menu" );

    function Al_Find_Menu_Item( haystack : A_Allegro_Menu;
                                id       : Unsigned_16;
                                menu     : access A_Allegro_Menu;
                                index    : access Integer ) return Boolean;

    -- Menu Events

    function Al_Get_Default_Menu_Event_Source return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Default_Menu_Event_Source, "al_get_default_menu_event_source" );

    function Al_Enable_Menu_Event_Source( menu : A_Allegro_Menu ) return A_Allegro_Event_Source;
    pragma Import( C, Al_Enable_Menu_Event_Source, "al_enable_menu_event_source" );

    procedure Al_Disable_Menu_Event_Source( menu : A_Allegro_Menu );
    pragma Import( C, Al_Disable_Menu_Event_Source, "al_disable_menu_event_source" );

    -- Displaying Menus

    function Al_Get_Display_Menu( display : A_Allegro_Display ) return A_Allegro_Menu;
    pragma Import( C, Al_Get_Display_Menu, "al_get_display_menu" );

    function Al_Set_Display_Menu( display : A_Allegro_Display; menu : A_Allegro_Menu ) return Boolean;

    procedure Al_Set_Display_Menu( display : A_Allegro_Display; menu : A_Allegro_Menu );

    function Al_Popup_Menu( popup : A_Allegro_Menu; display : A_Allegro_Display ) return Boolean;

    procedure Al_Popup_Menu( popup : A_Allegro_Menu; display : A_Allegro_Display );

    function Al_Remove_Display_Menu( display : A_Allegro_Display ) return A_Allegro_Menu;
    pragma Import( C, Al_Remove_Display_Menu, "al_remove_display_menu" );

    procedure Al_Remove_Display_Menu( display : A_Allegro_Display );

    -- Native dialog events

    ALLEGRO_EVENT_NATIVE_DIALOG_CLOSE : constant Allegro_Event_Type := 600;
    ALLEGRO_EVENT_MENU_CLICK          : constant Allegro_Event_Type := 601;

private

    type Allegro_Filechooser is limited null record;
    pragma Convention( C, Allegro_Filechooser );

    type Allegro_Textlog is limited null record;
    pragma Convention( C, Allegro_Textlog );

    type Allegro_Menu is limited null record;
    pragma Convention( C, Allegro_Menu );

end Allegro.Native_Dialogs;
