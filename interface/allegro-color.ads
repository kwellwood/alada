--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Color routines
package Allegro.Color is

    type Allegro_Color is private;

    -- Pixel mapping

    function Al_Map_RGB( r, g, b : Unsigned_8 ) return Allegro_Color;
    pragma Import( C, Al_Map_RGB, "al_map_rgb" );

    function Al_Map_RGB_f( r, g, b : Float ) return Allegro_Color;
    pragma Import( C, Al_Map_RGB_f, "al_map_rgb_f" );

    function Al_Map_RGBA( r, g, b, a : Unsigned_8 ) return Allegro_Color;
    pragma Import( C, Al_Map_RGBA, "al_map_rgba" );

    function Al_Map_RGBA_f( r, g, b, a : Float ) return Allegro_Color;
    pragma Import( C, Al_Map_RGBA_f, "al_map_rgba_f" );

    function Al_Premul_RGBA( r, g, b, a : Unsigned_8 ) return Allegro_Color;
    pragma Import( C, Al_Premul_RGBA, "al_premul_rgba" );

    function Al_Premul_RGBA_f( r, g, b, a : Float ) return Allegro_Color;
    pragma Import( C, Al_Premul_RGBA_f, "al_premul_rgba_f" );

    -- Pixel unmapping

    procedure Al_Unmap_RGB( color : Allegro_Color; r, g, b : out Unsigned_8 );
    pragma Import( C, Al_Unmap_RGB, "al_unmap_rgb" );

    procedure Al_Unmap_RGB_f( color : Allegro_Color; r, g, b : out Float );
    pragma Import( C, Al_Unmap_RGB_f, "al_unmap_rgb_f" );

    procedure Al_Unmap_RGBA( color : Allegro_Color; r, g, b, a : out Unsigned_8 );
    pragma Import( C, Al_Unmap_RGBA, "al_unmap_rgba" );

    procedure Al_Unmap_RGBA_f( color : Allegro_Color; r, g, b, a : out Float );
    pragma Import( C, Al_Unmap_RGBA_f, "al_unmap_rgba_f" );

    -- Pixel formats

    type Allegro_Pixel_Format is private;
    ALLEGRO_PIXEL_FORMAT_ANY                  : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ANY_NO_ALPHA         : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ANY_WITH_ALPHA       : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ANY_15_NO_ALPHA      : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ANY_16_NO_ALPHA      : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ANY_16_WITH_ALPHA    : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ANY_24_NO_ALPHA      : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ANY_32_NO_ALPHA      : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA    : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ARGB_8888            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_RGBA_8888            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ARGB_4444            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_RGB_888              : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_RGB_565              : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_RGB_555              : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_RGBA_5551            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ARGB_1555            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ABGR_8888            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_XBGR_8888            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_BGR_888              : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_BGR_565              : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_BGR_555              : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_RGBX_8888            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_XRGB_8888            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ABGR_F32             : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE         : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_RGBA_4444            : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_SINGLE_CHANNEL_8     : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT1 : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT3 : constant Allegro_Pixel_Format;
    ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT5 : constant Allegro_Pixel_Format;

    function Image( apf : Allegro_Pixel_Format ) return String;

    function Al_Get_Pixel_Size( format : Allegro_Pixel_Format ) return Integer;
    pragma Import( C, Al_Get_Pixel_Size, "al_get_pixel_size" );

    function Al_Get_Pixel_Format_Bits( format : Allegro_Pixel_Format ) return Integer;
    pragma Import( C, Al_Get_Pixel_Format_Bits, "al_get_pixel_format_bits" );

    function Al_Get_Pixel_Block_Size( format : Allegro_Pixel_Format ) return Integer;
    pragma Import( C, Al_Get_Pixel_Block_Size, "al_get_pixel_block_size" );

    function Al_Get_Pixel_Block_Width( format : Allegro_Pixel_Format ) return Integer;
    pragma Import( C, Al_Get_Pixel_Block_Width, "al_get_pixel_block_width" );

    function Al_Get_Pixel_Block_Height( format : Allegro_Pixel_Format ) return Integer;
    pragma Import( C, Al_Get_Pixel_Block_Height, "al_get_pixel_block_height" );

private

    type Allegro_Color is
        record
            r, g, b, a : Float := 0.0;
        end record;
    pragma Convention( C_Pass_By_Copy, Allegro_Color );

    type Allegro_Pixel_Format is new Integer;
    ALLEGRO_PIXEL_FORMAT_ANY                  : constant Allegro_Pixel_Format := 0;
    ALLEGRO_PIXEL_FORMAT_ANY_NO_ALPHA         : constant Allegro_Pixel_Format := 1;
    ALLEGRO_PIXEL_FORMAT_ANY_WITH_ALPHA       : constant Allegro_Pixel_Format := 2;
    ALLEGRO_PIXEL_FORMAT_ANY_15_NO_ALPHA      : constant Allegro_Pixel_Format := 3;
    ALLEGRO_PIXEL_FORMAT_ANY_16_NO_ALPHA      : constant Allegro_Pixel_Format := 4;
    ALLEGRO_PIXEL_FORMAT_ANY_16_WITH_ALPHA    : constant Allegro_Pixel_Format := 5;
    ALLEGRO_PIXEL_FORMAT_ANY_24_NO_ALPHA      : constant Allegro_Pixel_Format := 6;
    ALLEGRO_PIXEL_FORMAT_ANY_32_NO_ALPHA      : constant Allegro_Pixel_Format := 7;
    ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA    : constant Allegro_Pixel_Format := 8;
    ALLEGRO_PIXEL_FORMAT_ARGB_8888            : constant Allegro_Pixel_Format := 9;
    ALLEGRO_PIXEL_FORMAT_RGBA_8888            : constant Allegro_Pixel_Format := 10;
    ALLEGRO_PIXEL_FORMAT_ARGB_4444            : constant Allegro_Pixel_Format := 11;
    ALLEGRO_PIXEL_FORMAT_RGB_888              : constant Allegro_Pixel_Format := 12;
    ALLEGRO_PIXEL_FORMAT_RGB_565              : constant Allegro_Pixel_Format := 13;
    ALLEGRO_PIXEL_FORMAT_RGB_555              : constant Allegro_Pixel_Format := 14;
    ALLEGRO_PIXEL_FORMAT_RGBA_5551            : constant Allegro_Pixel_Format := 15;
    ALLEGRO_PIXEL_FORMAT_ARGB_1555            : constant Allegro_Pixel_Format := 16;
    ALLEGRO_PIXEL_FORMAT_ABGR_8888            : constant Allegro_Pixel_Format := 17;
    ALLEGRO_PIXEL_FORMAT_XBGR_8888            : constant Allegro_Pixel_Format := 18;
    ALLEGRO_PIXEL_FORMAT_BGR_888              : constant Allegro_Pixel_Format := 19;
    ALLEGRO_PIXEL_FORMAT_BGR_565              : constant Allegro_Pixel_Format := 20;
    ALLEGRO_PIXEL_FORMAT_BGR_555              : constant Allegro_Pixel_Format := 21;
    ALLEGRO_PIXEL_FORMAT_RGBX_8888            : constant Allegro_Pixel_Format := 22;
    ALLEGRO_PIXEL_FORMAT_XRGB_8888            : constant Allegro_Pixel_Format := 23;
    ALLEGRO_PIXEL_FORMAT_ABGR_F32             : constant Allegro_Pixel_Format := 24;
    ALLEGRO_PIXEL_FORMAT_ABGR_8888_LE         : constant Allegro_Pixel_Format := 25;
    ALLEGRO_PIXEL_FORMAT_RGBA_4444            : constant Allegro_Pixel_Format := 26;
    ALLEGRO_PIXEL_FORMAT_SINGLE_CHANNEL_8     : constant Allegro_Pixel_Format := 27;
    ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT1 : constant Allegro_Pixel_Format := 28;
    ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT3 : constant Allegro_Pixel_Format := 29;
    ALLEGRO_PIXEL_FORMAT_COMPRESSED_RGBA_DXT5 : constant Allegro_Pixel_Format := 30;

end Allegro.Color;
