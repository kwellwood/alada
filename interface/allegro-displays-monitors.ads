--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- Allegro 5.2.5 - Display routines - Monitors
package Allegro.Displays.Monitors is

    type Allegro_Monitor_Info is
        record
            x1 : Integer;
            y1 : Integer;
            x2 : Integer;
            y2 : Integer;
        end record;
    pragma Convention( C, Allegro_Monitor_Info );

    ALLEGRO_DEFAULT_DISPLAY_ADAPTER : constant Integer;

    function Al_Get_Num_Video_Adapters return Integer;
    pragma Import( C, Al_Get_Num_Video_Adapters, "al_get_num_video_adapters" );

    function Al_Get_Monitor_Info( adapter : Integer; info : access Allegro_Monitor_Info ) return Boolean;

    procedure Al_Get_Monitor_Info( adapter : Integer;
                                   info    : out Allegro_Monitor_Info;
                                   success : out Boolean );

    procedure Al_Get_Monitor_Info( adapter : Integer;
                                   info    : out Allegro_Monitor_Info );

    function Al_Get_Monitor_Dpi( adapter : Integer ) return Integer;
    pragma Import( C, Al_Get_Monitor_Dpi, "al_get_monitor_dpi" );

private

    ALLEGRO_DEFAULT_DISPLAY_ADAPTER : constant Integer := -1;

end Allegro.Displays.Monitors;
