--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - PhysicsFS integration
package Allegro.PhysicsFS is

    procedure Al_Set_PhysFS_File_Interface;
    pragma Import( C, Al_Set_PhysFS_File_Interface, "al_set_physfs_file_interface" );

    function Al_Get_Allegro_PhysFS_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_PhysFS_Version, "al_get_allegro_physfs_version" );

    ----------------------------------------------------------------------------

    -- PhysicsFS Bindings : physfs.h
    --------------------------------

    -- Initialize the PhysicsFS library.
    --
    -- This must be called before any other PhysicsFS function.
    --
    -- This should be called prior to any attempts to change your process's
    --  current working directory.
    --
    -- returns nonzero on success, zero on error. Specifics of the error can be
    --         gleaned from PHYSFS_GetLastError.
    --
    -- See also: PHYSFS_Deinit, PHYSFS_IsInit
    --
    function PHYSFS_Init return Boolean;

    -- Deinitialize the PhysicsFS library.
    --
    -- This closes any files opened via PhysicsFS, blanks the search/write paths,
    -- frees memory, and invalidates all of your file handles.
    --
    -- Note that this call can FAIL if there's a file open for writing that
    --  refuses to close (for example, the underlying operating system was
    --  buffering writes to network filesystem, and the fileserver has crashed,
    --  or a hard drive has failed, etc). It is usually best to close all write
    --  handles yourself before calling this function, so that you can gracefully
    --  handle a specific failure.
    --
    -- Once successfully deinitialized, PHYSFS_Init can be called again to
    --  restart the subsystem. All default API states are restored at this
    --  point, with the exception of any custom allocator you might have
    --  specified, which survives between initializations.
    --
    -- returns nonzero on success, zero on error. Specifics of the error can be
    --         gleaned from PHYSFS_GetLastError. If failure, state of PhysFS is
    --         undefined, and probably badly screwed up.
    --
    -- See also: PHYSFS_Init, PHYSFS_IsInit
    function PHYSFS_Deinit return Boolean;

    -- Determine if the PhysicsFS library is initialized.
    --
    -- Once PHYSFS_Init returns successfully, this will return True.
    --  Before a successful PHYSFS_Init and after PHYSFS_Deinit returns
    --  successfully, this will return False. This function is safe to call at
    --  any time.
    --
    -- returns True if library is initialized, False if library is not.
    --
    -- See also: PHYSFS_Init, PHYSFS_Deinit
    function PHYSFS_IsInit return Boolean;

    -- Add an archive or directory to the search path.
    --
    -- newDir : directory or archive to add to the path, in platform-dependent
    --          notation.
    --
    -- appendToPath : True to append to search path, False to prepend.
    --
    -- returns True if added to path, False on failure (bogus archive, dir
    --         missing, etc). Specifics of the error can be gleaned from
    --         PHYSFS_GetLastError.
    --
    -- This is a legacy call in PhysicsFS 2.0, equivalent to:
    --     PHYSFS_Mount( newDir, null, appendToPath );
    --
    -- You must use this and not PHYSFS_Mount if binary compatibility with
    --  PhysicsFS 1.0 is important (which it may not be for many people).
    --
    -- See also: PHYSFS_Mount, PHYSFS_RemoveFromSearchPath, PHYSFS_GetSearchPath
    function PHYSFS_AddToSearchPath( newDir       : String;
                                     appendToPath : Boolean := True ) return Boolean;

    -- Add an archive or directory to the search path.
    --
    -- If this is a duplicate, the entry is not added again, even though the
    -- function succeeds. You may not add the same archive to two different
    -- mountpoints: duplicate checking is done against the archive and not the
    -- mountpoint.
    --
    -- When you mount an archive, it is added to a virtual file system... all
    -- files in all of the archives are interpolated into a single hierachical
    -- file tree. Two archives mounted at the same place (or an archive with
    -- files overlapping another mountpoint) may have overlapping files: in such
    -- a case, the file earliest in the search path is selected, and the other
    -- files are inaccessible to the application. This allows archives to be
    -- used to override previous revisions; you can use the mounting mechanism
    -- to place archives at a specific point in the file tree and prevent
    -- overlap; this is useful for downloadable mods that might trample over
    -- application data or each other, for example.
    --
    -- The mountpoint does not need to exist prior to mounting, which is
    -- different than those familiar with the Unix concept of "mounting" may
    -- expect. As well, more than one archive can be mounted to the same
    -- mountpoint, or mountpoints and archive contents can overlap... the
    -- interpolation mechanism still functions as usual.
    --
    -- Specifying a symbolic link to an archive or directory is allowed here,
    -- regardless of the state of PHYSFS_permitSymbolicLinks(). That function
    -- only deals with symlinks inside the mounted directory or archive.
    --
    -- Parameters
    --   newDir	        directory or archive to add to the path, in platform-
    --                    dependent notation.
    --   mountPoint     Location in the interpolated tree that this archive will
    --                    be "mounted", in platform-independent notation. NULL
    --                    or "" is equivalent to "/".
    --   appendToPath   nonzero to append to search path, zero to prepend.
    --
    -- Returns
    --   nonzero if added to path, zero on failure (bogus archive, dir missing,
    --   etc). Use PHYSFS_getLastErrorCode() to obtain the specific error.
    --
    -- See also: PHYSFS_removeFromSearchPath, PHYSFS_getSearchPath,
    --   PHYSFS_getMountPoint, PHYSFS_mountIo
    function PHYSFS_Mount( newDir       : String;
                           mountPoint   : String;
                           appendToPath : Boolean ) return Boolean;

end Allegro.PhysicsFS;
