--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;

-- Allegro 5.2.5 - Mouse routines - Mouse cursors
package Allegro.Mouse.Cursors is

    type Allegro_Mouse_Cursor is limited private;
    type A_Allegro_Mouse_Cursor is access all Allegro_Mouse_Cursor;

    type Allegro_System_Mouse_Cursor is private;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_NONE        : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT     : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_ARROW       : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_BUSY        : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_QUESTION    : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_EDIT        : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_MOVE        : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_N    : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_W    : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_S    : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_E    : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NW   : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SW   : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SE   : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NE   : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_PROGRESS    : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_PRECISION   : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_LINK        : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_ALT_SELECT  : constant Allegro_System_Mouse_Cursor;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_UNAVAILABLE : constant Allegro_System_Mouse_Cursor;

    function Al_Create_Mouse_Cursor( sprite : A_Allegro_Bitmap;
                                     xfocus,
                                     yfocus : Integer ) return A_Allegro_Mouse_Cursor;
    pragma Import( C, Al_Create_Mouse_Cursor, "al_create_mouse_cursor" );

    procedure Al_Destroy_Mouse_Cursor( cursor : in out A_Allegro_Mouse_Cursor );

    function Al_Set_Mouse_Cursor( display : A_Allegro_Display;
                                  cursor  : A_Allegro_Mouse_Cursor ) return Boolean;

    -- Calls Al_Set_Mouse_Cursor, ignoring any errors.
    procedure Al_Set_Mouse_Cursor( display : A_Allegro_Display;
                                   cursor  : A_Allegro_Mouse_Cursor );

    function Al_Set_System_Mouse_Cursor( display   : A_Allegro_Display;
                                         cursor_id : Allegro_System_Mouse_Cursor ) return Boolean;

    -- Calls Al_Set_System_Mouse_Cursor, ignoring any errors.
    procedure Al_Set_System_Mouse_Cursor( display   : A_Allegro_Display;
                                          cursor_id : Allegro_System_Mouse_Cursor );

    function Al_Hide_Mouse_Cursor( display : A_Allegro_Display ) return Boolean;

    -- Calls Al_Hide_Mouse_Cursor, ignoring any errors.
    procedure Al_Hide_Mouse_Cursor( display : A_Allegro_Display );

    function Al_Show_Mouse_Cursor( display : A_Allegro_Display ) return Boolean;

    -- Calls Al_Show_Mouse_Cursor, ignoring any errors.
    procedure Al_Show_Mouse_Cursor( display : A_Allegro_Display );

private

    type Allegro_Mouse_Cursor is limited null record;
    pragma Convention( C, Allegro_Mouse_Cursor );

    type Allegro_System_Mouse_Cursor is new Integer;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_NONE        : constant Allegro_System_Mouse_Cursor :=  0;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT     : constant Allegro_System_Mouse_Cursor :=  1;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_ARROW       : constant Allegro_System_Mouse_Cursor :=  2;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_BUSY        : constant Allegro_System_Mouse_Cursor :=  3;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_QUESTION    : constant Allegro_System_Mouse_Cursor :=  4;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_EDIT        : constant Allegro_System_Mouse_Cursor :=  5;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_MOVE        : constant Allegro_System_Mouse_Cursor :=  6;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_N    : constant Allegro_System_Mouse_Cursor :=  7;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_W    : constant Allegro_System_Mouse_Cursor :=  8;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_S    : constant Allegro_System_Mouse_Cursor :=  9;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_E    : constant Allegro_System_Mouse_Cursor := 10;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NW   : constant Allegro_System_Mouse_Cursor := 11;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SW   : constant Allegro_System_Mouse_Cursor := 12;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_SE   : constant Allegro_System_Mouse_Cursor := 13;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_RESIZE_NE   : constant Allegro_System_Mouse_Cursor := 14;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_PROGRESS    : constant Allegro_System_Mouse_Cursor := 15;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_PRECISION   : constant Allegro_System_Mouse_Cursor := 16;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_LINK        : constant Allegro_System_Mouse_Cursor := 17;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_ALT_SELECT  : constant Allegro_System_Mouse_Cursor := 18;
    ALLEGRO_SYSTEM_MOUSE_CURSOR_UNAVAILABLE : constant Allegro_System_Mouse_Cursor := 19;

end Allegro.Mouse.Cursors;
