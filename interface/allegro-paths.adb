--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

package body Allegro.Paths is

    function To_Ada_Safe( str : C.Strings.chars_ptr ) return String is
    begin
        if str /= Null_Ptr then
            return To_Ada( Value( str ) );
        end if;
        return "";
    end To_Ada_Safe;

    ----------------------------------------------------------------------------

    function Al_Create_Path( str : String ) return A_Allegro_Path is

        function C_Al_Create_Path( str : C.char_array  ) return A_Allegro_Path;
        pragma Import( C, C_Al_Create_Path, "al_create_path" );

    begin
        return C_Al_Create_Path( To_C( str ) );
    end Al_Create_Path;

    ----------------------------------------------------------------------------

    function Al_Create_Path_For_Directory( str : String ) return A_Allegro_Path is

        function C_Al_Create_Path_For_Directory( str : C.char_array ) return A_Allegro_Path;
        pragma Import( C, C_Al_Create_Path_For_Directory, "al_create_path_for_directory" );

    begin
        return C_Al_Create_Path_For_Directory( To_C( str ) );
    end Al_Create_Path_For_Directory;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Path( path : in out A_Allegro_Path ) is

        procedure C_Al_Destroy_Path( path : A_Allegro_Path );
        pragma Import( C, C_Al_Destroy_Path, "al_destroy_path" );

    begin
        if path /= null then
            C_Al_Destroy_Path( path );
            path := null;
        end if;
    end Al_Destroy_Path;

    ----------------------------------------------------------------------------

    function Al_Join_Paths( path : A_Allegro_Path; tail : A_Allegro_Path ) return Boolean is

        function C_Al_Join_Paths( path : A_Allegro_Path; tail : A_Allegro_Path ) return Bool;
        pragma Import( C, C_Al_Join_Paths, "al_join_paths" );

    begin
        return To_Ada( C_Al_Join_Paths( path, tail ) );
    end Al_Join_Paths;

    ----------------------------------------------------------------------------

    function Al_Rebase_Path( head : A_Allegro_Path; tail : A_Allegro_Path ) return Boolean is

        function C_Al_Rebase_Path( head : A_Allegro_Path; tail : A_Allegro_Path ) return Bool;
        pragma Import( C, C_Al_Rebase_Path, "al_rebase_path" );

    begin
        return To_Ada( C_Al_Rebase_Path( head, tail ) );
    end Al_Rebase_Path;

    ----------------------------------------------------------------------------

    function Al_Get_Path_Drive( path : A_Allegro_Path ) return String is

        function C_Al_Get_Path_Drive( path : A_Allegro_Path ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Path_Drive, "al_get_path_drive" );

    begin
        return To_Ada_Safe( C_Al_Get_Path_Drive( path ) );
    end Al_Get_Path_Drive;

    ----------------------------------------------------------------------------

    function Al_Get_Path_Component( path : A_Allegro_Path; i : Integer ) return String is

        function C_Al_Get_Path_Component( path : A_Allegro_Path; i : Integer ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Path_Component, "al_get_path_component" );

    begin
        return To_Ada_Safe( C_Al_Get_Path_Component( path, i ) );
    end Al_Get_Path_Component;

    ----------------------------------------------------------------------------

    function Al_Get_Path_Tail( path : A_Allegro_Path ) return String is

        function C_Al_Get_Path_Tail( path : A_Allegro_Path ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Path_Tail, "al_get_path_tail" );

    begin
        return To_Ada_Safe( C_Al_Get_Path_Tail( path ) );
    end Al_Get_Path_Tail;

    ----------------------------------------------------------------------------

    function Al_Get_Path_Filename( path : A_Allegro_Path ) return String is

        function C_Al_Get_Path_Filename( path : A_Allegro_Path ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Path_Filename, "al_get_path_filename" );

    begin
        return To_Ada_Safe( C_Al_Get_Path_Filename( path ) );
    end Al_Get_Path_Filename;

    ----------------------------------------------------------------------------

    function Al_Get_Path_Basename( path : A_Allegro_Path ) return String is

        function C_Al_Get_Path_Basename( path : A_Allegro_Path ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Path_Basename, "al_get_path_basename" );

    begin
        return To_Ada_Safe( C_Al_Get_Path_Basename( path ) );
    end Al_Get_Path_Basename;

    ----------------------------------------------------------------------------

    function Al_Get_Path_Extension( path : A_Allegro_Path ) return String is

        function C_Al_Get_Path_Extension( path : A_Allegro_Path ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Path_Extension, "al_get_path_extension" );

    begin
        return To_Ada_Safe( C_Al_Get_Path_Extension( path ) );
    end Al_Get_Path_Extension;

    ----------------------------------------------------------------------------

    procedure Al_Set_Path_Drive( path : A_Allegro_Path; drive : String ) is

        procedure C_Al_Set_Path_Drive( path : A_Allegro_Path; drive : C.char_array );
        pragma Import( C, C_Al_Set_Path_Drive, "al_set_path_drive" );

    begin
        C_Al_Set_Path_Drive( path, To_C( drive ) );
    end Al_Set_Path_Drive;

    ----------------------------------------------------------------------------

    procedure Al_Append_Path_Component( path : A_Allegro_Path; s : String ) is

        procedure C_Al_Append_Path_Component( path : A_Allegro_Path; s : C.char_array );
        pragma Import( C, C_Al_Append_Path_Component, "al_append_path_component" );

    begin
        C_Al_Append_Path_Component( path, To_C( s ) );
    end Al_Append_Path_Component;

    ----------------------------------------------------------------------------

    procedure Al_Insert_Path_Component( path : A_Allegro_Path;
                                        i    : Integer;
                                        s    : String ) is

        procedure C_Al_Insert_Path_Component( path : A_Allegro_Path;
                                              i    : Integer;
                                              s    : C.char_array );
        pragma Import( C, C_Al_Insert_Path_Component, "al_insert_path_component" );

    begin
        C_Al_Insert_Path_Component( path, i, To_C( s ) );
    end Al_Insert_Path_Component;

    ----------------------------------------------------------------------------

    procedure Al_Replace_Path_Component( path : A_Allegro_Path;
                                         i    : Integer;
                                         s    : String ) is

        procedure C_Al_Replace_Path_Component( path : A_Allegro_Path;
                                               i    : Integer;
                                               s    : C.char_array );
        pragma Import( C, C_Al_Replace_Path_Component, "al_replace_path_component" );

    begin
        C_Al_Replace_Path_Component( path, i, To_C( s ) );
    end Al_Replace_Path_Component;

    ----------------------------------------------------------------------------

    procedure Al_Set_Path_Filename( path : A_Allegro_Path; filename : String ) is

        procedure C_Al_Set_Path_Filename( path     : A_Allegro_Path;
                                          filename : C.char_array );
        pragma Import( C, C_Al_Set_Path_Filename, "al_set_path_filename" );

    begin
        C_Al_Set_Path_Filename( path, To_C( filename ) );
    end Al_Set_Path_Filename;

    ----------------------------------------------------------------------------

    function Al_Set_Path_Extension( path : A_Allegro_Path; extension : String ) return Boolean is

        function C_Al_Set_Path_Extension( path      : A_Allegro_Path;
                                          extension : C.char_array ) return Bool;
        pragma Import( C, C_Al_Set_Path_Extension, "al_set_path_extension" );

    begin
        return To_Ada( C_Al_Set_Path_Extension( path, To_C( extension ) ) );
    end Al_Set_Path_Extension;

    ----------------------------------------------------------------------------

    function Al_Make_Path_Canonical( path : A_Allegro_Path ) return Boolean is

        function C_Al_Make_Path_Canonical( path : A_Allegro_Path ) return Bool;
        pragma Import( C, C_Al_Make_Path_Canonical, "al_make_path_canonical" );

    begin
        return To_Ada( C_Al_Make_Path_Canonical( path ) );
    end Al_Make_Path_Canonical;

    ----------------------------------------------------------------------------

    function Al_Path_CStr( path  : A_Allegro_Path;
                           delim : Character := ALLEGRO_NATIVE_PATH_SEP ) return String is

        function C_Al_Path_CStr( path  : A_Allegro_Path;
                                 delim : char ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Path_CStr, "al_path_cstr" );

    begin
        return To_Ada_Safe( C_Al_Path_CStr( path, char(delim) ) );
    end Al_Path_CStr;

    ----------------------------------------------------------------------------

    function Al_Path_Ustr( path  : A_Allegro_Path;
                           delim : Character := ALLEGRO_NATIVE_PATH_SEP ) return A_Allegro_Ustr is
        function C_Al_Path_Ustr( path : A_Allegro_Path; delim : char ) return A_Allegro_Ustr;
        pragma Import( C, C_Al_Path_Ustr, "al_path_ustr" );
    begin
        return C_Al_Path_Ustr( path, char(delim) );
    end Al_Path_Ustr;

end Allegro.Paths;
