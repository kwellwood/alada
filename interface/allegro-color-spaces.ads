--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- Allegro 5.2.5 - Color addon
package Allegro.Color.Spaces is

    function Al_Get_Allegro_Color_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Color_Version, "al_get_allegro_color_version" );

    function Al_Is_Color_Valid( color : Allegro_Color ) return Boolean;

    -- CMYK - cyan, magenta, yellow, key

    function Al_Color_CMYK( c, m, y, k : Float ) return Allegro_Color;
    pragma Import( C, Al_Color_CMYK, "al_color_cmyk" );

    procedure Al_Color_CMYK_To_RGB( cyan, magenta, yellow, key : Float;
                                    red, green, blue           : out Float );
    pragma Import( C, Al_Color_CMYK_To_RGB, "al_color_cmyk_to_rgb" );

    -- HSL - hue, saturation, lightness

    function Al_Color_HSL( h, s, l : Float ) return Allegro_Color;
    pragma Import( C, Al_Color_HSL, "al_color_hsl" );

    procedure Al_Color_HSL_To_RGB( hue, saturation, lightness : Float;
                                   red, green, blue           : out Float );
    pragma Import( C, Al_Color_HSL_To_RGB, "al_color_hsl_to_rgb" );

    -- HSV - hue, saturation, value

    function Al_Color_HSV( h, s, v : Float ) return Allegro_Color;
    pragma Import( C, Al_Color_HSV, "al_color_hsv" );

    procedure Al_Color_HSV_To_RGB( hue, saturation, value : Float;
                                   red, green, blue       : out Float );
    pragma Import( C, Al_Color_HSV_To_RGB, "al_color_hsv_to_rgb" );

    -- XYZ - CIE 1931 XYZ color space

    procedure Al_Color_RGB_To_XYZ( red, green, blue : Float;
                                   x, y, z          : out Float );
    pragma Import( C, Al_Color_RGB_To_XYZ, "al_color_rgb_to_xyz" );

    function Al_Color_XYZ( x, y, z : Float ) return Allegro_Color;
    pragma Import( C, Al_Color_XYZ, "al_color_xyz" );

    procedure Al_Color_XYZ_To_RGB( x, y, z          : Float;
                                   red, green, blue : out Float );
    pragma Import( C, Al_Color_XYZ_To_RGB, "al_color_xyz_to_rgb" );

   -- xyY color space

    procedure Al_Color_RGB_To_XYY( red, green, blue : Float;
                                   x, y, y2         : out Float );
    pragma Import( C, Al_Color_RGB_To_XYY, "al_color_rgb_to_xyy" );

    function Al_Color_XYY( x, y, y2 : Float ) return Allegro_Color;
    pragma Import( C, Al_Color_XYY, "al_color_xyy" );

    procedure Al_Color_XYY_To_RGB( x, y, y2         : Float;
                                   red, green, blue : out Float );
    pragma Import( C, Al_Color_XYY_To_RGB, "al_color_xyy_to_rgb" );

    -- L*a*b* color space

    procedure Al_Color_RGB_To_LAB( red, green, blue : Float;
                                   l, a, b          : out Float );
    pragma Import( C, Al_Color_RGB_To_LAB, "al_color_rgb_to_lab" );

    function Al_Color_LAB( l, a, b : Float ) return Allegro_Color;
    pragma Import( C, Al_Color_LAB, "al_color_lab" );

    procedure Al_Color_LAB_To_RGB( l, a, b          : Float;
                                   red, green, blue : out Float );
    pragma Import( C, Al_Color_LAB_To_RGB, "al_color_lab_to_rgb" );

    function Al_Color_Distance_CIEDE2000( color1, color2 : Allegro_Color ) return Long_Float;

    -- LCH - CIE LCH color space

    procedure Al_Color_RGB_To_LCH( red, green, blue : Float;
                                   l, c, h          : out Float );
    pragma Import( C, Al_Color_RGB_To_LCH, "al_color_rgb_to_lch" );

    function Al_Color_LCH( l, c, h : Float ) return Allegro_Color;
    pragma Import( C, Al_Color_LCH, "al_color_lch" );

    procedure Al_Color_LCH_To_RGB( l, c, h          : Float;
                                   red, green, blue : out Float );
    pragma Import( C, Al_Color_LCH_To_RGB, "al_color_lch_to_rgb" );

    -- HTML colors

    function Al_Color_HTML( str : String ) return Allegro_Color;

    function Al_Color_HTML_To_RGB( str : String; red, green, blue : access Float ) return Boolean;

    function Al_Color_RGB_To_HTML( red, green, blue : Float ) return String;

    -- Color names

    function Al_Color_Name( name : String ) return Allegro_Color;

    function Al_Color_Name_To_RGB( name : String; r, g, b : access Float ) return Boolean;

    -- RGB - red, green, blue

    procedure Al_Color_RGB_To_CMYK( red, green, blue           : Float;
                                    cyan, magenta, yellow, key : out Float );
    pragma Import( C, Al_Color_RGB_To_CMYK, "al_color_rgb_to_cmyk" );

    procedure Al_Color_RGB_To_HSL( red, green, blue           : Float;
                                   hue, saturation, lightness : out Float );
    pragma Import( C, Al_Color_RGB_To_HSL, "al_color_rgb_to_hsl" );

    procedure Al_Color_RGB_To_HSV( red, green, blue       : Float;
                                   hue, saturation, value : out Float );
    pragma Import( C, Al_Color_RGB_To_HSV, "al_color_rgb_to_hsv" );

    function Al_Color_RGB_To_Name( r, g, b : Float ) return String;

    procedure Al_Color_RGB_To_YUV( red, green, blue : Float;
                                   y, u, v          : out Float );
    pragma Import( C, Al_Color_RGB_To_YUV, "al_color_rgb_to_yuv" );

    -- Y'UV - luminance, chrominance

    function Al_Color_YUV( y, u, v : Float ) return Allegro_Color;
    pragma Import( C, Al_Color_YUV, "al_color_yuv" );

    procedure Al_Color_YUV_To_RGB( y, u, v          : Float;
                                   red, green, blue : out Float );
    pragma Import( C, Al_Color_YUV_To_RGB, "al_color_yuv_to_rgb" );

end Allegro.Color.Spaces;
