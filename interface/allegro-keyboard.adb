--
-- Copyright (c) 2013-2014 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Characters.Handling;           use Ada.Characters.Handling;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

package body Allegro.Keyboard is

    function Al_Install_Keyboard return Boolean is

        function C_Al_Install_Keyboard return Bool;
        pragma Import( C, C_Al_Install_Keyboard, "al_install_keyboard" );

    begin
        return To_Ada( C_Al_Install_Keyboard );
    end Al_Install_Keyboard;

    ----------------------------------------------------------------------------

    function Al_Is_Keyboard_Installed return Boolean is

        function C_Al_Is_Keyboard_Installed return Bool;
        pragma Import( C, C_Al_Is_Keyboard_Installed, "al_is_keyboard_installed" );

    begin
        return To_Ada( C_Al_Is_Keyboard_Installed );
    end Al_Is_Keyboard_Installed;

    ----------------------------------------------------------------------------

    function Al_Set_Keyboard_Leds( leds : Integer ) return Boolean is

        function C_Al_Set_Keyboard_Leds( leds : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Keyboard_Leds, "al_set_keyboard_leds" );

    begin
        return To_Ada( C_Al_Set_Keyboard_Leds( leds ) );
    end Al_Set_Keyboard_Leds;

    ----------------------------------------------------------------------------

    function Al_Keycode_To_Name( keycode : Integer ) return String is

        function C_Al_Keycode_To_Name( keycode : Integer ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Keycode_To_Name, "al_keycode_to_name" );

    begin
        return To_Ada( Value( C_Al_Keycode_To_Name( keycode ) ) );
    end Al_Keycode_To_Name;

    ----------------------------------------------------------------------------

    function Al_Key_Down( state   : Allegro_Keyboard_State;
                          keycode : Integer ) return Boolean is

        function C_Al_Key_Down( state   : access Allegro_Keyboard_State;
                                keycode : Integer ) return Bool;
        pragma Import( C, C_Al_Key_Down, "al_key_down" );

    begin
        return To_Ada( C_Al_Key_Down( state'Unrestricted_Access, keycode ) );
    end Al_Key_Down;

    ----------------------------------------------------------------------------

    function To_Allegro_Keycode( str : String ) return Integer is
        s : constant String := To_Upper( str );
    begin
        if s'Length = 1 then
            case s(s'First) is
                when 'A' => return ALLEGRO_KEY_A;
                when 'B' => return ALLEGRO_KEY_B;
                when 'C' => return ALLEGRO_KEY_C;
                when 'D' => return ALLEGRO_KEY_D;
                when 'E' => return ALLEGRO_KEY_E;
                when 'F' => return ALLEGRO_KEY_F;
                when 'G' => return ALLEGRO_KEY_G;
                when 'H' => return ALLEGRO_KEY_H;
                when 'I' => return ALLEGRO_KEY_I;
                when 'J' => return ALLEGRO_KEY_J;
                when 'K' => return ALLEGRO_KEY_K;
                when 'L' => return ALLEGRO_KEY_L;
                when 'M' => return ALLEGRO_KEY_M;
                when 'N' => return ALLEGRO_KEY_N;
                when 'O' => return ALLEGRO_KEY_O;
                when 'P' => return ALLEGRO_KEY_P;
                when 'Q' => return ALLEGRO_KEY_Q;
                when 'R' => return ALLEGRO_KEY_R;
                when 'S' => return ALLEGRO_KEY_S;
                when 'T' => return ALLEGRO_KEY_T;
                when 'U' => return ALLEGRO_KEY_U;
                when 'V' => return ALLEGRO_KEY_V;
                when 'W' => return ALLEGRO_KEY_W;
                when 'X' => return ALLEGRO_KEY_X;
                when 'Y' => return ALLEGRO_KEY_Y;
                when 'Z' => return ALLEGRO_KEY_Z;
                when '0' => return ALLEGRO_KEY_0;
                when '1' => return ALLEGRO_KEY_1;
                when '2' => return ALLEGRO_KEY_2;
                when '3' => return ALLEGRO_KEY_3;
                when '4' => return ALLEGRO_KEY_4;
                when '5' => return ALLEGRO_KEY_5;
                when '6' => return ALLEGRO_KEY_6;
                when '7' => return ALLEGRO_KEY_7;
                when '8' => return ALLEGRO_KEY_8;
                when '9' => return ALLEGRO_KEY_9;
                when '~' => return ALLEGRO_KEY_TILDE;
                when '`' => return ALLEGRO_KEY_TILDE;
                when '-' => return ALLEGRO_KEY_MINUS;
                when '=' => return ALLEGRO_KEY_EQUALS;
                when '[' => return ALLEGRO_KEY_OPENBRACE;
                when ']' => return ALLEGRO_KEY_CLOSEBRACE;
                when ';' => return ALLEGRO_KEY_SEMICOLON;
                when ''' => return ALLEGRO_KEY_QUOTE;
                when '\' => return ALLEGRO_KEY_BACKSLASH;
                when ',' => return ALLEGRO_KEY_COMMA;
                when '.' => return ALLEGRO_KEY_FULLSTOP;
                when '/' => return ALLEGRO_KEY_SLASH;
                when ' ' => return ALLEGRO_KEY_SPACE;
                when others => return 0;
            end case;
        elsif s = "ESC" then
            return ALLEGRO_KEY_ESCAPE;
        elsif s = "ENTER" then
            return ALLEGRO_KEY_ENTER;
        end if;
        return 0;
    end To_Allegro_Keycode;

end Allegro.Keyboard;
