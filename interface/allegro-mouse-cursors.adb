--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Mouse.Cursors is

    procedure Al_Destroy_Mouse_Cursor( cursor : in out A_Allegro_Mouse_Cursor ) is

        procedure C_Al_Destroy_Mouse_Cursor( cursor : A_Allegro_Mouse_Cursor );
        pragma Import( C, C_Al_Destroy_Mouse_Cursor, "al_destroy_mouse_cursor" );

    begin
        if cursor /= null then
            C_Al_Destroy_Mouse_Cursor( cursor );
            cursor := null;
        end if;
    end Al_Destroy_Mouse_Cursor;

    ----------------------------------------------------------------------------

    function Al_Set_Mouse_Cursor( display : A_Allegro_Display;
                                  cursor  : A_Allegro_Mouse_Cursor ) return Boolean is

        function C_Al_Set_Mouse_Cursor( display : A_Allegro_Display;
                                        cursor  : A_Allegro_Mouse_Cursor ) return Bool;
        pragma Import( C, C_Al_Set_Mouse_Cursor, "al_set_mouse_cursor" );

    begin
        return To_Ada( C_Al_Set_Mouse_Cursor( display, cursor ) );
    end Al_Set_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Al_Set_Mouse_Cursor( display : A_Allegro_Display;
                                   cursor  : A_Allegro_Mouse_Cursor ) is
    begin
        if Al_Set_Mouse_Cursor( display, cursor ) then
            null;
        end if;
    end Al_Set_Mouse_Cursor;

    ----------------------------------------------------------------------------

    function Al_Set_System_Mouse_Cursor( display   : A_Allegro_Display;
                                         cursor_id : Allegro_System_Mouse_Cursor ) return Boolean is

        function C_Al_Set_System_Mouse_Cursor( display   : A_Allegro_Display;
                                               cursor_id : Allegro_System_Mouse_Cursor ) return Bool;
        pragma Import( C, C_Al_Set_System_Mouse_Cursor, "al_set_system_mouse_cursor" );

    begin
        return To_Ada( C_Al_Set_System_Mouse_Cursor( display, cursor_id ) );
    end Al_Set_System_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Al_Set_System_Mouse_Cursor( display   : A_Allegro_Display;
                                          cursor_id : Allegro_System_Mouse_Cursor ) is
    begin
        if Al_Set_System_Mouse_Cursor( display, cursor_id ) then
            null;
        end if;
    end Al_Set_System_Mouse_Cursor;

    ----------------------------------------------------------------------------

    function Al_Hide_Mouse_Cursor( display : A_Allegro_Display ) return Boolean is

        function C_Al_Hide_Mouse_Cursor( display : A_Allegro_Display ) return Bool;
        pragma Import( C, C_Al_Hide_Mouse_Cursor, "al_hide_mouse_cursor" );

    begin
        return To_Ada( C_Al_Hide_Mouse_Cursor( display ) );
    end Al_Hide_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Al_Hide_Mouse_Cursor( display : A_Allegro_Display ) is
    begin
        if Al_Hide_Mouse_Cursor( display ) then
            null;
        end if;
    end Al_Hide_Mouse_Cursor;

    ----------------------------------------------------------------------------

    function Al_Show_Mouse_Cursor( display : A_Allegro_Display ) return Boolean is

        function C_Al_Show_Mouse_Cursor( display : A_Allegro_Display ) return Bool;
        pragma Import( C, C_Al_Show_Mouse_Cursor, "al_show_mouse_cursor" );

    begin
        return To_Ada( C_Al_Show_Mouse_Cursor( display ) );
    end Al_Show_Mouse_Cursor;

    ----------------------------------------------------------------------------

    procedure Al_Show_Mouse_Cursor( display : A_Allegro_Display ) is
    begin
        if Al_Show_Mouse_Cursor( display ) then
            null;
        end if;
    end Al_Show_Mouse_Cursor;

end Allegro.Mouse.Cursors;
