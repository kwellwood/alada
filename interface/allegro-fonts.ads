--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;
with Allegro.UTF8;                      use Allegro.UTF8;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

-- Allegro 5.2.5 - Fonts addon
package Allegro.Fonts is

    -- Initialization

    function Al_Init_Font_Addon return Boolean;

    procedure Al_Shutdown_Font_Addon;
    pragma Import( C, Al_Shutdown_Font_Addon, "al_shutdown_font_addon" );

    function Al_Get_Allegro_Font_Version return Unsigned_32;
    pragma Import( C, Al_Get_Allegro_Font_Version, "al_get_allegro_font_version" );

    -- General font routines

    type Allegro_Font is limited private;
    type A_Allegro_Font is access all Allegro_Font;

    subtype Font_Loading_Flags is Unsigned_32;

    function Al_Load_Font( filename : String;
                           size     : Integer;
                           flags    : Font_Loading_Flags ) return A_Allegro_Font;

    procedure Al_Destroy_Font( f : in out A_Allegro_Font );

    type A_Loader_Proc is access
        function( filename : C.char_array;
                  size     : Integer;
                  flags    : Font_Loading_Flags ) return A_Allegro_Font;
    pragma Convention( C, A_Loader_Proc );

    function Al_Register_Font_Loader( ext : String; load : A_Loader_Proc ) return Boolean;

    function Al_Get_Fallback_Font( font : A_Allegro_Font ) return A_Allegro_Font;
    pragma Import( C, Al_Get_Fallback_Font, "al_get_fallback_font" );

    procedure AL_Set_Fallback_Font( font : A_Allegro_Font; fallback : A_Allegro_Font );
    pragma Import( C, AL_Set_Fallback_Font, "al_set_fallback_font" );

    function Al_Get_Font_Line_Height( f : A_Allegro_Font ) return Integer;
    pragma Import( C, Al_Get_Font_Line_Height, "al_get_font_line_height" );

    function Al_Get_Font_Ascent( f : A_Allegro_Font ) return Integer;
    pragma Import( C, Al_Get_Font_Ascent, "al_get_font_ascent" );

    function Al_Get_Font_Descent( f : A_Allegro_Font ) return Integer;
    pragma Import( C, Al_Get_Font_Descent, "al_get_font_descent" );

    type Allegro_Glyph is
        record
            bitmap   : A_Allegro_Bitmap;
            x, y     : Integer;
            w, h     : Integer;
            kerning  : Integer;
            offset_x : Integer;
            offset_y : Integer;
            advance  : Integer;
        end record;
    pragma Convention( C, Allegro_Glyph );

    type Font_Range is
        record
            first : Integer;
            last  : Integer;
        end record;
    pragma Convention( C, Font_Range );

    type Font_Ranges is array (Integer range <>) of Font_Range;
    pragma Convention( C, Font_Ranges );

    procedure Al_Get_Font_Ranges( font   : A_Allegro_Font;
                                  ranges : in out Font_Ranges;
                                  total  : out Integer );

    function Al_Get_Font_Ranges( font : A_Allegro_Font ) return Integer;

    function Al_Get_Glyph_Width( f : A_Allegro_Font; codepoint : Integer ) return Integer;
    pragma Import( C, Al_Get_Glyph_Width, "al_get_glyph_width" );

    function Al_Get_Glyph_Dimensions( f         : A_Allegro_Font;
                                      codepoint : Integer_32;
                                      bbx, bby  : access Integer;
                                      bbw, bbh  : access Integer ) return Boolean;

    procedure Al_Get_Glyph_Dimensions( f         : A_Allegro_Font;
                                       codepoint : Integer_32;
                                       bbx, bby  : out Integer;
                                       bbw, bbh  : out Integer );

    function Al_Get_Glyph_Advance( f          : A_Allegro_Font;
                                   codepoint1 : Integer_32;
                                   codepoint2 : Integer_32 ) return Integer;
    pragma Import( C, Al_Get_Glyph_Advance, "al_get_glyph_advance" );

    function Al_Get_Glyph_Advance( f          : A_Allegro_Font;
                                   codepoint1 : Character;
                                   codepoint2 : Character ) return Integer;

    function Al_Get_Glyph( f              : A_Allegro_Font;
                           prev_codepoint,
                           codepoint      : Integer_32;
                           glyph          : access Allegro_Glyph ) return Boolean;

    function Al_Get_Text_Width( f : A_Allegro_Font; str : String ) return Integer;

    function Al_Get_Ustr_Width( f : A_Allegro_Font; ustr : A_Allegro_Ustr ) return Integer;
    pragma Import( C, Al_Get_Ustr_Width, "al_get_ustr_width" );

    subtype Font_Draw_Flags is Unsigned_32;
    ALLEGRO_ALIGN_LEFT    : constant Font_Draw_Flags :=  0;
    ALLEGRO_ALIGN_CENTRE  : constant Font_Draw_Flags :=  1;
    ALLEGRO_ALIGN_CENTER  : constant Font_Draw_Flags :=  1;
    ALLEGRO_ALIGN_RIGHT   : constant Font_Draw_Flags :=  2;
    ALLEGRO_ALIGN_INTEGER : constant Font_Draw_Flags :=  4;

    ALLEGRO_NO_KERNING : constant Integer_32 := -1;

    procedure Al_Draw_Glyph( font      : A_Allegro_Font;
                             color     : Allegro_Color;
                             x, y      : Float;
                             codepoint : Integer_32 );
    pragma Import( C, Al_Draw_Glyph, "al_draw_glyph" );

    procedure Al_Draw_Text( font  : A_Allegro_Font;
                            color : Allegro_Color;
                            x, y  : Float;
                            flags : Font_Draw_Flags;
                            text  : String );

    procedure Al_Draw_Ustr( font  : A_Allegro_Font;
                            color : Allegro_Color;
                            x, y  : Float;
                            flags : Font_Draw_Flags;
                            ustr  : A_Allegro_Ustr );
    pragma Import( C, Al_Draw_Ustr, "al_draw_ustr" );

    procedure Al_Draw_Justified_Text( font   : A_Allegro_Font;
                                      color  : Allegro_Color;
                                      x1, x2 : Float;
                                      y      : Float;
                                      diff   : Float;
                                      flags  : Font_Draw_Flags;
                                      text   : String );

    procedure Al_Draw_Justified_Ustr( font   : A_Allegro_Font;
                                      color  : Allegro_Color;
                                      x1, x2 : Float;
                                      y      : Float;
                                      diff   : Float;
                                      flags  : Font_Draw_Flags;
                                      text   : A_Allegro_Ustr );
    pragma Import( C, Al_Draw_Justified_Ustr, "al_draw_justified_ustr" );

    -- Variable arguments, will not implement.
    --ALLEGRO_FONT_PRINTFUNC(void, al_draw_textf, (const ALLEGRO_FONT *font, ALLEGRO_COLOR color, float x, float y, int flags, char const *format, ...), 6, 7);

    -- Variable arguments, will not implement.
    --ALLEGRO_FONT_PRINTFUNC(void, al_draw_justified_textf, (const ALLEGRO_FONT *font, ALLEGRO_COLOR color, float x1, float x2, float y, float diff, int flags, char const *format, ...), 8, 9);

    procedure Al_Get_Text_Dimensions( f    : A_Allegro_Font;
                                      text : String;
                                      bbx  : out Integer;
                                      bby  : out Integer;
                                      bbw  : out Integer;
                                      bbh  : out Integer );

    procedure Al_Get_Ustr_Dimensions( f    : A_Allegro_Font;
                                      text : A_Allegro_Ustr;
                                      bbx  : out Integer;
                                      bby  : out Integer;
                                      bbw  : out Integer;
                                      bbh  : out Integer );

    procedure Al_Draw_Multiline_Text( font        : A_Allegro_Font;
                                      color       : Allegro_Color;
                                      x, y        : Float;
                                      max_width   : Float;
                                      line_height : Float;
                                      flags       : Font_Draw_Flags;
                                      text        : String );

    -- Variable arguments, will not implement.
    --ALLEGRO_FONT_FUNC(void, al_draw_multiline_textf, (const ALLEGRO_FONT *font, ALLEGRO_COLOR color, float x, float y, float max_width, float line_height, int flags, const char *format, ...));

    procedure Al_Draw_Multiline_Ustr( font        : A_Allegro_Font;
                                      color       : Allegro_Color;
                                      x, y        : Float;
                                      max_width   : Float;
                                      line_height : Float;
                                      flags       : Font_Draw_Flags;
                                      text        : A_Allegro_Ustr );
    pragma Import( C, Al_Draw_Multiline_Ustr, "al_draw_multiline_ustr" );

    procedure Al_Do_Multiline_Text( font      : A_Allegro_Font;
                                    max_width : Integer;
                                    text      : String;
                                    cb        : access function( line_num : Integer;
                                                                 line     : String;
                                                                 extra    : Address ) return Boolean;
                                    extra     : Address );

    procedure Al_Do_Multiline_Ustr( font      : A_Allegro_Font;
                                    max_width : Integer;
                                    text      : A_Allegro_Ustr;
                                    cb        : access function( line_num : Integer;
                                                                 line     : A_Allegro_Ustr;
                                                                 extra    : Address ) return Boolean;
                                    extra     : Address );

    -- Bitmap fonts

    type Unicode_Range is
        record
            first : Integer_32;
            last  : Integer_32;
        end record;
    pragma Convention( C, Unicode_Range );

    type Unicode_Range_Array is array (Integer range <>) of Unicode_Range;
    pragma Convention( C, Unicode_Range_Array );

    function Al_Grab_Font_From_Bitmap( bmp    : A_Allegro_Bitmap;
                                       ranges : Unicode_Range_Array
                                     ) return A_Allegro_Font;
    pragma Precondition( ranges'Length > 0 );

    function Al_Create_Builtin_Font return A_Allegro_Font;
    pragma Import( C, Al_Create_Builtin_Font, "al_create_builtin_font" );

    function Al_Load_Bitmap_Font( filename : String ) return A_Allegro_Font;

    function Al_Load_Bitmap_Font_Flags( filename : String;
                                        flags    : Font_Loading_Flags ) return A_Allegro_Font;

private

    type Allegro_Font is limited null record;
    pragma Convention( C, Allegro_Font );

end Allegro.Fonts;
