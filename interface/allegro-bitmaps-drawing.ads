--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- Allegro 5.2.5 - Bitmap routines - Bitmap drawing operations
package Allegro.Bitmaps.Drawing is

    -- Drawing

    subtype Draw_Flags is Unsigned_32;
    ALLEGRO_FLIP_HORIZONTAL : constant Draw_Flags;
    ALLEGRO_FLIP_VERTICAL   : constant Draw_Flags;

    procedure Al_Draw_Bitmap( bitmap : A_Allegro_Bitmap;
                              dx, dy : Float;
                              flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Bitmap, "al_draw_bitmap" );

    procedure Al_Draw_Tinted_Bitmap( bitmap : A_Allegro_Bitmap;
                                     tint   : Allegro_Color;
                                     dx, dy : Float;
                                     flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Tinted_Bitmap, "al_draw_tinted_bitmap" );

    procedure Al_Draw_Bitmap_Region( bitmap : A_Allegro_Bitmap;
                                     sx, sy : Float;
                                     sw, sh : Float;
                                     dx, dy : Float;
                                     flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Bitmap_Region, "al_draw_bitmap_region" );

    procedure Al_Draw_Tinted_Bitmap_Region( bitmap : A_Allegro_Bitmap;
                                            tint   : Allegro_Color;
                                            sx, sy : Float;
                                            sw, sh : Float;
                                            dx, dy : Float;
                                            flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Tinted_Bitmap_Region, "al_draw_tinted_bitmap_region" );

    procedure Al_Draw_Rotated_Bitmap( bitmap : A_Allegro_Bitmap;
                                      cx, cy : Float;
                                      dx, dy : Float;
                                      angle  : Float;
                                      flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Rotated_Bitmap, "al_draw_rotated_bitmap" );

    procedure Al_Draw_Tinted_Rotated_Bitmap( bitmap : A_Allegro_Bitmap;
                                             tint   : Allegro_Color;
                                             cx, cy : Float;
                                             dx, dy : Float;
                                             angle  : Float;
                                             flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Tinted_Rotated_Bitmap, "al_draw_tinted_rotated_bitmap" );

    procedure Al_Draw_Scaled_Rotated_Bitmap( bitmap : A_Allegro_Bitmap;
                                             cx, cy : Float;
                                             dx, dy : Float;
                                             xscale,
                                             yscale : Float;
                                             angle  : Float;
                                             flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Scaled_Rotated_Bitmap, "al_draw_scaled_rotated_bitmap" );

    procedure Al_Draw_Tinted_Scaled_Rotated_Bitmap( bitmap : A_Allegro_Bitmap;
                                                    tint   : Allegro_Color;
                                                    cx, cy : Float;
                                                    dx, dy : Float;
                                                    xscale,
                                                    yscale : Float;
                                                    angle  : Float;
                                                    flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Tinted_Scaled_Rotated_Bitmap, "al_draw_tinted_scaled_rotated_bitmap" );

    procedure Al_Draw_Tinted_Scaled_Rotated_Bitmap_Region( bitmap : A_Allegro_Bitmap;
                                                           sx, sy : Float;
                                                           sw, wh : Float;
                                                           tint   : Allegro_Color;
                                                           cx, cy : Float;
                                                           dx, dy : Float;
                                                           xscale,
                                                           yscale : Float;
                                                           angle  : Float;
                                                           flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Tinted_Scaled_Rotated_Bitmap_Region, "al_draw_tinted_scaled_rotated_bitmap_region" );

    procedure Al_Draw_Scaled_Bitmap( bitmap : A_Allegro_Bitmap;
                                     sx, sy : Float;
                                     sw, sh : Float;
                                     dx, dy : Float;
                                     dw, dh : Float;
                                     flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Scaled_Bitmap, "al_draw_scaled_bitmap" );

    procedure Al_Draw_Tinted_Scaled_Bitmap( bitmap : A_Allegro_Bitmap;
                                            tint   : Allegro_Color;
                                            sx, sy : Float;
                                            sw, sh : Float;
                                            dx, dy : Float;
                                            dw, dh : Float;
                                            flags  : Draw_Flags );
    pragma Import( C, Al_Draw_Tinted_Scaled_Bitmap, "al_draw_tinted_scaled_bitmap" );

private

    ALLEGRO_FLIP_HORIZONTAL : constant Draw_Flags := 16#0001#;
    ALLEGRO_FLIP_VERTICAL   : constant Draw_Flags := 16#0002#;

end Allegro.Bitmaps.Drawing;
