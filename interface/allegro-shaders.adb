--
-- Copyright (c) 2016-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;
with System;                            use System;

package body Allegro.Shaders is

    function Al_Attach_Shader_Source( shader : A_Allegro_Shader;
                                      typ    : Allegro_Shader_Type;
                                      source : String ) return Boolean is
        function C_Al_Attach_Shader_Source( shader : A_Allegro_Shader;
                                            typ    : Allegro_Shader_Type;
                                            source : char_array ) return Bool;
        pragma Import( C, C_Al_Attach_Shader_Source, "al_attach_shader_source" );
    begin
        return C_Al_Attach_Shader_Source( shader, typ, To_C( source ) ) = B_TRUE;
    end Al_Attach_Shader_Source;

    ----------------------------------------------------------------------------

    procedure Al_Attach_Shader_Source( shader : A_Allegro_Shader;
                                       typ    : Allegro_Shader_Type;
                                       source : String ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Attach_Shader_Source( shader, typ, source );
    end Al_Attach_Shader_Source;

    ----------------------------------------------------------------------------

    function Al_Attach_Shader_Source_File( shader   : A_Allegro_Shader;
                                           typ      : Allegro_Shader_Type;
                                           filename : String ) return Boolean is
        function C_Al_Attach_Shader_Source_File( shader   : A_Allegro_Shader;
                                                 typ      : Allegro_Shader_Type;
                                                 filename : char_array ) return Bool;
        pragma Import( C, C_Al_Attach_Shader_Source_File, "al_attach_shader_source_file" );
    begin
        return C_Al_Attach_Shader_Source_File( shader, typ, To_C( filename ) ) = B_TRUE;
    end Al_Attach_Shader_Source_File;

    ----------------------------------------------------------------------------

    function Al_Build_Shader( shader : A_Allegro_Shader ) return Boolean is
        function C_Al_Build_Shader( shader : A_Allegro_Shader ) return Bool;
        pragma Import( C, C_Al_Build_Shader, "al_build_shader" );
    begin
        return C_Al_Build_Shader( shader ) = B_TRUE;
    end Al_Build_Shader;

    ----------------------------------------------------------------------------

    procedure Al_Build_Shader( shader : A_Allegro_Shader ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Build_Shader( shader );
    end Al_Build_Shader;

    ----------------------------------------------------------------------------

    function Al_Get_Shader_Log( shader : A_Allegro_Shader ) return String is
        function C_Al_Get_Shader_Log( shader : A_Allegro_Shader ) return chars_ptr;
        pragma Import( C, C_Al_Get_Shader_Log, "al_get_shader_log" );
    begin
        return Value( C_Al_Get_Shader_Log( shader ) );
    end Al_Get_Shader_Log;

    ----------------------------------------------------------------------------

    function Al_Use_Shader( shader : A_Allegro_Shader ) return Boolean is
        function C_Al_Use_Shader( shader : A_Allegro_Shader ) return Bool;
        pragma Import( C, C_Al_Use_Shader, "al_use_shader" );
    begin
        return C_Al_Use_Shader( shader ) = B_TRUE;
    end Al_Use_Shader;

    ----------------------------------------------------------------------------

    procedure Al_Use_Shader( shader : A_Allegro_Shader ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Use_Shader( shader );
    end Al_Use_Shader;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Shader( shader : in out A_Allegro_Shader ) is
        procedure C_Al_Destroy_Shader( shader : A_Allegro_Shader );
        pragma Import( C, C_Al_Destroy_Shader, "al_destroy_shader" );
    begin
        if shader /= null then
            C_Al_Destroy_Shader( shader );
            shader := null;
        end if;
    end Al_Destroy_Shader;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Sampler( name : String; bitmap : A_Allegro_Bitmap; unit : Integer ) return Boolean is
        function C_Al_Set_Shader_Sampler( name   : char_array;
                                          bitmap : A_Allegro_Bitmap;
                                          unit   : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Shader_Sampler, "al_set_shader_sampler" );
    begin
        return C_Al_Set_Shader_Sampler( To_C( name ), bitmap, unit ) = B_TRUE;
    end Al_Set_Shader_Sampler;

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Sampler( name : String; bitmap : A_Allegro_Bitmap; unit : Integer ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Sampler( name, bitmap, unit );
    end Al_Set_Shader_Sampler;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Matrix( name : String; matrix : Allegro_Transform ) return Boolean is
        function C_Al_Set_Shader_Matrix( name : char_array; matrix : access Allegro_Transform ) return Bool;
        pragma Import( C, C_Al_Set_Shader_Matrix, "al_set_shader_matrix" );
        mat : aliased Allegro_Transform;
    begin
        Al_Copy_Transform( mat, matrix );
        return C_Al_Set_Shader_Matrix( To_C( name ), mat'Access ) = B_TRUE;
    end Al_Set_Shader_Matrix;

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Matrix( name : String; matrix : Allegro_Transform ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Matrix( name, matrix );
    end Al_Set_Shader_Matrix;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Int( name : String; i : Integer ) return Boolean is
        function C_Al_Set_Shader_Int( name : char_array; i : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Shader_Int, "al_set_shader_int" );
    begin
        return C_Al_Set_Shader_Int( To_C( name ), i ) = B_TRUE;
    end Al_Set_Shader_Int;

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Int( name : String; i : Integer ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Int( name, i );
    end Al_Set_Shader_Int;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Float( name : String; f : Float ) return Boolean is
        function C_Al_Set_Shader_Float( name : char_array; f : Float ) return Bool;
        pragma Import( C, C_Al_Set_Shader_Float, "al_set_shader_float" );
    begin
        return C_Al_Set_Shader_Float( To_C( name ), f ) = B_TRUE;
    end Al_Set_Shader_Float;

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Float( name : String; f : Float ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Float( name, f );
    end Al_Set_Shader_Float;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Int_Vector( name           : String;
                                       num_components : Integer;
                                       i              : Integer_Array ) return Boolean is
        function C_Al_Set_Shader_Int_Vector( name           : char_array;
                                             num_components : Integer;
                                             i              : Address;
                                             num_elements   : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Shader_Int_Vector, "al_set_shader_int_vector" );
    begin
        return C_Al_Set_Shader_Int_Vector( To_C( name ), num_components, i(i'First)'Address, i'Length / num_components ) = B_TRUE;
    end Al_Set_Shader_Int_Vector;

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Int_Vector( name           : String;
                                        num_components : Integer;
                                        i              : Integer_Array ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Int_Vector( name, num_components, i );
    end Al_Set_Shader_Int_Vector;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Int_Vector( name : String; i : Integer_Array ) return Boolean
    is (Al_Set_Shader_Int_Vector( name, i'Length, i ));

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Int_Vector( name : String; i : Integer_Array ) is
    begin
        Al_Set_Shader_Int_Vector( name, i'Length, i );
    end Al_Set_Shader_Int_Vector;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Float_Vector( name           : String;
                                         num_components : Integer;
                                         f              : Float_Array ) return Boolean is
        function C_Al_Set_Shader_Float_Vector( name           : char_array;
                                               num_components : Integer;
                                               f              : Address;
                                               num_elements   : Integer ) return Bool;
        pragma Import( C, C_Al_Set_Shader_Float_Vector, "al_set_shader_float_vector" );
    begin
        return C_Al_Set_Shader_Float_Vector( To_C( name ), num_components, f(f'First)'Address, f'Length / num_components ) = B_TRUE;
    end Al_Set_Shader_Float_Vector;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Float_Vector( name : String; f : Float_Array ) return Boolean
    is (Al_Set_Shader_Float_Vector( name, f'Length, f ));

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Float_Vector( name : String; f : Float_Array ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Float_Vector( name, f );
    end Al_Set_Shader_Float_Vector;

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Float_Vector( name : String; num_components : Integer; f : Float_Array ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Float_Vector( name, num_components, f );
    end Al_Set_Shader_Float_Vector;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Float_Vector( name : String; color : Allegro_Color ) return Boolean is
        r, g, b, a : Float;
    begin
        Al_Unmap_RGBA_f( color, r, g, b, a );
        return Al_Set_Shader_Float_Vector( name, (r, g, b, a) );
    end Al_Set_Shader_Float_Vector;

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Float_Vector( name : String; color : Allegro_Color ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Float_Vector( name, color );
    end Al_Set_Shader_Float_Vector;

    ----------------------------------------------------------------------------

    function Al_Set_Shader_Bool( name : String; b : Boolean ) return Boolean is
        function C_Al_Set_Shader_Bool( name : char_array; b : Bool ) return Bool;
        pragma Import( C, C_Al_Set_Shader_Bool, "al_set_shader_bool" );
    begin
        return C_Al_Set_Shader_Bool( To_C( name ), (if b then B_TRUE else B_FALSE) ) = B_TRUE;
    end Al_Set_Shader_Bool;

    ----------------------------------------------------------------------------

    procedure Al_Set_Shader_Bool( name : String; b : Boolean ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Set_Shader_Bool( name, b );
    end Al_Set_Shader_Bool;

    ----------------------------------------------------------------------------

    function Al_Get_Default_Shader_Source( platform : Allegro_Shader_Platform;
                                           typ      : Allegro_Shader_Type ) return String is
        function C_Al_Get_Default_Shader_Source( platform : Allegro_Shader_Platform;
                                                 typ      : Allegro_Shader_Type ) return chars_ptr;
        pragma Import( C, C_Al_Get_Default_Shader_Source, "al_get_default_shader_source" );
    begin
        return Value( C_Al_Get_Default_Shader_Source( platform, typ ) );
    end Al_Get_Default_Shader_Source;

end Allegro.Shaders;
