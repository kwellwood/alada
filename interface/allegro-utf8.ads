--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

-- Allegro 5.2.5 - UTF-8 string routines
package Allegro.UTF8 is

    -- UTF-8 string types

    type Allegro_Ustr is limited
        record
            mlen : Integer;
            slen : Integer;
            data : C.Strings.chars_ptr;
        end record;
    pragma Convention( C, Allegro_Ustr );
    type A_Allegro_Ustr is access all Allegro_Ustr;

    type Allegro_Ustr_Info is
        record
            mlen : Integer;
            slen : Integer;
            data : C.Strings.chars_ptr;
        end record;
    type A_Allegro_Ustr_Info is access all Allegro_Ustr_Info;

    -- Creating and destroying strings

    function Al_Ustr_New( s : String ) return A_Allegro_Ustr;

    function Al_Ustr_New_From_Buffer( s : String ) return A_Allegro_Ustr;

    procedure Al_Ustr_Free( us : in out A_Allegro_Ustr );

    function Al_Cstr( us : A_Allegro_Ustr ) return String;

    procedure Al_Ustr_To_Buffer( us     : A_Allegro_Ustr;
                                 buffer : out String );

    function Al_Cstr_Dup( us : A_Allegro_Ustr ) return String;

    function Al_Ustr_Dup( us : A_Allegro_Ustr ) return A_Allegro_Ustr;
    pragma Import( C, Al_Ustr_Dup, "al_ustr_dup" );

    function Al_Ustr_Dup_Substr( us        : A_Allegro_Ustr;
                                 start_pos,
                                 end_pos   : Integer ) return A_Allegro_Ustr;
    pragma Import( C, Al_Ustr_Dup_Substr, "al_ustr_dup_substr" );

    -- Predefined strings

    function Al_Ustr_Empty_String return A_Allegro_Ustr;
    pragma Import( C, Al_Ustr_Empty_String, "al_ustr_empty_string" );

    -- Creating strings by referencing other data

    function Al_Ref_Cstr( info : access Allegro_Ustr_Info; s : String ) return A_Allegro_Ustr;

    function Al_Ref_Buffer( info : access Allegro_Ustr_Info;
                            s    : String ) return A_Allegro_Ustr;

    function Al_Ref_Ustr( info      : access Allegro_Ustr_Info;
                          us        : A_Allegro_Ustr;
                          start_pos,
                          end_pos   : Integer ) return A_Allegro_Ustr;
    pragma Import( C, Al_Ref_Ustr, "al_ref_ustr" );

    -- Sizes and offsets

    function Al_Ustr_Size( us : A_Allegro_Ustr ) return C.size_t;
    pragma Import( C, Al_Ustr_Size, "al_ustr_size" );

    function Al_Ustr_Length( us : A_Allegro_Ustr ) return C.size_t;
    pragma Import( C, Al_Ustr_Length, "al_ustr_length" );

    function Al_Ustr_Offset( us : A_Allegro_Ustr; index : Integer ) return Integer;
    pragma Import( C, Al_Ustr_Offset, "al_ustr_offset" );

    function Al_Ustr_Next( us : A_Allegro_Ustr; pos : access Integer ) return Boolean;

    function Al_Ustr_Prev( us : A_Allegro_Ustr; pos : access Integer ) return Boolean;

    -- Getting code points

    function Al_Ustr_Get( us : A_Allegro_Ustr; pos : Integer ) return Integer_32;
    pragma Import( C, Al_Ustr_Get, "al_ustr_get" );

    function Al_Ustr_Get_Next( us : A_Allegro_Ustr; pos : access Integer ) return Integer_32;
    pragma Import( C, Al_Ustr_Get_Next, "al_ustr_get_next" );

    function Al_Ustr_Prev_Get( us : A_Allegro_Ustr; pos : access Integer ) return Integer_32;
    pragma Import( C, Al_Ustr_Prev_Get, "al_ustr_prev_get" );

    -- Inserting into strings

    function Al_Ustr_Insert( us1 : A_Allegro_Ustr;
                             pos : Integer;
                             us2 : A_Allegro_Ustr ) return Boolean;

    function Al_Ustr_Insert_Cstr( us  : A_Allegro_Ustr;
                                  pos : Integer;
                                  us2 : String ) return Boolean;

    function Al_Ustr_Insert_Chr( us  : A_Allegro_Ustr;
                                 pos : Integer;
                                 chr : Integer_32 ) return C.size_t;
    pragma Import( C, Al_Ustr_Insert_Chr, "al_ustr_insert_chr" );

    -- Appending to strings

    function Al_Ustr_Append( us1, us2 : A_Allegro_Ustr ) return Boolean;

    procedure Al_Ustr_Append( us1, us2 : A_Allegro_Ustr );

    function Al_Ustr_Append_Cstr( us : A_Allegro_Ustr; s : String ) return Boolean;

    procedure Al_Ustr_Append_Cstr( us : A_Allegro_Ustr; s : String );

    function Al_Ustr_Append_Chr( us : A_Allegro_Ustr; chr : Integer_32 ) return C.size_t;
    pragma Import( C, Al_Ustr_Append_Chr, "al_ustr_append_chr" );

    -- Removing parts of strings

    function Al_Ustr_Remove_Chr( us : A_Allegro_Ustr; pos : Integer ) return Boolean;

    procedure Al_Ustr_Remove_Chr( us : A_Allegro_Ustr; pos : Integer );

    function Al_Ustr_Remove_Range( us        : A_Allegro_Ustr;
                                   start_pos,
                                   end_pos   : Integer ) return Boolean;

    function Al_Ustr_Truncate( us : A_Allegro_Ustr; start_pos : Integer ) return Boolean;

    function Al_Ustr_Ltrim_Ws( us : A_Allegro_Ustr ) return Boolean;

    procedure Al_Ustr_Ltrim_Ws( us : A_Allegro_Ustr );

    function Al_Ustr_Rtrim_Ws( us : A_Allegro_Ustr ) return Boolean;

    procedure Al_Ustr_Rtrim_Ws( us : A_Allegro_Ustr );

    function Al_Ustr_Trim_Ws( us : A_Allegro_Ustr ) return Boolean;

    procedure Al_Ustr_Trim_Ws( us : A_Allegro_Ustr );

    -- Assigning one string to another

    function Al_Ustr_Assign( us1, us2 : A_Allegro_Ustr ) return Boolean;

    function Al_Ustr_Assign_Substr( us1, us2  : A_Allegro_Ustr;
                                    start_pos,
                                    end_pos   : Integer ) return Boolean;

    function Al_Ustr_Assign_Cstr( us1 : A_Allegro_Ustr; s : String ) return Boolean;

    -- Replacing parts of a string

    function Al_Ustr_Set_Chr( us  : A_Allegro_Ustr;
                              pos : Integer;
                              chr : Integer_32 ) return C.size_t;
    pragma Import( C, Al_Ustr_Set_Chr, "al_ustr_set_chr" );

    function Al_Ustr_Replace_Range( us1        : A_Allegro_Ustr;
                                    start_pos1,
                                    end_pos1   : Integer;
                                    us2        : A_Allegro_Ustr ) return Boolean;

    -- Searching

    function Al_Ustr_Find_Chr( us        : A_Allegro_Ustr;
                               start_pos : Integer;
                               c         : Integer_32 ) return Integer;
    pragma Import( C, Al_Ustr_Find_Chr, "al_ustr_find_chr" );

    function Al_Ustr_Rfind_Chr( us        : A_Allegro_Ustr;
                                start_pos : Integer;
                                c         : Integer_32 ) return Integer;
    pragma Import( C, Al_Ustr_Rfind_Chr, "al_ustr_rfind_chr" );

    function Al_Ustr_Find_Set( us         : A_Allegro_Ustr;
                               start_pos  : Integer;
                               acceptChrs : A_Allegro_Ustr ) return Integer;
    pragma Import( C, Al_Ustr_Find_Set, "al_ustr_find_set" );

    function Al_Ustr_Find_Set_Cstr( us         : A_Allegro_Ustr;
                                    start_pos  : Integer;
                                    acceptChrs : String ) return Integer;

    function Al_Ustr_Find_Cset( us        : A_Allegro_Ustr;
                                start_pos : Integer;
                                reject    : A_Allegro_Ustr ) return Integer;
    pragma Import( C, Al_Ustr_Find_Cset, "al_ustr_find_cset" );

    function Al_Ustr_Find_Cset_Cstr( us        : A_Allegro_Ustr;
                                     start_pos : Integer;
                                     reject    : String ) return Integer;

    function Al_Ustr_Find_Str( haystack  : A_Allegro_Ustr;
                               start_pos : Integer;
                               needle    : A_Allegro_Ustr ) return Integer;
    pragma Import( C, Al_Ustr_Find_Str, "al_ustr_find_str" );

    function Al_Ustr_Find_Cstr( haystack  : A_Allegro_Ustr;
                                start_pos : Integer;
                                needle    : String ) return Integer;

    function Al_Ustr_Rfind_Str( haystack  : A_Allegro_Ustr;
                                start_pos : Integer;
                                needle    : A_Allegro_Ustr ) return Integer;
    pragma Import( C, Al_Ustr_Rfind_Str, "al_ustr_rfind_str" );

    function Al_Ustr_Rfind_Cstr( haystack  : A_Allegro_Ustr;
                                 start_pos : Integer;
                                 needle    : String ) return Integer;

    function Al_Ustr_Find_Replace( us        : A_Allegro_Ustr;
                                   start_pos : Integer;
                                   find      : A_Allegro_Ustr;
                                   replace   : A_Allegro_Ustr ) return Boolean;

    procedure Al_Ustr_Find_Replace( us        : A_Allegro_Ustr;
                                    start_pos : Integer;
                                    find      : A_Allegro_Ustr;
                                    replace   : A_Allegro_Ustr );

    function Al_Ustr_Find_Replace_Cstr( us        : A_Allegro_Ustr;
                                        start_pos : Integer;
                                        find      : String;
                                        replace   : String ) return Boolean;

    procedure Al_Ustr_Find_Replace_Cstr( us        : A_Allegro_Ustr;
                                         start_pos : Integer;
                                         find      : String;
                                         replace   : String );

    -- Comparing

    function Al_Ustr_Equal( us1, us2 : A_Allegro_Ustr ) return Boolean;

    function Al_Ustr_Compare( u, v : A_Allegro_Ustr ) return Integer;
    pragma Import( C, Al_Ustr_Compare, "al_ustr_compare" );

    function Al_Ustr_Ncompare( us1, us2 : A_Allegro_Ustr; n : Integer ) return Integer;
    pragma Import( C, Al_Ustr_Ncompare, "al_ustr_ncompare" );

    function Al_Ustr_Has_Prefix( u, v : A_Allegro_Ustr ) return Boolean;

    function Al_Ustr_Has_Prefix_Cstr( u : A_Allegro_Ustr; s : String ) return Boolean;

    function Al_Ustr_Has_Suffix( u, v : A_Allegro_Ustr ) return Boolean;

    function Al_Ustr_Has_Suffix_Cstr( us1 : A_Allegro_Ustr; s : String ) return Boolean;

    -- UTF-16 conversion

    function Al_Ustr_New_From_Utf16( s : Wide_String ) return A_Allegro_Ustr;

    function Al_Ustr_Size_Utf16( us : A_Allegro_Ustr ) return C.size_t;
    pragma Import( C, Al_Ustr_Size_Utf16, "al_ustr_size_utf16" );

    -- 'size' is in bytes, not characters!
    procedure Al_Ustr_Encode_Utf16( us   : A_Allegro_Ustr;
                                    s    : out Wide_String;
                                    size : out C.size_t );

    -- Low-level UTF-8 routines

    function Al_Utf8_Width( chr : Integer_32 ) return C.size_t;
    pragma Import( C, Al_Utf8_Width, "al_utf8_width" );

    procedure Al_Utf8_Encode( chr  : Integer_32;
                              s    : out String;
                              size : out C.size_t );
    pragma Precondition( s'Length = 4 );

    -- Low-level UTF-16 routines

    function C_Al_Utf16_Width( chr : Integer_32 ) return C.size_t;
    pragma Import( C, C_Al_Utf16_Width, "al_utf16_width" );

    procedure Al_Utf16_Encode( chr  : Integer_32;
                               s    : out Wide_String;
                               size : out C.size_t );
    pragma Precondition( s'Length = 2 );

end Allegro.UTF8;
