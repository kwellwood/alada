--
-- Copyright (c) 2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Direct3D is

    function Al_Have_D3D_Non_Pow2_Texture_Support return Boolean is
        function C_Al_Have_D3D_Non_Pow2_Texture_Support return Bool;
        pragma Import( C, C_Al_Have_D3D_Non_Pow2_Texture_Support, "al_have_d3d_non_pow2_texture_support" );
    begin
        return C_Al_Have_D3D_Non_Pow2_Texture_Support = B_TRUE;
    end Al_Have_D3D_Non_Pow2_Texture_Support;

    ----------------------------------------------------------------------------

    function Al_Have_D3D_Non_Square_Texture_Support return Boolean is
        function C_Al_Have_D3D_Non_Square_Texture_Support return Bool;
        pragma Import( C, C_Al_Have_D3D_Non_Square_Texture_Support, "al_have_d3d_non_square_texture_support" );
    begin
        return C_Al_Have_D3D_Non_Square_Texture_Support = B_TRUE;
    end Al_Have_D3D_Non_Square_Texture_Support;

    ----------------------------------------------------------------------------

    function Al_Get_D3D_Texture_Size( bitmap        : A_Allegro_Bitmap;
                                      width, height : access Integer ) return Boolean is
        function C_Al_Get_D3D_Texture_Size( bitmap        : A_Allegro_Bitmap;
                                            width, height : access Integer ) return Bool;
        pragma Import( C, C_Al_Get_D3D_Texture_Size, "al_get_d3d_texture_size" );
    begin
        return C_Al_Get_D3D_Texture_Size( bitmap, width, height ) = B_TRUE;
    end Al_Get_D3D_Texture_Size;

    ----------------------------------------------------------------------------

    function Al_Is_D3D_Device_Lost( display : A_Allegro_Display ) return Boolean is
        function C_Al_Is_D3D_Device_Lost( display : A_Allegro_Display ) return Bool;
        pragma Import( C, C_Al_Is_D3D_Device_Lost, "al_is_d3d_device_lost" );
    begin
        return C_Al_Is_D3D_Device_Lost( display ) = B_TRUE;
    end Al_Is_D3D_Device_Lost;

    ----------------------------------------------------------------------------

    release : A_Display_Callback := null;

    type A_C_Display_Callback is access procedure( display : A_Allegro_Display );
    pragma Convention( C, A_C_Display_Callback );

    procedure C_Release( display : A_Allegro_Display );
    pragma Convention( C, C_Release );

    procedure C_Release( display : A_Allegro_Display ) is
    begin
        if release /= null then
            release( display );
        end if;
    end C_Release;

    procedure Al_Set_D3D_Device_Release_Callback( callback : A_Display_Callback ) is
        procedure C_Al_Set_D3D_Device_Release_Callback( callback : A_C_Display_Callback );
        pragma Import( C, C_Al_Set_D3D_Device_Release_Callback, "al_set_d3d_device_release_callback" );
    begin
        release := callback;
        if callback /= null then
            C_Al_Set_D3D_Device_Release_Callback( C_Release'Access );
        else
            C_Al_Set_D3D_Device_Release_Callback( null );
        end if;
    end Al_Set_D3D_Device_Release_Callback;

    ----------------------------------------------------------------------------

    restore : A_Display_Callback := null;

    procedure C_Restore( display : A_Allegro_Display );
    pragma Convention( C, C_Restore );

    procedure C_Restore( display : A_Allegro_Display ) is
    begin
        if restore /= null then
            restore( display );
        end if;
    end C_Restore;

    procedure Al_Set_D3D_Device_Restore_Callback( callback : A_Display_Callback ) is
        procedure C_Al_Set_D3D_Device_Restore_Callback( callback : A_C_Display_Callback );
        pragma Import( C, C_Al_Set_D3D_Device_Restore_Callback, "al_set_d3d_device_restore_callback" );
    begin
        restore := callback;
        if callback /= null then
            C_Al_Set_D3D_Device_Restore_Callback( C_Restore'Access );
        else
            C_Al_Set_D3D_Device_Restore_Callback( null );
        end if;
    end Al_Set_D3D_Device_Restore_Callback;

end Allegro.Direct3D;
