--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- Allegro 5.2.5 - Transformation routines
package Allegro.Transformations is

    type Float_Matrix_2D is array (Integer range <>, Integer range <>) of Float;

    type Allegro_Transform is
        record
            m : Float_Matrix_2D(0..3, 0..3);
        end record;
    pragma Convention( C, Allegro_Transform );

    -- Building Transformations

    procedure Al_Copy_Transform( dest : in out Allegro_Transform; src : Allegro_Transform );
    pragma Import( C, Al_Copy_Transform, "al_copy_transform" );

    procedure Al_Identity_Transform( trans : in out Allegro_Transform );
    pragma Import( C, Al_Identity_Transform, "al_identity_transform" );

    procedure Al_Build_Transform( trans  : in out Allegro_Transform;
                                  x, y   : Float;
                                  sx, sy : Float;
                                  theta  : Float );
    pragma Import( C, Al_Build_Transform, "al_build_transform" );

    procedure Al_Build_Camera_Transform( trans                  : in out Allegro_Transform;
                                         pos_x,  pos_y,  pos_z  : Float;
                                         look_x, look_y, look_z : Float;
                                         up_x,   up_y,   up_z   : Float );
    pragma Import( C, Al_Build_Camera_Transform, "al_build_camera_transform" );

    -- Composition

    procedure Al_Compose_Transform( trans : in out Allegro_Transform;
                                    other : Allegro_Transform );
    pragma Import( C, Al_Compose_Transform, "al_compose_transform" );

    procedure Al_Invert_Transform( trans : in out Allegro_Transform );
    pragma Import( C, Al_Invert_Transform, "al_invert_transform" );

    procedure Al_Transpose_Transform( trans : in out Allegro_Transform );
    pragma Import( C, Al_Transpose_Transform, "al_transpose_transform" );

    function Al_Check_Inverse( trans : Allegro_Transform; tol : Float ) return Integer;
    pragma Import( C, Al_Check_Inverse, "al_check_inverse" );

    procedure Al_Orthographic_Transform( trans   : in out Allegro_Transform;
                                         left,
                                         top,
                                         n,
                                         right,
                                         bottom,
                                         f       : Float );
    pragma Import( C, Al_Orthographic_Transform, "al_orthographic_transform" );

    procedure Al_Perspective_Transform( trans  : in out Allegro_Transform;
                                        left,
                                        top,
                                        n,
                                        right,
                                        bottom,
                                        f      : Float );
    pragma Import( C, Al_Perspective_Transform, "al_perspective_transform" );

    -- Using Transformations

    procedure Al_Use_Transform( trans : Allegro_Transform );
    pragma Import( C, Al_Use_Transform, "al_use_transform" );

    procedure Al_Use_Projection_Transform( trans : Allegro_Transform );
    pragma Import( C, Al_Use_Projection_Transform, "al_use_projection_transform" );

    -- returns access to the current transform; don't delete it
    function Al_Get_Current_Transform return access Allegro_Transform;
    pragma Import( C, Al_Get_Current_Transform, "al_get_current_transform" );

    -- returns access to the current transform; don't delete it
    function Al_Get_Current_Inverse_Transform return access Allegro_Transform;
    pragma Import( C, Al_Get_Current_Inverse_Transform, "al_get_current_inverse_transform" );

    -- returns access to the current transform; don't delete it
    function Al_Get_Current_Projection_Transform return access Allegro_Transform;
    pragma Import( C, Al_Get_Current_Projection_Transform, "al_get_current_projection_transform" );

    -- Translating / Rotating / Scaling / Shearing

    procedure Al_Translate_Transform( trans : in out Allegro_Transform; x, y : Float );
    pragma Import( C, Al_Translate_Transform, "al_translate_transform" );

    procedure Al_Translate_Transform_3d( trans : in out Allegro_Transform; x, y, z : Float );
    pragma Import( C, Al_Translate_Transform_3d, "al_translate_transform_3d" );

    procedure Al_Rotate_Transform( trans : in out Allegro_Transform; theta : Float );
    pragma Import( C, Al_Rotate_Transform, "al_rotate_transform" );

    procedure Al_Rotate_Transform_3d( trans : in out Allegro_Transform; x, y, z, angle : Float );
    pragma Import( C, Al_Rotate_Transform_3d, "al_rotate_transform_3d" );

    procedure Al_Scale_Transform( trans : in out Allegro_Transform; sx, sy : Float );
    pragma Import( C, Al_Scale_Transform, "al_scale_transform" );

    procedure Al_Scale_Transform_3d( trans : in out Allegro_Transform; sx, sy, sz : Float );
    pragma Import( C, Al_Scale_Transform_3d, "al_scale_transform_3d" );

    procedure Al_Transform_Coordinates( trans : Allegro_Transform; x, y : in out Float );
    pragma Import( C, Al_Transform_Coordinates, "al_transform_coordinates" );

    procedure Al_Transform_Coordinates_3d( trans : Allegro_Transform; x, y, z : in out Float );
    pragma Import( C, Al_Transform_Coordinates_3d, "al_transform_coordinates_3d" );

    procedure Al_Transform_Coordinates_4d( trans : Allegro_Transform; x, y, z, w : in out Float );
    pragma Import( C, Al_Transform_Coordinates_4d, "al_transform_coordinates_4d" );

    procedure Al_Transform_Coordinates_3d_Projective( trans : Allegro_Transform; x, y, z : in out Float );
    pragma Import( C, Al_Transform_Coordinates_3d_Projective, "al_transform_coordinates_3d_projective" );

    procedure Al_Horizontal_Shear_Transform( trans : in out Allegro_Transform; theta : Float );
    pragma Import( C, Al_Horizontal_Shear_Transform, "al_horizontal_shear_transform" );

    procedure Al_Vertical_Shear_Transform( trans : in out Allegro_Transform; theta : Float );
    pragma Import( C, Al_Vertical_Shear_Transform, "al_vertical_shear_transform" );

end Allegro.Transformations;
