--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- Allegro 5.2.5 - Display routines - Fullscreen display modes
package Allegro.Displays.Fullscreen is

    type Allegro_Display_Mode is
        record
            width        : Integer;
            height       : Integer;
            format       : Allegro_Pixel_Format;
            refresh_rate : Integer;
        end record;
    pragma Convention( C, Allegro_Display_Mode );
    type A_Allegro_Display_Mode is access all Allegro_Display_Mode;

    function Al_Get_Num_Display_Modes return Integer;
    pragma Import( C, Al_Get_Num_Display_Modes, "al_get_num_display_modes" );

    function Al_Get_Display_Mode( index : Integer;
                                  mode  : A_Allegro_Display_Mode ) return A_Allegro_Display_Mode;
    pragma Import( C, Al_Get_Display_Mode, "al_get_display_mode" );

    -- A more convenient wrapper around the Al_Get_Display_Mode function above.
    procedure Al_Get_Display_Mode( index   : Integer;
                                   mode    : in out Allegro_Display_Mode;
                                   success : out Boolean );

    -- Same as the procedural version of Al_Get_Display_Mode but without failure
    -- checking.
    procedure Al_Get_Display_Mode( index : Integer;
                                   mode  : in out Allegro_Display_Mode );

end Allegro.Displays.Fullscreen;
