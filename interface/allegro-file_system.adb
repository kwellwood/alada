--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.File_System is

    function Al_Create_Fs_Entry( path : String ) return A_Allegro_FS_Entry is

        function C_Al_Create_Fs_Entry( path : C.char_array ) return A_Allegro_FS_Entry;
        pragma Import( C, C_Al_Create_Fs_Entry, "al_create_fs_entry" );

    begin
        return C_Al_Create_Fs_Entry( To_C( path ) );
    end Al_Create_Fs_Entry;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Fs_Entry( e : in out A_Allegro_Fs_Entry ) is

        procedure C_Al_Destroy_Fs_Entry( e : A_Allegro_Fs_Entry );
        pragma Import( C, C_Al_Destroy_Fs_Entry, "al_destroy_fs_entry" );

    begin
        if e /= null then
            C_Al_Destroy_Fs_Entry( e );
            e := null;
        end if;
    end Al_Destroy_Fs_Entry;

    ----------------------------------------------------------------------------

    function Al_Get_Fs_Entry_Name( e : A_Allegro_Fs_Entry ) return String is

        function C_Al_Get_Fs_Entry_Name( e : A_Allegro_Fs_Entry ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Fs_Entry_Name, "al_get_fs_entry_name" );

    begin
        return To_Ada( Value( C_Al_Get_Fs_Entry_Name( e ) ) );
    end Al_Get_Fs_Entry_Name;

    ----------------------------------------------------------------------------

    function Al_Update_Fs_Entry( e : A_Allegro_Fs_Entry ) return Boolean is

        function C_Al_Update_Fs_Entry( e : A_Allegro_Fs_Entry ) return Bool;
        pragma Import( C, C_Al_Update_Fs_Entry, "al_update_fs_entry" );

    begin
        return To_Ada( C_Al_Update_Fs_Entry( e ) );
    end Al_Update_Fs_Entry;

    ----------------------------------------------------------------------------

    function Al_Fs_Entry_Exists( e : A_Allegro_Fs_Entry ) return Boolean is

        function C_Al_Fs_Entry_Exists( e : A_Allegro_Fs_Entry ) return Bool;
        pragma Import( C, C_Al_Fs_Entry_Exists, "al_fs_entry_exists" );

    begin
        return To_Ada( C_Al_Fs_Entry_Exists( e ) );
    end Al_Fs_Entry_Exists;

    ----------------------------------------------------------------------------

    function Al_Remove_Fs_Entry( e : A_Allegro_Fs_Entry ) return Boolean is

        function C_Al_Remove_Fs_Entry( e : A_Allegro_Fs_Entry ) return Bool;
        pragma Import( C, C_Al_Remove_Fs_Entry, "al_remove_fs_entry" );

    begin
        return To_Ada( C_Al_Remove_Fs_Entry( e ) );
    end Al_Remove_Fs_Entry;

    ----------------------------------------------------------------------------

    function Al_Filename_Exists( path : String ) return Boolean is

        function C_Al_Filename_Exists( path : C.char_array ) return Bool;
        pragma Import( C, C_Al_Filename_Exists, "al_filename_exists" );

    begin
        return To_Ada( C_Al_Filename_Exists( To_C( path ) ) );
    end Al_Filename_Exists;

    ----------------------------------------------------------------------------

    function Al_Remove_Filename( path : String ) return Boolean is

        function C_Al_Remove_Filename( path : C.char_array ) return Bool;
        pragma Import( C, C_Al_Remove_Filename, "al_remove_filename" );

    begin
        return To_Ada( C_Al_Remove_Filename( To_C( path ) ) );
    end Al_Remove_Filename;

    ----------------------------------------------------------------------------

    function Al_Open_Directory( e : A_Allegro_Fs_Entry ) return Boolean is

        function C_Al_Open_Directory( e : A_Allegro_Fs_Entry ) return Bool;
        pragma Import( C, C_Al_Open_Directory, "al_open_directory" );

    begin
        return To_Ada( C_Al_Open_Directory( e ) );
    end Al_Open_Directory;

    ----------------------------------------------------------------------------

    function Al_Close_Directory( e : A_Allegro_Fs_Entry ) return Boolean is

        function C_Al_Close_Directory( e : A_Allegro_Fs_Entry ) return Bool;
        pragma Import( C, C_Al_Close_Directory, "al_close_directory" );

    begin
        return To_Ada( C_Al_Close_Directory( e ) );
    end Al_Close_Directory;

    ----------------------------------------------------------------------------

    function Al_Get_Current_Directory return String is

        function C_Al_Get_Current_Directory return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Current_Directory, "al_get_current_directory" );

    begin
        return To_Ada( Value( C_Al_Get_Current_Directory ) );
    end Al_Get_Current_Directory;

    ----------------------------------------------------------------------------

    function Al_Change_Directory( path : String ) return Boolean is

        function C_Al_Change_Directory( path : C.char_array ) return Bool;
        pragma Import( C, C_Al_Change_Directory, "al_change_directory" );

    begin
       return To_Ada( C_Al_Change_Directory( To_C( path ) ) );
    end Al_Change_Directory;

    ----------------------------------------------------------------------------

    function Al_Make_Directory( path : String ) return Boolean is

        function C_Al_Make_Directory( path : C.char_array ) return Bool;
        pragma Import( C, C_Al_Make_Directory, "al_make_directory" );

    begin
        return To_Ada( C_Al_Make_Directory( To_C( path ) ) );
    end Al_Make_Directory;

    ----------------------------------------------------------------------------

    function Al_Open_Fs_Entry( e    : A_Allegro_Fs_Entry;
                               mode : String ) return A_Allegro_File is

        function C_Al_Open_Fs_Entry( e    : A_Allegro_Fs_Entry;
                                     mode : C.char_array ) return A_Allegro_File;
        pragma Import( C, C_Al_Open_Fs_Entry, "al_open_fs_entry" );

    begin
        return C_Al_Open_Fs_Entry( e, To_C( mode ) );
    end Al_Open_Fs_Entry;

    ----------------------------------------------------------------------------

    function Al_For_Each_Fs_Entry( dir      : A_Allegro_Fs_Entry;
                                   callback : access function( fs_entry : A_Allegro_Fs_Entry; extra : Address ) return Allegro_For_Each_Fs_Entry_Result;
                                   extra    : Address ) return Allegro_For_Each_Fs_Entry_Result is

        type A_C_Callback is access function( fs_entry : A_Allegro_Fs_Entry; extra : Address ) return Allegro_For_Each_Fs_Entry_Result;
        pragma Convention( C, A_C_Callback );

        function C_Callback( fs_entry : A_Allegro_Fs_Entry; extra : Address ) return Allegro_For_Each_Fs_Entry_Result;
        pragma Convention( C, C_Callback );

        function C_Callback( fs_entry : A_Allegro_Fs_Entry; extra : Address ) return Allegro_For_Each_Fs_Entry_Result is (callback( fs_entry, extra ));

        function C_Al_For_Each_Fs_Entry( dir      : A_Allegro_Fs_Entry;
                                         callback : A_C_Callback;
                                         extra    : Address ) return Allegro_For_Each_Fs_Entry_Result;
        pragma Import( C, C_Al_For_Each_Fs_Entry, "al_for_each_fs_entry" );

    begin
        return C_Al_For_Each_Fs_Entry( dir, C_Callback'Access, extra );
    end Al_For_Each_Fs_Entry;

end Allegro.File_System;
