--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Strings.Unbounded;             use Ada.Strings.Unbounded;
with Allegro.Displays;                  use Allegro.Displays;

-- Allegro 5.2.5 - Clipboard copy/paste routines
package Allegro.Clipboard is

    function Al_Get_Clipboard_Text( display : A_Allegro_Display ) return String;

    function Al_Get_Clipboard_Text( display : A_Allegro_Display ) return Unbounded_String;

    function Al_Set_Clipboard_Text( display : A_Allegro_Display; text : String ) return Boolean;

    procedure Al_Set_Clipboard_Text( display : A_Allegro_Display; text : String );

    function Al_Set_Clipboard_Text( display : A_Allegro_Display; text : Unbounded_String ) return Boolean;

    procedure Al_Set_Clipboard_Text( display : A_Allegro_Display; text : Unbounded_String );

    function Al_Clipboard_Has_Text( display : A_Allegro_Display ) return Boolean;

end Allegro.Clipboard;
