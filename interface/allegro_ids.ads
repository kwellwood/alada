--
-- Copyright (c) 2013-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

-- Support for Allegro AL_ID tags
package Allegro_Ids is

    pragma Pure;

    type AL_ID is private;

    AL_ID_NONE : constant AL_ID;

    function To_AL_ID( a, b, c, d : Character ) return AL_ID with Inline_Always;

    function To_AL_ID( n : Unsigned_32 ) return AL_ID with Inline_Always;

    function To_String( id : AL_ID ) return String;

    function To_Unsigned_32( id : AL_ID ) return Unsigned_32 with Inline_Always;

private

    type AL_ID is new Unsigned_32;

    AL_ID_NONE : constant AL_ID := 0;

end Allegro_Ids;
