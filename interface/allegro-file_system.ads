--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.File_IO;                   use Allegro.File_IO;
with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;
with System;                            use System;

-- Allegro 5.2.5 - File system routines
package Allegro.File_System is

    type Allegro_Fs_Entry is limited private;
    type A_Allegro_Fs_Entry is access all Allegro_Fs_Entry;

    type Allegro_File_Mode is new Unsigned_32;

    ALLEGRO_FILEMODE_READ    : constant Allegro_File_Mode;    -- Readable
    ALLEGRO_FILEMODE_WRITE   : constant Allegro_File_Mode;    -- Writeable
    ALLEGRO_FILEMODE_EXECUTE : constant Allegro_File_Mode;    -- Executable
    ALLEGRO_FILEMODE_HIDDEN  : constant Allegro_File_Mode;    -- Hidden
    ALLEGRO_FILEMODE_ISFILE  : constant Allegro_File_Mode;    -- Regular file
    ALLEGRO_FILEMODE_ISDIR   : constant Allegro_File_Mode;    -- Directory

    function Al_Create_Fs_Entry( path : String ) return A_Allegro_Fs_Entry;

    procedure Al_Destroy_Fs_Entry( e : in out A_Allegro_Fs_Entry );

    function Al_Get_Fs_Entry_Name( e : A_Allegro_Fs_Entry ) return String;

    function Al_Update_Fs_Entry( e : A_Allegro_Fs_Entry ) return Boolean;

    function Al_Get_Fs_Entry_Mode( e : A_Allegro_Fs_Entry ) return Allegro_File_Mode;
    pragma Import( C, Al_Get_Fs_Entry_Mode, "al_get_fs_entry_mode" );

    function Al_Get_Fs_Entry_Atime( e : A_Allegro_Fs_Entry ) return time_t;
    pragma Import( C, Al_Get_Fs_Entry_Atime, "al_get_fs_entry_atime" );

    function Al_Get_Fs_Entry_Ctime( e : A_Allegro_Fs_Entry ) return time_t;
    pragma Import( C, Al_Get_Fs_Entry_Ctime, "al_get_fs_entry_ctime" );

    function Al_Get_Fs_Entry_Mtime( e : A_Allegro_Fs_Entry ) return time_t;
    pragma Import( C, Al_Get_Fs_Entry_Mtime, "al_get_fs_entry_mtime" );

    function Al_Get_Fs_Entry_Size( e : A_Allegro_Fs_Entry ) return off_t;
    pragma Import( C, Al_Get_Fs_Entry_Size, "al_get_fs_entry_size" );

    function Al_Fs_Entry_Exists( e : A_Allegro_Fs_Entry ) return Boolean;

    function Al_Remove_Fs_Entry( e : A_Allegro_Fs_Entry ) return Boolean;

    function Al_Filename_Exists( path : String ) return Boolean;

    function Al_Remove_Filename( path : String ) return Boolean;

    -- Directory functions

    function Al_Open_Directory( e : A_Allegro_Fs_Entry ) return Boolean;

    function Al_Read_Directory( e : A_Allegro_Fs_Entry ) return A_Allegro_Fs_Entry;
    pragma Import( C, Al_Read_Directory, "al_read_directory" );

    function Al_Close_Directory( e : A_Allegro_Fs_Entry ) return Boolean;

    function Al_Get_Current_Directory return String;

    function Al_Change_Directory( path : String ) return Boolean;

    function Al_Make_Directory( path : String ) return Boolean;

    function Al_Open_Fs_Entry( e    : A_Allegro_Fs_Entry;
                               mode : String ) return A_Allegro_File;

    -- Alternative filesystem functions

    type fs_create_entry is access function( path : C.char_array ) return A_Allegro_Fs_Entry;
    type fs_destroy_entry is access procedure( e : A_Allegro_Fs_Entry );
    type fs_entry_name is access function( e : A_Allegro_Fs_Entry ) return C.Strings.chars_ptr;
    type fs_update_entry is access function( e : A_Allegro_Fs_Entry ) return Bool;
    type fs_entry_mode is access function( e : A_Allegro_Fs_Entry ) return Allegro_File_Mode;
    type fs_entry_atime is access function( e : A_Allegro_Fs_Entry ) return time_t;
    type fs_entry_mtime is access function( e : A_Allegro_Fs_Entry ) return time_t;
    type fs_entry_ctime is access function( e : A_Allegro_Fs_Entry ) return time_t;
    type fs_entry_size is access function( e : A_Allegro_Fs_Entry ) return off_t;
    type fs_entry_exists is access function( e : A_Allegro_Fs_Entry ) return Bool;
    type fs_remove_entry is access function( e : A_Allegro_Fs_Entry ) return Bool;

    type fs_open_directory is access function( e : A_Allegro_Fs_Entry ) return Bool;
    type fs_read_directory is access function( e : A_Allegro_Fs_Entry ) return A_Allegro_Fs_Entry;
    type fs_close_directory is access function( e : A_Allegro_Fs_Entry ) return Bool;

    type fs_filename_exists is access function( path : C.char_array ) return Bool;
    type fs_remove_filename is access function( path : C.char_array ) return Bool;
    type fs_get_current_directory is access function return C.Strings.chars_ptr;
    type fs_change_directory is access function( path : C.char_array ) return Bool;
    type fs_make_directory is access function( path : C.char_array ) return Bool;

    type fs_open_file is access function( e    : A_Allegro_Fs_Entry;
                                          mode : C.char_array ) return A_Allegro_File;

    pragma Convention( C, fs_create_entry );
    pragma Convention( C, fs_destroy_entry );
    pragma Convention( C, fs_entry_name );
    pragma Convention( C, fs_update_entry );
    pragma Convention( C, fs_entry_mode );
    pragma Convention( C, fs_entry_atime );
    pragma Convention( C, fs_entry_mtime );
    pragma Convention( C, fs_entry_ctime );
    pragma Convention( C, fs_entry_size );
    pragma Convention( C, fs_entry_exists );
    pragma Convention( C, fs_remove_entry );
    pragma Convention( C, fs_open_directory );
    pragma Convention( C, fs_read_directory );
    pragma Convention( C, fs_close_directory );
    pragma Convention( C, fs_filename_exists );
    pragma Convention( C, fs_remove_filename );
    pragma Convention( C, fs_get_current_directory );
    pragma Convention( C, fs_change_directory );
    pragma Convention( C, fs_make_directory );
    pragma Convention( C, fs_open_file );

    type Allegro_Fs_Interface is
        record
            create_entry          : fs_create_entry;
            destroy_entry         : fs_destroy_entry;
            entry_name            : fs_entry_name;
            update_entry          : fs_update_entry;
            entry_mode            : fs_entry_mode;
            entry_atime           : fs_entry_atime;
            entry_mtime           : fs_entry_mtime;
            entry_ctime           : fs_entry_ctime;
            entry_size            : fs_entry_size;
            entry_exists          : fs_entry_exists;
            remove_entry          : fs_entry_size;

            open_directory        : fs_open_directory;
            read_directory        : fs_read_directory;
            close_directory       : fs_close_directory;

            filename_exists       : fs_filename_exists;
            remove_filename       : fs_remove_filename;
            get_current_directory : fs_get_current_directory;
            change_directory      : fs_change_directory;
            make_directory        : fs_make_directory;

            open_file             : fs_open_file;
        end record;
    pragma Convention( C, Allegro_Fs_Interface );
    type A_Allegro_Fs_Interface is access all Allegro_Fs_Interface;

    -- Helper function for iterating over a directory using a callback.

    type Allegro_For_Each_Fs_Entry_Result is private;
    ALLEGRO_FOR_EACH_FS_ENTRY_ERROR : constant Allegro_For_Each_Fs_Entry_Result;
    ALLEGRO_FOR_EACH_FS_ENTRY_OK    : constant Allegro_For_Each_Fs_Entry_Result;
    ALLEGRO_FOR_EACH_FS_ENTRY_SKIP  : constant Allegro_For_Each_Fs_Entry_Result;
    ALLEGRO_FOR_EACH_FS_ENTRY_STOP  : constant Allegro_For_Each_Fs_Entry_Result;

    function Al_For_Each_Fs_Entry( dir      : A_Allegro_Fs_Entry;
                                   callback : access function( fs_entry : A_Allegro_Fs_Entry; extra : Address ) return Allegro_For_Each_Fs_Entry_Result;
                                   extra    : Address ) return Allegro_For_Each_Fs_Entry_Result;

    procedure Al_Set_Fs_Interface( vtable : A_Allegro_Fs_Interface );
    pragma Import( C, Al_Set_Fs_Interface, "al_set_fs_interface" );

    procedure Al_Set_Standard_Fs_Interface;
    pragma Import( C, Al_Set_Standard_Fs_Interface, "al_set_standard_fs_interface" );

    function Al_Get_Fs_Interface return A_Allegro_Fs_Interface;
    pragma Import( C, Al_Get_Fs_Interface, "al_get_fs_interface" );

private

    type Allegro_FS_Entry is limited null record;

    ALLEGRO_FILEMODE_READ    : constant Allegro_File_Mode := 1;
    ALLEGRO_FILEMODE_WRITE   : constant Allegro_File_Mode := 2;
    ALLEGRO_FILEMODE_EXECUTE : constant Allegro_File_Mode := 4;
    ALLEGRO_FILEMODE_HIDDEN  : constant Allegro_File_Mode := 8;
    ALLEGRO_FILEMODE_ISFILE  : constant Allegro_File_Mode := 16;
    ALLEGRO_FILEMODE_ISDIR   : constant Allegro_File_Mode := 32;

    type Allegro_For_Each_Fs_Entry_Result is new Integer;
    ALLEGRO_FOR_EACH_FS_ENTRY_ERROR : constant Allegro_For_Each_Fs_Entry_Result := -1;
    ALLEGRO_FOR_EACH_FS_ENTRY_OK    : constant Allegro_For_Each_Fs_Entry_Result :=  0;
    ALLEGRO_FOR_EACH_FS_ENTRY_SKIP  : constant Allegro_For_Each_Fs_Entry_Result :=  1;
    ALLEGRO_FOR_EACH_FS_ENTRY_STOP  : constant Allegro_For_Each_Fs_Entry_Result :=  2;

end Allegro.File_System;
