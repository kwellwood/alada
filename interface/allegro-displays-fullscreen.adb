--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Displays.Fullscreen is

    procedure Al_Get_Display_Mode( index   : Integer;
                                   mode    : in out Allegro_Display_Mode;
                                   success : out Boolean ) is
    begin
        success := Al_Get_Display_Mode( index, mode'Unrestricted_Access ) /= null;
    end Al_Get_Display_Mode;

    ----------------------------------------------------------------------------

    procedure Al_Get_Display_Mode( index : Integer;
                                   mode  : in out Allegro_Display_Mode ) is
        success : Boolean;
        pragma Warnings( Off, success );
    begin
        success := Al_Get_Display_Mode( index, mode'Unrestricted_Access ) /= null;
    end Al_Get_Display_Mode;

end Allegro.Displays.Fullscreen;
