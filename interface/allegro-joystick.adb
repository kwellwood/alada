--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

package body Allegro.Joystick is

    function Al_Install_Joystick return Boolean is

        function C_Al_Install_Joystick return Bool;
        pragma Import( C, C_Al_Install_Joystick, "al_install_joystick" );

    begin
        return To_Ada( C_Al_Install_Joystick );
    end Al_Install_Joystick;

    ----------------------------------------------------------------------------

    function Al_Is_Joystick_Installed return Boolean is

        function C_Al_Is_Joystick_Installed return Bool;
        pragma Import( C, C_Al_Is_Joystick_Installed, "al_is_joystick_installed" );

    begin
        return To_Ada( C_Al_Is_Joystick_Installed );
    end Al_Is_Joystick_Installed;

    ----------------------------------------------------------------------------

    function Al_Reconfigure_Joysticks return Boolean is

        function C_Al_Reconfigure_Joysticks return Bool;
        pragma Import( C, C_Al_Reconfigure_Joysticks, "al_reconfigure_joysticks" );

    begin
        return To_Ada( C_Al_Reconfigure_Joysticks );
    end Al_Reconfigure_Joysticks;

    ----------------------------------------------------------------------------

    function Al_Get_Joystick_Active( joy : A_Allegro_Joystick ) return Boolean is

        function C_Al_Get_Joystick_Active( joy : A_Allegro_Joystick ) return Bool;
        pragma Import( C, C_Al_Get_Joystick_Active, "al_get_joystick_active" );

    begin
        return To_Ada( C_Al_Get_Joystick_Active( joy ) );
    end Al_Get_Joystick_Active;

    ----------------------------------------------------------------------------

    function Al_Get_Joystick_Name( joy : A_Allegro_Joystick ) return String is

        function C_Al_Get_Joystick_Name( joy : A_Allegro_Joystick ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Joystick_Name, "al_get_joystick_name" );

    begin
        return To_Ada( Value( C_Al_Get_Joystick_Name( joy ) ) );
    end Al_Get_Joystick_Name;

    ----------------------------------------------------------------------------

    function Al_Get_Joystick_Stick_Name( joy   : A_Allegro_Joystick;
                                         stick : Integer ) return String is

        function C_Al_Get_Joystick_Stick_Name( joy   : A_Allegro_Joystick;
                                               stick : Integer ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Joystick_Stick_Name, "al_get_joystick_stick_name" );

    begin
        return To_Ada( Value( C_Al_Get_Joystick_Stick_Name( joy, stick ) ) );
    end Al_Get_Joystick_Stick_Name;

    ----------------------------------------------------------------------------

    function Al_Get_Joystick_Axis_Name( joy   : A_Allegro_Joystick;
                                        stick : Integer;
                                        axis  : Integer ) return String is

        function C_Al_Get_Joystick_Axis_Name( joy   : A_Allegro_Joystick;
                                               stick : Integer;
                                               axis  : Integer ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Joystick_Axis_Name, "al_get_joystick_axis_name" );

    begin
        return To_Ada( Value( C_Al_Get_Joystick_Axis_Name( joy, stick, axis ) ) );
    end Al_Get_Joystick_Axis_Name;

    ----------------------------------------------------------------------------

    function Al_Get_Joystick_Button_Name( joy    : A_Allegro_Joystick;
                                          button : Integer ) return String is

        function C_Al_Get_Joystick_Button_Name( joy    : A_Allegro_Joystick;
                                                button : Integer ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Joystick_Button_Name, "al_get_joystick_button_name" );

    begin
        return To_Ada( Value( C_Al_Get_Joystick_Button_Name( joy, button ) ) );
    end Al_Get_Joystick_Button_Name;

end Allegro.Joystick;
