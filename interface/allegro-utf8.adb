--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with System;                            use System;

package body Allegro.UTF8 is

    function Al_Ustr_New( s : String ) return A_Allegro_Ustr is

        function C_Al_Ustr_New( s : C.char_array ) return A_Allegro_Ustr;
        pragma Import( C, C_Al_Ustr_New, "al_ustr_new" );

    begin
        return C_Al_Ustr_New( To_C( s ) );
    end Al_Ustr_New;

    ----------------------------------------------------------------------------

    function Al_Ustr_New_From_Buffer( s : String ) return A_Allegro_Ustr is

        function C_Al_Ustr_New_From_Buffer( s    : C.char_array;
                                            size : C.size_t ) return A_Allegro_Ustr;
        pragma Import( C, C_Al_Ustr_New_From_Buffer, "al_ustr_new_from_buffer" );

    begin
        return C_Al_Ustr_New_From_Buffer( To_C( s ), C.size_t(s'Length) );
    end Al_Ustr_New_From_Buffer;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Free( us : in out A_Allegro_Ustr ) is

        procedure C_Al_Ustr_Free( us : A_Allegro_Ustr );
        pragma Import( C, C_Al_Ustr_Free, "al_ustr_free" );

    begin
        if us /= null then
            C_Al_Ustr_Free( us );
            us := null;
        end if;
    end Al_Ustr_Free;

    ----------------------------------------------------------------------------

    function Al_Cstr( us : A_Allegro_Ustr ) return String is

        function C_Al_Cstr( us : A_Allegro_Ustr ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Cstr, "al_cstr" );

    begin
        return To_Ada( Value( C_Al_Cstr( us ) ) );
    end Al_Cstr;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_To_Buffer( us     : A_Allegro_Ustr;
                                 buffer : out String ) is

        procedure C_Al_Ustr_To_Buffer( us     : A_Allegro_Ustr;
                                       buffer : Address;
                                       size   : C.size_t );
        pragma Import( C, C_Al_Ustr_To_Buffer, "al_ustr_to_buffer" );

    begin
        C_Al_Ustr_To_Buffer( us, buffer(buffer'First)'Address, C.size_t(buffer'Length) );
    end Al_Ustr_To_Buffer;

    ----------------------------------------------------------------------------

    function Al_Cstr_Dup( us : A_Allegro_Ustr ) return String is

        function C_Al_Cstr_Dup( us : A_Allegro_Ustr ) return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Cstr_Dup, "al_cstr_dup" );

    begin
        return To_Ada( Value( C_Al_Cstr_Dup( us ) ) );
    end Al_Cstr_Dup;

    ----------------------------------------------------------------------------

    function Al_Ref_Cstr( info : access Allegro_Ustr_Info;
                          s    : String ) return A_Allegro_Ustr is

        function C_Al_Ref_Cstr( info : access Allegro_Ustr_Info;
                                s    : C.char_array ) return A_Allegro_Ustr;
        pragma Import( C, C_Al_Ref_Cstr, "al_ref_cstr" );

    begin
        return C_Al_Ref_Cstr( info, To_C( s ) );
    end Al_Ref_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ref_Buffer( info : access Allegro_Ustr_Info;
                            s    : String ) return A_Allegro_Ustr is

        function C_Al_Ref_Buffer( info : access Allegro_Ustr_Info;
                                  s    : Address;
                                  size : C.size_t ) return A_Allegro_Ustr;
        pragma Import( C, C_Al_Ref_Buffer, "al_ref_buffer" );

    begin
        return C_Al_Ref_Buffer( info, s(s'First)'Address, C.size_t(s'Length) );
    end Al_Ref_Buffer;

    ----------------------------------------------------------------------------

    function Al_Ustr_Next( us  : A_Allegro_Ustr;
                           pos : access Integer ) return Boolean is

        function C_Al_Ustr_Next( us  : A_Allegro_Ustr;
                                 pos : access Integer ) return Bool;
        pragma Import( C, C_Al_Ustr_Next, "al_ustr_next" );

    begin
        return To_Ada( C_Al_Ustr_Next( us, pos ) );
    end Al_Ustr_Next;

    ----------------------------------------------------------------------------

    function Al_Ustr_Prev( us  : A_Allegro_Ustr;
                           pos : access Integer ) return Boolean is

        function C_Al_Ustr_Prev( us  : A_Allegro_Ustr;
                                 pos : access Integer ) return Bool;
        pragma Import( C, C_Al_Ustr_Prev, "al_ustr_prev" );

    begin
        return To_Ada( C_Al_Ustr_Prev( us, pos ) );
    end Al_Ustr_Prev;

    ----------------------------------------------------------------------------

    function Al_Ustr_Insert( us1 : A_Allegro_Ustr;
                             pos : Integer;
                             us2 : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Insert( us1 : A_Allegro_Ustr;
                                   pos : Integer;
                                   us2 : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Insert, "al_ustr_insert" );

    begin
        return To_Ada( C_Al_Ustr_Insert( us1, pos, us2 ) );
    end Al_Ustr_Insert;

    ----------------------------------------------------------------------------

    function Al_Ustr_Insert_Cstr( us  : A_Allegro_Ustr;
                                  pos : Integer;
                                  us2 : String ) return Boolean is

        function C_Al_Ustr_Insert_Cstr( us  : A_Allegro_Ustr;
                                        pos : Integer;
                                        us2 : C.char_array ) return Bool;
        pragma Import( C, C_Al_Ustr_Insert_Cstr, "al_ustr_insert_cstr" );

    begin
        return To_Ada( C_Al_Ustr_Insert_Cstr( us, pos, To_C( us2 ) ) );
    end Al_Ustr_Insert_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Append( us1, us2 : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Append( us1, us2 : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Append, "al_ustr_append" );

    begin
        return To_Ada( C_Al_Ustr_Append( us1, us2 ) );
    end Al_Ustr_Append;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Append( us1, us2 : A_Allegro_Ustr ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Ustr_Append( us1, us2 );
    end Al_Ustr_Append;

    ----------------------------------------------------------------------------

    function Al_Ustr_Append_Cstr( us : A_Allegro_Ustr;
                                  s  : String ) return Boolean is

        function C_Al_Ustr_Append_Cstr( us : A_Allegro_Ustr;
                                        s  : C.char_array ) return Bool;
        pragma Import( C, C_Al_Ustr_Append_Cstr, "al_ustr_append_cstr" );

    begin
        return To_Ada( C_Al_Ustr_Append_Cstr( us, To_C( s ) ) );
    end Al_Ustr_Append_Cstr;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Append_Cstr( us : A_Allegro_Ustr; s : String ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Ustr_Append_Cstr( us, s );
    end Al_Ustr_Append_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Remove_Chr( us  : A_Allegro_Ustr;
                                 pos : Integer ) return Boolean is

        function C_Al_Ustr_Remove_Chr( us  : A_Allegro_Ustr;
                                       pos : Integer ) return Bool;
        pragma Import( C, C_Al_Ustr_Remove_Chr, "al_ustr_remove_chr" );

    begin
        return To_Ada( C_Al_Ustr_Remove_Chr( us, pos ) );
    end Al_Ustr_Remove_Chr;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Remove_Chr( us : A_Allegro_Ustr; pos : Integer ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Ustr_Remove_Chr( us, pos );
    end Al_Ustr_Remove_Chr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Remove_Range( us        : A_Allegro_Ustr;
                                   start_pos,
                                   end_pos   : Integer ) return Boolean is

        function C_Al_Ustr_Remove_Range( us        : A_Allegro_Ustr;
                                         start_pos,
                                         end_pos   : Integer ) return Bool;
        pragma Import( C, C_Al_Ustr_Remove_Range, "al_ustr_remove_range" );

    begin
        return To_Ada( C_Al_Ustr_Remove_Range( us, start_pos, end_pos ) );
    end Al_Ustr_Remove_Range;

    ----------------------------------------------------------------------------

    function Al_Ustr_Truncate( us        : A_Allegro_Ustr;
                               start_pos : Integer ) return Boolean is

        function C_Al_Ustr_Truncate( us        : A_Allegro_Ustr;
                                     start_pos : Integer ) return Bool;
        pragma Import( C, C_Al_Ustr_Truncate, "al_ustr_truncate" );

    begin
        return To_Ada( C_Al_Ustr_Truncate( us, start_pos ) );
    end Al_Ustr_Truncate;

    ----------------------------------------------------------------------------

    function Al_Ustr_Ltrim_Ws( us : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Ltrim_Ws( us : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Ltrim_Ws, "al_ustr_ltrim_ws" );

    begin
        return To_Ada( C_Al_Ustr_Ltrim_Ws( us ) );
    end Al_Ustr_Ltrim_Ws;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Ltrim_Ws( us : A_Allegro_Ustr ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Ustr_Ltrim_Ws( us );
    end Al_Ustr_Ltrim_Ws;

    ----------------------------------------------------------------------------

    function Al_Ustr_Rtrim_Ws( us : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Rtrim_Ws( us : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Rtrim_Ws, "al_ustr_rtrim_ws" );

    begin
        return To_Ada( C_Al_Ustr_Rtrim_Ws( us ) );
    end Al_Ustr_Rtrim_Ws;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Rtrim_Ws( us : A_Allegro_Ustr ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Ustr_Rtrim_Ws( us );
    end Al_Ustr_Rtrim_Ws;

    ----------------------------------------------------------------------------

    function Al_Ustr_Trim_Ws( us : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Trim_Ws( us : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Trim_Ws, "al_ustr_trim_ws" );

    begin
        return To_Ada( C_Al_Ustr_Trim_Ws( us ) );
    end Al_Ustr_Trim_Ws;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Trim_Ws( us : A_Allegro_Ustr ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Ustr_Trim_Ws( us );
    end Al_Ustr_Trim_Ws;

    ----------------------------------------------------------------------------

    function Al_Ustr_Assign( us1, us2 : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Assign( us1, us2 : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Assign, "al_ustr_assign" );

    begin
        return To_Ada( C_Al_Ustr_Assign( us1, us2 ) );
    end Al_Ustr_Assign;

    ----------------------------------------------------------------------------

    function Al_Ustr_Assign_Substr( us1, us2  : A_Allegro_Ustr;
                                    start_pos,
                                    end_pos   : Integer ) return Boolean is

        function C_Al_Ustr_Assign_Substr( us1, us2  : A_Allegro_Ustr;
                                          start_pos,
                                          end_pos   : Integer ) return Bool;
        pragma Import( C, C_Al_Ustr_Assign_Substr, "al_ustr_assign_substr" );

    begin
        return To_Ada( C_Al_Ustr_Assign_Substr( us1, us2, start_pos, end_pos ) );
    end Al_Ustr_Assign_Substr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Assign_Cstr( us1 : A_Allegro_Ustr; s : String ) return Boolean is

        function C_Al_Ustr_Assign_Cstr( us1 : A_Allegro_Ustr;
                                        s   : C.char_array ) return Bool;
        pragma Import( C, C_Al_Ustr_Assign_Cstr, "al_ustr_assign_cstr" );

    begin
        return To_Ada( C_Al_Ustr_Assign_Cstr( us1, To_C( s ) ) );
    end Al_Ustr_Assign_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Replace_Range( us1        : A_Allegro_Ustr;
                                    start_pos1,
                                    end_pos1   : Integer;
                                    us2        : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Replace_Range( us1        : A_Allegro_Ustr;
                                          start_pos1,
                                          end_pos1   : Integer;
                                          us2        : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Replace_Range, "al_ustr_replace_range" );

    begin
        return To_Ada( C_Al_Ustr_Replace_Range( us1, start_pos1, end_pos1, us2 ) );
    end Al_Ustr_Replace_Range;

    ----------------------------------------------------------------------------

    function Al_Ustr_Find_Set_Cstr( us         : A_Allegro_Ustr;
                                    start_pos  : Integer;
                                    acceptChrs : String ) return Integer is

        function C_Al_Ustr_Find_Set_Cstr( us         : A_Allegro_Ustr;
                                          start_pos  : Integer;
                                          acceptChrs : C.char_array ) return Integer;
        pragma Import( C, C_Al_Ustr_Find_Set_Cstr, "al_ustr_find_set_cstr" );

    begin
        return C_Al_Ustr_Find_Set_Cstr( us, start_pos, To_C( acceptChrs ) );
    end Al_Ustr_Find_Set_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Find_Cset_Cstr( us        : A_Allegro_Ustr;
                                     start_pos : Integer;
                                     reject    : String ) return Integer is

        function C_Al_Ustr_Find_Cset_Cstr( us        : A_Allegro_Ustr;
                                           start_pos : Integer;
                                           reject    : C.char_array ) return Integer;
        pragma Import( C, C_Al_Ustr_Find_Cset_Cstr, "al_ustr_find_cset_cstr" );

    begin
        return C_Al_Ustr_Find_Cset_Cstr( us, start_pos, To_C( reject ) );
    end Al_Ustr_Find_Cset_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Find_Cstr( haystack  : A_Allegro_Ustr;
                                start_pos : Integer;
                                needle    : String ) return Integer is

        function C_Al_Ustr_Find_Cstr( haystack  : A_Allegro_Ustr;
                                      start_pos : Integer;
                                      needle    : C.char_array ) return Integer;
        pragma Import( C, C_Al_Ustr_Find_Cstr, "al_ustr_find_cstr" );

    begin
        return C_Al_Ustr_Find_Cstr( haystack, start_pos, To_C( needle ) );
    end Al_Ustr_Find_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Rfind_Cstr( haystack  : A_Allegro_Ustr;
                                 start_pos : Integer;
                                 needle    : String ) return Integer is

        function C_Al_Ustr_Rfind_Cstr( haystack  : A_Allegro_Ustr;
                                       start_pos : Integer;
                                       needle    : C.char_array ) return Integer;
        pragma Import( C, C_Al_Ustr_Rfind_Cstr, "al_ustr_rfind_cstr" );

    begin
        return C_Al_Ustr_Rfind_Cstr( haystack, start_pos, To_C( needle ) );
    end Al_Ustr_Rfind_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Find_Replace( us        : A_Allegro_Ustr;
                                   start_pos : Integer;
                                   find      : A_Allegro_Ustr;
                                   replace   : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Find_Replace( us        : A_Allegro_Ustr;
                                         start_pos : Integer;
                                         find      : A_Allegro_Ustr;
                                         replace   : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Find_Replace, "al_ustr_find_replace" );

    begin
        return To_Ada( C_Al_Ustr_Find_Replace( us, start_pos, find, replace ) );
    end Al_Ustr_Find_Replace;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Find_Replace( us        : A_Allegro_Ustr;
                                    start_pos : Integer;
                                    find      : A_Allegro_Ustr;
                                    replace   : A_Allegro_Ustr ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Ustr_Find_Replace( us, start_pos, find, replace );
    end Al_Ustr_Find_Replace;

    ----------------------------------------------------------------------------

    function Al_Ustr_Find_Replace_Cstr( us        : A_Allegro_Ustr;
                                        start_pos : Integer;
                                        find      : String;
                                        replace   : String ) return Boolean is

        function C_Al_Ustr_Find_Replace_Cstr( us        : A_Allegro_Ustr;
                                            start_pos : Integer;
                                            find      : C.char_array;
                                            replace   : C.char_array ) return Bool;
        pragma Import( C, C_Al_Ustr_Find_Replace_Cstr, "al_ustr_find_replace_cstr" );

    begin
        return To_Ada( C_Al_Ustr_Find_Replace_Cstr( us, start_pos,
                                                    To_C( find ), To_C( replace ) ) );
    end Al_Ustr_Find_Replace_Cstr;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Find_Replace_Cstr( us        : A_Allegro_Ustr;
                                         start_pos : Integer;
                                         find      : String;
                                         replace   : String ) is
        ignored : Boolean;
        pragma Warnings( Off, ignored );
    begin
        ignored := Al_Ustr_Find_Replace_Cstr( us, start_pos, find, replace );
    end Al_Ustr_Find_Replace_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Equal( us1, us2 : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Equal( us1, us2 : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Equal, "al_ustr_equal" );

    begin
        return To_Ada( C_Al_Ustr_Equal( us1, us2 ) );
    end Al_Ustr_Equal;

    ----------------------------------------------------------------------------

    function Al_Ustr_Has_Prefix( u, v : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Has_Prefix( u, v : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Has_Prefix, "al_ustr_has_prefix" );

    begin
        return To_Ada( C_Al_Ustr_Has_Prefix( u, v ) );
    end Al_Ustr_Has_Prefix;

    ----------------------------------------------------------------------------

    function Al_Ustr_Has_Prefix_Cstr( u : A_Allegro_Ustr;
                                      s : String ) return Boolean is

        function C_Al_Ustr_Has_Prefix_Cstr( u : A_Allegro_Ustr;
                                            s : C.char_array ) return Bool;
        pragma Import( C, C_Al_Ustr_Has_Prefix_Cstr, "al_ustr_has_prefix_cstr" );

    begin
        return To_Ada( C_Al_Ustr_Has_Prefix_Cstr( u, To_C( s ) ) );
    end Al_Ustr_Has_Prefix_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_Has_Suffix( u, v : A_Allegro_Ustr ) return Boolean is

        function C_Al_Ustr_Has_Suffix( u, v : A_Allegro_Ustr ) return Bool;
        pragma Import( C, C_Al_Ustr_Has_Suffix, "al_ustr_has_suffix" );

    begin
        return To_Ada( C_Al_Ustr_Has_Suffix( u, v ) );
    end Al_Ustr_Has_Suffix;

    ----------------------------------------------------------------------------

    function Al_Ustr_Has_Suffix_Cstr( us1 : A_Allegro_Ustr;
                                      s   : String ) return Boolean is

        function C_Al_Ustr_Has_Suffix_Cstr( us1 : A_Allegro_Ustr;
                                            s   : C.char_array ) return Bool;
        pragma Import( C, C_Al_Ustr_Has_Suffix_Cstr, "al_ustr_has_suffix_cstr" );

    begin
        return To_Ada( C_Al_Ustr_Has_Suffix_Cstr( us1, To_C( s ) ) );
    end Al_Ustr_Has_Suffix_Cstr;

    ----------------------------------------------------------------------------

    function Al_Ustr_New_From_Utf16( s : Wide_String ) return A_Allegro_Ustr is

        function C_Al_Ustr_New_From_Utf16( s : Address ) return A_Allegro_Ustr;
        pragma Import( C, C_Al_Ustr_New_From_Utf16, "al_ustr_new_from_utf16" );

    begin
        return C_Al_Ustr_New_From_Utf16( s(s'First)'Address );
    end Al_Ustr_New_From_Utf16;

    ----------------------------------------------------------------------------

    procedure Al_Ustr_Encode_Utf16( us   : A_Allegro_Ustr;
                                    s    : out Wide_String;
                                    size : out C.size_t ) is

        -- 'n' and the return value are both in bytes
        function C_Al_Ustr_Encode_Utf16( us : A_Allegro_Ustr;
                                         s  : Address;
                                         n  : C.size_t ) return C.size_t;
        pragma Import( C, C_Al_Ustr_Encode_Utf16, "al_ustr_encode_utf16" );

    begin
        size := C_Al_Ustr_Encode_Utf16( us, s(s'First)'Address, s'Length * 2 );
    end Al_Ustr_Encode_Utf16;

    ----------------------------------------------------------------------------

    procedure Al_Utf8_Encode( chr  : Integer_32;
                              s    : out String;
                              size : out C.size_t ) is

        function C_Al_Utf8_Encode( s : Address; chr : Integer_32 ) return C.size_t;
        pragma Import( C, C_Al_Utf8_Encode, "al_utf8_encode" );

    begin
        if s'Length = 4 then
            size := C_Al_Utf8_Encode( s(s'First)'Address, chr );
        else
            size := 0;
        end if;
    end Al_Utf8_Encode;

    ----------------------------------------------------------------------------

    procedure Al_Utf16_Encode( chr  : Integer_32;
                               s    : out Wide_String;
                               size : out C.size_t ) is

        function C_Al_Utf16_Encode( s : Address; chr : Integer_32 ) return C.size_t;
        pragma Import( C, C_Al_Utf16_Encode, "al_utf16_encode" );

    begin
        if s'Length = 2 then
            size := C_Al_Utf16_Encode( s(s'First)'Address, chr );
        else
            size := 0;
        end if;
    end Al_Utf16_Encode;

end Allegro.UTF8;
