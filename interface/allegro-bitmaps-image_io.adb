--
-- Copyright (c) 2013-2016 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

package body Allegro.Bitmaps.Image_IO is

    function Al_Register_Bitmap_Loader( ext    : String;
                                        loader : Allegro_IIO_Loader_Function ) return Boolean is

        function C_Al_Register_Bitmap_Loader( ext    : C.char_array;
                                              loader : Allegro_IIO_Loader_Function ) return Bool;
        pragma Import( C, C_Al_Register_Bitmap_Loader, "al_register_bitmap_loader" );

    begin
        return To_Ada( C_Al_Register_Bitmap_Loader( To_C( ext ), loader ) );
    end Al_Register_Bitmap_Loader;

    ----------------------------------------------------------------------------

    function Al_Register_Bitmap_Saver( ext   : String;
                                       saver : Allegro_IIO_Saver_Function ) return Boolean is

        function C_Al_Register_Bitmap_Saver( ext   : C.char_array;
                                             saver : Allegro_IIO_Saver_Function ) return Bool;
        pragma Import( C, C_Al_Register_Bitmap_Saver, "al_register_bitmap_saver" );

    begin
        return To_Ada( C_Al_Register_Bitmap_Saver( To_C( ext ), saver ) );
    end Al_Register_Bitmap_Saver;

    ----------------------------------------------------------------------------

    function Al_Register_Bitmap_Loader_f( ext       : String;
                                          fs_loader : Allegro_IIO_FS_Loader_Function ) return Boolean is

        function C_Al_Register_Bitmap_Loader_f( ext       : C.char_array;
                                                fs_loader : Allegro_IIO_FS_Loader_Function ) return Bool;
        pragma Import( C, C_Al_Register_Bitmap_Loader_f, "al_register_bitmap_loader_f" );

    begin
        return To_Ada( C_Al_Register_Bitmap_Loader_f( To_C( ext ), fs_loader ) );
    end Al_Register_Bitmap_Loader_f;

    ----------------------------------------------------------------------------

    function Al_Register_Bitmap_Saver_f( ext      : String;
                                         fs_saver : Allegro_IIO_FS_Saver_Function ) return Boolean is

        function C_Al_Register_Bitmap_Saver_f( ext      : C.char_array;
                                               fs_saver : Allegro_IIO_FS_Saver_Function ) return Bool;
        pragma Import( C, C_Al_Register_Bitmap_Saver_f, "al_register_bitmap_saver_f" );

    begin
        return To_Ada( C_Al_Register_Bitmap_Saver_f( To_C( ext ), fs_saver ) );
    end Al_Register_Bitmap_Saver_f;

    ----------------------------------------------------------------------------

    function Al_Register_Bitmap_Identifier( ext        : String;
                                            identifier : Allegro_IIO_Identifier_Function ) return Boolean is

        function C_Al_Register_Bitmap_Identifier( ext        : C.char_array;
                                                  identifier : Allegro_IIO_Identifier_Function ) return Bool;
        pragma Import( C, C_Al_Register_Bitmap_Identifier, "al_register_bitmap_identifier" );

    begin
        return To_Ada( C_Al_Register_Bitmap_Identifier( To_C( ext ), identifier ) );
    end Al_Register_Bitmap_Identifier;

    ----------------------------------------------------------------------------

    function Al_Load_Bitmap( filename : String ) return A_Allegro_Bitmap is

       function C_Al_Load_Bitmap( filename : C.char_array ) return A_Allegro_Bitmap;
        pragma Import( C, C_Al_Load_Bitmap, "al_load_bitmap" );

    begin
        return C_Al_Load_Bitmap( To_C( filename ) );
    end Al_Load_Bitmap;

    ----------------------------------------------------------------------------

    function Al_Load_Bitmap_Flags( filename : String; flags : Bitmap_Loader_Flags ) return A_Allegro_Bitmap is

        function C_Al_Load_Bitmap_Flags( filename : C.char_array; flags : Bitmap_Loader_Flags ) return A_Allegro_Bitmap;
        pragma Import( C, C_Al_Load_Bitmap_Flags, "al_load_bitmap_flags" );

    begin
        return C_Al_Load_Bitmap_Flags( To_C( filename ), flags );
    end Al_Load_Bitmap_Flags;

    ----------------------------------------------------------------------------

    function Al_Load_Bitmap_f( fp : A_Allegro_File; ident : String ) return A_Allegro_Bitmap is

        function C_Al_Load_Bitmap_f( fp    : A_Allegro_File;
                                     ident : C.char_array ) return A_Allegro_Bitmap;
        pragma Import( C, C_Al_Load_Bitmap_f, "al_load_bitmap_f" );

    begin
        return C_Al_Load_Bitmap_f( fp, To_C( ident ) );
    end Al_Load_Bitmap_f;

    ----------------------------------------------------------------------------

    function Al_Load_Bitmap_Flags_f( fp    : A_Allegro_File;
                                     ident : String;
                                     flags : Bitmap_Loader_Flags ) return A_Allegro_Bitmap is

        function C_Al_Load_Bitmap_Flags_f( fp    : A_Allegro_File;
                                           ident : C.char_array;
                                           flags : Bitmap_Loader_Flags ) return A_Allegro_Bitmap;
        pragma Import( C, C_Al_Load_Bitmap_Flags_f, "al_load_bitmap_flags_f" );

    begin
        return C_Al_Load_Bitmap_Flags_f( fp, To_C( ident ), flags );
    end Al_Load_Bitmap_Flags_f;

    ----------------------------------------------------------------------------

    function Al_Save_Bitmap( filename : String; bitmap : A_Allegro_Bitmap ) return Boolean is

        function C_Al_Save_Bitmap( filename : C.char_array;
                                   bitmap   : A_Allegro_Bitmap ) return Bool;
        pragma Import( C, C_Al_Save_Bitmap, "al_save_bitmap" );

    begin
        return To_Ada( C_Al_Save_Bitmap( To_C( filename ), bitmap ) );
    end Al_Save_Bitmap;

    ----------------------------------------------------------------------------

    procedure Al_Save_Bitmap( filename : String; bitmap : A_Allegro_Bitmap ) is
        ok : Boolean;
        pragma Warnings( Off, ok );
    begin
        ok := Al_Save_Bitmap( filename, bitmap );
    end Al_Save_Bitmap;

    ----------------------------------------------------------------------------

    function Al_Save_Bitmap_f( fp     : A_Allegro_File;
                               ident  : String;
                               bitmap : A_Allegro_Bitmap ) return Boolean is

        function C_Al_Save_Bitmap_f( fp     : A_Allegro_File;
                                     ident  : C.char_array;
                                     bitmap : A_Allegro_Bitmap ) return Bool;
        pragma Import( C, C_Al_Save_Bitmap_f, "al_save_bitmap_f" );

    begin
        return To_Ada( C_Al_Save_Bitmap_f( fp, To_C( ident ), bitmap ) );
    end Al_Save_Bitmap_f;

    ----------------------------------------------------------------------------

    procedure Al_Save_Bitmap_f( fp     : A_Allegro_File;
                                ident  : String;
                                bitmap : A_Allegro_Bitmap ) is
        ok : Boolean;
        pragma Warnings( Off, ok );
    begin
        ok := Al_Save_Bitmap_f( fp, ident, bitmap );
    end Al_Save_Bitmap_f;

    ----------------------------------------------------------------------------

    function Al_Identify_Bitmap( filename : String ) return String is
        function C_Al_Identify_Bitmap( filename : C.char_array ) return chars_ptr;
        pragma Import( C, C_Al_Identify_Bitmap, "al_identify_bitmap" );
    begin
        return Value( C_Al_Identify_Bitmap( To_C( filename ) ) );
    end Al_Identify_Bitmap;

    ----------------------------------------------------------------------------

    function Al_Identify_Bitmap_f( fp : A_Allegro_File ) return String is
        function C_Al_Identify_Bitmap_f( fp : A_Allegro_File ) return chars_ptr;
        pragma Import( C, C_Al_Identify_Bitmap_f, "al_identify_bitmap" );
    begin
        return Value( C_Al_Identify_Bitmap_f( fp ) );
    end Al_Identify_Bitmap_f;

end Allegro.Bitmaps.Image_IO;

