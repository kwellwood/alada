--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Ada.Command_Line;
with Interfaces.C;                      use Interfaces.C;

package body Allegro.PhysicsFS is

    function PHYSFS_Init return Boolean is

        function C_PHYSFS_Init( argv0 : C.char_array ) return Integer;
        pragma Import( C, C_PHYSFS_Init, "PHYSFS_init" );

    begin
        -- Automatically pass arg[0].
        return C_PHYSFS_Init( To_C( Ada.Command_Line.Command_Name ) ) /= 0;
    end PHYSFS_Init;

    ----------------------------------------------------------------------------

    function PHYSFS_Deinit return Boolean is

        function C_PHYSFS_Deinit return Integer;
        pragma Import( C, C_PHYSFS_Deinit, "PHYSFS_deinit" );

    begin
        return C_PHYSFS_Deinit /= 0;
    end PHYSFS_Deinit;

    ----------------------------------------------------------------------------

    function PHYSFS_IsInit return Boolean is

        function C_PHYSFS_IsInit return Integer;
        pragma Import( C, C_PHYSFS_IsInit, "PHYSFS_isInit" );

    begin
        return C_PHYSFS_IsInit /= 0;
    end PHYSFS_IsInit;

    ----------------------------------------------------------------------------

    function PHYSFS_AddToSearchPath( newDir       : String;
                                     appendToPath : Boolean := True ) return Boolean is

        function C_PHYSFS_AddToSearchPath( newDir       : C.char_array;
                                           appendToPath : Integer ) return Integer;
        pragma Import( C, C_PHYSFS_AddToSearchPath, "PHYSFS_addToSearchPath" );

    begin
        return C_PHYSFS_AddToSearchPath( To_C( newDir ), Boolean'Pos( appendToPath ) ) /= 0;
    end PHYSFS_AddToSearchPath;

    ----------------------------------------------------------------------------

    function PHYSFS_Mount( newDir       : String;
                           mountPoint   : String;
                           appendToPath : Boolean ) return Boolean is
        function C_PHYSFS_Mount( newDir       : C.char_array;
                                 mountPoint   : C.char_array;
                                 appendToPath : Integer ) return Integer;
        pragma Import( C, C_PHYSFS_Mount, "PHYSFS_mount" );
    begin
        return C_PHYSFS_Mount( To_C( newDir ), To_C( mountPoint ), Boolean'Pos( appendToPath ) ) /= 0;
    end PHYSFS_Mount;

end Allegro.PhysicsFS;
