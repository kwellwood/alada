--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;

package body Allegro.Fonts.TTF is

    function Al_Init_TTF_Addon return Boolean is

        function C_Al_Init_TTF_Addon return Bool;
        pragma Import( C, C_Al_Init_TTF_Addon, "al_init_ttf_addon" );

    begin
        return To_Ada( C_Al_Init_TTF_Addon );
    end Al_Init_TTF_Addon;

    ----------------------------------------------------------------------------

    function Al_Load_TTF_Font( filename : String;
                               size     : Integer;
                               flags    : Font_Loading_Flags ) return A_Allegro_Font is

        function C_Al_Load_TTF_Font( filename : C.char_array;
                                     size     : Integer;
                                     flags    : Font_Loading_Flags ) return A_Allegro_Font;
        pragma Import( C, C_Al_Load_TTF_Font, "al_load_ttf_font" );

    begin
        return C_Al_Load_TTF_Font( To_C( filename ), size, flags );
    end Al_Load_TTF_Font;

    ----------------------------------------------------------------------------

    function Al_Load_TTF_Font_f( file     : A_Allegro_File;
                                 filename : String;
                                 size     : Integer;
                                 flags    : Font_Loading_Flags ) return A_Allegro_Font is

        function C_Al_Load_TTF_Font_f( file     : A_Allegro_File;
                                       filename : C.char_array;
                                       size     : Integer;
                                       flags    : Font_Loading_Flags ) return A_Allegro_Font;
        pragma Import( C, C_Al_Load_TTF_Font_f, "al_load_ttf_font_f" );

    begin
        return C_Al_Load_TTF_Font_f( file, To_C( filename ), size, flags );
    end Al_Load_TTF_Font_f;

    ----------------------------------------------------------------------------

    function Al_Load_TTF_Font_Stretch( filename : String;
                                         w, h     : Integer;
                                         flags    : Font_Loading_Flags ) return A_Allegro_Font is

        function C_Al_Load_TTF_Font_Stretch( filename : C.char_array;
                                               w, h     : Integer;
                                               flags    : Font_Loading_Flags ) return A_Allegro_Font;
        pragma Import( C, C_Al_Load_TTF_Font_Stretch, "al_load_ttf_font_stretch" );

    begin
        return C_Al_Load_TTF_Font_Stretch( To_C( filename ), w, h, flags );
    end Al_Load_TTF_Font_Stretch;

    ----------------------------------------------------------------------------

    function Al_Load_TTF_Font_Stretch_f( file     : A_Allegro_File;
                                         filename : String;
                                         w, h     : Integer;
                                         flags    : Font_Loading_Flags ) return A_Allegro_Font is

        function C_Al_Load_TTF_Font_Stretch_f( file     : A_Allegro_File;
                                               filename : C.char_array;
                                               w, h     : Integer;
                                               flags    : Font_Loading_Flags ) return A_Allegro_Font;
        pragma Import( C, C_Al_Load_TTF_Font_Stretch_f, "al_load_ttf_font_stretch_f" );

    begin
        return C_Al_Load_TTF_Font_Stretch_f( file, To_C( filename ), w, h, flags );
    end Al_Load_TTF_Font_Stretch_f;

end Allegro.Fonts.TTF;
