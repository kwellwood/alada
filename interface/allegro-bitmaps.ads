--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Blending;                  use Allegro.Blending;
with Allegro.Color;                     use Allegro.Color;
with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Bitmaps routines
package Allegro.Bitmaps is

    -- Bitmaps

    type Allegro_Bitmap is limited private;
    type A_Allegro_Bitmap is access all Allegro_Bitmap;

    subtype Allegro_Bitmap_Flags is Unsigned_32;
    ALLEGRO_MEMORY_BITMAP       : constant Allegro_Bitmap_Flags;
    ALLEGRO_NO_PRESERVE_TEXTURE : constant Allegro_Bitmap_Flags;
    ALLEGRO_MIN_LINEAR          : constant Allegro_Bitmap_Flags;
    ALLEGRO_MAG_LINEAR          : constant Allegro_Bitmap_Flags;
    ALLEGRO_MIPMAP              : constant Allegro_Bitmap_Flags;
    ALLEGRO_VIDEO_BITMAP        : constant Allegro_Bitmap_Flags;
    ALLEGRO_CONVERT_BITMAP      : constant Allegro_Bitmap_Flags;

    procedure Al_Add_New_Bitmap_Flag( flag : Allegro_Bitmap_Flags );
    pragma Import( C, Al_Add_New_Bitmap_Flag, "al_add_new_bitmap_flag" );

    function Al_Get_New_Bitmap_Depth return Integer;
    pragma Import( C, Al_Get_New_Bitmap_Depth, "al_get_new_bitmap_depth" );

    function Al_Get_New_Bitmap_Flags return Allegro_Bitmap_Flags;
    pragma Import( C, Al_Get_New_Bitmap_Flags, "al_get_new_bitmap_flags" );

    function Al_Get_New_Bitmap_Format return Allegro_Pixel_Format;
    pragma Import( C, Al_Get_New_Bitmap_Format, "al_get_new_bitmap_format" );

    function Al_Get_New_Bitmap_Samples return Integer;
    pragma Import( C, Al_Get_New_Bitmap_Samples, "al_get_new_bitmap_samples" );

    procedure Al_Set_New_Bitmap_Depth( depth : Integer );
    pragma Import( C, Al_Set_New_Bitmap_Depth, "al_set_new_bitmap_depth" );

    procedure Al_Set_New_Bitmap_Flags( flags : Allegro_Bitmap_Flags );
    pragma Import( C, Al_Set_New_Bitmap_Flags, "al_set_new_bitmap_flags" );

    procedure Al_Set_New_Bitmap_Format( format : Allegro_Pixel_Format );
    pragma Import( C, Al_Set_New_Bitmap_Format, "al_set_new_bitmap_format" );

    procedure Al_Set_New_Bitmap_Samples( samples : Integer );
    pragma Import( C, Al_Set_New_Bitmap_Samples, "al_set_new_bitmap_samples" );

    function Al_Create_Bitmap( w, h : Integer ) return A_Allegro_Bitmap;
    pragma Import( C, Al_Create_Bitmap, "al_create_bitmap" );

    function Al_Clone_Bitmap( bitmap : A_Allegro_Bitmap ) return A_Allegro_Bitmap;
    pragma Import( C, Al_Clone_Bitmap, "al_clone_bitmap" );

    procedure Al_Convert_Bitmap( bitmap : A_Allegro_Bitmap );
    pragma Import( C, Al_Convert_Bitmap, "al_convert_bitmap" );

    procedure Al_Convert_Memory_Bitmaps;
    pragma Import( C, Al_Convert_Memory_Bitmaps, "al_convert_memory_bitmaps" );

    procedure Al_Backup_Dirty_Bitmap( bitmap : A_Allegro_Bitmap );
    pragma Import( C, Al_Backup_Dirty_Bitmap, "al_backup_dirty_bitmap" );

    procedure Al_Destroy_Bitmap( bitmap : in out A_Allegro_Bitmap );

    function Al_Get_Bitmap_Depth( bitmap : A_Allegro_Bitmap ) return Integer;
    pragma Import( C, Al_Get_Bitmap_Depth, "al_get_bitmap_depth" );

    function Al_Get_Bitmap_Flags( bitmap : A_Allegro_Bitmap ) return Allegro_Bitmap_Flags;
    pragma Import( C, Al_Get_Bitmap_Flags, "al_get_bitmap_flags" );

    function Al_Get_Bitmap_Format( bitmap : A_Allegro_Bitmap ) return Allegro_Pixel_Format;
    pragma Import( C, Al_Get_Bitmap_Format, "al_get_bitmap_format" );

    function Al_Get_Bitmap_Height( bitmap : A_Allegro_Bitmap ) return Integer;
    pragma Import( C, Al_Get_Bitmap_Height, "al_get_bitmap_height" );

    function Al_Get_Bitmap_Samples( bitmap : A_Allegro_Bitmap ) return Integer;
    pragma Import( C, Al_Get_Bitmap_Samples, "al_get_bitmap_samples" );

    function Al_Get_Bitmap_Width( bitmap : A_Allegro_Bitmap ) return Integer;
    pragma Import( C, Al_Get_Bitmap_Width, "al_get_bitmap_width" );

    -- Bitmap access

    function Al_Get_Pixel( bitmap : A_Allegro_Bitmap; x, y : Integer ) return Allegro_Color;
    pragma Import( C, Al_Get_Pixel, "al_get_pixel" );

    procedure Al_Put_Pixel( x, y : Integer; color : Allegro_Color );
    pragma Import( C, Al_Put_Pixel, "al_put_pixel" );

    procedure Al_Put_Blended_Pixel( x, y : Integer; color : Allegro_Color );
    pragma Import( C, Al_Put_Blended_Pixel, "al_put_blended_pixel" );

    -- Masking

    procedure Al_Convert_Mask_To_Alpha( bitmap     : A_Allegro_Bitmap;
                                        mask_color : Allegro_Color );
    pragma Import( C, Al_Convert_Mask_To_Alpha, "al_convert_mask_to_alpha" );

    -- Blending

    -- Unstable API
    function Al_Get_Bitmap_Blend_Color return Allegro_Color;
    pragma Import( C, Al_Get_Bitmap_Blend_Color, "al_get_bitmap_blend_color" );

    -- Unstable API
    procedure Al_Get_Bitmap_Blender( op  : out Allegro_Blend_Operations;
                                     src : out Allegro_Blend_Mode;
                                     dst : out Allegro_Blend_Mode );
    pragma Import( C, Al_Get_Bitmap_Blender, "al_get_bitmap_blender" );

    -- Unstable API
    procedure Al_Get_Separate_Bitmap_Blender( op        : out Allegro_Blend_Operations;
                                              src       : out Allegro_Blend_Mode;
                                              dst       : out Allegro_Blend_Mode;
                                              alpha_op  : out Allegro_Blend_Operations;
                                              alpha_src : out Allegro_Blend_Mode;
                                              alpha_dst : out Allegro_Blend_Mode );
    pragma Import( C, Al_Get_Separate_Bitmap_Blender, "al_get_separate_bitmap_blender" );

    -- Unstable API
    procedure Al_Set_Bitmap_Blend_Color( color : Allegro_Color );
    pragma Import( C, Al_Set_Bitmap_Blend_Color, "al_set_bitmap_blend_color" );

    -- Unstable API
    procedure Al_Set_Bitmap_Blender( op  : Allegro_Blend_Operations;
                                     src : Allegro_Blend_Mode;
                                     dst : Allegro_Blend_Mode );
    pragma Import( C, Al_Set_Bitmap_Blender, "al_set_bitmap_blender" );

    -- Unstable API
    procedure Al_Set_Separate_Bitmap_Blender( op        : Allegro_Blend_Operations;
                                              src       : Allegro_Blend_Mode;
                                              dst       : Allegro_Blend_Mode;
                                              alpha_op  : Allegro_Blend_Operations;
                                              alpha_src : Allegro_Blend_Mode;
                                              alpha_dst : Allegro_Blend_Mode );
    pragma Import( C, Al_Set_Separate_Bitmap_Blender, "al_set_separate_bitmap_blender" );

    -- Unstable API
    procedure Al_Reset_Bitmap_Blender;
    pragma Import( C, Al_Reset_Bitmap_Blender, "al_reset_bitmap_blender" );

    -- Clipping

    procedure Al_Get_Clipping_Rectangle( x, y, w, h : out Integer );
    pragma Import( C, Al_Get_Clipping_Rectangle, "al_get_clipping_rectangle" );

    procedure Al_Reset_Clipping_Rectangle;
    pragma Import( C, Al_Reset_Clipping_Rectangle, "al_reset_clipping_rectangle" );

    procedure Al_Set_Clipping_Rectangle( x, y, width, height : Integer );
    pragma Import( C, Al_Set_Clipping_Rectangle, "al_set_clipping_rectangle" );

    -- Sub bitmaps

    function Al_Create_Sub_Bitmap( parent : A_Allegro_Bitmap;
                                   x, y   : Integer;
                                   w, h   : Integer ) return A_Allegro_Bitmap;
    pragma Import( C, Al_Create_Sub_Bitmap, "al_create_sub_bitmap" );

    function Al_Is_Sub_Bitmap( bitmap : A_Allegro_Bitmap ) return Boolean;

    function Al_Get_Parent_Bitmap( bitmap : A_Allegro_Bitmap ) return A_Allegro_Bitmap;
    pragma Import( C, Al_Get_Parent_Bitmap, "al_get_parent_bitmap" );

    function Al_Get_Bitmap_X( bitmap : A_Allegro_Bitmap ) return Integer;
    pragma Import( C, Al_Get_Bitmap_X, "al_get_bitmap_x" );

    function Al_Get_Bitmap_Y( bitmap : A_Allegro_Bitmap ) return Integer;
    pragma Import( C, Al_Get_Bitmap_Y, "al_get_bitmap_y" );

    procedure Al_Reparent_Bitmap( bitmap : A_Allegro_Bitmap;
                                  parent : A_Allegro_Bitmap;
                                  x, y   : Integer;
                                  w, h   : Integer );
    pragma Import( C, Al_Reparent_Bitmap, "al_reparent_bitmap" );

private

    ALLEGRO_MEMORY_BITMAP       : constant Allegro_Bitmap_Flags := 16#0001#;
    ALLEGRO_NO_PRESERVE_TEXTURE : constant Allegro_Bitmap_Flags := 16#0008#;
    ALLEGRO_MIN_LINEAR          : constant Allegro_Bitmap_Flags := 16#0040#;
    ALLEGRO_MAG_LINEAR          : constant Allegro_Bitmap_Flags := 16#0080#;
    ALLEGRO_MIPMAP              : constant Allegro_Bitmap_Flags := 16#0100#;
    ALLEGRO_VIDEO_BITMAP        : constant Allegro_Bitmap_Flags := 16#0400#;
    ALLEGRO_CONVERT_BITMAP      : constant Allegro_Bitmap_Flags := 16#1000#;

    type Allegro_Bitmap is limited null record;
    pragma Convention( C, Allegro_Bitmap );

end Allegro.Bitmaps;
