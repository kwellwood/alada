--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.Time;                      use Allegro.Time;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

-- Allegro 5.2.5 - Threads
package Allegro.Threads is

    -- Threads

    type Allegro_Thread is limited private;
    type A_Allegro_Thread is access all Allegro_Thread;

    type Thread_Proc is access
        function( thread : A_Allegro_Thread;
                  arg    : Address ) return Address;
    pragma Convention( C, Thread_Proc );

    function Al_Create_Thread( proc : Thread_Proc; arg : Address ) return A_Allegro_Thread;
    pragma Import( C, Al_Create_Thread, "al_create_thread" );

    -- Unstable API
    function Al_Create_Thread_With_Stacksize( proc : Thread_Proc; arg : Address; stacksize : size_t ) return A_Allegro_Thread;
    pragma Import( C, Al_Create_Thread_With_Stacksize, "al_create_thread_with_stacksize" );

    procedure Al_Start_Thread( thread : A_Allegro_Thread );
    pragma Import( C, Al_Start_Thread, "al_start_thread" );

    procedure Al_Join_Thread( thread : A_Allegro_Thread; ret_value : in out Address );
    pragma Import( C, Al_Join_Thread, "al_join_thread" );

    -- Same as Al_Join_Thread above, but the thread's return value is ignored.
    procedure Al_Join_Thread( thread : A_Allegro_Thread );

    procedure Al_Set_Thread_Should_Stop( thread : A_Allegro_Thread );
    pragma Import( C, Al_Set_Thread_Should_Stop, "al_set_thread_should_stop" );

    function Al_Get_Thread_Should_Stop( thread : A_Allegro_Thread ) return Boolean;

    procedure Al_Destroy_Thread( thread : in out A_Allegro_Thread );

    procedure Al_Run_Detached_Thread( proc : Thread_Proc; arg : Address );
    pragma Import( C, Al_Run_Detached_Thread, "al_run_detached_thread" );

    -- Mutexes

    type Allegro_Mutex is limited private;
    type A_Allegro_Mutex is access all Allegro_Mutex;

    function Al_Create_Mutex return A_Allegro_Mutex;
    pragma Import( C, Al_Create_Mutex, "al_create_mutex" );

    function Al_Create_Mutex_Recursive return A_Allegro_Mutex;
    pragma Import( C, Al_Create_Mutex_Recursive, "al_create_mutex_recursive" );

    procedure Al_Lock_Mutex( mutex : A_Allegro_Mutex );
    pragma Import( C, Al_Lock_Mutex, "al_lock_mutex" );

    procedure Al_Unlock_Mutex( mutex : A_Allegro_Mutex );
    pragma Import( C, Al_Unlock_Mutex, "al_unlock_mutex" );

    procedure Al_Destroy_Mutex( mutex : in out A_Allegro_Mutex );

    -- Condition variables

    type Allegro_Cond is limited private;
    type A_Allegro_Cond is access all Allegro_Cond;

    function Al_Create_Cond return A_Allegro_Cond;
    pragma Import( C, Al_Create_Cond, "al_create_cond" );

    procedure Al_Destroy_Cond( cond : in out A_Allegro_Cond );

    procedure Al_Wait_Cond( cond : A_Allegro_Cond; mutex : A_Allegro_Mutex );
    pragma Import( C, Al_Wait_Cond, "al_wait_cond" );

    function Al_Wait_Cond_Until( cond    : A_Allegro_Cond;
                                 mutex   : A_Allegro_Mutex;
                                 timeout : access Allegro_Timeout ) return Integer;
    pragma Import( C, Al_Wait_Cond_Until, "al_wait_cond_until" );

    procedure Al_Broadcast_Cond( cond : A_Allegro_Cond );
    pragma Import( C, Al_Broadcast_Cond, "al_broadcast_cond" );

    procedure Al_Signal_Cond( cond : A_Allegro_Cond );
    pragma Import( C, Al_Signal_Cond, "al_signal_cond" );

private

    type Allegro_Thread is limited null record;
    pragma Convention( C, Allegro_Thread );

    type Allegro_Mutex is limited null record;
    pragma Convention( C, Allegro_Mutex );

    type Allegro_Cond is limited null record;
    pragma Convention( C, Allegro_Cond );

end Allegro.Threads;
