--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Image_Addon is

    function Al_Init_Image_Addon return Boolean is

        function C_Al_Init_Image_Addon return Bool;
        pragma Import( C, C_Al_Init_Image_Addon, "al_init_image_addon" );

    begin
        return To_Ada( C_Al_Init_Image_Addon );
    end Al_Init_Image_Addon;

end Allegro.Image_Addon;
