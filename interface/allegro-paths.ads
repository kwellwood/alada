--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Allegro.UTF8;                      use Allegro.UTF8;
with GNAT.OS_Lib;

-- Allegro 5.2.5 - Path structures
package Allegro.Paths is

    ALLEGRO_NATIVE_PATH_SEP : constant Character;

    type Allegro_Path is limited private;
    type A_Allegro_Path is access all Allegro_Path;

    function Al_Create_Path( str : String ) return A_Allegro_Path;

    function Al_Create_Path_For_Directory( str : String ) return A_Allegro_Path;

    procedure Al_Destroy_Path( path : in out A_Allegro_Path );

    function Al_Clone_Path( path : A_Allegro_Path ) return A_Allegro_Path;
    pragma Import( C, Al_Clone_Path, "al_clone_path" );

    function Al_Join_Paths( path : A_Allegro_Path; tail : A_Allegro_Path ) return Boolean;

    function Al_Rebase_Path( head : A_Allegro_Path; tail : A_Allegro_Path ) return Boolean;

    function Al_Get_Path_Drive( path : A_Allegro_Path ) return String;

    function Al_Get_Path_Num_Components( path : A_Allegro_Path ) return Integer;
    pragma Import( C, Al_Get_Path_Num_Components, "al_get_path_num_components" );

    function Al_Get_Path_Component( path : A_Allegro_Path; i : Integer ) return String;

    function Al_Get_Path_Tail( path : A_Allegro_Path ) return String;

    function Al_Get_Path_Filename( path : A_Allegro_Path ) return String;

    function Al_Get_Path_Basename( path : A_Allegro_Path ) return String;

    function Al_Get_Path_Extension( path : A_Allegro_Path ) return String;

    procedure Al_Set_Path_Drive( path : A_Allegro_Path; drive : String );

    procedure Al_Append_Path_Component( path : A_Allegro_Path; s : String );

    procedure Al_Insert_Path_Component( path : A_Allegro_Path;
                                        i    : Integer;
                                        s    : String );

    procedure Al_Replace_Path_Component( path : A_Allegro_Path;
                                         i    : Integer;
                                         s    : String );

    procedure Al_Remove_Path_Component( path : A_Allegro_Path; i : Integer );
    pragma Import( C, Al_Remove_Path_Component, "al_remove_path_component" );

    procedure Al_Drop_Path_Tail( path : A_Allegro_Path );
    pragma Import( C, Al_Drop_Path_Tail, "al_drop_path_tail" );

    procedure Al_Set_Path_Filename( path : A_Allegro_Path; filename : String );

    function Al_Set_Path_Extension( path : A_Allegro_Path; extension : String ) return Boolean;

    function Al_Path_CStr( path  : A_Allegro_Path;
                           delim : Character := ALLEGRO_NATIVE_PATH_SEP ) return String;

    function Al_Path_Ustr( path : A_Allegro_Path; delim : Character := ALLEGRO_NATIVE_PATH_SEP ) return A_Allegro_Ustr;

    function Al_Make_Path_Canonical( path : A_Allegro_Path ) return Boolean;

private

    ALLEGRO_NATIVE_PATH_SEP : constant Character := GNAT.OS_Lib.Directory_Separator;

    type Allegro_Path is limited null record;
    pragma Convention( C, Allegro_Path );

end Allegro.Paths;
