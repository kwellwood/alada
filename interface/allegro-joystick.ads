--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;

-- Allegro 5.2.5 - Joystick routines
package Allegro.Joystick is

    type Allegro_Joystick is limited private;
    type A_Allegro_Joystick is access all Allegro_Joystick;

    subtype Joystick_Flags is Unsigned_32;
    JOYFLAG_DIGITAL  : constant Joystick_Flags := 16#01#;
    JOYFLAG_ANALOGUE : constant Joystick_Flags := 16#02#;

    -- Joystick state

    type Axis_Array is array(0..2) of Float;
    pragma Convention( C, Axis_Array );

    type Joystick_Stick is
        record
            axis : Axis_Array := (others => 0.0);      -- -1.0 to 1.0
        end record;
    pragma Convention( C, Joystick_Stick );

    type Stick_Array is array(0..15) of Joystick_Stick;
    pragma Convention( C, Stick_Array );

    type Button_Array is array(0..31) of Integer;
    pragma Convention( C, Button_Array );

    type Allegro_Joystick_State is
        record
            stick  : Stick_Array;
            button : Button_Array := (others => 0);    -- 0 to 32767
        end record;
    pragma Convention( C, Allegro_Joystick_State );
    type A_Allegro_Joystick_State is access all Allegro_Joystick_State;

    ----------------------------------------------------------------------------

    function Al_Install_Joystick return Boolean;

    procedure Al_Uninstall_Joystick;
    pragma Import( C, Al_Uninstall_Joystick, "al_uninstall_joystick" );

    function Al_Is_Joystick_Installed return Boolean;

    function Al_Reconfigure_Joysticks return Boolean;

    function Al_Get_Num_Joysticks return Integer;
    pragma Import( C, Al_Get_Num_Joysticks, "al_get_num_joysticks" );

    function Al_Get_Joystick( num : Integer ) return A_Allegro_Joystick;
    pragma Import( C, Al_Get_Joystick, "al_get_joystick" );

    procedure Al_Release_Joystick( joy : A_Allegro_Joystick );
    pragma Import( C, Al_Release_Joystick, "al_release_joystick" );

    function Al_Get_Joystick_Active( joy : A_Allegro_Joystick ) return Boolean;

    function Al_Get_Joystick_Name( joy : A_Allegro_Joystick ) return String;

    function Al_Get_Joystick_Num_Sticks( joy : A_Allegro_Joystick ) return Integer;
    pragma Import( C, Al_Get_Joystick_Num_Sticks, "al_get_joystick_num_sticks" );

    function Al_Get_Joystick_Stick_Flags( joy   : A_Allegro_Joystick;
                                          stick : Integer ) return Joystick_Flags;
    pragma Import( C, Al_Get_Joystick_Stick_Flags, "al_get_joystick_stick_flags" );

    function Al_Get_Joystick_Stick_Name( joy   : A_Allegro_Joystick;
                                         stick : Integer ) return String;

    function Al_Get_Joystick_Num_Axes( joy   : A_Allegro_Joystick;
                                       stick : Integer ) return Integer;
    pragma Import( C, Al_Get_Joystick_Num_Axes, "al_get_joystick_num_axes" );

    function Al_Get_Joystick_Axis_Name( joy   : A_Allegro_Joystick;
                                        stick : Integer;
                                        axis  : Integer ) return String;

    function Al_Get_Joystick_Num_Buttons( joy : A_Allegro_Joystick ) return Integer;
    pragma Import( C, Al_Get_Joystick_Num_Buttons, "al_get_joystick_num_buttons" );

    function Al_Get_Joystick_Button_Name( joy    : A_Allegro_Joystick;
                                          button : Integer ) return String;

    procedure Al_Get_Joystick_State( joy       : A_Allegro_Joystick;
                                     ret_state : out Allegro_Joystick_State );
    pragma Import( C, Al_Get_Joystick_State, "al_get_joystick_state" );

    function Al_Get_Joystick_Event_Source return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Joystick_Event_Source, "al_get_joystick_event_source" );

private

    type Allegro_Joystick is limited null record;
    pragma Convention( C, Allegro_Joystick );

end Allegro.Joystick;
