--
-- Copyright (c) 2013-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro.Timers is

    function Allegro_USecs_To_Secs( x : Long_Float ) return Long_Float is
    begin
        return x / 1_000_000.0;
    end Allegro_USecs_To_Secs;

    ----------------------------------------------------------------------------

    function Allegro_MSecs_To_Secs( x : Long_Float ) return Long_Float is
    begin
        return x / 1_000.0;
    end Allegro_MSecs_To_Secs;

    ----------------------------------------------------------------------------

    function Allegro_BPS_To_Secs( x : Long_Float ) return Long_Float is
    begin
        return 1.0 / x;
    end Allegro_BPS_To_Secs;

    ----------------------------------------------------------------------------

    function Allegro_BPM_To_Secs( x : Long_Float ) return Long_Float is
    begin
        return 60.0 / x;
    end Allegro_BPM_To_Secs;

    ----------------------------------------------------------------------------

    procedure Al_Destroy_Timer( timer : in out A_Allegro_Timer ) is

        procedure C_Al_Destroy_Timer( timer : A_Allegro_Timer );
        pragma Import( C, C_Al_Destroy_Timer, "al_destroy_timer" );

    begin
        if timer /= null then
            C_Al_Destroy_Timer( timer );
            timer := null;
        end if;
    end Al_Destroy_Timer;

    ----------------------------------------------------------------------------

    function Al_Get_Timer_Started( timer : A_Allegro_Timer ) return Boolean is

        function C_Al_Get_Timer_Started( timer : A_Allegro_Timer ) return Bool;
        pragma Import( C, C_Al_Get_Timer_Started, "al_get_timer_started" );

    begin
        return To_Ada( C_Al_Get_Timer_Started( timer ) );
    end Al_Get_Timer_Started;

end Allegro.Timers;
