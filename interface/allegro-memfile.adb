--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;

package body Allegro.Memfile is

    function Al_Open_Memfile( mem  : Address;
                              size : Integer_64;
                              mode : String ) return A_Allegro_File is

        function C_Al_Open_Memfile( mem  : Address;
                                    size : Integer_64;
                                    mode : C.char_array ) return A_Allegro_File;
        pragma Import( C, C_Al_Open_Memfile, "al_open_memfile" );

    begin
        return C_Al_Open_Memfile( mem, size, To_C( mode ) );
    end Al_Open_Memfile;

end Allegro.Memfile;
