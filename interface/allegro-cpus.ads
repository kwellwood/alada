--
-- Copyright (c) 2016-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

-- Allegro 5.2.5 - CPU routines
package Allegro.CPUs is

    function Al_Get_CPU_Count return Integer;
    pragma Import( C, Al_Get_CPU_Count, "al_get_cpu_count" );

    function Al_Get_RAM_Size return Integer;
    pragma Import( C, Al_Get_RAM_Size, "al_get_ram_size" );

end Allegro.CPUs;
