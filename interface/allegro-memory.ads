--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;
with Interfaces.C;                      use Interfaces.C;
with System;                            use System;

-- Allegro 5.2.5 - Memory management routines
package Allegro.Memory is

    -- Mamory interface types

    type mi_malloc  is access function( n : C.size_t; line : Integer; file : C.char_array; func : C.char_array ) return Address;
    type mi_free    is access procedure( ptr : Address; line : Integer; file : C.char_array; func : C.char_array );
    type mi_realloc is access function( ptr : Address; n : C.size_t; line : Integer; file : C.char_array; func : C.char_array ) return Address;
    type mi_calloc  is access function( count : C.size_t; n : C.size_t; line : Integer; file : C.char_array; func : C.char_array ) return Address;

    pragma Convention( C, mi_malloc );
    pragma Convention( C, mi_free );
    pragma Convention( C, mi_realloc );
    pragma Convention( C, mi_calloc );

    type Allegro_Memory_Interface is
        record
            malloc  : mi_malloc;
            free    : mi_free;
            realloc : mi_realloc;
            calloc  : mi_calloc;
        end record;
    pragma Convention( C, Allegro_Memory_Interface );
    type A_Allegro_Memory_Interface is access all Allegro_Memory_Interface;

    procedure Al_Set_Memory_Interface( iface : A_Allegro_Memory_Interface );
    pragma Import( C, Al_Set_Memory_Interface, "al_set_memory_interface" );

    -- Memory allocation and deallocation

    function Al_Malloc( n : C.size_t ) return Address;

    function Al_Calloc( count : C.size_t; n : C.size_t ) return Address;

    procedure Al_Free( ptr : Address );

    function Al_Realloc( ptr : Address; n : C.size_t ) return Address;

end Allegro.Memory;
