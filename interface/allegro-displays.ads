--
-- Copyright (c) 2013-2019 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces;                        use Interfaces;
with Allegro.Bitmaps;                   use Allegro.Bitmaps;
with Allegro.Color;                     use Allegro.Color;

-- Allegro 5.2.5 - Display routines
package Allegro.Displays is

    type Allegro_Display is limited private;
    type A_Allegro_Display is access all Allegro_Display;
    pragma Convention( C, A_Allegro_Display );
    pragma No_Strict_Aliasing( A_Allegro_Display );

    -- Display creation parameterization

    subtype Display_Flags is Unsigned_32;
    ALLEGRO_WINDOWED                  : constant Display_Flags;
    ALLEGRO_FULLSCREEN                : constant Display_Flags;
    ALLEGRO_OPENGL                    : constant Display_Flags;
    ALLEGRO_DIRECT3D                  : constant Display_Flags;
    ALLEGRO_RESIZABLE                 : constant Display_Flags;
    ALLEGRO_FRAMELESS                 : constant Display_Flags;
    ALLEGRO_NOFRAME                   : constant Display_Flags;
    ALLEGRO_GENERATE_EXPOSE_EVENTS    : constant Display_Flags;
    ALLEGRO_OPENGL_3_0                : constant Display_Flags;
    ALLEGRO_OPENGL_FORWARD_COMPATIBLE : constant Display_Flags;
    ALLEGRO_FULLSCREEN_WINDOW         : constant Display_Flags;
    ALLEGRO_MINIMIZED                 : constant Display_Flags;  -- status only
    ALLEGRO_PROGRAMMABLE_PIPELINE     : constant Display_Flags;
    ALLEGRO_GTK_TOPLEVEL_INTERNAL     : constant Display_Flags;
    ALLEGRO_MAXIMIZED                 : constant Display_Flags;
    ALLEGRO_OPENGL_ES_PROFILE         : constant Display_Flags;

    type Display_Option is new Integer;
    ALLEGRO_RED_SIZE               : constant Display_Option;
    ALLEGRO_GREEN_SIZE             : constant Display_Option;
    ALLEGRO_BLUE_SIZE              : constant Display_Option;
    ALLEGRO_ALPHA_SIZE             : constant Display_Option;
    ALLEGRO_RED_SHIFT              : constant Display_Option;
    ALLEGRO_GREEN_SHIFT            : constant Display_Option;
    ALLEGRO_BLUE_SHIFT             : constant Display_Option;
    ALLEGRO_ALPHA_SHIFT            : constant Display_Option;
    ALLEGRO_ACC_RED_SIZE           : constant Display_Option;
    ALLEGRO_ACC_GREEN_SIZE         : constant Display_Option;
    ALLEGRO_ACC_BLUE_SIZE          : constant Display_Option;
    ALLEGRO_ACC_ALPHA_SIZE         : constant Display_Option;
    ALLEGRO_STEREO                 : constant Display_Option;
    ALLEGRO_AUX_BUFFERS            : constant Display_Option;
    ALLEGRO_COLOR_SIZE             : constant Display_Option;
    ALLEGRO_DEPTH_SIZE             : constant Display_Option;
    ALLEGRO_STENCIL_SIZE           : constant Display_Option;
    ALLEGRO_SAMPLE_BUFFERS         : constant Display_Option;
    ALLEGRO_SAMPLES                : constant Display_Option;
    ALLEGRO_RENDER_METHOD          : constant Display_Option;
    ALLEGRO_FLOAT_COLOR            : constant Display_Option;
    ALLEGRO_FLOAT_DEPTH            : constant Display_Option;
    ALLEGRO_SINGLE_BUFFER          : constant Display_Option;
    ALLEGRO_SWAP_METHOD            : constant Display_Option;
    ALLEGRO_COMPATIBLE_DISPLAY     : constant Display_Option;
    ALLEGRO_UPDATE_DISPLAY_REGION  : constant Display_Option;
    ALLEGRO_VSYNC                  : constant Display_Option;
    ALLEGRO_MAX_BITMAP_SIZE        : constant Display_Option;
    ALLEGRO_SUPPORT_NPOT_BITMAP    : constant Display_Option;
    ALLEGRO_CAN_DRAW_INTO_BITMAP   : constant Display_Option;
    ALLEGRO_SUPPORT_SEPARATE_ALPHA : constant Display_Option;
    ALLEGRO_SUPPORTED_ORIENTATIONS : constant Display_Option;
    ALLEGRO_OPENGL_MAJOR_VERSION   : constant Display_Option;
    ALLEGRO_OPENGL_MINOR_VERSION   : constant Display_Option;

    type Option_Importance is new Integer;
    ALLEGRO_DONTCARE : constant Option_Importance;
    ALLEGRO_REQUIRE  : constant Option_Importance;
    ALLEGRO_SUGGEST  : constant Option_Importance;

    subtype Allegro_Display_Orientation is Unsigned_32;
    ALLEGRO_DISPLAY_ORIENTATION_UNKNOWN     : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_0_DEGREES   : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_90_DEGREES  : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_180_DEGREES : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_270_DEGREES : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_PORTRAIT    : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_LANDSCAPE   : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_ALL         : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_FACE_UP     : constant Allegro_Display_Orientation;
    ALLEGRO_DISPLAY_ORIENTATION_FACE_DOWN   : constant Allegro_Display_Orientation;

    -- Access creation parameters

    function Al_Get_New_Display_Adapter return Integer;
    pragma Import( C, Al_Get_New_Display_Adapter, "al_get_new_display_adapter" );

    function Al_Get_New_Display_Flags return Display_Flags;
    pragma Import( C, Al_Get_New_Display_Flags, "al_get_new_display_flags" );

    procedure Al_Get_New_Display_Option( option     : Display_Option;
                                         value      : out Integer;
                                         importance : out Option_Importance );

    function Al_Get_New_Display_Refresh_Rate return Integer;
    pragma Import( C, Al_Get_New_Display_Refresh_Rate, "al_get_new_display_refresh_rate" );

    procedure Al_Get_New_Window_Position( x, y : access Integer );
    pragma Import( C, Al_Get_New_Window_Position, "al_get_new_window_position" );

    function Al_Get_New_Window_Title return String;

    -- Modify creation parameters

    ALLEGRO_NEW_WINDOW_TITLE_MAX_SIZE : constant := 255;

    procedure Al_Set_New_Display_Adapter( adapter : Integer );
    pragma Import( C, Al_Set_New_Display_Adapter, "al_set_new_display_adapter" );

    procedure Al_Set_New_Display_Flags( flags : Display_Flags );
    pragma Import( C, Al_Set_New_Display_Flags, "al_set_new_display_flags" );

    procedure Al_Set_New_Display_Option( option     : Display_Option;
                                         value      : Unsigned_32;
                                         importance : Option_Importance );
    pragma Import( C, Al_Set_New_Display_Option, "al_set_new_display_option" );

    procedure Al_Reset_New_Display_Options;
    pragma Import( C, Al_Reset_New_Display_Options, "al_reset_new_display_options" );

    procedure Al_Set_New_Display_Refresh_Rate( refresh_rate : Integer );
    pragma Import( C, Al_Set_New_Display_Refresh_Rate, "al_set_new_display_refresh_rate" );

    procedure Al_Set_New_Window_Position( x, y : Integer );
    pragma Import( C, Al_Set_New_Window_Position, "al_set_new_window_position" );

    procedure Al_Set_New_Window_Title( title : String );

    -- Display creation

    function Al_Create_Display( w, h : Integer ) return A_Allegro_Display;
    pragma Import( C, Al_Create_Display, "al_create_display" );

    procedure Al_Destroy_Display( display : in out A_Allegro_Display );

    -- Get display properties

    function Al_Get_Backbuffer( display : A_Allegro_Display ) return A_Allegro_Bitmap;
    pragma Import( C, Al_Get_Backbuffer, "al_get_backbuffer" );

    function Al_Get_Display_Width( display : A_Allegro_Display ) return Integer;
    pragma Import( C, Al_Get_Display_Width, "al_get_display_width" );

    function Al_Get_Display_Height( display : A_Allegro_Display ) return Integer;
    pragma Import( C, Al_Get_Display_Height, "al_get_display_height" );

    function Al_Get_Display_Flags( display : A_Allegro_Display ) return Display_Flags;
    pragma Import( C, Al_Get_Display_Flags, "al_get_display_flags" );

    function Al_Get_Display_Format( display : A_Allegro_Display ) return Allegro_Pixel_Format;
    pragma Import( C, Al_Get_Display_Format, "al_get_display_format" );

    function Al_Get_Display_Option( display : A_Allegro_Display;
                                    option  : Display_Option ) return Integer;
    pragma Import( C, Al_Get_Display_Option, "al_get_display_option" );

    function Al_Get_Display_Orientation( display : A_Allegro_Display ) return Allegro_Display_Orientation;
    pragma Import( C, Al_Get_Display_Orientation, "al_get_display_orientation" );

    function Al_Get_Display_Refresh_Rate( display : A_Allegro_Display ) return Integer;
    pragma Import( C, Al_Get_Display_Refresh_Rate, "al_get_display_refresh_rate" );

    procedure Al_Get_Window_Position( display : A_Allegro_Display;
                                      x, y    : in out Integer );
    pragma Import( C, Al_Get_Window_Position, "al_get_window_position" );

    procedure Al_Get_Window_Constraints( display      : A_Allegro_Display;
                                         min_w, min_h : out Integer;
                                         max_w, max_h : out Integer );

    -- Set display properties

    procedure Al_Set_Display_Icon( display : A_Allegro_Display;
                                   icon    : A_Allegro_Bitmap );
    pragma Import( C, Al_Set_Display_Icon, "al_set_display_icon" );

    type Allegro_Bitmap_Array is array (Integer range <>) of A_Allegro_Bitmap;

    function Al_Set_Display_Flag( display : A_Allegro_Display;
                                  flag    : Display_Flags;
                                  onoff   : Boolean ) return Boolean;

    procedure Al_Set_Display_Flag( display : A_Allegro_Display;
                                   flag    : Display_Flags;
                                   onoff   : Boolean );

    procedure Al_Set_Display_Icons( display : A_Allegro_Display;
                                    icons   : Allegro_Bitmap_Array );

    procedure Al_Set_Display_Option( display : A_Allegro_Display;
                                     option  : Display_Option;
                                     value   : Integer );
    pragma Import( C, Al_Set_Display_Option, "al_set_display_option" );

    procedure Al_Set_Window_Position( display : A_Allegro_Display; x, y : Integer );
    pragma Import( C, Al_Set_Window_Position, "al_set_window_position" );

    function Al_Set_Window_Constraints( display      : A_Allegro_Display;
                                        min_w, min_h : Integer;
                                        max_w, max_h : Integer ) return Boolean;

    procedure Al_Set_Window_Constraints( display      : A_Allegro_Display;
                                         min_w, min_h : Integer;
                                         max_w, max_h : Integer );

    procedure Al_Apply_Window_Constraints( display : A_Allegro_Display; onoff : Boolean );

    procedure Al_Set_Window_Title( display : A_Allegro_Display; title : String );

    -- Current display operations

    procedure Al_Backup_Dirty_Bitmaps( display : A_Allegro_Display );
    pragma Import( C, Al_Backup_Dirty_Bitmaps, "al_backup_dirty_bitmaps" );

    function Al_Get_Current_Display return A_Allegro_Display;
    pragma Import( C, Al_Get_Current_Display, "al_get_current_display" );

    function Al_Get_Target_Bitmap return A_Allegro_Bitmap;
    pragma Import( C, Al_Get_Target_Bitmap, "al_get_target_bitmap" );

    procedure Al_Set_Target_Bitmap( bitmap : A_Allegro_Bitmap );
    pragma Import( C, Al_Set_Target_Bitmap, "al_set_target_bitmap" );

    procedure Al_Set_Target_Backbuffer( display : A_Allegro_Display );
    pragma Import( C, Al_Set_Target_Backbuffer, "al_set_target_backbuffer" );

    function Al_Wait_For_Vsync return Boolean;

    procedure Al_Flip_Display;
    pragma Import( C, Al_Flip_Display, "al_flip_display" );

    procedure Al_Update_Display_Region( x, y          : Integer;
                                        width, height : Integer );
    pragma Import( C, Al_Update_Display_Region, "al_update_display_region" );

    procedure Al_Hold_Bitmap_Drawing( hold : Boolean );

    function Al_Is_Bitmap_Drawing_Held return Boolean;

    function Al_Inhibit_Screensaver( inhibit : Boolean ) return Boolean;

    function Al_Is_Compatible_Bitmap( bitmap : A_Allegro_Bitmap ) return Boolean;

    -- Display resizing

    function Al_Acknowledge_Resize( display : A_Allegro_Display ) return Boolean;

    -- Calls Al_Acknowledge_Resize, ignoring any errors.
    procedure Al_Acknowledge_Resize( display : A_Allegro_Display );

    function Al_Resize_Display( display       : A_Allegro_Display;
                                width, height : Integer ) return Boolean;

    procedure Al_Resize_Display( display       : A_Allegro_Display;
                                 width, height : Integer );

    -- Events

    function Al_Get_Display_Event_Source( display : A_Allegro_Display ) return A_Allegro_Event_Source;
    pragma Import( C, Al_Get_Display_Event_Source, "al_get_display_event_source" );

    procedure Al_Acknowledge_Drawing_Halt( display : A_Allegro_Display );
    pragma Import( C, Al_Acknowledge_Drawing_Halt, "al_acknowledge_drawing_halt" );

    procedure Al_Acknowledge_Drawing_Resume( display : A_Allegro_Display );
    pragma Import( C, Al_Acknowledge_Drawing_Resume, "al_acknowledge_drawing_resume" );

private

    type Allegro_Display is limited null record;
    pragma Convention( C, Allegro_Display );

    ALLEGRO_WINDOWED                  : constant Display_Flags := 2 **  0;
    ALLEGRO_FULLSCREEN                : constant Display_Flags := 2 **  1;
    ALLEGRO_OPENGL                    : constant Display_Flags := 2 **  2;
    ALLEGRO_DIRECT3D                  : constant Display_Flags := 2 **  3;
    ALLEGRO_RESIZABLE                 : constant Display_Flags := 2 **  4;
    ALLEGRO_FRAMELESS                 : constant Display_Flags := 2 **  5;
    ALLEGRO_NOFRAME                   : constant Display_Flags := ALLEGRO_FRAMELESS;
    ALLEGRO_GENERATE_EXPOSE_EVENTS    : constant Display_Flags := 2 **  6;
    ALLEGRO_OPENGL_3_0                : constant Display_Flags := 2 **  7;
    ALLEGRO_OPENGL_FORWARD_COMPATIBLE : constant Display_Flags := 2 **  8;
    ALLEGRO_FULLSCREEN_WINDOW         : constant Display_Flags := 2 **  9;
    ALLEGRO_MINIMIZED                 : constant Display_Flags := 2 ** 10;
    ALLEGRO_PROGRAMMABLE_PIPELINE     : constant Display_Flags := 2 ** 11;
    ALLEGRO_GTK_TOPLEVEL_INTERNAL     : constant Display_Flags := 2 ** 12;
    ALLEGRO_MAXIMIZED                 : constant Display_Flags := 2 ** 13;
    ALLEGRO_OPENGL_ES_PROFILE         : constant Display_Flags := 2 ** 14;

    ALLEGRO_RED_SIZE               : constant Display_Option := 0;
    ALLEGRO_GREEN_SIZE             : constant Display_Option := 1;
    ALLEGRO_BLUE_SIZE              : constant Display_Option := 2;
    ALLEGRO_ALPHA_SIZE             : constant Display_Option := 3;
    ALLEGRO_RED_SHIFT              : constant Display_Option := 4;
    ALLEGRO_GREEN_SHIFT            : constant Display_Option := 5;
    ALLEGRO_BLUE_SHIFT             : constant Display_Option := 6;
    ALLEGRO_ALPHA_SHIFT            : constant Display_Option := 7;
    ALLEGRO_ACC_RED_SIZE           : constant Display_Option := 8;
    ALLEGRO_ACC_GREEN_SIZE         : constant Display_Option := 9;
    ALLEGRO_ACC_BLUE_SIZE          : constant Display_Option := 10;
    ALLEGRO_ACC_ALPHA_SIZE         : constant Display_Option := 11;
    ALLEGRO_STEREO                 : constant Display_Option := 12;
    ALLEGRO_AUX_BUFFERS            : constant Display_Option := 13;
    ALLEGRO_COLOR_SIZE             : constant Display_Option := 14;
    ALLEGRO_DEPTH_SIZE             : constant Display_Option := 15;
    ALLEGRO_STENCIL_SIZE           : constant Display_Option := 16;
    ALLEGRO_SAMPLE_BUFFERS         : constant Display_Option := 17;
    ALLEGRO_SAMPLES                : constant Display_Option := 18;
    ALLEGRO_RENDER_METHOD          : constant Display_Option := 19;
    ALLEGRO_FLOAT_COLOR            : constant Display_Option := 20;
    ALLEGRO_FLOAT_DEPTH            : constant Display_Option := 21;
    ALLEGRO_SINGLE_BUFFER          : constant Display_Option := 22;
    ALLEGRO_SWAP_METHOD            : constant Display_Option := 23;
    ALLEGRO_COMPATIBLE_DISPLAY     : constant Display_Option := 24;
    ALLEGRO_UPDATE_DISPLAY_REGION  : constant Display_Option := 25;
    ALLEGRO_VSYNC                  : constant Display_Option := 26;
    ALLEGRO_MAX_BITMAP_SIZE        : constant Display_Option := 27;
    ALLEGRO_SUPPORT_NPOT_BITMAP    : constant Display_Option := 28;
    ALLEGRO_CAN_DRAW_INTO_BITMAP   : constant Display_Option := 29;
    ALLEGRO_SUPPORT_SEPARATE_ALPHA : constant Display_Option := 30;
    ALLEGRO_AUTO_CONVERT_BITMAPS   : constant Display_Option := 31;
    ALLEGRO_SUPPORTED_ORIENTATIONS : constant Display_Option := 32;
    ALLEGRO_OPENGL_MAJOR_VERSION   : constant Display_Option := 33;
    ALLEGRO_OPENGL_MINOR_VERSION   : constant Display_Option := 34;

    ALLEGRO_DONTCARE : constant Option_Importance := 0;
    ALLEGRO_REQUIRE  : constant Option_Importance := 1;
    ALLEGRO_SUGGEST  : constant Option_Importance := 2;

    ALLEGRO_DISPLAY_ORIENTATION_UNKNOWN     : constant Allegro_Display_Orientation := 0;
    ALLEGRO_DISPLAY_ORIENTATION_0_DEGREES   : constant Allegro_Display_Orientation := 1;
    ALLEGRO_DISPLAY_ORIENTATION_90_DEGREES  : constant Allegro_Display_Orientation := 2;
    ALLEGRO_DISPLAY_ORIENTATION_180_DEGREES : constant Allegro_Display_Orientation := 4;
    ALLEGRO_DISPLAY_ORIENTATION_270_DEGREES : constant Allegro_Display_Orientation := 8;
    ALLEGRO_DISPLAY_ORIENTATION_PORTRAIT    : constant Allegro_Display_Orientation := ALLEGRO_DISPLAY_ORIENTATION_0_DEGREES or ALLEGRO_DISPLAY_ORIENTATION_180_DEGREES;
    ALLEGRO_DISPLAY_ORIENTATION_LANDSCAPE   : constant Allegro_Display_Orientation := ALLEGRO_DISPLAY_ORIENTATION_90_DEGREES or ALLEGRO_DISPLAY_ORIENTATION_270_DEGREES;
    ALLEGRO_DISPLAY_ORIENTATION_ALL         : constant Allegro_Display_Orientation := ALLEGRO_DISPLAY_ORIENTATION_PORTRAIT or ALLEGRO_DISPLAY_ORIENTATION_LANDSCAPE;
    ALLEGRO_DISPLAY_ORIENTATION_FACE_UP     : constant Allegro_Display_Orientation := 16;
    ALLEGRO_DISPLAY_ORIENTATION_FACE_DOWN   : constant Allegro_Display_Orientation := 32;

end Allegro.Displays;
