--
-- Copyright (c) 2013 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

with Interfaces.C;                      use Interfaces.C;
with Interfaces.C.Strings;              use Interfaces.C.Strings;

package body Allegro.System is

    function Al_Version_To_String( version : Unsigned_32 ) return String is
        major   : constant String := Unsigned_32'Image( Shift_Right( version, 24 ) );
        minor   : constant String := Unsigned_32'Image( Shift_Right( version, 16 ) and 16#0000FF# );
        patch   : constant String := Unsigned_32'Image( version and 16#0000FF# );
    begin
        return major(major'First+1..major'Last) & '.' &
               minor(minor'First+1..minor'Last) & '.' &
               patch(patch'First+1..patch'Last);
    end Al_Version_To_String;

    ----------------------------------------------------------------------------

    function Al_Copyright return String is
    begin
        return "Copyright � 2004-2011 the Allegro 5 Development Team";
    end Al_Copyright;

    ----------------------------------------------------------------------------

    function Al_Get_App_Name return String is

        function C_Get_App_Name return C.Strings.chars_ptr;
        pragma Import( C, C_Get_App_Name, "al_get_app_name" );

    begin
        return To_Ada( Value( C_Get_App_Name ) );
    end Al_Get_App_Name;

    ----------------------------------------------------------------------------

    procedure Al_Set_Exe_Name( path : String ) is

        procedure C_Al_Set_Exe_Name( path : C.char_array );
        pragma Import( C, C_Al_Set_Exe_Name, "al_set_exe_name" );

    begin
        C_Al_Set_Exe_Name( To_C( path ) );
    end Al_Set_Exe_Name;

    ----------------------------------------------------------------------------

    function Al_Get_Org_Name return String is

        function C_Al_Get_Org_Name return C.Strings.chars_ptr;
        pragma Import( C, C_Al_Get_Org_Name, "al_get_org_name" );

    begin
        return To_Ada( Value( C_Al_Get_Org_Name ) );
    end Al_Get_Org_Name;

    ----------------------------------------------------------------------------

    procedure Al_Set_Org_Name( org_name : String ) is

        procedure C_Al_Set_Org_Name( org_name : C.char_array );
        pragma Import( C, C_Al_Set_Org_Name, "al_set_org_name" );

    begin
        C_Al_Set_Org_Name( To_C( org_name ) );
    end Al_Set_Org_Name;

    ----------------------------------------------------------------------------

    procedure Al_Set_App_Name( app_name : String ) is

        procedure C_Al_Set_App_Name( app_name : C.char_array );
        pragma Import( C, C_Al_Set_App_Name, "al_set_app_name" );

    begin
        C_Al_Set_App_Name( To_C( app_name ) );
    end Al_Set_App_Name;

    ----------------------------------------------------------------------------

    function Al_Initialize return Boolean is

        function C_Al_Initialize return Bool;
        pragma Import( C, C_Al_Initialize, "al_initialize" );

    begin
        return To_Ada( C_Al_Initialize );
    end Al_Initialize;

    ----------------------------------------------------------------------------

    function Al_Is_System_Installed return Boolean is

        function C_Al_Is_System_Installed return Bool;
        pragma Import( C, C_Al_Is_System_Installed, "al_is_system_installed" );

    begin
        return To_Ada( C_Al_Is_System_Installed );
    end Al_Is_System_Installed;

end Allegro.System;
