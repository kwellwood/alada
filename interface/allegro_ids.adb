--
-- Copyright (c) 2013-2017 Kevin Wellwood
-- All rights reserved.
--
-- This source code is distributed under the Modified BSD License. For terms and
-- conditions, see license.txt.
--

package body Allegro_Ids is

    type AL_ID_Overlay is
        record
            d : Character;
            c : Character;
            b : Character;
            a : Character;
        end record;
    for AL_ID_Overlay'Size use AL_ID'Size;

    ----------------------------------------------------------------------------

    function To_AL_ID( a, b, c, d : Character ) return AL_ID is
        result  : AL_ID;
        overlay : AL_ID_Overlay;
        for overlay'Address use result'Address;
    begin
        overlay.a := a;
        overlay.b := b;
        overlay.c := c;
        overlay.d := d;
        return result;
    end To_AL_ID;

    ----------------------------------------------------------------------------

    function To_AL_ID( n : Unsigned_32 ) return AL_ID is
    begin
        return AL_ID(n);
    end To_AL_ID;

    ----------------------------------------------------------------------------

    function To_String( id : AL_ID ) return String is
        overlay : AL_ID_Overlay;
        for overlay'Address use id'Address;
    begin
        if id /= 0 then
            return overlay.a & overlay.b & overlay.c & overlay.d;
        end if;
        return "";
    end To_String;

    ----------------------------------------------------------------------------

    function To_Unsigned_32( id : AL_ID ) return Unsigned_32 is
    begin
        return Unsigned_32(id);
    end To_Unsigned_32;

end Allegro_Ids;
