Allegro 5.2.5 pre-compiled libraries for Windows
Build type: Release
Applied patch in /docs/allegro-fixes-5.2.5-1.patch

Compiled with:
  MinGW64 x86_64 gcc 7.3.0 (64-bits)

Linked with libraries:
  dumb 2.0.3
  flac 1.3.2
  freetype 2.9.1
  libogg 1.3.3
  libpng 1.6.36
  libtheora 1.1.1
  libvorbis 1.3.6
  physicsfs 3.0.2
  zlib 1.2.11