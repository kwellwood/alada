Allegro 5.2.5 pre-compiled libraries for MacOS
Build type: Release
Applied patch in /docs/allegro-fixes-5.2.5-1.patch

Compiled with:
  OS X 10.12.6
  Apple LLVM version 8.0.0 (clang-800.0.42.1)

Linked with libraries:
  dumb 2.0.3
  flac 1.3.2
  freetype 2.9.1
  libogg 1.3.2
  libvorbis 1.3.6
  libtheora 1.1.1
  physicsfs 3.0.2
  zlib 1.2.11
